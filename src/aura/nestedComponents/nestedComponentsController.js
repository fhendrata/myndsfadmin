// this is about salesforce standard component and not Force.com component that can only be used in Salesforce1
({
    getNumbers: function(component) {         
        var numbers = [];
        for (var i = 0; i < 10; i++) {  // For Loop, declare an array of numbers using variable declaration
            numbers.push({              // Push method in the array this is similar array in apex
                value: i
            });
        }
        component.set("v.numbers", numbers);
    }
})