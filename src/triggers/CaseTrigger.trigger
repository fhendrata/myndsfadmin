trigger CaseTrigger on Case (before insert, before update) {
    if(Trigger.isInsert && Trigger.isBefore){
            ANH.AutoNumberHelper.OnBeforeInsert(Trigger.new);
    }
    else if(Trigger.isUpdate && Trigger.isBefore){
            ANH.AutoNumberHelper.OnBeforeUpdate(Trigger.new,Trigger.OldMap);
    }
}