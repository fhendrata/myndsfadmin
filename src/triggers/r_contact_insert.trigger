trigger r_contact_insert on Contact (after insert) 
{
	private Contact[] contactList = Trigger.new; 
  
  	if (contactList != null) 
  	{ 
  		Date todaysDate = date.today();
   		Date nextMonthsDate = todaysDate.addMonths(1);
  		
 		rpt_PhyProcessController cont = new rpt_PhyProcessController();

		DateTime selDt = DateTime.newInstance(todaysDate.year(), todaysDate.month(), 1);
		DateTime selDt2 = DateTime.newInstance(nextMonthsDate.year(), nextMonthsDate.month(), 1);
	    for (Contact c : contactList) 
	    {
	    	if (!e_StringUtil.isNullOrEmpty(c.RecordTypeId) && c.RecordTypeId != '0126000000056yj' && c.RecordTypeId != '01260000000563M')
	    	{
   				cont.buildRpt(c, selDt); 	
   				cont.buildRpt(c, selDt2);
	    	}
    	}
  	}
}