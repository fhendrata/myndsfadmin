trigger r_patient_update on r_patient__c (before update, after update) 
{   
	if (Trigger.isBefore)
		patientTriggerHandler.updatePatAndReegForDob(Trigger.new);
	else if (Trigger.isAfter)
		patientTriggerHandler.handleUpdate(Trigger.newMap);

}