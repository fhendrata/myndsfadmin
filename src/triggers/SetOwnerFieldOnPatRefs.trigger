trigger SetOwnerFieldOnPatRefs on Patient_Referral__c (before insert, before update) 
{
	Map<Id, Patient_Referral__c> contactIdPatRef = new Map<Id, Patient_Referral__c>();
	for(Patient_Referral__c pr : Trigger.new) 
	{
		if(String.isNotBlank(pr.Referred_To__c)) 
			contactIdPatRef.put(pr.Referred_To__c, pr);
		else 
			pr.addError('No contact set in Referred To field');
	}

	List<User> contactUsers = [Select Id, ContactId from User where ContactId IN: contactIdPatRef.keySet()];

	for(User user : contactUsers) 
	{
		contactIdPatRef.get(user.ContactId).OwnerId = user.Id;
		contactIdPatRef.remove(user.ContactId);
	}

	if(!contactIdPatRef.isEmpty()) 
	{
		for(Patient_Referral__c patRef : contactIdPatRef.values()) 
			patRef.addError('Contact does not have a portal user');
	}

}