trigger r_reeg_insert on r_reeg__c (before insert) {  
  
  private r_reeg__c[] reegList = Trigger.new; 
  private List<Id> patRefIds = new List<Id>();
  
  String phyID;
  String patDob; 
  double patAge;
  
  if (reegList != null) { 
 
    for (r_patient__c pat : [select Id, Name, dob__c, patient_age__c, physician__c, Patient_Referral__c from r_patient__c where Id =:reegList[0].r_patient_id__c Limit 1]) {
      phyID = pat.physician__c;
      patDob = pat.dob__c;
      patAge = pat.patient_age__c;
      //CNSLPR - 18
      if(pat.Patient_Referral__c != null) {
        patRefIds.add(pat.Patient_Referral__c);
      }
    }
    
    if (!e_StringUtil.isNullOrEmpty(patDob) && patDob.contains('/'))
    {
    	List<string> dateList = patDob.split('/');
    	if (dateList != null && dateList.size() > 2)
    	{
    		if (reegList[0].req_date__c != null)
    		{
    			datetime myDate = datetime.newInstance(integer.valueOf(dateList[2]), integer.valueOf(dateList[0]), integer.valueOf(dateList[1]));
    			reegList[0].patient_age__c = rEEGUtil.getPatientAgeDbl(patDob, reegList[0].req_date__c);
    		}
    	}
    }
    else if (patAge != null && patAge > 0.1)
    {
    	reegList[0].patient_age__c = patAge;
    }
    
    ActionDao.insertNewReegAction(reegList[0].Id);
    
    reegList[0].r_physician_id__c = phyID;

    //CNSLPR-18
    if(!patRefIds.isEmpty()) {
      List<Patient_Referral__c> patRefs = [Select Id,PEER_Ordered__c from Patient_Referral__c where Id IN: patRefIds];
      for(Patient_Referral__c patRef : patRefs) {
        patRef.PEER_Ordered__c = Date.today();
      }
      update patRefs;
    }
  }

}