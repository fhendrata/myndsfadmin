trigger r_Patient_Referral_Status_Update on Patient_Referral__c (after update) {

	Set<Id> prsAcceptIds = new Set<Id>();
	Set<Id> leadIds = new Set<Id>();

	for(Patient_Referral__c pr : Trigger.new) {
		if(pr.Physician_Referral_Status__c == 'Accepted' && Trigger.oldMap.get(pr.Id).Physician_Referral_Status__c != 'Accepted') {
			prsAcceptIds.add(pr.Id);
			leadIds.add(pr.Lead__c);
		}
	}

	if(prsAcceptIds.size() > 0) {
		List<Patient_Referral__c> updatePatRef = [Select Physician_Referral_Status__c from Patient_Referral__c where Lead__c IN: leadIds];
		for(Patient_Referral__c pr : updatePatRef) {
			if(!prsAcceptIds.contains(pr.Id)) {
				pr.Physician_Referral_Status__c = 'Missed';
			}
		}
		update updatePatRef;
	}
}