trigger r_reeg_delete on r_reeg__c (before delete) 
{
	private r_reeg__c[] reegListOld = Trigger.old;
  	if (reegListOld != null && reegListOld.size() > 0) 
  	{ 
		ActionDao.deleteReegActionNF(reegListOld[0].Id); 
    }
}