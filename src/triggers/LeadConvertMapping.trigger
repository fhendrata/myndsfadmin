trigger LeadConvertMapping on Lead (after update) {
	Map<Id, Lead> leadAccountMap = new Map<Id, Lead>();
	Map<Id, Lead> leadContactMap = new Map<Id, Lead>();
	for(Lead ld : Trigger.new) {
		if(ld.isConverted) {
			leadAccountMap.put(ld.ConvertedAccountId,ld);
			leadContactMap.put(ld.ConvertedContactId,ld);
		}
	}

	List<Contact> convContacts = [Select Id, Practice_Type__c, Primary_Specialty__c, Other_Specialty_s__c, Ave_New_Patients_Month__c,
									Treats_Military_Veterans__c,Patient_Type_s_Treated__c,Other_Treatment_Type_s_Provided__c,Insurance_Carrier_s_Accepted__c,
									Other_Insurance_Carrier__c from Contact where Id IN: leadContactMap.keySet()];
	List<Account> convAccounts = [Select Id, Practice_Type__c,Number_of_Physicians_in_Practice__c,EEG_Tech__c,
									Treats_Military_Veterans__c,Patient_Type_s_Treated__c,EEG_Availability__c,Insurance_Carrier_s_Accepted__c,
									Other_Insurance_Carrier__c from Account where Id IN: leadAccountMap.keySet()];

	for(Contact contact : convContacts) {
		contact.Practice_Type__c = leadContactMap.get(contact.Id).Practice_Type__c;
		contact.Primary_Specialty__c = leadContactMap.get(contact.Id).Primary_Specialty__c;
		contact.Other_Specialty_s__c = leadContactMap.get(contact.Id).Other_Specialty_s__c;
		contact.Ave_New_Patients_Month__c = leadContactMap.get(contact.Id).Ave_New_Patients_Month__c;
		contact.Treats_Military_Veterans__c = leadContactMap.get(contact.Id).Treats_Military_Veterans__c;
		contact.Patient_Type_s_Treated__c = leadContactMap.get(contact.Id).Patient_Type_s_Treated__c;
		contact.Other_Treatment_Type_s_Provided__c = leadContactMap.get(contact.Id).Other_Treatment_Type_s_Provided__c;
		contact.Insurance_Carrier_s_Accepted__c = leadContactMap.get(contact.Id).Insurance_Carrier_s_Accepted__c;
		contact.Other_Insurance_Carrier__c = leadContactMap.get(contact.Id).Other_Insurance_Carrier__c;
	}

	for(Account account : convAccounts) {
		account.Practice_Type__c = leadAccountMap.get(account.Id).Practice_Type__c;
		account.Number_of_Physicians_in_Practice__c = leadAccountMap.get(account.Id).Number_of_Physicians_in_Practice__c;
		account.EEG_Tech__c = leadAccountMap.get(account.Id).EEG_Tech__c;
		account.Treats_Military_Veterans__c = leadAccountMap.get(account.Id).Treats_Military_Veterans__c;
		account.Patient_Type_s_Treated__c = leadAccountMap.get(account.Id).Patient_Type_s_Treated__c;
		account.EEG_Availability__c = leadAccountMap.get(account.Id).EEG_Availability__c;
		account.Insurance_Carrier_s_Accepted__c = leadAccountMap.get(account.Id).Insurance_Carrier_s_Accepted__c;
		account.Other_Insurance_Carrier__c = leadAccountMap.get(account.Id).Other_Insurance_Carrier__c;
	}

	update convContacts;
	update convAccounts;
}