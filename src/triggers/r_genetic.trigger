//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//   Trigger: r_genetic
//    Object: Genetic__c
// 
//     OWNER: CNS Response
//   CREATED: 02/13/18 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
trigger r_genetic on Genetic__c (after insert, after update, after delete, after undelete) 
{

	if (Trigger.isInsert)
		GeneticTriggerHandler.handleGeneticInsert(Trigger.new);
	else if (Trigger.isUpdate || Trigger.isUndelete)
		GeneticTriggerHandler.handleGeneticUpdate(Trigger.new);
	else if (Trigger.isDelete)
		GeneticTriggerHandler.handleGeneticDelete(Trigger.oldMap);

}