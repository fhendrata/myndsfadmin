trigger r_reeg_status_change on r_reeg__c (before update) {  
  
  private r_reeg__c[] reegList = Trigger.new; 
  
  if (reegList != null && reegList[0].rpt_stat__c != null && reegList[0].rpt_stat__c.startsWith('QA')) 
  { 
    reegList[0].req_stat__c = 'In Progress';
  }
}