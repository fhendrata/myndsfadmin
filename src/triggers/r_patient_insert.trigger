trigger r_patient_insert on r_patient__c (before insert, after insert) {  
  
  private r_patient__c[] patientList = Trigger.new; 
  
  String userID; 
  
  if (Trigger.isBefore && patientList != null) 
  { 
    if (e_StringUtil.isNullOrEmpty(patientList[0].physician__c))
    {
	    for (User u : [select Id, Name from User where Id =:patientList[0].OwnerId Limit 1]) {
	      userId = u.Id;
	    }
		 
	    for (Contact c : [select Id, Name from Contact where physician_portal_user__c =: userId Limit 1]) {
	      patientList[0].physician__c = c.Id; 
	    }
    }
    
    patientTriggerHandler.updatePatAndReegForDob(Trigger.new);
    
  }
  else if (Trigger.isAfter)
  {
    patientTriggerHandler.handleInsert(Trigger.new);
  }
}