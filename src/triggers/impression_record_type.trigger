trigger impression_record_type on Impression__c (before insert, before update) 
{
	RecordType monitoringRt = [select id from RecordType where SObjectType = 'Impression__c' and Name = 'Monitor' limit 1];
	RecordType newSubjectRt = [select id from RecordType where SObjectType = 'Impression__c' and Name = 'New Subject' limit 1];
	
	system.debug('### monitoringRt:' + monitoringRt);
	system.debug('### newSubjectRt:' + newSubjectRt);
		
	for (Impression__c impression : Trigger.new)
	{
		if (impression.completed__c != null)
		{	
			if (monitoringRt != null && newSubjectRt != null)
			{
				impression.RecordTypeId = (impression.completed__c == true) ? monitoringRt.Id : newSubjectRt.Id;
				system.debug('### impression.RecordTypeId:' + impression.RecordTypeId);
			}
			
		}
	}

}