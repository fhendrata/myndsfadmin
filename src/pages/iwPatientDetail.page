<apex:page standardController="r_patient__c" extensions="i_patientControllerExt" id="patientView" showHeader="false" 
	standardStylesheets="false" id="p">
<apex:form >
<apex:composition template="iwTemplate">
	<apex:define name="body">
	<apex:stylesheet value="{!$Resource.CNS_Util}/Theme/iwPatientDetail.css"/>
		<div id="topbar" class="blackMed">
			<div id="title">Referenced-EEG<sup>®</sup> (rEEG<sup>®</sup>)</div>
			<div id="leftnav">
				<a href="iwPatient">&nbsp;<img alt="iwPatient" src="{!$Resource.iWeb}/images/srch_icon_sml.png" class="patDet" />&nbsp;</a>
			</div>			
			<div id="rightnav">
				<span>{!$User.Username}</span>
				<input type="button" class="loBtn" value="Logout" onclick="parent.location='{!logoutUrl}'" />	
			</div>
		</div><!--  <span>{!$User.Username}</span> -->
		<div id="content">
			<span class="pageTitle">{!r_patient__c.name}, {!r_patient__c.first_name__c} </span>
			<div class="clear"></div>
			
			<apex:outputPanel rendered="{!$CurrentPage.parameters.show='all' || $CurrentPage.parameters.show='dem'}">
			<span class="graytitle">Demographics</span>
			<ul class="pageitem">
				<li class="textbox">
					<label class="colHdrL">Date of Birth:</label>
					<span class="dataL"><apex:outputText value="{!r_patient__c.dob__c}" id="pdob" /></span>
					<label class="colHdrR">Owner:</label>
					<span class="dataR"><apex:outputField value="{!r_patient__c.OwnerId}" id="poid"  /></span>					
				</li>								
				<li class="textbox">
					<label class="colHdrL">Handedness:</label>
					<span class="dataL"><apex:outputText value="{!r_patient__c.handedness__c}" id="phand" /></span>
					<label class="colHdrR">Gender:</label>
					<span class="dataR"><apex:outputText value="{!r_patient__c.Gender__c}" id="pgndr" /></span>					
				</li>									
				<li class="textbox">
					<label class="colHdrL">Anticipated EEG Test:</label>
					<span class="dataL"><apex:outputField value="{!r_patient__c.anticipated_eeg_test__c}" id="ptest" /></span>
					<label class="colHdrR"></label>
					<span class="dataR"></span>					
				</li>						
			</ul>
			</apex:outputPanel>

			<apex:outputPanel rendered="{!$CurrentPage.parameters.show='all' || $CurrentPage.parameters.show='notes'}">
			<span class="graytitle">Research Notes
			</span>
			<ul class="pageitem">
				<apex:repeat value="{!patientNotes}" var="note">				
					<li class="menu"><a href="/apex/iwPatientNotesView?pid={!r_patient__c.id}&id={!note.Id}&retUrl=/apex/iwPatientDetail?show=all|id={!r_patient__c.id}">
						<img alt="list" src="{!$Resource.CNS_Util}/Images/pat_notes_icon_sm.png" />
						<span class="name">{!note.name}</span>					
						<span class="medName"><apex:outputField value="{!note.CreatedDate}"/></span>
						<span class="lrgName">Subject:&nbsp;<apex:outputText value="{!LEFT(note.subject__c,20)}" /></span>
						<span class="lrgName">Created By:&nbsp;<apex:outputField value="{!note.CreatedById}" /></span>						
						<span class="arrow"></span></a>
					</li>
				</apex:repeat>
			</ul>
			<input type="button" class="rightBtn" value="New Note" onclick="parent.location='/apex/iwPatientNotesEdit?pid={!r_patient__c.id}&isNew=isNew&retUrl=/apex/iwPatientDetail?show=all|id={!r_patient__c.id}'" />
			<br/><br/>
			</apex:outputPanel>
			
			<apex:outputPanel rendered="{!$CurrentPage.parameters.show='all' || $CurrentPage.parameters.show='med'}">
			<span class="graytitle">Medications</span>
			<ul class="pageitem">
				<apex:repeat value="{!oMeds}" var="med">			
					<li class="menu"><a href="">
						<img  alt="list" src="{!$Resource.CNS_Util}/Images/Meds_Icon_sm.png" />
						<span class="name">{!med.obj.med_name__c}</span>
						<span class="shrtName">
							Dose:&nbsp;<apex:outputField id="doseFld" value="{!med.obj.dosage__c}" />
							<apex:outputField id="unitFld" value="{!med.obj.unit__c}" />
						</span>
						<span class="shrtName">Freq:&nbsp;<apex:outputField id="freqFld" value="{!med.obj.frequency__c}" /></span>
						<span class="lrgName">
							Start:&nbsp;<apex:outputField id="sDateFld" value="{!med.obj.start_date__c}" />&nbsp;
							<apex:outputText id="sDateFld2" value="*estimated" rendered="{!med.obj.start_date_is_estimated__c}" />
						</span>
						
						<span class="shrtName">End:&nbsp;<apex:outputField id="eDateFld" value="{!med.obj.end_date__c}" /></span>
						<span ></span></a>										
					</li>
				</apex:repeat>
			</ul>		
			</apex:outputPanel>
			
			<apex:outputPanel rendered="{!$CurrentPage.parameters.show='all' || $CurrentPage.parameters.show='reeg'}">
			<span class="graytitle">rEEG</span>
	 
			<ul class="pageitem">
				<apex:repeat value="{!reegs}" var="reegRow">				
					<li class="menu"><a href="/apex/iwrEEGSummary?id={!reegRow.obj.id}">
						<img alt="list" src="{!$Resource.CNS_Util}/Images/reeg_wave_icon_sm.png" />
						<span class="name">{!reegRow.obj.name}</span>
						<span class="shrtName first"><apex:outputText value="{!reegRow.obj.reeg_type__c}" /></span>
						<span class="lrgName"><apex:outputField value="{!reegRow.obj.eeg_rec_date__c}" /></span>
						<span class="medName">Status:&nbsp; <apex:outputText value="{!reegRow.obj.req_stat__c}" /></span>
						<span class="arrow"></span></a>
					</li>
				</apex:repeat>
			</ul>
	 
			</apex:outputPanel>
			
			<apex:outputPanel rendered="{!$CurrentPage.parameters.show='all' || $CurrentPage.parameters.show='outc'}">
			<span class="graytitle">Outcomes
			</span>
			<ul class="pageitem">
				<apex:repeat value="{!outcomes}" var="outcomeRow">				
					<li class="menu"><a href="/apex/iwPatientOutcomeView?pid={!r_patient__c.id}&id={!outcomeRow.obj.id}&oid={!outcomeRow.obj.id}&retUrl=/apex/iwPatientDetail?id={!r_patient__c.id}&show=all">
						<img alt="list" src="{!$Resource.CNS_Util}/Images/Outcome_Icon_sm.png" />
						<span class="name">{!outcomeRow.obj.name}</span>					
						<span class="shrtName first"><apex:outputField value="{!outcomeRow.obj.outcome_date__c}"/></span>
						<span class="shrtName">CGI-Physician:&nbsp;<apex:outputText value="{!outcomeRow.cgiPhy}" /></span>
						<span class="shrtName">CGI-Patient:&nbsp;<apex:outputText value="{!outcomeRow.cgiPat}" /></span>
						<span class="shrtName">CGS:&nbsp;<apex:outputText value="{!outcomeRow.cgs}" /></span>						
						<span class="arrow"></span></a>
					</li>
				</apex:repeat>
			</ul>
			<apex:commandButton value="Build Intervals" rendered="false" action="{!buildIntervals}" styleClass="cntrBtn right" rerender="intervals, is, ilr"/> &nbsp; &nbsp;
			<input type="button" class="rightBtn" value="New Outcome" onclick="parent.location='/apex/iwPatientOutcomeEdit?pid={!r_patient__c.id}&isNew=true&retUrl=/apex/iwPatientDetail?show=all|id={!r_patient__c.id}'" />
			<br/><br/>
			</apex:outputPanel>

			<apex:outputPanel rendered="false" id="intervals">
			<span class="graytitle">Intervals
			</span>
			<ul class="pageitem" id="is">
				<apex:repeat value="{!IntervalDispList}" var="int" id="ilr">
						<li class="menu"><a href="/apex/iwPatientIntervalsView?pid={!r_patient__c.id}&oid={!int.Interval.id}">
						<img alt="list" src="{!$Resource.CNS_Util}/Images/Ints_Icon_sm.png" />
						<span class="name">{!int.Interval.name}</span>					
						<span class="shrtName first">CGI Value:&nbsp;<apex:outputText value="{!int.Interval.cgi_value__c}"/></span>
						<span class="shrtName">Start:&nbsp;<apex:outputField value="{!int.Interval.start_date__c}"/></span>
						<span class="shrtName">Stop:&nbsp;<apex:outputField value="{!int.Interval.stop_date__c}"/></span>
						<span class="shrtName">Duration:&nbsp;<apex:outputText value="{!int.DurStr}" /></span>						
						<span class="arrow"></span></a>
					</li>    	
				</apex:repeat>
			</ul>
			</apex:outputPanel>	    	
				
		</div>
	</apex:define>
</apex:composition>

</apex:form>
</apex:page>