<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Record_Type_to_EEG_Tech</fullName>
        <field>RecordTypeId</field>
        <lookupValue>EEGTech_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to EEG Tech</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Generic</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Generic_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Generic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Government</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Government_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Government</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Investor</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Investor_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Investor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Media</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Media_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Media</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Provider</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Provider_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Provider</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CONTACT%3A Set Record Type to EEG Tech</fullName>
        <actions>
            <name>Set_Record_Type_to_EEG_Tech</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>EEG Testing Account</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CONTACT%3A Set Record Type to Generic</fullName>
        <actions>
            <name>Set_Record_Type_to_Generic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>Generic</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CONTACT%3A Set Record Type to Government</fullName>
        <actions>
            <name>Set_Record_Type_to_Government</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>Government</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CONTACT%3A Set Record Type to Investor</fullName>
        <actions>
            <name>Set_Record_Type_to_Investor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>Investor</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CONTACT%3A Set Record Type to Media</fullName>
        <actions>
            <name>Set_Record_Type_to_Media</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>Media</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CONTACT%3A Set Record Type to Provider</fullName>
        <actions>
            <name>Set_Record_Type_to_Provider</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Contact.Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>Provider Account</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
