<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Physician_Referral</fullName>
        <description>New Physician Referral</description>
        <protected>false</protected>
        <recipients>
            <recipient>etinney@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jfeintuch@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jhauser@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfadmin@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSR_Email_Templates/Physician_Referral_Alert</template>
    </alerts>
    <alerts>
        <fullName>Patient_LEAD_Send_State_Not_Covered_Email</fullName>
        <description>Patient LEAD: Send State Not Covered Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sfadmin@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/State_Not_Covered_Auto_Response_Jan2015</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_to_Geography_Not_Covered_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Geography_Not_Covered</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Geography Not Covered Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LEAD_Update_In_Process_Change_Date</fullName>
        <field>Lead_Status_Change_Date_In_Process__c</field>
        <formula>TODAY()</formula>
        <name>LEAD: Update In Process Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LEAD_Update_Record_Type_Hidden</fullName>
        <field>Record_Type_Hidden__c</field>
        <formula>RecordType.Name</formula>
        <name>LEAD: Update Record Type(Hidden)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>LEAD_Update_to_Patient_Initiated</fullName>
        <field>Lead_Initiation__c</field>
        <literalValue>Patient Initiated</literalValue>
        <name>LEAD: Update to Patient Initiated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Overwrite_No</fullName>
        <field>Willing_to_Pay_No_Count__c</field>
        <formula>0</formula>
        <name>Overwrite No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Overwrite_Yes</fullName>
        <field>Willing_to_Pay_Yes_Count__c</field>
        <formula>0</formula>
        <name>Overwrite Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_First_Name</fullName>
        <field>Patient_First_Name__c</field>
        <formula>FirstName</formula>
        <name>Patient First Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_Last_Name</fullName>
        <field>Patient_Last_Name__c</field>
        <formula>LastName</formula>
        <name>Patient Last Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Generic</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Generic_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Generic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Investor</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Investor_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Investor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Media</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Media_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Media</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Patient</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Patient_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Patient</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Provider</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Provider_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Provider</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Provider1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Provider_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Provider1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_record_Type_to_Government</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Government_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set record Type to Government</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Active_Date</fullName>
        <field>Lead_Status_Change_Date_Active__c</field>
        <formula>Lead_Status_Change_Date_EEG_Scheduled__c</formula>
        <name>Update Active Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Compassionate_use_Count</fullName>
        <field>Compassionate_Use_Count__c</field>
        <formula>1</formula>
        <name>Update Compassionate use Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Card_Count</fullName>
        <field>Credit_Card_Count__c</field>
        <formula>1</formula>
        <name>Update Credit Card Count</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Do_Not_Call</fullName>
        <field>DoNotCall</field>
        <literalValue>1</literalValue>
        <name>Update Do Not Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EEG_PEER_Requested_Date</fullName>
        <field>Lead_Status_Change_Date_EEG_PEER_Request__c</field>
        <formula>Lead_Status_Change_Date_EEG_Scheduled__c</formula>
        <name>Update EEG/PEER Requested Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_EEG_PEER_Status_Date</fullName>
        <field>Lead_Status_Change_Date_EEG_PEER_Request__c</field>
        <formula>TODAY()</formula>
        <name>Update EEG/PEER Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Email_Opt_Out</fullName>
        <field>HasOptedOutOfFax</field>
        <literalValue>1</literalValue>
        <name>Update Email Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Email_Opt_Out1</fullName>
        <field>HasOptedOutOfEmail</field>
        <literalValue>1</literalValue>
        <name>Update Email Opt Out</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Failure_Code_to_Geography_Not_Cov</fullName>
        <field>Failure_Code__c</field>
        <literalValue>Geography Not Covered</literalValue>
        <name>Update Failure Code to Geography Not Cov</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Origin_for_Web_Forms</fullName>
        <field>Lead_Origin__c</field>
        <literalValue>Web Form</literalValue>
        <name>Update Lead Origin for Web Forms</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Status_Change_Date</fullName>
        <field>Lead_Status_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Lead Status Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Status_to_Unqualified</fullName>
        <field>Status</field>
        <literalValue>Unqualified</literalValue>
        <name>Update Lead Status to Unqualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_OK_to_Nurture</fullName>
        <field>OK_to_Nurture__c</field>
        <literalValue>0</literalValue>
        <name>Update OK to Nurture</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_OK_to_Nurture1</fullName>
        <field>OK_to_Nurture__c</field>
        <literalValue>1</literalValue>
        <name>Update OK to Nurture</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PGx_Commission_Amount</fullName>
        <field>PGx_Commission_Amount__c</field>
        <formula>15</formula>
        <name>Update PGx Commission Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Revenue_Channel_Patient_Direct</fullName>
        <field>Revenue_Channel__c</field>
        <literalValue>Patient Direct</literalValue>
        <name>Update Revenue Channel-Patient Direct</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Change_Date_Active</fullName>
        <field>Lead_Status_Change_Date_Active__c</field>
        <formula>TODAY()</formula>
        <name>Update Status Change Date-Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Change_Date_EEG_Scheduled</fullName>
        <field>Lead_Status_Change_Date_EEG_Scheduled__c</field>
        <formula>TODAY()</formula>
        <name>Update Status Change Date-EEG Scheduled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Change_Date_In_Process</fullName>
        <field>Lead_Status_Change_Date_In_Process__c</field>
        <formula>TODAY()</formula>
        <name>Update Status Change Date-In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Change_Date_PEER_Completed</fullName>
        <field>Lead_Status_Change_Date_PEER_Completed__c</field>
        <formula>TODAY()</formula>
        <name>Update Status Change Date-PEER Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Change_Date_Refer_to_Phys</fullName>
        <field>Lead_Status_Change_Date_Refer_to_Phys__c</field>
        <formula>TODAY()</formula>
        <name>Update Status Change Date-Refer to Phys</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Willing_to_Pay_No</fullName>
        <field>Willing_to_Pay_No_Count__c</field>
        <formula>1</formula>
        <name>Update Willing to Pay-No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Willing_to_Pay_Yes</fullName>
        <field>Willing_to_Pay_Yes_Count__c</field>
        <formula>1</formula>
        <name>Update Willing to Pay Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_lead_Status_to_OK_to_Nurture</fullName>
        <field>Status</field>
        <literalValue>OK to Nurture</literalValue>
        <name>Update lead Status to OK to Nurture</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Physician_Initiated</fullName>
        <field>Lead_Initiation__c</field>
        <literalValue>Physician Initiated</literalValue>
        <name>Update to Physician Initiated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>LEAD%3A Caller is Patient</fullName>
        <actions>
            <name>Patient_First_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Patient_Last_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Calling_For__c</field>
            <operation>equals</operation>
            <value>Self</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Compassionate Use Count</fullName>
        <actions>
            <name>Update_Compassionate_use_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Compassionate_Use__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Credit Card Count</fullName>
        <actions>
            <name>Update_Credit_Card_Count</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Lead.Expiration_Month__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Expiration_Year__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CVC__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Lead Status %3D Inactive</fullName>
        <actions>
            <name>Update_Do_Not_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Email_Opt_Out1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_OK_to_Nurture</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Inactive</value>
        </criteriaItems>
        <description>Update Email Opt Out and OK to Nurture</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Patient Initiated</fullName>
        <actions>
            <name>LEAD_Update_to_Patient_Initiated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>notEqual</operation>
            <value>Physician Referral</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Physician Initiated</fullName>
        <actions>
            <name>Update_to_Physician_Initiated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Physician Referral</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Set Record Type</fullName>
        <actions>
            <name>LEAD_Update_Record_Type_Hidden</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Generic,Provider Lead,EEG Provider,Government,Investor,Media,Patient</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Set Record Type to Generic</fullName>
        <actions>
            <name>Set_Record_Type_to_Generic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Other</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Set Record Type to Government</fullName>
        <actions>
            <name>Set_record_Type_to_Government</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Government</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Set Record Type to Investor</fullName>
        <actions>
            <name>Set_Record_Type_to_Investor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Investor</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Set Record Type to Media</fullName>
        <actions>
            <name>Set_Record_Type_to_Media</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Media</value>
        </criteriaItems>
        <description>Lead Type = Journalist from the Web Form</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Set Record Type to Patient</fullName>
        <actions>
            <name>Set_Record_Type_to_Patient</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Patient</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Set Record Type to Provider</fullName>
        <actions>
            <name>Set_Record_Type_to_Provider1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Provider</value>
        </criteriaItems>
        <description>Lead Type = Physician from the Web Form</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Active Change Date</fullName>
        <actions>
            <name>Update_Active_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>EEG Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Status_Change_Date_Active__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update EEG%2FPEER Requested Change Date</fullName>
        <actions>
            <name>Update_EEG_PEER_Requested_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>EEG Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Status_Change_Date_EEG_PEER_Request__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update In Process Change Date</fullName>
        <actions>
            <name>LEAD_Update_In_Process_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>EEG Scheduled</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Status_Change_Date_In_Process__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Lead Origin Detail</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Lead_Source_Detail__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Lead Origin for Web Forms</fullName>
        <actions>
            <name>Update_Lead_Origin_for_Web_Forms</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Lead Status Change Date</fullName>
        <actions>
            <name>Update_Lead_Status_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Lead Status Change Date-Active</fullName>
        <actions>
            <name>Update_Status_Change_Date_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status) &amp;&amp;  ISPICKVAL(Status, &quot;Active&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Lead Status Change Date-EEG Scheduled</fullName>
        <actions>
            <name>Update_Status_Change_Date_EEG_Scheduled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status) &amp;&amp;  ISPICKVAL(Status, &quot;EEG Scheduled&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Lead Status Change Date-EEG%2FPEER Requested</fullName>
        <actions>
            <name>Update_EEG_PEER_Status_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status) &amp;&amp;  ISPICKVAL(Status, &quot;EEG/PEER Requested&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Lead Status Change Date-In Process</fullName>
        <actions>
            <name>Update_Status_Change_Date_In_Process</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status) &amp;&amp;  ISPICKVAL(Status, &quot;In Process&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Lead Status Change Date-PEER Completed</fullName>
        <actions>
            <name>Update_Status_Change_Date_PEER_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISCHANGED(Status) &amp;&amp;  ISPICKVAL(Status, &quot;PEER Completed&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Lead Status Change Date-Referred to Physician</fullName>
        <actions>
            <name>Update_Status_Change_Date_Refer_to_Phys</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status) &amp;&amp;  ISPICKVAL(Status, &quot;Referred to Physician&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Lead Status if OK to Nurture is True</fullName>
        <actions>
            <name>Update_lead_Status_to_OK_to_Nurture</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.OK_to_Nurture__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update OK to Nurture from Lead Status</fullName>
        <actions>
            <name>Update_OK_to_Nurture1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>OK to Nurture</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update PGx Commission Amount</fullName>
        <actions>
            <name>Update_PGx_Commission_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>EEG/PEER Requested,EEG Scheduled,PEER Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.PGx_Ordered__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.PGx_Commission_Amount__c</field>
            <operation>notEqual</operation>
            <value>15</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3A Update Revenue Channel-Patient Direct</fullName>
        <actions>
            <name>Update_Revenue_Channel_Patient_Direct</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Lead_Source_Detail__c</field>
            <operation>equals</operation>
            <value>Facebook</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3AAssign to Geography Not Covered Queue</fullName>
        <actions>
            <name>Update_Failure_Code_to_Geography_Not_Cov</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Status_to_Unqualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Patient Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.State</field>
            <operation>notEqual</operation>
            <value>AZ,BC,CA,CO,DC,FL,GA,HI,IL,MA,MD,NJ,NY,ON,OR,TN,TX,UT,VA</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3AWilling to Pay-No</fullName>
        <actions>
            <name>Overwrite_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Willing_to_Pay_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Willing_to_Pay__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>LEAD%3AWilling to Pay-Yes</fullName>
        <actions>
            <name>Overwrite_No</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Willing_to_Pay_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Willing_to_Pay__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Physician Referral Email Alert</fullName>
        <actions>
            <name>New_Physician_Referral</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>equals</operation>
            <value>Physician Referral</value>
        </criteriaItems>
        <description>When a new Physician Referral arrives, an email alert is sent to Scheduler and Regional</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Physician Referral Email Alert - NC</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>equals</operation>
            <value>Physician Referral</value>
        </criteriaItems>
        <description>When a new Physician Referral arrives, an email alert is sent to Scheduler and Regional for NC</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
