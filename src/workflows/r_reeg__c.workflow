<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Artifacting_Complete</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <description>Artifacting Complete - depr</description>
        <protected>false</protected>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/Artifacting</template>
    </alerts>
    <alerts>
        <fullName>Artifacting_Needed</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <description>Artifacting Needed</description>
        <protected>false</protected>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRWorkflowEmailTemplates/Artifacting_Needed2</template>
    </alerts>
    <alerts>
        <fullName>CA_PEER_report_is_ready_Email_Alert</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>CA PEER report is ready Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>customerPortalUser</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/PO_Rpt_Complete</template>
    </alerts>
    <alerts>
        <fullName>EEGsubmittedEmail</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>EEG submitted Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mmetzig@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/EEGfilesubmittedtemplate</template>
    </alerts>
    <alerts>
        <fullName>General_Study_Report_Ready_Control_Email_Alert</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>General Study - Report Ready - Control Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mmetzig@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/General_Study_Report_Ready_Control_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>General_Study_Report_Ready_Email_Alert</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>General Study - Report Ready - Treatment Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>r_physician_id__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Account_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mmetzig@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/General_Study_Report_Ready_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>PO_EEG_submitted_physician_email</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <description>PO EEG submitted (physician email)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/EEGfilesubmittedtemplate</template>
    </alerts>
    <alerts>
        <fullName>PO_Neuro_Report_Available</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <description>PO Neuro Report Available</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/PO_Neuro_Rpt_Available</template>
    </alerts>
    <alerts>
        <fullName>PO_Rpt_Complete</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>PO Rpt complete</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mmetzig@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/Commercial_Report_Ready_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>ParcErrorEmail</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>jswenski@ethos.com</ccEmails>
        <description>Parc Error Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/Parc_Error_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Pre_Washout_EEG_submitted_Email_Alert</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>Pre-Washout EEG submitted Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>customerPortalUser</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/Pre_Washout_EEG_uploaded_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>WREEGsubmittedEmail</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <description>WR EEG submitted Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>CNSRProduction/WREEGfilesubmittedtemplate</template>
    </alerts>
    <alerts>
        <fullName>WR_Complete_Group</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>andis.tirana.ctr@health.mil</ccEmails>
        <description>WR Rpt complete &amp; randomized to Complete Group</description>
        <protected>false</protected>
        <recipients>
            <type>customerPortalUser</type>
        </recipients>
        <recipients>
            <type>customerPortalUser</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>bmacdonald@cnsresponse.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSR_Email_Production/Patient_Randomized_to_Complete_VF</template>
    </alerts>
    <alerts>
        <fullName>WR_Test_Complete</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <description>WR Rpt complete &amp; randomized to Treatment Group</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/WR_PEER_Complete</template>
    </alerts>
    <alerts>
        <fullName>WR_Test_Complete_mike_joe</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <description>WR Test Complete Just Mike &amp; Joe</description>
        <protected>false</protected>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>bmacdonald@cnsresponse.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/WR_PEER_Complete</template>
    </alerts>
    <alerts>
        <fullName>rEEGReviewEmail</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <description>rEEG Review Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/AnrEEGisreadyforreview</template>
    </alerts>
    <rules>
        <fullName>CA PEER report is ready</fullName>
        <actions>
            <name>CA_PEER_report_is_ready_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_reeg__c.neuroguide_status__c</field>
            <operation>equals</operation>
            <value>Report Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.Process_Used_For_Test__c</field>
            <operation>equals</operation>
            <value>Canadian Study</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_patient__c.wr_control_group_type__c</field>
            <operation>equals</operation>
            <value>Treatment Group</value>
        </criteriaItems>
        <description>Rule for determining if a NeuroGuide report is complete and the test is a CA test</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Commercial - Report Ready</fullName>
        <actions>
            <name>PO_Rpt_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_reeg__c.neuroguide_status__c</field>
            <operation>equals</operation>
            <value>Report Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.Process_Used_For_Test__c</field>
            <operation>notEqual</operation>
            <value>Military Study,Canadian Study</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Commercial</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Rule for determining if a NeuroGuide report is complete and the test is not a WR test</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>EEG file submitted</fullName>
        <actions>
            <name>EEGsubmittedEmail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>r_reeg__c.eeg_rec_stat__c</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.Process_Used_For_Test__c</field>
            <operation>notEqual</operation>
            <value>Military Study</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.Process_Used_For_Test__c</field>
            <operation>notEqual</operation>
            <value>Canadian Study</value>
        </criteriaItems>
        <description>This rule notifies CNSR personnel of a new EEG that has been submitted by an EEG tech.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>General Study - Report Ready - Control</fullName>
        <actions>
            <name>General_Study_Report_Ready_Control_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_reeg__c.neuroguide_status__c</field>
            <operation>equals</operation>
            <value>Report Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>General Study</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_patient__c.CA_Control_Group_Type__c</field>
            <operation>equals</operation>
            <value>Control Group,Not Randomized Group</value>
        </criteriaItems>
        <description>This workflow rule will send an email when a Control group PEER Report is ready.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>General Study - Report Ready - Treatment</fullName>
        <actions>
            <name>General_Study_Report_Ready_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_reeg__c.neuroguide_status__c</field>
            <operation>equals</operation>
            <value>Report Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>General Study</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_patient__c.CA_Control_Group_Type__c</field>
            <operation>equals</operation>
            <value>Treatment Group</value>
        </criteriaItems>
        <description>This workflow rule will send an email to the rEEG record&apos;s physician when their PEER Report is ready.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Artifacting Job</fullName>
        <actions>
            <name>Artifacting_Needed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>r_reeg__c.art_status__c</field>
            <operation>equals</operation>
            <value>Sent To Portal</value>
        </criteriaItems>
        <description>This rule will send an email alert when a new artifacting job is uploaded to the artifact portal.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New rEEG Requested</fullName>
        <active>false</active>
        <criteriaItems>
            <field>r_reeg__c.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PO EEG submitted Physician</fullName>
        <actions>
            <name>PO_EEG_submitted_physician_email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>r_reeg__c.eeg_rec_stat__c</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.Process_Used_For_Test__c</field>
            <operation>notEqual</operation>
            <value>Military Study</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.rpt_stat__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.req_stat__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>This rule notifies the physician that an EEG file has been uploaded and the PEER report submitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PO Neuro Rpt Available</fullName>
        <actions>
            <name>PO_Neuro_Report_Available</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_reeg__c.neuroguide_status__c</field>
            <operation>equals</operation>
            <value>Report Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.Process_Used_For_Test__c</field>
            <operation>notEqual</operation>
            <value>Military Study</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.neuro_stat__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Rule for determining if the neuro review report is complete and the test is not a WR test</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Parc Error</fullName>
        <actions>
            <name>ParcErrorEmail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>r_reeg__c.Translate_Status__c</field>
            <operation>equals</operation>
            <value>Parc Error</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Pre-Washout EEG submitted</fullName>
        <actions>
            <name>Pre_Washout_EEG_submitted_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>r_reeg__c.reeg_type__c</field>
            <operation>equals</operation>
            <value>pre-Washout</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule notifies MYnd personnel that a Pre-Washout EEG has been submitted.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WR Complete Group</fullName>
        <actions>
            <name>WR_Complete_Group</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>r_reeg__c.neuroguide_status__c</field>
            <operation>equals</operation>
            <value>Report Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.WR_Online_Report_URL__c</field>
            <operation>startsWith</operation>
            <value>http</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_patient__c.wr_control_group_type__c</field>
            <operation>equals</operation>
            <value>Completed Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.Process_Used_For_Test__c</field>
            <operation>equals</operation>
            <value>Military Study</value>
        </criteriaItems>
        <description>Rule for determining if a NeuroGuide report is complete, the download URL has been set and the WR group type is the Complete group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WR EEG file submitted</fullName>
        <actions>
            <name>WREEGsubmittedEmail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>r_reeg__c.eeg_rec_stat__c</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.Process_Used_For_Test__c</field>
            <operation>equals</operation>
            <value>Military Study</value>
        </criteriaItems>
        <description>This rule notifies CNSR personnel of a new EEG that has been submitted by a WR EEG tech.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WR PEER Complete</fullName>
        <actions>
            <name>WR_Test_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_reeg__c.neuroguide_status__c</field>
            <operation>equals</operation>
            <value>Report Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.WR_Online_Report_URL__c</field>
            <operation>startsWith</operation>
            <value>https://</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_patient__c.CA_Control_Group_Type__c</field>
            <operation>equals</operation>
            <value>Treatment Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.Process_Used_For_Test__c</field>
            <operation>equals</operation>
            <value>Military Study</value>
        </criteriaItems>
        <description>Rule for determining if a NeuroGuide report is complete, the download URL has been set and the WR group type is the Treatment group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>WR PEER Complete Resend</fullName>
        <actions>
            <name>WR_Test_Complete</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_reeg__c.neuroguide_status__c</field>
            <operation>equals</operation>
            <value>Report Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.WR_Online_Report_URL__c</field>
            <operation>startsWith</operation>
            <value>https://</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_patient__c.wr_control_group_type__c</field>
            <operation>equals</operation>
            <value>Treatment Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_patient__c.Resend_Treatment_Group_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Rule for determining if a NeuroGuide report is complete, the download URL has been set and the WR group type is the Treatment group. This is used to resend an email.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>WR test uploaded</fullName>
        <active>false</active>
        <criteriaItems>
            <field>r_reeg__c.eeg_rec_stat__c</field>
            <operation>equals</operation>
            <value>Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>A new EEG recording has been uploaded from WR/Ft. Belvoir.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>rEEG Review</fullName>
        <actions>
            <name>rEEGReviewEmail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_reeg__c.rpt_stat__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.req_stat__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_reeg__c.test_data_flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule will send an email to CNSR Production Managers when an rEEG is ready for review.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
