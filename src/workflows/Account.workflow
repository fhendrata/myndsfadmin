<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Account_Record_Type_To_Provider</fullName>
        <field>RecordTypeId</field>
        <lookupValue>ProviderAccount</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Account Record Type to Provider</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_EEG_Testing_Account</fullName>
        <field>RecordTypeId</field>
        <lookupValue>EEGTestingAccount</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to EEG Testing Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_Government</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Government_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to Government</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_to_media</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Media_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type to media</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_record_Type_to_Generic</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Generic_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set record Type to Generic</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Investor</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Investor_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Investor</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ACCOUNT%3A Set Record Type to EEG Provider</fullName>
        <actions>
            <name>Set_Record_Type_to_EEG_Testing_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Record_Type_Hidden__c</field>
            <operation>equals</operation>
            <value>EEG Provider</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACCOUNT%3A Set Record Type to Generic</fullName>
        <actions>
            <name>Set_record_Type_to_Generic</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Record_Type_Hidden__c</field>
            <operation>equals</operation>
            <value>Generic</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACCOUNT%3A Set Record Type to Government</fullName>
        <actions>
            <name>Set_Record_Type_to_Government</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Record_Type_Hidden__c</field>
            <operation>equals</operation>
            <value>Government</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACCOUNT%3A Set Record Type to Investor</fullName>
        <actions>
            <name>Update_Record_Type_to_Investor</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Record_Type_Hidden__c</field>
            <operation>equals</operation>
            <value>Investor</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACCOUNT%3A Set Record Type to Media</fullName>
        <actions>
            <name>Set_Record_Type_to_media</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Record_Type_Hidden__c</field>
            <operation>equals</operation>
            <value>Media</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACCOUNT%3A Set Record Type to Provider</fullName>
        <actions>
            <name>Set_Account_Record_Type_To_Provider</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Record_Type_Hidden__c</field>
            <operation>equals</operation>
            <value>Provider Lead</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
