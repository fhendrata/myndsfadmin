<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>New Marketing Request Notification</fullName>
        <actions>
            <name>ProcessNewMarketingRequest</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Marketing_Request__c.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>New Marketing Request Notification</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>ProcessNewMarketingRequest</fullName>
        <assignedTo>kpolich@myndanalytics.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Process New Marketing Request</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Marketing_Request__c.CreatedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Process New Marketing Request</subject>
    </tasks>
</Workflow>
