<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Patient_Reported_Outcome</fullName>
        <description>Patient Reported Outcome</description>
        <protected>false</protected>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>bmacdonald@cnsresponse.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRWorkflowEmailTemplates/Patient_Reported_Outcome</template>
    </alerts>
    <rules>
        <fullName>Patient Reported Outcome</fullName>
        <actions>
            <name>Patient_Reported_Outcome</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_patient_outc__c.insert_method__c</field>
            <operation>equals</operation>
            <value>PRO</value>
        </criteriaItems>
        <description>fires when a patient reported outcome has been posted to the outcomes database</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
