<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Outcome_Med_Misspelled_Alert</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>Email Alert for when an Outcome Medication is misspelled or not in the database</description>
        <protected>false</protected>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mmetzig@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/Outcome_Med_Misspelled_Template</template>
    </alerts>
    <rules>
        <fullName>Outcome Med Misspelled</fullName>
        <actions>
            <name>Outcome_Med_Misspelled_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_outcome_med__c.is_misspelled__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow rule will send an email to Brian if any Outcome Medications are misspelled or are not in the database.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
