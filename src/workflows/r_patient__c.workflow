<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Control_group</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>Randomized to Control group</description>
        <protected>false</protected>
        <recipients>
            <field>physician__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>customerPortalUser</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/CA_Patient_Randomized_to_Control_Group_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>General_Study_Subject_Randomized_Control_Email_Alert</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>General Study - Subject Randomized Control Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>physician__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Account_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mmetzig@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/General_Study_Subject_Randomized_Control_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>General_Study_Subject_Randomized_Treatment_Email_Alert</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>General Study - Subject Randomized Treatment Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>physician__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <field>Account_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email_2__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email_3__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Study_Coordinator_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mmetzig@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/General_Study_Subject_Randomized_Treatment_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Patient_randomized_to_Control_Group</fullName>
        <ccEmails>dclarke@cnsresponse.com</ccEmails>
        <description>Patient randomized to Control Group - depr</description>
        <protected>false</protected>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>bmacdonald@cnsresponse.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRWorkflowEmailTemplates/Patient_Randomized_to_Control</template>
    </alerts>
    <alerts>
        <fullName>WR_Control_Group_Email_Resend</fullName>
        <description>WR Control Group Email Resend</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>bmacdonald@cnsresponse.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRWorkflowEmailTemplates/Patient_Randomized_to_Control</template>
    </alerts>
    <alerts>
        <fullName>WR_Treatment_Group_EEG_Upload_Email</fullName>
        <ccEmails>jdepetro@ethos.com</ccEmails>
        <ccEmails>mcorbin@ethos.com</ccEmails>
        <description>WR Treatment Group EEG Upload Email</description>
        <protected>false</protected>
        <recipients>
            <type>customerPortalUser</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>bmacdonald@cnsresponse.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kpolich@myndanalytics.com</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRProduction/CA_Patient_Randomized_to_Treatment_Group_Email_Template</template>
    </alerts>
    <rules>
        <fullName>Complete group e-mail</fullName>
        <active>true</active>
        <criteriaItems>
            <field>r_patient__c.wr_control_group_type__c</field>
            <operation>equals</operation>
            <value>Completed Group</value>
        </criteriaItems>
        <description>e-mail to CRC&apos;s and referring physician that patient has been randomized to the Complete group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Control group resend e-mail</fullName>
        <actions>
            <name>WR_Control_Group_Email_Resend</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_patient__c.wr_control_group_type__c</field>
            <operation>equals</operation>
            <value>Control Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>r_patient__c.Resend_Control_Group_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Resend of the e-mail to CRC&apos;s and referring physician that patient has been randomized to the control group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>General Study - Subject Randomized Control</fullName>
        <actions>
            <name>General_Study_Subject_Randomized_Control_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_patient__c.CA_Control_Group_Type__c</field>
            <operation>equals</operation>
            <value>Control Group</value>
        </criteriaItems>
        <description>This workflow rule will send an email to the owner of the PEER Requisition record (physician) when their Patient is randomized to the control group.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>General Study - Subject Randomized Treatment</fullName>
        <actions>
            <name>General_Study_Subject_Randomized_Treatment_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>r_patient__c.CA_Control_Group_Type__c</field>
            <operation>equals</operation>
            <value>Treatment Group</value>
        </criteriaItems>
        <description>This workflow rule will send an email to the owner of the PEER Requisition record (physician) when their Patient is randomized to the treatment group.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Internal Control group e-mail</fullName>
        <actions>
            <name>Patient_randomized_to_Control_Group</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>r_patient__c.wr_control_group_type__c</field>
            <operation>equals</operation>
            <value>Control Group</value>
        </criteriaItems>
        <description>e-mail to CRC&apos;s and referring physician that patient has been randomized to the control group</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Randomized to Control group e-mail</fullName>
        <actions>
            <name>Control_group</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>e-mail to CRC&apos;s and referring physician that patient has been randomized to the control group</description>
        <formula>OR(ISPICKVAL(wr_control_group_type__c, &quot;Control Group&quot;), AND(ISPICKVAL(CA_Control_Group_Type__c, &quot;Control Group&quot;), physician__r.Account.Name = &quot;PEER Study - Canada&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Randomized to Treatment group e-mail</fullName>
        <actions>
            <name>WR_Treatment_Group_EEG_Upload_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An email is sent to Artifactors when the patient WR Patient Type is changed to &quot;Treatment Group.&quot;</description>
        <formula>OR(ISPICKVAL(wr_control_group_type__c, &quot;Treatment Group&quot;), AND(ISPICKVAL(CA_Control_Group_Type__c, &quot;Treatment Group&quot;), physician__r.Account.Name = &quot;PEER Study - Canada&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>test</fullName>
        <active>false</active>
        <criteriaItems>
            <field>r_patient__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
