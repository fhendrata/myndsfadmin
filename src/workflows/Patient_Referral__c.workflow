<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_Patient_Referral_Email_Alert</fullName>
        <description>New Patient Referral Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Referred_To__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRWorkflowEmailTemplates/New_Patient_Referral_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Patient_Referral_Accepted_Email_Alert</fullName>
        <description>Patient Referral Accepted Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Referred_To__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRWorkflowEmailTemplates/Patient_Referral_Accepted</template>
    </alerts>
    <alerts>
        <fullName>Patient_Referral_Missed_Email_Alert</fullName>
        <description>Patient Referral Missed Email Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Referred_To__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>peerreport@myndanalytics.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CNSRWorkflowEmailTemplates/Patient_Referral_Missed</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Referral_current_Status</fullName>
        <field>Referral_Current_Status__c</field>
        <literalValue>PEER Report Ordered</literalValue>
        <name>Update Referral current Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New Patient Referral</fullName>
        <actions>
            <name>New_Patient_Referral_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email notification for a new patient referral</description>
        <formula>Referred_To__c != null</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Patient Referral Accepted</fullName>
        <actions>
            <name>Patient_Referral_Accepted_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patient_Referral__c.Physician_Referral_Status__c</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <description>Sends an email to the physician that has accepted a patient referral</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Patient Referral Missed</fullName>
        <actions>
            <name>Patient_Referral_Missed_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patient_Referral__c.Physician_Referral_Status__c</field>
            <operation>equals</operation>
            <value>Missed</value>
        </criteriaItems>
        <description>Workflow rule for patient referrals that have been missed by the physician</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Referral Current Status</fullName>
        <actions>
            <name>Update_Referral_current_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patient_Referral__c.PEER_Ordered__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
