<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Activity_Type_Pardot_Drip_Emai</fullName>
        <field>Activity_Type__c</field>
        <literalValue>Pardot Drip Email</literalValue>
        <name>Update Activity Type - Pardot Drip Emai</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Activity_Type_to_Call</fullName>
        <field>Activity_Type__c</field>
        <literalValue>Call</literalValue>
        <name>Update Activity Type to Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Activity_Type_to_Conversation</fullName>
        <field>Activity_Type__c</field>
        <literalValue>Conversation</literalValue>
        <name>Update Activity Type to Conversation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Activity_Type_to_Email</fullName>
        <field>Activity_Type__c</field>
        <literalValue>Email</literalValue>
        <name>Update Activity Type to Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Activity_Type_to_Mail</fullName>
        <field>Activity_Type__c</field>
        <literalValue>Mail</literalValue>
        <name>Update Activity Type to Mail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_First_Call</fullName>
        <field>First_Call_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update First Call</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>TASK%3A Update Activity Type to Call</fullName>
        <actions>
            <name>Update_Activity_Type_to_Call</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Call</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TASK%3A Update Activity Type to Conversation</fullName>
        <actions>
            <name>Update_Activity_Type_to_Conversation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Conversation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TASK%3A Update Activity Type to Email</fullName>
        <actions>
            <name>Update_Activity_Type_to_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Email,Mass Email,Pardot Drip Email</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TASK%3A Update Activity Type to Mail</fullName>
        <actions>
            <name>Update_Activity_Type_to_Mail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Mail</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TASK%3A Update Activity Type to Pardot Drip Email</fullName>
        <actions>
            <name>Update_Activity_Type_Pardot_Drip_Emai</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Subject</field>
            <operation>startsWith</operation>
            <value>Pardot Drip</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
