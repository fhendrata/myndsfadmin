public class LeadConvController
{
    public Lead leadObj {get; set;}
    public Patient_Referral__c patRef {get; set;}
    
    public LeadConvController()
    {
    	
    }

    //---Constructor from the standard controller
    public LeadConvController( ApexPages.StandardController stdController)
    {    
        leadObj = (Lead)stdController.getRecord();
    }
    
    private void loadLeadObj()
    {
    	leadObj = [select Id, Name, LastName, FirstName, Street, City, State, Country, PostalCode, Email, Phone, Comments__c from Lead where Id = :leadObj.Id];
    }
    
    private void loadPatRef()
    {
    	patRef = new Patient_Referral__c();
    	if (leadObj != null)
    	{
	    	patRef.Lead__c = leadObj.Id;
	    	patRef.Name = leadObj.Name;
	    	patRef.Caller_First_Name__c = leadObj.FirstName;
	    	patRef.Address_1__c = leadObj.Street;
	    	patRef.City__c = leadObj.City;
	    	patRef.State__c = leadObj.State;
	    	patRef.Country__c = leadObj.Country;
	    	patRef.Postal_Code__c = leadObj.PostalCode;
	    	patRef.Email_Address__c = leadObj.Email;
	    	patRef.Best_Phone_Number__c = leadObj.Phone;
	  		patRef.Home_Phone_Number__c = leadObj.Phone;
	  		patRef.Call_Comments__c = leadObj.Comments__c;
    	}
/*
		patRef.Patient_First_Name__c = leadObj.FirstName;
		patRef.Patient_Last_Name__c = leadObj.LastName;
		patRef.rEEG_Ordered__c = '';
    	patRef.Best_Phone_Number_Type__c = '';
    	patRef.Call_Type__c = '';
    	patRef.Caller_First_Name__c = '';
    	patRef.Caller_Interest__c = '';
    	patRef.Caller_Interest_Other__c= '';
    	patRef.Calling_for__c = '';
    	patRef.Date_Time_Called__c = '';
    	patRef.Has_Insurance__c = '';
    	patRef.Insurance_Carrier__c = '';
    	patRef.Other_Country__c = '';
    	patRef.Referral_Current_Status__c = leadObj.;
    	patRef.Referral_Source_Other__c = '';
    	patRef.Referral_Source_Type_Other__c = '';
    	patRef.Referral_Status__c = '';
    	patRef.Referred_To__c = '';
*/    	
    }
    
    private string getReferralId(string contactId)
    {
    	string returnVal;
    	
    	for (Contact c : [select Id, Name, physician_portal_user__c from Contact where Id = :contactId Limit 1]) {
      		returnVal = c.physician_portal_user__c; 
    	}
		
    	return returnVal;
    }

    public PageReference ConvertLead()
    {
    	PageReference pr = null;
    	
    	if (patRef != null)
    	{
    		if (!e_StringUtil.isNullOrEmpty(patRef.Referred_To__c))
    		{
				patRef.OwnerId = getReferralId(patRef.Referred_To__c);	
    		}
    		
    		try
    		{
	    		if (e_StringUtil.isNullOrEmpty(patRef.Id))
	    			insert patRef;
	    		else
	    			update patRef;
	    		
	    		pr = new PageReference('/' + patRef.Id);
    		}
    		catch (Exception ex)
    		{
    		}	
    	}
    
        return pr;
    }
    
    public PageReference loadAction()
    {
    	loadLeadObj();
    	
    	if (leadObj != null) loadPatRef();
    	
        return null;
    }

    public PageReference cancelEdit()
    {
        return new PageReference('/' + leadObj.Id);
    }

    //-----------------TEST-----------------------------------
    public static testMethod void testController()
    {
    	Lead lead = new Lead();
    	lead.LastName = 'last';
    	lead.Company = 'Company';
    	lead.Status = 'Open';
    	insert lead;
    	
        LeadConvController cont = new LeadConvController();
        cont.patRef = new Patient_Referral__c();
        cont.leadObj = lead;
		cont.loadAction();
		cont.ConvertLead();
		cont.cancelEdit();

        System.assertEquals( '1', '1');
    }
 }