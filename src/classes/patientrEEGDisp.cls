public class patientrEEGDisp
{
    private r_reeg__c obj;
    public r_reeg__c getObj()
    {
        return obj;
    }
    public void setObj(r_reeg__c s)
    {
        obj = s;
    }
                    
    public void Setup(r_reeg__c o)
    {
        obj = o;
    }

    public static testMethod void testDisp()
    {
        r_reeg__c obj = new r_reeg__c();

        patientrEEGDisp disp = new patientrEEGDisp();
        disp.setObj(obj);
        disp.Setup( obj);

        r_reeg__c obj2 = disp.getObj();

        System.assertEquals( obj, obj2);
    }
}