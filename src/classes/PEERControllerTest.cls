//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: PEERControllerTest
// PURPOSE: Test class for PEER View page controller
// CREATED: 05/2/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class PEERControllerTest {
	
	@isTest static void testPEER() 
	{
		r_reeg__c reeg = rEEGDao.getTestReeg();
		ApexPages.StandardController sc = new ApexPages.standardController(reeg);
		PEERController pc = new PEERController(sc);

		pc.loadAction();

		PageReference pr = pc.adminPage();
		pr = pc.dxListEditPage();

		String testStr = pc.getStatusLightReportSrcNG();

		reeg.neuroguide_status__c = 'Report Complete';
		rEEGDao rDao = new rEEGDao();
		rDao.Save(reeg, 'UPDATE_DB_STATUS');

		testStr = pc.getShowOnlineReportLink();
		//System.assertEquals('visible', testStr);

		testStr = pc.getStatusLightReportSrcNG();
		//System.assertEquals('greenStatus', testStr);
		
		reeg.neuroguide_status__c = 'NEW';
		rDao.Save(reeg, 'UPDATE_DB_STATUS');

		testStr = pc.getShowOnlineReportLink();
		System.assertEquals('hidden', testStr);

		testStr = pc.getStatusLightReportSrcNG();
		System.assertEquals('yellowStatus', testStr);

/*
		testStr = pc.getDownloadEEGHref();
	    testStr = pc.getDownloadArtHref();
	    testStr = pc.getDownloadNeuroHref();
*/
		testStr = pc.getStatusLightEEGSrc();
		testStr = pc.getStatusLightNeuroSrc();
		testStr = pc.getStatusLightArtSrc();
 
 		testStr = pc.getShowNeuroLink();
 		testStr = pc.getShowEEGLink();
 		testStr = pc.getShowArtLink();
 		testStr = pc.getReportUrl();
    
		pr = pc.dxListEditPage();

		List<r_reeg_dx__c> dxList = pc.getrEEGDxs();

		List<rEEGTestDisp> testDispList =  pc.getTests();

    	testStr = pc.getEegtechHref();
    	testStr = pc.getPhysicianHref();
    	testStr = pc.getPatientHref();
    	pr = pc.CancelReegPatReq();

	}

	@isTest static void testPEERPO2() 
	{
		r_reeg__c reegPO2 = rEEGDao.getTestReegPO2();
		ApexPages.StandardController sc = new ApexPages.standardController(reegPO2);
		PEERController pc = new PEERController(sc);

		pc.loadAction();
	}
	
	
}