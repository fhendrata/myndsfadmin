public with sharing class PEERBrainMapViewController extends BaseController
{
	public String PEERId { get; set; } 

	public PEERBrainMapViewController() 
	{
		PEERId = ApexPages.currentPage().getParameters().get('rId');
	}
}