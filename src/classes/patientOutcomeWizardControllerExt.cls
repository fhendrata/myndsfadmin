public class patientOutcomeWizardControllerExt extends BaseController
{
    private r_patient__c patient;
    private r_patient_outc__c outcome;
    private List<r_outcome_med__c> previousMeds;
    private String reegId;
    private String patId;
    private String qsId;
    private r_reeg__c reeg;
    private String newPat;
      
    public patientOutcomeWizardControllerExt()
    {
        //---this is only used for the apex test case
        // rEEGDao reegDao = new rEEGDao();
        // reeg = new r_reeg__c();
        reeg = rEEGDao.getTestReeg(); 
        // reeg.reeg_type__c = 'Type I';
        // reeg.r_patient_id__c = 'a0b60000000I4DD';
        outcome = new r_patient_outc__c();
        outcome.cgi__c = '4 - Baseline';
        outcome.cgs__c = '0 - Not assessed';
    }

    //---Constructor from the standard controller
    public patientOutcomeWizardControllerExt( ApexPages.StandardController stdController)
    {  
       	init();
        this.outcome = (r_patient_outc__c)stdController.getRecord();
        
        if (outcome.patient__c == null)
            outcome.patient__c = ApexPages.currentPage().getParameters().get('pid');  
        
    }
    
    public void init()
    {
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.qsId = ApexPages.currentPage().getParameters().get('qsid');
        this.newPat = ApexPages.currentPage().getParameters().get('np');

        if (reegId != null) getReeg();
        
        if (previousMeds == null)
        {
        	patientOutcomeDao outcDao = new patientOutcomeDao();
        	this.previousMeds = outcDao.getExistingOutcomeMeds(ApexPages.currentPage().getParameters().get('pid'));
        }
        
        if (!e_StringUtil.isNullOrEmpty(outcome.cgi__c))         
			outcome.cgi__c = '4 - Baseline';
    }

    private void getReeg()
    {
        if (reeg == null)
        {
            if (reegId != null)
            {
                reeg = [select id, r_patient_id__c, reeg_type__c from r_reeg__c where id = :reegId];
            }
        }
    }

    public Boolean getCGIRequired()
    {
        return (getIsReegType2() && qsid == 'null');
    }

    public Boolean getCGSRequired()
    {
        return (getIsReegType1() && qsid == 'null');
    }

    public Boolean getShowCGI()
    {
        return (getIsReegType2() || getIsMgr());
    }
     
    private Boolean getIsReegType1()
    {
        return (reeg != null && reeg.reeg_type__c == 'Type I');
    }

    private Boolean getIsReegType2()
    {
        return (reeg != null && reeg.reeg_type__c == 'Type II');
    }

    public PageReference reegStep3()
    {
        PageReference returnVal = new PageReference( '/apex/rEEGWizard3Page?id=' + reegId + '&pid=' + ApexPages.currentPage().getParameters().get('pid') + '&qsid=' + ApexPages.currentPage().getParameters().get('qsid') + '&np=' + newPat);
        returnVal.setRedirect(true);
        return returnVal;
    }

    public PageReference reegStep4()
    {
    	outcome.outcome_date__c = Date.today();
    	outcome.rEEG__c = reegId;

        if (outcome.id != null) upsert outcome;
        else
        {
            if (ApexPages.currentPage().getParameters().get('pid') != null)
            {
                outcome.patient__c = ApexPages.currentPage().getParameters().get('pid');
                insert outcome;
            }
        }

		// set outcome meds = to previous outcome meds
		if (previousMeds != null) 
		{
			for(r_outcome_med__c oMedRow : previousMeds) 
			{
					// System.debug('## outcome.id = ' + outcome.id);
					r_outcome_med__c newOutcMed = new r_outcome_med__c();
					newOutcMed.r_outcome_id__c = outcome.id;
					newOutcMed.med_name__c = oMedRow.med_name__c;
					newOutcMed.dosage__c = oMedRow.dosage__c;
					newOutcMed.unit__c = oMedRow.unit__c;
					newOutcMed.frequency__c = oMedRow.frequency__c;
					newOutcMed.start_date__c = oMedRow.start_date__c;
					newOutcMed.start_date_is_estimated__c = oMedRow.start_date_is_estimated__c;
					insert newOutcMed;
			}
		}
  
        // build intervals
		GlobalPatientDao patDao = new GlobalPatientDao();
		patient = patDao.getById(outcome.patient__c);
        
        if (patient != null)
        {
	    	patientIntervalBuilder intBuilder = new patientIntervalBuilder();
	  		intBuilder.buildAllIntervalsForPat(patient.Id, patient.OwnerId);
        }
	  	// intBuilder.buildAllIntervalsForPat(pat.Id, pat.OwnerId); outcome.patient__c
		
		// next page
        PageReference returnVal = new PageReference( '/apex/rEEGWizard4Page?id=' + reegId + '&pid=' + ApexPages.currentPage().getParameters().get('pid')
                                                         + '&oid=' + outcome.id + '&qsid=' + ApexPages.currentPage().getParameters().get('qsid') 
                                                            + '&np=' + ApexPages.currentPage().getParameters().get('np'));
        returnVal.setRedirect(true);
        return returnVal;
    }

    public PageReference cancelWizard()
    {
        if (reeg == null) reeg = [select id, r_patient_id__c from r_reeg__c where id = :ApexPages.currentPage().getParameters().get('rid')];
        String patientId;
        if (reeg.r_patient_id__c != null) patientId = reeg.r_patient_id__c;
        else if (ApexPages.currentPage().getParameters().get('pid') != null) patientId = ApexPages.currentPage().getParameters().get('pid');

        if (patientId != null)    
        {
            r_patient__c patient = [select Id from r_patient__c where id = :patientId];
            if (ApexPages.currentPage().getParameters().get('np') == '1')
                delete patient;
            else
                RemoveReeg();
        }
        return new PageReference('/apex/patientSearchPage');
    }

         //-- delete reeg and all related objects
    public void RemoveReeg()
    {
        if (reeg == null) reeg = [select id, r_patient_id__c from r_reeg__c where id = :ApexPages.currentPage().getParameters().get('id')];

        for(r_reeg_wmed__c wmedRow : [select id from r_reeg_wmed__c where r_reeg_id__c = :reeg.id])
        {
            delete wmedRow;
        }

        for(r_reeg_group__c medGrpRow : [select id from r_reeg_group__c where r_reeg_id__c = :reeg.id])
        {
            delete medGrpRow;
        }

        for(r_reeg_med__c medSensRow : [select id from r_reeg_med__c where r_reeg_id__c = :reeg.id])
        {
            delete medSensRow;
        }

        for(r_reeg_dx__c dxRow : [select id from r_reeg_dx__c where r_reeg_id__c = :reeg.id])
        {
            delete dxRow;
        }

        for(r_reeg_var__c cnsVarRow : [select id from r_reeg_var__c where r_reeg_id__c = :reeg.id])
        {
            delete cnsVarRow;
        }

        delete reeg;

    }

     //---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
        patientOutcomeWizardControllerExt cont = new patientOutcomeWizardControllerExt();
        cont.IsTestCase = true;
        r_reeg__c reeg = rEEGDao.getTestReeg();
        ApexPages.currentPage().getParameters().put('id', reeg.Id);
        
        cont.init();
      
        cont.getReeg();
        PageReference pRef2 = cont.reegStep3();
        PageReference pRef3 = cont.reegStep4();
		// PageReference pRef4 = cont.cancelWizard();
        
        cont.getReeg();
        Boolean reegtype = cont.getCGIRequired();
        reegtype = cont.getShowCGI();
        reegtype = cont.getCGSRequired();
        
        
        
        cont.cancelWizard();

        cont.outcome = new r_patient_outc__c();

        System.assertEquals( 1, 1);
    }
 }