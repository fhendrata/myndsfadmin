/**********************************************************************************

 PURPOSE: A geocode utility prototype for Salesforce Contacts, it is used in conjunction
          with Physician Locator VF page which is displayed on the cnsresponse.com
          corporate site. This is a modified version of the GeocodeUtil class from
          the Ethos Geocoding Utility Beta Package. It should eventually be merged in
          so we can support both Contacts and Leads with our Package.

 CLASS: GeocodeUtil
 CREATED: 06/20/2012 Ethos Solutions - www.ethos.com
 AUTHOR: Jason M. Swenski

 Copyright© belongs to Ethos Solutions
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification,
 are not permitted without permission from Ethos Solutions, LLC.

***********************************************************************************/

public with sharing class GeocodeUtil {

    public String zip {get; set;}
    private String city {get; set;}
    private String state {get; set;}

    private Decimal latitude {get; set;}
    private Decimal longitude {get; set;}

    private String country {get; set;}
    public Contact oContact {get; set;}
    public String jsonData {get; set;}
    public String skip {get; set;}
    private static String objKey;

    /**
   * Constructor which takes the Apex Standard Controller for the Salesforce
   * standard Lead object as the paramter
     *
   * @param  sc  the reference to the Standard Controller
   */
    public GeocodeUtil(ApexPages.StandardController sc)
    {
       //get handle to the Account
       oContact = (Contact)sc.getRecord();
       //set the lead key prefix -- key should never really change but this
       //was a good suggestion and will help if we extend to other objects.
       objKey = Schema.SObjectType.Contact.getKeyPrefix();
    }


    /**
     * Constructor for the ClientRequest page. This Constructor assumes
     * at least one of the following page parameter exist, 'id' or 'newid'
     */
    public GeocodeUtil()
    {
       //get handle to the Account
       try
       {

           String id = ApexPages.currentPage().getParameters().get('id');
           String warn = ApexPages.currentPage().getParameters().get('warn');


           if(id == null || id == '') id = ApexPages.currentPage().getParameters().get('newid');
           oContact = [select Id,MailingPostalCode,MailingCity,
                              MailingState,MailingCountry,Latitude__c,Longitude__c
                              from Contact where Id =: id];

           if(!defined(oContact.MailingPostalCode))
           {
           	    ApexPages.Message mesg = new ApexPages.Message(ApexPages.Severity.ERROR, 'This physician has no postal code. Please add a postal code for this physician.');
                ApexPages.addMessage(mesg);
           }

           if(warn != null && warn == '1')
           {
               ApexPages.Message mesg = new ApexPages.Message(ApexPages.Severity.WARNING, 'This physician has not ordered a test in the last year');
               ApexPages.addMessage(mesg);
           }
           //skip = skipGeocode();
           zip = oContact.MailingPostalCode;
       }
       catch(Exception e){ system.debug(e); }
       objKey = Schema.SObjectType.Contact.getKeyPrefix();

    }

    /**
     *  Redirects to the Lead page after we parse the result
     *  from Google. This method assumes that the JSON data has been
     *  passed back from Google.
     **/
    public PageReference redirect()
    {
    	try
    	{
            ParseResultJSON(jsonData);
    	}
    	catch(Exception e)
    	{
    		return gotoRecord();
    	}
        return GeocodeRecord();
    }

    /**
     *  Redirects to the Lead page
     **/
    public PageReference gotoRecord()
    {
        PageReference pg = new PageReference('/' + oContact.Id);
        pg.setRedirect(true);
        return pg;
    }

    /**
     *  Parses the JSON result returned to the Client
     *  from Google
     *
     *  @param  data  a JSON response from Google Maps API
    **/
    private void ParseResultJSON(String jsonData)
    {

    	system.debug(jsonData);

        JSONParser parser = JSON.createParser(jsonData);


        String val = '';
        while (parser.nextToken() != null) {

            if(parser.getText() == 'long_name')
            {
                parser.nextToken();
                val = parser.getText();
            }

            if(parser.getText() == 'location')
            {
                parser.nextToken();
            	parser.nextValue();
            	latitude =  Decimal.valueOf(parser.getText());
            	parser.nextValue();
            	longitude = Decimal.valueOf(parser.getText());
            }

            if (parser.getCurrentToken() == JSONToken.START_ARRAY)
            {
                parser.nextToken();
                String key = parser.getText();
                if(key == 'administrative_area_level_1') state = val;
                if(key == 'locality') city = val;
                if(key == 'country') country = val;
            }

        }
    }

    /**
     *  method exposed for edit button override
    **/
    public PageReference editOverride()
    {
		//the URl of any VF page could be volatile between Dev/orgs with package installed
		//so instead of hardcoding the url let's let SF decide what the URL for
		//the page should be
		String retUrl = Page.ClientRequest.getUrl();
		//nooverride set to 1 to prevent infinite loops
		PageReference pg = new PageReference('/' + oContact.Id + '/e?nooverride=1&retURL=' + retUrl + '?id=' + oContact.Id);
		pg.setRedirect(true);
		return pg;
    }

    public PageReference gotoConfirm()
    {
        String requestUrl = Page.ConfirmShowLocation.getUrl();
        String warn = '0';
        r_reeg__c lastTest = null;
        try
        {
        	lastTest = [select CreatedDate from r_reeg__c where r_physician_id__c =: oContact.Id and CreatedDate > LAST_N_YEARS:1 limit 1 ];
        }
        catch(Exception e){}

        if(lastTest == null) warn = '1';

        PageReference pg = new PageReference(requestUrl + '?id=' + oContact.Id + '&warn='+warn);
        pg.setRedirect(true);
        return pg;
    }

    public PageReference confirm()
    {
    	update oContact;
    	return forceGeocode();
    }

    public PageReference forceGeocode()
    {
    	String requestUrl = Page.ClientRequest.getUrl();
        //nooverride set to 1 to prevent infinite loops
        PageReference pg = new PageReference(requestUrl + '?id=' + oContact.Id);
        pg.setRedirect(true);
        return pg;
    }


    /**
    *  method exposed for new button override
    **/
    public PageReference newOverride()
    {
        String retUrl = Page.ClientRequest.getUrl();
        PageReference pg = new PageReference('/'+objKey+'/e?nooverride=1&retURL=%2F'+objKey+'%2Fo&saveURL=' + retUrl);
        pg.setRedirect(true);
        return pg;
    }

    /**
    *  checks to see if should actually make an API call
    **/
    public String skipGeocode()
    {
        /**if(defined(oContact.MailingCity) &&
           defined(oContact.MailingState) &&
           defined(oContact.MailingCountry) &&
           defined(oContact.Latitude__c) &&
           defined(oContact.Longitude__c)) return 'true';
        **/
        //if(!defined(oContact.MailingPostalCode)) return 'true';
        return 'false';
    }

    /**
     *  Using the data returned from Google, update the Lead
    **/
    public PageReference GeocodeRecord()
    {

        try
        {
            oContact = [select Id,MailingPostalCode,MailingCity,MailingState,MailingCountry from Contact where Id =: oContact.Id];

            oContact.MailingCity = city;
            oContact.MailingState = state;
            oContact.MailingCountry = country;
            oContact.Latitude__c = latitude;
            oContact.Longitude__c = longitude;
            update oContact;
        }
        catch(Exception e){ system.debug(e); }
        return gotoRecord();

    }

    private static Boolean defined(Decimal d)
    {
    	return d != null;
    }

    //helper method
    private static Boolean defined(String s)
    {
        return  !(s == '' || s == null);
    }

    //fake JSON response
    private static String testData = '{   "results" : [  { "address_components" : [{   "long_name" : "1600",   "short_name" : "1600",   "types" : [ "street_number" ]},{   "long_name" : "Amphitheatre Pkwy",   "short_name" : "Amphitheatre Pkwy",   "types" : [ "route" ]},{   "long_name" : "Mountain View",   "short_name" : "Mountain View",   "types" : [ "locality", "political" ]},{   "long_name" : "Santa Clara",   "short_name" : "Santa Clara",   "types" : [ "administrative_area_level_2", "political" ]},{   "long_name" : "California",   "short_name" : "CA",   "types" : [ "administrative_area_level_1", "political" ]},{   "long_name" : "United States",   "short_name" : "US",   "types" : [ "country", "political" ]},{   "long_name" : "94043",   "short_name" : "94043",   "types" : [ "postal_code" ]} ], "formatted_address" : "1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA", "geometry" : {"location" : {   "lat" : 37.42291810,   "lng" : -122.08542120},"location_type" : "ROOFTOP","viewport" : {   "northeast" : {  "lat" : 37.42426708029149,  "lng" : -122.0840722197085   },   "southwest" : {  "lat" : 37.42156911970850,  "lng" : -122.0867701802915   }} }, "types" : [ "street_address" ]  }   ],   "status" : "OK"}';
    private static testMethod void testGeocoding()
    {
        Test.startTest();
        Contact testCont = new Contact();
        testCont.LastName = 'Test';
        testCont.Email = 'test@test.com';
        testCont.MailingPostalCode = '94043';
        insert testCont;

        //test the constructor from the standard controller
        //needed for override buttons
        ApexPages.StandardController sc = new ApexPages.StandardController(testCont);
        GeocodeUtil gc = new GeocodeUtil(sc);
        //actions for the VF pages that override the new/edit button
        gc.editOverride();
        gc.newOverride();

        //test the constructor for the redirect page
        ApexPages.currentPage().getParameters().put('id',testCont.Id);
        gc = new GeocodeUtil();

        //Check that we shouldn't skip because we have a ZIP
        //and not all the address data is there
        //it's a string because the value is injected into
        //the javascript
        system.assert(gc.skipGeocode() == 'false');
        //do the city/state/country lookup with a faked response
        //since you can't do HTTP requests in test code much less
        //client javascript
        gc.jsonData = testData;
        gc.redirect();

        //make sure we parsed the response correctly
        system.assert(gc.city == 'Mountain View');
        system.assert(gc.state == 'California');
        system.assert(gc.country == 'United States');
        Test.stopTest();
    }

}