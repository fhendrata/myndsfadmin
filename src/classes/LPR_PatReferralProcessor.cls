//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: LPR_PatReferralProcessor
// PURPOSE: Functionality for updating a Lead record if any of the Lead records 
//			patient referrals records are updated.
// CREATED: 06/06/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
global class LPR_PatReferralProcessor implements Database.Batchable<sObject> {
	
	global final String Query;

	global LPR_PatReferralProcessor(String q)
	{
		Query = q;
	}
   	
   	/* -- Code to insert initial scheduled process (Execute Anonymous in Eclipse) 
	String chronStr = '0 0 * * * ? *';
	String chronStr2 = '0 30 * * * ? *';
	LPR_PatReferralRunner batchRunner = new LPR_PatReferralRunner();
	System.schedule('Patient Referral update Lead - Top of the hour',chronStr, batchRunner);
	System.schedule('Patient Referral update Lead - Bottom of the hour',chronStr2, batchRunner);
	*/
	
	/*
	   -- Code to run Lead -> RIGHT NOW --
	  DateTime n = datetime.now().addSeconds(15);
	String cron = '';
	cron += n.second();
	cron += ' ' + n.minute();
	cron += ' ' + n.hour();
	cron += ' ' + n.day();
	cron += ' ' + n.month();
	cron += ' ' + '?';
	cron += ' ' + n.year();
	LPR_PatReferralRunner batchRunner = new LPR_PatReferralRunner();
	System.schedule('Patient Referral update Lead - Immediate',cron, batchRunner);
   */
	
   	global Database.QueryLocator start(Database.BatchableContext BC)
	{
    	return Database.getQueryLocator(Query);
   	}

 	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
   		//-- If any updated Patient Referrals are found, update the associated Lead record.
   		for(Sobject s : scope)	LPR_PatReferralLead.UpdateLead(s);
   	}

	global void finish(Database.BatchableContext BC) 
	{
		
	}
	
}