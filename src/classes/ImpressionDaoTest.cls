//--------------------------------------------------------------------------------
// COMPONENT: rEEG Application
// CLASS: ImpressionDaoTest
// PURPOSE: Test class for the Impression trigger
// CREATED: 07/10/13 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest (SeeAllData=true)
private class ImpressionDaoTest 
{
	/**
    * Test ImpressionDao insert & update
    */
    private static testmethod void testObjectSave() 
    {
		String defaultId = '';
		List<RecordType> newSubjectRtList = [select id from RecordType where SObjectType = 'Impression__c' and Name = 'New Subject'];

		if (newSubjectRtList != null && newSubjectRtList.size() > 0)
		{
			r_patient__c testPat = patientDao.getTestPat();
	    	Impression__c impress = new Impression__c();
	    	impress.Last_name__c = 'Test';
	    	impress.RecordTypeId = newSubjectRtList[0].Id;
	    	impress.Patient_id__c = testPat.Id;
	    	impress.acv_not_to_use__c = 'test';
	    	impress.acv_to_use__c = 'test';
	    	impress.augmentation__c = 'test';
	    	impress.caution__c = 'test';
	    	impress.class__c = 'test';
	    	impress.classes_not_to_use__c = 'test';
	    	impress.classes_to_use__c = 'tesst1';
	    	impress.comfort_level__c = 'ok';
	    	impress.completed__c = true;
	    	impress.data_labs_name__c = 'test';
	    	impress.First_name__c = 'first';
	    	impress.Last_name__c = 'last';
	    	impress.miscellaneous_comments__c = 'test';
	    	impress.monitoring_complete__c = true;
	    	impress.other_options__c = 'none';
	    	impress.PEER_report_link__c = 'http://www.ethos.com';
	    	impress.randomization_id__c = 'test';
	    	impress.dl_summary_report__c = 'http://www.ethos.com';
	    	
	    	insert impress;
	    	
	    	impress.comfort_level__c = 'not ok';
	    	update impress;
		}
    	
    }
}