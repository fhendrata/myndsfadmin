//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: phy_PeerUnifiedRequisitionControllerTest
// PURPOSE: Test class for the phy_PeerUnifiedRequisitionController class methods
// CREATED: 01/08/15 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class phy_PeerUnifiedRequisitionControllerTest 
{
	
	private static testmethod void testInitialLoadMethods() 
	{
		PEER_Requisition__c req = PEERRequisitionDao.getTestObj();
		
		Test.startTest();

    	phy_PeerUnifiedRequisitionController reqController = new phy_PeerUnifiedRequisitionController(new ApexPages.standardController(req));       
    	ApexPages.currentPage().getParameters().put('id', req.id);

    	reqController.loadAction();
    	System.assertEquals(reqController.ReqObj.Id, req.id);
    	//System.assertEquals(reqController.PatRefId, '');
    	System.assertEquals(reqController.ActiveTabName, 'patientTab');
    	//System.assertEquals(reqController.IsFirstSearchDone, false);

    	reqController.dxTabActive();
    	reqController.woTabActive();
    	reqController.eegUploadTabActive();
    	reqController.submitTabActive();

		Test.stopTest();
	}

	private static testmethod void testPatientMethods() 
	{
		PEER_Requisition__c req = PEERRequisitionDao.getTestObj();
		ApexPages.StandardController sc = new ApexPages.standardController(req);                
    	phy_PeerUnifiedRequisitionController reqController = new phy_PeerUnifiedRequisitionController(new ApexPages.standardController(req));       
    	ApexPages.currentPage().getParameters().put('id', req.id);
		reqController.ReqObj.patient_first_name__c = '';
		reqController.ReqObj.patient_name__c = '';

		r_patient__c pat = patientDao.getTestPat();

		Test.startTest();

		System.assertEquals(reqController.PatAllList, null);
		reqController.patientSearch();
		System.assertNotEquals(reqController.PatAllList, null);
		System.assert(reqController.PatAllList.isEmpty());
		System.assertEquals(reqController.ShowPatientSearchResults, false);

		reqController.ReqObj.patient_first_name__c = pat.first_name__c;
		reqController.ReqObj.patient_name__c = pat.name;

		reqController.patientSearch();
		System.assertNotEquals(reqController.PatAllList, null);
		System.assertEquals(reqController.PatAllList.isEmpty(), false);
		System.assert(reqController.ShowPatientSearchResults);

		reqController.PatId = pat.Id;
		reqController.selectPatient();
		System.assertEquals(reqController.ReqObj.patient_dob__c, '01/01/1960');

		reqController.createNewPatient();
		reqController.newPatientSearch();

		Test.stopTest();
	}

	private static testmethod void testReegMethods() 
	{
		PEER_Requisition__c req = PEERRequisitionDao.getTestObj();
		ApexPages.StandardController sc = new ApexPages.standardController(req);                
    	phy_PeerUnifiedRequisitionController reqController = new phy_PeerUnifiedRequisitionController(new ApexPages.standardController(req));   
    	ApexPages.currentPage().getParameters().put('id', req.id);

		r_patient__c pat = patientDao.getTestPat();
		reqController.ReqObj.patient_first_name__c = pat.first_name__c;
		reqController.ReqObj.patient_name__c = pat.name;
		reqController.patientSearch();
		reqController.PatId = pat.Id;
		reqController.selectPatient();

		r_reeg__c reeg = rEEGDao.getTestReeg();
		reqController.ReqObj.reeg_reeg_type__c = reeg.reeg_type__c;
		reqController.ReqObj.reeg_eeg_rec_stat__c = 'Received';
		Contact con = ContactDao.getTestPhysician();
		reqController.ReqObj.Physician__c = con.Id;

		reqController.ReqObj = PEERRequisitionDao.getInstance().getById(reqController.ReqObj.Id);

		Test.startTest();

		Boolean result = reqController.IsTypeOne;
		result = reqController.IsEegUploaded;
		Boolean rp = reqController.ShowRapidTurnaroundOption;
		//System.assertEquals(rp , false);

		Test.stopTest();
	}
	private static testmethod void testDxMethods() 
	{
		PEER_Requisition__c req = PEERRequisitionDao.getTestObj();
		ApexPages.StandardController sc = new ApexPages.standardController(req);                
    	phy_PeerUnifiedRequisitionController reqController = new phy_PeerUnifiedRequisitionController(new ApexPages.standardController(req));   
    	ApexPages.currentPage().getParameters().put('id', req.id);
		reqController.ReqObj.patient_first_name__c = '';
		reqController.ReqObj.patient_name__c = '';

		r_patient__c pat = patientDao.getTestPat();

		Test.startTest();

		List<rEEGDxGroupDisp> groups = reqController.getDxGroupsL();
		groups = reqController.getDxGroupsR();
		groups = reqController.getDxGroups();

		for(rEEGDxGroupDisp grpRow : groups)
        {
            for(rEEGDxDisp dxRow : grpRow.getDxs())
            {   
            	r_reeg_dx__c obj = dxRow.getObj();
            	obj.primary__c = true;
            	dxRow.setObj(obj);
            	break;
            }
            break;
        }

        reqController.saveAllDx();

		Test.stopTest();
	}

	private static testmethod void testMedMethods() 
	{

		PEER_Requisition__c req = PEERRequisitionDao.getTestObj();
		ApexPages.StandardController sc = new ApexPages.standardController(req);                
    	phy_PeerUnifiedRequisitionController reqController = new phy_PeerUnifiedRequisitionController(new ApexPages.standardController(req));   
    	ApexPages.currentPage().getParameters().put('id', req.id);
		reqController.loadAction();
		r_patient__c pat = patientDao.getTestPat();

		Test.startTest();

		PageReference pr = reqController.saveRequisition();
		reqController.reopenRequisition();
		pr = reqController.deleteRequisition();

		Boolean testBool = reqController.SkipValidation;

		//testBool = reqController.getWashoutConfirmError();
		reqController.forwardCalc();
		reqController.backwardsCalc();
		reqController.saveOutcomeMeds();
		reqController.displayWashoutError();

		Test.stopTest();
	}
	
}