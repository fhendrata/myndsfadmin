//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: UserAgreementDaoTest
// PURPOSE: Unit test method for the UserAgreementDao class
// CREATED: 05/03/16 Ethos Solutions - www.ethos.com
// Author: Mitchell Corbin
//--------------------------------------------------------------------------------
@isTest
public with sharing class UserAgreementDaoTest {
	
	private static testmethod void testUserAgreementDao()
	{
		UserDao dao = new UserDao();
		User CurrentUser = dao.getById(UserInfo.getUserId());

		UserAgreementDao uDao = new UserAgreementDao();
		User_Agreement__c uAgree = uDao.getByUser(CurrentUser);
		insert uAgree;

		System.assertEquals(true, (uAgree != null));

		uAgree = uDao.getByUser(CurrentUser);

		// Since a User_Agreement__c record already exists for the User, this will allow the test to traverse the else logic in the UserAgreementDao class
		User_Agreement__c uAgreeClone = uDao.getByUser(CurrentUser);

		// Ensuring the two variables are actually referring to the same record after an insert, signifying there is only one User_Agreement__c record for the specific User
		System.assertEquals(true, (uAgreeClone == uAgree));
	}
}