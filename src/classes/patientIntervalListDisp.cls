//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: patientIntervalListDisp
// PURPOSE: Display class for interval lists
// CREATED: 07/13/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class patientIntervalListDisp 
{
	public r_interval__c Interval {get; set;}
	public Decimal Duration {get; set;}
	
	public string getDurStr()
	{
		if (Duration != null)
			return Duration.intValue().format();
		else
			return '';
	}
	
	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testPatientIntervalListDisp()
    {
        patientIntervalListDisp obj = new patientIntervalListDisp();
     
     	obj.Interval = new r_interval__c();
     	obj.Duration = 1.1;
     	System.assertEquals(obj.Duration, 1.1);
     	System.assertEquals(obj.getDurStr(), '1');
     
    }
        
}