//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: LeadDao
//   PURPOSE: Data Access for: the Lead records
//     OWNER: CNS Response
//   CREATED: 06/06/2014 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
public class LeadDao extends BaseDao 
{
	private static final String NAME = 'Lead';
	private static final LeadDao leadDao = new LeadDao();

	public LeadDao() {
		super(NAME);
	}

	public static LeadDao getInstance() {
		return leadDao;
	}

	public Lead getById(String idInp) {
		return (Lead)getSObjectById(getFieldStrNew(), idInp);
	}

	public Lead getTestLead() 
	{
		Lead newLead = new Lead();
		newLead.LastName = 'TESTER';
		newLead.FirstName = 'John';
		newLead.Company = 'Test Company';
		newLead.Status = 'Open';
		insert newLead;
		return newLead;
		
	}
}