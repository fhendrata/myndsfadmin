/**
 * An apex page controller that exposes the site login functionality
 */
public class SiteLoginController extends BaseController
{   
    public SiteLoginController()
    {
    }
    
    public PageReference loadAction()
    {   
        if (UserInfo.getUserType() != 'Guest') return Page.phy_PeerListS2;

        return null;
    }
    
}