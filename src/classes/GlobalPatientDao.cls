//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: GlobalPatientDao
//   PURPOSE: Permission free Data Access for patient records.
// 
//     OWNER: CNS Response
//   CREATED: 03/22/17 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
public without sharing class GlobalPatientDao extends BaseDao  
{
	private static String NAME = 'r_patient__c'; 
    private static final GlobalPatientDao patDao = new GlobalPatientDao();

    public static GlobalPatientDao getInstance() 
    {
        return patDao; 
    }
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_patient__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public r_patient__c getById(String idInp)
    {
		return (r_patient__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public r_patient__c getById(String idInp, boolean includePhyFields)
    {
    	if (!includePhyFields)
			return (r_patient__c)getSObjectById(getFieldStr(), NAME, idInp);
		else
		{
			string fullFieldStr = getFieldStr();
			fullFieldStr += ', physician__r.OwnerId, physician__r.name, physician__r.Account.name';
			
			System.debug('### fullFieldStr: ' + fullFieldStr);
			return (r_patient__c)getSObjectById(fullFieldStr, NAME, idInp);
		}
    } 

    public Boolean addManualSharingRule(Id recordId, Id shareWithId, String accessLevel)
    {
        r_patient__share patShr  = new r_patient__share();
        patShr.ParentId = recordId;
        patShr.UserOrGroupId = shareWithId;
        patShr.AccessLevel = accessLevel;
        patShr.RowCause = Schema.r_patient__share.RowCause.Manual;

        Database.SaveResult sr = Database.insert(patShr,false);

        if(!sr.isSuccess())
        {
            Database.Error err = sr.getErrors()[0];
             
            // Check if the error is related to trival access level. Access level must be more permissive than the object's default.
            // These sharing records are not required and thus an insert exception is acceptable. 
            return (err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION && err.getMessage().contains('AccessLevel'));
        }

        return true;
    }
    
    public r_patient__c getByReegId(String reegId)
    {
    	r_patient__c pat = null;
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
	    	rEEGDao rDao = new rEEGDao();
	    	r_reeg__c reeg = rDao.getById(reegId);
	    	
	    	if (reeg != null && !e_StringUtil.isNullOrEmpty(reeg.r_patient_id__c))
	    	{
		    	pat = getById(reeg.r_patient_id__c);
	    	}
    	}
		return pat;
    } 
    
    public List<r_patient__c> getByPhyId(String phyId, DateTime dt)
    {
    	List<r_patient__c> patList = null;
    	
    	String whereStr = ' physician__c = \'' + phyId + '\'';
    	
    	whereStr += ' and ' + getMonthSpanSql(dt);
    	
    	patList = (List<r_patient__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, '', '9500');

		return patList;
    } 
    
    public Contact getContactByPortalUsername(String username) 
    {
        for (Contact con : [select Id, Name, physician_portal_user__c from Contact where physician_portal_user__r.Username =: username])
            return con;

        return null;
    }
    
    public List<r_patient__c> getByMonthYear(integer month, integer year)
    {
    	DateTime dt = DateTime.newInstance(year, month, 1);
    	string whereStr = getMonthSpanSql(dt);
    	return (List<r_patient__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, '', '9500');
    }
    
    public String getProviderPatientsQuery(String limitStr, String ordStr)
    {
        return getProviderPatientsQuery(limitStr, ordStr, false);
    }
     
    public String getProviderPatientsQuery(String limitStr, String ordStr, Boolean isAdminOrPortalUserMgr)
    {
    	String whereStr = '';
    	String query = '';
        String baseQuery = 'SELECT Id, Name, Last_PEER_Date__c, Days_Since_Last_PEER__c, Outcomes__c, first_name__c, middle_initial__c, prov_pat_id__c, (Select r.Id, r.neuroguide_status__c, r.Process_Used_For_Test__c, r.rpt_stat__c, r.req_stat__c From r_reeg__r r order by r.req_date__c desc) FROM r_patient__c ';
    	 
        query += ' WHERE CA_Control_Group_Type__c != \'Control Group\'';

        if (String.isNotBlank(filter))
    	   query +=  ' AND ' + filter;
    	
        if(!e_StringUtil.isNullOrEmpty(ordStr)) 
            query += ' ' + ordStr + ' ';
    	else 
            query += ' order by Last_PEER_Date__c desc NULLS LAST ';
    	
        if (StringUtil.isNullOrEmpty(limitStr)) 
            limitStr = ' limit 9999 ';
        query += ' ' + limitStr + ' ';

    	system.debug('QUERY: ' + baseQuery + query);
    	
    	return baseQuery + query;
    	
    }
    
    public String getNameSearchQuery(String firstLastName, String rLimit)
    {
    	if (StringUtil.isNullOrEmpty(rLimit)) rLimit = 'limit 9999';
    	firstLastName = escapeStr(firstLastName);
        List<String> tokens = firstLastName.split(' ',2);
    	 
    	String whereStr = '';
        String baseQuery = 'SELECT Id, Name, Last_PEER_Date__c, Days_Since_Last_PEER__c, Outcomes__c, first_name__c, middle_initial__c, prov_pat_id__c FROM r_patient__c ';
         
        if(tokens.size() == 1)
        {
            whereStr = 'where (Name like '+ addLike(firstLastName) + ' OR first_name__c like ' + addLike(firstLastName) + ')';
        }
        else
        {
            String whereStr1 = 'where ((Name like '+ addLike(tokens[0]) + ' AND first_name__c like ' + addLike(tokens[1]) + ') ';
            String whereStr2 = 'OR (first_name__c like '+ addLike(tokens[0]) + ' AND Name like ' + addLike(tokens[1]) + '))';
            whereStr = whereStr1 + whereStr2;
        }
        
        //TODO - include filter
        
        String query = baseQuery + whereStr + ' AND ' + filter + ' order by Last_PEER_Date__c desc NULLS LAST  ' + rLimit; 
        
        system.debug('%%FILTER: ' + filter);
        
        if(!e_StringUtil.isNullOrEmpty(filter)) return baseQuery + whereStr + ' AND ' + filter + ' order by Last_PEER_Date__c desc NULLS LAST  ' + rLimit; 
        else return baseQuery + whereStr + ' order by Last_PEER_Date__c desc NULLS LAST ' + rLimit;
    }

    public List<r_patient__c> getByIds(Set<Id> patIds, String rLimit)
    {
        String whereStr = 'WHERE Id IN :patIds ';
        String baseQuery = 'SELECT Id, Name, Last_PEER_Date__c, Days_Since_Last_PEER__c, Outcomes__c, first_name__c, middle_initial__c, prov_pat_id__c FROM r_patient__c ';
        String orderBy = ' ORDER BY Last_PEER_Date__c DESC NULLS LAST ';

        return Database.query(baseQuery + whereStr + orderBy + rLimit);
    }
    
    public List<r_patient__c> getAllSearchResults(string lnStr, string fnStr, string dob)
    {
    	string whereStr = '';
    	if (!e_StringUtil.isNullOrEmpty(lnStr))
    	{
    		whereStr = ' name LIKE \'' + lnStr + '%\'';
    	}
    	
    	if (!e_StringUtil.isNullOrEmpty(fnStr))
    	{
    		if (!e_StringUtil.isNullOrEmpty(whereStr))
    			whereStr += ' AND ';
    		whereStr += ' first_name__c LIKE \'' + fnStr + '%\'';
    	}
    	
    	if (!e_StringUtil.isNullOrEmpty(dob))
    	{
    		if (!e_StringUtil.isNullOrEmpty(whereStr))
    			whereStr += ' AND ';
    		whereStr += ' dob__c LIKE \'' + dob + '%\'';
    	}

        system.debug('### whereStr: ' + whereStr);
    	
		return getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'name, first_name__c', '500');
    } 
    
    public List<r_patient__c> getAllByFnOrLn(string nameStr)
    {
    	String whereStr = ' name LIKE \'' + nameStr + '\' or first_name__c LIKE \'' + nameStr + '\'';
		return getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'name, first_name__c', '500');
    } 
    
    public List<r_patient__c> getAllByFnAndLnAndDOB(string lnStr, string fnStr, string dob)
    {
    	String whereStr = ' name LIKE \'' + lnStr + '%\' AND first_name__c LIKE \'' + fnStr + '%\' AND dob__c LIKE \'' + dob + '%\'';
		return getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'name, first_name__c', '500');
    } 
    
    public List<r_patient__c> getAllByFnAndLn(string lnStr, string fnStr)
    {
    	String whereStr = ' name LIKE \'' + lnStr + '%\' AND first_name__c LIKE \'' + fnStr + '%\'';
		return getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'name, first_name__c', '500');
    }
    
    public List<r_patient__c> getAllByFnAndDOBOrLnAndDOB(string lnStr, string fnStr, string dob)
    {
    	String whereStr = ' (name LIKE \'' + lnStr + '%\' AND dob__c LIKE \'' + dob + '%\') OR (first_name__c LIKE \'' + fnStr + '%\' AND dob__c LIKE \'' + dob + '%\')';
		return getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'name, first_name__c', '500');
    }  
    
    public List<r_patient__c> getAllByFnOrLnOrCnsId(string nameStr)
    {
    	String whereStr = ' name LIKE \'' + nameStr + '\' or first_name__c LIKE \'' + nameStr + '\' or CNS_ID__c LIKE \'' + nameStr + '\'';
		return getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'name, first_name__c', '500');
    } 
    
    public List<r_patient__c> getMostRecent(string numRec)
    {
		return getSObjectListByWhere(getFieldStr(), NAME, '', 'LastModifiedDate DESC', numRec);
    }  
    
    public static r_patient__c getTestPat()
    {
    	Contact con = ContactDao.getTestPhysician();
    	r_patient__c testPat = new r_patient__c();
    	testPat.Name = 'Smith';
    	testPat.first_name__c = 'John';
    	testPat.dob__c = '01/01/1960';
    	testPat.physician__c = con.Id;
    	
    	insert testPat;
    	
    	return testPat;
    }

    public static r_patient__c getTestPatPO2()
    {
        Contact con = ContactDao.getTestPhysicianPO2();
        r_patient__c testPat = new r_patient__c();
        testPat.Name = 'Smith';
        testPat.first_name__c = 'John';
        testPat.dob__c = '01/01/1960';
        testPat.physician__c = con.Id;
        
        insert testPat;
        
        return testPat;
    }
 
}