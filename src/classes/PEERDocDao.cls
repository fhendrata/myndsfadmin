//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: PEERDocDao
//   PURPOSE: Data Access for: PEER Documents
// 
//     OWNER: CNS Response
//   CREATED: 03/08/12 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
public with sharing class PEERDocDao extends BaseDao 
{
	private static String NAME = 'PEER_Documentation__c';
	
	private static String fldList;	
	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.PEER_Documentation__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public PEER_Documentation__c getById(String idInp)
    {
		return (PEER_Documentation__c)getSObjectById('Id, Name, Categories__c, Sub_Title__c, Body__c, Environment_Type__c', NAME, idInp);
    } 
    
    public List<PEER_Documentation__c> getMostRecent(string numRec)
    {
		return getSObjectListByWhere(getFieldStr(), NAME, '', 'LastModifiedDate DESC', numRec);
    }  
    
    public String getCategorySearchQuery(String limitStr, String ordStr)
    {
    	String query = '';
        String baseQuery = 'SELECT Id, Name, Categories__c, Sequence__c, Sub_Title__c, Environment_Type__c FROM PEER_Documentation__c ';
    	    	
    	if(filter != null) {query += (' WHERE Published__c = true and ' + filter);}
    	if(!e_StringUtil.isNullOrEmpty(limitStr)) {query += ' ' + limitStr + ' ';}
        if(!e_StringUtil.isNullOrEmpty(ordStr)) {query += ' ' + ordStr + ' ';}
    	    	
    	system.debug('### query: ' + query);    	
    	return baseQuery + query;
    }   
    
    public static PEER_Documentation__c getTestPEERDoc()
    {
    	PEER_Documentation__c testDoc = new PEER_Documentation__c();
    	testDoc.Name = 'Test Doc';
    	testDoc.Body__c = 'this is a test';
    	testDoc.Published__c = true;
    	insert testDoc;
    	return testDoc;
    }
    
    
    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testPEERDocDao()
    {
        PEERDocDao dao = new PEERDocDao();
        dao.IsTestCase = true;
        
      //  System.assert(!e_StringUtil.isNullOrEmpty(PEERDocDao.getFieldStr()));
        
        PEER_Documentation__c testDoc = PEERDocDao.getTestPEERDoc();
        PEER_Documentation__c testDoc2 = PEERDocDao.getTestPEERDoc();
        PEER_Documentation__c testDoc3 = PEERDocDao.getTestPEERDoc();
        
        PEER_Documentation__c doc = dao.getById(testDoc.Id);
        
      //  List<PEER_Documentation__c> testList = dao.getMostRecent('2');
        
   		String queryStr = dao.getCategorySearchQuery('', '');
   		queryStr = dao.getCategorySearchQuery('test', '');
   		queryStr = dao.getCategorySearchQuery('', 'test');
   		queryStr = dao.getCategorySearchQuery('test', 'test');
    }    
    
}