//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: phy_PEERValidationController
// PURPOSE: Controller class for PEER requisistion validation page
// CREATED: 01/05/15 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class phy_PEERValidationController extends PEERBaseController
{
	public PEER_Requisition__c ReqObj { get; set; }
	public List<rEEGDxDisp> UserSelectedDxList { get; set; }
    public List<Req_Outcome_Medication__c> CurrentMedList { get; set; }
    private String PatRefId { get; set; }
    private Id ReegId { get; set; }
    private Id OutcomeId { get; set; }
    public Boolean ShowStatus { get; set; }

	public Boolean IsTypeOne {  get { return (!IsPeerTypeEmpty && ReqObj.reeg_reeg_type__c == 'Type I'); } }
    public Boolean IsDxListEmpty {  get { return UserSelectedDxList.isEmpty(); } }
    public Boolean IsFirstNameEmpty {  get { return (String.isBlank(ReqObj.patient_first_name__c)); } }
    public Boolean IsLastNameEmpty {  get { return (String.isBlank(ReqObj.patient_name__c)); } }
    public Boolean IsDobEmpty {  get { return (String.isBlank(ReqObj.patient_dob__c)); } }
    public Boolean IsPeerTypeEmpty { get { return (String.isBlank(ReqObj.reeg_reeg_type__c)); } }
    public Boolean IsEegTechEmpty { get { return (String.isBlank(ReqObj.reeg_r_eeg_tech_contact__c)); } }
    public Boolean IsCGSEmpty { get { return (String.isBlank(ReqObj.outcome_cgs__c)); }}
    public Boolean IsBillingCodeEmpty { get { return (String.isBlank(ReqObj.reeg_phy_billing_code__c)); }}
    public Boolean IsCGIEmpty { get { return (String.isBlank(ReqObj.outcome_cgi__c)); } }
    public Boolean IsEegRecStatusEmpty { get { return (String.isBlank(ReqObj.reeg_eeg_rec_stat__c)); } }
    public Boolean IsPending { get { return (ReqObj.Status__c == 'Pending'); } }
    public Boolean IsSubmitted { get { return (ReqObj.Status__c == 'Submitted' || ReqObj.Status__c == 'Complete'); } }
    public Boolean IsEegStatusEmpty { get { return (String.isBlank(ReqObj.EEG_Status__c) || ReqObj.EEG_Status__c == 'Not Uploaded' || ReqObj.EEG_Status__c == 'Bad' || ReqObj.EEG_Status__c == 'Not Supported' || ReqObj.EEG_Status__c == 'EEG Make does not match file type'); } }
    public Boolean IsPhysicianEmpty { get { return (String.isBlank(ReqObj.Physician__c)); } }
    //public Boolean IsMedListVerified {get { return (!ReqObj.No_Medication_Verified__c && ReqObj.Is_Medication_List_Empty__c); } }
    public Boolean CanSubmit { get { return (!IsFirstNameEmpty && !IsLastNameEmpty && !IsDobEmpty && !IsPeerTypeEmpty && !IsEegTechEmpty && !IsEegStatusEmpty && !IsPhysicianEmpty && ReqObj.Status__c != 'Submitted' && ReqObj.Status__c != 'Complete' && ReqObj.Patient_Consent__c); } }

    public phy_PEERValidationController(PEER_Requisition__c obj) 
    {
        this.ReqObj = obj;
    }

	public phy_PEERValidationController(ApexPages.StandardController stdController) 
    {
        this.ReqObj = (PEER_Requisition__c)stdController.getRecord();
    }

	public PageReference loadAction()
    {
    	ReqObj = PEERRequisitionDao.getInstance().getById(ReqObj.Id);
        PatRefId = ApexPages.currentPage().getParameters().get('prId');
        
        loadSavedDxList();
        loadSavedMedList();

        ShowStatus = false;
        
        return null;
    }

    private void loadSavedDxList()
    {
        UserSelectedDxList = new List<rEEGDxDisp>();

        for(r_reeg_dx__c dxRow : [select dx_name__c, primary__c, r_ref_dx_id__c, rule_out__c, secondary__c, id, name from r_reeg_dx__c where PEER_Requisition__c = :ReqObj.Id limit 20])
        {
            UserSelectedDxList.add(new rEEGDxDisp(dxRow));
        }
    }

    private void loadSavedMedList()
    {
        CurrentMedList = new List<Req_Outcome_Medication__c>();
        CurrentMedList = ReqOutcomeMedDao.getInstance().getByRequisitionId(ReqObj.Id);
    }

    public void submit()
    {
        submit(ReqObj);
    }

    public void submit(PEER_Requisition__c ReqObj)
    {
        Boolean saveOk = true;
        Savepoint sp = Database.setSavepoint();
        ReqObj.Patient_Id__c = savePatient();

        System.debug('### submit:ReqObj.Patient_Id__c: ' + ReqObj.Patient_Id__c);

        saveOk = String.isNotBlank(ReqObj.Patient_Id__c);

        System.debug('### submit:saveOk: ' + saveOk);
        
        if (saveOk)
        {
            ReegId = GlobalPEERRequisitionDao.getInstance().saveReeg(ReqObj);
            saveOk = (ReegId != null);
        }

        if (saveOk)
            saveOk = saveDx();

        System.debug('submit:saveDx:saveOk ' + saveOk);

        if (saveOk)
        {
            Id ocId = saveOutcome(ReqObj.Patient_Id__c);

            System.debug('submit:saveOutcome:ocId ' + ocId);

            if (String.isNotBlank(ocId))
                saveOk = saveOutcomeMed(ocId);
            else
                saveOk = false;
        }

        buildIntervals();

        if (!saveOk)
        {
            Database.rollback( sp ); 
            loadAction();
        }
        else
        {
            System.debug('ReqObj.RecordTypeId: ' + ReqObj.RecordTypeId);
            ReqObj.Status__c = 'Submitted';
            GlobalPEERRequisitionDao.getInstance().save(ReqObj);
        }

        ShowStatus = true;
    }

    private Id savePatient()
    {
        r_patient__c pat = new r_patient__c();

        if (String.isNotBlank(ReqObj.Patient_Id__c))
        {
            pat = patientDao.getInstance().getById(ReqObj.Patient_Id__c);
        }

        pat.first_name__c = ReqObj.patient_first_name__c;
        pat.name = ReqObj.patient_name__c;
        if (String.isNotBlank(ReqObj.patient_middle_initial__c))
            pat.middle_initial__c = ReqObj.patient_middle_initial__c.left(1);
        pat.dob__c = ReqObj.patient_dob__c;
        pat.Gender__c = ReqObj.patient_gender__c;
        
        pat.physician__c = ReqObj.Physician__c;
        pat.test_data_flag__c = ReqObj.test_data_flag__c;
        pat.anticipated_eeg_test__c = ReqObj.patient_anticipated_eeg_test__c;
        pat.prov_pat_id__c = ReqObj.patient_prov_pat_id__c;
        pat.desired_eeg_test__c = ReqObj.patient_desired_eeg_test__c;
        pat.PEER_Requisition__c = ReqObj.Id;

        if (String.isNotBlank(ReqObj.Account_Manager_Email__c))
            pat.Account_Manager_Email__c = ReqObj.Account_Manager_Email__c;

        if (String.isNotBlank(ReqObj.Study_Coordinator_Email__c))
            pat.Study_Coordinator_Email__c = ReqObj.Study_Coordinator_Email__c;
        if (String.isNotBlank(ReqObj.Study_Coordinator_Email_2__c))
            pat.Study_Coordinator_Email_2__c = ReqObj.Study_Coordinator_Email_2__c;
        if (String.isNotBlank(ReqObj.Study_Coordinator_Email_3__c))
            pat.Study_Coordinator_Email_3__c = ReqObj.Study_Coordinator_Email_3__c;

        System.debug('### PatRefId: ' + PatRefId);

        if(String.isNotBlank(PatRefId)) 
        {
            Patient_Referral__c convPatRef = patientReferralDao.getInstance().getById(PatRefId);
            if (convPatRef != null)
            {  
                pat.Patient_Referral__c = convPatRef.Id;
                convPatRef.Physician_Referral_Status__c = 'Accepted';
                patientReferralDao.getInstance().saveSObject(convPatRef);
            }
        }

        UserDao useDao = new UserDao();
        User currentUser = useDao.getCurrentUser();

        System.debug('Val: currentUser.Contact.PEER_Ownership__c: ' + currentUser.Contact.PEER_Ownership__c);
        
        if ( !(currentUser != null && !String.isBlank(currentUser.Contact.PEER_Ownership__c) && currentUser.Contact.PEER_Ownership__c.equalsIgnoreCase('Self')))
        {
            //-- if the user is an admin the phy field will be set by the admin and the owner of the record must be set to the phy.
            if (String.isNotBlank(pat.physician__c))
            {
                System.debug('Contact Id (pat.physician__c): ' + pat.physician__c);
                User phy = useDao.getUserIDByContactID(pat.physician__c);
                System.debug('phy: ' + phy);

                if (phy != null)
                    pat.OwnerId = phy.Id;
            }
        }

        try
        {
            upsert pat;
            System.debug('### savePatient:upsert pat OK');
        }
        catch (Exception ex)
        {
            System.debug('### savePatient:upsert pat ex: ' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Patient save error: ' + ex.getMessage()));
            return null;
        }
        
        ReqObj.Patient_Id__c = pat.Id;
        ReqObj.Patient__c = pat.Id;

        return pat.Id;
    }

    //-- 8-15-17.jdepetro deprecated to use only the GlobalPEERRequisitionDao for saving new rEEG records.
    //private Boolean saveReeg()
    //{
    //    r_reeg__c reeg = null;

    //    if (String.isBlank(ReqObj.PEER__c))
    //    {
    //        reeg = new r_reeg__c();
    //    }
    //    else
    //    {
    //        rEEGDao rDao = new rEEGDao();
    //        reeg = rDao.getById(ReqObj.PEER__c);
    //    }

    //    reeg.r_patient_id__c = ReqObj.Patient_Id__c;
    //    reeg.phy_billing_code__c = ReqObj.reeg_phy_billing_code__c;
    //    reeg.eeg_notes__c = ReqObj.reeg_eeg_notes__c;
    //    reeg.eeg_rec_stat__c = (ReqObj.EEG_Status__c == 'Uploaded' || ReqObj.EEG_Status__c == 'Complete') ? 'Received' : '';
    //    reeg.r_eeg_tech_contact__c = ReqObj.reeg_r_eeg_tech_contact__c;
    //    reeg.eeg_upload__c = (ReqObj.EEG_Status__c == 'Uploaded' || ReqObj.EEG_Status__c == 'Complete') ? 'Yes' : '';
    //    reeg.reeg_type__c = ReqObj.reeg_reeg_type__c;
    //    if (!ReqObj.Patient_will_be_washed_out__c && ReqObj.reeg_reeg_type__c != 'Type II')
    //    {
    //        reeg.reeg_type_mod__c = '(m)';
    //    }
    //    reeg.Use_Rapid_Turnaround__c = ReqObj.reeg_Rapid_Turnaround__c;
    //    reeg.test_data_flag__c = ReqObj.test_data_flag__c;
    //    reeg.do_not_send_neuro__c = !ReqObj.Neuro_Review__c;
    //    reeg.PEER_Requisition__c = ReqObj.Id;
    //    reeg.Bill_To__c = ReqObj.Bill_To__c;
    //    reeg.req_date__c = Datetime.now();
    //    reeg.Study_Name__c = ReqObj.Study_Name__c;

    //    System.debug('saveReeg:ReqObj.EEG_Upload_Date__c: ' + ReqObj.EEG_Upload_Date__c);
    //    if (ReqObj.EEG_Upload_Date__c != null)
    //        reeg.eeg_rec_date__c = ReqObj.EEG_Upload_Date__c;
    //    reeg.req_stat__c = 'In Progress';
    //    reeg.rpt_stat__c = 'In Progress';
    //    reeg.eeg_make__c = ReqObj.EEG_Make__c;
    //    reeg.neuroguide_status__c = 'Requisition Submitted';
    //    reeg.Process_Used_For_Test__c = (String.isNotBlank(ReqObj.Process_Used_For_Test__c)) ? ReqObj.Process_Used_For_Test__c : 'NeuroGuide 2';

    //    if (String.isNotBlank(ReqObj.Study_Coordinator_Email__c))
    //        reeg.Study_Coordinator_Email__c = ReqObj.Study_Coordinator_Email__c;
    //    if (String.isNotBlank(ReqObj.Study_Coordinator_Email_2__c))
    //        reeg.Study_Coordinator_Email_2__c = ReqObj.Study_Coordinator_Email_2__c;
    //    if (String.isNotBlank(ReqObj.Study_Coordinator_Email_3__c))
    //        reeg.Study_Coordinator_Email_3__c = ReqObj.Study_Coordinator_Email_3__c;

    //    if (String.isNotBlank(ReqObj.patient_dob__c))
    //    {
    //        Date dobDt = e_StringUtil.setDateFromString(ReqObj.patient_dob__c);
    //        integer numberDaysOld = dobDt.daysBetween(Date.today());             
    //        if (numberDaysOld > 0)
    //            reeg.patient_age__c = (numberDaysOld / 365.242199);
    //    }

    //    if (String.isNotBlank(ReqObj.Study_Name__c) && ReqObj.Study_Name__c.equalsIgnoreCase('Walter Reed'))
    //    {
    //        reeg.RecordTypeId = PEERRequisitionDao.getInstance().getRecordTypeIdByName('WR Record Type');
    //    }
    //    else if (reqObj.RecordTypeId != null && String.isNotBlank(reqObj.RecordType.Name))
    //    {         
    //        reeg.RecordTypeId = PEERRequisitionDao.getInstance().getRecordTypeIdByName(reqObj.RecordType.Name);
    //    }      

    //    try
    //    {
    //        rEEGDao.getInstance().SavePEEROnly(reeg, 'UPDATE_DB_STATUS');
    //    }
    //    catch (Exception ex)
    //    {
    //        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'PEER save error: ' + ex.getMessage()));
    //        System.debug('saveReeg:ex: ' + ex);
    //        return false;
    //    }

    //    ReegId = reeg.Id;
    //    ReqObj.PEER__c = reeg.Id;

    //    return true;
    //}

    private Boolean saveDx()
    {
        List<r_reeg_dx__c> saveList = new List<r_reeg_dx__c>();
        for (rEEGDxDisp dispRow : UserSelectedDxList)
        {
            dispRow.SetReegId(ReegId);
            saveList.add(dispRow.getObj());
        }

        upsert saveList;

        return true;
    }

    private Id saveOutcome(Id patId)
    {
        r_patient_outc__c oc = new r_patient_outc__c();
        oc.patient__c = patId;
        oc.PEER_Requisition__c = ReqObj.Id;
        oc.rEEG__c = ReegId;
        oc.cgi__c = ReqObj.outcome_cgi__c;
        oc.cgs__c = ReqObj.outcome_cgs__c;
        oc.insert_method__c = 'Portal 2.5';
        oc.outcome_date__c = Date.today();

        try
        {
            upsert oc;
        }
        catch (Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Outcome save error: ' + ex.getMessage()));
            System.debug('saveOutcome:ex: ' + ex);
            return null;
        }

        return oc.Id;
    }

    private Boolean saveOutcomeMed(Id outcomeId)
    {
        List<r_outcome_med__c> newMeds = new List<r_outcome_med__c>();
        for(Req_Outcome_Medication__c med : CurrentMedList)
        {
            newMeds.add(new r_outcome_med__c(r_outcome_id__c = outcomeId, previous_med__c = med.previous_med__c, calc_washout_date__c = med.calc_washout_date__c, dosage__c = med.dosage__c, end_date__c = med.end_date__c, frequency__c = med.frequency__c, med_name__c = med.med_name__c, start_date__c = med.start_date__c, start_date_is_estimated__c = med.start_date_is_estimated__c, unit__c = med.unit__c, washout_days__c = med.washout_days__c, is_misspelled__c = med.is_misspelled__c, patient_id__c = ReqObj.Patient_Id__c, patient_initials__c = ReqObj.Patient_Initials__c, Entry_Type__c = med.Entry_Type__c, Patient_Will_Wash_Out__c = med.Patient_Will_Wash_Out__c, Washout_Requirement__c = med.Washout_Requirement__c));
        }

        try
        {
            upsert newMeds;
        }
        catch (Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Outcome Med save error: ' + ex.getMessage()));
            System.debug('saveOutcome:ex: ' + ex);
            return false;
        }

        return true;
    }

    private void buildIntervals()
    {
        patientIntervalBuilder intBuilder = new patientIntervalBuilder();
        intBuilder.buildAllIntervalsForPat(ReqObj.Patient_Id__c, ReqObj.patient_physician_user__c);
    }

    public PageReference returnToPatient()
    {
        if (String.isNotBlank(ApexPages.currentPage().getParameters().get('retURL')))
        {
            PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
            pr.getParameters().put('tab', 'pat');
            return pr;
        }
        else return null;
    }

    public PageReference returnToDx()
    {
        if (String.isNotBlank(ApexPages.currentPage().getParameters().get('retURL')))
        {
            PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
            pr.getParameters().put('tab', 'dx');
            return pr;
        }
        else return null;
    }

    public PageReference returnToWo()
    {
        if (String.isNotBlank(ApexPages.currentPage().getParameters().get('retURL')))
        {
            PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
            pr.getParameters().put('tab', 'wo');
            return pr;
        }
        else return null;
    }

    public PageReference returnToEeg()
    {
        if (String.isNotBlank(ApexPages.currentPage().getParameters().get('retURL')))
        {
            PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
            pr.getParameters().put('tab', 'eeg');
            return pr;
        }
        else return null;
    }

    public PageReference returnToSubmit()
    {
        if (String.isNotBlank(ApexPages.currentPage().getParameters().get('retURL')))
        {
            PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
            pr.getParameters().put('tab', 'submit');
            return pr;
        }
        else return null;
    }

    public PageReference returnToRequisition()
    {
        if (String.isNotBlank(ApexPages.currentPage().getParameters().get('retURL')))
            return new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        else return null;
    }

}