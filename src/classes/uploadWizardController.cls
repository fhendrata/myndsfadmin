public with sharing class uploadWizardController extends BaseUploadWizController
{
    public string firstname {get; set;}
    public string lastname {get; set;}
    public string dob {get; set;}
    public List<r_patient__c> patAllList {get; set;}
    public boolean showResults {get; set;}
    public boolean isFirstSearchDone {get; set;}
    public boolean isNewPatBtnVisible {get; set;}

    Public uploadwizardController()
    {    
        init();
    }
    
    public void init()
    {
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.qsid = ApexPages.currentPage().getParameters().get('qsid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        string strIsNewPat = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(strIsNewPat) && strIsNewPat == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        isFirstSearchDone = false;
        showResults = false;
        isNewPatBtnVisible = false;
        isWiz = true;
        pDao = new patientDao();
    }
    
    public PageReference loadAction()
    {
    	if (e_StringUtil.isNullOrEmpty(patId))
    	{
    		patient = pDao.getById(patId);
    	}
    		
    	return null;
    }
    
    private List<r_patient__c> getMatchingPatients()
    {
        patAllList = new List<r_patient__c>();
        boolean fnameOk = !e_StringUtil.isNullOrEmpty(firstname);
        boolean lnameOk = !e_StringUtil.isNullOrEmpty(lastname);
        if (!e_StringUtil.isNullOrEmpty(dob))
        {
        	if (dob == 'mm/dd/yyyy')
        		dob = '';
        }
        
        boolean dobOk = !e_StringUtil.isNullOrEmpty(dob);
        
        if (fnameOk || dobOk || lnameOk) 
        {
        	patAllList = pDao.getAllSearchResults(lastname, firstname, dob);
        }
        else
        {
        	addMessage('At least one field must be entered.');
        }
        
        return patAllList;
    }
    
    public PageReference search1()
    {
		isFirstSearchDone = true;
        patAllList = getMatchingPatients();

       	showResults = (patAllList != null && patAllList.size() > 0);
        
        return null;
    }

    public PageReference newPatient()
    {
        if (lastname == null || firstname == null || lastname == '' || firstname == '') 
        {
            addMessage('First name and last name fields must be completed');
            return null;
        }
        else
        {
            AddPatient();

            if (patient != null && patient.id != null)
            {
                PageReference returnVal = new PageReference('/apex/uploadWizard2');
                isNewPat = true;
               	AddParameters(returnVal);
               	
                returnVal.setRedirect(true);
                return returnVal;
            }
            else
            {
                return new PageReference('/apex/uploadWizard1');
            }
        }
    }

    private void AddPatient()
    {
        patient = new r_patient__c();
        patient.name = lastName;
        patient.first_name__c = firstname;
        patient.dob__c = dob;

        pDao.saveSObject(patient);
    }

          //---Add a message to the message list
    private void addMessage(String value)
    {
        String message = e_StringUtil.isNullOrEmpty(value) ? 'All fields must be completed' : value; 
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, message);
        ApexPages.addMessage(msg);
    }
    
    
        //---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
        uploadWizardController cont = new uploadWizardController();
		cont.IsTestCase = true;
		r_reeg__c reeg = rEEGDao.getTestReeg();
		ApexPages.currentPage().getParameters().put('rid', reeg.Id);
		ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
		cont.init();

		boolean testBool = cont.isNewPatBtnVisible;
		
		PageReference pRef1 = cont.search1();
		pRef1 = cont.newPatient();

		patientDao patDao = new patientDao();
		r_patient__c pat = patDao.getById(reeg.r_patient_id__c);
		cont.patient = pat;	
		cont.AddPatient();
		
		cont.lastname = 'test';
		cont.firstname = 'test';
		
		pRef1 = cont.newPatient();
		
        System.assertEquals( 1, 1);
    }
}