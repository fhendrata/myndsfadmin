public class patientOutcomeDisp
{
	public string cgiPhy { get; set; }
	public string cgiPat { get; set; }
	public string cgs { get; set; }
	
    //---
    private r_patient_outc__c obj;
    public r_patient_outc__c getObj()
    {
        return obj;
    }
    public void setObj(r_patient_outc__c s)
    {
        obj = s;
    }

	public void setCgiPhysician(string CgiPhysician)
	{	
		cgiPhy = CgiPhysician;
	}
	
	public void setCgiPatient(string CgiPatient)
	{	
		cgiPat = CgiPatient;
	}
	
	public void setCgs(string CgsVal)
	{	
		cgs = CgsVal;
	}

    public void Setup(r_patient_outc__c o)
    {
        obj = o;
    }

    public string medications  { get; set; }
    public void setMedications(string s) {
    	medications = s;
    }

    //------------TEST-----------------------------
    public static testMethod void testDisp()
    {
        r_patient_outc__c obj = new r_patient_outc__c();

        patientOutcomeDisp disp = new patientOutcomeDisp();
        disp.setObj(obj);
        disp.Setup( obj);
        disp.setCgiPatient('test');
        disp.setCgiPhysician('test');
        disp.setCgs('test');
        disp.setMedications('test');

        r_patient_outc__c obj2 = disp.getObj();

        System.assertEquals( obj, obj2);
    }
}