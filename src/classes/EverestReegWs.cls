public class EverestReegWs {
    public class rEEGWsSoap {
        public String Domain { get  {  return rEEGUtil.getWebUrl();  }  }
        public String endpoint_x = Domain + '/everestws/reegWs.asmx';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://ethos.com/', 'EverestReegWs'};
        public String refreshSfStatus(String sessionId,String reegId) {
            EverestReegWs.refreshSfStatus_element request_x = new EverestReegWs.refreshSfStatus_element();
            EverestReegWs.refreshSfStatusResponse_element response_x;
            request_x.sessionId = sessionId;
            request_x.reegId = reegId;
            Map<String, EverestReegWs.refreshSfStatusResponse_element> response_map_x = new Map<String, EverestReegWs.refreshSfStatusResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://ethos.com/refreshSfStatus',
              'http://ethos.com/',
              'refreshSfStatus',
              'http://ethos.com/',
              'refreshSfStatusResponse',
              'EverestReegWs.refreshSfStatusResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.refreshSfStatusResult;
        }
        public String statusAction(String sessionId,String reegId,String userId,String actionType) {
            EverestReegWs.statusAction_element request_x = new EverestReegWs.statusAction_element();
            EverestReegWs.statusActionResponse_element response_x;
            request_x.sessionId = sessionId;
            request_x.reegId = reegId;
            request_x.userId = userId;
            request_x.actionType = actionType;
            Map<String, EverestReegWs.statusActionResponse_element> response_map_x = new Map<String, EverestReegWs.statusActionResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://ethos.com/statusAction',
              'http://ethos.com/',
              'statusAction',
              'http://ethos.com/',
              'statusActionResponse',
              'EverestReegWs.statusActionResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.statusActionResult;
        }
    }
    public class refreshSfStatusResponse_element {
        public String refreshSfStatusResult;
        private String[] refreshSfStatusResult_type_info = new String[]{'refreshSfStatusResult','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ethos.com/','true','false'};
        private String[] field_order_type_info = new String[]{'refreshSfStatusResult'};
    }
    public class statusAction_element {
        public String sessionId;
        public String reegId;
        public String userId;
        public String actionType;
        private String[] sessionId_type_info = new String[]{'sessionId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] reegId_type_info = new String[]{'reegId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] userId_type_info = new String[]{'userId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] actionType_type_info = new String[]{'actionType','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ethos.com/','true','false'};
        private String[] field_order_type_info = new String[]{'sessionId','reegId','userId','actionType'};
    }
    public class refreshSfStatus_element {
        public String sessionId;
        public String reegId;
        private String[] sessionId_type_info = new String[]{'sessionId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] reegId_type_info = new String[]{'reegId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ethos.com/','true','false'};
        private String[] field_order_type_info = new String[]{'sessionId','reegId'};
    }
    public class statusActionResponse_element {
        public String statusActionResult;
        private String[] statusActionResult_type_info = new String[]{'statusActionResult','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://ethos.com/','true','false'};
        private String[] field_order_type_info = new String[]{'statusActionResult'};
    }
}