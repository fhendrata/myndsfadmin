Public Class patientWizardController 
{

    private r_patient__c patient;
    private String patId;
    private String qsid;
    private String tid;
    private String reegId;
    private Profile profile;
    private boolean isWR {get; set;}
    private String outcomeId {get; set;}
    public boolean isWiz {get; set;}
    public boolean isNew {get; set;}
    public boolean isNewPat {get; set;}

    //---Constructor
    Public patientWizardController()
    {    
        this.patId = ApexPages.currentPage().getParameters().get('id');
        this.qsid = ApexPages.currentPage().getParameters().get('qsid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
         string wizParam = ApexPages.currentPage().getParameters().get('wiz');
    	this.isWiz = (!e_StringUtil.isNullOrEmpty(wizParam) && wizParam == 'true');
    	string newParam = ApexPages.currentPage().getParameters().get('isNew');
    	this.isNew = (!e_StringUtil.isNullOrEmpty(newParam) && newParam == 'true');
    	string newPatStr = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        this.profile = [Select p.Name From Profile p where p.Id=:UserInfo.getProfileId()];
        //this.patient = getPatient();
    }
    
    public PageReference loadAction()
    {
    	patientDao pDao = new patientDao();
    	if (!e_StringUtil.isNullOrEmpty(patId))
    	{
    		this.patient = pDao.getById(patId, true);
    	}
    	
    	return null;
    }

    public void setPatient(r_patient__c pat)
    {
        this.patient = pat;
    }

    //-- get current patient or create a new patient
    public r_patient__c getPatient()
    {
        if (patient == null)
        {
            if (patId == null)
                patient = new r_patient__c();
            else
                patient = [select id, name, age__c, CNS_ID__C, dob__c, first_name__c, Gender__c, handedness__c, insert_method__c, meds_gen__c, middle_initial__c,
                                    no_meds__c, physician__c, prov_pat_id__c, test_data_flag__c, anticipated_eeg_test__c from r_patient__c where id = :ApexPages.currentPage().getParameters().get('id')];
        }
        return patient;
    }


    public Boolean getIsMgr()
    {
        Boolean returnVal = false;
        
        if (profile != null)
        {
            if (profile.Name == 'CNSR Medical Director' || profile.Name == 'CNSR Production Manager' ||
                profile.Name == 'System Administrator' || profile.Name == 'CNSR System Administrator')
            {
               returnVal = true;
            }
        }
        
        return returnVal;
    }
    
    public Boolean getIsNotMgr()
    {
        return !getIsMgr();
    }

    public Boolean getDOBRequired()
    {
        return (qsid == 'null');
    }

    public PageReference patStep1()
    {
        if (patient != null && patient.id != null)
        {	
            PageReference returnVal = new PageReference( '/apex/patientWizard1Page?id=' + patient.id + '&qsid=' + qsid 
                                                            + '&rid=' + reegId + '&tid=' + tid + '&wr=' + isWR
                                                             + '&np=' +  ApexPages.currentPage().getParameters().get('np'));
            returnVal.setRedirect(true);
            return returnVal;
        }
        else
        {
            return Page.patientWizard1Page;
        }
    }

    public PageReference patStep2()
    {
        if (patient != null && patient.id != null)
        {
            SavePatient();

            if (qsid != null && qsid != '' && qsid != 'null')
            {
                if (patient.physician__c != null)
                {
                    String contactUserId;
                    for (Contact c : [select Id, Name from Contact where Id =:patient.physician__c limit 1]) {
                        contactUserId = c.Id;
                    }

                    if (contactUserId != null && contactUserId != '')
                    {
                    	string uId = '';
                    	boolean isActive = false;
                        for (User u : [select Id, Name, IsActive from User where ContactId =:contactUserId limit 1] ) {
                        	uId = u.Id;
                        	isActive = u.IsActive;
                        }
                        
                        if (isActive && !e_StringUtil.isNullOrEmpty(uId)) 
                        	patient.OwnerId = uId;

                        SavePatient();
                    }
                }
            }
            
            PageReference returnVal = new PageReference( '/apex/reegWizard3Page');
            if (!e_StringUtil.isNullOrEmpty(reegId))
	    		returnVal.getParameters().put('id', reegId);
	        if (!e_StringUtil.isNullOrEmpty(patId))
		    	returnVal.getParameters().put('pid', patId);
	    	else if (patient != null && !e_StringUtil.isNullOrEmpty(patient.Id))
	    		returnVal.getParameters().put('pid', patient.Id);
	    	if (!e_StringUtil.isNullOrEmpty(outcomeId))
	    		returnVal.getParameters().put('oid', outcomeId);
	        if (!e_StringUtil.isNullOrEmpty(tid))
		    	returnVal.getParameters().put('tid', tid);
	    	if (!e_StringUtil.isNullOrEmpty(qsid))
	    		returnVal.getParameters().put('qsid', qsid);
	    	if (isNewPat != null && isNewPat)
	    		returnVal.getParameters().put('np', 'true');
	    	if (isWiz != null && isWiz)
	        	returnVal.getParameters().put('wiz', 'true');
	    	if (isNew != null && isNew)
	        	returnVal.getParameters().put('isNew', 'true');
	        if (isWR != null && isWR)
	    		returnVal.getParameters().put('wr', 'true');
            returnVal.setRedirect(true);
            return returnVal;
        }
        else
        {
            return Page.reegWizard3Page;
        }
    }

    public PageReference patStep2SavePastMeds()
    {
        if (patient != null && patient.id != null)
        {
            savePastMeds();
            PageReference returnVal = new PageReference( '/apex/patientWizard2Page?id=' + patient.id + '&qsid=' + qsid 
                                                            + '&rid=' + reegId + '&tid=' + tid + '&wr=' + isWR
                                                             + '&np=' +  ApexPages.currentPage().getParameters().get('np'));
            returnVal.setRedirect(true);
            return returnVal;
        }
        else
        {
            return Page.patientWizard2Page;
        }
    }


    public PageReference patStep3()
    {
        SavePatient();
        saveCurrentMeds();
        if (patient != null && patient.id != null)
        {
            PageReference returnVal = new PageReference( '/apex/rEEGWizard2bPage?id=' + patient.id + '&qsid=' + qsid 
                                                            + '&rid=' + reegId + '&tid=' + tid + '&wr=' + isWR
                                                             + '&np=' +  ApexPages.currentPage().getParameters().get('np'));
            returnVal.setRedirect(true);
            return returnVal;
        }
        else
        {
            return Page.patientWizard2Page;
        }
    }
    
    public PageReference patientStep4()
    {
        savePastMeds();
        if (patient != null && patient.id != null)
        {
            PageReference returnVal;

            //-- The test for the string - 'null' must be included. 
            if (reegId != null && reegId != 'null')
                returnVal = new PageReference( '/apex/rEEGWizard3Page?id=' + reegId + '&qsid=' + qsid + '&pid=' 
                                                + patient.id + '&tid=' + tid + '&wr=' + isWR
                                                + '&np=' +  ApexPages.currentPage().getParameters().get('np'));
            else
                returnVal = new PageReference( '/apex/rEEGWizard3Page?qsid=' + qsid + '&pid=' + patient.id 
                                                + '&tid=' + tid + '&wr=' + isWR
                                                + '&np=' +  ApexPages.currentPage().getParameters().get('np'));

            returnVal.setRedirect(true);
            return returnVal;
        }
        else
        {
            return Page.patientWizard1Page;
        }
    }

    public PageReference cancelWizard()
    {
        if (patient != null && patient.id != null)
        {
            String newPat =  ApexPages.currentPage().getParameters().get('np');
            if (ApexPages.currentPage().getParameters().get('np') == '1') delete patient;
        }

        PageReference returnVal = new PageReference('/apex/patientSearchPage');
        returnVal.setRedirect(true);

        return returnVal;
    }

       //---Get a handle back to the patient page
    private PageReference getDestPage()
    {
        PageReference returnVal = new PageReference( '/' + ApexPages.currentPage().getParameters().get('id'));
        returnVal.setRedirect(true);
        return returnVal;
    }

    public void SavePatient()
    {
        if (patient != null && patient.id != null) upsert patient;
        else insert patient;
    }

    //---Cancel action
    public PageReference cancelMedList()
    {
        return getDestPage();
    } 

    public PageReference moreCurrentMeds()
    { 
        saveCurrentMeds();
        PageReference returnVal = new PageReference( '/apex/patientWizard2Page?id=' + patient.id + '&qsid=' + qsid + '&rid=' + reegId + '&tid=' + tid);
        returnVal.setRedirect(true);
        return returnVal;
    }

    public PageReference morePastMeds()
    { 
        savePastMeds();
        PageReference returnVal = new PageReference( '/apex/rEEGWizard2bPage?id=' + patient.id + '&qsid=' + qsid + '&rid=' + reegId + '&tid=' + tid);
        returnVal.setRedirect(true);
        return returnVal;
    }

    //---Show or not show the med list
    public Boolean getShowMedList()
    {
    //    getPatient();
    //    return !patient.no_meds__c;
        return true;
    }

    //---Number of meds in list
    private Integer medCount;
    public Integer getMedCount()
    {
        return medCount;
    }

    //---Total number of displayed rows
    private Integer rowCount = 1;
    public Integer getRowCount()
    {
        return rowCount;
    }

    //---Build the current med list
    private List<PatientMedDisp> currentMeds;
    public List<PatientMedDisp> getCurrentMeds()
    {
         if (currentMeds == null)
         {
             String medStatus = 'Current';
             currentMeds = new List<PatientMedDisp>();  
             PatientMedDisp med;
             Integer ctr = -1;

            if (!patient.no_meds__c)        
            {
            	//--Set current meds to most recent outc meds if exist (don't know if type II yet)
            	patientOutcomeDao outcDao = new patientOutcomeDao();
				List<r_outcome_med__c> previousMeds = outcDao.getExistingOutcomeMeds(ApexPages.currentPage().getParameters().get('id')); 
				if (previousMeds != null) 
				{	                    
	                    for(r_outcome_med__c oMedRow : previousMeds) 
	                    {
							ctr++;	
	                    	med = new PatientMedDisp();
	                    	med.setRowNum( ctr);	                    	

		                    r_patient_med__c hldgMed = new r_patient_med__c();
		                    Date startDate = oMedRow.start_date__c;
		                    hldgMed.start_m_pl__c = startDate.month().format();	                    
		                    hldgMed.start_y_pl__c = startDate.year().format().replace(',','');
		                    hldgMed.dosage__c = oMedRow.dosage__c;
		                    hldgMed.med_name__c = oMedRow.med_name__c;
		                    hldgMed.Unit__c = oMedRow.unit__c;

		                    med.Setup(hldgMed);       
		                    currentMeds.Add(med);
	                    }	                                              	                    
				}
            	else
            	{
	                //---Add the current meds
	                 for(r_patient_med__c medRow : 
	                     [select start_m_pl__c, start_y_pl__c, end_m_pl__c, end_y_pl__c, reason_stop_pl__c, effect_pl__c, dosage__c, drugreview__c, 
	                            generic__c, is_wo_extended__c, med_name__c, no_dosage__c, reason_stop_text__c, side_effects__c, status__c, symtoms__c, Unit__c, id, name 
	                      from r_patient_med__c 
	                      where r_patient_id__c = :ApexPages.currentPage().getParameters().get('id') AND status__c = :medStatus])
	                {
	                    ctr++;
	
	                    med = new PatientMedDisp();
	                    med.setRowNum( ctr);
	                    med.Setup( medRow );                                 
	                    currentMeds.Add( med);
	                }
            	}
            }
            medCount = ctr + 1;  //---ctr is zero based

            //---Create blank rows
            for (Integer i = 0; i < 8; i++)
            {
                ctr++;
                med = new PatientMedDisp();
                med.setRowNum( ctr);

                r_patient_med__c medRow = new r_patient_med__c();
                med.Setup(medRow);                                   
                currentMeds.Add(med);
            }     
            rowCount = ctr + 1;    //---ctr is zero based
        }    

        return currentMeds;
    }

  //---Build the past med list
    private List<PatientMedDisp> pastMeds;
    public List<PatientMedDisp> getPastMeds()
    {
         if (pastMeds == null)
         {
            String medStatus = 'Past';
            pastMeds = new List<PatientMedDisp>();  
            PatientMedDisp med;
            Integer ctr = -1;

            if (!patient.no_meds__c)        
            {
                //---Add the current meds
                 for(r_patient_med__c medRow : 
                     [select start_m_pl__c, start_y_pl__c, end_m_pl__c, end_y_pl__c, reason_stop_pl__c, effect_pl__c, dosage__c, drugreview__c, 
                            generic__c, is_wo_extended__c, med_name__c, no_dosage__c, reason_stop_text__c, side_effects__c, status__c, symtoms__c, Unit__c, id, name 
                         from r_patient_med__c
                         where r_patient_id__c = :ApexPages.currentPage().getParameters().get('id') AND status__c = :medStatus])
                {
                    ctr++;

                    med = new PatientMedDisp();
                    med.setRowNum( ctr);
                    med.Setup( medRow );                                 
                    pastMeds.Add( med);
                }
            }
            medCount = ctr + 1;  //---ctr is zero based

            //---Create blank rows
            for (Integer i = 0; i < 8; i++)
            {
                ctr++;
                med = new PatientMedDisp();
                med.setRowNum( ctr);

                r_patient_med__c medRow = new r_patient_med__c();
                med.Setup(medRow);                                   
                pastMeds.Add(med);
            }     
            rowCount = ctr + 1;    //---ctr is zero based
        }    

        return pastMeds;
    }

    //---Save current meds 
    public Boolean saveCurrentMeds()
    {
        if (currentMeds != null)
        {
                //---Loop through each row
            for(PatientMedDisp medRow : currentMeds)
            {
                if (medRow != null)
                {
                    if (medRow.getObj() == null)
                    {
                       addMessage( 'medRow.getObj() is null');         
                    }
                    else
                    {
                       SaveMed(medRow.getObj(), true);     
                    }
                }
            }
        }

        return true;
    }

    //---Save past meds
    public Boolean savePastMeds()
    {
        if (pastMeds != null)
        {
                //---Loop through each row
            for(PatientMedDisp medRow : pastMeds)
            {
                if (medRow != null)
                {
                    if (medRow.getObj() == null)
                    {
                       addMessage( 'medRow.getObj() is null');         
                    }
                    else
                    {
                       SaveMed(medRow.getObj(), false);     
                    }
                }
            }
        }

        return true;
    }

    //---Save a med row
    private void SaveMed(r_patient_med__c obj, Boolean isCurrentMed)
    {
        // -- med name is not blank
        if (obj != null && obj.med_name__c != null && obj.med_name__c != '')
        {
            obj.status__c = (isCurrentMed) ? 'Current' : 'Past';
            
            ref_drug__c refDrug = getRefDrug(obj.med_name__c);
            obj.generic__c = (refDrug != null) ? refDrug.generic__c : obj.med_name__c;
            
            if (obj.no_dosage__c)
            {
                obj.Unit__c = '';
                obj.dosage__c = null;
                obj.Unknown__c = '';
            }

            if (obj.id != null)
            {
                //---Update previous row
                upsert obj;
            }
            else
            {
                //---New row, insert
                if (patient != null && patient.id != null)
                {
                    obj.r_patient_id__c = patient.id;
                    insert obj;
                }
            }                
        }
        else
        {
                //---If the id is not blank and the name is, then delete the old record
            if (obj.id != null) delete obj;
        }    
    }

    private ref_drug__c getRefDrug(String medName)
    {
        ref_drug__c refDrug;
        for (ref_drug__c rd : [Select id, name, generic__c, trade__c
                          from ref_drug__c where 
                          name =:medName and is_deleted__c = false limit 1]) 
        {
            refDrug = rd;
        }
         
        return refDrug;
    }

    //---Add a message to the message list
    private void addMessage(String value)
    {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.INFO, value);
        ApexPages.addMessage( msg);
    }

        //---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
    	r_reeg__c reeg = rEEGDao.getTestReeg();
    	patientDao patDao = new patientDao();
    	r_patient__c pat = patDao.getById(reeg.r_patient_id__c);
    	ApexPages.currentPage().getParameters().put('id', pat.Id);
    	ApexPages.currentPage().getParameters().put('rid', reeg.Id);

        patientWizardController cont = new patientWizardController();
        cont.setPatient(pat);
        cont.getPatient();
        
        PageReference pRef1 = cont.patStep1();
        PageReference pRef2 = cont.patStep2();
        PageReference pRef2a = cont.patStep2SavePastMeds();
        PageReference pRef3 = cont.patStep3();
        PageReference pRef3a = cont.patientStep4();
        PageReference pRef4 = cont.cancelWizard();
        PageReference pRef6 = cont.cancelMedList();
        PageReference pRef7 = cont.morePastMeds();
        PageReference pRef8 = cont.moreCurrentMeds();

        cont.patient = new r_patient__c();
        cont.patient.dob__c = '12/12/1970';
        cont.patient.first_name__c = 'joe';
    
        Boolean flag = cont.getShowMedList();
        flag = cont.getIsMgr();
        flag = cont.getIsNotMgr();
        flag = cont.getDOBRequired();
        flag = cont.saveCurrentMeds();
        flag = cont.saveCurrentMeds();
        Integer medCount = cont.getMedCount();
        Integer rowCount = cont.getRowCount();
        List<PatientMedDisp> currentmedList = cont.getCurrentMeds();
        List<PatientMedDisp> pastmedList = cont.getPastMeds();

        System.assertEquals( 1, 1);
    }
}