//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: BaseWizardController
//   PURPOSE: Base controller class for the wizard pages/controllers
// 
//     OWNER: CNS Response
//   CREATED: 11/19/10 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
public abstract class BaseWizardController extends BaseController
{
	public r_patient__c patient { get; set; }
	public r_reeg__c reeg { get; set; }
	public r_patient_outc__c outcome { get; set; }
	public boolean isWiz {get; set;}
    public boolean isNew {get; set;}
    public boolean isNewPat {get; set;}
    public boolean isiPad {get; set;}
    public string reegId {get; set;}
    public string patId {get; set;}
    public string reportDocId {get; set;}
    public String qsid {get; set;}
    public String tid {get; set;}
    public String outcomeId {get; set;}
    public boolean isWR {get; set;}
    
    public patientDao pDao { get; set; }
    public rEEGDao rDao { get; set; }
    public patientOutcomeDao outDao { get; set; }
    
    public virtual PageReference cancelWizAction()
    {
    	PageReference returnVal = null;
    	
    	if (isWiz)
    	{
	        if (reeg == null && !e_StringUtil.isNullOrEmpty(reegId))
	        {
	            reeg = [select id, r_patient_id__c from r_reeg__c where id = :reegId];
	        }
	        
	        if (reeg != null)
	        {
	        	if (pDao == null) pDao = new patientDao();
	        	
	            if (patient == null)
	            {
	            	string patIdVal = (!e_StringUtil.isNullOrEmpty(patId)) ? patId : reeg.r_patient_id__c;
	            	patient = pDao.getById(reeg.r_patient_id__c);
	            }
	            
	            if (isNewPat && patient != null)
		            pDao.realDeleteSObject(patient);
		        else
		        {
		        	if (!e_StringUtil.isNullOrEmpty(reeg.Id))
		            RemoveReeg(reeg);
		        }
	        }
	        
	        if (!e_StringUtil.isNullOrEmpty(outcomeId))
	        {
	        	patientOutcomeDao outDao = new patientOutcomeDao();
	    
	        	r_patient_outc__c outc = outDao.getById(outcomeId);
	        	
	        	if (outc != null)
	        	{
					outDao.deleteSObject(outc);        		
	        	}
	        }
	        
	        if (isiPad != null && isiPad)
	      	    returnVal = new PageReference('/apex/iwPatient');
	        else
		        returnVal = new PageReference('/apex/patientSearchPage');
    	}
    	else
    	{
    		if (reeg != null)
    			returnVal = new PageReference('/' + reeg.Id);
    	}
        
        return returnVal;
    }
  
    
          //-- delete reeg and all related objects
    public void RemoveReeg(r_reeg__c reeg)
    {
    	if (reeg != null)
    	{
	        for(r_reeg_wmed__c wmedRow : [select id from r_reeg_wmed__c where r_reeg_id__c = :reeg.id])
	        {
	            delete wmedRow;
	        }
	
	        for(r_reeg_group__c medGrpRow : [select id from r_reeg_group__c where r_reeg_id__c = :reeg.id])
	        {
	            delete medGrpRow;
	        }
	
	        for(r_reeg_med__c medSensRow : [select id from r_reeg_med__c where r_reeg_id__c = :reeg.id])
	        {
	            delete medSensRow;
	        }
	
	        for(r_reeg_dx__c dxRow : [select id from r_reeg_dx__c where r_reeg_id__c = :reeg.id])
	        {
	            delete dxRow;
	        }
	
	        for(r_reeg_var__c cnsVarRow : [select id from r_reeg_var__c where r_reeg_id__c = :reeg.id])
	        {
	            delete cnsVarRow;
	        }
	
	        delete reeg;
    	}
    }
    
    public PageReference gotoSubmitPage()
    {
    	system.debug('### phy_PeerRequisition3S2');
        PageReference pg = new PageReference('/apex/phy_PeerRequisition3S2?id='+patient.Id + '&rid=' + reegId);
        pg.setRedirect(true);
        return pg;
    }
    
    public PageReference gotoEEGUploadPage()
    {
        PageReference pg = new PageReference('/apex/phy_PeerRequisition4S3?id='+patient.Id + '&rid=' + reegId);
        pg.setRedirect(true);
        return pg;
    }
    
    public  PageReference gotoPeerReqStart()
    {
        PageReference pg = new PageReference('/apex/phy_PeerRequisition1S2?id='+patient.Id);
        pg.setRedirect(true);
        return pg;
    }
    
    public PageReference gotoDxPage()
    {
        PageReference pg = new PageReference('/apex/phy_PeerRequisition2S2?id=' + patient.id + '&rid=' + reegId);
        pg.setRedirect(true);
        return pg;  
    }
    
    public virtual PageReference cancelWizard()
    {
    	//TODO-remove reeg
    	RemoveReeg(reeg);
    	return gotoHome();
    }
    
    public PageReference gotoPatientList()
    {
        
        PageReference pg = new PageReference('/apex/phy_PatientListS2');
        pg.setRedirect(true);
        return pg;
    }
    
      
    public PageReference gotoHome()
    {
    	
        PageReference pg = new PageReference('/apex/phy_PeerListS2');
        pg.setRedirect(true);
        return pg;
    }
    
    public PageReference gotoPatient()
    {
    	PageReference pg = new PageReference('/apex/phy_PatientViewS2?id=' + patient.id);
        pg.setRedirect(true);
        return pg;
    }
    
    public PageReference gotoReegView()
    {
    	PageReference pg = new PageReference('/apex/phy_PeerViewS2?id=' + reeg.id);
        pg.setRedirect(true);
        return pg;
    }
    
    public PageReference gotoPatientWithMesg(String mesg)
    {
        PageReference pg = new PageReference('/apex/phy_PatientViewS2?id=' + patient.id + '&mesg=' + mesg);
        pg.setRedirect(true);
        return pg;
    }
    
}