Public Class reegDisp
{
	public reegDisp()
	{
	}
	public reegDisp(r_reeg__c o)
	{
		obj = o;
	}

    private r_reeg__c obj;
    public r_reeg__c getObj()
    {
        return obj;
    }
    public void setObj(r_reeg__c s)
    {
        obj = s;
    }

    public void Setup(r_reeg__c o)
    {
        obj = o;
    } 
    
    public boolean showPEERReport
    {
        get { return (obj != null && !e_StringUtil.isNullOrEmpty(obj.neuroguide_status__c) && (obj.neuroguide_status__c == 'Report Complete' || obj.rpt_stat__c == 'Complete' || obj.rpt_stat__c == 'NG Complete' || obj.rpt_stat__c == 'Prelim - No Neuro Commentary' || obj.rpt_stat__c == 'Delivered' ) );}
    } 
    
    public boolean showPEERReportNoClassic
    {
    	get { return (obj != null && rEEGUtil.showOnlineReportNoClassic(obj));}
    }
    
    public boolean isPEER2
    {
    	get { return (obj != null && (reegUtil.isPO2Report(obj) || reegUtil.isStudyReport(obj)));}
    }  

    public boolean isWR
    {
        get { return (obj != null && reegUtil.isWRMilitaryStudyReport(obj)); }
    }  
    

    //------------TEST-----------------------------
    public static testMethod void testDisp()
    {
        r_reeg__c obj = rEEGDao.getTestReeg();
        obj.req_stat__c = 'Complete';
        obj.neuroguide_status__c = 'Report Complete';

        reegDisp disp = new reegDisp();
        disp.setObj(obj);
        disp.Setup( obj);
        boolean result = disp.isPEER2;
        result = disp.showPEERReportNoClassic;
        result = disp.showPEERReport;
        result = disp.isWR;

        r_reeg__c obj2 = disp.getObj();

        System.assertEquals( obj, obj2);
    }
}