//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: AccountDao
// PURPOSE: Data access class for the Account object
// CREATED: 10/22/13 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class AccountDao extends BaseDao 
{
	private static String NAME = 'Account';    
	//-- TODO - there is a field names 'Jigsaw' on the account object that will not allow the field describe to work. this needs to be solved in the future.
	private static String fldList = 'Id, Name, Study_Group__c, Is_Commercial__c';
		
	public static String getFieldStr()
	{
		//if (e_StringUtil.isNullOrEmpty(fldList)) 
		//{
		//	e_SysTableDao dao = new e_SysTableDao();
		//	e_SysTable__c obj = dao.getByName(NAME);
			
		//	if (obj == null)
		//	{
		//		fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.Account.fields.getMap());
		//	}
		//	else
		//	{
		//		fldList = obj.field_list__c;
		//	}
		//}
		
		return fldList;
	}
	
	public Account getById(String idInp)
    {
    	string fieldStr = getFieldStr();
		return (Account)getSObjectById(fieldStr, NAME, idInp);
    } 
    
    public List<Account> getByName(String acctName)
    {
   		String whereStr = 'Name = \'' + acctName + '\' ';
		return (List<Account>)getSObjectListByWhere(getFieldStr(), NAME, whereStr);
    } 

    public Account getByContactId(String idInp)
    {
    	string fieldStr = getFieldStr();
		return (Account)getSObjectById(fieldStr, NAME, idInp);
    } 

    public Account getAccountForCurrentUser()
    {
    	Account returnObj = null;

    	UserDao uDao = new UserDao();
    	User phyUser = uDao.getUserContactID(UserInfo.getUserId());

    	if (phyUser != null && String.isNotBlank(phyUser.ContactId)) 
    	{
    		List<Contact> conList = [select Id,AccountId FROM Contact where Id =:phyUser.ContactId LIMIT 1];

	        if (conList != null && !conList.isEmpty() && String.isNotBlank(conList[0].AccountId))
	        {
				returnObj = getById(conList[0].AccountId);
			}
		}

		return returnObj;
    } 
    

    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testAccountDao()
    {
    	AccountDao dao = new AccountDao();
    	Account acct = AccountDao.getTestAccount();
    	acct = dao.getById(acct.Id);
    	
    	List<Account> acctList = dao.getByName('test account');

    	Contact con = ContactDao.getTestPhysicianPO2();
    	acct = dao.getById(con.AccountId);

    	Account testAcct = dao.getAccountForCurrentUser();
    }
    
    public static Account getTestAccount()
    {
    	Account a = new Account(name = 'test account');
        insert a;
    	return a;
    }
}