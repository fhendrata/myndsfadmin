global class bat_PhyRpt implements Database.Batchable<sObject>
{
   	global final String Query;
   	global final integer month;
   	global final integer year;

   	global bat_PhyRpt(String q, integer m, integer y)
   	{
   		Query = q; month = m; year = y; 
   	}
	
   	global Database.QueryLocator start(Database.BatchableContext BC)
	{
    	return Database.getQueryLocator(Query);
   	}

 	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
   		date todaysDate = date.today();
   		if (todaysDate.day() == 1)
   		{
	   		if (month != null && month > 0 && month <= 12)
			{
				if (year != null && year <= DateTime.now().year() && year > 2006)
				{
					DateTime selDt = DateTime.newInstance(year, month, 1);
					rpt_PhyProcessController cont = new rpt_PhyProcessController();
					for(Sobject s : scope)
		      		{
		      			cont.buildRpt(s, selDt);
		      		}
				}
			}
   		}
   	}
 
   	global void finish(Database.BatchableContext BC)
   	{
   		if (month != null && month > 0 && month <= 12)
		{
			if (year != null && year <= DateTime.now().year() && year > 2006)
			{
				DateTime selDt = DateTime.newInstance(year, month, 1);
				rpt_PhyProcessController cont = new rpt_PhyProcessController();
				cont.processRpts(selDt);
			}
		}
   	}
}