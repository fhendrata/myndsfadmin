public with sharing class TestController extends BaseController
{	
	public string testInputTb {get; set;}
	public string testsessionTb {get; set;}
	public string resultStr {get; set;}
	
	public String apiSessionId {get;set;} 
 	public String apiServerURL {get;set;} 
 	
 	public String TimeZone {get;set;} 
 	public String Locale {get;set;} 
 	public String EmailEncoding {get;set;} 
 	public String Language {get;set;} 
 	public String Username {get;set;} 
 	public String Lastname {get;set;} 
 	public String Email {get;set;} 
 	public String Alias {get;set;} 
 	public String Nicknm {get;set;}
 	public String ProfileId {get;set;}
 	public String PsName {get;set;} 
 
	
	public PageReference loadAction()
	{		
		testInputTb = 'a0hR0000002jpqSIAQ';
		
		app_keys__c appKey = app_keys__c.getInstance();
		if(appKey != null) resultStr = appKey.web_service_key__c;
		UserDao uDao = new UserDao();
		User currentUser = uDao.getById(UserInfo.getUserId());
		
		TimeZone = currentUser.TimeZoneSidKey;
		Locale = currentUser.LocaleSidKey;
		EmailEncoding = currentUser.EmailEncodingKey;
		Language = currentUser.LanguageLocaleKey;
		
		return null;
	}
	
	public PageReference parseXlsx()
	{
		Attachment[] attArray = [Select a.Name, a.Id, a.ContentType, a.BodyLength, a.Body From Attachment a where a.ParentId = '003R000000LLxUy'];
		
		System.debug(attArray);
		
		if (attArray != null && attArray.size() > 0)
		{
			System.debug(attArray[0].body);
			
			
		}
		
		return null;
	}
	
	public PageReference setUserAction()
	{
		ContactDao cDao = new ContactDao();
		Contact contact = cDao.getById('003R000000LN6Oh');
		
		if (contact != null)
		{
			User u = new User();
			u.LastName = contact.LastName;
			u.Alias = 'tAlias';
			u.Username = 'test123456@ethos.com';
			u.CommunityNickname = 'test123456';
			
		//	u.UserType = 'CustomerSuccess';
			u.ProfileId = '00e60000000v6Iw';
			u.Email = 'apex@ethos.com';
			u.TimeZoneSidKey = TimeZone;
			u.LocaleSidKey = Locale;
			u.EmailEncodingKey = EmailEncoding;
			u.LanguageLocaleKey = Language;
			
			UserDao uDao = new UserDao();
			uDao.saveSObject(u);
			
		}
		
		return null;
	}
	
	 public PageReference doLogin()
	 { 
		 System.debug('apiSessionId: ' + apiSessionId); 
		 System.debug('apiServerURL: ' + apiServerURL); 
		 return null; 
	 }
	 
	public PageReference testPsAction()
	{		
		return null;
	}		
		
	public PageReference testWsAction()
	{		
		ActionDao.insertReegAction('a0hR0000002jsoHIAQ', 'TEST');
		
		return null;
	}	
	
	public PageReference buildIntervals()
    {
      //  patientIntervalBuilder intBuilder = new patientIntervalBuilder();
      //  string patientId = 'a0bR0000001LZ31';
      //  intBuilder.buildAllIntervalsForPat(patientId);
        
        return null;
    }
    
    public List<rpt_physician__c> getPhyList()
    {
    	//rpt_PhyDao phyDao = new rpt_PhyDao();
		//return phyDao.getAll();
		
		QsDao qDao = new QsDao();
		qDao.getLastQSFiles();
		return null;
    }
    
    public List<r_qs_file__c> getQsList()
    {
		QsDao qDao = new QsDao();
		return qDao.getLastQSFiles();
    }
    /*
    public PageReference RunCalc()
    {
    	ContactDao conDao = new ContactDao();
		List<Contact> phyList = conDao.getAll();

		if (phyList != null && phyList.size() > 0)
		{
			for(Contact c : phyList)
			{
		    	rpt_PhyProcessController cont = new rpt_PhyProcessController();
//		    	cont.processPhy(c, DateTime.now());
			}
		}
    	return null; 
    }
    
    public PageReference delAllRpt()
    {
     	rpt_PhyDao phyDao = new rpt_PhyDao();
		List<rpt_physician__c> phyList = phyDao.getAll();
     	phyDao.realDeleteSObjectList(phyList);
        
        return null;
    }
    
    public PageReference ActionWsTest()
    {
     	ActionDao.insertReegAction(testInputTb, 'REPORT_FINALIZE', '00530000000kqaDAAQ');
        
        return null;
    }
    
    public PageReference ActionWsUpdateTest()
    {
     	ActionDao.insertReegAction(testInputTb, 'UPDATE_DB_STATUS', '00530000000kqaDAAQ');
        
        return null;
    }
    */
    private static testmethod void testController()
    {
        TestController t = new TestController();
        t.doLogin();
        t.loadAction();
        t.buildIntervals();
        t.getQsList();
        t.getPhyList();
        t.setUserAction();
    }
    
}