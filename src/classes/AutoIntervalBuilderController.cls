//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: AutoIntervalBuilderController
// PURPOSE: Controller class for the autoIntervalBuilder page
// CREATED: 1/22/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
public class AutoIntervalBuilderController 
{
	public AutoIntervalBuilderController() { }
    
    public PageReference loadAction()
    {
   		String patId = ApexPages.currentPage().getParameters().get('patId');	
   		String url = ApexPages.currentPage().getParameters().get('returl');	
   		
        GlobalPatientDao patDao = new GlobalPatientDao();
        r_patient__c pat = patDao.getById(patId);
        
        if (pat != null)
        {
            patientIntervalBuilder intBuilder = new patientIntervalBuilder();
            intBuilder.buildAllIntervalsForPat(pat.Id, pat.OwnerId);
        }  

    	return new PageReference(url);
    }
}