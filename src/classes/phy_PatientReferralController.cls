public class phy_PatientReferralController extends BaseController 
{
	public Patient_Referral__c patientReferral {get;set;}
	public Contact physician {get;set;}
	public ContactDao cDao = new ContactDao();
	public patientReferralDao prDao = patientReferralDao.getInstance();
	
	public phy_PatientReferralController(ApexPages.StandardController stdController) 
	{
		this.patientReferral = patientReferralDao.getInstance().getById(stdController.getId());	
		this.physician = new Contact();
		if(patientReferral.Referred_To__c != null) 
		{
			this.physician = cDao.getById(patientReferral.Referred_To__c);
		}
	}

	public PageReference convertToPatient() 
	{
		String patFName = (String.isNotBlank(patientReferral.Patient_First_Name__c)) ? patientReferral.Patient_First_Name__c : '';
		String patLName = (String.isNotBlank(patientReferral.Patient_Last_Name__c)) ? patientReferral.Patient_Last_Name__c : '';
		PageReference pr = new PageReference('/apex/PeerRequisition?fn=' + patFName + '&ln=' + patLName + '&prId=' + patientReferral.Id + '&prPhy=' + patientReferral.Referred_To__c);
		pr.setRedirect(true);
		return pr;
	}

	public PageReference edit() 
	{
		PageReference pr = new PageReference('/apex/phy_PatientReferralEditS2?id='+patientReferral.Id);
		pr.setRedirect(true);
		return pr;
	}

	public PageReference save() 
	{
		PageReference pr = new PageReference('/apex/phy_PatientReferralViewS2?id='+patientReferral.Id);
		pr.setRedirect(true);
		patientReferral.Is_Updated__c = true;
		prDao.saveSObject(patientReferral);
		return pr;
	}

	public PageReference cancel() 
	{
		return new PageReference('/apex/phy_PatientReferralViewS2?id='+patientReferral.Id);
	}
}