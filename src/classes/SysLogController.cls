Public Class SysLogController extends BaseController
{   
	public List<sys_log__c> logs {get; set;}
	
    public PageReference LoadAction()
    {
    	SysLogDao logDao = new SysLogDao();
    	LogDao.IsTestCase = this.IsTestCase;
    	logs = logDao.getRecentLogs();
    	return null;
    }

    public PageReference refresh()
    {
        logs = null;
        return getThisPage();
    }

    private PageReference getThisPage()
    {
        PageReference returnVal = new PageReference( '/apex/SysLogPage');
        returnVal.setRedirect(true);
        return returnVal;
    }

    //---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
        SysLogController cont = new SysLogController();
        cont.IsTestCase = true;
        cont.LoadAction();
        List<sys_log__c> logs = cont.logs;

        PageReference pRef1 = cont.refresh();

        System.assertEquals( 1, 1);
    }
}