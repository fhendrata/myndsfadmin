//--------------------------------------------------------------------------------
// COMPONENT: Genetic Testing
// CLASS: GeneticTriggerHandler
// PURPOSE: Trigger handler class for the Genetic__c object.
// CREATED: 02/14/18 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
public with sharing class GeneticTriggerHandler 
{
	private static Integer MAX_CALLOUTS_ALLOWED = 10; 
    public static Boolean runTrigger = true;

    public static void handleGeneticInsert (List<Genetic__c> genRecordList) 
    {
        if (!runTrigger || (!TriggerSettings.isTriggerActive('Genetic__c'))) 
            return;

        system.debug('handleGeneticInsert:genRecordList: ' + genRecordList);
        
        if (!genRecordList.isEmpty() && genRecordList.size() < MAX_CALLOUTS_ALLOWED)
        {
        	for (Genetic__c gen : genRecordList)
            {
                system.debug('handleGeneticInsert:gen.Id: ' + gen.Id);
                
        		ActionDao.insertReegActionPEER(gen.Id, 'INSERT_GENETIC_RECORD', '');
            }
        }
        else
        {
        	ActionDao.insertReegActionPEER('', 'UPDATE_GENETIC_DB', '');
        }

    }

    public static void handleGeneticUpdate (List<Genetic__c> genRecordList) 
    {
        if (!runTrigger || (!TriggerSettings.isTriggerActive('Genetic__c'))) 
            return;
        
		if (!genRecordList.isEmpty() && genRecordList.size() < MAX_CALLOUTS_ALLOWED)
        {
        	for (Genetic__c gen : genRecordList)
        		ActionDao.insertReegActionPEER(gen.Id, 'UPDATE_GENETIC_RECORD', '');
        }
        else
        {
        	ActionDao.insertReegActionPEER('', 'UPDATE_GENETIC_DB', '');
        }

    }

    public static void handleGeneticDelete (Map<Id, Genetic__c> genRecordMap) 
    {
        if (!runTrigger || (!TriggerSettings.isTriggerActive('Genetic__c'))) 
            return;
        
		if (!genRecordMap.isEmpty() && genRecordMap.size() < MAX_CALLOUTS_ALLOWED)
        {
        	for (Genetic__c gen : genRecordMap.values())
        		ActionDao.insertReegActionPEER(gen.Id, 'DELETE_GENETIC_RECORD', '');
        }
        else
        {
        	ActionDao.insertReegActionPEER('', 'UPDATE_GENETIC_DB', '');
        }

    }
}