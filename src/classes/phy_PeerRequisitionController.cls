public with sharing class phy_PeerRequisitionController extends BaseWizardController{
	
	private List<rEEGDxDisp> dxs;
    private List<rEEGDxDisp> dxObjs;
    private List<rEEGDxGroupDisp> dxGroups;
    private List<rEEGDxGroupDisp> dxGroupsL;
    private List<rEEGDxGroupDisp> dxGroupsR;
    private Boolean newPatBtnVisible;
    private patientDao pDao {get; set;}
    private rEEGDao rDao {get; set;}
    private patientReferralDao prDao {get;set;}
    
 
    public List<r_patient__c> patAllList {get; set;}
    public List<Patient_Referral__c> patRefAllList {get;set;}
    public boolean showResults {get; set;}
    public Boolean showRefResults {get;set;}
    public boolean isFirstSearchDone {get; set;}
    

    //---Constructor
    public phy_PeerRequisitionController()
    {   
    	reegId = ApexPages.currentPage().getParameters().get('rid'); 
    	patId = ApexPages.currentPage().getParameters().get('id'); 
    	
    	pDao = new patientDao();
    	rDao = new rEEGDao();
        prDao = patientReferralDao.getInstance();
    	if(e_StringUtil.isNullOrEmpty(patId))
    	{
            patient = new r_patient__c();
            //-- if coming from the search page there will be search parameters
            patient.Name = ApexPages.currentPage().getParameters().get('ln'); 
            patient.first_name__c = ApexPages.currentPage().getParameters().get('fn'); 
            //-- if there are search params the first search is done
            isFirstSearchDone = (!e_StringUtil.isNullOrEmpty(patient.Name) && !e_StringUtil.isNullOrEmpty(patient.first_name__c));
    	}
    	else
    	{
    		patient = pDao.getById(patId);
    		isFirstSearchDone = true;
    	}
    }
    
    public PageReference loadAction()
    {
    	try {
    		if (reeg == null)
    		{
    			if(!e_StringUtil.isNullOrEmpty(reegId))
    				reeg = rDao.getById(reegId);
    		}
    		
            if(reeg.eeg_rec_stat__c == 'Received') return gotoPatient();
    	
    	}catch(Exception e){}
    	
    	try
    	{
	        List<r_reeg__c> reegList = [select Id from r_reeg__c where r_patient_id__c =: patient.Id and CreatedDate = TODAY];
	        if(reegList.size() > 0)
	        {
	            addMessage(ApexPages.Severity.Warning,'A report was already ordered for this patient today! Click<a style="font-size: 100%; margin-left: 5px; margin-right: 5px; font-weight:bold; color: blue;" href="/apex/phy_PatientViewS2?id=' + patient.Id + '">here</a>to go to the patient page.');
	        }
    	}
    	catch(Exception e)
    	{
    		//do nothing for now
    	}
        
    	return null;
    }
  
    public Boolean getIsNewPatBtnVisible()
    {
        return newPatBtnVisible;
    }

    private List<r_patient__c> getMatchingPatients()
    {
        patAllList = new List<r_patient__c>();
        patRefAllList = new List<Patient_Referral__c>();
        Boolean fnameOk = !e_StringUtil.isNullOrEmpty(patient.first_name__c);
        Boolean lnameOk = !e_StringUtil.isNullOrEmpty(patient.Name);
       
        if (fnameOk || lnameOk) 
        {
            patAllList = pDao.getAllSearchResults(patient.Name, patient.first_name__c,'');
            patRefAllList = prDao.getAllSearchResults(patient.Name , patient.first_name__c);
        }
        
        return patAllList;
    }
    
    
    public PageReference patientSearch()
    {
    	if(patient.Id != null) return gotoDxPage();
    	
        isFirstSearchDone = true;
        patAllList = getMatchingPatients();
        showResults = (patAllList != null && patAllList.size() > 0);
        showRefResults = (patRefAllList != null && patRefAllList.size() > 0);
        
        if(!showResults && !showRefResults)
        {
        	//-- this patient was not found so navigate to the patient edit page with the first and last name
        	PageReference pg = new PageReference('/apex/phy_PeerRequisition1S2?fn=' + patient.first_name__c + '&ln=' + patient.Name);
    		pg.setRedirect(true);
    		return pg;
    		
    		//addMessage(ApexPages.Severity.Info,'No patients found. To add a new patient enter patient information and click New Patient');
        }
        
        if(showResults && patAllList.size() > 1) addMessage(ApexPages.Severity.Warning,'Multiple patients found matching search criteria. Select patient from below or fill out the form and click New Patient.');

        return null;
    }
    
    public PageReference newSearch()
    {
    	
    	PageReference pg = new PageReference('/apex/phy_PatientSearch');
    	pg.setRedirect(true);
    	return pg;
    	
    }
    
    public PageReference newPatient()
    {
    	if(e_StringUtil.isNullOrEmpty(patient.dob__c))
    	{
    		addMessage(ApexPages.Severity.Error,'Date of Birth is required for new patients');
    		return null;
    	}
/*
        if(e_StringUtil.isNullOrEmpty(patient.physician__c))
        {
            addMessage(ApexPages.Severity.Error,'Physician must be selected');
            return null;
        }
*/  	
        if (getIsClinicAdminOrAdmin() && !getIsClinicMoreThanOnePhy())
        {
            List<Contact> contactList = getContactListForAccount();
            if (contactList != null && contactList.size() > 0)
                patient.physician__c = contactList.get(0).Id;
        }
        String patientRefId = ApexPages.currentPage().getParameters().get('prId');
        if(patientRefId != null && patientRefId != '' ) {
            Patient_Referral__c convPatRef = prDao.getById(patientRefId);
            patient.Patient_Referral__c = convPatRef.Id;
            convPatRef.Physician_Referral_Status__c = 'Accepted';
            prDao.saveSObject(convPatRef);
        }
        
        if (AddPatient())
        {
            UserDao useDao = new UserDao();
            User currentUser = useDao.getCurrentUser();
            if (currentUser == null || String.isBlank(currentUser.Contact.PEER_Ownership__c) || !(currentUser.Contact.PEER_Ownership__c.equalsIgnoreCase('Self')) )
            {
            	//-- if the user is an admin the phy field will be set by the admin and the owner of the record must be set to the phy.
            	if (String.isNotBlank(patient.physician__c))
            	{
                    User phy = useDao.getUserIDByContactID(patient.physician__c);
                    patient.OwnerId = phy.Id;

    				pDao.saveSObject(patient);
            	}
            }
        	return gotoDxPage();
        }
        //jswenski.1/23/2012 -- if patient creation fails, display at minimum the error below
        //including any validation errors that happen on the page.
        else {
        	
        	addMessage(ApexPages.Severity.Error,'Error: Could not create new patient.');
            return null;
        	
        }
    }
    
    public boolean getIsPhysician()
    {
    	return getIsPortalUser2() || getIsPortalUser1_1() || getIsPortalUser1() || getIsPhyTech();
    }

    public boolean getIsClinicAdminOrAdmin()
    {
        return getIsPhyTech() || getIsMgrLevel3() || getIsPortalUserMgr();
    }

    private Boolean AddPatient()
    {    
       return pDao.saveSObject(patient);
    }

 	public PageReference save()
    {
        //jswenski 6/9/2011 - need to get this when we're post QS
        this.reegId = ApexPages.currentPage().getParameters().get('rid');


        rEEGDao reegDao = new rEEGDao();
        r_reeg__c reeg = reegDao.getById(reegId);

        if (reeg != null)
        {
        	reeg.is_ng_only__c = true;
            reeg.stat_dx_gen__c = saveDx();
            reegDao.Save(reeg, '');
        }

        PageReference pg = new PageReference('/apex/wr_PeerSummaryS2?rid=' + this.reegId);
        pg.setRedirect(true);
        return pg;

        //return getDestPage();
    }

    public PageReference cancel()
    {
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        PageReference pg = new PageReference('/apex/wr_PeerSummaryS2?rid=' + this.reegId);
        pg.setRedirect(true);
        return pg;
    }

    public PageReference gotoSubmitPageExt()
    {
    	if(e_StringUtil.isNullOrEmpty(reegId) || reeg == null)
    	{
	    	reeg = new r_reeg__c();
	    	reeg.r_patient_id__c = patient.Id;
	    	reeg.test_data_flag__c = patient.test_data_flag__c;
	    	
	        rDao.Save(reeg, '');
	        reegId = reeg.Id;
    	}
    	
    	system.debug('### reeg: ' + reeg);
    	reeg.stat_dx_gen__c = saveDx();
    	rDao.Save(reeg, '');
    	
    	return gotoSubmitPage();
    }
    
    private void saveDxGroup(List<rEEGDxGroupDisp> groupList)
    {
        if (groupList!= null)
        {
            for(rEEGDxGroupDisp grpRow : groupList)
            {
                for(rEEGDxDisp dxRow : grpRow.getDxs())
                {   
                    if (dxRow == null)
                    {
                        system.debug('DxRow is null');         
                    }
                    else if (dxRow.getObj() == null)
                    {
                        system.debug('DxRow.getObj() is null');         
                    }
                    else
                    {
                        SaveDx(dxRow.getObj(), dxRow.getRefObj());
                    }
                }
            }
        }
    }
    
    public void SaveDx( r_reeg_dx__c obj, ref_dx__c refObj)
    {
        Boolean hasValue = (obj.primary__c || obj.secondary__c || obj.rule_out__c);

        if (hasValue)
        {
        	obj.OwnerId = UserInfo.getUserId();
        	
             if (obj.id != null)
             {
                upsert obj;
             }
             else
             {
             	obj.Reference_Dx__c = refObj.Id;
                 obj.r_reeg_id__c = reegId;
                 obj.r_ref_dx_id__c = refObj.id; 
                 obj.dx_name__c = refObj.description__c; 
                 obj.name = refObj.name; 

                 insert obj;
             }                
        }
        else
        {
            if (obj.id != null) delete obj;
        }    
    }
    
    
    public List<rEEGDxDisp> getDxs()
    {
        if (dxs== null)
        {
            loadFullList();
            loadSavedList();                
        }    
        return dxs;
    }
    
    public List<rEEGDxGroupDisp> getDxGroupsL()
    {
        getDxGroups();
        return dxGroupsL;
    }
    public List<rEEGDxGroupDisp> getDxGroupsR()
    {
        getDxGroups();
        return dxGroupsR;
    }
    
    public List<rEEGDxGroupDisp> getDxGroups()
    {
        if (dxGroups== null)
        {
            getDxs();
            loadGroupList();
        }    
        return dxGroups;
    }
    
    private void loadFullList()
    {  
        dxs = new List<rEEGDxDisp>();
    
        for(ref_dx__c refRow : 
                 [select description__c, is_group__c, primary__c, rule_out__c, secondary__c, sequence__c, id, name 
                     from ref_dx__c
                     order by sequence__c])
        {
            rEEGDxDisp dx = new rEEGDxDisp();
            dx.SetupRef( refRow );     
            dx.Setup(new r_reeg_dx__c());                                        
            dxs.Add( dx);
        } 
    }
    
   private void loadSavedList()
    {
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            for(r_reeg_dx__c dxRow : 
                 [select dx_name__c, primary__c, r_ref_dx_id__c, rule_out__c, secondary__c, id, name 
                     from r_reeg_dx__c
                     where r_reeg_id__c = :reegId limit 5])
            {
                AddSavedToList(dxRow);
            }
        } 
    }
    
    private void AddSavedToList(r_reeg_dx__c o)
    {
        for(rEEGDxDisp dxRow : dxs)
        {
            if (dxRow.getRefObj() != null && dxRow.getRefObj().id == o.r_ref_dx_id__c)
            {
                dxRow.Setup(o);
            }
        }
    }
    
    private void loadGroupList()
    {
        dxGroups = new List<rEEGDxGroupDisp>();
        
        rEEGDxGroupDisp currGroup = null;
        Integer lastGroupId = 0;
        Integer totalItemCount = 0;
                
        for(rEEGDxDisp dxRow : dxs)
        {
            if (dxRow.getRefObj().is_group__c)
            {
                currGroup = new rEEGDxGroupDisp();
                currGroup.Setup( dxRow.getObj());
                currGroup.SetupRef( dxRow.getRefObj());
                
                dxGroups.Add(currGroup);
            }
            else
            {
                if (currGroup != null) 
                {
                    totalItemCount++;
                    currGroup.addItem( dxRow);
                }
            }
        }
               
        dxGroupsL = new List<rEEGDxGroupDisp>();
        dxGroupsR = new List<rEEGDxGroupDisp>();
 
        Integer addedItemCount = 0;
        
        for(rEEGDxGroupDisp groupRow : dxGroups)
        {
            if (addedItemCount > (totalItemCount/2))
            {
                dxGroupsR.Add(groupRow);    
            }
            else
            {
                dxGroupsL.Add(groupRow);
                if (groupRow != null && groupRow.getItemCount() != null)
                	addedItemCount += groupRow.getItemCount();
                else
                	addedItemCount = (addedItemCount == null) ? 0 : addedItemCount;
            }
        }       
    }  
    
    public Boolean saveDx()
    {          
        saveDxGroup( dxGroupsL );
        saveDxGroup( dxGroupsR );

        return true;
    } 
    
    
    
    
    //---Add a message to the message list
	 public void addMessage(ApexPages.Severity s, String value)
	 {   
	    String message = e_StringUtil.isNullOrEmpty(value) ? '' : value; 
	    ApexPages.Message msg = new ApexPages.Message(s, message);
	    ApexPages.addMessage(msg);
	 }
	
	
	//------------ TEST METHODS ---------------------------
	
	private static testmethod void testController()
	{
	    phy_PeerRequisitionController prc = new phy_PeerRequisitionController();
	    prc.addMessage(ApexPages.Severity.CONFIRM, 'message');
	    
	    prc.getCurrentPhysician();
	    prc.getCurrentAccount();
        prc.getIsClinicMoreThanOnePhy();
	    prc.newPatient();
	    prc.saveDx();
	    prc.cancelWizard();

	    prc.gotoHome();
	    prc.gotoPatient();
	    prc.gotoPeerReqStart();
	}
	  
	  
	private static testmethod void testController2()
    {
    
        phy_PeerRequisitionController prc = new phy_PeerRequisitionController(); 
        prc.getMatchingPatients();
        prc.patientSearch();
        prc.loadFullList();
        
        prc.getDxGroupsL();
        prc.getDxGroupsR();
        prc.getDxGroups();
        prc.loadGroupList();
        prc.loadSavedList();
    }
}