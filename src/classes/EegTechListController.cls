//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: EegTechListController
//   PURPOSE: EEG Tech rEEG list page controller
// 
//     OWNER: CNSR
//   CREATED: 10/27/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class EegTechListController extends BaseController
{
	public PEERRequisitionPager pager {get; set;} 	
	public Boolean ShowResults {get; set;} 
	public string listCategory {get; set;} 

    public rEEGPager reegPager {get; set;}  
    public Boolean reegShowResults {get; set;} 
    public string reegListCategory {get; set;} 
    
    private rEEGDao reegDao;
    
	public EegTechListController() 
	{
		pager = new PEERRequisitionPager();
        reegPager = new rEEGPager();
		IstestCase = false;
	}
	
	public PageReference setupAction()
	{
		listCategory = 'In Progress';
        reegListCategory = 'In Progress';
		if (!IstestCase)
		{
			if (getIsClinicAdmin())
			{
				pager.TechId = 'clinicAdmin';
                reegPager.techId = 'clinicAdmin';
				string pName = getProfileName();
				if (String.isNotBlank(pName) && pName.startswith('WR'))
				{
					pager.AccountName = 'Walter Reed Army Medical Center';
                    reegPager.AccountName = 'Walter Reed Army Medical Center';
				}
			}
			else
			{
				UserDao uDao = new UserDao();
				User usr = uDao.getById(UserInfo.getUserId());
				
				if (usr != null && usr.ContactId != null)
                {
					pager.TechId = usr.ContactId;
                    reegPager.TechId = usr.ContactId;
                }
			}
		}
		
		ShowResults = true;
        
		pager.IsTestCase = IstestCase;
		pager.ListCategory = listCategory;
		pager.CurrSort = 'CreatedDate DESC NULLS last';
		pager.loadData();

        reegShowResults = true;
        reegPager.IsTestCase = IstestCase;
        reegPager.ListCategory = reegListCategory;
        reegPager.CurrSort = 'CreatedDate DESC NULLS last';
        reegPager.loadData();
				
		return null;
	}

	public PageReference filterBtnAction()
	{
		pager.IsTestCase = IstestCase;
        pager.ListCategory = listCategory;
        pager.loadData();

		return null;
	}

    public PageReference reegFilterBtnAction()
    {
        reegPager.IsTestCase = IstestCase;
        reegPager.ListCategory = reegListCategory;
        reegPager.loadData();

        return null;
    }
	
	public PageReference sortName()
    {
        pager.sortList('Name');
        pager.IsTestCase = IstestCase;
        pager.ListCategory = listCategory;
        pager.loadData();

        reegPager.sortList('Name');
        reegPager.IsTestCase = IstestCase;
        reegPager.ListCategory = reegListCategory;
        reegPager.loadData();
        return null;
    }
    
    public PageReference sortType()
    {
        pager.sortList('reeg_reeg_type__c');
        pager.IsTestCase = IstestCase;
        pager.ListCategory = listCategory;
        pager.loadData();

        reegPager.sortList('reeg_type__c');
        reegPager.IsTestCase = IstestCase;
        reegPager.ListCategory = reegListCategory;
        reegPager.loadData();
        return null;
    }
    
    public PageReference sortPatName()
    {
        pager.sortList('patient_name__c');
        pager.IsTestCase = IstestCase;
        pager.ListCategory = listCategory;
        pager.loadData();

        reegPager.sortList('r_patient_id__r.Name');
        reegPager.IsTestCase = IstestCase;
        reegPager.ListCategory = reegListCategory;
        reegPager.loadData();
        return null;
    }

    public PageReference sortReqDate()
    {
        reegPager.sortList('req_date__c');
        reegPager.IsTestCase = IstestCase;
        reegPager.listCategory = reegListCategory;
        reegPager.loadData();
        return null;
    }

    public PageReference sortRptTestDate()
    {
        reegPager.sortList('rpt_test_date__c');
        reegPager.IsTestCase = IstestCase;
        reegPager.listCategory = reegListCategory;
        reegPager.loadData();
        return null;
    }
    
    public PageReference sortAntTestDate()
    {
        pager.sortList('patient_anticipated_eeg_test__c');
        pager.IsTestCase = IstestCase;
        pager.ListCategory = listCategory;
        pager.loadData();

        reegPager.sortList('r_patient_id__r.anticipated_eeg_test__c');
        reegPager.IsTestCase = IstestCase;
        reegPager.ListCategory = reegListCategory;
        reegPager.loadData();
        return null;
    }
    
    public PageReference sortEEGUploadStatus()
    {
        pager.sortList('reeg_eeg_rec_stat__c');
        pager.IsTestCase = IstestCase;
        pager.ListCategory = listCategory;
        pager.loadData();

        reegPager.sortList('eeg_rec_stat__c');
        reegPager.IsTestCase = IstestCase;
        reegPager.ListCategory = reegListCategory;
        reegPager.loadData();
        return null;
    }
    
    public PageReference sortStatus()
    {
        pager.sortList('Status__c');
        pager.IsTestCase = IstestCase;
        pager.ListCategory = listCategory;
        pager.loadData();

        reegPager.sortList('req_stat__c');
        reegPager.IsTestCase = IstestCase;
        reegPager.ListCategory = reegListCategory;
        reegPager.loadData();
        return null;
    }

	 //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---  
    //-----------------------------------------------------------------------
    public static testMethod void test()
    {
    	EegTechListController obj = new EegTechListController();
    	obj.IsTestCase = true;
    	
    	obj.pager = new PEERRequisitionPager(); 	
		obj.ShowResults = true;
			
    	PageReference pr = obj.setupAction();	
    	pr = obj.sortName();	
    	pr = obj.sortType();	
    	pr = obj.sortStatus();	
    	pr = obj.sortEEGUploadStatus();	
    	pr = obj.filterBtnAction();
    	pr = obj.sortPatName();
    	pr = obj.sortAntTestDate();
    	
    }
}