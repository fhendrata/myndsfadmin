//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: GlobalUserDao
//   PURPOSE: Permission free Data Access for User records.
// 
//   OWNER: CNS Response
//   CREATED: 01/02/2018 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
public without sharing class GlobalUserDao extends BaseDao  
{
	private static String NAME = 'User'; 
    private static final GlobalUserDao userDao = new GlobalUserDao();

    public static GlobalUserDao getInstance() 
    {
        return userDao; 
    }
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
			fldList ='Id, Email, Name, Phone, Title, Username, ContactId, IsActive, Can_Skip_Med_Validation__c, Enable_Neuroguide__c, medDataLink_login__c, medDataLink_Password__c, rpt_finalize_pass__c, View_All_Drug_Results__c';
		
		return fldList;
	}
	
	public User getById(String idInp)
    {
		return (User)getSObjectById(getFieldStr(), NAME, idInp);
    } 

    public User getUserIDByContactID(Id contactId)
    {
        UserDao useDao = new UserDao();
        return useDao.getUserIDByContactID(contactId);
    }
}