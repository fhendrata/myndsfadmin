//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: LeadDaoTest
// PURPOSE: Test class for LeadDao class
// CREATED: 06/06/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class LeadDaoTest 
{	
	@isTest static void testDaoMethod()
	{
		LeadDao lDao = new LeadDao();
		Lead testLead = LeadDao.getInstance().getTestLead();
		System.assert(testLead != null);

		Lead testLead2 = LeadDao.getInstance().getById(testLead.Id);
		System.assertEquals(testLead.LastName, testLead2.LastName);
	}

}