public abstract Class BaseController
{
    //----------------------------------------- ATTRIBUTES ---------------------------

    public Integer RESULT_TABLE_SIZE = 13;

    public Boolean IsTestCase { get; set;}
    private List<Contact> clinicContactList { get; set;}
    public String Domain { get  {  return rEEGUtil.getWebUrl();  }  }
    public Map<String, Profile_PEER_Permissions__c> ProfilePermissionMap { get; set; }

    private Boolean mHasNotification = false;
    public Boolean hasNotification
    {
    	get {
    		return getNotification() != 'No notifications';
    	}
    }

    public void loadPermissions()
    {
        ProfilePermissionMap = new Map<String, Profile_PEER_Permissions__c>();
        for (Profile_PEER_Permissions__c ppp : Profile_PEER_Permissions__c.getAll().values())
        {
            if (String.isNotBlank(ppp.Profile_Full_Name__c))
                ProfilePermissionMap.put(ppp.Profile_Full_Name__c, ppp);
            else if (String.isNotBlank(ppp.PermissionSet_API_Name__c))
                ProfilePermissionMap.put(ppp.PermissionSet_API_Name__c, ppp);
        }
    }

    public void addPageMessage(ApexPages.Severity s, String value)
    {
    	String message = e_StringUtil.isNullOrEmpty(value) ? '' : value;
        ApexPages.Message msg = new ApexPages.Message(s, message);
        ApexPages.addMessage(msg);
    }

    public String userNotification
    {
        get {
        	return getNotification();
        }
    }
/*
    public void SaveReeg(r_reeg__c reeg)
    {
        SaveReeg(reeg, '');
    }

    public void SaveReeg(r_reeg__c reeg, string statusType)
    {
        rEEGDao rDao = new rEEGDao();
        rDao.Save(reeg, statusType);
    }
*/
     /// <summary>
    /// Checks if we need to redirect to the other portal.
    /// </summary>
    /// <returns>A page reference to the WR home page if the profile is WR, null otherwise</returns>
    public PageReference checkProfileForRedirect()
    {
        if(IsMilitaryStudyPortalUser())
        {
            PageReference pg = Page.wr_PeerListS2;
            pg.setRedirect(true);
            return pg;
        }
        return null;
    }

    public boolean isLoggedIn()
    {
        return (UserInfo.getUserType() != 'Guest');
    }

    public String userName {

    	get {
    		return getUserName();
    	}
    }

    public string getLogoutUrl()
	{
		return rEEGUtil.getSitesLoginPage();
	}

	public String getUserName()
	{
		String name = '';
		String firstName = UserInfo.getFirstName();
		String lastName = UserInfo.getLastName();
		
		Integer firstNameLength = e_StringUtil.isNullOrEmpty(firstName) ? 0 : firstName.length();
		Integer lastNameLength = e_StringUtil.isNullOrEmpty(lastName) ? 0 : lastName.length();
		
		//-- believe it or not some records do not have a first name
		if (firstNameLength > 0 && (firstNameLength + lastNameLength) > 12)
			firstName = firstName.substring(0, 1);
		
		if (!e_StringUtil.isNullOrEmpty(firstName))
		{
			name += firstName;
			name += ' ';
		}
		name += lastName;
		return name;
	}

    protected Profile profile;
    public Profile getProfile()
    {
        if (profile == null)
            profile = [Select p.Name From Profile p where p.Id=:UserInfo.getProfileId()];

        return profile;
    }

    public void setProfile(Profile value)
    {
        profile = value;
    } 

    public String getShowPrintLinkAdmin()
    {
        return (getIsSysAdmin()) ? 'visible' : 'hidden';
    }
    
    public String getShowWrPrintLink()
    {
        return (getIsWalterReed() || getIsMgr()) ? 'visible' : 'hidden';
    }
    
    public String getShowPrintLink()
    {
        return 'visible';
    }

    public Boolean getCanAdminPEERs()
    {
        return getHasPermission('Admin_PEER__c');
    }

    public Boolean getCanManagePEERs()
    {
        return getHasPermission('Manage_PEER__c');
    }

    public Boolean getCanViewPEERs()
    {
        return getHasPermission('View_PEER__c');
    }

    private Boolean getHasPermission(String permType)
    {
        if (ProfilePermissionMap == null) 
            loadPermissions();

        if (ProfilePermissionMap.isEmpty()) 
            return false;

        List<String> permSetIdList = new List<String>();
        for (String key : ProfilePermissionMap.keySet())
        {
            Profile_PEER_Permissions__c permSetting = ProfilePermissionMap.get(key);

            if (permSetting != null && (Boolean)permSetting.get(permType) != null)
            {
                Boolean isChecked = (Boolean)permSetting.get(permType);
                if (isChecked)
                    permSetIdList.add(ProfilePermissionMap.get(key).PermissionSet_Id__c);
            }
        }

        List<PermissionSetAssignment> permList = null;
        if (!permSetIdList.isEmpty())
        {
            String query = 'Select Id, AssigneeId From PermissionSetAssignment Where AssigneeId = \'' + UserInfo.getUserId() + '\' AND PermissionSetId IN (\'' + String.join(permSetIdList,'\',\'') + '\')';
            permList = (List<PermissionSetAssignment>)Database.query(query);
        }

        return (permList != null && !permList.isEmpty());
    }

    public string getProfileName()
    {
        if (profile == null) getProfile();
        return (profile != null) ? profile.Name : '';
    }

    public Boolean getIsSysAdmin()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'System Administrator' || profile.Name == 'CNSR System Administrator' || profile.Name == 'Non Lightning System Administrator'));
    }

    public Boolean getIsSeniorProdMgr()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'CNSR Senior Production Manager');
    }

    public Boolean getIsProdMgr()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'CNSR Production Manager' || profile.Name == 'Mynd Operations'));
    }

    public Boolean getIsMedDir()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'CNSR Medical Director');
    }

    public Boolean getIsSalesDir()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'Mynd VP, Sales & Marketing');
    }

    public Boolean getIsSalesProvider()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'Mynd Sales & Marketing - Lightning');
    }

    public Boolean getIsWalterReed()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'WR Physician Portal' || profile.Name == 'Walter Reed Admin Portal'));
    }

    public Boolean getIsClinicAdmin()
    {
    	if (profile == null) getProfile();
        return (profile != null && profile.Name == 'Walter Reed Admin Portal' || profile.Name == 'Physician Portal Manager');
    }

    public Boolean getIsClinicPhy()
    {
    	if (profile == null) getProfile();
        return (profile != null && profile.Name == 'WR Physician Portal');
    }

    public Boolean getIsPhyTech()
    {
    	if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'Level 2 Physician/Tech Portal Profile' || profile.Name == 'Tech Portal Manager' || getIsPortalUserMgr() || getIsMgrLevel4()));
    }

    public Boolean getIsProduction()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'CNSR Production Platform Seat');
    }

    public Boolean getIsPortalUserMgr()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'Physician Portal Manager');
    }

    public Boolean getIsPortalUser3()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'Level 3 Physician Portal Profile'|| profile.Name == 'WR Physician Portal'));
    }

    public Boolean getIsPortalUser2()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'Level 2 Physician Portal Profile'|| profile.Name == 'WR Physician Portal'));
    }

    public Boolean IsMilitaryStudyPortalUser()
    {
        if (profile == null) getProfile();
        system.debug('### profile.Name: ' + profile.Name);
        return (profile != null && (profile.Name == 'WR Physician Portal' || profile.Name == 'Walter Reed Admin Portal'));
    }

    public Boolean getIsPhyAndTechLevel2()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'Level 2 Physician/Tech Portal Profile'));
    }

    public Boolean getIsPortalUser1_1()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'Level 1.1 Physician Portal Profile');
    }

    public Boolean getIsPortalUser1()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'Level 1 Physician Portal Profile');
    }

    public Boolean getIsQA()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'rEEG QA');
    }

    public Boolean getIsMgr()
    {
        return getIsMgrLevel3() || getIsMedDir() || getIsQA();
    }

    public Boolean getIsMgrLevel4()
    {
        return getIsSysAdmin() || getIsSeniorProdMgr() || getCanAdminPEERs();
    }

    public Boolean getIsMgrLevel3()
    {
        return getIsMgrLevel4() || getIsProdMgr() || getCanManagePEERs() || getCanViewPEERs();
    }

    public Boolean getIsMgrLevel2()
    {
        return getIsPortalUserMgr() || getIsMgrLevel3() || getIsPortalUser2();
    }

    public Boolean getIsMgrLevel1()
    {
        return getIsMgrLevel2() || getIsPortalUser1() || getIsSalesDir() || getIsProduction();
    }

    public Boolean getIsNotQA()
    {
        return !getIsQA();
    }

    public Boolean getIsNotMgr()
    {
        return !getIsMgr();
    }

    public Boolean getIsProdTestUser()
    {
        string userId = UserInfo.getUserId();
        return (!StringUtil.isNullOrEmpty(userId) && userId.startsWith('00560000000mMkz'));
    }

    public Boolean getIsAdminOrPortalUserMgr()
    {
        return getIsPortalUserMgr() || getIsSysAdmin();
    }

    public String getNotification()
    {
    	String mesg = '';
    	List<r_patient_outc__c> outcomes = [select CreatedDate from r_patient_outc__c where CreatedById =: UserInfo.getUserId() and cgi__c != '4 - Baseline' order by CreatedDate desc limit 1];
        List<Patient_Referral__c> patRefs = [Select CreatedDate from Patient_Referral__c where OwnerId =: UserInfo.getUserId() AND CreatedDate >= THIS_WEEK AND Physician_Referral_Status__c =: ''];
    	//if(outcomes == null || outcomes.length == 0) return 'You have not entered any outcomes';

    	if (outcomes != null && outcomes.size() > 0)
    	{
	    	Decimal weeks = Math.floor(Decimal.valueOf(Datetime.now().getTime() - outcomes[0].CreatedDate.getTime())/(1000.0*60*60*24*7));
    		if(weeks == 1) mesg =  'You have not entered any outcomes in ' + weeks + ' week.\r\n';
    		else if(weeks > 1) mesg = 'You have not entered any outcomes in ' + weeks + ' weeks.\r\n';
    	}
        if(patRefs.size() > 0) mesg += 'You have ' + patRefs.size() + ' new Patient Referral(s) this week.';
        if(e_StringUtil.isNullOrEmpty(mesg)) mesg += 'No Notifications';

    	return mesg;
    }

    public String quote(String s)
    {
        return '\'' + s + '\'';
    }

    public List<Contact> getContactListForAccount()
    {
        if (clinicContactList == null)
        {
            String acctId = getCurrentAccount();

            if (!e_StringUtil.isNullOrEmpty(acctId))
            {
                clinicContactList = [select Id, Name, Title from Contact where Title != 'Administration' AND Contact.AccountId =: acctId];
            }
        }

        return clinicContactList;
    }

    public boolean getIsClinicMoreThanOnePhy()
    {
        clinicContactList = getContactListForAccount();
        return (clinicContactList != null && clinicContactList.size() > 1);
    }

    public String getCurrentAccount()
    {
        List<User> uList = [select Id,Contact.AccountId from User where Id =: UserInfo.getUserId()];
        if(uList != null && !uList.IsEmpty()) return uList.get(0).Contact.AccountId;
        return '';
    }

    public String getCurrentPhysician()
    {

        //TODO-probably need to check if this is actually a physician
        List<User> uList = [select Id,ContactId from User where Id =: UserInfo.getUserId()];
        if(uList != null && !uList.IsEmpty() && uList.get(0).ContactId != null) return uList.get(0).ContactId;
        return '';

    }

    //----------------------------------------- ATTRIBUTES ---------------------------
    private String className;
    public String getClassName2()
    {
        return className;
    }
    public void setClassName2(String value)
    {
        className = value;
    }

    //----------------------------------------- LOG METHODS ---------------------------
    public void handleError(String method, Exception ex)
    {
        LogUtil.handleError( getClassName2(), method, ex);
        debug( method + ':' + ex);
    }

    //----------------------------------------- LOG METHODS ---------------------------
    public void debug(String message)
    {
        LogUtil.debug( className, message);
    }

    public void logMethod(String methodName)
    {
        LogUtil.debug( className, methodName);
    }

    private void addErrorMessage(String value)
    {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, value);
        ApexPages.addMessage( msg);
    }

    //----------------------------------------- VALIDATION METHODS ---------------------------
    public static Boolean isNullOrEmpty(String value)
    {
        return value == null || value == '';
    }

    public Boolean isEmailValid(String email)
    {
        Boolean valid = false;
        Pattern cpattern = Pattern.compile('^[a-zA-Z0-9\\._-]+@[a-zA-Z0-9\\._-]+\\.[a-zA-Z]{2,4}$');
        Matcher cmatcher = cpattern.matcher(email);
        if (!isNullOrEmpty(email)) valid = cmatcher.matches();
        return valid;
    }

    public Boolean isSsnValid(String ssn)
    {
        Boolean valid = false;
        Pattern cpattern = Pattern.compile('^[0-9]{3}-[0-9]{2}-[0-9]{4}$');
        Matcher cmatcher = cpattern.matcher(ssn);
        if (!isNullOrEmpty(ssn)) valid = cmatcher.matches();
        return valid;
    }

    public Boolean isStateValid(String state)
    {
        Boolean valid = false;
        Pattern cpattern = Pattern.compile('^(AK|AL|AR|AZ|CA|CO|CT|DC|DE|FL|GA|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MI|MN|MO|MS|MT|NB|NC|ND|NH|NJ|NM|NV|NY|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VA|VT|WA|WI|WV|WY)$');
        Matcher cmatcher = cpattern.matcher(state);
        if (!isNullOrEmpty(state)) valid = cmatcher.matches();
        return valid;
    }

    public Boolean isPhoneValid(String pNum)
    {
        Boolean valid = false;
		Pattern phPattern = Pattern.compile('[\\(\\)-]');
		String tempPh = phPattern.matcher(pNum).replaceAll('');
		tempPh = tempPh.replaceAll(' ', '');
		if(tempPh.length() == 10) valid = true;
        return valid;
    }

    public Boolean isZipValid(String zip)
    {
        Boolean valid = false;
        Pattern cpattern = Pattern.compile('^[0-9]{5}$|^[0-9]{5}-[0-9]{4}$');
        Matcher cmatcher = cpattern.matcher(zip);
        if (!isNullOrEmpty(zip)) valid = cmatcher.matches();
        return valid;
    }

    public Boolean isDobValid(Date dob)
    {
        Boolean valid = false;
        if(dob <= System.today()) valid = true;
        return valid;
    }

    public Boolean isDateRangeValid(Date startDate, Date endDate)
    {
        Boolean valid = false;
        if(startDate <= endDate) valid = true;
        return valid;
    }

}