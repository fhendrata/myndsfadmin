//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: e_DataUtil
//   PURPOSE: Utility data class
// 
//     OWNER: CNSR
//   CREATED: 05/12/10 Ethos Solutions - www.ethos.com
//
//--------------------------------------------------------------------------------
public with sharing class e_DataUtil 
{
	public static List<string> getAllObjsNames2()
	{
		List<string> returnVal = new List<string>();
		
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
		List<Schema.SObjectType> gdTokens = gd.values() ;
		for(Schema.SObjectType row : gdTokens)
		{
			Schema.Describesobjectresult objResult = row.getDescribe();
			returnVal.add(objResult.getName());
		}
		
		return returnVal;
	}
	
	public static List<SelectOption> getAllObjsNames()
	{
		List<SelectOption> returnVal = new List<SelectOption>();
		returnVal = new List<SelectOption>();
		returnVal.add(new SelectOption('-- none --', '-- none --')); 
			
		Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
		List<Schema.SObjectType> gdTokens = gd.values() ;
		for(Schema.SObjectType row : gdTokens)
		{
			Schema.Describesobjectresult objResult = row.getDescribe();
			returnVal.add(new SelectOption(objResult.getLabel(), objResult.getName())); 
		}
		
		return returnVal;
	}
	
	public static Boolean isCalcDataField(DisplayType fType)
    {
        return isStringField(fType) || isBooleanField(fType) || isDateField(fType) || isNumberField(fType) || isPickListField(fType);
    }

    public static Boolean isStringField(DisplayType fType)
    {
        return fType != null && (fType == DisplayType.String || fType == DisplayType.TextArea || fType == DisplayType.Phone || fType == DisplayType.Email);
    }

     public static Boolean isPickListField(DisplayType fType)
    {
        return fType == DisplayType.Picklist;
    }

    public static Boolean isBooleanField(DisplayType fType)
    {
        return fType != null && (fType == DisplayType.Boolean);
    }

    public static Boolean isDateField(DisplayType fType)
    {
        return fType != null && (fType == DisplayType.Date || fType == DisplayType.DateTime);
    }

    public static Boolean isNumberField(DisplayType fType)
    {
        return fType != null && (fType == DisplayType.Currency || fType == DisplayType.Double || fType == DisplayType.Integer || fType == DisplayType.Percent );
    }
  
  	public static boolean isNumber(string numStr)
    {
        return pattern.matches('[0-9]+[/.]?[0-9]*', numStr);
    }
    
    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testDataUtil()
    {
        e_DataUtil.isCalcDataField( DisplayType.String);
        e_DataUtil.isStringField( DisplayType.TextArea);
        e_DataUtil.isPickListField( DisplayType.Picklist);
        e_DataUtil.isBooleanField( DisplayType.Boolean);
        e_DataUtil.isDateField( DisplayType.Date);
        e_DataUtil.isNumberField( DisplayType.Currency);

        e_DataUtil.isNumber('5');
        List<string> names = e_DataUtil.getAllObjsNames2();
        List<SelectOption> namesSo = e_DataUtil.getAllObjsNames();
    }

}