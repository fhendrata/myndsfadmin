//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: LPR_PatReferralRunner
// PURPOSE: Runner for the Lead update scheduled process 
// CREATED: 06/06/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
global class LPR_PatReferralRunner implements Schedulable
{
	global void execute(SchedulableContext sc) 
	{	
		string query = 'SELECT Id, Name, Patient_Last_Name__c, Patient_First_Name__c, Address_1__c, City__c, State__c, Postal_code__c, Best_Phone_Number__c, Email_Address__c, Lead__c, Is_Updated__c FROM Patient_Referral__c WHERE Is_Updated__c = true';	
		system.debug('LPR_PatReferralRunner:query: ' + query);

		database.executeBatch(new LPR_PatReferralProcessor(query), 10);
	}
}