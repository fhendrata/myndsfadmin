/**********************************************************************************

 PURPOSE: This class defines a utility class which mirrors the DrugUtil class in
          EverestData in the .NET app. It provides methods to compute parameters
          to send to the PEER Summary.aspx page to filter certain drugs to the
          report

 CLASS: DrugUtil
 CREATED: 06/27/2012 Ethos Solutions - www.ethos.com
 AUTHOR: Jason M. Swenski
 ***********************************************************************************/
public with sharing class DrugUtil {

	private Map<String, Integer> drugMap;
	private Map<Long,String> hexMap;
	private static DrugUtil instance;

	private DrugUtil()
	{
        loadHexMap();
        //loadWrDrugMap();
        //-- jdepetro.12.28.12 this mapping was changed for PeerInteractive. Since this class is not currently 
        //-- used for non peer intet reports
        
        //-- jdepetro.9.7.16 - activated the drug visibility for the Smart MD Canadian study.
        loadDrugMap();
	}

   /**
    * This object should never be constructed using the parameterless
    * constructor. Use this getInstance method instead to get an instance
    * of this utility class
    *
    * @return an instance of DrugUtil
    */
	public static DrugUtil getInstance()
	{
		if(instance == null)
		{
			instance = new DrugUtil();
		}
		return instance;
	}

   /**
    * This method computes the sdh parameter for the Summary.aspx page
    * in the EverestWeb app. The output of this function control what drugs
    * show up on the page in the .NET app
    *
    *
    * @param a List of r_reeg_dx__c objects
    *
    * @returns a hex code representing which drugs to show
    */
	public String getHexFilter(List<r_reeg_dx__c> diags)
	{
		Long longCode = 0;
	    if(diags == null || diags.size() == 0) return toHex(0);
	    //---don't double add diagnoses.
	    Map<String,Boolean> alreadySaw = new Map<String,Boolean>();
	    for(r_reeg_dx__c diag : diags)
	    {
	    	System.debug('### diag: ' + diag);
	    	System.debug('### diag.Reference_Dx__r.PEER_Drug_mapping__c: ' + diag.Reference_Dx__r.PEER_Drug_mapping__c);

			if(diag.Reference_Dx__r.PEER_Drug_mapping__c == null) continue;
			else
			{
	            List<String> drugs = diag.Reference_Dx__r.PEER_Drug_mapping__c.split(';');

	            for(String s : drugs)
	            {
	            	if(drugMap.containsKey(s) && !alreadySaw.containsKey(s))
	            	{
	            	    alreadySaw.put(s,true);
	            	    Integer power = drugmap.get(s);
	            	    longCode += Math.pow(2,power).longValue();
	            	}
	            }
			}
	    }
	    return toHex(longCode);
	}

    /**
    * This method takes a Long value and converts it to it's
    * hex representation.
    *
    * @param a Long value
    *
    * @returns a String hex code representing this Long
    */
	public String toHex(Long input)
	{
		//---TODO this is a simple calculation that only handles
		//---positve Longs, should be enhanced to work for negs
		//---but this isn't really needed right now.
		Long quotient = input;
		String temp = '';
		do
		{
			Long rem = Math.mod(quotient,16);
			temp = hexMap.get(rem) + temp;
			quotient = quotient / 16;
		}
		while(quotient > 0);

		return temp;
	}

    //-----------ugly maps to hold the constants
    private void loadHexMap()
    {
    	hexMap = new Map<Long,String>();
    	hexMap.put(0,'0');
    	hexMap.put(1,'1');
    	hexMap.put(2,'2');
    	hexMap.put(3,'3');
    	hexMap.put(4,'4');
    	hexMap.put(5,'5');
    	hexMap.put(6,'6');
    	hexMap.put(7,'7');
    	hexMap.put(8,'8');
    	hexMap.put(9,'9');
    	hexMap.put(10,'A');
    	hexMap.put(11,'B');
    	hexMap.put(12,'C');
    	hexMap.put(13,'D');
    	hexMap.put(14,'E');
    	hexMap.put(15,'F');

    }

	private void loadDrugMap()
	{	    
	    drugMap = new Map<String, Integer>();
		drugMap.put('alprazolam',0);
	    drugMap.put('amantadine',1);
	    drugMap.put('amitriptyline',2);
	    drugMap.put('atomoxetine',3);
	    drugMap.put('bupropion',4);
	    drugMap.put('carbamazepine',5);
	    drugMap.put('citalopram',6);
	    drugMap.put('clomipramine',7);
	    drugMap.put('clonazepam',8);
	    drugMap.put('clonidine',9);
	    drugMap.put('desipramine',10);
	    drugMap.put('dlamphetamine',11);
	    drugMap.put('divalproex',12);
	    drugMap.put('duloxetine',13);
	    drugMap.put('escitalopram',14);
	    drugMap.put('fluoxetine',15);
	    drugMap.put('fluvoxamine',16);
	    drugMap.put('gabapentin',17);
	    drugMap.put('imipramine',18);
	    drugMap.put('lamotrigine',19);
	    drugMap.put('lithium',20);
	    drugMap.put('lorazepam',21);
	    drugMap.put('methylphenidate',22);
	    drugMap.put('damphetamine',23);
	    drugMap.put('nortriptyline',24);
	    drugMap.put('olanzapine',25);
	    drugMap.put('oxcarbazepine',26);
	    drugMap.put('paroxetine',27);
	    drugMap.put('risperidone',28);
	    drugMap.put('selegiline',29);
	    drugMap.put('sertraline',30);
	    drugMap.put('tranylcypromine',31);
	    drugMap.put('venlafaxine',32);
	    drugMap.put('efa',33);
	    drugMap.put('modafanil',34);
	}
	
	private void loadWrDrugMap()
	{
		drugMap = new Map<String, Integer>();
		drugMap.put('alprazolam',0);
	    drugMap.put('amantadine',1);
	    drugMap.put('amitriptyline',2);
	    drugMap.put('atomoxetine',3);
	    drugMap.put('bupropion',4);
	    drugMap.put('carbamazepine',5);
	    drugMap.put('citalopram',6);
	    drugMap.put('clomipramine',7);
	    drugMap.put('clonazepam',8);
	    drugMap.put('clonidine',9);
	    drugMap.put('desipramine',10);
	    drugMap.put('dlamphetamine',11);
	    drugMap.put('divalproex',12);
	    drugMap.put('duloxetine',13);
	    drugMap.put('escitalopram',14);
	    drugMap.put('fluoxetine',15);
	    drugMap.put('fluvoxamine',16);
	    drugMap.put('gabapentin',17);
	    drugMap.put('imipramine',18);
	    drugMap.put('lamotrigine',19);
	    drugMap.put('lithium',20);
	    drugMap.put('lorazepam',21);
	    drugMap.put('methylphenidate',22);
	    drugMap.put('damphetamine',23);
	    drugMap.put('nortriptyline',24);
	    drugMap.put('olanzapine',25);
	    drugMap.put('oxcarbazepine',26);
	    drugMap.put('paroxetine',27);
	    drugMap.put('risperidone',28);
	    drugMap.put('selegiline',29);
	    drugMap.put('sertraline',30);
	    drugMap.put('tranylcypromine',31);
	    drugMap.put('venlafaxine',32);
	}

	private static testmethod void testDrugUtil()
	{
	   DrugUtil du = DrugUtil.getInstance();
	   system.assert('0' == du.toHex(0));
	   system.assert('1' == du.toHex(1));
	   system.assert('29A' == du.toHex(666));
	}
}