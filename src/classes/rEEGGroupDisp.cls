public class rEEGGroupDisp
{
    private String name;
    public String getName()
    {
        return rEEGUtil.getGroupName(name);
    }
    public void setName(String s)
    {
        name= s;
    }    
    public String getNameStyle()
    { 
        return '';
    }


    private String drugClass;
    public String getDrugClass()
    {
        return drugClass;
    }
    public void setDrugClass(String s)
    {
        drugClass= s;
    }    
    public String getDrugClassStyle()
    { 
        return '';
    }
        
    private String sensitivity;
    public String getSensitivity()
    {
        return sensitivity;
    }
    public void setSensitivityName(String s)
    {
        sensitivity= s;
    }
    
    public String getSensitivityStyle()
    {
        return rEEGStyleUtil.getSensitivityStyle(sensitivity);
    }
    
    private String bioPredom;
    public String getBioPredom()
    {
        return bioPredom;
    }
    public void setBioPredom(String s)
    {
        bioPredom= s;
    }
    
    public String getBioPredomStyle()
    {
        return 'font-weight: normal';
    }
                    
    public void Setup(r_reeg_group__c obj)
    {
        name= obj.name;
        drugClass= obj.drug_class__c;
        sensitivity= obj.sensitivity__c;
        bioPredom= obj.bio_predom__c;
    }

     //------------TEST-----------------------------
    public static testMethod void testGroupDisp()
    {
        rEEGGroupDisp disp = new rEEGGroupDisp();

        r_reeg_group__c obj = new r_reeg_group__c();
        obj.name = 'other';
        obj.drug_class__c = 'xx';
        obj.sensitivity__c = 'yy';
        obj.bio_predom__c = 'zz';

        disp.Setup(obj);
        disp.setName(obj.name);
        disp.setBioPredom(obj.bio_predom__c);
        disp.setSensitivityName(obj.sensitivity__c);
        disp.setDrugClass(obj.drug_class__c);
     
        System.assertEquals(disp.getName(), obj.name);
        System.assertEquals(disp.getDrugClass(), obj.drug_class__c);
        System.assertEquals(disp.getSensitivity(), obj.sensitivity__c);
        System.assertEquals(disp.getBioPredom(), obj.bio_predom__c);
        System.assertEquals(disp.getNameStyle(), '');
    }
}