public with sharing class wr_PatientListController extends BaseController
{
	protected string searchCriteria { get; set; }
	public Boolean hasNext { get { return con.getHasNext(); } set; }
    public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }
    public Boolean hasResults { get { return (resultNumber > 0); } set; }
    public Integer pageNumber  { get { return con.getPageNumber(); } set; }
    public Integer resultNumber  { get { return con.getResultSize(); } set; }
    public void previous() { con.previous(); }
    public void next() { con.next(); }
    
    private String sortDirection { get; set; }
    private String sortExp { get; set; }
    public String sortExpression 
    { 
    	get { return sortExp; }
	    set {
	       if (value == sortExp)
	         sortDirection = (sortDirection == 'ASC') ? 'DESC' : 'ASC';
	       else
	         sortDirection = 'ASC';
	       sortExp = value;
	    }
   	}
    
    private integer resultTableSize {get; set;}
    private patientDao pDao;

    public string newFilterId {get; set;}
	public ApexPages.StandardSetController con {get; set;}
	public String patSearchString {get; set;}

	public wr_PatientListController ()
    {
    	sortExp = 'Last_PEER_Date__c';
    	sortDirection = 'DESC';
    	
    	pDao = new patientDao();
    	resultTableSize = RESULT_TABLE_SIZE;
    	setupFilter();
        loadPatients();
    }

    private void setupFilter()
    {	
    	if(getIsClinicPhy()) pDao.filter = 'physician__c = ' + quote(getCurrentPhysician());
    	if(getIsClinicAdmin())
    	{
    		
    		Id ftBAcctId = null;
        	Id wrAcctId = null;
        	AccountDao acctDao = new AccountDao();
    		List<Account> acctList = acctDao.getByName('Fort Belvoir Community Hospital');
    		if (acctList != null && !acctList.IsEmpty())
    			ftBAcctId = acctList.get(0).Id;
    		acctList = acctDao.getByName('Walter Reed Army Medical Center');
    		if (acctList != null && !acctList.IsEmpty())
    			wrAcctId = acctList.get(0).Id;
   
    		if (ftBAcctId != null && wrAcctId != null)
        		pDao.filter = '(physician__r.AccountId = ' + quote(wrAcctId) + ' OR physician__r.AccountId = ' + quote(ftBAcctId) + ')'; 
        	else if (ftBAcctId != null)
        		pDao.filter = 'physician__r.AccountId = ' + quote(ftBAcctId); 
        	else if (wrAcctId != null)
        		pDao.filter = 'physician__r.AccountId = ' + quote(wrAcctId); 
        	else
        		pDao.filter = 'physician__r.AccountId = ' + quote(getCurrentAccount());
    	}
    }

    private PageReference loadPatients()
    {
    	system.debug('FILTER: ' + pDao.filter);

    	String query = pDao.getProviderPatientsQuery('', getOrderText());
    	system.debug('loadPatients:query: ' + query);
    	
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
        return null;
    }
    
    public PageReference loadPatients(String fieldName)
    {
    	system.debug('FILTER: ' + pDao.filter);

    	String query = pDao.getProviderPatientsQuery('', fieldName);
    	system.debug('WR loadPatients:query: ' + query);
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
        return null;
    }

    public List<r_patient__c> getPatients()
    {
        if(con != null)
            return (List<r_patient__c>)con.getRecords();
        else
            return null;
    }

    public void searchPatients()
    {
        String query = pDao.getNameSearchQuery(patSearchString,'limit 9500');

        system.debug('SEARCH QUERY: ' + query);

        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);

    }
    
    public String getSortDirection()
	{
	    if (sortExpression == null || sortExpression == '')
	      return 'ASC';
	    else
	     return sortDirection;
	}
	
	public String getOrderText()
	{
	    return ' order by ' + sortExpression  + ' ' + sortDirection + '  NULLS LAST';
	}
	
    
    public void sortPatientName()
    {
    	sortExpression = 'name';
    	loadPatients();
    }
    
    public void sortPeerDate()
    {
    	sortExpression = 'Last_PEER_Date__c';
    	loadPatients();
    }
    
    public void sortOutcome()
    {
    	sortExpression = 'Outcomes__c';
    	loadPatients();
    }
    
    public void sortProviderId()
    {
    	sortExpression = 'prov_pat_id__c';
    	loadPatients();
    }


    //------------ TEST METHODS ------------------------

    private static testmethod void testController()
    {/*
        wr_PatientListController plc = new wr_PatientListController();
        plc.patSearchString = 'test';
        plc.searchPatients();
        plc.getPatients();
*/

    }




}