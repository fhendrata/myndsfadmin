public with sharing class uploadWizardController2 extends BaseUploadWizController
{
    private Profile profile {get; set;}

    public uploadWizardController2()
    {    
        init();
    }
    
    public void init()
    {
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.qsid = ApexPages.currentPage().getParameters().get('qsid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        string strIsNewPat = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(strIsNewPat) && strIsNewPat == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        String strIsNew = ApexPages.currentPage().getParameters().get('isNew');
        this.IsNew = (!e_StringUtil.isNullOrEmpty(strIsNew) && strIsNew == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        pDao = new patientDao();
    }
    
    public PageReference loadAction()
    {
    	this.profile = [Select p.Name From Profile p where p.Id=:UserInfo.getProfileId()];
        if (!e_StringUtil.isNullOrEmpty(patId))
    	{
    		this.patient = pDao.getById(patId);
    	}
    	
    	return null;
    }

    public PageReference wizStep1()
    {
        if (patient != null && patient.id != null)
        {	
        	PageReference returnVal = new PageReference('/apex/uploadWizard1');
          	AddParameters(returnVal);
               	
            returnVal.setRedirect(true);
            return returnVal;
        }
        else
        {
            return new PageReference( '/apex/uploadWizard1');
        }
    }

    public PageReference wizStep3()
    {
       	if (patient != null)
           	pDao.saveSObject(patient);

        PageReference returnVal = new PageReference('/apex/uploadWizard3');
       	AddParameters(returnVal);
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference cancelWizOverride()
    {
    	PageReference pr = cancelWizAction();
    	pr = new PageReference('/apex/UploadWizard1');
    	pr.setRedirect(true);
        return pr;
    }
    
          //---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
        uploadWizardController2 cont = new uploadWizardController2();
		cont.IsTestCase = true;
		r_reeg__c reeg = rEEGDao.getTestReeg();
		ApexPages.currentPage().getParameters().put('rid', reeg.Id);
		ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
		cont.init();
		cont.loadAction();

		PageReference pRef1 = cont.wizStep1();
		pRef1 = cont.wizStep3();
		cont.cancelWizOverride();
        System.assertEquals( 1, 1);
    }
    
}