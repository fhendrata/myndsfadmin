//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: PEERDocumentListController
//   PURPOSE: Controller for the PEER document list apge
// 
//     OWNER: CNS Response
//   CREATED: 03/08/12 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
public with sharing class PEERDocumentListController extends BaseController
{
	private string searchCriteria {get; set;}
	private string environmentType {get; set;}
    private integer resultTableSize {get; set;}
    private PEERDocDao docDao {get; set;} 
     
    public string newFilterId {get; set;}
	public ApexPages.StandardSetController con {get; set;}
	public String docSearchString {get; set;}
	 
    public Boolean hasNext { get { return con.getHasNext(); } set; }  
    public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }  
    public Boolean hasResults { get { return (resultNumber > 0); } set; }  
    public Integer pageNumber  { get { return con.getPageNumber(); } set; }
    public Integer resultNumber  { get { return con.getResultSize(); } set; }  
    public void previous() { con.previous(); }  
    public void next() { con.next(); }  
    
	
	public PEERDocumentListController ()
    {
    	environmentType = ApexPages.currentPage().getParameters().get('env'); 
    	docDao = new PEERDocDao();
    	resultTableSize = RESULT_TABLE_SIZE;
    	setupFilter(); 
        loadDocs();
    }
    
    private void setupFilter()
    {
    	if (!e_StringUtil.isNullOrEmpty(environmentType) && environmentType == 'ol')
    		docDao.filter = 'Environment_Type__c = \'PEER Online\'';
    }
    
    private PageReference loadDocs()
    {  
    	system.debug('FILTER: ' + docDao.filter);
    	String query = docDao.getCategorySearchQuery('', 'order by Sequence__c');
    		
    	System.debug('Doc query: ' + query);
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
        return null;
    }
    
    public List<PEER_Documentation__c> getDocs()
    {
        if(con != null)  
            return (List<PEER_Documentation__c>)con.getRecords();
        else
            return null;
    } 
    
    public void searchDocs()
    { 
        String query = docDao.getCategorySearchQuery(docSearchString,'');
        system.debug('SEARCH QUERY: ' + query);
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
    }
    
    //------------ TEST METHODS ------------------------
    private static testmethod void testController()
    {
        PEERDocumentListController docController = new PEERDocumentListController();
        docController.docSearchString = 'a';
        docController.searchDocs();
        docController.getDocs();
        
    }
}