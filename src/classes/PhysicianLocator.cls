public with sharing class PhysicianLocator {
	
	public List<Contact> physicians {get; set;}
	public String physcianCode {get; set;}
	public PhysicianLocator()
	{
	   physicians = [select Latitude__c,Longitude__c from Contact where not Latitude__c =: null];	
	   physcianCode = '';
	   Integer i = 0;
	   for(Contact physician : physicians)
	   {
	   	
	   	  
	   	   	   Decimal lt = physician.Latitude__c;
	   	   	   Decimal ln = physician.Longitude__c;
	           physcianCode += '\n\tmapItems['+ i + '] = new google.maps.LatLng(' + lt + ',' + ln + ');\n';
	           i++;

	   }
	}

}