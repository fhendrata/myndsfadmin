Public Class ActionControllerExt extends BaseController
{
    private final r_action__c action;

    Public ActionControllerExt()
    {
        //---this is only used for the apex test case
        action = new r_action__c();
        action.status__c = 'Complete';
        action.type__c = 'REPORT_RELOAD'; 

        profile = new Profile();
        profile.Name = 'Physician Portal Profile';
    }

    //---Constructor from the standard controller
    public ActionControllerExt( ApexPages.StandardController stdController)
    {    
        this.action = (r_action__c)stdController.getRecord();
    }

    public PageReference Save()
    {
        if (action.id != null) upsert action;
        else insert action;

        PageReference returnVal = new PageReference( '/apex/ActionListPage');
        returnVal.setRedirect(true);
        return returnVal;
    }

    private List<r_action__c> actions;   
    public List<r_action__c> getActions()
    {
         if (actions == null)
         {
             actions = new List<r_action__c>();  

             for(r_action__c actionRow : 
                  [select id, name, reeg_id__c, status__c, type__c, LastModifiedDate
                  from r_action__c
                  where status__c = 'NEW' order by CreatedDate desc limit 50])
             {
                 actions.Add(actionRow);
             } 
        }    
        return actions;
    } 

    private List<r_action__c> qsScans;   
    public List<r_action__c> getQSScans()
    {
         if (qsScans == null)
         {
             qsScans = new List<r_action__c>();  

             for(r_action__c actionRow : 
                  [select id, name, reeg_id__c, status__c, type__c, LastModifiedDate
                  from r_action__c
                  where type__c = 'QUICKSTART_SCAN' and status__c != 'Complete' order by CreatedDate desc limit 50])
             {
                 qsScans.Add(actionRow);
             } 
        }    
        return qsScans;
    } 

    private List<r_action__c> qsProcesses;   
    public List<r_action__c> getQSProcesses()
    {
         if (qsProcesses == null)
         {
             qsProcesses = new List<r_action__c>();  

             for(r_action__c actionRow : 
                  [select id, name, reeg_id__c, status__c, type__c, LastModifiedDate
                  from r_action__c
                  where type__c = 'QUICKSTART_PROCESS' and status__c != 'Complete' order by CreatedDate desc limit 50])
             {
                 qsProcesses.Add(actionRow);
             } 
        }    
        return qsProcesses;
    } 

    private List<r_action__c> cnsVars;   
    public List<r_action__c> getCNSVars()
    {
         if (cnsVars == null)
         {
             cnsVars = new List<r_action__c>();  

             for(r_action__c actionRow : 
                  [select id, name, reeg_id__c, status__c, type__c, LastModifiedDate
                  from r_action__c
                  where type__c = 'CNS_VAR_RELOAD' and status__c != 'Complete' order by CreatedDate desc limit 50])
             {
                 cnsVars.Add(actionRow);
             } 
        }    
        return cnsVars;
    } 

    private List<r_action__c> reportReloads;   
    public List<r_action__c> getReportReloads()
    {
         if (reportReloads == null)
         {
             reportReloads = new List<r_action__c>();  

             for(r_action__c actionRow : 
                  [select id, name, reeg_id__c, status__c, type__c, LastModifiedDate
                  from r_action__c
                  where (type__c = 'REPORT_RELOAD' or type__c = 'FULL_REPORT_RELOAD' or type__c = 'full_REPORT_RELOAD' or type__c = 'REPORT_FINALIZE' or type__c = 'REPORT_RULES_TEST' ) and status__c != 'Complete'
                  order by CreatedDate desc limit 50])
             {
                 reportReloads.Add(actionRow);
             } 
        }    
        return reportReloads;
    } 

    private List<r_action__c> completeActions;   
    public List<r_action__c> getCompleteActions()
    {
         if (completeActions == null)
         {
             completeActions = new List<r_action__c>();  

             for(r_action__c actionRow : 
                  [select id, name, reeg_id__c, status__c, type__c, LastModifiedDate
                  from r_action__c
                  where status__c = 'COMPLETE' order by CreatedDate desc limit 50])
             {
                 completeActions.Add(actionRow);
             } 
        }    
        return completeActions;
    } 

    public PageReference NewAction()
    {
        PageReference returnVal = new PageReference( '/apex/ActionEditPage');
        returnVal.setRedirect(true);
        return returnVal;
    }

    public PageReference DeleteAction()
    {
        if (action != null) delete action;

        PageReference returnVal = new PageReference( '/apex/ActionListPage');
        returnVal.setRedirect(true);
        return returnVal;
    }

    public PageReference CancelEdit()
    {
        PageReference returnVal = new PageReference( '/apex/ActionListPage');
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    
    //-----------------TEST-----------------------------------

    public static testMethod void testController()
    {
        ActionControllerExt cont = new ActionControllerExt();
        PageReference pRef1 = cont.Save();
        PageReference pRef2 = cont.NewAction();
        List<r_action__c> kw = cont.getActions();

        Boolean val1 = cont.getIsMgr();
        Boolean notval = cont.getIsNotMgr();
        Boolean val = !notval;
        System.assertEquals( val1, val);
        System.assertEquals( '1', '1');
        rEEGControllerExt rEEGContr = new rEEGControllerExt();
        r_reeg__c reeg = rEEGContr.getTestReeg();
        r_action__c action = new r_action__c();
        action.reeg_id__c = reeg.Id;
        action.status__c = 'Complete';
        action.type__c = 'REPORT_RELOAD'; 
        insert action;  
        
        action = new r_action__c();
        action.reeg_id__c = reeg.Id;
        action.status__c = 'NEW';
        action.type__c = 'REPORT_RELOAD'; 
        insert action;  
        
        List<r_action__c>  actionList = cont.getQSProcesses();
        
        actionList = cont.getCNSVars(); 
        actionList = cont.getQSScans(); 
        actionList = cont.getCompleteActions();   
        actionList = cont.getReportReloads(); 
        
        pRef2 = cont.DeleteAction();
        pRef2 = cont.CancelEdit();
        
        
    }
 }