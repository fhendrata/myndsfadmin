//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: TriggerSettings
// PURPOSE: Helper class for trigger events.
// CREATED: 02/14/18 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public without sharing class TriggerSettings
{
	private static Map<String, Triggers__c> triggerSettingMap;
	
	//-- Helper method to return the active or inactive status for a specified trigger
	public static Boolean isTriggerActive(String triggerName)
	{
		if (triggerSettingMap == null)
			triggerSettingMap = Triggers__c.getAll();
		
		Triggers__c setting = triggerSettingMap.get(triggerName);

		return (setting != null && setting.Is_Active__c != null) ? setting.Is_Active__c : true;

	}
}