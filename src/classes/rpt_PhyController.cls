//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: rpt_PhyController
// PURPOSE: Used for building records for the Phy statistics report.
// CREATED: 09/17/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class rpt_PhyController extends BaseController
{
	public integer month {get; set;}
	public integer year {get; set;}
	public string errorStr {get; set;}
	public integer contactCount {get; set;}
	public string sfProcessId {get; set;}
	public integer batchSize {get; set;}
	public string searchText {get; set;}
	public List<rpt_physician__c> phyList {get; set;}
	
	public rpt_PhyController()
	{
		IsTestCase = false;
	}
	
	public PageReference loadAction()
	{	
		/*
		ContactDao conDao = new ContactDao();
		conDao.IsTestCase = IsTestCase;
		List<Contact> phyList = conDao.getfirstK();
		contactCount = phyList.size();
		*/
		return null;
	}	
	
    public PageReference search()
    {
    	rpt_PhyDao phyDao = new rpt_PhyDao();
    	phyDao.IsTestCase = IsTestCase;
		phyList = phyDao.getByName(searchText);
		
		return null;
    }
    
    public List<r_reeg__c> getReegList()
    {
    	rEEGDao rDao = new rEEGDao();
		rDao.IsTestCase = IsTestCase;
		return rDao.getByMonthYear(7, 2010);
    }
    
    public PageReference RunSetup()
    {
    	string whereClause = '';
    	string orderClause = '';

    	RecordTypeDao rtDao = new RecordTypeDao();
    	RecordType cnsrContactRecordType = rtDao.getByRecordTypeName('CNSR Contact');
    	RecordType cnsrEEGTechRecordType = rtDao.getByRecordTypeName('CNSR EEG Tech');

    	if (cnsrContactRecordType != null && cnsrEEGTechRecordType != null)
    	{
	   		// we don't allow blank, CNSR EEG Tech or CNSR Contact record types
	    	whereClause = ' RecordTypeId != \'\' and RecordTypeId != \'' + cnsrContactRecordType.Id + '\' and RecordTypeId != \'' + cnsrEEGTechRecordType.Id + '\'';
		}
		
		ContactDao conDao = new ContactDao();
		conDao.IsTestCase = this.IsTestCase;
		string query = conDao.getSql('Id, FirstName, LastName, reeg_forecast__c', 'Contact', whereClause, orderClause);

		if (!IsTestCase)
		{
			sfProcessId = database.executeBatch(new bat_PhyRptLoad(query, month, year, 1), batchSize);
		}
		
		errorStr = 'Empty records for ' + month + '-' + year + ' submitted as a batch process'; 

    	return null; 
    }
   
    public PageReference RunCalc()
    {
    	string whereClause = '';
		string orderClause = '';
		
		ContactDao conDao = new ContactDao();
		conDao.IsTestCase = this.IsTestCase;
		string query = conDao.getSql('Id, FirstName, LastName, reeg_forecast__c', 'Contact', whereClause, orderClause);
		query += ' limit 1';
		errorStr = 'Batch process submitted for record building'; 
		if (!IsTestCase)
		{
			sfProcessId = database.executeBatch(new bat_PhyRptLoad(query, month, year, 2), batchSize);
		}

    	return null; 
    }
    
    public PageReference SetupSchedule()
    {
    	sch_PhyRpt schedRpt = new sch_PhyRpt();
    	String sch = '0 0 1 * * ?';
    	if (!IsTestCase)
        	system.schedule('Physician Monthy Report', sch, schedRpt);
        
    	return null; 
    }
 
    public PageReference delAllRpt()
    {
     	rpt_PhyProcessController pDao = new rpt_PhyProcessController();
     	pDao.IsTestCase = IsTestCase;
     	DateTime selDt = DateTime.newInstance(year, month, 1);
        return pDao.delAllRpt(selDt);
    }
    
    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testrpt_PhyController()
    {
    	DateTime selDt = DateTime.now();
		rpt_PhyController cont = new rpt_PhyController();
		cont.IsTestCase = true;
		cont.month = selDt.month();
		cont.year = selDt.year();
		
//		List<rpt_physician__c> phyList = cont.getPhyList();
		PageReference pr =  cont.loadAction();
		pr =  cont.delAllRpt();
		pr =  cont.RunCalc();
		pr = cont.SetupSchedule();
    }   
}