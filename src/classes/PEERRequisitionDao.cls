/* --------------------------------------------------------------------------------
* COMPONENT: PEER Online 2.0
* CLASS: PEERRequisitionDao
* PURPOSE: Data access object for the PEERRequisition object
* CREATED: 12/5/14 Ethos Solutions - www.ethos.com
* Author: Joe DePetro
* --------------------------------------------------------------------------------
*/
public with sharing class PEERRequisitionDao extends BaseDao 
{
	private static final PEERRequisitionDao reqDao = new PEERRequisitionDao();
    private static String NAME = 'PEER_Requisition__c'; 

    public static PEERRequisitionDao getInstance() 
    {
        reqDao.ObjectName = NAME;
        return reqDao; 
    }

    /**
    * @Description This method will get all the fields needed for the CustomerStory and related objects. 
    * @returns a string with comma separated field names. These field names will be all the user accessable fields for the CustomerStory object and related objects.
    */
    public String getAllFieldStr()
    {
        String fldList = getFieldSql(Schema.SObjectType.PEER_Requisition__c.fields.getMap(), '');
        fldList += ', RecordType.Name, Physician__r.Account.Allow_Rapid_Turnaround__c, Physician__r.physician_portal_user__c, Physician__r.Account.PEER_Interactive__c, Physician__r.Account.Report_Build_Type__c ';
        //fldList += ',(Select Id, Name, cgi__c, cgs__c, outcome_date__c, cgi_patient__c, cns_pat_id__c, cgi_patient_score__c, cgi_score__c, cgs_score__c, submitted_type__c From Outcome__r)';
        //fldList += ',(Select Id, Name, eeg_loc__c, eeg_make__c, eeg_notes__c, eeg_rec_date__c, eeg_rec_stat__c, eeg_type__c, eeg_upload__c, insert_method__c, med_no_med__c, med_wo_gen__c, r_eeg_tech_id__c, reeg_type__c, req_date__c, req_stat__c, rpt_stat__c, rpt_test_date__c, sub_alcohol__c, sub_caff__c, reeg_type_mod__c, test_data_flag__c, r_physician_id__c, billing_code__c, billing_other__c, billed_status__c, billing_date__c, patient_age__c, r_eeg_tech_contact__c, eeg_tech_schedule_notes__c, phy_billing_code__c, do_not_send_neuro__c, PEER_Requisition__c, neuroguide_status__c, use_ng_process__c, is_ng_only__c, Use_Rapid_Turnaround__c From rEEG__r)';
        //fldList += ',(Select Id, Name, dx_name__c, primary__c, r_reeg_id__c, r_ref_dx_id__c, rule_out__c, secondary__c, PEER_Drug_mapping__c, Reference_Dx__c, PEER_Requisition__c From rEEG_Dx__r)';
        return fldList;
    }

    /**
    * @Description This method return a PEERRequisition record by id
    * @param recId - PEERRequisition record Id
    * @return PEERRequisition record
    */
    public PEER_Requisition__c getById(String recId)
    {
        List<PEER_Requisition__c> reqList = Database.query('select ' + getAllFieldStr() + ' from PEER_Requisition__c where Id =: recId');
        if (reqList != null && !reqList.isEmpty())
            return reqList[0];
        return null;      
    }

    /**
    * @Description This method return PEERRequisition records by patient id.
    * @param recId - patient id
    * @return list of PEERRequisition records .
    */
    public List<PEER_Requisition__c> getByPatientId(String patId)
    {
        List<PEER_Requisition__c> reqList = new List<PEER_Requisition__c>();
        String query = 'select ' + getAllFieldStr() + ' from PEER_Requisition__c where Patient_Id__c =: patId';
        reqList = Database.query(query);
        return reqList;      
    }

    public String getOrderListQuery(String filterStr, String limitStr, String ordStr)
    {
        String query = '';
        String baseQuery = 'SELECT Id, Name, reeg_phy_billing_code__c, EEG_Status__c, reeg_r_eeg_tech_contact__c, patient_first_name__c, patient_name__c, reeg_reeg_type__c, Physician__c, patient_prov_pat_id__c, Status__c FROM PEER_Requisition__c ';
        
        String whereStr = '';
        system.debug('FILTER: ' + filterStr);
        if(!e_StringUtil.isNullOrEmpty(filterStr))
        {
            whereStr += ' WHERE ' + filterStr;
        } 
        
        query = baseQuery + whereStr;

        system.debug('!!!! query: ' + query);

        if(!e_StringUtil.isNullOrEmpty(ordStr)) query += ' ' + ordStr + ' ';
        else query += ' order by CreatedDate DESC ';
        //-- jdepetro - Mike requested that the sorting be done by req date and not lastmodifieddate
        //else query += ' order by LastModifiedDate DESC';
        
        if(!e_StringUtil.isNullOrEmpty(limitStr)) query += ' ' + limitStr + ' ';
        
        system.debug('RDAO QUERY: ' + query);
        return query;
        
    }

    public Boolean Save(PEER_Requisition__c reqObj)
    {
        //-- if the PEERReq could be commercial or a study (Account can do both types of tests) we need to see what the user chose (Commercial or study).
        if (String.isNotBlank(ReqObj.Study_Name__c))
        {
            if (ReqObj.Study_Name__c.equalsIgnoreCase('standard'))
                ReqObj.RecordTypeId = PEERRequisitionDao.getInstance().getRecordTypeIdByName('Commercial');
            else
                ReqObj.RecordTypeId = PEERRequisitionDao.getInstance().getRecordTypeIdByName('General Study');
        }

        //-- If the EEG was uploaded since the last PEER Req form save there may be values on the PEER Requesition record that have been updated by the server website. 
        //-- those values need to kept in the record and not overwritted here.
        if (reqObj != null && reqObj.Id != null)
        {
            PEER_Requisition__c currentReq = getById(reqObj.Id);

            if (currentReq != null)
            {
                if (String.isNotBlank(currentReq.EEG_Status__c))
                    reqObj.EEG_Status__c = currentReq.EEG_Status__c;
                
                if (String.isNotBlank(currentReq.reeg_eeg_rec_stat__c))
                    reqObj.reeg_eeg_rec_stat__c = currentReq.reeg_eeg_rec_stat__c;
                
                if (String.isNotBlank(currentReq.EEG_Filename__c))
                    reqObj.EEG_Filename__c = currentReq.EEG_Filename__c;
                
                if (String.isNotBlank(currentReq.EEG_Make__c))
                    reqObj.EEG_Make__c = currentReq.EEG_Make__c;
                
                if (currentReq.EEG_Upload_Date__c != null)
                    reqObj.EEG_Upload_Date__c = currentReq.EEG_Upload_Date__c;
            }
        }

        return saveSObject(reqObj);
    }

    public static PEER_Requisition__c getTestObj()
    {
        PEER_Requisition__c obj = new PEER_Requisition__c();
        obj.patient_first_name__c = 'John';
        obj.patient_name__c = 'Smith';
        obj.patient_gender__c = 'Male';
        obj.patient_dob__c = '01/01/1970';
        insert obj;

        return obj;      
    }
}