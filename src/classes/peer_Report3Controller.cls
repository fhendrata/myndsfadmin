//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 3.0
// CLASS: peer_Report3Controller
// PURPOSE: Controller class for the Summary page
// CREATED: 09/03/13 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
public with sharing class peer_Report3Controller extends BaseDrugReportController
{
    public string drugName {get; set;}
    public String Environment {get; set;}
    public string BrainMapUrl {get; set;}
    public boolean IsNeuroRptAvail {get; set;}
    public boolean isNeuroRptOrdered { get; set; }
    public String printUrl {get; set;}

    public peer_Report3Controller()
    {
        getParams();
    }
    
    public peer_Report3Controller(ApexPages.StandardController stdController)
    {
        getParams();
    }
    
    private void getParams()
    {
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
        if (e_StringUtil.isNullOrEmpty(reegId))
            this.reegId = ApexPages.currentPage().getParameters().get('id');
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        rDao = new rEegDao();
    }
    
    public PageReference loadAction()
    {
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            this.reeg = rDao.getById(reegId, true);
            
            if(this.reeg.req_stat__c != 'Complete')
            {
            	PageReference pg = new PageReference('/apex/phy_PeerViewS2?id=' + this.reeg.Id);
            	pg.setRedirect(true);
            	return pg;
            }
         
            baseUrl = '/apex/peer_Report?rid=' + reeg.Id;
            baseReport2Url = '/apex/peer_Report?rid=' + reeg.Id;
            baseReportTabbedUrl = '/apex/peer_Report?rid=' + reeg.Id;
            baseDrugGoupUrl = '/apex/peer_DrugClass?rid=' + reeg.Id;

            //baseDrugGoupUrl = '/apex/wr_PeerDrugClassS2?rid=' + reeg.Id;
            
            if (!e_StringUtil.isNullOrEmpty(pageId))
            {
                DrugTreeMenuUtil.MenuItem menuItem = menuPEERMap.get(pageId);
                if (menuItem != null)
                    drugName = menuItem.drugName;
            }
        }
        else
        {
            this.reeg = new r_reeg__c();
        }  
        
        Environment = 'https://mypeerinteractive.com/';
        BrainMapUrl = rEEGUtil.getNgBrainMapUrl() + '/' + reeg.id + '.png';  
        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        isNeuroRptOrdered = (reeg != null && reeg.neuro_stat__c == 'In Progress');
        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        printUrl = getPrintViewPage();
        return null;    
    }
    
    public PageReference returnAction()
    {
    	return null;
       // return Page.peer_ReportList;
    }
    
    //---temporary
    public String getPrintViewPage()
    {
        String key = '';
        string versionTxt = '';
        if (!e_StringUtil.isNullOrEmpty(this.reegId))
        {
            String part1 = this.reegId.substring(5, 10);
            String part2 = this.reegId.substring(0, 5);
            String part3 = this.reegId.substring(10, reegId.length());
            key = '6H3a2' + part1 + 's' + part2 + part3;
        }
        
        return 'https://mypeerinteractive.com/Public/Download.aspx?key=' + key + '&v=1&p2=1&nr=1&enc=' + reeg.r_patient_id__c;
  
    }
    
    public void orderNeuroReport()
    {
    	reeg.do_not_send_neuro__c = false;
    	isNeuroRptOrdered = true;
    	rDao.Save(reeg, 'ORDER_NEURO_REPORT');
    	
    	
    }
    
   
}