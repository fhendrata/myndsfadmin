//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: patientIntervalBuilder
//   PURPOSE: This class is used to build an interval for a patient using an 
//				outcome record.
//     OWNER: CNSR
//   CREATED: 07/5/10 jdepetro Ethos Solutions - www.ethos.com 
//-------------------------------------------------------------------------------- 
public class patientIntervalBuilder 
{    
    public class dateHolder
    {    	
    	public Date dateVal {get; set;}    	
    	public string cgi {get; set;}
    	public Boolean isCgi {get; set;}
    	public Boolean isMed {get; set;}
    }
    
    public class medHolder
    {    	
    	public Date StartDate {get; set;}
    	public Date EndDate {get; set;}
    	public string medName {get; set;}
    	public string medDesc {get; set;}
    }    
	
	public string className = 'patientIntervalBuilder';
	public boolean IsTestCase {get; set;}
			
	public List<r_interval__c> intList {get; set;}
	public r_interval__c newIntervalObj {get; set;}		
	
	private List<medHolder> medHolderList {get; set;}	 
	private List<dateHolder> dateHolderList {get; set;}
	private List<r_outcome_med__c> outMedList {get; set;}
	private List<patientOutcomeDisp> outList {get; set;}
	
	private String refPatId;	
	private String refPatOwnerId;	
    private List<intervalHolder> intervalHolderList {get; set;}	
	
    public void buildAllIntervalsForPat(SObject obj)
    {
        r_patient__c pat = (r_patient__c)obj;
        buildAllIntervalsForPat(pat.Id, pat.OwnerId);
    }

	public void buildAllIntervalsForPat(string patId, string ownerId)
	{
		refPatId = patId;

        UserDao uDao = new UserDao();
        User user = null;

        if (!e_StringUtil.isNullOrEmpty(ownerId))
        {
            user = uDao.getById(ownerId); 
        }

        if (user != null && user.IsActive)
            refPatOwnerId = ownerId;
        else
        {
            user = uDao.getByUsername('kpolich@cnsr.com');

            system.debug('user == null: ' + user != null);

            if (user != null)
            {
                system.debug('user.Id: ' + user.Id);
                refPatOwnerId = user.Id;
            }
        }

		patientIntervalsDao iDao = new patientIntervalsDao();
        intList = iDao.getByPatId(refPatId);		

        system.debug('### buildAllIntervalsForPat:intList (deletes)' + intList);
	        
		if (intList != null) iDao.realDeleteSObjectList(intList);
		   
        intList = new List<r_interval__c>();
        buildIntervals();         

		//---Save the new list of intervals
		patientIntervalsDao intDao = new patientIntervalsDao();
		intDao.IsTestCase = IsTestCase;
		intDao.saveSObjectList(intList);		
	}
	
	//---Build the intervals
	private void buildIntervals()
	{		
		dateHolderList = new List<dateHolder>();
		medHolderList = new List<medHolder>();
		intervalHolderList = new List<intervalHolder>();
		
        system.debug('### buildIntervals');

		loadOutcomes();
		loadMeds();
    	    	
    	addMedDates();		//---Add the med dates
    	addCgiDates();		//---Add the cgi dates	
    	
    	assembleIntervals();
    	addMedsToIntervals();	
    	
    	for (intervalHolder row : intervalHolderList)
    	{
    		intList.add(row.obj);
    	}
	}
	
	private void loadOutcomes()
	{
		outList = new List<patientOutcomeDisp>();
	
		//---Load the outcomes and reverse direction (early to latest)
		patientOutcomeDao outDao = new patientOutcomeDao();
		List<patientOutcomeDisp> tmpList = outDao.getPatOutcomeDispList(refPatId); 	

        system.debug('### loadOutcomes:tmpList.size: ' + tmpList.size());		
				
	    if (tmpList != null && tmpList.size() > 0)
		{
			//-- reverse list
			if (tmpList.size() > 0)
			{
				for (integer i = tmpList.size()-1; i >= 0; i--)
				{
                    system.debug('### loadOutcomes:AddOutcome:name: ' + tmpList[i].getObj().Name + '  date:' + tmpList[i].getObj().outcome_date__c);
					outList.add(tmpList[i]);
				}
			}
		}			
	}
	
	private void loadMeds()
	{		
		patientOutcomeMedDao patOutMedDao = new patientOutcomeMedDao();
    	outMedList = patOutMedDao.getAllByPatientId(refPatId);
	}
	
	//---Add Med dates
	private void addMedDates()
	{		
		dateHolder dateH = null;
		
		if (outMedList != null)
		{			
			for (r_outcome_med__c row : outMedList)
    		{
    			if (row.med_name__c != null && row.med_name__c != '')
    			{
    				if (row.start_date__c != null)
    				{
    					 dateH = addDateToList(row.start_date__c);
    					 dateH.isMed = true;
    				}
    				if (row.end_date__c != null)
    				{
    					dateH = addDateToList(row.end_date__c);
    					dateH.isMed = true;    					 
    				}  
    				    				
    				addMedToList(row);   //---Add the list of unique meds   				 
    			}
    		}    		
    	}
	}
	
	//---Add Cgi dates
	private void addCgiDates()
	{		
		dateHolder dateH = null;
		
		if (outList != null)
		{			
			for (patientOutcomeDisp row : outList)
    		{
    			r_patient_outc__c ocObj = row.getObj();

                system.debug('### addCgiDates:addDate: ' + ocObj.outcome_date__c);

 				if (ocObj != null)
				{
    				dateH = addDateToList(ocObj.outcome_date__c);
    		   		
					if (!e_StringUtil.isNullOrEmpty(ocObj.cgi__c)) dateH.cgi = ocObj.cgi__c.substring(0,1);    			
    				dateH.isCgi = true;
				}
    		}    		
    	}
	}
	
	//---Add a specific date to the list
	private dateHolder addDateToList(Date inputVal)
	{		
		Boolean isProcessed = false;
		
		//---Create a blank one ready to use
		dateHolder dateH = new dateHolder();
    	dateH.dateVal = inputVal;
    	dateH.isCgi = false;
    	dateH.cgi = '0';
    	dateH.isMed = false;
						
		if (dateHolderList.size() > 0)
		{		
			for (integer i = 0; i < dateHolderList.size(); i++)
    		{
    			if (dateHolderList[i].dateVal > inputVal)
    			{
    				//---Have found its position
    				dateHolderList.add(i, dateH);
    				isProcessed = true;
    				break;
    			}
    			else if (dateHolderList[i].dateVal == inputVal)
    			{
    				//---Equals, already exists
    				dateH = dateHolderList[i];
    				isProcessed = true;
    				break;
    			}    			
    		}
		}		
		
		if (!isProcessed) dateHolderList.add(dateH);
				
		return dateH;
	}
	
	//---Assemble the intervals
	private void assembleIntervals()
	{
		//---No intervals if less than 2 dates
		if (dateHolderList == null || dateHolderList.size() < 2) return;		
		
		intervalHolder currI;		
		dateHolder currH;
		dateHolder anchH;
		dateHolder lastH;
		dateHolder nextH;		
				
		for (integer i = 0; i < dateHolderList.size(); i++)
    	{       		
    		Boolean isFinalH = (i+1 == dateHolderList.size());		
    		currH = dateHolderList[i];
    		lastH = null;
    		if (i > 0) lastH = dateHolderList[i-1];    		  		
    		    		    		
    		if (anchH == null)
    		{        			
    			currI = addAnInterval(currH.dateVal, 'o1');   //---First one   
    			anchH = dateHolderList[i]; 			    			
    		}
    		else
    		{    			 
    			//String cgiReason = 'CGI:' + anchH.cgi + '-' + currH.cgi;
    			String cgiReason = 'CGI';
    			    			
    			//---Check if Med change
    			if (currH.isMed)
    			{   
    				if (currH.isCgi)
    				{    
    					currI.obj.stop_date__c = currH.dateVal;
    					//currI.obj.cgi_value__c = anchH.cgi + '-' + currH.cgi;    	
    					currI.obj.cgi_value__c = currH.cgi;
    					currI.obj.reason__c += 'c2';
   									
    					if (!currH.cgi.equalsIgnoreCase(anchH.cgi)) currI.obj.stop_reason__c = cgiReason;
    					    									
    					if (!isFinalH)
    					{
    						 currI = addAnInterval(currH.dateVal, 'o2');	//---Start a new interval    						 
    						 if (!currH.cgi.equalsIgnoreCase(anchH.cgi)) currI.obj.start_reason__c = cgiReason;
    					}	
    				}
    				else
    				{
    					if (currI.dateCount <= 1)
    					{
    						//---One step interval, no StopBack    						
    						currI.obj.stop_date__c = currH.dateVal; 
    						//currI.obj.cgi_value__c = anchH.cgi + '-' + currH.cgi;
    						currI.obj.cgi_value__c = currH.cgi;
    						currI.obj.reason__c += 'c3';
    						  				    				
    						if (!isFinalH)
    						{
    							currI = addAnInterval(currH.dateVal, 'o3');	//---Start a new interval
    							//currI.obj.start_reason__c = cgiReason;
    						}
    					}
    					else
    					{
    						currI.obj.stop_date__c = lastH.dateVal; 
    						//currI.obj.cgi_value__c = anchH.cgi + '-' + lastH.cgi;
    						currI.obj.cgi_value__c = lastH.cgi;    						
    						currI.obj.reason__c += 'c4';
    						
    						//---StopBack, start new interval
    						currI = addAnInterval(lastH.dateVal, 'o4');	//---Add the StopBack Interval
    						
    						currI.obj.stop_date__c = currH.dateVal;    	
    						//currI.obj.cgi_value__c = lastH.cgi + '-' + currH.cgi;
    						currI.obj.cgi_value__c = currH.cgi;
    						currI.obj.reason__c += 'c5';
    											
    						if (!isFinalH)
    						{
    							 currI = addAnInterval(currH.dateVal, 'o5');	//---Start a new interval
    							 //currI.obj.start_reason__c = cgiReason;
    						}
    					}    					
    				}
    				
    				anchH = dateHolderList[i];
    			}   			
    			else
    			{    				
    				if (!currH.cgi.equalsIgnoreCase(anchH.cgi))
    				{	
    					nextH = null;
    					if (i+1 < dateHolderList.size()) nextH = dateHolderList[i+1]; 
    					
    					if (nextH != null && nextH.cgi.equalsIgnoreCase(currH.cgi))
    					{   
    						//---Is a HOP   			
    						currI.dateCount++;
    						currI.obj.reason__c += 'h6';			   						
    					}
    					else
    					{
    						//---Cgi changed
    						currI.obj.stop_date__c = currH.dateVal;    		
    						//currI.obj.cgi_value__c = anchH.cgi + '-' + currH.cgi;
    						currI.obj.cgi_value__c = currH.cgi;    					
    						currI.obj.reason__c += 'c6';
    						currI.obj.stop_reason__c = cgiReason;
    							    					
    						if (!isFinalH)
    						{
    							currI = addAnInterval(currH.dateVal, 'o6');	//---Start a new interval
    							currI.obj.start_reason__c = cgiReason;
    						}    						
    						
    						anchH = dateHolderList[i];
    					}    					
    				}
    				else
    				{   
    					currI.dateCount++;
    					currI.obj.stop_date__c = currH.dateVal;
    					currI.obj.cgi_value__c = currH.cgi;
    					
    					currI.obj.reason__c += 'h7';    					    					
    				}    						    				
    			}    			
    		}   		 
    	}
	}
	
	//---Add an interval with the start date
	private intervalHolder addAnInterval(Date startDate, String openReason)
	{
		intervalHolder iH = new intervalHolder();
		iH.dateCount = 1;		
		iH.obj = new r_interval__c();
		iH.obj.r_patient__c = refPatId;
		if (!e_StringUtil.isNullOrEmpty(refPatOwnerId))
			iH.obj.OwnerId = refPatOwnerId;
    	iH.obj.start_date__c = startDate;
    	
    	iH.obj.reason__c = openReason;    	
    	    	    	
    	intervalHolderList.add(iH);
    	
    	return iH;
	}	
	
	//---Add a med to the list
	public void addMedToList(r_outcome_med__c obj)
	{
		Boolean isFound = false;
		
		for (integer i = 0; i < medHolderList.size(); i++)
    	{
    		medHolder curH = medHolderList[i];
    		
    		//---Compare by med name and start date
    		if (curH.medName == obj.med_name__c && curH.StartDate == obj.start_date__c)
    		{
    			isFound = true;
    			
    			//---Check if the end date of the med is not null
    			if (obj.end_date__c != null)
    			{
    				if (curH.EndDate == null || curH.EndDate < obj.end_date__c)
    				{    					
    					curH.EndDate = obj.end_date__c;  //---Replace the end date
    				}
    			}
    		}    		
    	}	
    	
    	//---Add the new drug
    	if (!isFound)
    	{
    		medHolder newObj = new medHolder();
    		newObj.medName = obj.med_name__c;
    		newObj.StartDate = obj.start_date__c;
    		newObj.EndDate = obj.end_date__c;    	
    		    		
    		if (obj.med_name__c != null)
    		{
    			newObj.medDesc = obj.med_name__c + '(';
    			
    			if (obj.dosage__c != null) newObj.medDesc += obj.dosage__c; 
    			if (obj.unit__c != null) newObj.medDesc += ' ' + obj.unit__c;            
            	if (obj.frequency__c != null) newObj.medDesc += ' ' + obj.frequency__c; 
            	
            	newObj.medDesc += ')';            	
            }	
    		
    		medHolderList.add( newObj);    		
    	}
	}
	
	
	//---Add the meds to the intervals
	private void addMedsToIntervals()
	{
		//---Process all the meds in the list
		for (medHolder row : medHolderList)
    	{
    		addMedToIntervals(row);
    	}
    	
    	for(intervalHolder row : intervalHolderList)
		{
			row.obj.meds__c = row.activeMedNames;
			
			row.obj.start_reason__c = getStartEndReason(row.obj.start_reason__c, row.startMedNamesA, row.endMedNamesA);
			row.obj.stop_reason__c = getStartEndReason(row.obj.stop_reason__c, row.startMedNamesB, row.endMedNamesB);
			
			if (row.obj.start_reason__c == null || row.obj.start_reason__c == '') row.obj.start_reason__c = 'CGI';
			if (row.obj.stop_reason__c == null || row.obj.stop_reason__c == '') row.obj.stop_reason__c = 'CGI'; 						
		}
	}
	
	private String getStartEndReason(String initReason, String startMeds, String endMeds)
	{
		String returnVal = initReason;
		if (returnVal == null || returnVal == '')
		{
			returnVal = ' ';
		}
		else
		{
			returnVal += ' ';
		}
		
		if (startMeds != null && startMeds != '')
		{
			returnVal += 'Start {' + startMeds + '} ';
		}
		if (endMeds != null && endMeds != '')
		{
			returnVal += 'End {' + endMeds + '}';
		}
		
		return returnVal;	
	}
		
	//---Add a specific med to all the intervals
	private void addMedToIntervals(medHolder medH)
	{
		for(intervalHolder row : intervalHolderList)
		{
			row.StartDate = row.obj.start_date__c;
			row.EndDate = row.obj.stop_date__c;
			
			//---Record Active Name
			if ((row.EndDate == null || medH.StartDate < row.EndDate) && (medH.EndDate == null || medH.EndDate > row.StartDate))
			{
				row.activeMedNames = addName( row.activeMedNames, medH.medDesc);								
			} 			
						
			//---Med start dates
			if (medH.StartDate != null)
			{
				if (row.StartDate != null && medH.StartDate == row.StartDate)
				{
					row.startMedNamesA = addName( row.startMedNamesA, medH.medName);			
				}			
				if (row.EndDate != null && medH.StartDate == row.EndDate)
				{
					row.startMedNamesB = addName( row.startMedNamesB, medH.medName);			
				}			
			}
			
			//---Med end dates
			if (medH.EndDate != null)
			{
				if (row.StartDate != null && medH.EndDate == row.StartDate)
				{
					row.endMedNamesA = addName( row.endMedNamesA, medH.medName);			
				}
				if (row.EndDate != null && medH.EndDate == row.EndDate)
				{
					row.endMedNamesB = addName( row.endMedNamesB, medH.medName);			
				}			
			}
		}
	}
	
	//---Add a name to the list
	private String addName(String currValue, String newName)
	{
		if (currValue != null && currValue != '')
		{
			currValue += ',' + newName;
		}
		else
		{
			currValue = newName;
		}
		
		return currValue;
	}
	
	
    private void addErrorMessage(String value)
    {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, value);
        ApexPages.addMessage( msg);
    }

	//---------------TEST METHODS ---------------------------------
    //
    //-------------------------------------------------------------
    public static testMethod void testController()
    {
    	patientIntervalBuilder ib= new patientIntervalBuilder();
    	
    	patientIntervalBuilder.dateHolder dh = new patientIntervalBuilder.dateHolder();
    	dh.dateVal = date.today();
		dh.cgi = 'test';
		dh.isCgi = true;
		dh.isMed = true;

		patientIntervalBuilder.medHolder mh = new patientIntervalBuilder.medHolder();
		mh.StartDate = date.today();
		mh.EndDate = date.today();
		mh.medName = 'test';
		mh.medDesc = 'test';
    	
    	patientOutcomeDao outDao = new patientOutcomeDao();
    	r_patient_outc__c patOutc = outDao.getTestOutcome();
    	r_patient_outc__c patOutc2 = outDao.getTestOutcome(patOutc.patient__c);
    	
    	patientDao patDao = new patientDao();
    	r_patient__c pat = patDao.getById(patOutc.patient__c);
    	
    	patOutc2 = outDao.getTestOutcome(patOutc.patient__c);
    	r_patient_outc__c obj = new r_patient_outc__c();
    	obj.patient__c = patOutc.patient__c;
    	obj.cgi__c = 'test222';
    	obj.outcome_date__c = Date.today().addDays(10);
    	
    	outDao.saveSObject(obj);
//    	ib.buildIntervalForOutcome(patOutc);

		ib.buildAllIntervalsForPat(patOutc.patient__c, pat.OwnerId);
		
		patientOutcomeMedDao ocmedDao = new patientOutcomeMedDao();
		r_outcome_med__c ocMed = ocmedDao.getTestOutcomeMed(patOutc.Id);
		ib.addMedToList(ocMed);
		
		ib.buildAllIntervalsForPat(patOutc.patient__c, pat.OwnerId);
    	
    	patOutc2 = outDao.getTestOutcome(patOutc.patient__c);
    	obj = new r_patient_outc__c();
    	obj.patient__c = patOutc.patient__c;
    	obj.cgi__c = 'test222';
    	obj.outcome_date__c = Date.today().addDays(-30);
    	
    	outDao.saveSObject(obj);
 //   	ib.buildIntervalForOutcome(patOutc);
    	
    	ib.newIntervalObj = patientIntervalsDao.getTestInterval();
		List<r_interval__c> intList = new List<r_interval__c>();
		intList.add(ib.newIntervalObj);
		ib.intList = intList;
    	    	
    	ib.addErrorMessage('test');
        System.assertEquals( 1, 1);
    }

}