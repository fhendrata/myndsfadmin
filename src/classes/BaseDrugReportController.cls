//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: BaseDrugReportController
// PURPOSE: Base controller class for the Drug Report page
// CREATED: 04/29/11 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public abstract class BaseDrugReportController extends BaseController
{
	public string reegId { get; set; }
	public rEEGDao rDao { get; set; }
	public r_reeg__c reeg { get; set; }
	public string pageId { get; set; }
	public string tabId { get; set; }
	public string cnsTestId { get; set; }
	public string cnsTestId2 {get; set;}
	public string cnsTestId3 {get; set;}
	public string cnsTestId4 {get; set;}
	
	public string baseUrl { get; set; }
	public string baseReport2Url { get; set; }
	public string baseReportTabbedUrl { get; set; }
	public string baseDrugGoupUrl { get; set; }
	public string cnsVarsUrl { get; set; }
	public string neuroUrl {get; set;}
	public string oldReportUrl {get; set;}

    public boolean IsDrugLimitedStudy {  get { return (reeg != null && reeg.RecordTypeId != null && !(reeg.RecordType.Name.equalsIgnoreCase('Commercial')) ); } }
	
	public Map<string, DrugTreeMenuUtil.MenuItem> menuMap {get; set;}
	public Map<string, DrugTreeMenuUtil.MenuItem> menuPEERMap {get; set;}
	public Map<string, DrugTreeMenuUtil.MenuItem> wrMenuMap {get; set;}
	
	public BaseDrugReportController()
    {
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
    	oldReportUrl = reegUtil.getViewUrl(reegId, '5');
    	
    	System.debug('### oldReportUrl:' + oldReportUrl);
    	
    	neuroUrl = reegUtil.getViewUrl(reegId, '3');
    	
    	menuMap = DrugTreeMenuUtil.getMenuMap();
    	menuPEERMap = DrugTreeMenuUtil.getPEERMenuMap();
    	wrMenuMap = DrugTreeMenuUtil.getWrMenuMap();
    }
    
   	public void AddParameters(PageReference pr)
    {
    	if (!e_StringUtil.isNullOrEmpty(reegId))
        	pr.getParameters().put('rid', reegId);
        else if (reeg != null && !e_StringUtil.isNullOrEmpty(reeg.Id))
        	pr.getParameters().put('rid', reeg.Id);
        
        if (!e_StringUtil.isNullOrEmpty(pageId))
        	pr.getParameters().put('pgid', pageId);
        	
       	if (!e_StringUtil.isNullOrEmpty(cnsTestId))
        	pr.getParameters().put('ctid', cnsTestId);
        	
        if (!e_StringUtil.isNullOrEmpty(cnsTestId2))
        	pr.getParameters().put('ctid2', cnsTestId2);
        	
        if (!e_StringUtil.isNullOrEmpty(cnsTestId3))
        	pr.getParameters().put('ctid3', cnsTestId3);
        	
        if (!e_StringUtil.isNullOrEmpty(cnsTestId4))
        	pr.getParameters().put('ctid4', cnsTestId4);
    }

    public Boolean isNeuroComplete 
    {
        get { return (reeg != null && reeg.neuro_stat__c == 'Complete' && !isNeuroNotAvailable); }
        set;
    }
    public Boolean isNeuroNotOrdered
    {
        get { return (reeg != null && reeg.neuro_stat__c == null && reeg.Neuro_Order_Date__c == null && !isNeuroNotAvailable); }
        set;
    }
    public Boolean isNeuroInProgress 
    {
        get { return (reeg != null && reeg.Neuro_Order_Date__c != null && reeg.neuro_stat__c != 'Complete' && !isNeuroNotAvailable); }
        set;
    }

    public Boolean reegNotNull 
    {
        get { return (reeg != null); }
        set;
    }
    public Boolean orderDatNotNull 
    {
        get { return (reeg.Neuro_Order_Date__c != null); }
        set;
    }
    public Boolean neuroStatNotComplete 
    {
        get { return (reeg.neuro_stat__c != 'Complete'); }
        set;
    }
    public Boolean nuroNotAvail 
    {
        get { return (!isNeuroNotAvailable); }
        set;
    }

    public Boolean isNeuroNotAvailable
    {
        get { return (reeg != null && reeg.reeg_type__c == 'Type II'); }
        set;
    }
}