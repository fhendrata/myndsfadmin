public class patientMedListController 
{
    //---Get a reference to the patient object
    private r_patient__c patient;        
    public r_patient__c getPatient()
    {
        if (patient == null)
        {       
            patient = [select id, name, no_meds__c from r_patient__c where id = :ApexPages.currentPage().getParameters().get('id')];
        }               
        return patient;
    }

    private void SavePatient()
    {
        if (patient != null && patient.Id != null) upsert patient;
        else insert patient;
    }
                    
    //---Save current meds action
    public PageReference saveCurrentMedList()
    {
        SavePatient();
        
        if (saveCurrentMeds()) return getDestPage();
        return null;            
    }

    //---Save past meds action
    public PageReference savePastMedList()
    {
        SavePatient();

        if (savePastMeds()) return getDestPage();
        return null;            
    }
    
    //---Cancel action
    public PageReference cancelMedList()
    {
        return getDestPage();
    } 
    
    //---No Meds Checkbox changed
    public PageReference noMedsChanged()
    {                   
        return null;
    }
    
    //---Show or not show the med list
    public Boolean getShowMedList()
    {
        getPatient();

        if (patient == null) return false;
        return !patient.no_meds__c;
    }
         
    //---Get a handle back to the patient page
    private PageReference getDestPage()
    {
        PageReference returnVal = new PageReference( '/' + ApexPages.currentPage().getParameters().get('id'));
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    //---Number of meds in list
    private Integer medCount;
    public Integer getMedCount()
    {
        return medCount;
    }
    
    //---Total number of displayed rows
    private Integer rowCount;
    public Integer getRowCount()
    {
        return rowCount;
    }
    
    //---Build the med list
    private List<PatientMedDisp> meds;
    public List<PatientMedDisp> getMeds()
    {
         if (meds == null)
         {
             meds = new List<PatientMedDisp>();  
             PatientMedDisp med;
             
             Integer ctr = -1;
             
             //---Add the current meds
             for(r_patient_med__c medRow : 
                 [select start_m_pl__c, start_y_pl__c, end_m_pl__c, end_y_pl__c, reason_stop_pl__c, effect_pl__c, dosage__c, drugreview__c, generic__c, is_wo_extended__c, med_name__c, no_dosage__c, reason_stop_text__c, side_effects__c, status__c, symtoms__c, Unit__c, id, name 
                     from r_patient_med__c
                     where r_patient_id__c = :ApexPages.currentPage().getParameters().get('id')])
            {
                ctr++;
                
                med = new PatientMedDisp();
                med.setRowNum( ctr);
                med.Setup( medRow );                                 
                meds.Add( med);
            }
            
            medCount = ctr + 1;  //---ctr is zero based
            
            //---Create blank rows
            for (Integer i = 0; i < 4; i++)
            {
                ctr++;
                
                med = new PatientMedDisp();
                med.setRowNum( ctr);
                r_patient_med__c medRow = new r_patient_med__c();
                med.Setup(medRow);                                   
                meds.Add(med);
            }     
            
            rowCount = ctr + 1;    //---ctr is zero based
        }    
        return meds;
    }

       //---Build the current med list
    private List<PatientMedDisp> currentMeds;
    public List<PatientMedDisp> getCurrentMeds()
    {
         if (currentMeds == null)
         {
             String medStatus = 'Current';
             currentMeds = new List<PatientMedDisp>();  
             PatientMedDisp med;
             Integer ctr = -1;

            if (!patient.no_meds__c)        
            {
                //---Add the current meds
                 for(r_patient_med__c medRow : 
                     [select start_m_pl__c, start_y_pl__c, end_m_pl__c, end_y_pl__c, reason_stop_pl__c, effect_pl__c, dosage__c, drugreview__c, 
                            generic__c, is_wo_extended__c, med_name__c, no_dosage__c, reason_stop_text__c, side_effects__c, status__c, symtoms__c, Unit__c, id, name 
                      from r_patient_med__c 
                      where r_patient_id__c = :ApexPages.currentPage().getParameters().get('id') AND status__c = :medStatus])
                {
                    ctr++;

                    med = new PatientMedDisp();
                    med.setRowNum( ctr);
                    med.Setup( medRow );                                 
                    currentMeds.Add( med);
                }
            }
            medCount = ctr + 1;  //---ctr is zero based

            //---Create blank rows
            for (Integer i = 0; i < 4; i++)
            {
                ctr++;
                med = new PatientMedDisp();
                med.setRowNum( ctr);

                r_patient_med__c medRow = new r_patient_med__c();
                med.Setup(medRow);                                   
                currentMeds.Add(med);
            }     
            rowCount = ctr + 1;    //---ctr is zero based
        }    

        return currentMeds;
    }

      //---Build the past med list
    private List<PatientMedDisp> pastMeds;
    public List<PatientMedDisp> getPastMeds()
    {
         if (pastMeds == null)
         {
            String medStatus = 'Past';
            pastMeds = new List<PatientMedDisp>();  
            PatientMedDisp med;
            Integer ctr = -1;

            if (!patient.no_meds__c)        
            {
                //---Add the current meds
                 for(r_patient_med__c medRow : 
                     [select start_m_pl__c, start_y_pl__c, end_m_pl__c, end_y_pl__c, reason_stop_pl__c, effect_pl__c, dosage__c, drugreview__c, 
                            generic__c, is_wo_extended__c, med_name__c, no_dosage__c, reason_stop_text__c, side_effects__c, status__c, symtoms__c, Unit__c, id, name 
                         from r_patient_med__c
                         where r_patient_id__c = :ApexPages.currentPage().getParameters().get('id') AND status__c = :medStatus])
                {
                    ctr++;

                    med = new PatientMedDisp();
                    med.setRowNum( ctr);
                    med.Setup( medRow );                                 
                    pastMeds.Add( med);
                }
            }
            medCount = ctr + 1;  //---ctr is zero based

            //---Create blank rows
            for (Integer i = 0; i < 4; i++)
            {
                ctr++;
                med = new PatientMedDisp();
                med.setRowNum( ctr);

                r_patient_med__c medRow = new r_patient_med__c();
                med.Setup(medRow);                                   
                pastMeds.Add(med);
            }     
            rowCount = ctr + 1;    //---ctr is zero based
        }    

        return pastMeds;
    }
      
   //---Save current meds 
    public Boolean saveCurrentMeds()
    {
        if (currentMeds != null)
        {
                //---Loop through each row
            for(PatientMedDisp medRow : currentMeds)
            {
                if (medRow != null)
                {
                    if (medRow.getObj() == null)
                    {
                       addMessage( 'medRow.getObj() is null');         
                    }
                    else
                    {
                       SaveMed(medRow.getObj(), true);     
                    }
                }
            }
        }

        return true;
    }

    //---Save past meds
    public Boolean savePastMeds()
    {
        if (pastMeds != null)
        {
                //---Loop through each row
            for(PatientMedDisp medRow : pastMeds)
            {
                if (medRow != null)
                {
                    if (medRow.getObj() == null)
                    {
                       addMessage( 'medRow.getObj() is null');         
                    }
                    else
                    {
                       SaveMed(medRow.getObj(), false);     
                    }
                }
            }
        }

        return true;
    }
    
    //---Save a med row
    private void SaveMed(r_patient_med__c obj, Boolean isCurrentMed)
    {
        // -- med name is not blank
        if (obj != null && obj.med_name__c != null && obj.med_name__c != '')
        {
            Boolean genSet = false;
            Boolean tradeSet = false;
            for(ref_drug__c refMed : [select id, name, generic__c, trade__c
                                    from ref_drug__c rd 
                                    where (rd.name = :obj.med_name__c OR rd.generic__c = :obj.med_name__c) AND rd.is_deleted__c = false])
            {
                if (refMed != null)
                {
                    if (!genSet && refMed != null && refMed.generic__c != null)
                    {
                        obj.generic__c = refMed.generic__c;
                        genSet = true;
                    }
                    if (!tradeSet && refMed.trade__c != null)
                    {
                        obj.med_name__c = refMed.trade__c;
                        tradeSet = true;
                    }
                    if (genSet && tradeSet) break;
                }
            }

            obj.status__c = (isCurrentMed) ? 'Current' : 'Past';
            if (obj.id != null)
            {
                //---Update previous row
                upsert obj;
            }
            else
            {
                //---New row, insert
                if (patient != null && patient.id != null)
                {
                    obj.r_patient_id__c = patient.id;
                    insert obj;
                }
            }                
        }
        else
        {
                //---If the id is not blank and the name is, then delete the old record
            if (obj.id != null) delete obj;
        }    
    }

    
    //---Add a message to the message list
    private void addMessage(String value)
    {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.INFO, value);
        ApexPages.addMessage( msg);
    }

    //---TEST METHODS
    public static testMethod void testController()
    {
    	r_patient__c pat = patientDao.getTestPat();
    	PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('id',pat.id);
    	
        patientMedListController cont = new patientMedListController();
        r_patient__c newPat = new r_patient__c();
        newPat.first_name__c = 'test';
        newPat.Name = 'testLast';
        cont.patient = newPat;
        cont.SavePatient();
        
        cont.patient = null;
        pat = cont.getPatient();
        cont.SavePatient();
        PageReference pRef1 = cont.getDestPage();
        Boolean test = cont.saveCurrentMeds();
        List<PatientMedDisp> medList = cont.getCurrentMeds();
        List<PatientMedDisp> pastmedList = cont.getPastMeds();
        pRef1 = cont.saveCurrentMedList();
        pRef1 = cont.savePastMedList();
        test = cont.savePastMeds();
        PageReference pRef3 = cont.cancelMedList();
        PageReference pRef4 = cont.noMedsChanged();

        cont.patient = new r_patient__c();
        pat = cont.getPatient();

        r_patient_med__c med = new r_patient_med__c();

        medList = cont.getMeds();
        Integer x = cont.getRowCount();
        Integer y = cont.getMedCount();

        cont.SaveMed(med, true);
        cont.getShowMedList();

        cont.addMessage('xxx');

        System.assertEquals( 1, 1);
    }
}