//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: LogUtil
//   PURPOSE: Utility class for all logging and debugging
// 
//     OWNER: CNS Response
//   CREATED: 06/08/09 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class LogUtil
{
    public static void debug(String message)
    {
        System.debug(message);
    }

    public static void debug(String source, String message)
    {
        System.debug(source + '::' + message);
    }

    public static void handleError(String className, String method, Exception ex)
    {
        showMessage( className + ':' + method + ':' + ex);
        ApexPages.addMessages(ex);
    }

    public static void showMessage(String message)
    {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.INFO, message);
        ApexPages.addMessage( myMsg);
    }


    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testrLogUtil()
    {
        LogUtil.debug('test');
        LogUtil.debug('test', 'testMessage');
        LogUtil.showMessage('test');

        System.assertEquals(1, 1);
    }
}