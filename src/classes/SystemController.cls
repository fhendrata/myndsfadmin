Public Class SystemController extends BaseController
{  

    Public SystemController()
    {
        profile = new Profile();
    }

    public SystemController(ApexPages.StandardController stdController)
    {    
    }

    public PageReference rEEGAnalysisPage()
    {
        return new PageReference('/apex/rEEGAnalysisPage');
    }

    public PageReference currentActionsPage()
    {
        return new PageReference('/apex/ActionListPage');
    }
    
    public void runConversion()
    {
    	rEEGConversion conversion = new reegConversion();
    	ID batchprocessid = Database.executeBatch(conversion);
    	
    	//rEEGConversion1 conversion1 = new reegConversion1();
    	//ID batchprocessid1 = Database.executeBatch(conversion1);
    }

    public PageReference sysLogPage()
    {
        return new PageReference('/apex/SysLogPage');
    }
    
    public PageReference intervalPage()
    {
        return new PageReference('/apex/OutcomeIntervalProcessPage');
    }
    
    public PageReference phyReportPage()
    {
        return new PageReference('/apex/PhysicianReportPage');
    }

    //---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
        SystemController cont = new SystemController();
        PageReference pr = cont.rEEGAnalysisPage();
        System.assert(pr != null);
        pr = cont.sysLogPage();
        System.assert(pr != null);        
        pr = cont.currentActionsPage();
        System.assert(pr != null);
    }
}