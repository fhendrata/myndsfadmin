//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: PEERDocController
//   PURPOSE: Controller for the PEER Document view page
// 
//   OWNER: CNS Response
//   CREATED: 03/08/12 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
public class PEERDocController extends BaseController
{
	public PEER_Documentation__c DocObj { get; set; }
	public string userName2 {get; set;}
	
	//---Constructor from the standard controller
    public PEERDocController()
    {    
    }
	
	//---Constructor from the standard controller
    public PEERDocController( ApexPages.StandardController stdController)
    {    
    	userName2 = userName;
        this.DocObj = (PEER_Documentation__c)stdController.getRecord();
    }
    
 	public PageReference loadAction()
    {
    	PEERDocDao dDao = new PEERDocDao();
    	DocObj = dDao.getById(DocObj.Id);
        return null;
    } 

	 //------------ TEST METHODS ------------------------
    private static testmethod void testController()
    {
        PEERDocController docController = new PEERDocController();
        docController.DocObj = new PEER_Documentation__c();
    }

}