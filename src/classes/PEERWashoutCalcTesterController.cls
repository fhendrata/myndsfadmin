//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: PEERWashoutCalcTesterController
// PURPOSE: Controller class for PEERWashoutCalcTest page
// CREATED: 08/10/16 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class PEERWashoutCalcTesterController extends PEERBaseController
{
    private static final Integer MAX_NEW_MED_ROWS = 5;
    public PEER_Requisition__c ReqObj { get; set; }
    public List<ReqOutcomeMedDisp> NewMeds  { get; set; }
    public List<Req_Outcome_Medication__c> CurrentMedList { get; set; } 
    public Integer NewRowCount { get; set; }
    public Integer RowCount { get; set; }
    public Boolean WashoutErr { get; set; }
    public Boolean HasWashOutConfirmError { get; set; }
    public String IsWashedOutStr { get; set; }
    public Map<String, Integer> RefDrugMap { get; set; }
    public String WashoutStatus { get; set; }

    public String AnticipatedEEGTest { get; set; }  //forward = patient_anticipated_eeg_test__c
    public String DesiredEEGTest { get; set; }      //backward = patient_desired_eeg_test__c
    public Boolean DesiredEEGTestError { get; set; }
    public Boolean DesiredEEGTestBlank { get; set; }
    public Boolean IsDesiredEEGTestDateAdequate { get; set; }

    //-- since there is the possibility that the radio button has not yet been selected both of these can be false. that is why there are two of them that are not always opposite.
    public Boolean IsWashedOut {  get { return !String.isBlank(IsWashedOutStr) && IsWashedOutStr == 'yes';  }  }
    public Boolean IsNotWashedOut {  get { return !String.isBlank(IsWashedOutStr) && IsWashedOutStr == 'no';  }  }

    public PEERWashoutCalcTesterController() 
    {
        RefDrugMap = new Map<String, Integer>();
    }

    public PEERWashoutCalcTesterController(ApexPages.StandardController stdController) 
    {
        this.ReqObj = (PEER_Requisition__c)stdController.getRecord();
        IsWashedOutStr = 'yes';

        if (String.isNotBlank(ReqObj.Id))
            ReqObj = PEERRequisitionDao.getInstance().getById(ReqObj.Id);

        ReqObj.test_data_flag__c = (String.isNotBlank(UserInfo.getUserName()) && UserInfo.getUserName().ToLowerCase().contains('@ethos.com'));

        RefDrugMap = new Map<String, Integer>();
        DesiredEEGTestError = false;
        DesiredEEGTestBlank = false;
        IsDesiredEEGTestDateAdequate = true;
    }

    public void loadAction() 
    {
        refreshMeds();
        checkWashoutStatus();
    }

    public Boolean SkipValidation
    {
        get {
            try {
                User u = [select Id, Can_Skip_Med_Validation__c from User where Id =: UserInfo.getUserId()];
                return u.Can_Skip_Med_Validation__c;    
            }catch(Exception e){}
            return false;
        }
    }

    public void loadPreviousMeds()
    {
        Integer oMedCount = 0;
        if (NewMeds == null) NewMeds = new List<ReqOutcomeMedDisp>();         
        Integer ctr = -1;

        if (ReqObj.Id != null)
            CurrentMedList = ReqOutcomeMedDao.getInstance().getByRequisitionId(ReqObj.Id);

        if (CurrentMedList != null && !CurrentMedList.isEmpty())
        {
            for(Req_Outcome_Medication__c oMedRow : CurrentMedList) 
                addMedToNewList(oMedRow, ctr, null);                
        }
        else
        {
            List<r_outcome_med__c> prevMeds = patientOutcomeMedDao.getInstance().getExistingOutcomeMeds(ReqObj.Patient_Id__c);
            if (prevMeds != null && !prevMeds.isEmpty())
            {
                for(r_outcome_med__c oMedRow : prevMeds) 
                    addMedToNewList(new Req_Outcome_Medication__c(), ctr, oMedRow.Id);   
            }
                                                       
        }

        if (!NewMeds.isEmpty()) {
            for (ReqOutcomeMedDisp med : NewMeds) {
                med.TempEndDate = med.EndDate;
            }
        }

        oMedCount = ctr + 1;    //---ctr is zero based   
        RowCount = ctr + 1; //---ctr is zero based 
    }

    private void addMedToNewList(Req_Outcome_Medication__c oMedRow, Integer ctr, String previousMedId)
    {
        ctr++;  
        ReqOutcomeMedDisp oMed = new ReqOutcomeMedDisp(oMedRow, oMedRow.med_name__c, oMedRow.dosage__c, oMedRow.unit__c, oMedRow.frequency__c, oMedRow.start_date__c, oMedRow.end_date__c, previousMedId, oMedRow.start_date_is_estimated__c);
        oMed.setDisplayAction(true);
        oMed.setRowNum(ctr);                  
        NewMeds.Add(oMed);  
    }

    public void addBlankMedRows()
    {
        if (NewMeds == null) NewMeds = new List<ReqOutcomeMedDisp>();    
                
        for (integer i = 0; i < MAX_NEW_MED_ROWS; i++)
        {
            ReqOutcomeMedDisp medDisp = new ReqOutcomeMedDisp();
            Req_Outcome_Medication__c newMed = new Req_Outcome_Medication__c();
            newMed.unit__c = 'mg';  
            medDisp.Obj = newMed;     
            medDisp.setRowNum(newMeds.size());
            NewMeds.add(medDisp);       
        }
        NewRowCount = NewMeds.size();
    }

    public void forwardCalc()
    {
        Date latestDate = Date.today();
        DesiredEEGTestError = false;
        DesiredEEGTestBlank = false;
        IsDesiredEEGTestDateAdequate = true;

        if (!NewMeds.isEmpty())
        {
            loadRefDrugMap();

            for (ReqOutcomeMedDisp medWrapper : NewMeds)
            {
                if (String.isNotBlank(medWrapper.Obj.med_name__c) && medWrapper.obj.Patient_Will_Wash_Out__c)
                {
                    medWrapper.WashoutErr = false;
                    List<String> lastDoseDateSplit = new List<String>();

                    if (String.isNotBlank(medWrapper.TempEndDate) && !medWrapper.TempEndDate.contains(':'))
                        lastDoseDateSplit = medWrapper.TempEndDate.split('/');
                    else if((String.isNotBlank(medWrapper.TempEndDate) && medWrapper.TempEndDate.contains(':')) || String.isBlank(medWrapper.TempEndDate))
                        medWrapper.Obj.end_date__c = null;

                    if (isValidDate(lastDoseDateSplit))
                    {
                        String lastDoseDate = lastDoseDateSplit.get(2) + '-' + lastDoseDateSplit.get(0) + '-' + lastDoseDateSplit.get(1);
                        medWrapper.Obj.end_date__c = Date.valueOf(lastDoseDate);
                    }
                    else if (isInvalidDate(lastDoseDateSplit))
                        medWrapper.WashoutErr = true;

                    medWrapper.Obj.washout_days__c = getWashoutDays(medWrapper.Obj.med_name__c, medWrapper.Obj.washout_days__c);
                    medWrapper.WashedOutDate = (medWrapper.Obj.end_date__c == null) ? Date.today().addDays(Integer.valueOf(medWrapper.Obj.washout_days__c)) : medWrapper.Obj.end_date__c.addDays(Integer.valueOf(medWrapper.Obj.washout_days__c));

                    if (medWrapper.WashedOutDate.daysBetween(latestDate) < 0)
                        latestDate = medWrapper.WashedOutDate;

                    if (String.isNotBlank(medWrapper.TempEndDate) && !medWrapper.TempEndDate.contains(':'))
                        medWrapper.EndDate = medWrapper.TempEndDate;
                }
                else if (String.isNotBlank(medWrapper.Obj.med_name__c) && !medWrapper.obj.Patient_Will_Wash_Out__c)
                {
                    medWrapper.WashoutErr = false;
                    medWrapper.Obj.end_date__c = null;
                    medWrapper.WashedOutDate = null;
                    medWrapper.EndDate = null;
                }

                if (String.isNotBlank(medWrapper.Obj.med_name__c) && !RefDrugMap.containsKey(medWrapper.Obj.med_name__c))
                    medWrapper.IsFoundInDatabase = false;
                else
                    medWrapper.IsFoundInDatabase = true;
            }

            for (ReqOutcomeMedDisp medWrapper : NewMeds)
            {
                if (String.isNotBlank(medWrapper.Obj.med_name__c) && medWrapper.obj.Patient_Will_Wash_Out__c)
                {
                    if (medWrapper.WashedOutDate.daysBetween(latestDate) < 0)
                        latestDate = medWrapper.WashedOutDate;

                    if (medWrapper.Obj.end_date__c == null)
                        medWrapper.Obj.end_date__c = latestDate.addDays((Integer.valueOf(medWrapper.Obj.washout_days__c)) * -1);

                    medWrapper.TempEndDate = medWrapper.EndDate;
                }
                else if (String.isNotBlank(medWrapper.Obj.med_name__c) && !medWrapper.obj.Patient_Will_Wash_Out__c)
                    medWrapper.TempEndDate = null;
            }
        }

        ReqObj.patient_anticipated_eeg_test__c = latestDate;
        List<String> latestDateSplit = String.valueOf(latestDate).split('-');

        if (latestDateSplit.size() == 3)
            AnticipatedEEGTest = latestDateSplit.get(1) + '/' + latestDateSplit.get(2) + '/' + latestDateSplit.get(0);
        else
            AnticipatedEEGTest = String.valueOf(latestDate);

        ReqObj.patient_desired_eeg_test__c = null;
        DesiredEEGTest = '';
    }

    public void backwardsCalc()
    {
        Date todayDate = Date.today();
        DesiredEEGTestError = false;
        DesiredEEGTestBlank = false;
        IsDesiredEEGTestDateAdequate = true;

        if (String.isNotBlank(DesiredEEGTest))
        {
            List<String> desiredEEGTestSplit = DesiredEEGTest.split('/');
            if (isValidDate(desiredEEGTestSplit))
            {
                if (!NewMeds.isEmpty() && Date.parse(DesiredEEGTest) > todayDate.addDays(-1))
                {
                    loadRefDrugMap();

                    for (ReqOutcomeMedDisp medWrapper : NewMeds)
                    {
                        if (String.isNotBlank(medWrapper.Obj.med_name__c) && medWrapper.obj.Patient_Will_Wash_Out__c)
                        {
                            medWrapper.WashoutErr = false;
                            medWrapper.Obj.washout_days__c = getWashoutDays(medWrapper.Obj.med_name__c, medWrapper.Obj.washout_days__c);
                            medWrapper.Obj.end_date__c = Date.parse(DesiredEEGTest).addDays((Integer.valueOf(medWrapper.Obj.washout_days__c)) * -1);
                            List<String> lastDoseDateSplit = new List<String>();
                            String lastDoseDate = '';

                            if (String.isNotBlank(medWrapper.TempEndDate) && !medWrapper.TempEndDate.contains(':'))
                                lastDoseDateSplit = medWrapper.TempEndDate.split('/');

                            if (isValidDate(lastDoseDateSplit))
                                lastDoseDate = lastDoseDateSplit.get(2) + '-' + lastDoseDateSplit.get(0) + '-' + lastDoseDateSplit.get(1);
                            else
                                medWrapper.WashoutErr = true;

                            medWrapper.TempEndDate = medWrapper.EndDate;

                            if (medWrapper.Obj.end_date__c < todayDate && String.isBlank(lastDoseDate))
                            {
                                medWrapper.WashoutErr = true;
                                IsDesiredEEGTestDateAdequate = false;
                            }
                            else
                                medWrapper.WashoutErr = false;

                            if (String.isNotBlank(lastDoseDate) && Date.valueOf(lastDoseDate) > medWrapper.Obj.end_date__c)
                                medWrapper.WashoutErr = true;
                        }
                        else if (String.isNotBlank(medWrapper.Obj.med_name__c) && !medWrapper.obj.Patient_Will_Wash_Out__c)
                        {
                            medWrapper.WashoutErr = false;
                            medWrapper.Obj.end_date__c = null;
                            medWrapper.WashedOutDate = null;
                            medWrapper.EndDate = null;
                            medWrapper.TempEndDate = null;
                        }
                        
                        if (String.isNotBlank(medWrapper.Obj.med_name__c) && !RefDrugMap.containsKey(medWrapper.Obj.med_name__c))
                            medWrapper.IsFoundInDatabase = false;
                        else
                            medWrapper.IsFoundInDatabase = true;
                    }
                }
                else
                {
                    if (!NewMeds.isEmpty() && !(Date.parse(DesiredEEGTest) > todayDate.addDays(-1)))
                        DesiredEEGTestError = true;
                }
            }
            else
                DesiredEEGTestError = true;
        }
        else
            DesiredEEGTestBlank = true;
        
        ReqObj.patient_anticipated_eeg_test__c = null;
        AnticipatedEEGTest = '';
    }

    private Decimal getWashoutDays(String medName, Decimal currentWashoutDaysValue)
    {
        Decimal returnVal = currentWashoutDaysValue;

        if (RefDrugMap.containsKey(medName))
        {
            if (returnVal == null || (RefDrugMap.get(medName) > currentWashoutDaysValue))
                returnVal = RefDrugMap.get(medName);
        }
        else
        {
            if (returnVal == null)
                returnVal = 0;
        }

        return returnVal;
    }

    //public boolean getWashoutConfirmError()
    //{
    //    HasWashOutConfirmError = false;
    //    boolean medsSpecified = isPatOutcMedListed();
    //    HasWashOutConfirmError = ((ReqObj.No_Medication_Verified__c && medsSpecified) || (!ReqObj.No_Medication_Verified__c && !medsSpecified));

    //    return HasWashOutConfirmError;
    //}

    private boolean isPatOutcMedListed()
    {
        for(ReqOutcomeMedDisp nMedRow : NewMeds) 
        {
            if (nMedRow.isEmpty()) return true; 
        }

        return false;
    }

    //-- Checks if there are any misspelled medications and updates the is_misspelled__c checkbox on the Req_Outcome_Medication__c object to true if there are
    public void checkMedNames(List<Req_Outcome_Medication__c> medList, Set<String> medSet)
    {
        if (medList != null && medList.size() > 0)
        {
            for(Req_Outcome_Medication__c med : medList)
            {
                if (med != null && String.isNotBlank(med.Name))
                    med.is_misspelled__c = !medSet.contains(med.Name.ToLowerCase());
            }
        }
    }
    
    public Boolean saveOutcomeMeds()
    {
        Boolean saveOk = true;
        Boolean deleteOk = true;

        if (NewMeds != null && NewMeds.size() > 0)
        {
            List<Req_Outcome_Medication__c> outCMedsToSave = new List<Req_Outcome_Medication__c>();
            List<Req_Outcome_Medication__c> outCMedsToDelete = new List<Req_Outcome_Medication__c>();
            Set<String> dbMedSet = new Set<String>();
            Boolean isTypeM = false;

            for (ref_drug__c med : [SELECT Name, generic__c, trade__c FROM ref_drug__c WHERE is_deleted__c = false])
            {
                if (String.isNotBlank(med.Name))
                    dbMedSet.add(med.Name.toLowerCase());
                if (String.isNotBlank(med.generic__c))
                    dbMedSet.add(med.generic__c.toLowerCase());
                if (String.isNotBlank(med.trade__c))
                    dbMedSet.add(med.trade__c.toLowerCase());
            }

            for (ReqOutcomeMedDisp nMedRow : NewMeds) 
            {
                nMedRow.Obj.PEER_Requisition__c = ReqObj.Id;
                if (nMedRow.isEmpty())
                {
                    if (nMedRow.Obj.Id != null)
                        outCMedsToDelete.add(nMedRow.Obj);
                }
                else
                {
                    if (String.isBlank(nMedRow.Obj.Name))
                        nMedRow.Obj.Name = nMedRow.Obj.med_name__c;

                    if (String.isNotBlank(nMedRow.TempEndDate) && !nMedRow.TempEndDate.contains(':'))
                        nMedRow.EndDate = nMedRow.TempEndDate;

                    nMedRow.Obj.Entry_Type__c = 'Entered through Washout';

                    outCMedsToSave.add(nMedRow.Obj);

                    if ((nMedRow != null && nMedRow.obj != null && String.isNotBlank(nMedRow.obj.Washout_Requirement__c)) && (nMedRow.obj.Washout_Requirement__c.equalsIgnoreCase('Yes') || nMedRow.obj.Washout_Requirement__c.equalsIgnoreCase('Recommended')))
                    {
                        if (!nMedRow.obj.Patient_Will_Wash_Out__c)
                            isTypeM = true;
                    }
                }
            }

            if (isTypeM)
                ReqObj.Patient_will_be_washed_out__c = false;

            if (!outCMedsToSave.isEmpty())
            {
                checkMedNames(outCMedsToSave, dbMedSet);
                try
                {
                    upsert outCMedsToSave;
                }
                catch (DMLException ex)
                {
                    System.debug('outCMedsToSave:ex: ' + ex);
                    saveOk = false;
                }

            }

            if (!outCMedsToDelete.isEmpty())
            {
                try
                {
                    delete outCMedsToDelete;
                }
                catch (DMLException ex)
                {
                    System.debug('outCMedsToDelete:ex: ' + ex);
                    deleteOk = false;
                }
            }

        }
        
        return saveOk && deleteOk;
    }

    private void refreshMeds()
    {
        loadPreviousMeds();
        addBlankMedRows();
    }
    
    public PageReference displayWashoutError()
    {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, 'Will the patient be washed out of all medications?');
        ApexPages.addMessage( msg);
        WashoutErr = true;
        return null;
    }

    private void loadRefDrugMap()
    {
        if (RefDrugMap.isEmpty())
        {
            for (ref_drug__c drug : [Select id, name, half_life_days__c,half_life_hours__c from ref_drug__c where is_deleted__c = false])
            {
                Integer addOnDay = (drug.half_life_hours__c != null && drug.half_life_hours__c > 0) ? 1 : 0;
                Integer woDays = (drug.half_life_days__c != null) ? Integer.valueOf(drug.half_life_days__c) : 0;
                RefDrugMap.put(drug.name, woDays + addOnDay);
            }
        }
    }

    public void clearAction()
    {
        if (!NewMeds.isEmpty())
        {
            for (ReqOutcomeMedDisp medWrapper : NewMeds)
            {
                medWrapper.Obj.end_date__c = null;
                medWrapper.WashoutErr = false;
            }
        }
        IsDesiredEEGTestDateAdequate = true;
        checkWashoutStatus();
        //ReqObj.No_Medication_Verified__c = false;
    }

    private Boolean isValidDate(List<String> splitDateList)
    {
        return (!splitDateList.isEmpty() && splitDateList.size() == 3 && splitDateList.get(0).length() <= 2 && splitDateList.get(1).length() <= 2 
                && splitDateList.get(2).length() == 4 && splitDateList.get(0).isNumeric() && splitDateList.get(1).isNumeric() && splitDateList.get(2).isNumeric());
    }

    private Boolean isInvalidDate(List<String> splitDateList)
    {
        return (!splitDateList.isEmpty() && splitDateList.size() == 3 && (splitDateList.get(0).length() > 2 || splitDateList.get(1).length() > 2 
                || splitDateList.get(2).length() != 4 || !splitDateList.get(0).isNumeric() || !splitDateList.get(1).isNumeric() || !splitDateList.get(2).isNumeric()));
    }

    public void checkWashoutStatus() {
        Boolean isAllMedsWashedOut = true;
        if (NewMeds != null && !NewMeds.isEmpty())
        {
            for (ReqOutcomeMedDisp med : NewMeds)
            {
                if (!med.isEmpty() && med.obj.Washout_Requirement__c != null && (med.obj.Washout_Requirement__c.equalsIgnoreCase('Yes') || med.obj.Washout_Requirement__c.equalsIgnoreCase('Recommended')))
                {
                    if (!med.obj.Patient_Will_Wash_Out__c)
                    {
                        isAllMedsWashedOut = false;
                        break;
                    }
                }
            }
        }
        
        if (isAllMedsWashedOut)
            WashoutStatus = 'All medications will be washed out';
        else
            WashoutStatus = 'All medications will not be washed out';
    }

    //--------------------------------------------------- Below is PEERWashoutCalcTester Page Stuff ONLY ------------------------------------------------------

    public void checkPEERType() {
        Boolean isTypeM = false;
        for (ReqOutcomeMedDisp med : NewMeds)
        {
            if (!med.isEmpty() && med.obj.Washout_Requirement__c != null && (med.obj.Washout_Requirement__c.equalsIgnoreCase('Yes') || med.obj.Washout_Requirement__c.equalsIgnoreCase('Recommended')))
            {
                if (!med.obj.Patient_Will_Wash_Out__c)
                {
                    isTypeM = true;
                    break;
                }
            }
        }
        if (isTypeM)
        {
            ReqObj.Patient_will_be_washed_out__c = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'PEER Type = Type I (m)'));
        }
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'PEER Type = Type I'));
        }
    }
}