//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: dec_RuleDao
// PURPOSE: Data access class for the decision rule object
// CREATED: 05/11/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class dec_RuleDao extends BaseDao
{
	private static String NAME = 'dec_rule__c';    
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.dec_rule__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public dec_rule__c getById(String idInp)
    {
		return (dec_rule__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public List<dec_rule__c> getByActionId(string actionId)
    {
    	List<dec_rule__c> ruleList = null;
    	if (!e_StringUtil.isNullOrEmpty(actionId))
    		ruleList = (List<dec_rule__c>)getSObjectListByWhere(getFieldStr(), NAME, 'r_dec_action__c = \'' + actionId + '\'', 'rule_number__c');
		return ruleList; 
    } 
    
    public List<dec_rule__c> cloneRulesForAction(string oldActionId, string newActionId)
    {
    	List<dec_rule__c> ruleList = new List<dec_rule__c>();
    	
    	List<dec_rule__c> currentRuleList = getByActionId(oldActionId);
    	
    	if (currentRuleList != null && currentRuleList.size() > 0)
    	{
	    	for (dec_rule__c r : currentRuleList)
	    	{
	    		ruleList.add(buildRule(r, newActionId));
	    	}
    	}
    	
    	return ruleList;
    }
    
    private dec_rule__c buildRule(dec_rule__c rule, string newActionId)
    {
    	dec_rule__c returnVal = new dec_rule__c();
    	returnVal.description__c = rule.description__c;
    	returnVal.literal_2__c = rule.literal_2__c;
    	returnVal.literal__c = rule.literal__c;
    	returnVal.Name = rule.Name;
    	returnVal.op_2__c = rule.op_2__c;
    	returnVal.op__c = rule.op__c;
    	returnVal.pri_datatype_2__c = rule.pri_datatype_2__c;
   		returnVal.pri_datatype__c = rule.pri_datatype__c;
   		returnVal.pri_field_2__c = rule.pri_field_2__c;
   		returnVal.pri_field__c = rule.pri_field__c;
   		returnVal.pri_field_label_2__c = rule.pri_field_label_2__c;
   		returnVal.pri_field_label__c = rule.pri_field_label__c;
   		returnVal.pri_object__c = rule.pri_object__c;
   		returnVal.pri_object_label__c = rule.pri_object_label__c;
   		returnVal.r_dec_action__c = newActionId;
   		returnVal.rule_number__c = rule.rule_number__c;
   		returnVal.sec_object_label__c = rule.sec_object_label__c;
   		returnVal.sec_object_Label_2__c = rule.sec_object_Label_2__c;
   		returnVal.sec_object__c = rule.sec_object__c;
   		returnVal.sec_object_2__c = rule.sec_object_2__c;
   		returnVal.sec_field_label__c = rule.sec_field_label__c;
   		returnVal.sec_field_label_2__c = rule.sec_field_label_2__c;
   		returnVal.sec_field__c = rule.sec_field__c;
   		returnVal.sec_field_2__c = rule.sec_field_2__c;
   		returnVal.sec_datatype__c = rule.sec_datatype__c;
   		returnVal.sec_datatype_2__c = rule.sec_datatype_2__c;
    	
    	return returnVal;
    }

	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testRuleDao()
    {
        dec_RuleDao dao = new dec_RuleDao();
        dec_rule__c ruleObj = dec_RuleDao.getTestRule();
        dec_rule__c ruleObj2 = dec_RuleDao.getTestRule();
        
        List<dec_rule__c> ruleList = dao.getByActionId(ruleObj.r_dec_action__c);
        ruleObj = dao.getById(ruleObj.Id);
        
        List<dec_rule__c> ruleList2 = dao.cloneRulesForAction(ruleObj.r_dec_action__c, ruleObj2.r_dec_action__c);
        
    }
        
	public static dec_rule__c getTestRule()
    {
    	dec_action__c action = dec_ActionDao.getTestAction();
    	
    	dec_rule__c testRule = new dec_rule__c(); 
    	testRule.literal__c = 'test';
    	testRule.op__c = '&';
    	testRule.rule_number__c = 1;
    	testRule.pri_field__c = 'test2';
    	testRule.r_dec_action__c = action.Id;
    	insert testRule;
    	
    	return testRule;
    }

}