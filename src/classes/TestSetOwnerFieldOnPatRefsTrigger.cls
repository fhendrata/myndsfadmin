@isTest
private class TestSetOwnerFieldOnPatRefsTrigger {
	
	@isTest static void testNoReferredToField() {
		Patient_Referral__c testPR = new Patient_Referral__c();
		testPR.Date_Time_Called__c = Datetime.now();
		testPR.Call_Type__c = 'Live Answer';
		testPR.CIC_Rep__c = 'George Carpenter';
		testPR.Patient_First_Name__c = 'Test';
		testPR.Patient_Last_Name__c = 'Referral';
		testPR.Referral_Status__c = 'Referred to PEER Provider';
		testPR.Referral_Current_Status__c = 'Contact - Appointment Pending';

		Test.startTest();
		String ex = '';
		try {
			insert testPR;
			ex = 'Did not work';
		} catch (Exception e) {
			ex = e.getMessage();
		}

		System.assert(ex.contains('No contact set in Referred To field'));
	}
	
	@isTest static void testNoPortalUser() {
		//Contact con = ContactDao.getTestPhysician();
		//Patient_Referral__c testPR = new Patient_Referral__c();
		//testPR.Date_Time_Called__c = Datetime.now();
		//testPR.Call_Type__c = 'Live Answer';
		//testPR.CIC_Rep__c = 'George Carpenter';
		//testPR.Patient_First_Name__c = 'Test';
		//testPR.Patient_Last_Name__c = 'Referral';
		//testPR.Referral_Status__c = 'Referred to PEER Provider';
		//testPR.Referral_Current_Status__c = 'Contact - Appointment Pending';
		//testPR.Referred_To__c = con.Id;

		//Test.startTest();
		//String ex = '';
		//try {
		//	insert testPR;
		//	ex = 'did not work';
		//} catch (Exception e) {
		//	ex = e.getMessage();
		//}

		//System.assert(ex.contains('Contact does not have a portal user'));
	}

	@isTest static void testTrigger() {
		Contact con = ContactDao.getTestPhysician();
		Profile p = [SELECT Id FROM Profile WHERE Name =:'Level 2 Physician/Tech Portal Profile']; 
      	User returnObj = new User(Alias = 'newUser', Email='newuser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com',
         ContactId = con.Id);

      	insert returnObj;

      	con.physician_portal_user__c = returnObj.Id;
      	update con;

      	Patient_Referral__c testPR = new Patient_Referral__c();
		testPR.Date_Time_Called__c = Datetime.now();
		testPR.Call_Type__c = 'Live Answer';
		testPR.CIC_Rep__c = 'George Carpenter';
		testPR.Patient_First_Name__c = 'Test';
		testPR.Patient_Last_Name__c = 'Referral';
		testPR.Referral_Status__c = 'Referred to PEER Provider';
		testPR.Referral_Current_Status__c = 'Contact - Appointment Pending';
		testPR.Referred_To__c = con.Id;

		Test.startTest();
		insert testPR;
		Test.stopTest();

		System.assertEquals(returnObj.Id, [Select OwnerId from Patient_Referral__c where Id =: testPR.Id].OwnerId);
	}
}