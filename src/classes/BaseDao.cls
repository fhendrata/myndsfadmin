//--------------------------------------------------------------------------------
// COMPONENT: Base
// CLASS: BaseDao
// PURPOSE: Base Data Access class
// CREATED: 03/26/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
Public abstract Class BaseDao 
{
	public String WILDCARD = '*';
    private static final String MAX_RECORDS = '49999';
	 
    //----------------------------------------- ATTRIBUTES ---------------------------
    public String ObjectName { get; set;}
    public String FieldList { get; set;}
    public String ExtraFieldList { get; set;}
    public String ClassName { get; set;}
    public Boolean IsTestCase { get; set;}    
    public Boolean DeactivateChangeLog { get; set;}
    public Boolean HideDeleted { get; set;}
    public String filter {get; set;}
    public List<RecordType> RecordTypeList { get; set; }

    //----------------------------------------- Constructors -------------------------

    public BaseDao() { }
    
    public BaseDao(String objName) {
        ObjectName = objName;
    }
    
    public BaseDao(String objName,String extraFields) {
        ObjectName = objName;
        ExtraFieldList = extraFields;
    }

    //----------------------------------------- Data Methods ---------------------------
    public String clean(String value)
    {
        return e_StringUtil.clean(value); 
    }

    /**
    * @description This method will get the RecordTypeId of a given Record Type
    * @param rtName - String representing the name of the Record Type
    * @return An Id for the given Record Type
    */
    public Id getRecordTypeIdByName(string rtName)
    {
        if (String.isNotBlank(ObjectName))
        {
            RecordTypeList = new List<RecordType>([SELECT Id FROM RecordType WHERE (IsActive = TRUE AND Name = :rtName AND SObjectType = :ObjectName)]);

            if(!RecordTypeList.isEmpty())
            {
                return RecordTypeList.get(0).Id;
            }
        }
        return null;
    }

    //----------------------------------------- LOG METHODS ---------------------------
    public void handleError(String method, Exception ex)
    {
//        e_LogUtil.handleError( className, method, ex);        
    }

    //----------------------------------------- LOG METHODS ---------------------------
    public void debug(String message)
    {
//        System.debug( className + '::' +  message);
    }

    public void logMethod(String methodName)
    {
//        System.debug( className + '::' +  methodName);
    }
    
    public void log(String message)
    {
 //       System.debug( className + '::' +  'e_out:' + message);
    }

    //----------------------------------------- LOG METHODS ---------------------------
    public void logSaved(SObject obj)
    {
 //       System.debug( className + '::' +  'Saved: ' + obj);
    }

    public void logLoaded(SObject obj)
    {
//        System.debug( className + '::' +  'Loaded: ' + obj);
    }
    
    public List<SObject> getSObjectListByWhere(String fieldList, String objName, String whereClause)
    {
    	return getSObjectListByWhere( fieldList, objName, whereClause, '');
    }
    
    public List<SObject> getSObjectListByWhere(String fieldList, String objName, String whereClause, String orderClause)
    {
        List<SObject> returnVal = null;
        
        if (HideDeleted != null && HideDeleted == true)
        {        	       	
        	if ((whereClause == null || whereClause == ''))        
        	{
        		whereClause = 'is_deleted__c = false';
        	}
        	else
        	{
        		whereClause += ' and is_deleted__c = false';
        	}
        }

		String query = getSql( fieldList, objName, whereClause, orderClause);
		
		if (IsTestCase != null && IsTestCase == true) query += ' LIMIT 2';
		else query += ' LIMIT 9500';
				 
		System.debug( 'e_OUT:' + query);

        try
        {
             returnVal = (List<SObject>)Database.query(query);
        }
        catch (Exception ex)
        {
            handleError( 'getSObjectListByWhere', ex);
        }

        return returnVal;
    }
    
    public List<SObject> getSObjectListByWhere(String fieldList, String objName, String whereClause, String orderClause, String limitSize)
    {
        List<SObject> returnVal = null;
        
        if (HideDeleted != null && HideDeleted == true)
        {        	       	
        	if ((whereClause == null || whereClause == ''))        
        	{
        		whereClause = 'is_deleted__c = false';
        	}
        	else
        	{
        		whereClause += ' and is_deleted__c = false';
        	}
        }

		String query = getSql( fieldList, objName, whereClause, orderClause);
		
		if (IsTestCase != null && IsTestCase == true) query += ' LIMIT 2';
		else 
		{
			query += (!e_StringUtil.isNullOrEmpty(limitSize))? ' LIMIT ' + limitSize : ' LIMIT 9500';
		}
				 
        System.debug('## getSObjectListByWhere:query: ' + query);
        
        try
        {
             returnVal = (List<SObject>)Database.query(query);
        }
        catch (Exception ex)
        {
            handleError( 'getSObjectListByWhere', ex);
        }

        return returnVal;
    }
    
    public String getSql(String fieldList, String objName, String whereClause, String orderClause)
    {
    	String query = 'select ' + fieldList + ' from ' + objName;
    	    	
    	if (!(whereClause == null || whereClause == '')) query += ' where ' + whereClause;	
		if (!(orderClause == null || orderClause == '')) query += ' order by ' + orderClause;
				
		return query;
    }
    
    public SObject getSObjectById(String fieldList, String objName, String idInp)
    {
    	String id = clean(idInp);
        SObject returnVal = null;

		String query = 'select ' + fieldList + ' from ' + objName + ' where id = \'' + id + '\'';

        try
        {
             returnVal = (SObject)Database.query(query);
        }
        catch (Exception ex)
        {
            handleError( 'getSObjectById:' + query , ex);
        }

        return returnVal;
    }

    /**
    * This method gets an sObject with the given Id
    *
    * @param  fieldList a list of fields to get for each sObject
    * @param  recordId the Id of the record
    *
    * @returns an sObject with the given Id
    */
    public sObject getSObjectById(String fieldList, String recordId) {
        if (e_StringUtil.isNullOrEmpty(recordId) || e_StringUtil.isNullOrEmpty(fieldList))
            return null;
        String query = 'select ' + fieldList + ' from ' + objectName + ' where id = ' + quote(recordId);
        return (SObject)Database.query(query);
    }

    /**
    * Gets the list of fields for this object. If there are extra fields added then we must get a fresh list of fields
    * since we do not know if those extra fields are already present
    *
    * @returns a comma separated list of fields for this object
    */
    public String getFieldStrNew() {
        if (e_StringUtil.isNullOrEmpty(FieldList) || !e_StringUtil.isNullOrEmpty(ExtraFieldList)) {
            SObjectType objToken = Schema.getGlobalDescribe().get(ObjectName);
            DescribeSObjectResult objDef = objToken.getDescribe();
            FieldList = getFieldSql(objDef.fields.getMap());
            if(ExtraFieldList != null) FieldList += ',' + ExtraFieldList;
        }
        return FieldList;
    } 

    /**
    * This method returns a list of fields for the sobject
    *
    * @param fMap a map of sObject fields
    *
    * @returns a comma separated String of sObject fields
    */
    public static String getFieldSql(Map<String, Schema.SObjectField> fMap) {
        return getFieldSql(fMap, '');
    }

    /**
    * This method returns a list of fields for the sobject
    *
    * @param fMap a map of sObject fields
    * @param prefix  a prefix for the fields
    *
    * @returns a comma separated String of sObject fields
    */
    public static String getFieldSql(Map<String, Schema.SObjectField> fMap, String prefix) {
        String fieldList = '';
        List<Schema.SObjectField> fTokens = fMap.values();
        
        for( Integer i = 0 ; i < fTokens.size() ; i++ ) {
            Schema.DescribeFieldResult f = fTokens.get(i).getDescribe();
            if( f.isAccessible()) {
                if (!e_StringUtil.isNullOrEmpty(fieldList)) fieldList += ',';
                if (!e_StringUtil.isNullOrEmpty(prefix)) fieldList += prefix + '.';
                fieldList += f.getName();
            }
        }
        return fieldList;
    }
    
    public string getMonthSpanSql(DateTime dt)
    {
    	string returnVal = '';
    	
    	if (dt != null)
    	{
	    	integer y = dt.year();
	    	integer y2 = y;
	    	integer m = dt.month();
	    	integer m2 = m + 1;
	    	
	    	if(m2 == 13)
	    	{
	    		y2 = y + 1;
	    		m2 = 1;
	    	}
	    	
	    	string month = (m < 10) ? '0' + m.format() : m.format();
	    	string month2 = (m2 < 10) ? '0' + m2.format() : m2.format();
	    	string year = y + '';
	    	string year2 = y2 + '';
	    	
	    	returnVal += ' CreatedDate < ' + year2 + '-' + month2 + '-01T12:00:00.000Z';
	    	returnVal += ' and CreatedDate > ' + year + '-' + month + '-01T12:00:00.000Z';
    	
    	}
    	
    	return returnVal;
    }   
    
    public Boolean saveSObject(SObject obj)
    {       
        
        Boolean isSaved = false;

		String action = getAction(obj);

        try
        {
            upsert obj;
            isSaved = true;
        }
        catch (Exception ex)
        {
            handleError( 'saveSObject', ex);
        }
        
        return isSaved;
    }
   
    public Exception saveSObjectEx(SObject obj)
    {       
        
        Boolean isSaved = false;

		String action = getAction(obj);

        try
        {
            upsert obj;
            isSaved = true;
        }
        catch (Exception ex)
        {
            return ex;
        }
        
        return null;
    }
        
    public Boolean saveSObjectList(List<SObject> objList)
    {        
        Boolean isSaved = false;

		//---Build the list of actions for the change log
		List<String> actionList = new List<String>();
		isSaved = true;
		
		if (isSaved && (DeactivateChangeLog == null || DeactivateChangeLog == false))
        {
			if (objList != null && objList.size() >= 0)
			{
				for(SObject row : objList)
				{
					actionList.add( getAction(row));
				}
			}
        } 
		isSaved = saveSObjectListBatching(objList);
        
        if (isSaved && (DeactivateChangeLog == null || DeactivateChangeLog == false))
        {
        	//TODO FIXME Uncomment this prior to deploy to production?
        	//if (chgLogDao == null) chgLogDao = new sys_ChangeLogDao();        	
        	//chgLogDao.record(objList, ObjectName, actionList);
        }

        return isSaved;
    }
    
    private boolean saveSObjectListBatching(List<SObject> objList)
    {
    	boolean returnVal = false;
    	if (objList.size() < 199)
    	{
    		try
	        {
	            upsert objList;
	            returnVal = true;
	        }
	        catch (Exception ex)
	        {
	            handleError( 'saveSObjectList', ex);
	        }
    	}
    	else
    	{
    		integer count = 0;
    		List<SObject> batchList = new List<SObject>();
    		for (SObject row : objList)
    		{
    			batchList.add(row);
    			count++;	
    			if (count > 198)
    			{
    				try
			        {
			            upsert batchList;
			            returnVal = true && returnVal;
			            batchList = new List<SObject>();
			            count = 0;
			        }
			        catch (Exception ex)
			        {
			            handleError( 'saveSObjectList', ex);
			        }
		    	}
    		}
    	}
    
    	return returnVal;
    }
    
    private String getAction(SObject obj)
    {
    	String returnVal = '';
    	
    	String idStr = '' + obj.get('id');
    	
    	if (idStr == null || idStr == '' || idStr.length() < 10)
    	{
    		returnVal = 'INSERT';
    	}
    	else
    	{
    		returnVal = 'UPDATE';
    	}
    	
    	return returnVal;
    }
    
    public Boolean deleteSObject(SObject obj)
    {        
        Boolean isOk = false;

        try
        {
            // Should not allow deleting of data, do a soft delete
            obj.put('is_deleted__c',true);
            upsert obj;
            isOk = true;
        }
        catch (Exception ex)
        {
            handleError( 'deleteSObject', ex);
        }

        return isOk;
    }
    
    public Boolean realDeleteSObject(SObject obj)
    {        
        Boolean isOk = false;

        try
        {
            delete obj;
            isOk = true;
        }
        catch (Exception ex)
        {
            handleError( 'deleteSObject', ex);
        }

        return isOk;
    }
    
    public Boolean deleteSObjectById(String id)
    {        
        Boolean isOk = false;

        try
        {
            // delete id;
            Database.delete(id);
            isOk = true;
        }
        catch (Exception ex)
        {
            handleError( 'deleteSObjectById', ex);
        }

        return isOk;
    }
    
    public boolean realDeleteSObjectList(List<SObject> objList)
    {
    	boolean returnVal = false;
    	if (objList.size() < 199)
    	{
    		try
	        {
	            delete objList;
	            returnVal = true;
	        }
	        catch (Exception ex)
	        {
	            handleError( 'realDeleteSObjectList', ex);
	        }
    	}
    	else
    	{
    		integer count = 0;
    		List<SObject> batchList = new List<SObject>();
    		for (SObject row : objList)
    		{
    			batchList.add(row);
    			count++;	
    			if (count > 198)
    			{
    				try
			        {
			            delete batchList;
			            returnVal = true && returnVal;
			            batchList = new List<SObject>();
			            count = 0;
			        }
			        catch (Exception ex)
			        {
			            handleError( 'realDeleteSObjectList', ex);
			        }
		    	}
    		}
    	}
    
    	return returnVal;
    }
 /*   
    public Boolean realDeleteSObjectList(List<SObject> obj)
    {        
        Boolean isOk = false;

        try
        {
            delete obj;
            isOk = true;
        }
        catch (Exception ex)
        {
            handleError( 'deleteSObjectList', ex);
        }

        return isOk;
    }
 */   
    public Boolean deleteSObjectList(List<SObject> obj)
    {        
        Boolean isOk = false;

        try
        {
        	// the "delete obj fails
            // delete obj;
            
            for(SObject row: obj)
            {
            	deleteSObject(row);
            }
            isOk = true;
            //delete obj;
        }
        catch (Exception ex)
        {
            handleError( 'deleteSObjectList', ex);
        }

        return isOk;
    }
    
    //-----------------------Search methods --------------------------------
    public String addSqlWhere(String fieldName, String fieldVal)
    {
    	String returnVal = '';
    	    	  	
    	if (!(fieldVal == null || fieldVal == ''))    	
    	{
    		//---Clean any illegal characters
    		String inputVal = e_StringUtil.clean(fieldVal.trim()); 
    		
    		//---Build the base sql
    		returnVal += ' and ' + fieldName + ' LIKE \'';
    		
    		if (inputVal.contains(WILDCARD))
    		{
    			returnVal += inputVal.replace(WILDCARD, '%'); 
    		}
    		else
    		{
    			returnVal += inputVal + '%';	
    		}    	
    		
    		returnVal += '\'';	
    	}
    	
    	return returnVal;
    }
    
    public String addSqlWhereMatch(String fieldName, String fieldVal)
    {
    	return ' and ' + fieldName + ' = \'' + fieldVal + '\'';
    }
    
    public String addSqlWhereMatch(String fieldName, Date fieldVal)
    {
		Datetime dTime = datetime.newInstance(fieldVal.year(), fieldVal.month(), fieldVal.day());
		String dtStr = dTime.format('yyyy-MM-dd');
    	
    	return ' AND ' + fieldName + '=' + dtStr;
    }
    
    public String removeLeadingAnd(String inputVal)
    {
    	String returnVal = inputVal;
    	
    	if (returnVal.startsWith(' AND '))
    	{
    		 returnVal = returnVal.substring(5);
    	}
    	else if (returnVal.startsWith(' and '))
    	{
    		 returnVal = returnVal.substring(5);
    	}
    	
    	return returnVal;
    }
    
    public string getInIdsFromObjects(List<SObject> objList)
    {
    	string returnVal;
    	if (objList != null)
    	{
    	  	for(SObject row: objList)
            {
            	if (!e_StringUtil.isNullOrEmpty(returnVal))
            		returnVal = '\'' + row.Id + '\'';
				else
				{
					returnVal += ',\'' + row.Id + '\'';
				}            		
            }	
    	}
    	
    	return returnVal;
    }
    
    public string getWhereInIdsFromIdList(string fieldName, List<string> strList)
    {
    	string returnVal = '';
    	if (strList != null && strList.size() > 0)
    	{
	    	integer returnLimit = 250;
	    	returnVal = fieldName + ' IN (';
	    	if (strList != null)
	    	{
	    		integer count = 0;
	    	  	for(string row: strList)
	            {
	            	returnVal += (count > 0) ? ',\'' : '\'';
			    	returnVal += row.substring(0,15) + '\'';
	            	count++;
	            	if (count > returnLimit) break;  
	            }	
	    	}
			returnVal = returnVal.substring(1);
	    	returnVal += ')';
		}
    	return returnVal;
    }
    

    
    public String quote(String s)
    {
    	return '\'' + s + '\'';
    }
     
    public String escapeStr(String s)
    {
        return String.escapeSingleQuotes(s);
    }
    
    public String addLike(String s)
    {
        return  '\'' + s + '%\'';
    }
}