//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: UserAgreementDao
// PURPOSE: Data Access for User_Agreement__c
// CREATED: 05/03/16 Ethos Solutions - www.ethos.com
// Author: Mitchell Corbin
//--------------------------------------------------------------------------------
public with sharing class UserAgreementDao
{
	public UserAgreementDao()
	{
	}

	public User_Agreement__c getByUser(User currentUser)
	{
		User_Agreement__c returnVal = null;
		List<User_Agreement__c> AgreementList = [SELECT PEER_User__c, Has_Accepted_TOU__c, PEER_Contact__c FROM User_Agreement__c WHERE PEER_User__c = :currentUser.Id];

		if (AgreementList.isEmpty())
			returnVal = new User_Agreement__c(PEER_User__c = currentUser.Id, PEER_Contact__c = currentUser.ContactId);
		else
			returnVal = AgreementList[0];

		return returnVal;
	}
}