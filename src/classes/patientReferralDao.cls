//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: patientReferralDao
//   PURPOSE: Data Access for: Patient Referral
// 
//     OWNER: CNS Response
//   CREATED: 06/05/2014 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------

public class patientReferralDao extends BaseDao 
{
	private static final String NAME = 'Patient_Referral__c';
	private static final String FIELD_LIST = 'Id,OwnerId,Name,CreatedDate,Postal_Code__c,Referral_Current_Status__c,Caller_First_Name__c,Home_Phone_Number__c,Best_Phone_Number__c,Email_Address__c,Address_1__c,Address_2__c,City__c,State__c,Country__c,Other_Country__c,Calling_for__c,Patient_First_Name__c,Patient_Last_Name__c,Referred_To__c,Call_Type__c,Date_Time_Called__c,Referral_Status__c,Appointment_Scheduled__c,Has_Insurance__c,Insurance_Carrier__c,Referral_Source_Type_Other__c,Referral_Source_Other__c,Best_Phone_Number_Type__c,Caller_Interest__c,Caller_Interest_Other__c,Call_Comments__c,PEER_Ordered__c,PEER_Not_Ordered_Reason__c,Meds_on_Board__c,Appointment_Made__c,Appointment_Date_and_Time__c,Phoned_2_days_prior_to_appointment__c,If_not_Reason__c,Appointment_Attended__c,Reason_patient_did_not_attend_appt__c,Fee_Quoted__c,Time_Call_Ended__c,Appt_not_made_Comments__c,Part_of_ad_that_related_to_caller__c,Action_Code__c,CIC_Rep__c,Current_Doctor_City__c,Current_Doctor_Country__c,Current_Doctor_Email__c,Current_Doctor_First_Name__c,Current_Doctor_Last_Name__c,Current_Doctor_Phone__c,Current_Doctor_State__c,Current_Doctor_ZIP_Code__c,Currently_seeing_a_Doctor__c,Diagnosed_with_a_Mental_Health_Disorder__c,Is_Updated__c,Lead_Created_Date_Time__c,Lead_Source_Detail_2__c,Lead_Source_Detail__c,Lead_Source__c,Mental_Health_Disorders__c,OK_to_contact_current_Doctor__c,Other_Insurance_Carrier__c,PEER_Action_Preferred__c,Patient_Insurance_Carrier__c,Phone_2_Type__c,Phone_2__c,Phone_Type__c,Phone__c,Physician_Referral_Status__c,Prescribing_Clinician__c,Relationship_To_Patient__c,Street__c';
	private static final patientReferralDao prDao = new patientReferralDao();

	public patientReferralDao() 
	{
		super(NAME);
	}

	public static patientReferralDao getInstance() 
	{
		return prDao;
	}

	public Patient_Referral__c getById(String idInp) 
	{
		return (Patient_Referral__c)getSObjectById(FIELD_LIST, idInp);
	}

	public String getNameSearchQuery(String firstLastName) 
	{
		firstLastName = escapeStr(firstLastName);
		List<String> tokens = firstLastName.split(' ',2);

		String whereStr;

		String baseQuery = 'Select ' + FIELD_LIST + ' from ' + NAME;

		if(tokens.size() == 1)
        {
            whereStr = ' where (Patient_Last_Name__c like '+ addLike(firstLastName) + ' OR Patient_First_Name__c like ' + addLike(firstLastName) + ')';
        }
        else
        {
            String whereStr1 = ' where ((Patient_Last_Name__c like '+ addLike(tokens[0]) + ' AND Patient_First_Name__c like ' + addLike(tokens[1]) + ') ';
            String whereStr2 = ' OR (Patient_First_Name__c like '+ addLike(tokens[0]) + ' AND Patient_Last_Name__c like ' + addLike(tokens[1]) + '))';
            whereStr = whereStr1 + whereStr2;
        }

        String query;
        if(!e_StringUtil.isNullOrEmpty(filter)) {
        	query = baseQuery + whereStr + ' AND ' + filter + ' AND Physician_Referral_Status__c != ' + quote('Missed') + ' AND Physician_Referral_Status__c != ' + quote('Accepted') + ' order by Physician_Referral_Status__c desc NULLS FIRST, CreatedDate desc NULLS FIRST';
        } else {
        	query = baseQuery + whereStr + ' AND Physician_Referral_Status__c != ' + quote('Missed') + ' AND Physician_Referral_Status__c != ' + quote('Accepted') + 'order by Physician_Referral_Status__c desc NULLS FIRST, CreatedDate desc NULLS FIRST';
        }

        return query;
	}

	public String getMyPatientsReferralQuery() 
	{
		String baseQuery = 'Select ' + FIELD_LIST + ' from ' + NAME;
		String whereStr = '';
		if(filter != null) whereStr += ' Where ' + filter;

		String query = baseQuery + whereStr + ' AND Physician_Referral_Status__c != ' + quote('Missed') + ' AND Physician_Referral_Status__c != ' + quote('Accepted') + ' order by Physician_Referral_Status__c desc NULLS FIRST, CreatedDate desc NULLS FIRST';
		System.debug('getMyPatientsReferralQuery: ' + query);
		return query;
	}

	public List<Patient_Referral__c> getAllSearchResults(String lstName, String frstName) 
	{
		String whereStr = '';
		if (!e_StringUtil.isNullOrEmpty(lstName))
    	{
    		whereStr = ' Patient_Last_Name__c LIKE \'' + lstName + '%\'';
    	}
    	
    	if (!e_StringUtil.isNullOrEmpty(frstName))
    	{
    		if (!e_StringUtil.isNullOrEmpty(whereStr))
    			whereStr += ' AND ';
    		whereStr += ' Patient_First_Name__c LIKE \'' + frstName + '%\'';
    	}
    	whereStr += ' AND Physician_Referral_Status__c != ' + quote('Accepted');
    	return(List<Patient_Referral__c>)getSObjectListByWhere(FIELD_LIST, NAME, whereStr, 'Patient_Last_Name__c, Patient_First_Name__c', '500');
	}

	public Patient_Referral__c getTestPatientReferral() 
	{
		Contact con = ContactDao.getTestPhysician();
		Profile p = [SELECT Id FROM Profile WHERE Name =:'Level 2 Physician/Tech Portal Profile']; 
      	User returnObj = new User(Alias = 'newUser', Email='newuser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com',
         ContactId = con.Id);
      	insert returnObj;

      	con.physician_portal_user__c = returnObj.Id;
      	update con;

		Patient_Referral__c testPR = new Patient_Referral__c();
		testPR.Date_Time_Called__c = Datetime.now();
		testPR.Call_Type__c = 'Live Answer';
		testPR.CIC_Rep__c = 'George Carpenter';
		testPR.Patient_First_Name__c = 'Test';
		testPR.Patient_Last_Name__c = 'Referral';
		testPR.Referral_Status__c = 'Referred to PEER Provider';
		testPR.Referral_Current_Status__c = 'Contact - Appointment Pending';
		testPR.Referred_To__c = con.Id;
		insert testPR;
		return testPR;
	}

	public Patient_Referral__c getTestPatientReferral2() 
	{
		Contact con = ContactDao.getTestPhysician();
		Profile p = [SELECT Id FROM Profile WHERE Name =:'Level 2 Physician/Tech Portal Profile']; 
      	User returnObj = new User(Alias = 'newUser', Email='newuser2@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='newuser2@testorg.com',
         ContactId = con.Id);
      	insert returnObj;

      	con.physician_portal_user__c = returnObj.Id;
      	update con;

		Patient_Referral__c testPR = new Patient_Referral__c();
		testPR.Date_Time_Called__c = Datetime.now();
		testPR.Call_Type__c = 'Live Answer';
		testPR.CIC_Rep__c = 'George Carpenter';
		testPR.Patient_First_Name__c = 'Test';
		testPR.Patient_Last_Name__c = 'Referral';
		testPR.Referral_Status__c = 'Referred to PEER Provider';
		testPR.Referral_Current_Status__c = 'Contact - Appointment Pending';
		testPR.Referred_To__c = con.Id;
		insert testPR;
		return testPR;
	}
}