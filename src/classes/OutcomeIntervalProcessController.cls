//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: OutcomeIntervalProcessController
// PURPOSE: Controller for building the interval records.
// CREATED: 06/21/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------

public with sharing class OutcomeIntervalProcessController extends BaseController
{
	public List<r_patient_outc__c> outcomeList {get; set;}
	public integer nonIntervalOutcomeCount;
	private patientOutcomeDao patOutcomeDao {get; set;}
	private patientIntervalsDao intDao {get; set;}
	public string sfProcessId {get; set;}
	public integer batchId {get; set;}
	public integer batchSize {get; set;}
	public integer numPatients {get; set;}
	public integer numNotProcessed {get; set;}
	
	public PageReference loadAction()
	{
		string whereClause = 'IsDeleted = false and total_outcome_entries__c > 1 and interval_batch__c < 1';
		GlobalPatientDao patDao = new GlobalPatientDao();
		patDao.IsTestCase = this.IsTestCase;
		List<r_patient__c> patList = patDao.getSObjectListByWhere('Id', 'r_patient__c', whereClause, '');
		
		if (patList != null)
			numNotProcessed = patList.size();
		
		return null;
	}
	
	public integer getOutcomeListSize()
	{
		return nonIntervalOutcomeCount;	
	}
	
	public PageReference runProcess()
	{
		string whereClause = 'IsDeleted = false and total_outcome_entries__c > 1';
		string orderClause = 'Id';
		GlobalPatientDao patDao = new GlobalPatientDao();
		patDao.IsTestCase = this.IsTestCase;
		string query = patDao.getSql( 'Id, ownerId, Owner.IsActive, interval_batch__c', 'r_patient__c', whereClause, orderClause);
		
		if (numPatients > 0)
			query += ' limit ' + numPatients;
		else
			query += ' limit 500';
		
		if (!IsTestCase)
			sfProcessId = database.executeBatch(new e_BatchUtil(query,'','','', batchId), batchSize);
		
		return null;
	}
	
	public PageReference deleteAllIntervals()
	{
		intDao = new patientIntervalsDao();
		intDao.IsTestCase = this.IsTestCase;
		List<r_interval__c> allInt = intDao.getTotalIntervals();
		
		intDao.realDeleteSObjectList(allInt);
		return null;
	}
	
	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testOutcomeIntervalProcessController()
    {
   
        OutcomeIntervalProcessController controller = new OutcomeIntervalProcessController();
        controller.IsTestCase = true;
        patientOutcomeDao outcomeDao = new patientOutcomeDao();
        outcomeDao.IsTestCase = true;
        r_patient_outc__c testOutcome = outcomeDao.getTestOutcome();
        testOutcome = outcomeDao.getTestOutcome();
        
        PageReference pr = controller.loadAction();
        integer testVal = controller.getOutcomeListSize();
     	controller.runProcess();
        
        testOutcome = outcomeDao.getTestOutcome();
        controller.runProcess();
        
        controller.deleteAllIntervals();
    }    
}