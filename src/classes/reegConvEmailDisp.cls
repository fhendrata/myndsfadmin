public with sharing class reegConvEmailDisp {

    

    private static final reegConvEmailDisp instance = new reegConvEmailDisp();
    private String mesg = '';    
    private reegConvEmailDisp(){}

    public static reegConvEmailDisp getInstance()
    {
    	return instance;
    }
    
    public void updateMesg(String m)
    {
    	mesg = mesg + '\n' + m;
    }
    
    public String getMesg()
    {
        return mesg;	
    }
    
    public void sendEmail()
    {
    	Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
		message.setSubject('reegConversion Output');
		message.setPlainTextBody(mesg);
		message.setToAddresses(new String[] { 'jswenski@ethos.com' });
		Messaging.sendEmail(new Messaging.Email[] {message});	
    }
    
    

}