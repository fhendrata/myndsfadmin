//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: dec_NG_DevController
// PURPOSE: Development controller for the Online Reporting Rules Based Decision engine
// CREATED: 01/27/12 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class dec_NG_DevController  extends BaseDrugReportController
{
	//public List<dec_action__c> ActionList {get; set;}
	private dec_ActionDao actDao {get; set;}
	private dec_action__c action {get; set;}
	public List<dec_action__c> ActionList {get; set;}
	public string reegId {get; set;}
	public string listType {get; set;}
	public boolean isActionSelectEdit {get; set;}
	DrugTreeMenuUtil menuObj {get; set;}
	public DrugTreeMenuUtil.MenuItem selectedMenuItem {get; set;}
	
	public rEEGXmlUtil.ActionObj[] ActArray {get; set;}
	
	//-- drug category parameters
	public boolean isCategoryEdit {get; set;}
	public List<report_drug_category__c> categoryList {get; set;}
	public report_drug_category__c category {get; set;}
	public string selectCategory {get; set;}
	public string previousSelectCategory {get; set;}
	public string selectCategoryName {get; set;}
	public string resultStr {get; set;}
	
	public dec_NG_DevController()
	{
		actDao = new dec_ActionDao();
		IsTestCase = false;
	}
	
	public dec_NG_DevController(ApexPages.StandardController std)
	{
		actDao = new dec_ActionDao();
		IsTestCase = false;
	}
	
	public PageReference loadAction()
	{
		reegId = ApexPages.currentPage().getParameters().get('rid');
		string edit = ApexPages.currentPage().getParameters().get('edit');
		string editSeq = ApexPages.currentPage().getParameters().get('editSq');
		string listType = ApexPages.currentPage().getParameters().get('l');
		pageId = ApexPages.currentPage().getParameters().get('pgid');
		tabId = ApexPages.currentPage().getParameters().get('tbid');
		
		if (e_StringUtil.isNullOrEmpty(pageId)) pageId = '1';
		if (!e_StringUtil.isNullOrEmpty(tabId)) selectCategory = tabId;
		
		menuObj = new DrugTreeMenuUtil();
		if (menuObj != null && menuObj.MenuList != null)
		{		
			selectedMenuItem = menuObj.MenuList.get(pageId);
		}
		
		isActionSelectEdit = (!e_StringUtil.isNullOrEmpty(edit) && edit.equals('1'));
		
		baseUrl = '/apex/NgDecisionDevPage?rid=' + reegId;
		
		loadCategories();
		loadActionList();
		
		return null;
	}
	
	private void loadCategories()
	{
		DrugCategoryDao dcDao = new DrugCategoryDao();
		categoryList = dcDao.getAll();
		
		if (e_StringUtil.isNullOrEmpty(selectCategory) && categoryList != null && categoryList.size() > 0)
		{
		
		}
	/*
		else
		{
			for (report_drug_category__c row : categoryList)
			{
				if (row.Id == selectCategory)
				{
					selectCategoryName = row.Name;
					break;
				}
			}
		}
	*/	
		category = new report_drug_category__c();
		isCategoryEdit = false;
	}
	
	public PageReference loadActionList()
	{
		if (e_StringUtil.isNullOrEmpty(listType)) listType = 'Active-Pending';
		
		loadActionListByCategory();
		
		return null;
	}
	
	public PageReference loadActionListByCategory()
	{
		if(selectedMenuItem != null)
		{
			
		}
			
		return null;
	}
	
	public PageReference newDecAction()
	{
		PageReference pr = new PageReference('/apex/NgDecisionActionEditPage');
		pr.getParameters().put('rid', reegId);
		pr.getParameters().put('cid', selectCategory);
		
		if (selectedMenuItem != null) 
		{
			
		}
		
		if (!e_StringUtil.isNullOrEmpty(selectCategory))
			pr.getParameters().put('tbid', selectCategory);
		
		return pr;
	}
	
	public PageReference runTestAction()
	{
		PageReference returnVal = null;
		
		return returnVal;
	}
	
	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testActDevCont()
    {
        dec_NG_DevController cont = new dec_NG_DevController();
        cont.IsTestCase = true;
        
		r_reeg__c reeg = rEEGDao.getTestReeg();
		ApexPages.currentPage().getParameters().put('rid', reeg.Id);
        
        dec_action__c actObj = dec_ActionDao.getTestAction();
     	List<dec_action__c> actList = new List<dec_action__c>();
     	actList.add(actObj);
     	
     //	cont.ActionList = actList;
		cont.actDao = new dec_ActionDao();
		cont.action = actObj;
		cont.reegId = reeg.Id;			
		
		cont.isActionSelectEdit = true;
		
		PageReference pr = cont.newDecAction();
		pr = cont.loadAction();
		pr = cont.runTestAction();
    }   
}