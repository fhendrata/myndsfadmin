Public Class rEEGAnalysisDisp
{
    private r_reeg__c obj;
    public r_reeg__c getObj()
    {
        return obj;
    }
    public void setObj(r_reeg__c s)
    {
        obj = s;
    }

    public void Setup(r_reeg__c o, String patDob)
    {
        if (0 != null && patDob != null)
        {
            obj = o;
            DateTime refDate = (o.eeg_rec_date__c != null) ? o.eeg_rec_date__c : o.req_date__c; 
            ShortReqDateStr = (refDate != null) ? refDate.month() + '/' + refDate.day() + '/' + refDate.year() : '';
            String typeMod = (o.reeg_type_mod__c != null) ? o.reeg_type_mod__c : '';
            TestType = (o.reeg_type__c != null) ? o.reeg_type__c.Replace('Type ', '') + typeMod : '';
            Age = rEEGUtil.getPatientAge(patDob, refDate);
            SetCommentary(o);
        }
    }

    public Boolean IsSelected { get; set; }
    public String ShortReqDateStr { get; set; }
    public String TestType { get; set; }
    public String Age { get; set;}
    public String CommentaryAdded { get; set; }

    //---Get the CNS Variables
    public List<rEEGVarDisp> getVars()
    {   
        List<rEEGVarDisp> returnList = new List<rEEGVarDisp>();                
        
        if (obj != null && obj.Id != null)
        {
            List<r_reeg_var__c> curList = getVarsById(obj.id) ;                
    
            if (curList != null)
            {
                //---Load the current test vars
                for(r_reeg_var__c varRow : curList)
                {
                    rEEGVarDisp varItem = new rEEGVarDisp();
                    varItem.Setup(varRow);                                 
                    returnList.Add(varItem);
                }
            }
        }

        return returnList;
    } 

        //---Get the CNS Vars for an rEEG
    private List<r_reeg_var__c> getVarsById(String rEEGId)
    {   
        List<r_reeg_var__c> returnList = new List<r_reeg_var__c>();                

        for(r_reeg_var__c varRow : [select id, name, pat_value__c, pat_zvalue__c
                from r_reeg_var__c
                where
                r_reeg_id__c = :rEEGId])
        {
            returnList.Add( varRow);
        }       

        return returnList;
    } 

    private void SetCommentary(r_reeg__c reeg)
    {
        if (reeg != null)
        {
            if ((reeg.type2_physiologic_changes__c != null) && (reeg.type2_physiologic_changes__c != ''))
                CommentaryAdded = 'Physio changes  ';
            if ((reeg.type2_cur_med_implications__c != null) && (reeg.type2_cur_med_implications__c != ''))
                CommentaryAdded +=  'Med Implications  ';
            if ((reeg.type2_other__c != null) && (reeg.type2_other__c != ''))
                CommentaryAdded += 'Other'; 
        }
    }

    //------------TEST-----------------------------
    public static testMethod void testrEEGAnalysisDisp()
    {
        String testStr = 'test';

        r_patient__c pat = new r_patient__c();
        pat.name = testStr;
        pat.first_name__c = 'firstname';
        pat.dob__c = '01/01/1930';
        insert pat;

        DateTime testDate = System.now();
        rEEGAnalysisDisp obj = new rEEGAnalysisDisp();
        r_reeg__c reeg = new r_reeg__c();
        reeg.r_patient_id__c = pat.Id;
        reeg.eeg_rec_date__c = testDate;
        reeg.req_date__c = testDate;
        reeg.reeg_type__c = 'Type I';
        reeg.reeg_type_mod__c = '(m)';
        insert reeg;

        List<rEEGVarDisp> varDispList = obj.getVars();

        rEEGAnalysisDisp disp = new rEEGAnalysisDisp();
        disp.setObj(reeg);
        disp.Setup(reeg, pat.Id);

        r_reeg__c reeg2 = disp.getObj();

        System.assertEquals(reeg, reeg2);

        Boolean boolTrue = true;
        Boolean boolFalse = false;
        obj.IsSelected = boolTrue;
        System.assertEquals(boolTrue, obj.IsSelected);
        obj.IsSelected = boolFalse;
        System.assertEquals(boolFalse, obj.IsSelected);
        
        obj.ShortReqDateStr = testStr;
        System.assertEquals(testStr, obj.ShortReqDateStr);
        obj.TestType = testStr;
        System.assertEquals(testStr, obj.TestType);
        obj.Age = testStr;
        System.assertEquals(testStr, obj.Age);
    }
}