//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: RecordTypeDao
// PURPOSE: Data access class for RecordTypes
// CREATED: 11/29/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class RecordTypeDao extends BaseDao
{
	private static String NAME = 'RecordType';    
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.RecordType.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public RecordType getById(String idInp)
    {
		return (RecordType)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public RecordType getByRecordTypeName(string rtName)
    {
    	RecordType recordT = null;
    	string whereStr = ' name = \'' + rtName + '\'';
    	List<RecordType> rt = (List<RecordType>)getSObjectListByWhere(getFieldStr(), NAME, whereStr);
    	if(rt.size() == 1) {
    		recordT = rt.get(0);
    	}
		return recordT;
    } 

	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testRecTypeDao()
    {
        RecordTypeDao dao = new RecordTypeDao();
        dao.IsTestCase = true;
        string fieldStr = RecordTypeDao.getFieldStr();

        RecordType rt = dao.getByRecordTypeName('Commercial');
        
      //  RecordType rt = RecordTypeDao.getTestRecordType();
     //   rt = dao.getById(rt.Id);
    }
   
   /* --- DML not allowed on RecordType     
	public static RecordType getTestRecordType()
    {
    	RecordType rt = new RecordType();
    	rt.Description = 'test record Type';
    	rt.Name = 'test RT';

        insert rt;
    	return rt;
    }
    */
}