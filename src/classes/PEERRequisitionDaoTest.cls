//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: PEERRequisitionDaoTest
// PURPOSE: Test class for the PEERRequisitionDao class & methods
// CREATED: 12/5/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class PEERRequisitionDaoTest 
{
	
	private static testmethod void testDaoMethods() 
	{
		PEER_Requisition__c req = PEERRequisitionDao.getTestObj();
		
		Test.startTest();

		PEERRequisitionDao mDao  = PEERRequisitionDao.getInstance();

		PEER_Requisition__c req2 = PEERRequisitionDao.getInstance().getById(req.Id);
		String testStr = PEERRequisitionDao.getInstance().getOrderListQuery('', '10', '');

		r_reeg__c reeg = rEEGDao.getTestReegPO2();

		req2.Patient__c = reeg.r_patient_id__c;

		mDao.Save(req2);

		List<PEER_Requisition__c> reqList = mDao.getByPatientId(reeg.r_patient_id__c);


		Test.stopTest();
	}

	
}