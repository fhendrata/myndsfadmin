public with sharing class wr_PeerReportController extends BaseDrugReportController
{
    public string drugName {get; set;}
    public string BrainMapUrl {get; set;}
    public boolean IsNeuroRptAvail {get; set;}
    public String drugFilter {get; set;}
    public boolean isOkToShowReport {get; set;}

    public wr_PeerReportController()
    {
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        rDao = new rEegDao();
    }

    public PageReference loadAction()
    {
    	System.debug('@@@ loadAction:reegId: ' + reegId + ' pageId:' + pageId);
    	
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            this.reeg = rDao.getById(reegId, true);
            baseUrl = '/apex/wr_PeerReportS2?rid=' + reeg.Id;
            baseReport2Url = '/apex/wr_PeerReportS2?rid=' + reeg.Id;
            baseReportTabbedUrl = '/apex/wr_PeerReportS2?rid=' + reeg.Id;
            baseDrugGoupUrl = '/apex/wr_PeerDrugClassS2?rid=' + reeg.Id;
            BrainMapUrl = '/apex/PEERBrainMapView?rid=' + reeg.id;
            cnsVarsUrl = '/apex/wr_CNSVarsS2?rid=' + reeg.Id + '&ctid=' + reeg.corr_cns_id__c;
            
            Boolean isControlGroup = ((reeg.r_patient_id__r.CA_Control_Group_Type__c != null && reeg.r_patient_id__r.CA_Control_Group_Type__c != '') && (reeg.r_patient_id__r.CA_Control_Group_Type__c == 'Control Group' || reeg.r_patient_id__r.CA_Control_Group_Type__c == 'Not Randomized Group'));
            
            isOkToShowReport = !isControlGroup || getIsSysAdmin();

            if (!e_StringUtil.isNullOrEmpty(pageId))
            {
                DrugTreeMenuUtil.MenuItem menuItem = wrMenuMap.get(pageId);
                if (menuItem != null)
                {
                    drugName = menuItem.drugName;
                 	System.debug('@@@ set drugname: ' + drugName);   
                }
            }
        }
        else
        {
            this.reeg = new r_reeg__c();
        }

        System.debug('### neurostat:' + reeg.neuro_stat__c);
        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        setupDrugFilter();
        return null;
    }

    public void setupDrugFilter()
    {
        DrugUtil du = DrugUtil.getInstance();
        try
        {
           rEEGDxDao rDao = new rEEGDxDao();
           List<r_reeg_dx__c> dxs = rDao.getByrEEGId(this.reeg.Id);
           drugFilter = du.getHexFilter(dxs);
        }
        catch(Exception e)
        {
            drugFilter = '';
        }
    }

    public String getDxEditUrl()
    {
    	if (reeg != null)
    	{
    		String patId = [select r_patient_id__c from r_reeg__c where Id =: reegId].r_patient_id__c;
        	return '/apex/wr_PeerDxEdit?id=' + patId + '&rid=' + this.reegId;
    	}
        else
        {
        	return null;
        }
    }
    
    public String getSummaryUrl()
    {
    	return '/apex/wr_PeerSummaryS2?pgid=' + this.pageId + '&rid=' + this.reegId;
    }

    public PageReference summaryAction()
    {
        PageReference pr = new PageReference('/apex/wr_PeerSummaryS2?pgid=' + this.pageId + '&rid=' + this.reegId);
        pr.setRedirect(true);

        return pr;
    }

    public PageReference returnAction()
    {
        PageReference pr = new PageReference('/apex/wr_PatientListS2');

        pr.setRedirect(true);
        return pr;
    }

    public PageReference printAction()
    {
        PageReference pr = new PageReference('/apex/wr_PeerReportPrintPage?pgid=' + this.pageId + '&rid=' + this.reegId);

        pr.setRedirect(true);
        return pr;
    }

    // --------- TEST METHODS ----------

    private static testmethod void testController()
    {
    	wr_PeerReportController prc = new wr_PeerReportController();
    	prc.returnAction();
    	prc.summaryAction();
    	prc.loadAction();
    	prc.getSummaryUrl();
    }
}