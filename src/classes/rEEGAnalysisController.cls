Public Class rEEGAnalysisController extends BaseController
{
    //-- Controller Parameters
    public r_patient__c Patient { get; set; }
    public String PatLastName { get; set; }
    public String PatFirstName { get; set; }
    public String PatId { get; set; }
    public String ReegSearchName { get; set; }
    public String PhyName { get; set; }

    public Boolean PatInfoPanel { get; set; }
    public Boolean TestOverviewPnl { get; set; }
    public Boolean DxPnl { get; set; }
    public Boolean DrugPnl { get; set; }
    public Boolean BrainMapPnl { get; set; }
    public Boolean CnsVarsPnl { get; set; }
    public Boolean ShowPage { get; set; }
    public Boolean BrainMapReSize { get; set; }
    
    public rEEGAnalysisDisp Reeg1 { get; set; }
    public rEEGAnalysisDisp Reeg2 { get; set; }
    public rEEGAnalysisDisp Reeg3 { get; set; }
    public rEEGAnalysisDisp Reeg4 { get; set; }
    public rEEGAnalysisDisp Reeg5 { get; set; }
    public Integer reegCount;
    private Integer numSelected;

    //-- Constructor
    public rEEGAnalysisController()
    {
        ShowPage = true;
        setClassName2('rEEGAnalysisController');        

        patientList = new List<r_patient__c>();
        Patient = new r_patient__c();
    
        PatInfoPanel = true;
        BrainMapReSize = false;

        selectPatient();
    }

    //---- Object Lists --------------
    private List<r_patient__c> patientList;
    public List<r_patient__c> getPatientList()
    {
        return patientList;
    }

    private List<rEEGAnalysisDisp> reegDispList;
    public List<rEEGAnalysisDisp> getReegList()
    {
        return reegDispList;
    }

    //-------------- Action methods ---------------------

    public PageReference reegCbChanged()
    { 
        return null;
    }

    public PageReference patSearchAction()
    { 
        HideAll();
        setPatientList(true);
        showPatientResults = true;
        return null;
    }

    public PageReference rEEGSearchAction()
    { 
        HideAll();
        if (FindReeg(ReegSearchName)) showReegResults = true;
        else showPatientResults = true;

        return null;
    }

    public PageReference phySearchAction()
    { 
        HideAll();
        setPatientList(false);
        showPatientResults = true;
        return null;
    }

    //--------------------------------------------------------------------------------------

    public Boolean getSysAdminOrProdMgr()
    {
        return getIsMgrLevel3();
    }

    public void selectPatient()
    {
        String pid = System.currentPageReference().getParameters().get('patId');
        PatId = (pid != null && pid != '' && pid != 'null') ? pid : PatId;
        
        if (PatId != null) loadPatient();
    }

    private void loadPatient()
    {
        reegDispList = new List<rEEGAnalysisDisp>();
        loadReegsForPatId(PatId);
        HideAll();
        showReegResults = true;
    }

    private void loadReegsForPatId(String pId)
    {
        reegCount = 0;
        List<r_reeg__c> reegList = new List<r_reeg__c>();

        if (pId != '') Patient = getPatById(pId);

        if (Patient != null)
        {
            for(r_reeg__c row : 
                [select id, name, req_date__c, eeg_rec_date__c, reeg_type__c, reeg_type_mod__c, rpt_test_med__c, corr_cns_id__c,
                    r_eeg_tech_contact__c, rpt_neuro_abnormal__c, sub_alcohol__c, sub_caff__c, sub_tobac__c, vital_weight_req__c,
                    vital_bp_req_long__c, vital_pulse_req__c, type2_physiologic_changes__c, type2_cur_med_implications__c, 
                    type2_other__c, lamotrigine_visible__c
                    from r_reeg__c
                    where r_patient_id__c = :Patient.Id order by req_date__c desc])
            {
                reegList.Add(row);
            }

            String dob = '';
            if (Patient != null) dob = Patient.dob__c;
            reegDispList = new rEEGAnalysisDisp[reegList.size()];

            for(r_reeg__c reeg : reegList)
            {
                rEEGAnalysisDisp disp = new rEEGAnalysisDisp();
                disp.Setup(reeg, dob);
                reegDispList[reegCount] = disp;
                reegCount++;
            }
        }

        setupObj();   
    }

    private void setupObj()
    {
        //-- set up the med table sensitivities and bio predominance 
        stimSens = new String[5];
        aconvSens = new String[5];
        adepSens = new String[5];
        bbSens = new String[5];
        stimBio = new String[5];
        aconvBio = new String[5];
        adepBio = new String[5];
        bbBio = new String[5];

        //-- Load the reeg's, DX's and med tables
        if (reegCount > 0) { Reeg1 = reegDispList[0]; Reeg1Dx = getDxByReegId(Reeg1.getObj().Id); SetMedsForReeg(0);}
        if (reegCount > 1) { Reeg2 = reegDispList[1]; Reeg2Dx = getDxByReegId(Reeg2.getObj().Id); SetMedsForReeg(1);}
        if (reegCount > 2) { Reeg3 = reegDispList[2]; Reeg3Dx = getDxByReegId(Reeg3.getObj().Id); SetMedsForReeg(2);}
        if (reegCount > 3) { Reeg4 = reegDispList[3]; Reeg4Dx = getDxByReegId(Reeg4.getObj().Id); SetMedsForReeg(3);}
        if (reegCount > 4) { Reeg5 = reegDispList[4]; Reeg5Dx = getDxByReegId(Reeg5.getObj().Id); SetMedsForReeg(4);}
    }

    private Boolean FindReeg(String searchCriteria)
    {
        List<r_reeg__c> searchResultList = getReegCriteriaResults(searchCriteria);
        Boolean returnVal = false;

        if (searchResultList != null && searchResultList.size() > 0)
        {
                //-- 1 result found. Find parent (patient) and load all reeg's into reeg disp list
            if (searchResultList.size() == 1)
            {
                Patient = getPatById(searchResultList[0].r_patient_id__c);
                LoadReegsForPatId(Patient.Id);
                returnVal = true;
            }
                //-- multiple rEEG's found. Show all patients in pat list, clear reeg disp list
            else
            { 
                //-- clear reeg disp list
                reegDispList = new List<rEEGAnalysisDisp>();

                //-- find the patient for all reeg's found
                List<String> patIdList = new List<String>();
                for (r_reeg__c row : searchResultList)
                {
                    patIdList.Add(row.r_patient_id__c);
                }

                //-- clear previous list
                patientList = new List<r_patient__c>();

                //-- add all found patient to patient list for page 
                for (String id : patIdList)
                {
                    patientList.Add(getPatById(id));
                }
            }
        }

        return returnVal;
    }

    private Boolean showPatientResults;
    public Boolean getShowPatientResults()
    {
        return showPatientResults;
    }

    private Boolean showReegResults;
    public Boolean getShowReegResults()
    {
        return showReegResults;
    }

    private void setPatientList(Boolean IsPatSearch)
    {
        patientList = new List<r_patient__c>();

        if (IsPatSearch)
        {
            if (PatLastName != '')
                addResultsToPatientList( getPatCriteriaResults(PatLastName, 'L')); 
            if (PatFirstName != '')
                addResultsToPatientList( getPatCriteriaResults(PatFirstName, 'F')); 
            if (PatId != '')
                addResultsToPatientList( getPatCriteriaResults(PatId, 'I')); 
        }
        else
        {
            if (getSearchStr(PhyName) != '')
                addResultsToPatientList( getPhyCriteriaResults(PhyName)); 
        }
    }

    private void addResultsToPatientList(List<r_patient__c> resultList)
    {
        for (r_patient__c row : resultList)
        {
            patientList.Add(row);
        }
    }

    private void addResultsToPatientList(List<Contact> phyList)
    {
        if (phyList != null)
        {
            List<r_patient__c> tempPatList = new List<r_patient__c>();

            for(Contact row : phyList)
            {
                tempPatList = [select Id, name, first_name__c, dob__c, Gender__c, physician__c from r_patient__c where physician__c = :row.Id];
                for(r_patient__c patRow : tempPatList)
                {
                    patientList.Add(patRow);
                }

                tempPatList = new List<r_patient__c>();
            }
        }
    }

    //-------------- SOQL Search query methods -------------------------

    private r_patient__c getPatById(String id)
    {
        r_patient__c returnVal;
        for(r_patient__c row : [select Id, name, first_name__c, dob__c, Gender__c, physician__c from r_patient__c where Id = :id limit 100])
        {
            returnVal = row;
            break;
        }

        return returnVal;
    }

    private r_reeg__c getReegById(String id)
    {
        r_reeg__c returnVal;
        for(r_reeg__c row : [select Id, name, r_patient_id__c, eeg_rec_date__c, r_physician_id__c from r_reeg__c where Id = :id])
        {
            returnVal = row;
            break;
        }

        return returnVal;
    }

    private List<r_patient__c> getPatCriteriaResults(String criteria, String searchType )
    {
        List<r_patient__c> returnList = new List<r_patient__c>();
        if (searchType == 'L')
        {
            for(r_patient__c row : [select Id, name, first_name__c, dob__c, Gender__c, physician__c, CNS_ID__c, prov_pat_id__c from r_patient__c where name like :getSearchStr(criteria) limit 100])
            {
                returnList.Add(row);
            }
        }
        else if (searchType == 'F')
        {
            for(r_patient__c row : [select Id, name, first_name__c, dob__c, Gender__c, physician__c, CNS_ID__c, prov_pat_id__c from r_patient__c where first_name__c like :getSearchStr(criteria) limit 100])
            {
                returnList.Add(row);
            }
        }
        else
        {
            for(r_patient__c row : [select Id, name, first_name__c, dob__c, Gender__c, physician__c, CNS_ID__c, prov_pat_id__c from r_patient__c where (CNS_ID__c like :getSearchStr(criteria) OR prov_pat_id__c like :getSearchStr(criteria)) limit 100])
            {
                returnList.Add(row);
            }
        }

        return returnList;
    }

    private List<r_reeg__c> getReegCriteriaResults(String criteria)
    {
        return [select Id, name, r_patient_id__c, eeg_rec_date__c, r_physician_id__c from r_reeg__c where name like :getSearchStr(criteria) limit 50];
    }

    private List<Contact> getPhyCriteriaResults(String criteria)
    {
        return [select Id, name from Contact where Lastname like :getSearchStr(criteria) limit 100];
    }

    private String getSearchStr(String value)
    {
        String returnVal = '';
        if (value != null && value != '')
            returnVal = value.replace('*', '%');

        return returnVal;
    }

    private void SavePat()
    {
        if (Patient.Id != null) upsert Patient;
        else insert Patient;
    }

    //--------- Visibility -------------------------

    private void HideAll()
    {
        showReegResults = false;
        showPatientResults = false;
    }

    //--------- DX -------------------------
    public List<r_reeg_dx__c> Reeg1Dx { get; set; }
    public List<r_reeg_dx__c> Reeg2Dx { get; set; }
    public List<r_reeg_dx__c> Reeg3Dx { get; set; }
    public List<r_reeg_dx__c> Reeg4Dx { get; set; }
    public List<r_reeg_dx__c> Reeg5Dx { get; set; }

    private List<r_reeg_dx__c> getDxByReegId(String id)
    {
        List<r_reeg_dx__c> dxList = new List<r_reeg_dx__c>();  
    
        for(r_reeg_dx__c dxRow : 
             [select dx_name__c, primary__c, r_ref_dx_id__c, rule_out__c, secondary__c, id, name 
                 from r_reeg_dx__c
                 where r_reeg_id__c = :id])
        {
            dxList.Add(dxRow);
        } 
        
        return getSortedDxList(dxList);
    }

    private List<r_reeg_dx__c> getSortedDxList(List<r_reeg_dx__c> dxList)
    {
        List<r_reeg_dx__c> newList = new List<r_reeg_dx__c>();
        
        if (dxList != null && dxList.size() > 0)
        {
            List<r_reeg_dx__c> pList = new List<r_reeg_dx__c>();
            List<r_reeg_dx__c> sList = new List<r_reeg_dx__c>();
            List<r_reeg_dx__c> roList = new List<r_reeg_dx__c>();
        
            for(r_reeg_dx__c dxRow : dxList)
            {
                if (dxRow.primary__c) pList.Add(dxRow);
                if (dxRow.secondary__c) sList.Add(dxRow);
                if (dxRow.rule_out__c) roList.Add(dxRow);
            } 
            
            if (pList.size() > 0) for(r_reeg_dx__c pRow : pList) { newList.Add(pRow); }
            if (sList.size() > 0) for(r_reeg_dx__c sRow : sList) { newList.Add(sRow); }
            if (roList.size() > 0) for(r_reeg_dx__c roRow : roList) { newList.Add(roRow); }
        }
        return newList;
    }

    private rEEGAnalysisDisp getReegDisp(Integer reegNum)
    {
        if (reegNum == 0) return Reeg1;
        else if (reegNum == 1) return Reeg2;
        else if (reegNum == 2) return Reeg3;
        else if (reegNum == 3) return Reeg4;
        else if (reegNum == 4) return Reeg5;
        else return null; 
    }

    public String getReegColWidth1()
    {
        if (Reeg1 != null && Reeg1.IsSelected) return getReegColWidth();
        else return '1%';
    }

    public String getReegColWidth2()
    {
        if (Reeg2 != null && Reeg2.IsSelected) return getReegColWidth();
        else return '1%';
    }

    public String getReegColWidth3()
    {
        if (Reeg3 != null && Reeg3.IsSelected) return getReegColWidth();
        else return '1%';
    }

    public String getReegColWidth4()
    {
        if (Reeg4 != null && Reeg4.IsSelected) return getReegColWidth();
        else return '1%';
    }

    public String getReegColWidth5()
    {
        if (Reeg5 != null && Reeg5.IsSelected) return getReegColWidth();
        else return '1%';
    }

    public String getReegColWidth()
    {
        Integer numSelected = 0;
        if (reegCount > 0 && Reeg1.IsSelected) numSelected++;
        if (reegCount > 1 && Reeg2.IsSelected) numSelected++;
        if (reegCount > 2 && Reeg3.IsSelected) numSelected++;
        if (reegCount > 3 && Reeg4.IsSelected) numSelected++;
        if (reegCount > 4 && Reeg5.IsSelected) numSelected++;
        
        if (numSelected > 0) return String.valueOf(100 / numSelected) + '%';
        else return '';
    }

    //----------------- Brain Map ---------------------------------------------------------------
    public String getBrainMapUrl1() { return 'https://mypeerinteractive.com/BrainMap/Prodb/' + Reeg1.getObj().id + '.png';}
    public String getBrainMapUrl2() { return 'https://mypeerinteractive.com/BrainMap/Prodb/' + Reeg2.getObj().id + '.png';}
    public String getBrainMapUrl3() { return 'https://mypeerinteractive.com/BrainMap/Prodb/' + Reeg3.getObj().id + '.png'; }
    public String getBrainMapUrl4() { return 'https://mypeerinteractive.com/BrainMap/Prodb/' + Reeg4.getObj().id + '.png';}
    public String getBrainMapUrl5() { return 'https://mypeerinteractive.com/BrainMap/Prodb/' + Reeg5.getObj().id + '.png';}
   
    //--------------------- Med Sens/Group Tables ----------------------------------------------
    private String[] stimSens;
    private String[] aconvSens;
    private String[] adepSens;
    private String[] bbSens;

    private String[] stimBio;
    private String[] aconvBio;
    private String[] adepBio;
    private String[] bbBio;

    private void SetMedsForReeg(Integer reegNum)
    {
        rEEGAnalysisDisp reegDispObj = getReegDisp(reegNum);

        if (reegDispObj != null && reegDispObj.getObj().id != null)
        { 
            for(r_reeg_group__c medRow : [Select Name, bio_predom__c, sensitivity__c
                    from r_reeg_group__c
                    where
                    r_reeg_id__c = :reegDispObj.getObj().id])
             {
                if (medRow.Name == 'ADEP')
                {
                    adepSens[reegNum] = medRow.sensitivity__c;
                    adepBio[reegNum] = medRow.bio_predom__c;
                }
                else if (medRow.Name == 'STIM')
                {
                    stimSens[reegNum] = medRow.sensitivity__c;
                    stimBio[reegNum] = medRow.bio_predom__c;
                }
                else if (medRow.Name == 'ACONV')
                {
                    aconvSens[reegNum] = medRow.sensitivity__c;
                    aconvBio[reegNum] = medRow.bio_predom__c;
                }
                else if (medRow.Name == 'BB')
                {
                    bbSens[reegNum] = medRow.sensitivity__c;
                    bbBio[reegNum] = medRow.bio_predom__c;
                }
             }
        }
    }
    
    public String getMedSenBB1() { return bbSens[0]; } 
    public String getMedSenACONV1() { return aconvSens[0]; } 
    public String getMedSenADEP1() { return adepSens[0]; } 
    public String getMedSenSTIM1() { return stimSens[0]; } 
    public String getMedBioBB1() { return bbBio[0]; } 
    public String getMedBioACONV1() { return aconvBio[0]; } 
    public String getMedBioADEP1() { return adepBio[0]; } 
    public String getMedBioSTIM1() { return stimBio[0]; } 

    public String getMedSenBB2() { return bbSens[1]; } 
    public String getMedSenACONV2() { return aconvSens[1]; } 
    public String getMedSenADEP2() { return adepSens[1]; } 
    public String getMedSenSTIM2() { return stimSens[1]; } 
    public String getMedBioBB2() { return bbBio[1]; } 
    public String getMedBioACONV2() { return aconvBio[1]; } 
    public String getMedBioADEP2() { return adepBio[1]; } 
    public String getMedBioSTIM2() { return stimBio[1]; } 

    public String getMedSenBB3() { return bbSens[2]; } 
    public String getMedSenACONV3() { return aconvSens[2]; } 
    public String getMedSenADEP3() { return adepSens[2]; } 
    public String getMedSenSTIM3() { return stimSens[2]; } 
    public String getMedBioBB3() { return bbBio[2]; } 
    public String getMedBioACONV3() { return aconvBio[2]; } 
    public String getMedBioADEP3() { return adepBio[2]; } 
    public String getMedBioSTIM3() { return stimBio[2]; } 

    public String getMedSenBB4() { return bbSens[3]; } 
    public String getMedSenACONV4() { return aconvSens[3]; } 
    public String getMedSenADEP4() { return adepSens[3]; } 
    public String getMedSenSTIM4() { return stimSens[3]; } 
    public String getMedBioBB4() { return bbBio[3]; } 
    public String getMedBioACONV4() { return aconvBio[3]; } 
    public String getMedBioADEP4() { return adepBio[3]; } 
    public String getMedBioSTIM4() { return stimBio[3]; } 

    public String getMedSenBB5() { return bbSens[4]; } 
    public String getMedSenACONV5() { return aconvSens[4]; } 
    public String getMedSenADEP5() { return adepSens[4]; } 
    public String getMedSenSTIM5() { return stimSens[4]; } 
    public String getMedBioBB5() { return bbBio[4]; } 
    public String getMedBioACONV5() { return aconvBio[4]; } 
    public String getMedBioADEP5() { return adepBio[4]; } 
    public String getMedBioSTIM5() { return stimBio[4]; } 
    
    public String getMedSenStyleBB1() { return rEEGStyleUtil.getSensitivityStyle(getMedSenBB1()); } 
    public String getMedSenStyleACONV1() { return rEEGStyleUtil.getSensitivityStyle(getMedSenACONV1()); } 
    public String getMedSenStyleADEP1() { return rEEGStyleUtil.getSensitivityStyle(getMedSenADEP1()); } 
    public String getMedSenStyleSTIM1() { return rEEGStyleUtil.getSensitivityStyle(getMedSenSTIM1()); } 

    public String getMedSenStyleBB2() { return rEEGStyleUtil.getSensitivityStyle(getMedSenBB2()); } 
    public String getMedSenStyleACONV2() { return rEEGStyleUtil.getSensitivityStyle(getMedSenACONV2()); } 
    public String getMedSenStyleADEP2() { return rEEGStyleUtil.getSensitivityStyle(getMedSenADEP2()); } 
    public String getMedSenStyleSTIM2() { return rEEGStyleUtil.getSensitivityStyle(getMedSenSTIM2()); } 

    public String getMedSenStyleBB3() { return rEEGStyleUtil.getSensitivityStyle(getMedSenBB3()); } 
    public String getMedSenStyleACONV3() { return rEEGStyleUtil.getSensitivityStyle(getMedSenACONV3()); } 
    public String getMedSenStyleADEP3() { return rEEGStyleUtil.getSensitivityStyle(getMedSenADEP3()); } 
    public String getMedSenStyleSTIM3() { return rEEGStyleUtil.getSensitivityStyle(getMedSenSTIM3()); } 

    public String getMedSenStyleBB4() { return rEEGStyleUtil.getSensitivityStyle(getMedSenBB4()); } 
    public String getMedSenStyleACONV4() { return rEEGStyleUtil.getSensitivityStyle(getMedSenACONV4()); } 
    public String getMedSenStyleADEP4() { return rEEGStyleUtil.getSensitivityStyle(getMedSenADEP4()); } 
    public String getMedSenStyleSTIM4() { return rEEGStyleUtil.getSensitivityStyle(getMedSenSTIM4()); } 

    public String getMedSenStyleBB5() { return rEEGStyleUtil.getSensitivityStyle(getMedSenBB5()); } 
    public String getMedSenStyleACONV5() { return rEEGStyleUtil.getSensitivityStyle(getMedSenACONV5()); } 
    public String getMedSenStyleADEP5() { return rEEGStyleUtil.getSensitivityStyle(getMedSenADEP5()); } 
    public String getMedSenStyleSTIM5() { return rEEGStyleUtil.getSensitivityStyle(getMedSenSTIM5()); } 
    
    public List<rEEGMedSensitivityDisp> getMedSenListBB1() { return getMedSensitivities(0, 'BB'); }    
    public List<rEEGMedSensitivityDisp> getMedSenListACONV1() { return getMedSensitivities(0, 'ACONV'); }
    public List<rEEGMedSensitivityDisp> getMedSenListADEP1() { return getMedSensitivities(0, 'ADEP'); }
    public List<rEEGMedSensitivityDisp> getMedSenListSTIM1() { return getMedSensitivities(0, 'STIM'); }

    public List<rEEGMedSensitivityDisp> getMedSenListBB2() { return getMedSensitivities(1, 'BB'); }    
    public List<rEEGMedSensitivityDisp> getMedSenListACONV2() { return getMedSensitivities(1, 'ACONV'); }
    public List<rEEGMedSensitivityDisp> getMedSenListADEP2() { return getMedSensitivities(1, 'ADEP'); }
    public List<rEEGMedSensitivityDisp> getMedSenListSTIM2() { return getMedSensitivities(1, 'STIM'); }

    public List<rEEGMedSensitivityDisp> getMedSenListBB3() { return getMedSensitivities(2, 'BB'); }    
    public List<rEEGMedSensitivityDisp> getMedSenListACONV3() { return getMedSensitivities(2, 'ACONV'); }
    public List<rEEGMedSensitivityDisp> getMedSenListADEP3() { return getMedSensitivities(2, 'ADEP'); }
    public List<rEEGMedSensitivityDisp> getMedSenListSTIM3() { return getMedSensitivities(2, 'STIM'); }

    public List<rEEGMedSensitivityDisp> getMedSenListBB4() { return getMedSensitivities(3, 'BB'); }    
    public List<rEEGMedSensitivityDisp> getMedSenListACONV4() { return getMedSensitivities(3, 'ACONV'); }
    public List<rEEGMedSensitivityDisp> getMedSenListADEP4() { return getMedSensitivities(3, 'ADEP'); }
    public List<rEEGMedSensitivityDisp> getMedSenListSTIM4() { return getMedSensitivities(3, 'STIM'); }

    public List<rEEGMedSensitivityDisp> getMedSenListBB5() { return getMedSensitivities(4, 'BB'); }    
    public List<rEEGMedSensitivityDisp> getMedSenListACONV5() { return getMedSensitivities(4, 'ACONV'); }
    public List<rEEGMedSensitivityDisp> getMedSenListADEP5() { return getMedSensitivities(4, 'ADEP'); }
    public List<rEEGMedSensitivityDisp> getMedSenListSTIM5() { return getMedSensitivities(4, 'STIM'); }
    
    //---Get the sensitivity list
    private List<rEEGMedSensitivityDisp> getMedSensitivities(Integer reegNum, String drugClass)
    {   
        List<rEEGMedSensitivityDisp> returnList = new List<rEEGMedSensitivityDisp>();                
        rEEGAnalysisDisp reegDispObj = getReegDisp(reegNum);

        if (reegDispObj != null && reegDispObj.getObj() != null && reegDispObj.getObj().id != null)
        {
            Boolean showLamictal = reegDispObj.getObj().lamotrigine_visible__c;
            for(r_reeg_med__c medRow : [select trade__c, seq__c, generic__c, sensitivity__c, sub_group_name__c, sub_group_sensitivity__c
                    from r_reeg_med__c
                    where drug_class__c = :drugClass AND r_reeg_id__c = :reegDispObj.getObj().id
                    order by seq__c])
            {
                if (!medRow.trade__c.Equals('Lamictal') || showLamictal)
                {
                    rEEGMedSensitivityDisp medItem = new rEEGMedSensitivityDisp();
                    medItem.Setup( medRow);                                 
                    returnList.Add( medItem);
                }
            }
        }
        
        return returnList;
    } 


        //---TEST METHODS ---------------------------------
    public static testMethod void testController()
    {
        rEEGAnalysisController cont = new rEEGAnalysisController();

        String testStr = 'test';
        r_patient__c pat = new r_patient__c();
        pat.name = testStr;
        pat.first_name__c = testStr;
        pat.dob__c = '01/25/1950';
        insert pat;

        cont.Patient = pat;
        System.assert(cont.Patient != null);
        System.assertEquals(pat.Id, cont.Patient.Id);
        
        cont.PatLastName = testStr;
        System.assertEquals(cont.PatLastName, testStr);
        cont.PatFirstName = testStr;
        System.assertEquals(cont.PatFirstName, testStr);
        cont.PatId = testStr;
        System.assertEquals(cont.PatId, testStr);
        cont.ReegSearchName = testStr;
        System.assertEquals(cont.ReegSearchName, testStr);
        cont.PhyName = testStr;
        System.assertEquals(cont.PhyName, testStr);

        r_reeg__c reeg = new r_reeg__c();
        reeg.reeg_type__c = 'Type I';
        reeg.r_patient_id__c = pat.Id;
        insert reeg;

        PageReference pr = cont.patSearchAction();
        System.assert(pr == null);
        pr = cont.reegCbChanged();
        System.assert(pr == null);

        pr = cont.rEEGSearchAction();
        System.assert(pr == null);

        cont.ReegSearchName = reeg.name;
        pr = cont.rEEGSearchAction();
        System.assert(pr == null);

        pr = cont.phySearchAction();
        System.assert(pr == null);
        
        r_patient__c pat2 = new r_patient__c();
        pat2.name = testStr;
        pat2.first_name__c = testStr;
        pat2.dob__c = '01/25/1950';
        insert pat2;

        cont.Patient = pat2;
        cont.selectPatient();

        Boolean testBool = cont.getShowPatientResults();
        testBool = cont.getShowReegResults();
        cont.PatInfoPanel = testBool;
        cont.TestOverviewPnl = testBool;
        cont.DxPnl = testBool;
        cont.DrugPnl = testBool;
        cont.BrainMapPnl = testBool;
        cont.CnsVarsPnl = testBool;
        cont.ShowPage = testBool;

        rEEGAnalysisDisp testReeg = new rEEGAnalysisDisp();
        
        System.assert(cont.Reeg1 == null);
        cont.Reeg1 = testReeg;
        System.assert(cont.Reeg1 != null);
        System.assert(cont.Reeg2 == null);
        cont.Reeg2 = testReeg;
        System.assert(cont.Reeg2 != null);
        System.assert(cont.Reeg3 == null);
        cont.Reeg3 = testReeg;
        System.assert(cont.Reeg3 != null);
        System.assert(cont.Reeg4 == null);
        cont.Reeg4 = testReeg;
        System.assert(cont.Reeg4 != null);
        System.assert(cont.Reeg5 == null);
        cont.Reeg5 = testReeg;
        System.assert(cont.Reeg5 != null);

        List<r_patient__c> patList = cont.getPatientList();
        List<rEEGAnalysisDisp> reegList = cont.getReegList();

        List<r_reeg_dx__c> dxList = new List<r_reeg_dx__c>();
        System.assert(cont.Reeg1Dx == null);
        cont.Reeg1Dx = dxList;
        System.assert(cont.Reeg1Dx != null);
        System.assert(cont.Reeg2Dx == null);
        cont.Reeg2Dx = dxList;
        System.assert(cont.Reeg2Dx != null);
        System.assert(cont.Reeg3Dx == null);
        cont.Reeg3Dx = dxList;
        System.assert(cont.Reeg3Dx != null);
        System.assert(cont.Reeg4Dx == null);
        cont.Reeg4Dx = dxList;
        System.assert(cont.Reeg4Dx != null);
        System.assert(cont.Reeg5Dx == null);
        cont.Reeg5Dx = dxList;
        System.assert(cont.Reeg5Dx != null);

        rEEGAnalysisDisp rDisp = new rEEGAnalysisDisp();
        rDisp.Setup(reeg, '01/01/1950');
        
        //List<rEEGAnalysisDisp> dispList = new List<rEEGAnalysisDisp>();
        //dispList.Add(rDisp);
        //dispList.Add(rDisp);
        //dispList.Add(rDisp);
        //dispList.Add(rDisp);
        //dispList.Add(rDisp);
        //cont.setReegList(dispList);

        cont.selectPatient();
        cont.reegCount = 5;

        String t = cont.getMedSenBB1();
        t = cont.getMedSenACONV1();
        t = cont.getMedSenADEP1();
        t = cont.getMedSenSTIM1();
        t = cont.getMedBioBB1();
        t = cont.getMedBioACONV1();
        t = cont.getMedBioADEP1(); 
        t = cont.getMedBioSTIM1(); 

        t = cont.getMedSenACONV2();
        t = cont.getMedSenADEP2();
        t = cont.getMedSenSTIM2();
        t = cont.getMedBioBB2();
        t = cont.getMedBioACONV2();
        t = cont.getMedBioADEP2(); 
        t = cont.getMedBioSTIM2(); 

        t = cont.getMedSenACONV3();
        t = cont.getMedSenADEP3();
        t = cont.getMedSenSTIM3();
        t = cont.getMedBioBB3();
        t = cont.getMedBioACONV3();
        t = cont.getMedBioADEP3(); 
        t = cont.getMedBioSTIM3(); 

        t = cont.getMedSenACONV4();
        t = cont.getMedSenADEP4();
        t = cont.getMedSenSTIM4();
        t = cont.getMedBioBB4();
        t = cont.getMedBioACONV4();
        t = cont.getMedBioADEP4(); 
        t = cont.getMedBioSTIM4(); 

        t = cont.getMedSenACONV5();
        t = cont.getMedSenADEP5();
        t = cont.getMedSenSTIM5();
        t = cont.getMedBioBB5();
        t = cont.getMedBioACONV5();
        t = cont.getMedBioADEP5(); 
        t = cont.getMedBioSTIM5(); 
        
        System.assertEquals(cont.getMedSenStyleBB1() , 'color: black');
        System.assertEquals(cont.getMedSenStyleACONV1() , 'color: black');
        System.assertEquals(cont.getMedSenStyleADEP1() , 'color: black');
        System.assertEquals(cont.getMedSenStyleSTIM1() , 'color: black');

        System.assertEquals(cont.getMedSenStyleBB2() , 'color: black');
        System.assertEquals(cont.getMedSenStyleACONV2() , 'color: black');
        System.assertEquals(cont.getMedSenStyleADEP2() , 'color: black');
        System.assertEquals(cont.getMedSenStyleSTIM2() , 'color: black');

        System.assertEquals(cont.getMedSenStyleBB3() , 'color: black');
        System.assertEquals(cont.getMedSenStyleACONV3() , 'color: black');
        System.assertEquals(cont.getMedSenStyleADEP3() , 'color: black');
        System.assertEquals(cont.getMedSenStyleSTIM3() , 'color: black');

        System.assertEquals(cont.getMedSenStyleBB4() , 'color: black');
        System.assertEquals(cont.getMedSenStyleACONV4() , 'color: black');
        System.assertEquals(cont.getMedSenStyleADEP4() , 'color: black');
        System.assertEquals(cont.getMedSenStyleSTIM4() , 'color: black');
        
        System.assertEquals(cont.getMedSenStyleBB5() , 'color: black');
        System.assertEquals(cont.getMedSenStyleACONV5() , 'color: black');
        System.assertEquals(cont.getMedSenStyleADEP5() , 'color: black');
        System.assertEquals(cont.getMedSenStyleSTIM5() , 'color: black');

        System.assert(cont.getMedSenListBB1() != null);
        System.assert(cont.getMedSenListACONV1() != null);
        System.assert(cont.getMedSenListADEP1() != null);
        System.assert(cont.getMedSenListSTIM1() != null);

        System.assert(cont.getMedSenListBB2() != null);
        System.assert(cont.getMedSenListACONV2() != null);
        System.assert(cont.getMedSenListADEP2() != null);
        System.assert(cont.getMedSenListSTIM2() != null);

        System.assert(cont.getMedSenListBB3() != null);
        System.assert(cont.getMedSenListACONV3() != null);
        System.assert(cont.getMedSenListADEP3() != null);
        System.assert(cont.getMedSenListSTIM3() != null);

        System.assert(cont.getMedSenListBB4() != null);
        System.assert(cont.getMedSenListACONV4() != null);
        System.assert(cont.getMedSenListADEP4() != null);
        System.assert(cont.getMedSenListSTIM4() != null);
        
        System.assert(cont.getMedSenListBB5() != null);
        System.assert(cont.getMedSenListACONV5() != null);
        System.assert(cont.getMedSenListADEP5() != null);
        System.assert(cont.getMedSenListSTIM5() != null);

    }
}