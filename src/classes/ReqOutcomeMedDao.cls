//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: ReqOutcomeMedDao
//   PURPOSE: data access class for the Requisition outcome med obj.
//     OWNER: CNSR
//   CREATED: 01/2/15 jdepetro Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class ReqOutcomeMedDao extends BaseDao 
{
	private static String NAME = 'Req_Outcome_Medication__c';
	private static final ReqOutcomeMedDao reqMedDao = new ReqOutcomeMedDao();

    public static ReqOutcomeMedDao getInstance() 
    {
        return reqMedDao; 
    }
	
	  //---Constructor
	public ReqOutcomeMedDao()
	{
		ObjectName = NAME;
		ClassName = 'ReqOutcomeMedDao';
	}
	
	public static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.Req_Outcome_Medication__c.fields.getMap());					
		}  	
		return fldList;
	}

	public String getAllFieldStr()
    {
        return getFieldSql(Schema.SObjectType.Req_Outcome_Medication__c.fields.getMap(), '');
    }
	
	//---Get by Id
	public Req_Outcome_Medication__c getById(String idInp)  
    {
		Req_Outcome_Medication__c obj = (Req_Outcome_Medication__c) getSObjectById(getFieldStr(), NAME, idInp);
		return obj;
    }  
    
 	public List<Req_Outcome_Medication__c> getByRequisitionId(String reqId)
	{
		String whereClause =  ' PEER_Requisition__c =  \'' + reqId + '\'';
		return (List<Req_Outcome_Medication__c>) getSObjectListByWhere(getAllFieldStr(), NAME, whereClause, 'Name');
	}

	public List<Req_Outcome_Medication__c> getActiveByUserId(String userId)
	{
		String whereClause =  ' patient_physician_user__c =  \'' + userId + '\'' ;
		return (List<Req_Outcome_Medication__c>) getSObjectListByWhere(getAllFieldStr(), NAME, whereClause, 'Name');
	}
	
	public static Req_Outcome_Medication__c getTestReqOutcomeMed()
	{
		PEER_Requisition__c reqObj = PEERRequisitionDao.getTestObj();

		Req_Outcome_Medication__c med = new Req_Outcome_Medication__c();
		med.med_name__c = 'Prozac';
		med.dosage__c = 30.00;
		Date today = Date.today();
		med.start_date__c = today.addDays(-50);
		med.end_date__c = today.addDays(-30);
		med.PEER_Requisition__c = reqObj.Id;
		insert med;

		return med;
	}
	
}