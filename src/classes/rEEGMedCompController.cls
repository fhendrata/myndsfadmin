public class rEEGMedCompController 
{        
    List<rEEGMedSensitivityDisp> meds;
        
    private String cnsId;
    private String medGrp;
                
    public void setCnsId(String s)
    { 
        cnsId = s;
    }   
    public String getCnsId()
    { 
        return cnsId;
    }
        
    public void setMedGrp(String s)
    { 
        medGrp = s;
    }
    public String getMedGrp()
    { 
        return medGrp;
    }
   
    private string x1 = 'Default Setting';
    public String getX1()
    { 
        return x1;
    }
                
    public List<rEEGMedSensitivityDisp> getMeds()
    {   
        if (meds == null ) 
        {
            x1 = 'New -' + medGrp + cnsID;
            meds = new List<rEEGMedSensitivityDisp>();
            
            for(r_reeg_med__c medRow : [select trade__c, generic__c, seq__c, sensitivity__c, sub_group_name__c, sub_group_sensitivity__c
                from r_reeg_med__c
                where drug_class__c = :medGrp AND r_reeg_id__c = :cnsId 
                order by seq__c])
            {
                rEEGMedSensitivityDisp medItem = new rEEGMedSensitivityDisp();
                medItem.Setup(medRow);                                 
                meds.Add(medItem);
            }       
        }
        
        return meds;
    }   

             //---TEST METHODS ------------------------------
    public static testMethod void testMedCompController()
    {
        rEEGMedCompController cont = new rEEGMedCompController();

        cont.setCnsId('test');
        cont.setMedGrp('medGrp');
        String x1 = cont.getX1();
                
        List<rEEGMedSensitivityDisp> medSens = cont.getMeds();
   
        System.assertEquals(cont.getCnsId(), 'test');
        System.assertEquals(cont.getMedGrp(), 'medGrp');
    }
}