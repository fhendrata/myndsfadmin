global class bat_PhyRptLoad implements Database.Batchable<sObject>
{
   	global final String Query;
   	global final integer month;
   	global final integer year;
   	global final integer loadType;

   	global bat_PhyRptLoad(String q, integer m, integer y, integer lt)
   	{
   		Query = q; month = m; year = y; loadType = lt;
   	}
	
   	global Database.QueryLocator start(Database.BatchableContext BC)
	{
    	return Database.getQueryLocator(Query);
   	}

 	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
   		if (month != null && month > 0 && month <= 12)
		{
			if (year != null && year <= (DateTime.now().year() + 1) && year > 2004)
			{
				DateTime selDt = DateTime.newInstance(year, month, 1);
				rpt_PhyProcessController cont = new rpt_PhyProcessController();
					
				if (loadType != null && loadType > 0)
				{
					if (loadType == 1)
					{
						for(Sobject s : scope)
			      		{
			      			cont.buildRpt(s, selDt);
			      		}
					}
					else if (loadType == 2)
					{
						cont.processRpts(selDt);
					}
				}
			}
		}
   	}
 
   	global void finish(Database.BatchableContext BC)
   	{
   	}
}