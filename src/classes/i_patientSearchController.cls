public with sharing class i_patientSearchController extends BaseWizardController 
{
    public List<r_patient__c> patientList;
    public string lastname {get; set;}
    public string firstname {get; set;}
    public string dob {get; set;}
    public string dob2 {get; set;}
    public boolean resultVisible {get; set;}
    public boolean newPatBtnVisible {get; set;}
    public Boolean allResultsFound { get; set; }
    public Boolean lnFnResultsFound { get; set; }
    public Boolean nameDobResultsFound { get; set; }
    public List<r_patient__c> patAllList { get; set; }
    public List<r_patient__c> patFLNameList { get; set; }
    public List<r_patient__c> patNameDobList { get; set; }
    
    private String id;
    private String wizardTab;
    private String newPat;
    
    //---Constructor
    Public i_patientSearchController()
    {    
        this.id = ApexPages.currentPage().getParameters().get('id');
        this.qsid = ApexPages.currentPage().getParameters().get('qsid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        newPatBtnVisible = false;
        resultVisible = false;
        allResultsFound = false;
        lnFnResultsFound = false;
        nameDobResultsFound = false;
        pDao = new patientDao();
    }
    
    public String getQsId()
    {
        return qsid;
    }

    public Boolean getDOBRequired()
    {
        return (qsid == 'null');
    }
    
    public PageReference loadAction()
    {
    	
    	return null;
    }

    private List<r_patient__c> getMatchingPatients()
    {
        patAllList = new List<r_patient__c>();
        boolean fnameOk = !e_StringUtil.isNullOrEmpty(firstname);
        boolean lnameOk = !e_StringUtil.isNullOrEmpty(lastname);
        if (!e_StringUtil.isNullOrEmpty(dob))
        {
        	if (dob == 'mm/dd/yyyy')
        		dob = '';
        	else
				dob2 = dob;
        }
        
        if (!e_StringUtil.isNullOrEmpty(dob2) && dob2 == 'mm/dd/yyyy')
        	dob2 = '';
        	
        boolean dobOk = !e_StringUtil.isNullOrEmpty(dob);
        
        if (fnameOk || dobOk || lnameOk) 
        {
        	patAllList = pDao.getAllSearchResults(lastname, firstname, dob);
        }
        else
        {
        	addMessage('All required fields were not completed.');
        }
        
        return patAllList;
    }

    public PageReference searchAction()
    {
        newPatBtnVisible = true;
        
        patAllList = getMatchingPatients();

        if (patAllList != null && patAllList.size() > 0)
        {
        	resultVisible = true;
        	return null;
        }
        else
        	return newPatAction();
    }
    
    public PageReference newPatAction()
    {
    	PageReference returnVal = new PageReference('/apex/iwPatientEdit');
        returnVal.getParameters().put('isNew', 'true');
        returnVal.getParameters().put('np', 'true');
        returnVal.getParameters().put('fn', firstname);
        returnVal.getParameters().put('ln', lastname);
        returnVal.getParameters().put('dob', dob);
        returnVal.getParameters().put('wiz', 'true');
        returnVal.getParameters().put('show', 'dem');
        if (isWR != null && isWR)
        		returnVal.getParameters().put('wr', 'true');
        returnVal.setRedirect(true);
        return returnVal;
    }

       //---Get a handle back to the patient page
    private PageReference getDestPage()
    {
        PageReference returnVal = new PageReference( '/' + ApexPages.currentPage().getParameters().get('id'));
        returnVal.setRedirect(true);
        return returnVal;
    }

          //---Add a message to the message list
    private void addMessage(String value)
    {
        String message = 'All fields must be completed'; 
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, value);
        ApexPages.addMessage(msg);
    }

        //---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
        i_patientSearchController cont = new i_patientSearchController();
        cont.IsTestCase = true;
        
        string testStr = cont.getQsId();
        boolean testBool = cont.getDOBRequired();
        
         r_reeg__c reeg = rEEGDao.getTestReeg();
        cont.reeg = reeg;
        patientDao patDao = new patientDao();
        patDao.IsTestCase = true;
        
        cont.patient = patDao.getById(reeg.r_patient_id__c);
        
        patientOutcomeDao patOutcomeDao = new patientOutcomeDao();
        r_patient_outc__c outcome = patOutcomeDao.getTestOutcome(reeg.r_patient_id__c);
        patientOutcomeMedDao medDao = new patientOutcomeMedDao();
        medDao.IsTestCase = true;
        r_outcome_med__c outMed =  medDao.getTestOutcomeMed(outcome.Id);    
        
        //ApexPages.currentPage().getParameters().put('qsid');
 		cont.outcomeId = ApexPages.currentPage().getParameters().put('oid', outcome.Id);
 		cont.patId = ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
 		ApexPages.currentPage().getParameters().put('wiz', 'true');
 		ApexPages.currentPage().getParameters().put('isNew', 'true');
		ApexPages.currentPage().getParameters().put('np', 'true');
		ApexPages.currentPage().getParameters().put('isNew', 'true');
		ApexPages.currentPage().getParameters().put('ln', 'last');
		ApexPages.currentPage().getParameters().put('fn', 'first');
		ApexPages.currentPage().getParameters().put('dob', '01/01/1955');
        
        cont.loadAction();
        cont.searchAction();
        cont.newPatAction();
        cont.getDestPage();
        

        System.assertEquals( 1, 1);
    }
}