public class rEEGDxGroupDisp extends BaserEEGDxDisp
{
    private Integer itemCount;
    public Integer getItemCount()
    {
        return itemCount;
    }
       
    private List<rEEGDxDisp> dxs;
    public List<rEEGDxDisp> getDxs()
    {
        return dxs;
    }
    public void setDxs(List<rEEGDxDisp> s)
    {
        dxs= s;
    }
    
    public Boolean getIsNotEmpty()
    {
        return (dxs != null && !dxs.isEmpty());
    }
    
    public void addItem(rEEGDxDisp o)
    {
        if (dxs == null)
        {
             itemCount = 0;
             dxs = new List<rEEGDxDisp>();
        }
        
        dxs.Add(o);
        itemCount++;
    }    

     //------------TEST-----------------------------
    public static testMethod void testDxGroupDisp()
    {
        rEEGDxDisp disp = new rEEGDxDisp();

        rEEGDxGroupDisp grp = new rEEGDxGroupDisp();
        grp.addItem( disp);

        List<rEEGDxDisp> list2 = grp.getDxs();
        System.assertEquals( list2[0], disp);

        System.assertEquals( grp.getItemCount(), 1);
        
        Boolean result = grp.getIsNotEmpty();
    }
 }