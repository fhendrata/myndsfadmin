public with sharing class i_ObjectDisp 
{
	public r_patient__c patient {get; set;}
	public r_reeg__c reeg {get;	set;}
	
	public string iconUrl {get; set;}
	public string navUrl {get; set;}
	public boolean isPatient {get; set;}
	public boolean isReeg {get; set;}
	
	public i_ObjectDisp()
	{
		isPatient = false;
		isReeg = false;
	}
	
	public void setObjParams()
	{
		if (patient != null)
		{
			isPatient = true;
			iconUrl = '/Images/pat_icon_sm.png';
			navUrl = '/apex/iwPatientDetail?id=' + patient.Id;
		}
		else if (reeg != null)
		{
			isReeg = true;
			iconUrl = '/Images/reeg_wave_icon_sm.png';
			navUrl = '/apex/iwrEEGSummary?id=' + reeg.Id;
		}
	}
}