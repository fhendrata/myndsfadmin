public with sharing class phy_OutcomeListController extends BaseController
{
    private string searchCriteria {get; set;}
    private integer resultTableSize {get; set;}
    public string newFilterId {get; set;}
    public ApexPages.StandardSetController con {get; set;}
    
    public Boolean hasNext { get { return con.getHasNext(); } set; }  
    public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }  
    public Boolean hasResults { get { return (resultNumber > 0); } set; }  
    public Integer pageNumber  { get { return con.getPageNumber(); } set; }
    public Integer resultNumber  { get { return con.getResultSize(); } set; }  
    public void previous() { con.previous(); }  
    public void next() { con.next(); }
    
    public String patSearchString {get; set;}  
    public patientOutcomeDao oDao;
    
    public phy_OutcomeListController()
    {
    	oDao = new patientOutcomeDao();
        resultTableSize = RESULT_TABLE_SIZE;
        
        setupFilter();
        loadOutcomes();
        
    }
    
    private void setupFilter()
    {
        //-- jdepetro.8.26.14 - this was eliminating the patient sharing between dr's. The clinic admin is still not allowed patient sharing.
        //if(getIsClinicPhy()) oDao.filter = 'patient__r.physician__c = ' + quote(getCurrentPhysician());
        if(getIsClinicAdmin()) oDao.filter = 'patient__r.physician__r.AccountId = ' + quote(getCurrentAccount());
        //else oDao.filter = 'patient__r.physician__c = ' + quote(getCurrentPhysician());
    }
    
    
    private PageReference loadOutcomes()
    {
    	String query = oDao.getMyOutcomesQuery(getCurrentPhysician(),' limit 10000 ','');

        System.debug('### wr_OutcomeListController:loadOutcomes:query: ' + query);

        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
        return null;
    }
    
    public List<r_patient_outc__c> getOutcomes()
    {
        if(con != null)  
            return (List<r_patient_outc__c>)con.getRecords(); 
        else
            return null;
    }
    
     
    public void searchPatients()
    {
        con = new ApexPages.StandardSetController(oDao.getByInputWildcard(patSearchString, ' LIMIT 10000'));
        con.setPageSize(resultTableSize);
    }
    
    
      //------------ TEST METHODS ------------------------
    
    private static testmethod void testController()
    {
        phy_OutcomeListController plc = new phy_OutcomeListController();
        plc.patSearchString = 'a';
       // plc.searchPatients();
       // plc.getOutcomes();
        
    
    }
    
    
    
    
    
}