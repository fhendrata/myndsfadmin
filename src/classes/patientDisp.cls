//--------------------------------------------------------------------------------
// Class: patientDisp
// Purpose: Display wrapper object for Patient records
// Author: Joe DePetro
//-- 1.10.14 - created
//--------------------------------------------------------------------------------
public class patientDisp 
{
	public patientDisp(r_patient__c pat, Boolean hasReportComplete) 
	{
		Patient = pat;
		IsReportComplete = hasReportComplete;
	}

	public r_patient__c Patient { get; set; }
	public Boolean IsReportComplete { get; set; }
}