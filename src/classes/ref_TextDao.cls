//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: ref_TextDao
// PURPOSE: Data access class for the Reference Text object
// CREATED: 05/11/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class ref_TextDao extends BaseDao
{
	private static String NAME = 'ref_text__c';    
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.ref_text__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public ref_text__c getById(String idInp)
    {
		return (ref_text__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public List<ref_text__c> getAll()
    {
		return (List<ref_text__c>)getSObjectListByWhere(getFieldStr(), NAME, '');
    } 
	

	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testTextDao()
    {
        ref_TextDao dao = new ref_TextDao();
        
        ref_text__c textObj = ref_TextDao.getTestRefText();
        textObj = dao.getById(textObj.Id);
        List<ref_text__c> textList = dao.getAll();
        
    }   
    
    public static ref_text__c getTestRefText()
    {
    	ref_text__c testText = new ref_text__c();
    	testText.Name = 'This is a test text string';
    	insert testText;
    	
    	return testText;
    } 

}