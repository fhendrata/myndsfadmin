public with sharing class uploadWizardController4 extends BaseUploadWizController
{
	public ApexPages.StandardController StdCont { get; set;}
	public List<PatientOutcomeMedDisp> oMeds  { get; set; }
    public List<PatientOutcomeMedDisp> newMeds  { get; set; }
    public List<r_outcome_med__c> holdingList { get; set; } 
    public List<r_outcome_med__c> previousMeds { get; set; }
    public String billingCode { get; set; }
    public boolean IsNewPageNameType { get; set; }
    public integer rowCount {get; set;}
	public integer newRowCount {get; set;}
    public Boolean NoStDtMessage { get; set; }
    public boolean medsErrorMessage { get; set; }
    public List<String> medsToUpdate { get; set; }
    public boolean addNewMed {get; set;}
    public Integer oMedCount {get; set;}
    
    private patientOutcomeDao dao {get; set;}
    private patientOutcomeMedDao ocMedDao { get; set; }
    private patientIntervalsDao intDao { get; set; }
    
	public uploadWizardController4()
    {
    	init();
    }
    
    public uploadWizardController4( ApexPages.StandardController stdController)
    {
    	init(); 
    	StdCont = stdController;
    }

    public void init()
    {
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.qsid = ApexPages.currentPage().getParameters().get('qsid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        string strIsNewPat = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(strIsNewPat) && strIsNewPat == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        String strIsNew = ApexPages.currentPage().getParameters().get('isNew');
        this.IsNew = (!e_StringUtil.isNullOrEmpty(strIsNew) && strIsNew == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        
        pDao = new patientDao();
        ocMedDao = new patientOutcomeMedDao();
        intDao = new patientIntervalsDao();
        rDao = new rEEGDao();
        dao = new patientOutcomeDao();
        
        addNewMed = false;
    	IsNewPageNameType = false;
        NoStDtMessage = false;
    	medsErrorMessage = false;
    }
    
    public PageReference loadAction()
    {
        if (!e_StringUtil.isNullOrEmpty(patId) && patient == null)
    	{
    		this.patient = pDao.getById(patId);
    	}
    	
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
   			this.reeg = rDao.getById(reegId);
    	}
    	
    	if (!e_StringUtil.isNullOrEmpty(outcomeId))
    	{
   			this.outcome = dao.getById(outcomeId);
    	}
    	
    	// assign most recent type I rEEG, set billing code, set outcome date to rEEG date
		List<r_reeg__c> reegs;
    	if (!e_StringUtil.isNullOrEmpty(this.patId))
   			reegs = rDao.getByPatientId(this.patId);

    	if (outcome == null)
    	{
    		outcome = new r_patient_outc__c();
    		Date oDate = date.today();
    		outcome.put('outcome_date__c', oDate); 
    		outcome.put('cgi__c', '4 - Baseline'); 
    		
   			outcome.put('rEEG__c', this.reegId);
   			if (reeg != null)
   				billingCode = reeg.billing_code__c;
    		outcome.put('patient__c', this.patId);
    		
			oMeds = getExistingOutcomeMeds(this.patId);
			
			if (oMeds == null) 
	    		oMeds = new List<PatientOutcomeMedDisp>();  
			
			dao.saveSObject(outcome);
    	}
    	else
    	{
    		string reegId = (String) outcome.get('rEEG__c');
    		if (!e_StringUtil.isNullOrEmpty(reegId))
    			billingCode = dao.getbillingCode(reegId);
	
       		oMeds = getOutcomeMeds();
    	}    		
    	
    	return null;
    }
    
     //---Build the existing outcome meds list (meds from previous outcome)
    public List<PatientOutcomeMedDisp> getExistingOutcomeMeds(string pid)
    {	
         if (oMeds == null)
         { 	
             oMeds = new List<PatientOutcomeMedDisp>();  
             // List<string> previousIds = new List<string>();
             PatientOutcomeMedDisp oMed;          
             Integer ctr = -1;
             
             if (medsToUpdate == null)
				medsToUpdate = new List<String>();
             
             //---Get the current meds
             previousMeds = dao.getExistingOutcomeMeds(this.patId);
             if (previousMeds != null) 
             {
	            for(r_outcome_med__c oMedRow : previousMeds) {
		                ctr++;  
		                oMed = new PatientOutcomeMedDisp();
		                oMed.setDisplayAction(true);
		                oMed.setRowNum(ctr);
		                
		                r_outcome_med__c hldgMed = new r_outcome_med__c();
		                hldgMed.med_name__c = oMedRow.med_name__c;
		                hldgMed.dosage__c = oMedRow.dosage__c;
		                hldgMed.unit__c = oMedRow.unit__c;
		                hldgMed.frequency__c = oMedRow.frequency__c;
		                hldgMed.start_date__c = oMedRow.start_date__c;
		                hldgMed.end_date__c = oMedRow.end_date__c;
		                hldgMed.previous_med__c = oMedRow.Id;
		                hldgMed.start_date_is_estimated__c = oMedRow.start_date_is_estimated__c;
						oMed.Setup(hldgMed);
		                // oMed.Setup(oMedRow);     
		                // previousIds.Add(oMedRow.Id);                     
		                oMeds.Add(oMed);		      
		                
		                // running list of previous meds to update on save
						medsToUpdate.add(oMedRow.Id);          
	            }
	            oMedCount = ctr + 1;	//---ctr is zero based	 
	            rowCount = ctr + 1;	//---ctr is zero based	                         	
             }
             else {
             	oMeds = null;
             }
        }    
        return oMeds;
    }
    
	//---Build the current outcome's meds list
    public List<PatientOutcomeMedDisp> getOutcomeMeds()
    {
         if (oMeds == null)
         {
             oMeds = new List<PatientOutcomeMedDisp>();  
             PatientOutcomeMedDisp oMed;
             
             Integer ctr = -1;
             
             if (outcome != null)
             {
	             //---Add the current meds
	             for(r_outcome_med__c oMedRow : 
	                 [select med_name__c, dosage__c, unit__c, frequency__c, start_date__c, end_date__c, name, start_date_is_estimated__c
	                     from r_outcome_med__c
	                     where r_outcome_id__c = :(String) outcome.get('id')])
	             {
	                ctr++;
	                
	                oMed = new PatientOutcomeMedDisp();
	                oMed.setRowNum(ctr);
	                oMed.Setup(oMedRow );                                 
	                oMeds.Add(oMed);
	             }
	             oMedCount = ctr + 1;  //---ctr is zero based
             
		         if (ctr < 0) 
		         {
			       //---Create blank row
			     	r_outcome_med__c objMed = new r_outcome_med__c();
			        objMed.r_outcome_id__c = outcome.Id;
			        objMed.unit__c = 'mg';
			            
			        oMed = new PatientOutcomeMedDisp();
			        oMed.setRowNum(0);    
			        rowCount = 1; 
			        oMed.Setup(objMed);                           
			        oMeds.Add(oMed);  	
			   	 } 
             }       
	         rowCount = ctr + 1;    //---ctr is zero based
        }    
        return oMeds;
    }

    //------------ wizard button actions ----------------------------
    public PageReference wizStep3()
    {
       	PageReference returnVal = new PageReference('/apex/uploadWizard3');
       	AddParameters(returnVal);
               	
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference wizStep5()
    {
    	saveActionOverride();
    	
    	if (false && medsErrorMessage)
    	{
    		return null;
    	}
    	else
    	{
	       PageReference returnVal = new PageReference('/apex/uploadWizard5');
   	   		AddParameters(returnVal);
               	
       		returnVal.setRedirect(true);
       		return returnVal;
    	}
    }
    
    public PageReference saveActionOverride()
    {
        if (oMeds != null) 
        {
        	NoStDtMessage = false;
            //---Loop through each row
            for(PatientOutcomeMedDisp oMedRow : oMeds) 
            {
                if (oMedRow != null) 
                {
                	r_outcome_med__c outcMed = oMedRow.getObj();
                    if (outcMed != null) 
                    {
                    	if (IsNew)
                   			outcMed.r_outcome_id__c = outcome.Id;
                   		oMedRow.Setup(outcMed);
                  		SaveOutcomeMed(outcMed);
                    }
                }
            }
        }
            
        debug('### newMeds:' + newMeds);
        if (newMeds != null && newMeds.size() > 0)
        {
         	for(PatientOutcomeMedDisp nMedRow : newMeds) 
            {
            	debug('### nMedRow:' + nMedRow);
	            if (nMedRow != null) 
	            {
                   	r_outcome_med__c nOutcMed = nMedRow.getObj();
                    if (nOutcMed != null) 
					{
                   		nOutcMed.r_outcome_id__c = outcome.Id;
                   		nMedRow.Setup(nOutcMed);
                   		debug('### nOutcMed:' + nOutcMed);
                  		SaveOutcomeMed(nOutcMed);   
                  		if (nOutcMed != null && nOutcMed.med_name__c != null && nOutcMed.med_name__c != '')
                  			oMeds.add(nMedRow);
	                }
	            }
	        }
        }
        
        if (isWiz && !e_StringUtil.isNullOrEmpty(reegId))
        {
        	r_reeg__c reeg = rDao.getById(reegId);
        	if (reeg != null && !reeg.med_no_med__c && !e_StringUtil.isNullOrEmpty(reeg.reeg_type__c) && reeg.reeg_type__c != 'Type II')
			{
				boolean isActiveMed = false;
				if (oMeds != null && oMeds.size() > 0)
				{
					for (PatientOutcomeMedDisp row : oMeds)
					{
						r_outcome_med__c medObj = row.getObj();
						isActiveMed = (medObj != null && medObj.end_date__c != null);
						if (isActiveMed) break;
					}	
				}
				
				debug('### isActiveMed:' + isActiveMed);
				
				if (!isActiveMed)
				{
					medsErrorMessage = true;
				}
				else
				{
					reeg.reeg_type_mod__c = rEEGUtil.getMedicatedTestMod(oMeds);
					rDao.saveSObject(reeg);
				}
			}
        }

        patient = pDao.getById(patId);
        
        if (patient != null)
        {
	        patientIntervalBuilder intBuilder = new patientIntervalBuilder();
	  		intBuilder.buildAllIntervalsForPat(patient.Id, patient.OwnerId);
        }
        
        dao.saveSObject(outcome);
        
    	return null;
    }
    
    // ---Save an outcome med row
    private void SaveOutcomeMed(r_outcome_med__c medObj)
    {
        if (medObj != null && medObj.med_name__c != null && medObj.med_name__c != '')
        {
            ocMedDao.saveSObject(medObj);
        }
        else 
        {
            //---If the id is not blank and the name is, then delete the old record
            if (medObj.id != null) ocMedDao.deleteSObject(medObj);
        }    
    }

	public PageReference addMedRow()
    { 	
    	Integer rowNum = Integer.valueOf(System.currentPageReference().getParameters().get('rowNumber'));
		// rowNum is 0 based, rowCount starts at 1
		if ((rowNum + 1) == rowCount) 
		{
			//---Create blank row
			r_outcome_med__c objMed = new r_outcome_med__c();
			objMed.r_outcome_id__c = outcome.Id;
	            
			PatientOutcomeMedDisp oMed = new PatientOutcomeMedDisp();
			oMed.setRowNum(rowCount); 
			rowCount++; 
			oMed.Setup(objMed);                           
			oMeds.Add(oMed);  	
		}        
    	return null; 	  
    }
    
    public PageReference addBlankMedRows()
    {
    	addNewMed = true;
    	if (newMeds == null) newMeds = new List<PatientOutcomeMedDisp>();    
    	    	
       	for (integer i = 0; i < 5; i++)
    	{
    		PatientOutcomeMedDisp medDisp = new PatientOutcomeMedDisp();
    		r_outcome_med__c newMed = new r_outcome_med__c();
    		newMed.unit__c = 'mg';	
    		medDisp.setObj(newMed);		
    		medDisp.setRowNum(newMeds.size());
    		newMeds.add(medDisp);    	
    	}
    	newRowCount = newMeds.size();
    	
    	return null;
    }  
    
    public PageReference cancelWizOverride()
    {
    	PageReference pr = cancelWizAction();
    	pr = new PageReference('/apex/UploadWizard1');
    	pr.setRedirect(true);
        return pr;
    }
    
          //---TEST METHODS ---------------------------------
    public static testMethod void testController()
    {
		r_reeg__c reeg = rEEGDao.getTestReeg();
    	patientDao patDao = new patientDao();
    	r_patient__c pat = patDao.getById(reeg.r_patient_id__c);
    	ApexPages.currentPage().getParameters().put('id', pat.Id);
    	ApexPages.currentPage().getParameters().put('rid', reeg.Id);
    	
    	uploadWizardController4 cont = new uploadWizardController4();
		cont.loadAction();
        PageReference pRef1 = cont.wizStep3();
        PageReference pRef2 = cont.wizStep5();
        pRef2 = cont.cancelWizOverride();
    }
}