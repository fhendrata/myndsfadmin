//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: QsDao
// PURPOSE: Data access class for Quickstart fields
// CREATED: 1/25/11 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------

public class QsDao extends CrudDao 
{
	private static String NAME = 'r_qs_file__c';    
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_qs_file__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public r_qs_file__c getById(String idInp)
    {
		return (r_qs_file__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public void setReegId(string qsId, string reegId)
    {
    	r_qs_file__c tempObj = getById(qsId);
    	if (tempObj != null)
    	{
    		tempObj.r_reeg_id__c = reegId;
    		saveSObject(tempObj);
    	}
    }
   
    public List<r_qs_file__c> getLastQSFiles()
    {
    	DateTime back = DateTime.now();
    	//return [Select Id, name, ftp_dir__c, r_eeg_tech__c, status__c, eeg_upload_date__c From r_qs_file__c where eeg_upload_date__c > :back.addDays(-2)];
    	return [Select Id, name, ftp_dir__c, r_eeg_tech__c, status__c, eeg_upload_date__c From r_qs_file__c order by eeg_upload_date__c desc limit 50];
    }
    
    public static r_qs_file__c getTestQsFile()
    {
    	r_qs_file__c obj = new r_qs_file__c();
    	Contact phy = ContactDao.getTestPhysician();
    	obj.r_eeg_tech__c = phy.Id;
    	obj.status__c = 'NEW';
    	obj.eeg_upload_date__c = DateTime.now();
    	insert obj;
    	
    	return obj;
    }

    //-----------------------------------------------------------------------
    //--                          TEST METHODS               ---
    //-----------------------------------------------------------------------
    public static testMethod void testQsDao()
    {
    	QsDao dao = new QsDao();
    	dao.IsTestCase = true;
    	r_qs_file__c obj = QsDao.getTestQsFile();
    	
    	if(obj != null)
    		obj = dao.getById(obj.Id);
    	
    	r_reeg__c reeg = rEEGDao.getTestReeg();
    	if (reeg != null)
    		dao.setReegId(obj.Id, reeg.Id);
    	
    	List<r_qs_file__c> fileList = dao.getLastQSFiles();
    	
    }
}