//--------------------------------------------------------------------------------
// COMPONENT: Patient Outcome + Outcome Medicines
// CLASS: patientOutcomeDao
// PURPOSE: Data Access class
// CREATED: 03/29/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class patientNoteDao extends CrudDao 
{ 
	private static String NAME = 'r_patient_note__c';  
	
	public patientNoteDao()
	{
		HideDeleted = true;
	}
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_patient_note__c.fields.getMap());
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}		
		return fldList;
	}
	
	public List<r_patient_note__c> getPatientNotesByPatId(string pid) 
	{
		string whereStr = 'r_patient__c = ' + pid;
		string orderStr = 'CreatedDate desc';
    	List<r_patient_note__c> noteList = (List<r_patient_note__c>) getSObjectListByWhere(getFieldStr(), NAME, whereStr, orderStr);
		if (noteList != null && noteList.size() > 0) return noteList;
		else return null;     		
    }
    
    public r_patient_note__c getPatientNote(string nid) 
    {
    	r_patient_note__c note = (r_patient_note__c) getSObjectById(getFieldStr(), NAME, nid);
		if (note != null) return note;
		else return null;     		
    }
    
    public static r_patient_note__c getTestPatNote() 
    {
    	r_patient__c testPat = patientDao.getTestPat();
    	r_patient_note__c note = new r_patient_note__c();
    	note.r_patient__c = testPat.Id;
    	note.body__c = 'test body';
    	note.is_deleted__c = false;
    	note.subject__c = 'subject';
    	insert note;
    	
    	return note;
    }
    
    //-----------------------------------------------------------------------
    //--                          TEST METHODS               ---
    //-----------------------------------------------------------------------
    public static testMethod void testPatNoteDao()
    {
        patientNoteDao dao = new patientNoteDao();       
        System.assert(!e_StringUtil.isNullOrEmpty(patientNoteDao.getFieldStr()));     
        r_patient_note__c testPatNote = patientNoteDao.getTestPatNote();
        r_patient_note__c note1 = dao.getPatientNote(testPatNote.Id);
        testPatNote = patientNoteDao.getTestPatNote();
        List<r_patient_note__c> noteList =  dao.getPatientNotesByPatId(testPatNote.r_patient__c);
    }    
	
}