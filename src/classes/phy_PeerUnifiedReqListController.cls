//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: phy_PeerUnifiedReqListController
// PURPOSE: Controller class for Unified PEER requisistion list page
// CREATED: 01/09/15 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class phy_PeerUnifiedReqListController extends BaseController
{
    private integer resultTableSize {get; set;}
    public ApexPages.StandardSetController con {get; set;}

    public Boolean hasNext { get { return con.getHasNext(); } set; }
    public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }
    public Boolean hasResults { get { return (resultNumber > 0); } set; }
    public Integer pageNumber  { get { return con.getPageNumber(); } set; }
    public Integer resultNumber  { get { return con.getResultSize(); } set; }
    public void previous() { con.previous(); }
    public void next() { con.next(); }

	public phy_PeerUnifiedReqListController() 
	{
        resultTableSize = RESULT_TABLE_SIZE;

        String filterStr = '';

        //-- mcorbin.2.24.17 - This was not respecting the newly setup Sharing Rules (MAD-213 & MAD-249)
        //--if(getIsClinicAdmin()) filterStr = 'Physician__r.AccountId = ' + quote(getCurrentAccount());

        if (String.isNotBlank(filterStr)) filterStr += ' AND ';
        filterStr += '(Status__c = \'New\' OR Status__c = \'Pending\' OR Status__c = \'Complete\' OR Status__c = \'Submitted\' OR Status__c = \'EEG Submitted\')';
        if (!getIsAdminOrPortalUserMgr())
            filterStr += ' AND PEER__r.r_patient_id__r.CA_Control_Group_Type__c != \'Control Group\' AND reeg_reeg_type__c != \'pre-Washout\'';
        String query = PEERRequisitionDao.getInstance().getOrderListQuery(filterStr, ' limit 10000 ', '');
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
	}

	public List<PEER_Requisition__c> getPEERRequisitions()
	{
        return (List<PEER_Requisition__c>) con.getRecords();
	}

}