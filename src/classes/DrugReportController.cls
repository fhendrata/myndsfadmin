//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: DrugReportController
// PURPOSE: Controller class for the Drug Report page
// CREATED: 04/28/11 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class DrugReportController extends BaseDrugReportController
{
	public string drugName {get; set;}

	public DrugReportController()
    {
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
    	this.pageId = ApexPages.currentPage().getParameters().get('pgid');
    	rDao = new rEegDao();
    }
    
    public PageReference loadAction()
    {
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
   			this.reeg = rDao.getById(reegId);
   			baseUrl = '/apex/DrugReport?rid=' + reeg.Id;
   			baseReport2Url = '/apex/DrugReport_2?rid=' + reeg.Id;
   			baseReportTabbedUrl = '/apex/DrugReport_tabbed?rid=' + reeg.Id;
   			cnsVarsUrl = '/apex/DrugReportCnsVar?rid=' + reeg.Id + '&ctid=' + reeg.corr_cns_id__c;
   			
   			if (!e_StringUtil.isNullOrEmpty(pageId))
   			{
   				DrugTreeMenuUtil.MenuItem menuItem = menuMap.get(pageId);
   				if (menuItem != null)
   					drugName = menuItem.drugName;
   			}
    	}
		else
		{
			this.reeg = new r_reeg__c();
		}    
		
		return null;	
    }
    
    public PageReference summaryAction()
    {
    	PageReference pr = Page.ReportSummary;
    	AddParameters(pr);
    	
    	return pr;
    }	
    
    public PageReference returnAction()
    {
    	PageReference pr = new PageReference('/a0h/o');
   // 	AddParameters(pr);
    	
    	return pr;
    }
	
	private static testmethod void testController()
    {
        DrugReportController dc = new DrugReportController();
        dc.returnAction();  
        dc.summaryAction();
    
    }
    
}