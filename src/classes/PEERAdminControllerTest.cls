//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: PEERAdminControllerTest
// PURPOSE: Test class for PEER Admin page controller
// CREATED: 05/9/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class PEERAdminControllerTest 
{
	
	@isTest static void testPEER() 
	{
		r_reeg__c reeg = rEEGDao.getTestReeg();
		ApexPages.StandardController sc = new ApexPages.standardController(reeg);
		PEERAdminController pc = new PEERAdminController(sc);

		pc.loadAction();
		pc.saveAdminAction();
		pc.cancelAction();
	}

	
}