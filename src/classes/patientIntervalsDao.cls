//--------------------------------------------------------------------------------
// COMPONENT: Patient Intervals, Outcomes + Outcome Medicines
// CLASS: patientIntervalsDao
// PURPOSE: Data Access class
// CREATED: 06/09/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class patientIntervalsDao extends CrudDao 
{
	private static String NAME = 'r_interval__c'; 
	private static String PAT_NAME = 'r_patient_outc__c';  
	private static String MED_NAME = 'r_outcome_med__c';
	private static String FREQ_NAME = 'ref_frequency__c';
	private static String REEG_NAME = 'r_reeg__c';
	
	//---Only retrieve fields that are displayed, edited
	private static String FIELD_LIST = 'id, name, cgi_value__c, reason__c, start_reason__c, stop_reason__c, r_patient__c, '
		+ 'reason_type__c, start_date__c, stop_date__c, is_deleted__c, type__c, meds__c, meds_text_area__c';	
	
	private static String OUTC_FIELD_LIST = 'id, name, cgs__c, cgi_patient__c, cgi__c, '
		+ 'comments__c, rEEG__c, reeg_h_index__c, outcome_date__c, patient__c';
		
	private static String MED_FIELD_LIST = 'id, name, dosage__c, start_date_is_estimated__c, '
		+ 'r_outcome_id__c, start_date__c, end_date__c, med_name__c, '
		+ 'unit__c, frequency__c';
		
	private static String FREQ_FIELD_LIST = 'id, name, desc_med__c, '
		+ 'daily_factor__c';		

	private static String REEG_FIELD_LIST = 'id, name, req_date__c, CreatedDate, billing_code__c, '
		+ 'r_patient_id__c';	
		
	private static String INT_FIELD_LIST = 'Id, OwnerId, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, '
		+ 'start_date__c, stop_date__c, reason__c, cgi_value__c, r_patient__c, reason_type__c, is_deleted__c, temp_outcome_id__c, type__c, '
		+ 'meds__c, meds_text_area__c, start_reason__c, stop_reason__c ';			
	
	//---Constructor
	public patientIntervalsDao()
	{
		ObjectName = NAME;
		ClassName = 'patientIntervalsDao';
		HideDeleted = true; 
	}
	
	public static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_interval__c.fields.getMap());		
		}  	
		return fldList;
	}
	
	public static String outcFldList;	
	public static String getOutcomeFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(outcFldList)) 
		{
			// outcFldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_patient_outc__c.fields.getMap());		
			outcFldList = OUTC_FIELD_LIST;			
		}  	
		return outcFldList;
	}
	
	public static String intFldList;	
	public static String getIntervalsFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(intFldList)) 
		{	
			intFldList = INT_FIELD_LIST;			
		}  	
		return intFldList;
	}

	public static String medFldList;	
	public static String getMedFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(medFldList)) 
		{
			// medFldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_outcome_med__c.fields.getMap());	
			medFldList = MED_FIELD_LIST;				
		}  	
		return medFldList;
	}
	
	public static String freqFldList;	
	public static String getFreqFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(freqFldList)) 
		{
			// freqFldList = e_FieldUtil.getFieldSql(Schema.SObjectType.ref_frequency__c.fields.getMap());
			freqFldList = FREQ_FIELD_LIST;
		}  		
		return freqFldList;
	}

	public static String reegFldList;	
	public static String getReegFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(reegFldList)) 
		{
			// reegFldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_reeg__c.fields.getMap());
			reegFldList = REEG_FIELD_LIST;
		}  		
		return reegFldList;
	}

	//---Get by Id
	public r_interval__c getById(String idInp)  
    {
		return (r_interval__c) getSObjectById(fldList, NAME, idInp);
    }  

	public r_interval__c getByIntId(String idInp)  
    {
		return (r_interval__c) getSObjectById(INT_FIELD_LIST, NAME, idInp);
    }  	
	
	public List<r_interval__c> getTotalIntervals()
	{
		String whereClause =  '';
		return (List<r_interval__c>) getSObjectListByWhere(FIELD_LIST, NAME, whereClause, 'Name');
	}
	
 	public List<r_interval__c> getByPatId(String id)
	{
		String whereClause =  ' r_patient__c =  \'' + id + '\'' + ' AND is_deleted__c = false';
		return (List<r_interval__c>) getSObjectListByWhere(FIELD_LIST, NAME, whereClause, 'start_date__c desc');
	}
	
		//---Get by Id
	public List<r_patient_outc__c> getOutcomesByInterval(String iid)  
    {
    	List<r_patient_outc__c> objList = null;
    	
    	return (List<r_patient_outc__c>) [select  id, name, cgi_patient__c, cgs__c, cgi__c,  comments__c, rEEG__c, r_interval__c,
			reeg_h_index__c, outcome_date__c, patient__c FROM r_patient_outc__c 
    			WHERE r_interval__c = : iid AND is_deleted__c = false];	
    }  

 	public Integer getNmbrMedsByInterval(String iid)
	{
		Integer nm = 0;
		
		for(r_outcome_med__c outcMed : [SELECT id, name, dosage__c, r_outcome_id__c, start_date__c, r_interval__c,
    		end_date__c, med_name__c, unit__c, frequency__c FROM r_outcome_med__c 
    		WHERE r_interval__c = : iid AND is_deleted__c = false])
    	{
			nm++;
    	}

        return nm;
	}
 
 	public List<r_outcome_med__c> getMedsByInterval(String iid)
	{
		List<r_outcome_med__c> objList = null;
		
		return (List<r_outcome_med__c>) [SELECT id, name, dosage__c, r_outcome_id__c, start_date__c, r_interval__c,
    			end_date__c, med_name__c, unit__c, frequency__c, start_date_is_estimated__c FROM r_outcome_med__c 
    			WHERE r_interval__c = : iid AND is_deleted__c = false];	
	}
	
	public static r_interval__c getTestInterval()
	{
		r_patient__c pat = patientDao.getTestPat();
		
		r_interval__c intv = new r_interval__c();
		intv.cgi_value__c = '1 - Very much improved';
		intv.r_patient__c = pat.Id;
		//intv.reason__c = 'First Outcome, New Interval 0';
		intv.reason_type__c = '1 - New Outcome';
		intv.start_date__c = Date.today().addDays(-1);
		intv.stop_date__c = Date.today();
		
		patientIntervalsDao intDao = new patientIntervalsDao();
		intDao.saveSObject(intv);
		
		return intv;
	}
	
	
	//---------------TEST METHODS ---------------------------------
    //
    //-------------------------------------------------------------
    public static testMethod void testController()
    {
    	patientIntervalsDao dao = new patientIntervalsDao();
    	dao.IsTestCase = true;
    	
    	String test = patientIntervalsDao.getFieldStr();
    	test = patientIntervalsDao.getOutcomeFieldStr();
    	test = patientIntervalsDao.getMedFieldStr();
    	test = patientIntervalsDao.getFreqFieldStr();
    	test = patientIntervalsDao.getReegFieldStr();
	
		r_patient__c patient = patientDao.getTestPat();
		List<r_interval__c> intList = dao.getTotalIntervals();
		intList = dao.getByPatId(patient.Id);
		
		r_interval__c intv = patientIntervalsDao.getTestInterval();
    	intv = dao.getById(intv.Id);
    	intList = dao.getTotalIntervals();
    	intList = dao.getByPatId(intv.r_patient__c);
    	
    	patientOutcomeDao outDao = new patientOutcomeDao();
    	r_patient_outc__c outcome = outDao.getTestOutcome();
    	outcome.r_interval__c = intv.Id;
    	outDao.saveSObject(outcome);
    	
    	List<r_patient_outc__c> ocList = dao.getOutcomesByInterval(intv.Id);
    	
    	patientOutcomeMedDao oMedDao = new patientOutcomeMedDao();
    	r_outcome_med__c oMed = oMedDao.getTestOutcomeMed();
    	oMed.r_interval__c = intv.Id;
    //	oMed.r_outcome_id__c = outcome.Id;
    	oMedDao.saveSObject(oMed);
    	dao.getMedsByInterval(intv.Id);
    	
    	Integer num = dao.getNmbrMedsByInterval(intv.Id);
    	
        System.assertEquals( 1, 1);
    }
}