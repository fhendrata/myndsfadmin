//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: ContactDao
// PURPOSE: Data access class for the Contqact (Physician) object
// CREATED: 09/17/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class ContactDao extends BaseDao 
{
	private static String NAME = 'Contact';    
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.Contact.fields.getMap());
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public Contact getById(String idInp)
    {
    	string fieldStr = getFieldStr();
    	fieldStr += ', Account.Allow_Rapid_Turnaround__c,Account.Name,';
		return (Contact)getSObjectById(fieldStr, NAME, idInp);
    } 
    
    public List<Contact> getAll()
    {
		return (List<Contact>)getSObjectListByWhere(getFieldStr(), NAME, '');
    } 
    
    public List<Contact> getAllEegTechs()
    {
    	return [Select c.name, c.RecordType.Name, c.RecordTypeId From Contact c where c.RecordType.Name = 'EEG Tech'];
    } 
    
    public List<Contact> getfirstK()
    {
		return (List<Contact>)getSObjectListByWhere(getFieldStr(), NAME, '', '', '999');
    } 

    public Contact getByPhyUserId(String idInp)
    {
        for (Contact con : [select Id, Name, physician_portal_user__c from Contact where physician_portal_user__c = : idInp])
        {
            return con;
        }
        
        return null;
    } 

    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testContactDao()
    {
    	ContactDao dao = new ContactDao();
    	Contact con = ContactDao.getTestPhysician();
    	con = dao.getById(con.Id);
    }

    public static testMethod void testContactDaoPO2()
    {
        ContactDao dao = new ContactDao();
        Contact con = ContactDao.getTestPhysicianPO2();
        con = dao.getById(con.Id);
    }

    public static Contact getTestPhysicianPO2()
    {
        RecordTypeDao rDao = new RecordTypeDao();
        RecordType r = rDao.getByRecordTypeName('Provider Contact'); 
        RecordType rA = rDao.getByRecordTypeName('Provider Account');
        System.debug('#### recordtypeId : ' + r.Id);
        Account a = new Account(name = 'test account', Report_Build_Type__c = 'PEER Online 2.0',RecordTypeId=rA.Id);
        insert a;
        Contact c = new Contact(FirstName = 'Joe', LastName = 'Test', AccountId = a.Id,RecordTypeId=r.Id);
        insert c;
        System.debug('#### Contact recordtypeId : ' + c.RecordTypeId);

        return c;
    }
    
    public static Contact getTestPhysician()
    {
        RecordTypeDao rDao = new RecordTypeDao();
        RecordType r = rDao.getByRecordTypeName('Provider Contact'); 
        RecordType rA = rDao.getByRecordTypeName('Provider Account');
        System.debug('#### recordtypeId : ' + r.Id);
    	Account a = new Account(name = 'test account',RecordTypeId=rA.Id, Allow_Rapid_Turnaround__c = true);
        insert a;
        Contact c = new Contact(FirstName = 'Joe', LastName = 'Test', AccountId = a.Id,RecordTypeId=r.Id);
        insert c;

    	return c;
    }
}