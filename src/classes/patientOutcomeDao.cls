//--------------------------------------------------------------------------------
// COMPONENT: Patient Outcome + Outcome Medicines
// CLASS: patientOutcomeDao
// PURPOSE: Data Access class
// CREATED: 03/29/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class patientOutcomeDao extends CrudDao 
{ 
	private static String NAME = 'r_patient_outc__c';  
	private static String MED_NAME = 'r_outcome_med__c';
	private static String FREQ_NAME = 'ref_frequency__c';
	private static String REEG_NAME = 'r_reeg__c';
	private patientOutcomeMedDao outMedDao {get; set;}
	
	private static String EXT_FIELD_LIST = ', r_interval__r.cgi_value__c, r_interval__r.r_patient__c, r_interval__r.reason_type__c, r_interval__r.reason__c, r_interval__r.start_date__c, r_interval__r.stop_date__c';
	
	//---Constructor
	public patientOutcomeDao()
	{
		ObjectName = NAME;
		ClassName = 'patientOutcomeDao';
		HideDeleted = true; 
		outMedDao = new patientOutcomeMedDao();
	}
	
	public static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_patient_outc__c.fields.getMap()) + EXT_FIELD_LIST;					
		}  	
		return fldList;
	}

	public static String medFldList;	
	public static String getMedFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(medFldList)) 
		{
			medFldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_outcome_med__c.fields.getMap());					
		}  	
		return medFldList;
	}
	
	public static String freqFldList;	
	public static String getFreqFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(freqFldList)) 
		{
			freqFldList = e_FieldUtil.getFieldSql(Schema.SObjectType.ref_frequency__c.fields.getMap());
		}  		
		return freqFldList;
	}
	
	//---Get by Id
	public r_patient_outc__c getById(String idInp)  
    {
		return (r_patient_outc__c) getSObjectById(getFieldStr(), NAME, idInp);
    }  
    
    public List<r_patient_outc__c> getByCgi(String cgiScore)
	{
		String whereClause =  ' cgi__c =  \'' + cgiScore + '\'';				
		return (List<r_patient_outc__c>) getSObjectListByWhere(getFieldStr(), NAME, whereClause, 'outcome_date__c ASC');
	}
		
 	public List<r_patient_outc__c> getByPatId(String id)
	{
		return getByPatId(id, 'Name');
	}
	
	public List<r_patient_outc__c> getByPatId(String id, string order)
	{
		String whereClause =  ' patient__c =  \'' + id + '\' AND is_deleted__c = false';
		string fieldStr = getFieldStr();
		
		fieldStr += ', (Select dosage__c, frequency__c, med_name__c, unit__c From Outcome_Medications__r)';
		return (List<r_patient_outc__c>) getSObjectListByWhere(fieldStr, NAME, whereClause, order);
	}
	
	public List<r_patient_outc__c> getOutcomesWithIntervalsByPatId(String id)
	{
		String whereClause =  ' patient__c =  \'' + id + '\'' + ' AND r_interval__c != \'\'';
		return (List<r_patient_outc__c>) getSObjectListByWhere(getFieldStr(), NAME, whereClause, 'r_interval__r.start_date__c');
	}
	
	public List<r_patient_outc__c> getByNullInterval()
	{
		String whereClause =  ' r_interval__c =  \'\'';				
		return (List<r_patient_outc__c>) getSObjectListByWhere(getFieldStr(), NAME, whereClause, 'patient__c, outcome_date__c ASC');
	}
	
	public List<r_patient_outc__c> getExistingOutcomes(String id)
	{
		// get existing outcomes where outcome date is not null
		String whereClause =  ' patient__c =  \'' + id + '\'' + ' AND is_deleted__c = false AND outcome_date__c != null';
        return (List<r_patient_outc__c>) getSObjectListByWhere(getFieldStr(), NAME, whereClause, 'CreatedDate DESC');   	
	}
	
	public List<r_patient_outc__c> getOutcomesForReeg(string reegId)
	{
		// get existing outcomes where outcome date is not null
		String whereClause =  ' rEEG__c =  \'' + reegId + '\'' + ' AND is_deleted__c = false';
        return (List<r_patient_outc__c>) getSObjectListByWhere(getFieldStr(), NAME, whereClause, 'CreatedDate DESC');   	
	}
	
 	public List<r_outcome_med__c> getByOutcome(String id)
	{
		String whereClause =  ' r_outcome_id__c =  \'' + id + '\'' + ' AND is_deleted__c = false';
		return (List<r_outcome_med__c>) getSObjectListByWhere(getMedFieldStr(), MED_NAME, whereClause, 'Name');
	}
	
	//-- TODO -jdepetro - this should be in the outcome med dao not here. (10.22.10 - Copy placed there for new changes) 
 	public List<r_outcome_med__c> getExistingOutcomeMeds(String id)
	{
		// get existing outcomes where outcome date is not null
		String whereClause =  ' patient__c =  \'' + id + '\'' + ' AND outcome_date__c != null';
    	List<r_patient_outc__c> obj = (List<r_patient_outc__c>) getSObjectListByWhere(getFieldStr(), NAME, whereClause, 'CreatedDate DESC');  
		
    	// use meds from most recent outcome reegs[0].Id
    	List<r_outcome_med__c> objList = null; 
    	if (obj != null && obj.size() > 0) {
    		
            String medWhereClause =  ' r_outcome_id__c =  \'' + obj[0].Id + '\'' + ' AND (end_date__c = null OR end_date__c > ' + String.valueOf(Date.today()) + ')';
			objList = (List<r_outcome_med__c>) getSObjectListByWhere(getMedFieldStr(), MED_NAME, medWhereClause, 'Name');
    	}
    	
        return objList;    	
	}

	public List<patientOutcomeDisp> getPatOutcomeDispListNonWR(string pid)
	{
		 List<patientOutcomeDisp> returnVal = new List<patientOutcomeDisp>();  
		 patientOutcomeMedDao medDao = new patientOutcomeMedDao();

         if (!e_StringUtil.isNullOrEmpty(pid))
         {
         	for(r_patient_outc__c outcomeRow : getByPatId(pid, 'outcome_date__c desc, CreatedDate desc'))
            {
            	
            	patientOutcomeDisp outcome = new patientOutcomeDisp();
        	    outcome.Setup(outcomeRow);      
    	    	string outcomesMeds = '';
				

				for(r_outcome_med__c outcomeMedRow : outcomeRow.Outcome_Medications__r)
               	{    
               		string freq = '';
               		string unit = '';
               		string dose = '';
               		if (outcomeMedRow.frequency__c != null) { freq = outcomeMedRow.frequency__c; }
               		if (outcomeMedRow.dosage__c != null) { dose = String.valueOf(outcomeMedRow.dosage__c); }
               		if (outcomeMedRow.unit__c != null) { unit = outcomeMedRow.unit__c; }
               		
               		if (outcomeMedRow.med_name__c != null) {
               			outcomesMeds += outcomeMedRow.med_name__c + '(' + dose + ' ' + unit + ' ' + freq + ') ';
               		}
               	}  

                outcome.setMedications(outcomesMeds);                           
                returnVal.Add(outcome);
            } 
        }     
		
		return returnVal;
	}
	
	public List<patientOutcomeDisp> getPatOutcomeDispList(string pid)
	{
		 List<patientOutcomeDisp> returnVal = new List<patientOutcomeDisp>();  
		 patientOutcomeMedDao medDao = new patientOutcomeMedDao();

         if (!e_StringUtil.isNullOrEmpty(pid))
         {
         	for(r_patient_outc__c outcomeRow : getByPatId(pid, 'CreatedDate desc'))
            {
            	
            	patientOutcomeDisp outcome = new patientOutcomeDisp();
        	    outcome.Setup(outcomeRow);      
    	    	string outcomesMeds = '';
				

				for(r_outcome_med__c outcomeMedRow : outcomeRow.Outcome_Medications__r)
               	{    
               		string freq = '';
               		string unit = '';
               		string dose = '';
               		if (outcomeMedRow.frequency__c != null) { freq = outcomeMedRow.frequency__c; }
               		if (outcomeMedRow.dosage__c != null) { dose = String.valueOf(outcomeMedRow.dosage__c); }
               		if (outcomeMedRow.unit__c != null) { unit = outcomeMedRow.unit__c; }
               		
               		if (outcomeMedRow.med_name__c != null) {
               			outcomesMeds += outcomeMedRow.med_name__c + '(' + dose + ' ' + unit + ' ' + freq + ') ';
               		}
               	}  

                outcome.setMedications(outcomesMeds);                           
                returnVal.Add(outcome);
            } 
        }     
		
		return returnVal;
	}
    
	//---Get all available frequencies
	public List<ref_frequency__c> getFrequencies()  
    {
		List <ref_frequency__c> obj = (List<ref_frequency__c>) getSObjectListByWhere(getFreqFieldStr(), FREQ_NAME, null, 'Name');		
		if (obj != null)
    	{
    		return obj;
    	}
        return null;
    }  
    
	//-- jdepetro (doing qa/stage code merge)- this may have been replaced earlier in the correct dao. 
	//--get rEEG billing code by rEEG
	public string getbillingCode(string rid)
	{
		string billingCode = '';
		if (rid != null && !rid.equalsIgnoreCase(''))
		{
			r_reeg__c reeg =  [select billing_code__c from r_reeg__c where Id  = : rid];
			if (reeg != null & reeg.billing_code__c != null) 
			{
				billingCode = reeg.billing_code__c;
			}
		}
		return billingCode;
	}

	public List<r_patient_outc__c> getOutcomesWithoutIntervals()
	{
		String whereCond = '(r_interval__c = \'\' OR r_interval__c = null)'  ;
		return (List<r_patient_outc__c>)getSObjectListByWhere(getFieldStr(), NAME, whereCond, '', '3000');
	}
	
	 //-- jdepetro (doing qa/stage code merge)- this may have been replaced earlier in the correct dao. 
    //---Assign rEEG to Outcome
	public List<r_reeg__c> getReeg(String id)
	{   	
    	List<r_reeg__c> reegs = new List<r_reeg__c>();  
        r_reeg__c reeg;
    	
		for(r_reeg__c reegRow : 
			[select id, name, req_date__c, CreatedDate, billing_code__c, r_patient_id__c 
				from r_reeg__c where is_deleted__c = false	AND r_patient_id__c = : id ORDER BY req_date__c DESC])
		{                           
			reegs.Add(reegRow);
		}   	

   		return reegs;
	}   
    		
    //---Create test outcome script
    public r_patient_outc__c getTestOutcome()
    {	
    	r_patient__c testPat = patientDao.getTestPat();
    	r_patient_outc__c obj = new r_patient_outc__c();
    	obj.patient__c = testPat.Id;
    	obj.cgi__c = 'test';
    	obj.cgi_patient__c = 'test';
       	obj.cgs__c = 'test';
    	obj.outcome_date__c = Date.today();
    	
    	saveSObject(obj);
    	
    	return obj;
    }
    
    //---Create test outcome script
    public r_patient_outc__c getTestOutcome(string patientId)
    {	
    	r_patient_outc__c obj = new r_patient_outc__c();
    	obj.patient__c = patientId;
    	obj.cgi__c = 'test';    	
    	obj.cgi_patient__c = 'test';
       	obj.cgs__c = 'test';
    	
    	obj.outcome_date__c = Date.today().addDays(-5);
    	
    	saveSObject(obj);
    	
    	return obj;
    }
    
    public String getNameSearchQuery(String firstLastName)
    {
        firstLastName = escapeStr(firstLastName);
        List<String> tokens = firstLastName.split(' ',2);
         
        String whereStr = '';
        String baseQuery = 'SELECT Id,Name,patient__c,rEEG__c,cgi__c,cgi_patient__c,cgs__c,patient__r.Name,patient__r.first_name__c,patient__r.middle_initial__c,rEEG__r.rpt_date__c FROM r_patient_outc__c ';
         
        if(tokens.size() == 1)
        {
            whereStr = 'where (patient__r.Name like '+ addLike(firstLastName) + ' OR patient__r.first_name__c like ' + addLike(firstLastName) + ')';
        }
        else
        {
            String whereStr1 = 'where (patient__r.Name like '+ addLike(tokens[0]) + ' AND patient__r.first_name__c like ' + addLike(tokens[1]) + ') ';
            String whereStr2 = 'OR (patient__r.first_name__c like '+ addLike(tokens[0]) + ' AND patient__r.Name like ' + addLike(tokens[1]) + ')'; 
            
            whereStr = whereStr1 + whereStr2;
        }
        //TODO - add filter
        if(filter != null) whereStr += ' AND ' + filter;
        
        whereStr += ' order by patient__r.Name limit 10000 ';
        system.debug('NAME QUERY: ' + baseQuery + whereStr);
        return baseQuery + whereStr;
    }
    
    public String getMyOutcomesQuery(String providerId,String limitStr,String ordStr)
    {
    	String baseQuery = 'SELECT Id,Name,patient__c,rEEG__c,cgi__c,cgi_patient__c,cgs__c,patient__r.Name,patient__r.first_name__c,patient__r.middle_initial__c,rEEG__r.rpt_date__c FROM r_patient_outc__c';
    	String whereStr = '';
    	if(filter != null) whereStr += ' WHERE ' + filter;
    	
    	String query = baseQuery + whereStr;
        

    	if(!e_StringUtil.isNullOrEmpty(ordStr)) query += ' ' + ordStr + ' ';
        else query += ' order by LastModifiedDate DESC';

        if(!e_StringUtil.isNullOrEmpty(limitStr)) query += ' ' + limitStr + ' ';
        else query += '  limit 10000 ';
                
        system.debug('QUERY: ' + query);
        return query;
    	
    }

    public List<r_patient_outc__c> getByInputWildcard(String inputVal, String rLimit)
    {
        if (StringUtil.isNullOrEmpty(rLimit)) rLimit = ' LIMIT 10000';
        inputVal = escapeStr(inputVal);
        List<String> tokens = inputVal.split(' ',2);

        string baseQuery = 'SELECT Id,Name,patient__c,rEEG__c,cgi__c,cgi_patient__c,cgs__c,patient__r.Name,patient__r.first_name__c,patient__r.middle_initial__c,rEEG__r.rpt_date__c FROM r_patient_outc__c ';

        string whereStr = ' WHERE rEEG__r.Name LIKE ' + addLike(inputVal) + ' OR patient__r.prov_pat_id__c LIKE ' + addLike(inputVal) + ' OR patient__r.CNS_ID__c LIKE ' + addLike(inputVal);

        if(tokens.size() == 1)
        {
            whereStr += ' OR patient__r.first_name__c LIKE ' + addLike(inputVal) + ' OR patient__r.Name LIKE ' + addLike(inputVal);
        }
        else
        {
            whereStr += ' OR (patient__r.first_name__c LIKE ' + addLike(tokens[0]) + ' AND patient__r.Name LIKE ' + addLike(tokens[1]) + ') ';
            whereStr += ' OR (patient__r.first_name__c LIKE ' + addLike(tokens[1]) + ' AND patient__r.Name LIKE ' + addLike(tokens[0]) + ') ';
        }

        String orderBy = ' ORDER BY outcome_date__c DESC ';

        return Database.query(baseQuery + whereStr + orderBy + rLimit);
    }
     
    
    
    
       //-----------------------------------------------------------------------
    //--                          TEST METHODS               ---
    //-----------------------------------------------------------------------
    public static testMethod void testPatDao()
    {
        patientOutcomeDao dao = new patientOutcomeDao();
        dao.IsTestCase = true;
        
        r_patient_outc__c patientOutcome = dao.getTestOutcome();
        patientOutcome = dao.getTestOutcome(patientOutcome.patient__c);
        
        System.assert(!e_StringUtil.isNullOrEmpty(patientOutcomeDao.getFieldStr()));
        System.assert(!e_StringUtil.isNullOrEmpty(patientOutcomeDao.getFreqFieldStr()));
        
        r_patient_outc__c pOut = dao.getById(patientOutcome.Id);
        
        patientOutcomeMedDao outMedDao = new patientOutcomeMedDao();
        outMedDao.IsTestCase = true;
        r_outcome_med__c outMed = outMedDao.getTestOutcomeMed();
        List<r_patient_outc__c> patOutcList = dao.getByCgi('test');
        
        patOutcList = dao.getByPatId(patientOutcome.patient__c);
        patOutcList = dao.getByPatId(patientOutcome.patient__c, 'name');
        
        patOutcList = dao.getOutcomesWithIntervalsByPatId(patientOutcome.patient__c);
        patOutcList = dao.getByNullInterval();
        List<r_outcome_med__c> objMedList = dao.getExistingOutcomeMeds(patientOutcome.patient__c);
        objMedList = dao.getByOutcome(patientOutcome.Id);
        
        List<patientOutcomeDisp> patOutDisp = dao.getPatOutcomeDispList(patientOutcome.patient__c);
        patOutDisp = dao.getPatOutcomeDispListNonWR(patientOutcome.patient__c);
        
        patOutcList = dao.getByCgi('6 - Much worse');
        List<ref_frequency__c> freqList = dao.getFrequencies();
        
		r_reeg__c reeg = rEEGDao.getTestReeg();
		List<r_reeg__c> reegList = dao.getReeg(reeg.Id);
    	string BillCd = dao.getbillingCode(reeg.Id);
	
		List<string> medIds = new List<string>();
		medIds.add(outMed.Id);

		List<r_patient_outc__c> patOutList = dao.getOutcomesWithoutIntervals();
        
    }    
}