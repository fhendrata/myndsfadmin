//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: peer_ReportController
// PURPOSE: Controller class for the Report page
// CREATED: 09/03/13 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
public with sharing class peer_ReportController extends BaseDrugReportController
{
    public string drugName { get; set; }
    public string BrainMapUrl { get; set; }
    public boolean IsNeuroRptAvail { get; set; }
    public boolean isNeuroRptOrdered { get; set; }
    public boolean isOkToShowReport {get; set;}
    
    public peer_ReportController()
    {
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        rDao = new rEegDao();
    }
    
    public PageReference loadAction()
    {
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            this.reeg = rDao.getById(reegId, true);
            baseUrl = '/apex/peer_Report?rid=' + reeg.Id;
            baseReport2Url = '/apex/peer_Report?rid=' + reeg.Id;
            baseReportTabbedUrl = '/apex/peer_Report?rid=' + reeg.Id;
            baseDrugGoupUrl = '/apex/peer_DrugClass?rid=' + reeg.Id;
            BrainMapUrl = '/apex/PEERBrainMapView?rid=' + reeg.id;

            Boolean isControlGroup = ((reeg.r_patient_id__r.CA_Control_Group_Type__c != null && reeg.r_patient_id__r.CA_Control_Group_Type__c != '') && (reeg.r_patient_id__r.CA_Control_Group_Type__c == 'Control Group' || reeg.r_patient_id__r.CA_Control_Group_Type__c == 'Not Randomized Group'));

            isOkToShowReport = !isControlGroup || getIsSysAdmin();
            
            if (!e_StringUtil.isNullOrEmpty(pageId))
            {
                DrugTreeMenuUtil.MenuItem menuItem = menuPEERMap.get(pageId);
                if (menuItem != null)
                    drugName = menuItem.drugName;
            }
        }
        else
        {
            this.reeg = new r_reeg__c();
        }    
        
        System.debug('### neurostat:' + reeg.neuro_stat__c);
        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        isNeuroRptOrdered = (reeg != null && reeg.neuro_stat__c == 'In Progress');
        return null;    
    }
    
    public String getSummaryUrl()
    {
    	return '/apex/peer_Summary2?pgid=' + this.pageId + '&rid=' + this.reegId;
    }
    
    public PageReference summaryAction()
    {
        PageReference pr = new PageReference('/apex/peer_Summary2?pgid=' + this.pageId + '&rid=' + this.reegId);
        pr.setRedirect(true);
        
        return pr;
    }   
    
    public PageReference returnAction()
    {
        PageReference pr = new PageReference('/apex/phy_PatientListS2');
  
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference printAction()
    {
        PageReference pr = new PageReference('/apex/peer_ReportPrintPage?pgid=' + this.pageId + '&rid=' + this.reegId);
  
        pr.setRedirect(true);
        return pr;
    }
    
    public void orderNeuroReport()
    {
    	reeg.do_not_send_neuro__c = false;
    	isNeuroRptOrdered = true;
    	rDao.Save(reeg, 'ORDER_NEURO_REPORT');
    }

    // --------- TEST METHODS ----------
    
    @isTest(SeeAllData=true)
    private static void testController()
    {
    	peer_ReportController prc = new peer_ReportController();
    	ApexPages.currentPage().getParameters().put('pgid', '5');
    	
    	prc.loadAction();
    	
    	r_reeg__c testrEEG = rEEGDao.getTestReeg();
    	prc.reegId = testrEEG.Id;
    	prc.loadAction();
    	
    	prc.returnAction();
    	prc.summaryAction();
    	prc.loadAction();
    	prc.getSummaryUrl();
    	prc.printAction();
    }
}