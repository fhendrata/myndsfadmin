//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: i_patientControllerExt
//   PURPOSE: Controller class for the iPad patient page.
// 
//     OWNER: CNS Response
//   CREATED: 11/19/10 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
public class i_patientControllerExt extends BaseWizardController
{
    public List<r_outcome_med__c> previousMeds { get; set; }
    public string dob { get; set; }
    public boolean patDobSet {get; set;}
 
	// private Profile profile;
    private Contact contact;

    //--- this is only used for the apex test case
    public i_patientControllerExt()
    {
    	isWiz = false;
    }

    //---Constructor from the standard controller
    public i_patientControllerExt( ApexPages.StandardController stdController)
    {    
        this.patient = (r_patient__c) stdController.getRecord();
        this.outcomes = getOutcomes();
        this.patientNotes = getPatientNotes();
    }
    
    public PageReference loadAction()
    {
    	this.qsid = ApexPages.currentPage().getParameters().get('qsid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        this.patId = ApexPages.currentPage().getParameters().get('id');
    	string wizParam = ApexPages.currentPage().getParameters().get('wiz');
    	this.isWiz = (!e_StringUtil.isNullOrEmpty(wizParam) && wizParam == 'true');
    	string newParam = ApexPages.currentPage().getParameters().get('isNew');
    	this.isNew = (!e_StringUtil.isNullOrEmpty(newParam) && newParam == 'true');
    	string newPatStr = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        newPatStr = ApexPages.currentPage().getParameters().get('isNew');
        this.isNew = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        
        this.isiPad = true;
        
        pDao = new patientDao();
    	
    	if (!e_StringUtil.isNullOrEmpty(patId))
	    	patient = pDao.getById(patId, true);
    	
    	if (isNew)
    	{
    		patient.OwnerId = UserInfo.getUserId();
    		string tempStr =  ApexPages.currentPage().getParameters().get('ln');
    		if (!e_StringUtil.isNullOrEmpty(tempStr)) patient.Name = tempStr;
    		tempStr = ApexPages.currentPage().getParameters().get('fn');
    		if (!e_StringUtil.isNullOrEmpty(tempStr)) patient.first_name__c = tempStr;
    		tempStr = ApexPages.currentPage().getParameters().get('dob');
    		if (!e_StringUtil.isNullOrEmpty(tempStr)) patient.dob__c = tempStr;
    	}
    	
		patDobSet = (patient != null && !e_StringUtil.isNullOrEmpty(patient.dob__c));
	    if (patDobSet)
	    	dob = patient.dob__c;
  
    	return null;
    }
    
    public PageReference nextAction()
    {
   		if (!e_StringUtil.isNullOrEmpty(dob) && dob == 'mm/dd/yyyy') dob = '';
   		patient.dob__c = dob;
		
		if (!e_StringUtil.isNullOrEmpty(patient.name) && !e_StringUtil.isNullOrEmpty(patient.first_name__c) 
				&& !e_StringUtil.isNullOrEmpty(patient.dob__c) && !e_StringUtil.isNullOrEmpty(patient.Gender__c))
		{
			pDao.saveSObject(patient);
	  
	        PageReference returnVal = new PageReference('/apex/iwrEEGEdit');
	        returnVal.getParameters().put('isNew', (isNew != null) ? string.valueOf(isNew) : '');
	
	        if (patient != null) returnVal.getParameters().put('pid', patient.id);
	        if (!e_StringUtil.isNullOrEmpty(patId)) returnVal.getParameters().put('pid', patId);
	        if (reeg != null) returnVal.getParameters().put('id', reeg.id);
	        if (!e_StringUtil.isNullOrEmpty(reegId)) returnVal.getParameters().put('id', reegId);
	
	        returnVal.getParameters().put('wiz', (isWiz != null) ? string.valueOf(isWiz) : '');
	        returnVal.getParameters().put('np', string.valueOf(isNewPat));
	        returnVal.getParameters().put('oid', outcomeId);
	        if (isWR != null && isWR)
        		returnVal.getParameters().put('wr', 'true');
	        returnVal.setRedirect(true);
	        return returnVal;
		}
		else
		{
			if (e_StringUtil.isNullOrEmpty(patient.name))
				addMessage('Patient Lastname field is empty');
			if (e_StringUtil.isNullOrEmpty(patient.first_name__c))
				addMessage('Patient Firstname field is empty');
			if (e_StringUtil.isNullOrEmpty(patient.dob__c))
				addMessage('Date of Birth field is empty');
			if (e_StringUtil.isNullOrEmpty(patient.Gender__c))
				addMessage('Date of Birth field is empty');
			return null;
		}
    }
    
    public PageReference cancelAction()
    {
        PageReference returnVal = cancelWizAction();
        returnVal.setRedirect(true);
        return returnVal;
    }

    public PageReference Save()
    {
        if (patient.id != null) upsert patient;
        else insert patient;

        PageReference returnVal = new PageReference( '/' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }

          //--- med entry page
    public PageReference saveAndNew()
    {
        if (patient.id != null) upsert patient;
        else insert patient;

        PageReference returnVal = new PageReference( '/apex/patientNewPage');
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public string eegAnticipatedDate  
    {     	
    	get { return e_StringUtil.getDateTimeString(patient.anticipated_eeg_test__c); }    	 
    	set { patient.anticipated_eeg_test__c = e_StringUtil.setDateTimeFromString(value); } 
    }  

    //--- med entry page
    public PageReference medListEditPage()
    {
        return new PageReference('/apex/patientMedListEditPage?id=' + patient.id);
    }
    
    public PageReference medListPastEditPage()
    {
        return new PageReference('/apex/patientMedListPastEditPage?id=' + patient.id);
    }

    //--new outcome
    public PageReference patOutcomeEditPage()
    {
        return new PageReference('/r_patient__c.object?id=' + patient.id);
    }
    
    List<PatientOutcomeMedDisp> omeds;
	public List<PatientOutcomeMedDisp> getoMeds()
    {
    	if (omeds == null)
        {
            omeds = new List<PatientOutcomeMedDisp>();  
            PatientOutcomeMedDisp oMed;          
            Integer ctr = -1;
             
			patientOutcomeMedDao medDao = new patientOutcomeMedDao();
            previousMeds = medDao.getExistingOutcomeMeds(patient.id);
       
            if (previousMeds != null) 
            {
            	// holdingList = previousMeds.deepClone(false);
	            for(r_outcome_med__c oMedRow : previousMeds) 
	            {
			        ctr++;  
		            oMed = new PatientOutcomeMedDisp();
		            oMed.setDisplayAction(true);
		            oMed.setRowNum(ctr);
		                
 	                r_outcome_med__c hldgMed = new r_outcome_med__c();
	                hldgMed.med_name__c = oMedRow.med_name__c;
	                hldgMed.dosage__c = oMedRow.dosage__c;
	                hldgMed.unit__c = oMedRow.unit__c;
	                hldgMed.frequency__c = oMedRow.frequency__c;
	                hldgMed.start_date__c = oMedRow.start_date__c;
	                hldgMed.end_date__c = oMedRow.end_date__c;
	                hldgMed.previous_med__c = oMedRow.Id;
	                hldgMed.start_date_is_estimated__c = oMedRow.start_date_is_estimated__c;
					oMed.Setup(hldgMed);
	                // oMed.Setup(oMedRow);     
	                // previousIds.Add(oMedRow.Id);                     
	                omeds.Add(oMed);		                
	            }
            }             
        }    
        
        return omeds;
    }

    private List<ReegDisp> reegs;   
    public List<ReegDisp> getReegs()
    {
         if (reegs == null)
         {
             reegs = new List<ReegDisp>();  

             if (patient.id != null)
             {
                for(r_reeg__c reegRow : 
                     [select id, name, req_stat__c, req_date__c, reeg_type__c, reeg_type_mod__c, eeg_rec_date__c
                     from r_reeg__c
                     where r_patient_id__c = :patient.id order by req_date__c asc])
                {
                    ReegDisp reeg = new ReegDisp();
                    reeg.Setup( reegRow );                                 
                    reegs.Add( reeg);
                } 
            }     
        }    
        return reegs;
    }  

    //---Action for new rEEG
    public PageReference rEEGNewPage()
    {
        PageReference returnVal = new PageReference( '/apex/rEEGNewPage?pid=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }

           //--- rEEG Anlysis page
    public PageReference rEEGAnalysisPage()
    {
        PageReference returnVal = new PageReference('/apex/rEEGAnalysisPage?patId=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }

    //---Outcomes for the Patient
    private List<patientOutcomeDisp> outcomes;   
    public List<patientOutcomeDisp> getOutcomes()
    {
    	string outcomesMeds = '';
         if (outcomes == null)
         {
             outcomes = new List<patientOutcomeDisp>();  

             if (patient.id != null)
             {
                for(r_patient_outc__c outcomeRow : 
                     [select name, cgi__c, cgs__c, cgi_patient__c,submitted_type__c, reeg_h_index__c, outcome_date__c, CreatedDate, LastModifiedDate 
                     	from r_patient_outc__c where is_deleted__c = false AND patient__c = :patient.id order by CreatedDate desc])
                {
                    patientOutcomeDisp outcome = new patientOutcomeDisp();
                    outcome.Setup(outcomeRow);           
        
                    if (outcomeRow.cgi__c != null) { outcome.setCgiPhysician(outcomeRow.cgi__c.substring(0,1)); }
        			if (outcomeRow.cgi_patient__c != null) { outcome.setCgiPatient(outcomeRow.cgi_patient__c.substring(0,1)); }
       				if (outcomeRow.cgs__c != null) { outcome.setCgs(outcomeRow.cgs__c.substring(0,1)); }
       				
                    outcomesMeds = '';              
                	 for(r_outcome_med__c outcomeMedRow : 
                     	[select med_name__c, dosage__c, unit__c, frequency__c from r_outcome_med__c where r_outcome_id__c = :outcomeRow.id])
                	{    
                		string freq = '';
                		string unit = '';
                		string dose = '';
                		
                		if (outcomeMedRow.frequency__c != null) { freq = outcomeMedRow.frequency__c; }
                		if (outcomeMedRow.dosage__c != null) { dose = String.valueOf(outcomeMedRow.dosage__c); }
                		if (outcomeMedRow.unit__c != null) { unit = outcomeMedRow.unit__c; }
                		
                		if (outcomeMedRow.med_name__c != null) {
                			outcomesMeds += outcomeMedRow.med_name__c + '(' + dose + ' ' + unit + ' ' + freq + ') ';
                		}
                	}  
                    outcome.setMedications(outcomesMeds);                           
                    outcomes.Add(outcome);
                } 
            }     
        }    
        return outcomes;
    }  

    //--- Patient Notes for the Patient
    private List<r_patient_note__c> patientNotes;   
    public List<r_patient_note__c> getPatientNotes() {
         if (patientNotes == null) {
    		patientNotes = new List<r_patient_note__c>();  
			if (patient.id != null) {
                // patientNotes = dao.getPatientNotes(patient.id);
                patientNotes = [select Id, Name, CreatedDate, CreatedById, LastModifiedById, is_deleted__c, body__c, 
                	subject__c, r_patient__c from r_patient_note__c where r_patient__c =: patient.id AND is_deleted__c = false order by CreatedDate desc];
     		}     
        }    
        return patientNotes;
    }  

    //---Outcomes for the Patient
    private List<patientIntervalListDisp> intervalDispList;   
    public List<patientIntervalListDisp> getIntervalDispList()
    {
       	patientIntervalsDao intDao = new patientIntervalsDao();
       	List<r_interval__c> intList = intDao.getByPatId(patient.id);
       	patientIntervalListDisp intDisp;
       	intervalDispList = new List<patientIntervalListDisp>();
        	
       	if (intList != null && intList.size() > 0)
       	{
	       	for (r_interval__c row : intList)
	       	{
	       		intDisp = new patientIntervalListDisp();
	       		intDisp.Interval = row;
	       		if (row.start_date__c != null && row.stop_date__c != null)
	       			intDisp.Duration = row.start_date__c.daysBetween(row.stop_date__c);
				
				intervalDispList.add(intDisp);
	       	}
       	}
        return intervalDispList;

    }  
    
    public PageReference buildIntervals()
    {
        patientIntervalBuilder intBuilder = new patientIntervalBuilder();
        System.debug('## patient.id = ' + patient.Id);
        intBuilder.buildAllIntervalsForPat(patient.Id, patient.OwnerId);
        
        return null;
    }

    //--- Action for new outcome
    public PageReference newOutcome()
    {
        PageReference returnVal = new PageReference( '/apex/iwPatientOutcomeEdit?pid=' + patient.id + '&retUrl=/apex/iwPatientDetail?id=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }

    //--- Action for new patient note
    public PageReference newPatientNote()
    {
        PageReference returnVal = new PageReference( '/apex/iwPatientNoteEdit?pid=' + patient.id + '&retUrl=/apex/iwPatientDetail?id=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }

    public PageReference deletePatient()
    {
        if (getIsMgr()) delete patient;

        String patientPrefix = rEEGUtil.getPatientPrefix();
        PageReference returnVal = new PageReference('/' + patientPrefix + '/o');
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public List<r_patient__History> getPatHistList()
    {
       List<r_patient__History> patHistList = new List<r_patient__History>(); 
       if (patient.id != null) {
         	patHistList = [Select ParentId, OldValue, NewValue, Id, Field, CreatedDate, CreatedById 
                   			From r_patient__History
                   			where ParentId = :patient.id
                   			order by CreatedDate Desc limit 50];
       }            
       return patHistList;
    } 
    
            //---Add a message to the message list
    private void addMessage(String value)
    {
        String message = 'All fields must be completed'; 
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, value);
        ApexPages.addMessage(msg);
    }

    //-----------------TEST-----------------------------------
    public static testMethod void testController()
    {
        i_patientControllerExt cont = new i_patientControllerExt();
        cont.IsTestCase = true;
        
        r_reeg__c reeg = rEEGDao.getTestReeg();
        cont.reeg = reeg;
        patientDao patDao = new patientDao();
        patDao.IsTestCase = true;
        
        cont.patient = patDao.getById(reeg.r_patient_id__c);
        
        patientOutcomeDao patOutcomeDao = new patientOutcomeDao();
        r_patient_outc__c outcome = patOutcomeDao.getTestOutcome(reeg.r_patient_id__c);
        patientOutcomeMedDao medDao = new patientOutcomeMedDao();
        medDao.IsTestCase = true;
        r_outcome_med__c outMed =  medDao.getTestOutcomeMed(outcome.Id);    
        
        //ApexPages.currentPage().getParameters().put('qsid');
 		cont.outcomeId = ApexPages.currentPage().getParameters().put('oid', outcome.Id);
 		cont.patId = ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
 		ApexPages.currentPage().getParameters().put('wiz', 'true');
 		ApexPages.currentPage().getParameters().put('isNew', 'true');
		ApexPages.currentPage().getParameters().put('np', 'true');
		ApexPages.currentPage().getParameters().put('isNew', 'true');
		ApexPages.currentPage().getParameters().put('ln', 'last');
		ApexPages.currentPage().getParameters().put('fn', 'first');
		ApexPages.currentPage().getParameters().put('dob', '01/01/1955');

		cont.loadAction();
		cont.nextAction();
		cont.getoMeds();	
		cont.buildIntervals();	
		
        Profile p = new Profile();
        p.Name = 'Physician Portal Profile';
        cont.setProfile(p);

        cont.contact = new Contact();
        cont.contact.gender_cd__c = 'Male';
        
        PageReference pRef1 = cont.Save();
        PageReference pRef2 = cont.saveAndNew();
        PageReference pRef3 = cont.medListEditPage();
        PageReference pRef3a = cont.medListPastEditPage();
        PageReference pRef4 = cont.rEEGNewPage();
        PageReference pRef5 = cont.patOutcomeEditPage();
        PageReference pRef6 = cont.rEEGAnalysisPage();
        PageReference pRef7 = cont.newOutcome();
        pRef7 = cont.medListPastEditPage();
        
		PageReference pRef8 = cont.newPatientNote();
		List<r_patient__History> patHistList = cont.getPatHistList();

		// Need to set up rEEG, PatientNotes, PatientMedications, Outcomes and Outcome Meds
		// How to delete set up objs
		r_patient__c patient = cont.patient;
		r_reeg__c testReeg = new r_reeg__c();
    	testReeg.r_patient_id__c = patient.id;
    	testReeg.reeg_type__c = 'Type I';
    	insert testReeg;	
    	
    	 cont.cancelAction();
    	
    	testReeg = rEEGDao.getTestReeg();
    	patientDao pDao = new patientDao();
    	patient = pDao.getById(testReeg.r_patient_id__c);
    	
    	patientOutcomeDao outDao = new patientOutcomeDao();
    	r_patient_outc__c testOutcome = outDao.getTestOutcome(patient.id);
		r_outcome_med__c testOutcMed = new r_outcome_med__c();
    	testOutcMed.med_name__c = 'Test';
     	testOutcMed.frequency__c = 'tid';
    	testOutcMed.dosage__c = 10;
    	testOutcMed.unit__c = 'mg';

    	if (testOutcome != null)
    	{
	    	testOutcMed.r_outcome_id__c = testOutcome.Id;
	    	insert testOutcMed;
    	}	
    	
    	r_patient_med__c testMed = new r_patient_med__c();
    	testMed.r_patient_id__c = patient.id;
    	testMed.med_name__c = 'Test Med';
    	insert testMed;

        Boolean val1 = cont.getIsMgr();
        Boolean notval = cont.getIsNotMgr();
        Boolean val = !notval;
        System.assertEquals( val1, val);
        
		List<r_patient_note__c> patNotesList = cont.getPatientNotes();
        List<ReegDisp> reegList = cont.getReegs();
        List<patientOutcomeDisp> outList = cont.getOutcomes();
        // System.assertEquals(testReeg, reegList[0].getObj());
        // System.assertEquals(testOutcome, outList[0].getObj());
        // System.assertEquals(testMed, medList[0].getObj());

        pRef7 = cont.deletePatient();
        
        List<patientIntervalListDisp> iList = cont.getIntervalDispList();
        
        System.assertEquals( '1', '1');
    }
 }