//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: DatabaseTableDao 
// PURPOSE: Data access class for Database table records
// CREATED: 06/14/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class DatabaseTableDao extends BaseDao
{
private static String NAME = 'database_table__c';    
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.database_table__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public database_table__c getById(String idInp)
    {
		return (database_table__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public List<database_table__c> getAll()
    {
		return (List<database_table__c>)getSObjectListByWhere(getFieldStr(), NAME, '');
    } 
    
    public List<database_table__c> getAllActive()
    {
		return (List<database_table__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'active\'');
    } 
    
  	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testDatabaseTableDao()
    {
        DatabaseTableDao dao = new DatabaseTableDao();
        
        database_table__c dbObj = DatabaseTableDao.getTestDbTable();
        
        dbObj = dao.getById(dbObj.Id);
        List<database_table__c> dbList = dao.getAll();
        dbList = dao.getAllActive();
    }   
    
    public static database_table__c getTestDbTable()
    {
    	database_table__c testDbTable = new database_table__c();
		testDbTable.Name = 'test';
    	insert testDbTable;
    	
    	return testDbTable;
    } 
}