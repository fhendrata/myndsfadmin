//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: GeneticTriggerHandlerTest
// PURPOSE: Test class for the GeneticTriggerHandler trigger class methods
// CREATED: 02/14/18 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class GeneticTriggerHandlerTest 
{
	@testSetup
    private static void setupOrgData()
    {
    	List<Triggers__c> triggerSettingList = new List<Triggers__c>();
        Triggers__c triggerItem = new Triggers__c(Name = 'Genetic__c', Is_Active__c = true);
        triggerSettingList.add(triggerItem);
        insert triggerSettingList;
    }

    @IsTest
    private static void testPatientInsertWithGeneticValues() 
    {
    	Contact con = ContactDao.getTestPhysician();

        Test.startTest();

       	r_patient__c testPat = new r_patient__c();
    	testPat.Name = 'Smith';
    	testPat.first_name__c = 'John';
    	testPat.dob__c = '01/01/1960';
    	testPat.physician__c = con.Id;
    	testPat.Genetic_Accession_Id__c = '123456';
    	testPat.Genetic_Patient_Id__c = '999999999';
    	insert testPat;

    	Genetic__c genRecord = null;
    	for (Genetic__c gen : [select Id, Patient__c, Translational_Accession_Id__c, Translational_Patient_Id__c from Genetic__c where Patient__c =: testPat.Id])
    	{
    		genRecord = gen;
    		break;
    	}

    	System.assertNotEquals(null, genRecord);
    	System.assertEquals(genRecord.Translational_Accession_Id__c, '123456');
    	System.assertEquals(genRecord.Translational_Patient_Id__c, '999999999');
        
        Test.stopTest();
    } 


    @IsTest
    private static void testPatientUpdateWithGeneticValues() 
    {
		r_patient__c pat = patientDao.getTestPat();  	

        Test.startTest();

        pat.Genetic_Accession_Id__c = '123456';
    	pat.Genetic_Patient_Id__c = '999999999';
    	update pat;
       
        Genetic__c genRecord = null;
    	for (Genetic__c gen : [select Id, Patient__c, Translational_Accession_Id__c, Translational_Patient_Id__c from Genetic__c where Patient__c =: pat.Id])
    	{
    		genRecord = gen;
    		break;
    	}

    	System.assertNotEquals(null, genRecord);
    	System.assertEquals(genRecord.Translational_Accession_Id__c, '123456');
    	System.assertEquals(genRecord.Translational_Patient_Id__c, '999999999');

        Test.stopTest();
    } 


}