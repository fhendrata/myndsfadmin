//--------------------------------------------------------------------------------
// COMPONENT: Ethos common
//     CLASS: rptDocDao
//   PURPOSE: data access object for report documents
// 
//     OWNER: CNSR
//   CREATED: 03/26/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class rptDocDao extends BaseDao
{
	private static String NAME = 'report_document__c';
	
	private static String fldList;	
	public static String getFieldStr()
	{
		
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.report_document__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public report_document__c getById(String idInp)
    {
		return (report_document__c)getSObjectById(getFieldStr(), NAME, idInp);
    }   
   
	public List<report_document__c> getVersions(string reegId)
	{
		string whereStr = 'is_delivered__c = false and r_finalized_by__c = \'\' and r_reeg__c = \'' + reegId + '\'';
		return (List<report_document__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'CreatedDate');
	}
	
	public List<report_document__c> getFinalized(string reegId)
	{
		string whereStr = 'is_delivered__c = false and r_finalized_by__c != \'\' and r_reeg__c = \'' + reegId + '\'';
		return (List<report_document__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'CreatedDate');
	}
	
	public List<report_document__c> getDelivered(string reegId)
	{
		string whereStr = 'is_delivered__c = true and r_reeg__c = \'' + reegId + '\'';
		return (List<report_document__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'CreatedDate');
	}
	
	public List<report_document__c> getActive(string reegId)
	{
		string whereStr = 'is_active__c = true and r_reeg__c = \'' + reegId + '\'';
		return (List<report_document__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'CreatedDate');
	}
	
	public static report_document__c getTestRptDoc()
    {
    	rEEGControllerExt r = new rEEGControllerExt();
    	r_reeg__c reeg = r.getTestReeg();
    	
    	report_document__c rptDoc = new report_document__c();
    	rptDoc.delivered_date__c = DateTime.now();	
    	rptDoc.r_delivered_by__c = UserInfo.getUserId();
		rptDoc.is_delivered__c = true;
		rptDoc.is_active__c = true;
		rptDoc.r_reeg__c = reeg.Id;
			
    	insert rptDoc;
    	
    	return rptDoc;
    }

	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testrptDocDao()
    {
        rptDocDao dao = new rptDocDao();
        dao.IsTestCase = true;
        
        System.assert(!e_StringUtil.isNullOrEmpty(rptDocDao.getFieldStr()));
        
        report_document__c testDoc = rptDocDao.getTestRptDoc();
        
        testDoc = dao.getById(testDoc.Id);
        testDoc.is_delivered__c = false;
        boolean boolVal = dao.saveSObject(testDoc);
        List<report_document__c> rptList = dao.getVersions(testDoc.r_reeg__c);
		testDoc.r_finalized_by__c = UserInfo.getUserId();
		boolVal = dao.saveSObject(testDoc);
        rptList = dao.getFinalized(testDoc.r_reeg__c);
		testDoc.is_delivered__c = true;
		boolVal = dao.saveSObject(testDoc);
        rptList = dao.getDelivered(testDoc.r_reeg__c);
        rptList = dao.getActive(testDoc.r_reeg__c);
        
        dao.ClassName = 'report_document__c';
        string testStr = dao.clean('test');
        dao.deleteSObject(testDoc);
        testDoc = rptDocDao.getTestRptDoc();
        dao.deleteSObjectById(testDoc.Id);
        testDoc = rptDocDao.getTestRptDoc();
        rptList.add(testDoc);
        dao.deleteSObjectList(rptList);
        dao.HideDeleted = true;
        testStr = dao.removeLeadingAnd('and test');
        dao.ObjectName = 'report_document__c';
    }    
}