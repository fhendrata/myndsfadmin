//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: rEEGPager
//   PURPOSE: Pager logic for the rEEG result items
// 
//     OWNER: CNS Response
//   CREATED: 10/27/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class rEEGPager extends BasePager 
{
    boolean isTest = false; 
    public string techId {get; set;}
    public string accountName {get; set;}
    public string listCategory {get; set;}

    public rEEGPager()
    {
        setDispRows(10);
    }

     //---Build the display list
    private List<reegDisp> dispList; 
    public List<reegDisp> getDispList()
    {
        if (dispList == null) loadData();
        return dispList;
    }

    public override void loadData()
    {
     	rEEGDao reegDao = new rEEGDao();
     	reegDao.IsTestCase = IsTestCase;
     	if (!e_StringUtil.isNullOrEmpty(techId) && techId != 'clinicAdmin')
			dispList = reegDao.getListForTech(this);
		else
			dispList = reegDao.getListForClinic(this);
    }

    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testItemPager()
    {
      	rEEGPager obj = new rEEGPager();
		obj.isTest = true;
		obj.IsTestCase = true;
        List<reegDisp> dispList = obj.getDispList();
        obj.loadData();
        dispList = obj.getDispList();

        System.assertEquals(1, 1);
    }
   
}