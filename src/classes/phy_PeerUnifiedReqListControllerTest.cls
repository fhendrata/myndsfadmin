//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: phy_PeerUnifiedReqListControllerTest
// PURPOSE: Test class for the phy_PeerUnifiedReqListController class methods
// CREATED: 01/09/15 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class phy_PeerUnifiedReqListControllerTest 
{
	private static testmethod void testReqListMethods() 
	{
		List<PEER_Requisition__c> reqList = new List<PEER_Requisition__c>();
		PEER_Requisition__c req1 = PEERRequisitionDao.getTestObj();
		phy_PeerUnifiedReqListController con = new phy_PeerUnifiedReqListController();

		Integer ctr = 0;

		for (Integer i = 0; i < 50; i++)
		{
			PEER_Requisition__c req = PEERRequisitionDao.getTestObj();
			if (math.mod(i, 3) == 0)
			{
				req.Status__c = 'Removed by user';
				ctr++;
			} 
			reqList.add(req);
		}

		Test.startTest();

		con.next();
		con.previous();

		Test.stopTest();
	}
	
}