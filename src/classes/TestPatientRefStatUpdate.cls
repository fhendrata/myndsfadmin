@isTest
private class TestPatientRefStatUpdate {
	
	@isTest static void testTrigger() {
		Lead testLead = new Lead(LastName='testLastName',Company='testComp');
		insert testLead;
		Patient_Referral__c testpr = patientReferralDao.getInstance().getTestPatientReferral();
		Patient_Referral__c testpr2 = patientReferralDao.getInstance().getTestPatientReferral2();
		testpr.Lead__c = testLead.Id;
		testpr2.Lead__c = testLead.Id;
		update testpr;
		update testpr2;

		Test.startTest();
		testpr.Physician_Referral_Status__c = 'Accepted';
		update testpr;
		Test.stopTest();

		testpr2 = patientReferralDao.getInstance().getById(testpr2.Id);
		System.assertEquals(testpr2.Physician_Referral_Status__c,'Missed');
	}
	
}