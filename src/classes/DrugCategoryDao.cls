//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: DrugCategoryDao
// PURPOSE: Data access class for Drug CAtegory records
// CREATED: 05/10/11 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class DrugCategoryDao 
 extends BaseDao
{
	private static String NAME = 'report_drug_category__c';    
	private static SysLogDao logDao {get; set;}  
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.report_drug_category__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public report_drug_category__c getById(String idInp)
    {
		return (report_drug_category__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public List<report_drug_category__c> getAll()
    {
    	return (List<report_drug_category__c>)getSObjectListByWhere(getFieldStr(), NAME, '', 'display_sequence__c');
    }

	public List<report_drug_category__c> getByCategoryName(string cName)
    {
    	List<report_drug_category__c> catList = null;
    	if (!e_StringUtil.isNullOrEmpty(cName))
    		catList = (List<report_drug_category__c>)getSObjectListByWhere(getFieldStr(), NAME, 'Name = \'' + cName + '\'', 'display_sequence__c');
		return catList; 
    } 
}