public class patientControllerExt extends BaseController
{
    public r_patient__c patient {get; set;}
    public List<r_patient_outc__c> outcomeList {get; set;}
    public List<r_outcome_med__c> previousMeds { get; set; }
    public String intervalBuildMessage { get; set; }
    public Boolean isWalterReed { get; set; }
   
//    private Profile profile;
    private Contact contact;

    //---this is only used for the apex test case
    public patientControllerExt()
    {
        patient = new r_patient__c();
        patient.dob__c = '12/12/1970';
        patient.first_name__c = 'test case patient2';

        Profile p = new Profile();
        p.Name = 'Physician Portal Profile';
        setProfile(p);

        contact = new Contact();
        contact.gender_cd__c = 'Male';
        intervalBuildMessage = '';
    }

    //---Constructor from the standard controller
    public patientControllerExt( ApexPages.StandardController stdController)
    {    
        this.patient = (r_patient__c)stdController.getRecord();
        intervalBuildMessage = '';
        isWalterReed = (String.isNotBlank(patient.Study_Group__c) && patient.Study_Group__c.equalsIgnoreCase('Walter Reed'));
       	init();
    }
    
    public void init()
    {
    	this.outcomes = getOutcomes();
        loadPermissions();
    }
    
    public PageReference buildIntervals()
    {
        patientIntervalBuilder intBuilder = new patientIntervalBuilder();
        intBuilder.buildAllIntervalsForPat(patient.Id, patient.OwnerId);
        intervalBuildMessage = 'Intervals updated';

        return null;
    }

    public PageReference Save()
    {
        if (patient.id != null) upsert patient;
        else insert patient;

        PageReference returnVal = new PageReference( '/' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }

          //--- med entry page
    public PageReference saveAndNew()
    {
        if (patient.id != null) upsert patient;
        else insert patient;

        PageReference returnVal = new PageReference( '/apex/patientNewPage');
        returnVal.setRedirect(true);
        return returnVal;
    }
    
        //-- stdController.getRecord(); does not get all the fields with an pbject, just the fields on the page. 
    	//-- Since we sometimes have to get/set other fields this method is necessary for that purpose.
    private r_patient__c getFullPatientRec(String patId)
    {
    	GlobalPatientDao patDao = new GlobalPatientDao();
    	return patDao.getById(patId);
    }

    //--- button action - change to treatment group
    public PageReference changeToTreamentGroupGeneral()
    {
        return updateGeneralControlGroup('Treatment Group');
    }
    
    //--- button action - change to not randomized group
    public PageReference changeToNotRandomizedGroupGeneral()
    {
        return updateGeneralControlGroup('Not Randomized Group');
    }
    
    //--- button action - change to control group
    public PageReference changeToControlGroupGeneral()
    {       
        return updateGeneralControlGroup('Control Group');
    }
    
    //--- button action - change to completed group
    public PageReference changeToCompletedGroupGeneral()
    {
        return updateGeneralControlGroup('Completed Group');
    }
    
    private PageReference updateGeneralControlGroup(String groupType)
    {
        PageReference pr = Page.PatientViewPage;
        this.patient = getFullPatientRec(patient.Id);
        if (patient != null)
        {
            patient.CA_Control_Group_Type__c = groupType;
            pr = Save();
            ActionDao.insertReegActionPEER(patient.Id, 'UPDATE_PAT_STATUS');
        }
        
        return pr;
    }

    public Boolean getCanViewStudy()
    {
        String profileName = getProfileName();
        return (ProfilePermissionMap.containsKey(profileName)) ? ProfilePermissionMap.get(profileName).Study_Related_View__c : false;
    }

    public Boolean getCanEditStudy()
    {
        String profileName = getProfileName();
        return (ProfilePermissionMap.containsKey(profileName)) ? ProfilePermissionMap.get(profileName).Study_Related_Edit__c : false;   
    }

    public Boolean getCanViewIntervals()
    {
        String profileName = getProfileName();
        return (ProfilePermissionMap.containsKey(profileName)) ? ProfilePermissionMap.get(profileName).View_Intervals__c : false;
    }   

    public Boolean getCanViewTestFlag()
    {
        String profileName = getProfileName();
        return (ProfilePermissionMap.containsKey(profileName)) ? ProfilePermissionMap.get(profileName).View_Test_Flag__c : false;
    }   

         //--- med entry page
    public PageReference medListEditPage()
    {
        return new PageReference('/apex/patientMedListEditPage?id=' + patient.id);
    }
    
    public PageReference medListPastEditPage()
    {
        return new PageReference('/apex/patientMedListPastEditPage?id=' + patient.id);
    }

    //new outcome
    public PageReference patOutcomeEditPage()
    {
        return new PageReference('/r_patient__c.object?id=' + patient.id);
    }
    
    public PageReference patSharingPage()
    {
        return new PageReference('/p/share/CustomObjectSharingDetail?parentId=' + patient.id);
    }
    
	List<PatientOutcomeMedDisp> omeds;
	public List<PatientOutcomeMedDisp> getoMeds()
    {
    	if (omeds == null)
        {
            omeds = new List<PatientOutcomeMedDisp>();  
            PatientOutcomeMedDisp oMed;          
            Integer ctr = -1;
             
			patientOutcomeMedDao medDao = new patientOutcomeMedDao();
            previousMeds = medDao.getExistingOutcomeMeds(patient.id);
            if (previousMeds != null) 
            {
            	// holdingList = previousMeds.deepClone(false);
	            for(r_outcome_med__c oMedRow : previousMeds) 
	            {
			        ctr++;  
		            oMed = new PatientOutcomeMedDisp();
		            oMed.setDisplayAction(true);
		            oMed.setRowNum(ctr);
		                
 	                /*r_outcome_med__c hldgMed = new r_outcome_med__c();
	                hldgMed.med_name__c = oMedRow.med_name__c;
	                hldgMed.dosage__c = oMedRow.dosage__c;
	                hldgMed.unit__c = oMedRow.unit__c;
	                hldgMed.frequency__c = oMedRow.frequency__c;
	                hldgMed.start_date__c = oMedRow.start_date__c;
	                hldgMed.end_date__c = oMedRow.end_date__c;
	                hldgMed.previous_med__c = oMedRow.Id;
	                hldgMed.start_date_is_estimated__c = oMedRow.start_date_is_estimated__c;*/

					//oMed.Setup(hldgMed);
	                oMed.Setup(oMedRow);
	                // previousIds.Add(oMedRow.Id);
	                omeds.Add(oMed);
	            }
            }             
        }    
        
        return omeds;
    }

	//-- 10.21.10 - jdepetro - replaced with outcome meds above
        //---Get the med list for the patient page
    private List<PatientMedDisp> meds;   
    public List<PatientMedDisp> getMeds()
    {
         if (meds == null)
         {
             meds = new List<PatientMedDisp>();  

             if (patient.id != null)
             {
                for(r_patient_med__c medRow : 
                     [select dosage__c, drugreview__c, end_m_pl__c, end_y_pl__c, generic__c, is_wo_extended__c, med_name__c, no_dosage__c, reason_stop_text__c, reason_stop_pl__c, effect_pl__c, side_effects__c, start_m_pl__c, start_y_pl__c, status__c, symtoms__c, Unit__c, id, name, washout_required__c 
                     from r_patient_med__c
                     where r_patient_id__c = :patient.id
                     order by CreatedDate desc])
                {
                    PatientMedDisp med = new PatientMedDisp();
                    med.Setup( medRow );                                 
                    meds.Add( med);
                } 
            }     
        }    

        return meds;
    }

    private List<ReegDisp> reegs;   
    public List<ReegDisp> getReegs()
    {
         if (reegs == null)
         {
             reegs = new List<ReegDisp>();  

             if (patient.id != null)
             {
                for(r_reeg__c reegRow : 
                     [select id, name, req_stat__c, req_date__c, reeg_type__c, reeg_type_mod__c
                     from r_reeg__c
                     where r_patient_id__c = :patient.id order by req_date__c desc])
                {
                    ReegDisp reeg = new ReegDisp();
                    reeg.Setup( reegRow );                                 
                    reegs.Add( reeg);
                } 
            }     
        }    
        return reegs;
    }  

    //---Action for new rEEG
    public PageReference rEEGNewPage()
    {
        PageReference returnVal = new PageReference( '/apex/patientWizard1Page?id=' + patient.id + '&qsid=&np=0');
        returnVal.setRedirect(true);
        return returnVal;
    }

           //--- rEEG Anlysis page
    public PageReference rEEGAnalysisPage()
    {
        PageReference returnVal = new PageReference('/apex/rEEGAnalysisPage?patId=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }

    //---Outcomes for the Patient
    private List<patientOutcomeDisp> outcomes;   
    public List<patientOutcomeDisp> getOutcomes()
    {
	   	string outcomesMeds = '';
	   	string patId = patient.id;
        if (outcomes == null)
        {
    	   	patientOutcomeDao outDao = new patientOutcomeDao();
            outcomes = outDao.getPatOutcomeDispListNonWR(patient.id);  
        }    
        
        return outcomes;
    }  

    //---Action for new outcome
    public PageReference newOutcome()
    {
        PageReference returnVal = new PageReference( '/apex/patientOutcomeEditPage?pid=' + patient.id + '&isNew=true&retUrl=/apex/PatientViewPage?id=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    //---Outcomes for the Patient
    private List<patientIntervalListDisp> intervalDispList;   
    public List<patientIntervalListDisp> getIntervalDispList()
    {
       	patientIntervalsDao intDao = new patientIntervalsDao();
	 	List<r_interval__c> intList = intDao.getByPatId(patient.id);
  			
       	patientIntervalListDisp intDisp;
       	intervalDispList = new List<patientIntervalListDisp>();
      	
       	if (intList != null && intList.size() > 0)
       	{
	       	for (r_interval__c row : intList)
	       	{
	       		intDisp = new patientIntervalListDisp();
	       		intDisp.Interval = row;
	       		if (row.start_date__c != null && row.stop_date__c != null)
	       			intDisp.Duration = row.start_date__c.daysBetween(row.stop_date__c);
				
				intervalDispList.add(intDisp);
	       	}
       	}
       	
        return intervalDispList;

    }  
    
    public PageReference deletePatient()
    {
        if (getIsMgr()) delete patient;

        String patientPrefix = rEEGUtil.getPatientPrefix();
        PageReference returnVal = new PageReference('/' + patientPrefix + '/o');
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public List<r_patient__History> getPatHistList()
    {
       List<r_patient__History> patHistList = new List<r_patient__History>(); 

       if (patient.id != null)
       {
         	patHistList = [Select ParentId, OldValue, NewValue, Id, Field, CreatedDate, CreatedById 
                   			From r_patient__History
                   			where ParentId = :patient.id
                   			order by CreatedDate Desc limit 25];
       }     
       
       return patHistList;
    } 

 }