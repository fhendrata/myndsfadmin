//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: e_StringUtil
//   PURPOSE: Utility data class
// 
//     OWNER: CNSR
//   CREATED: 03/25/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class e_StringUtil 
{
    public static Boolean isNullOrEmpty(String value)
    {
        return value == null || value == '' || value.equalsIgnoreCase('null');
    }
    
    public static Boolean isNullStr(String value)
    {
        return value == 'null';
    }

	public static Boolean isMatch(String value1, String value2, Boolean ignoreCase)
	{
		Boolean returnVal = false;
		
		if (!isNullOrEmpty(value1) && !isNullOrEmpty( value2))
		{
			if (ignoreCase)
			{
				returnVal = value1.equalsIgnoreCase( value2);
			}
			else
			{
				returnVal = value1.equals( value2);
			}
		}
		
		return returnVal;		
	}

    public static String clean(String value)
    {
        String returnVal = '';

        if (!isNullOrEmpty(value))
        {
        	returnVal = value.replaceAll( '<>', '');
        }

        return returnVal;
    }

    public static Boolean isInList(String csList, String key)
    {
        Boolean returnVal = false;
        String[] sFields = csList.split(',');
        for (Integer i = 0; i < sFields.size(); i++)
        {
            if (sFields[i] != null && sFields[i].Equals(key))
                returnVal = true;
        }
        return returnVal;
    }
    
    public static String getDateTimeString(DateTime inputVal)
    {
    	if (inputVal == null) return '';    		 
    	return inputVal.format();
    }
    
    public static String getDateString(Date inputVal)
    {
    	if (inputVal == null) return '';    		 
    	return inputVal.format();
    }
    
    public static Date setDateFromString(String inputVal)
    {
    	Date returnVal = null;
    	
    	try
    	{
           	returnVal = date.parse(inputVal);
        } 
        catch (Exception e)
        {
        	//DebugMsg = inputVal + e;           	
        }
    	    	
    	return returnVal;    
    }
    
    public static DateTime setDateTimeFromString(String inputVal)
    {
    	DateTime returnVal = null;
    	
    	try
    	{
           	returnVal = datetime.parse(inputVal);
        } 
        catch (Exception e)
        {
        	//DebugMsg = inputVal + e;           	
        }
    	    	
    	return returnVal;    
    }


    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testDataUtil()
    {
        System.assertEquals(e_StringUtil.isNullOrEmpty(''), true);
        System.assertEquals(e_StringUtil.isNullOrEmpty(null), true);
        System.assertEquals(e_StringUtil.isNullStr('null'), true);
        System.assertEquals(e_StringUtil.isNullOrEmpty('test'), false);
        String dirtyStr = 'xx<>yy';
      	String cleanStr = e_StringUtil.clean(dirtyStr);
		System.assert(cleanStr == 'xxyy');
        System.assert(e_StringUtil.isNullOrEmpty(''));
        System.assert(e_StringUtil.isMatch('xxx', 'xxx', true));
        System.assert(e_StringUtil.isMatch('xxx', 'xxx', false));
        boolean isMatch = e_StringUtil.isMatch('xxx', 'xyy',true);
        System.assert(!isMatch);
        System.assert(e_StringUtil.isInList('Id,Name', 'Id'));
        
        string test1 = e_StringUtil.getDateTimeString(DateTime.now());
        DateTime dt1 = e_StringUtil.setDateTimeFromString(test1);
    	test1 = e_StringUtil.getDateString(Date.today());
   		Date dt = e_StringUtil.setDateFromString(test1);
    }
}