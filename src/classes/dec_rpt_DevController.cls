//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: dec_rpt_DevController
// PURPOSE: Development controller for the Online Reporting Rules Based Decision engine
// CREATED: 06/10/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class dec_rpt_DevController extends BaseDrugReportController
{
	//public List<dec_action__c> ActionList {get; set;}
	private dec_ActionDao actDao {get; set;}
	private dec_action__c action {get; set;}
	public List<dec_action__c> ActionList {get; set;}
	public string reegId {get; set;}
	public string listType {get; set;}
	public boolean isActionSelectEdit {get; set;}
	DrugTreeMenuUtil menuObj {get; set;}
	public DrugTreeMenuUtil.MenuItem selectedMenuItem {get; set;}
	
	public rEEGXmlUtil.ActionObj[] ActArray {get; set;}
	
	//-- drug category parameters
	public boolean isCategoryEdit {get; set;}
	public List<report_drug_category__c> categoryList {get; set;}
	public report_drug_category__c category {get; set;}
	public string selectCategory {get; set;}
	public string previousSelectCategory {get; set;}
	public string selectCategoryName {get; set;}
	public string resultStr {get; set;}
	
	public dec_rpt_DevController()
	{
		actDao = new dec_ActionDao();
		IsTestCase = false;
	}
	
	public dec_rpt_DevController(ApexPages.StandardController std)
	{
		actDao = new dec_ActionDao();
		IsTestCase = false;
	}
	
	public PageReference loadAction()
	{
		reegId = ApexPages.currentPage().getParameters().get('rid');
		string edit = ApexPages.currentPage().getParameters().get('edit');
		string editSeq = ApexPages.currentPage().getParameters().get('editSq');
		string listType = ApexPages.currentPage().getParameters().get('l');
		pageId = ApexPages.currentPage().getParameters().get('pgid');
		tabId = ApexPages.currentPage().getParameters().get('tbid');
		
		if (e_StringUtil.isNullOrEmpty(pageId)) pageId = '1';
		if (!e_StringUtil.isNullOrEmpty(tabId)) selectCategory = tabId;
		
		menuObj = new DrugTreeMenuUtil();
		if (menuObj != null && menuObj.MenuList != null)
		{		
			selectedMenuItem = menuObj.MenuList.get(pageId);
		}
		
		isActionSelectEdit = (!e_StringUtil.isNullOrEmpty(edit) && edit.equals('1'));
		
		baseUrl = '/apex/RptDecisionDevPage?rid=' + reegId;
		
		loadCategories();
		loadActionList();
		
		return null;
	}
	
	public PageReference categorySelect()
	{
		loadActionListByCategory();
		return null;
	}
	
	private void loadCategories()
	{
		DrugCategoryDao dcDao = new DrugCategoryDao();
		categoryList = dcDao.getAll();
		
		if (e_StringUtil.isNullOrEmpty(selectCategory) && categoryList != null && categoryList.size() > 0)
		{
			selectCategory = categoryList[0].Id;
			selectCategoryName = categoryList[0].Name;
		}
	/*
		else
		{
			for (report_drug_category__c row : categoryList)
			{
				if (row.Id == selectCategory)
				{
					selectCategoryName = row.Name;
					break;
				}
			}
		}
	*/	
		category = new report_drug_category__c();
		isCategoryEdit = false;
	}
	
	public PageReference loadActionList()
	{
		if (e_StringUtil.isNullOrEmpty(listType)) listType = 'Active-Pending';
		
		loadActionListByCategory();
		
		return null;
	}
	
	public PageReference loadActionListByCategory()
	{
		if(selectedMenuItem != null)
		{
			if (listType == 'Active-Pending')
				ActionList = actDao.getAllReportActivePending(selectCategory, selectedMenuItem.drugName);
			if (listType == 'Active')
				ActionList = actDao.getAllReportActive(selectCategory, selectedMenuItem.drugName);
			if (listType == 'Pending')
				ActionList = actDao.getAllReportPending(selectCategory, selectedMenuItem.drugName);
			if (listType == 'InActive')
				ActionList = actDao.getAllReportInactive(selectCategory, selectedMenuItem.drugName);
		}
			
		return null;
	}
	
	public PageReference newDecAction()
	{
		PageReference pr = new PageReference('/apex/RptDecisionActionEditPage');
		pr.getParameters().put('rid', reegId);
		pr.getParameters().put('cid', selectCategory);
		
		if (selectedMenuItem != null) 
		{
			pr.getParameters().put('d', selectedMenuItem.drugName);
			pr.getParameters().put('pgid', selectedMenuItem.num);
		}
		
		if (!e_StringUtil.isNullOrEmpty(selectCategory))
			pr.getParameters().put('tbid', selectCategory);
		
		return pr;
	}
	
	public PageReference runTestAction()
	{
		PageReference returnVal = null;
		
		return returnVal;
	}
	
	public PageReference editDecActions()
	{
		PageReference pr = new PageReference('/apex/RptDecisionDevPage');
		pr.getParameters().put('rid', reegId);
		pr.getParameters().put('cid', selectCategory);
		pr.getParameters().put('edit', '1');
		
		if (selectedMenuItem != null) 
		{
			pr.getParameters().put('d', selectedMenuItem.drugName);
			pr.getParameters().put('pgid', selectedMenuItem.num);
		}
		
		if (!e_StringUtil.isNullOrEmpty(selectCategory))
			pr.getParameters().put('tbid', selectCategory);
		
		return pr;
	}
	
	public PageReference saveDecActions()
	{
		updateActionLists();
		
		PageReference pr = new PageReference('/apex/RptDecisionDevPage');
		pr.getParameters().put('rid', reegId);
		pr.getParameters().put('cid', selectCategory);
		pr.getParameters().put('edit', '0');
		
		if (selectedMenuItem != null) 
		{
			pr.getParameters().put('d', selectedMenuItem.drugName);
			pr.getParameters().put('pgid', selectedMenuItem.num);
		}
		
		if (!e_StringUtil.isNullOrEmpty(selectCategory))
			pr.getParameters().put('tbid', selectCategory);
		
		return pr;
	}
	
	public PageReference editDecSeq()
	{
		PageReference pr = new PageReference('/apex/RptDecisionDevPage');
		pr.getParameters().put('rid', reegId);
		pr.getParameters().put('cid', selectCategory);
		pr.getParameters().put('editSq', '1');
		
		if (selectedMenuItem != null) 
		{
			pr.getParameters().put('d', selectedMenuItem.drugName);
			pr.getParameters().put('pgid', selectedMenuItem.num);
		}
		
		if (!e_StringUtil.isNullOrEmpty(selectCategory))
			pr.getParameters().put('tbid', selectCategory);
		
		return pr;
	}
	
	public PageReference saveDecSeq()
	{
		PageReference pr = new PageReference('/apex/RptDecisionDevPage');
		pr.getParameters().put('rid', reegId);
		pr.getParameters().put('cid', selectCategory);
		pr.getParameters().put('editSq', '0');
		
		if (selectedMenuItem != null) 
		{
			pr.getParameters().put('d', selectedMenuItem.drugName);
			pr.getParameters().put('pgid', selectedMenuItem.num);
		}
		
		if (!e_StringUtil.isNullOrEmpty(selectCategory))
			pr.getParameters().put('tbid', selectCategory);
		
		return pr;
	}
	
	private void updateActionLists()
	{
		actDao.saveSObjectList(ActionList);
	}
	
	//------------ drug category actions ---------------------------
	
	public PageReference SaveDrugCategoryAction()
	{
		isCategoryEdit = false;
		
		DrugCategoryDao dcDao = new DrugCategoryDao();
		dcDao.saveSObject(category);
		loadCategories();
		
		return null;
	}
	
	public PageReference CancelDrugCategoryAction()
	{
		isCategoryEdit = false;
		category = new report_drug_category__c();
		return null;
	}
	
	public PageReference EditCategoriesAction()
	{
		isCategoryEdit = true;
		return null;
	}
	
	public PageReference DeleteSelectedCategoryAction()
	{
		DrugCategoryDao dcDao = new DrugCategoryDao();
		dcDao.deleteSObjectById(selectCategory);
		return null;
	}
	
	public PageReference runXmlTestAction()
	{
		PageReference returnVal = null;
		
		ActArray = null;
		String xmlString = '';
		string drugName = '';
		
		if (selectedMenuItem != null) 
		{
			drugName = selectedMenuItem.drugName;
		}
		
		if (!IsTestCase)	
		{	
			dec_ActionWs2.RulesWsSoap ws = new dec_ActionWs2.RulesWsSoap();
			ws.timeout_x = 60000;
			xmlString = ws.testReportAction('', reegId, drugName);
			system.debug('@@@ xmlString:' + xmlString);
		}
		else
		{
			xmlString = '<dec><result>True</result><act><n>Bridged leads</n><v></v><rules><rule><n>2</n><v>True</v></rule></rules></act><act><n>test 1</n><v>True</v><rules><rule><n>18</n><v>True</v></rule><rule><n>20</n><v>False</v></rule><rule><n>19</n><v>False</v></rule></rules></act></dec>';
		}

		
		if (!e_StringUtil.isNullOrEmpty(xmlString))
		{
//			resultStr = xmlString;	// testing
			ActArray = rEEGXmlUtil.parseActions(xmlString);
		}
		else
		{
			resultStr = 'Error: No xml return from web service. Possible loss of rEEG Id parameter, please go back to rEEG record and start again.';
		}
		
		return returnVal;
	}
	

	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testActDevCont()
    {
        dec_rpt_DevController cont = new dec_rpt_DevController();
        cont.IsTestCase = true;
        
		r_reeg__c reeg = rEEGDao.getTestReeg();
		ApexPages.currentPage().getParameters().put('rid', reeg.Id);
        
        dec_action__c actObj = dec_ActionDao.getTestAction();
     	List<dec_action__c> actList = new List<dec_action__c>();
     	actList.add(actObj);
     	
     //	cont.ActionList = actList;
		cont.actDao = new dec_ActionDao();
		cont.action = actObj;
		cont.reegId = reeg.Id;			
		
		cont.isActionSelectEdit = true;
		
		PageReference pr = cont.newDecAction();
		pr = cont.loadAction();
		pr = cont.runTestAction();
    }   
}