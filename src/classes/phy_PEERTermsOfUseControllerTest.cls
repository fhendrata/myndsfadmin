//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: phy_PEERTermsOfUseControllerTest
// PURPOSE: Unit test method for the phy_PEERTermsOfUseController Controller class
// CREATED: 04/22/16 Ethos Solutions - www.ethos.com
// Author: Mitchell Corbin
//--------------------------------------------------------------------------------
@isTest
public with sharing class phy_PEERTermsOfUseControllerTest {

	private static testmethod void testController()
	{
		TOU_Document_Name_Settings__c currentTOUDocSettings = TOU_Document_Name_Settings__c.getInstance('Default');
		if (currentTOUDocSettings != null)
			delete currentTOUDocSettings;

		TOU_Document_Name_Settings__c TOUDocSettings = new TOU_Document_Name_Settings__c(Name = 'Default', TOU_Document_Name__c = 'MYnd Analytics TOU');
		insert TOUDocSettings;

		Test.startTest();

		PEER_Documentation__c testDoc = PEERDocDao.getTestPEERDoc();
		ApexPages.StandardController sc = new ApexPages.standardController(testDoc);

		phy_PEERTermsOfUseController tos = new phy_PEERTermsOfUseController(sc);
		tos.loadAction();

		// Testing the cancelAction
		PageReference prActual = tos.cancelAction();
		PageReference prExpected = new PageReference('/secur/logout.jsp');
		System.assertEquals(prActual.getUrl(), prExpected.getUrl());

		// Testing the okAction when TOUAgreed = false
		tos.TOUAgreed = false;
		prActual = tos.okAction();
		prExpected = null;
		System.assertEquals(prActual, prExpected);
		System.assertEquals(tos.NotAgreedOK, true);

		// Testing the okAction when TOUAgreed = true
		tos.TOUAgreed = true;
		tos.ReturnURL = '/apex/phy_PeerListS2';
		prActual = tos.okAction();
		prExpected = new PageReference('/apex/phy_PeerListS2');
		System.assertEquals(prActual.getUrl(), prExpected.getUrl());
		System.assertEquals(tos.NotAgreedOK, false);

		// Setting the DocID
		// tos.DocID = '01560000001RPVA';

		Test.stopTest();
	}
}