//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: patientTriggerHandler
// PURPOSE: class to handle patient trigger events
// CREATED: 01/10/17 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class patientTriggerHandler 
{

	public static void updatePatAndReegForDob(List<r_patient__c> patList)
	{
		map<Id, Decimal> patAgeMap = new map<Id, Decimal>();

	    for (r_patient__c pat : patList)
	    {
	    	if (String.isNotBlank(pat.dob__c))
	        {
	            Date dobDt = e_StringUtil.setDateFromString(pat.dob__c);
	            integer numberDaysOld = dobDt.daysBetween(Date.today());             
	            if (numberDaysOld > 0)
	            {
	                pat.patient_age__c = (numberDaysOld / 365.242199).setScale(1);
	                patAgeMap.put(pat.Id, pat.patient_age__c);
	            }
	        }
	    }

	    List<r_reeg__c> reegUpdateList = new List<r_reeg__c>();

	    for (r_reeg__c reeg : [select Id, patient_age__c, r_patient_id__c from r_reeg__c where r_patient_id__c IN :patAgeMap.keyset()])
	    {
	    	reeg.patient_age__c = patAgeMap.get(reeg.r_patient_id__c);
	    	reegUpdateList.add(reeg);
	    }

	    update reegUpdateList;
	}

	public static void handleInsert(List<r_patient__c> patList)
	{
		List<Genetic__c> geneticObjList = new List<Genetic__c>();
	    for (r_patient__c pat : patList)
	    {
	    	if (String.isNotBlank(pat.Genetic_Accession_Id__c) || String.isNotBlank(pat.Genetic_Patient_Id__c))
	        {
	        	System.debug('pat.Id: ' + pat.Id);
	        	Genetic__c  g = new Genetic__c();
	        	g.Patient__c = pat.Id;
	        	g.Translational_Accession_Id__c = pat.Genetic_Accession_Id__c;
	        	g.Translational_Patient_Id__c = pat.Genetic_Patient_Id__c;
	        	geneticObjList.add(g);
	        }
	    }

	    if (!geneticObjList.isEmpty())
	    	insert geneticObjList;
	}

	public static void handleUpdate(Map<Id, r_patient__c> patMap)
	{
	    List<Id> patIdList = new List<Id>(patMap.keySet());
		Map<Id, Genetic__c> geneticObjMap = new Map<Id, Genetic__c>();

	    //-- this will update any current Genetic__c records that have a patient Id matching a patient Id in the update map.
	    for (Genetic__c gen : [select Id, Patient__c, Translational_Accession_Id__c, Translational_Patient_Id__c from Genetic__c where Patient__c in :patIdList])
	    {
	    	gen.Translational_Accession_Id__c = patMap.get(gen.Patient__c).Genetic_Accession_Id__c;
	    	gen.Translational_Patient_Id__c = patMap.get(gen.Patient__c).Genetic_Patient_Id__c;
	    	geneticObjMap.put(gen.Patient__c, gen);
	    }

	   	List<Genetic__c> newAdditionList = new List<Genetic__c>();
	    for (r_patient__c pat : patMap.values())
	    {
	    	if (!geneticObjMap.containsKey(pat.Id) && (String.isNotBlank(pat.Genetic_Accession_Id__c) || String.isNotBlank(pat.Genetic_Patient_Id__c) ))
	        {
	        	Genetic__c  g = new Genetic__c();
	        	g.Patient__c = pat.Id;
	        	g.Translational_Accession_Id__c = pat.Genetic_Accession_Id__c;
	        	g.Translational_Patient_Id__c = pat.Genetic_Patient_Id__c;
	        	newAdditionList.add(g);
	        }
	    }

	    newAdditionList.addAll(geneticObjMap.values());

	    if (!newAdditionList.isEmpty())
	    	upsert newAdditionList;
	}
	
}