public with sharing class wr_PeerListController extends BaseController
{
    private string searchCriteria {get; set;}
    private integer resultTableSize {get; set;}
    public string newFilterId {get; set;}
    public ApexPages.StandardSetController con {get; set;}
    
    public Boolean hasNext { get { return con.getHasNext(); } set; }  
    public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }  
    public Boolean hasResults { get { return (resultNumber > 0); } set; }  
    public Integer pageNumber  { get { return con.getPageNumber(); } set; }
    public Integer resultNumber  { get { return con.getResultSize(); } set; }  
    public void previous() { con.previous(); }  
    public void next() { con.next(); }
    public String patSearchString {get; set;}  
    
    private rEEGDao rDao; 
    
    private String sortDirection { get; set; }
    private String sortExp { get; set; }
    public String sortExpression 
    { 
    	get { return sortExp; }
	    set {
	       if (value == sortExp)
	         sortDirection = (sortDirection == 'ASC') ? 'DESC' : 'ASC';
	       else
	         sortDirection = 'ASC';
	       sortExp = value;
	    }
   	}
    
    public wr_PeerListController()
    {
    	rDao = new rEEGDao();
        resultTableSize = RESULT_TABLE_SIZE;
        setupFilter();
        loadPeers(); 
    }
    
    private void setupFilter()
    {
    	sortExp = 'req_date__c';
    	sortDirection = 'DESC';

        if(getIsClinicAdmin())
        {
        	Id ftBAcctId = null;
        	Id wrAcctId = null;
        	AccountDao acctDao = new AccountDao();
    		List<Account> acctList = acctDao.getByName('Fort Belvoir Community Hospital');
    		if (acctList != null && !acctList.IsEmpty())
    			ftBAcctId = acctList.get(0).Id;
    		acctList = acctDao.getByName('Walter Reed Army Medical Center');
    		if (acctList != null && !acctList.IsEmpty())
    			wrAcctId = acctList.get(0).Id;
    		
    		if (ftBAcctId != null && wrAcctId != null)
        		rDao.filter = '(r_patient_id__r.physician__r.AccountId = ' + quote(wrAcctId) + ' OR r_patient_id__r.physician__r.AccountId = ' + quote(ftBAcctId) + ')'; 
        	else if (ftBAcctId != null)
        		rDao.filter = 'r_patient_id__r.physician__r.AccountId = ' + quote(ftBAcctId); 
        	else if (wrAcctId != null)
        		rDao.filter = 'r_patient_id__r.physician__r.AccountId = ' + quote(wrAcctId); 
        	else
        		rDao.filter = 'r_patient_id__r.physician__r.AccountId = ' + quote(getCurrentAccount());
        }
        else rDao.filter = 'r_patient_id__r.physician__c = ' + quote(getCurrentPhysician());
    }
    
    
    private PageReference loadPeers()
    {
    	String query = rDao.getMyPeerListQuery(getCurrentPhysician(), '', getOrderText());
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
        return null;
    }
    
    public List<reegDisp> getPeers()
    {
    	List<reegDisp> reegDispList = null;
        if(con != null)  
        {
        	List<r_reeg__c> reegList = (List<r_reeg__c>)con.getRecords();
        	
        	if (reegList != null && reegList.size() > 0)
        	{
        		reegDispList = new List<reegDisp>();
        		reegDisp newObj;
        		for (r_reeg__c row : reegList)
        		{
        			newObj = new reegDisp(row);
        			reegDispList.add(newObj);
        		}
        	}
        } 
        
        return reegDispList;
    }
    
    public void searchPatients()
    { 
        String query = rDao.getNameSearchQuery(patSearchString);
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
    }
    
    public String getSortDirection()
	{
	    if (sortExpression == null || sortExpression == '')
	      return 'ASC';
	    else
	     return sortDirection;
	}
	
	public String getOrderText()
	{
	    return ' order by ' + sortExpression  + ' ' + sortDirection + '  NULLS LAST';
	}
	
	public void sortReportId()
    {
    	sortExpression = 'name';
    	loadPeers();
    }
    
    public void sortPatientName()
    {
    	sortExpression = 'r_patient_id__r.Name';
    	loadPeers();
    }
    
    public void sortDOB()
    {
    	sortExpression = 'r_patient_id__r.dob__c';
    	loadPeers();
    }
    
    public void sortProviderId()
    {
    	sortExpression = 'r_patient_id__r.prov_pat_id__c';
    	loadPeers();
    }
    
    public void sortPeerDate()
    {
    	sortExpression = 'req_date__c';
    	loadPeers();
    }
    
    public void sortCompletedDate()
    {
    	sortExpression = 'rpt_date__c';
    	loadPeers();
    }
    
    public void sortStatus()
    {
    	sortExpression = 'req_stat__c';
    	loadPeers();
    }
    
    
    //-------- TEST METHODS ----------------
    
    
    private static testmethod void testController()
    {
    	wr_PeerListController plc = new wr_PeerListController();
    	plc.patSearchString = 'a';
    	plc.searchPatients();
    	plc.getPeers();
    }

}