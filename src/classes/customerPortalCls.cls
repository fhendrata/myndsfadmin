public class customerPortalCls 
{
 	   	public Contact c { get; set; }
       	public User u { get; set; }
       
       	public customerPortalCls() 
       	{
		}
		
		public PageReference loadAction()
		{
			try
			{
        	   c = [select id, Email, LastName, FirstName from Contact where id = '003R000000LN6Oh' limit 1];
			}catch(Exception e){}
			
			return null;
		}
    	
       	public PageReference createPortaluser()
       	{
       	
        	//customerPortalCls.createUser(c.id, c.Email, c.FirstName, c.LastName, 'user1@ethos.com','00e60000000v6Iw');
  	    	return null;
       	}
       
	    @future 
	    static void createUser(String contactId, String email, String firstname, String lastname, String username, String profileId) 
	    {
    		User u = new User();
    		u.alias = 'testU1';
    		u.email = email;
	        u.emailencodingkey = 'UTF-8';
	        u.lastname = lastname;
	        u.languagelocalekey = 'en_US'; 
            u.localesidkey = 'en_US';
            u.profileid = profileId;
            u.contactId = contactId;
            u.timezonesidkey = 'America/Los_Angeles';
            u.username = username;
        
        	Database.DMLOptions dmlOpt = new Database.DMLOptions();
			dmlOpt.EmailHeader.triggerUserEmail = true;
        	u.setOptions(dmlOpt);
        	
        	try {
        	    insert u;
        	}catch(Exception e)
        	{
        		system.debug('Exception: ' + e);
        	}
    	}
    	
    	private static testmethod void testClass()
    	{
    	    Test.startTest();
    	    Contact c = ContactDao.getTestPhysician();
    	    customerPortalCls cls = new customerPortalCls();
    	    cls.loadAction();
    	    
    	    try {
    	        cls.createPortaluser();
    	    }
    	    catch(Exception e)
    	    {
    	        system.debug('Exception: ' + e);	
    	    }
    	    
    	    Test.stopTest();	
    	}
}