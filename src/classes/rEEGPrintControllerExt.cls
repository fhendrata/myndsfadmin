public class rEEGPrintControllerExt 
{
    private r_reeg__c reeg;
    private String reegId;
    public string patName {get; set;}
    
    public rEEGPrintControllerExt()
    {
        //---this is only used for the apex test case
        reeg = new r_reeg__c();
        reeg.reeg_type__c = 'Type I';
        reegId = ApexPages.currentPage().getParameters().get('id');
        
    }

    //---Constructor from the standard controller
    public rEEGPrintControllerExt( ApexPages.StandardController stdController)
    {    
        this.reeg = (r_reeg__c)stdController.getRecord();
        reegId = ApexPages.currentPage().getParameters().get('id');
    }
    
    public PageReference loadAction()
    {
    	reeg = getReeg();
    	patName = getPatientFullName();
    	return null;
    }

    public r_reeg__c getReeg()
    {
       	if (!e_StringUtil.isNullOrEmpty(reegId))
       	{
        	rEEGDao rDao = new rEEGDao();
        	reeg = rDao.getById(reegId);
        }
       
       	return reeg;
    }

    private r_patient__c getPatient()
    {
        if (reeg != null && !e_StringUtil.isNullOrEmpty(reeg.Id))
        {
			patientDao patDao = new patientDao();
			return patDao.getByReegId(reeg.Id);
        }   
        return null; 
    }

    public String getPatientFullName()
    {
        r_patient__c pat = getPatient();
        
         
        System.debug('### pat:' + pat);

        String returnVal = '';
        if (pat != null && pat.first_name__c != null)
        {
            returnVal = pat.first_name__c + ' ' + pat.name;
        }

        return returnVal;
    }

    public String getReqDateStr()
    {
        if (reeg == null) reeg = getReeg();
        String dateStr = '';
        if (reeg != null && reeg.req_date__c != null)
        {
        	DateTime reqdate = reeg.req_date__c;
        	if (reqdate != null) dateStr = reqdate.format('MM-dd-yyyy');
        }
        return dateStr;
    }

       //---Get the saved Washout Meds
    private List<r_reeg_wmed__c> wmeds;   
    public List<r_reeg_wmed__c> getWMeds()
    {
         if (wmeds == null)
         {
            wmeds = new List<r_reeg_wmed__c>();  

            for(r_reeg_wmed__c wmedRow : 
                 [select id, name, wo_md_comments__c, daily_dose_long__c, date_stopped__c, days_for_withdrawal__c, days_stopped__c, is_wo_extended__c, 
                    med_name__c, proj_end_date__c, r_reeg_id__c, drugreview__c, unit__c, r_wo_complete_date__c, washout_required__c
                   from r_reeg_wmed__c
                   where r_reeg_id__c = :reeg.id])
            {                                                    
                wmeds.Add( wmedRow);
            } 
     
        }    
        return wmeds;
    }

        //---TEST METHODS ---------------------------------
    public static testMethod void testController()
    {
    	r_reeg__c testReeg = rEEGDao.getTestReeg();
    	PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('id',testReeg.id);
    	
        rEEGPrintControllerExt cont = new rEEGPrintControllerExt();
        r_reeg__c reeg = cont.getReeg();
        r_patient__c patTest = cont.getPatient();
        String StatusStr = cont.getPatientFullName();
        StatusStr = cont.getReqDateStr();
        List<r_reeg_wmed__c> medList = cont.getWMeds();

        System.assertEquals( 1, 1);
    }
}