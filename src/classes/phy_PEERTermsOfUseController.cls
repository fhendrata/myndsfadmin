//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: phy_PEERTermsOfUseController
// PURPOSE: Controller class for the Terms of Use page
// CREATED: 04/21/16 Ethos Solutions - www.ethos.com
// Author: Mitchell Corbin
//--------------------------------------------------------------------------------
public with sharing class phy_PEERTermsOfUseController {

	private static TOU_Document_Name_Settings__c CUSTOM_SETTINGS = TOU_Document_Name_Settings__c.getInstance('Default');

	public Boolean NotAgreedOK { get; set; }
	public Boolean TOUAgreed { get; set; }
	public Id DocID { get; set; }
	public Id UserID { get; set; }
	public PEER_Documentation__c PEERDoc { get; set; }
	public String Redirected { get; set; }
	public String ReturnURL { get; set; }
	public User CurrentUser { get; set; }
	public User_Agreement__c CurrentUserAgreement { get; set; }

	public phy_PEERTermsOfUseController(ApexPages.StandardController stdController)
	{
		Redirected = ApexPages.currentPage().getParameters().get('redirect');
		ReturnURL = ApexPages.currentPage().getParameters().get('retURL');
		getDocumentID();
		getCurrentUser();
	}

	public PageReference loadAction()
	{
		getCurrentUserAgreement();
		return null;
	}

	public void getDocumentID()
	{
		if (CUSTOM_SETTINGS != null)
		{
			List<PEER_Documentation__c> DocsList = [SELECT Id FROM PEER_Documentation__c WHERE Name = :CUSTOM_SETTINGS.TOU_Document_Name__c];
			if (!DocsList.isEmpty())
			{
				DocID = DocsList[0].Id;
				PEERDocDao dDao = new PEERDocDao();
    			PEERDoc = dDao.getById(DocID);
			}
		}
	}

	public void getCurrentUser()
	{
		UserID = UserInfo.getUserId();
		if (String.isNotBlank(UserID))
		{
			UserDao dao = new UserDao();
			CurrentUser = dao.getById(UserID);
		}
	}

	public void getCurrentUserAgreement()
	{
		if (CurrentUser != null)
		{
			UserAgreementDao dao = new UserAgreementDao();
			CurrentUserAgreement = dao.getByUser(CurrentUser);

			// upsert the CurrentUserAgreement to track if the user visits the page but does not accept the TOU, ensuring a User_Agreement__c record is created
			upsert CurrentUserAgreement;
		}
	}

	public PageReference okAction()
	{
		PageReference pr = null;
		if (TOUAgreed)
		{
			NotAgreedOK = false;
			if ((URL.getSalesforceBaseUrl().getProtocol() == 'https' || URL.getSalesforceBaseUrl().getProtocol() == 'http') && String.isBlank(Redirected))
			{
				pr = new PageReference(ReturnURL);
				pr.setRedirect(true);
			}

			CurrentUserAgreement.Has_Accepted_TOU__c = TOUAgreed;
			update CurrentUserAgreement;
		}
		else
			NotAgreedOK = true;

		return pr;
	}

	public PageReference cancelAction()
	{
		PageReference pr = new PageReference('/secur/logout.jsp');
		return pr;
	}
}