//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: CPL_PatientLeadProcessor
// PURPOSE: Functionality for updating a Lead record if any of the Lead record's 
//			Customer Portal Lead records are updated.
// CREATED: 05/05/16 Ethos Solutions - www.ethos.com
// Author: Mitchell Corbin & Joe DePetro
//--------------------------------------------------------------------------------
global class CPL_PatientLeadProcessor implements Database.Batchable<sObject>
{
	global final String Query;

	global CPL_PatientLeadProcessor(String q)
	{
		Query = q;
	}
   	
   	/* -- Code to insert initial scheduled process (Execute Anonymous in Eclipse) 
	String chronStr = '0 0 * * * ? *';
	String chronStr2 = '0 30 * * * ? *';
	CPL_PatientLeadRunner batchRunner = new CPL_PatientLeadRunner();
	System.schedule('Customer Portal Lead update Lead - Top of the hour',chronStr, batchRunner);
	System.schedule('Customer Portal Lead update Lead - Bottom of the hour',chronStr2, batchRunner);
	*/
	
	/*
	-- Code to run Lead -> RIGHT NOW --
	DateTime n = datetime.now().addSeconds(5);
	String cron = '';
	cron += n.second();
	cron += ' ' + n.minute();
	cron += ' ' + n.hour();
	cron += ' ' + n.day();
	cron += ' ' + n.month();
	cron += ' ' + '?';
	cron += ' ' + n.year();
	CPL_PatientLeadRunner batchRunner = new CPL_PatientLeadRunner();
	System.schedule('Customer Portal Lead update Lead - Immediate',cron, batchRunner);
   */
	
   	global Database.QueryLocator start(Database.BatchableContext BC)
	{
    	return Database.getQueryLocator(Query);
   	}

 	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
   		//-- If any new or updated Customer Portal Leads are found, create a new Lead record or update the associated Lead record.
   		for(Sobject s : scope)	CPL_PatientLead.ProcessLead(s);
   	}

	global void finish(Database.BatchableContext BC) 
	{
		
	}
}