//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: ReferenceDrugDao
// PURPOSE: Data access class for Reference Drug fields
// CREATED: 8/2/11 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class ReferenceDrugDao extends BaseDao
{
	private static String NAME = 'ref_drug__c';    
	private static SysLogDao logDao {get; set;}  
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.ref_drug__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public ref_drug__c getById(String idInp)
    {
		return (ref_drug__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public List<ref_drug__c> getAll()
    {
		return (List<ref_drug__c>)getSObjectListByWhere(getFieldStr(), NAME, '', 'name');
    } 

    public static ref_drug__c getTestRefDrug()
    {
    	ref_drug__c refDrug = new ref_drug__c();
        refDrug.name = 'asprin';
        refDrug.generic__c = 'asprin';
        refDrug.half_life_days__c = 3;
        refDrug.trade__c = 'Buffrin';
        insert refDrug;
        
        return refDrug;
    }

	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testReferenceDrugDao()
    {
        ReferenceDrugDao dao = new ReferenceDrugDao();
        ref_drug__c refDrugObj = ReferenceDrugDao.getTestRefDrug();
        
        refDrugObj = dao.getById(refDrugObj.Id);
        List<ref_drug__c> fullList = dao.getAll();
    }
        
}