//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: dec_ActionDao
// PURPOSE: Data access class for the decision action object
// CREATED: 05/11/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class dec_ActionDao extends CrudDao 
{
	private static String NAME = 'dec_action__c';    
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.dec_action__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public dec_action__c getById(String idInp)
    {
		return (dec_action__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public List<dec_action__c> getAll()
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, '');
    } 
    
    public List<dec_action__c> getAllActive()
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'active\' and is_report_action__c = false', 'sequence__c');
    } 
    
    public List<dec_action__c> getAllPending()
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'pending\' and is_report_action__c = false', 'sequence__c');
    } 
    
    public List<dec_action__c> getAllInactive()
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'inactive\' and is_report_action__c = false', 'sequence__c');
    } 
    
    public List<dec_action__c> getAllByCategory(string category)
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, 'report_drug_category__c = \'' + category + '\'', 'sequence__c');
    } 
    
    public List<dec_action__c> getAllReportActive()
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'active\' and is_report_action__c = true', 'sequence__c');
    } 
    
    public List<dec_action__c> getAllReportPending()
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'pending\' and is_report_action__c = true', 'sequence__c');
    } 
    
    public List<dec_action__c> getAllReportInactive()
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'inactive\' and is_report_action__c = true', 'sequence__c');
    }
    
    public List<dec_action__c> getAllReportActivePending(string category, string drug)
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, '(status__c = \'active\' OR status__c = \'pending\') and is_report_action__c = true and report_drug__c =  \'' + drug + '\' and report_drug_category__c = \'' + category + '\'', 'sequence__c');
    } 
    
    public List<dec_action__c> getAllReportActive(string category, string drug)
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'active\' and is_report_action__c = true and report_drug__c =  \'' + drug + '\' and report_drug_category__c = \'' + category + '\'', 'sequence__c');
    } 
    
    public List<dec_action__c> getAllReportPending(string category, string drug)
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'pending\' and is_report_action__c = true and report_drug__c =  \'' + drug + '\' and report_drug_category__c = \'' + category + '\'', 'sequence__c');
    } 
    
    public List<dec_action__c> getAllReportInactive(string category, string drug)
    {
		return (List<dec_action__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'inactive\' and is_report_action__c = true and report_drug__c =  \'' + drug + '\' and report_drug_category__c = \'' + category + '\'', 'sequence__c');
    } 
    
    public dec_action__c cloneObj(dec_action__c action)
    {
    	dec_action__c newAction = new dec_action__c();
    	newAction.append_text__c = action.append_text__c;
    	newAction.description__c = action.description__c;
    	newAction.logic_conditions__c = action.logic_conditions__c;
    	newAction.Name = action.Name;
    	newAction.pri_field_label__c = action.pri_field_label__c;
    	newAction.pri_field_name__c = action.pri_field_name__c;
    	newAction.pri_location__c = action.pri_location__c;
    	newAction.pri_obj_label__c = action.pri_obj_label__c;
    	newAction.pri_obj_name__c = action.pri_obj_name__c;
    	newAction.r_reeg_id__c = action.r_reeg_id__c;
    	newAction.ref_text__c = action.ref_text__c;
    	newAction.sec_field_label__c = action.sec_field_label__c;
    	newAction.sec_field_name__c = action.sec_field_name__c;
    	newAction.sec_obj_label__c = action.sec_obj_label__c;
    	newAction.sec_obj_name__c = action.sec_obj_name__c;
    	newAction.sequence__c = action.sequence__c;
    	newAction.status__c = 'Pending';
    	newAction.text__c = action.text__c;
    	newAction.calculation_object__c = action.calculation_object__c;
    	newAction.is_report_action__c = action.is_report_action__c;
    	newAction.report_drug__c = action.report_drug__c;
    	newAction.Report_Drug_Category__c = action.Report_Drug_Category__c;
    	
		return newAction;
    } 
    
  	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testActDao()
    {
        dec_ActionDao dao = new dec_ActionDao();
        
        dec_action__c actObj = dec_ActionDao.getTestAction();
        
        dec_action__c cloneActObj = dao.cloneObj(actObj);
        
        actObj = dao.getById(actObj.Id);
        List<dec_action__c> actList = dao.getAll();
      
      	actObj.status__c = 'active';
      	dao.saveSObject(actObj);
        actList = dao.getAllActive();
        
        actObj.status__c = 'pending';
      	dao.saveSObject(actObj);
        actList = dao.getAllPending();
        
        actObj.status__c = 'inactive';
      	dao.saveSObject(actObj);
        actList = dao.getAllInactive();
    }   
    
    public static dec_action__c getTestAction()
    {
    	ref_text__c textObj = ref_TextDao.getTestRefText();
    	
    	dec_action__c testAction = new dec_action__c();
    	testAction.ref_text__c = textObj.Id;
    //	testAction.custom_text__c = 'test';
    //	testAction.field_name__c = 'FirstName';
    //	testAction.field_obj_name__c = 'Contact';
    	testAction.ref_text__c = textObj.Id;
    	insert testAction;
    	
    	return testAction;
    } 

}