//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: ReqOutcomeMedDispTest
// PURPOSE: Test class for the ReqOutcomeMedDisp wrapper class
// CREATED: 01/02/15 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class ReqOutcomeMedDispTest 
{
	//------------TEST-----------------------------
    public static testMethod void testDisp()
    {
        Req_Outcome_Medication__c obj = new Req_Outcome_Medication__c();

        ReqOutcomeMedDisp disp = new ReqOutcomeMedDisp();
        String testEndDt = disp.EndDate;
        disp.Obj = obj;
        disp.Setup( obj);
        disp.setDosageDisplay();

        Req_Outcome_Medication__c obj2 = disp.Obj;

        System.assertEquals( obj, obj2);

        disp.setRowNum(1);
        System.assertEquals( disp.getRowNum(), 1);
        
        disp.setupPreviousId('test');
        string test =  disp.getPreviousId();
		disp.setDisplayAction(true);
    	boolean testB = disp.getIsDisplayActions();
    	
    	disp.StartDate = '01/01/1950';
    	string testDt = disp.StartDate;
    	disp.EndDate = '01/01/1950';
    	testDt = disp.EndDate;

        System.assertEquals(disp.isEmpty(), true);

        Req_Outcome_Medication__c med = ReqOutcomeMedDao.getTestReqOutcomeMed();
        disp = new ReqOutcomeMedDisp(obj, 'Prozac', 10.0, 'mg', 'two per day', Date.today().addYears(-30).addDays(-30), Date.today().addYears(-30), med.Id, false);

        System.assertEquals(disp.isEmpty(), false);
    }
}