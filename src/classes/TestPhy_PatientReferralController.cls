@isTest
private class TestPhy_PatientReferralController {
	
	@isTest static void testController() {
		Patient_Referral__c testPR = patientReferralDao.getInstance().getTestPatientReferral();
		ApexPages.StandardController sc = new ApexPages.StandardController(testPR);
		phy_PatientReferralController cont = new phy_PatientReferralController(sc);
		cont.patientReferral.Patient_Last_Name__c = 'NewLastName';

		Test.startTest();
		cont.save();
		Test.stopTest();

		System.assert(cont.convertToPatient().getURL().contains('prId='+testPR.Id));
		System.assertEquals([Select Patient_Last_Name__c from Patient_Referral__c where Id=:testPR.Id].Patient_Last_Name__c,'NewLastName');
		System.assert(cont.edit().getURL().contains('/phy_PatientReferralEditS2?id='+testPR.Id));
		System.assert(cont.cancel().getURL().contains('/apex/phy_PatientReferralViewS2?id='+testPR.Id));
	}
	
}