//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: peer_SummaryPrintController
// PURPOSE: Controller class for the Summary Print page
// CREATED: 07/06/16 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
public with sharing class peer_SummaryPrintController extends BaseDrugReportController
{
    public string drugName {get; set;}
    public string BrainMapUrl {get; set;}
    public boolean IsNeuroRptAvail {get; set;}
    public boolean isNeuroRptOrdered { get; set; }
    public String printUrl {get; set;}
    public String printUrlTesting {get; set;}

    public peer_SummaryPrintController()
    {
        getParams();
    }
    
    public peer_SummaryPrintController(ApexPages.StandardController stdController)
    {
        getParams();
    }
    
    private void getParams()
    {
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        if (e_StringUtil.isNullOrEmpty(reegId))
            this.reegId = ApexPages.currentPage().getParameters().get('id');
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        rDao = new rEegDao();
    }
    
    public PageReference loadAction()
    {
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            this.reeg = rDao.getById(reegId, true);
            
            if(this.reeg.req_stat__c != 'Complete')
            {
                PageReference pg = new PageReference('/apex/phy_PeerViewS2?id=' + this.reeg.Id);
                pg.setRedirect(true);
                return pg;
            }
         
            baseUrl = '/apex/peer_Report?rid=' + reeg.Id;
            baseReport2Url = '/apex/peer_Report?rid=' + reeg.Id;
            baseReportTabbedUrl = '/apex/peer_Report?rid=' + reeg.Id;
            baseDrugGoupUrl = '/apex/peer_DrugClass?rid=' + reeg.Id;
            BrainMapUrl = '/apex/PEERBrainMapView?rid=' + reeg.id;

            //baseDrugGoupUrl = '/apex/wr_PeerDrugClassS2?rid=' + reeg.Id;
            
            if (!e_StringUtil.isNullOrEmpty(pageId))
            {
                DrugTreeMenuUtil.MenuItem menuItem = menuPEERMap.get(pageId);
                if (menuItem != null)
                    drugName = menuItem.drugName;
            }
        }
        else
        {
            this.reeg = new r_reeg__c();
        }  
         
        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        isNeuroRptOrdered = (reeg != null && reeg.neuro_stat__c == 'In Progress');
        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        printUrl = getPrintViewPage();
        printUrlTesting = getTestPrintViewPage();
        return null;    
    }
    
    public PageReference returnAction()
    {
        return null;
       // return Page.peer_ReportList;
    }
    
    //---temporary
    public String getPrintViewPage()
    {
        String key = '';
        string versionTxt = '';
        if (!e_StringUtil.isNullOrEmpty(this.reegId))
        {
            String part1 = this.reegId.substring(5, 10);
            String part2 = this.reegId.substring(0, 5);
            String part3 = this.reegId.substring(10, reegId.length());
            key = '6H3a2' + part1 + 's' + part2 + part3;
        }
        
        return 'https://mypeerinteractive.com/Public/Download.aspx?key=' + key + '&v=1&p2=1';
  
    }

    public String getTestPrintViewPage()
    {
        String key = '';
        string versionTxt = '';
        if (!e_StringUtil.isNullOrEmpty(this.reegId))
        {
            String part1 = this.reegId.substring(5, 10);
            String part2 = this.reegId.substring(0, 5);
            String part3 = this.reegId.substring(10, reegId.length());
            key = '6H3a2' + part1 + 'SV' + part2 + part3;
        }
        
        return 'https://mypeerinteractive.com/Public/Download.aspx?key=' + key + '&v=1&p2=1';
  
    }
    
    public void orderNeuroReport()
    {
        reeg.do_not_send_neuro__c = false;
        isNeuroRptOrdered = true;
        rDao.Save(reeg, 'ORDER_NEURO_REPORT');
        
        
    }
  

}