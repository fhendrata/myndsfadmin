//--------------------------------------------------------------------------------
// COMPONENT: Ethos common
//     CLASS: e_FieldUtil
//   PURPOSE: Utility for getting field names for SObjects 
// 
//     OWNER: CNSR
//   CREATED: 03/25/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class e_FieldUtil 
{	
	public static String getFieldSql(Map<String, Schema.SObjectField> fMap)
    {
    	return getFieldSql( fMap, ''); 	
    }
	
	public static String getFieldSql(Map<String, Schema.SObjectField> fMap, String prefix)
    {
    	String fieldList = '';    	
    	List<Schema.SObjectField> fTokens = fMap.values();

		for( Integer i = 0 ; i < fTokens.size() ; i++ )
		{
    		Schema.DescribeFieldResult f = fTokens.get(i).getDescribe();
    		
         	if( f.isAccessible())
         	{
         		if (!e_StringUtil.isNullOrEmpty(fieldList)) fieldList += ',';
         		if (!e_StringUtil.isNullOrEmpty(prefix)) fieldList += prefix + '.';         		
         		fieldList += f.getName();	
         	}  
		}
    	
    	return fieldList;    
    } 

}