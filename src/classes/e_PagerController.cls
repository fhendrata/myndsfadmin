//--------------------------------------------------------------------------------
// COMPONENT: SSearch
//     CLASS: e_PagerController
//   PURPOSE: Controller for the Pager Component
// 
//     OWNER: CNS Response
//   CREATED: 10/27/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class e_PagerController extends BaseController
{
    public e_PagerController()
    {
        isTest = false;
        PageNumberText = '';
    }

	public boolean isTest { get; set; }
    public String PageNumberText { get; set; }

    private rEEGPager pager;
    public rEEGPager getPager()
    {
        return pager;
    }
    public void setPager(rEEGPager value)
    {
    	if (isTest)
    	{
    		if (value != null) value.setRecordCount(10);
    	}
    	
        if (value != null && value.getRecordCount() != null)
        {
            PageNumberText = '';
            Decimal numPages = Double.valueOf(String.valueOf(value.getRecordCount())) / value.getDispRows();

            if (numPages > 1)
                PageNumberText = '' + value.getCurrentPage() + ' of ' + numPages.round(system.roundingMode.CEILING);
        
            pager = value;
        }
    }
    
    private String reRender;
    public String getReRender()
    {
        return reRender;
    }
    public void setReRender(String value)
    {
        reRender = value;
    }
    
    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testPagerController()
    {
        e_PagerController obj = new e_PagerController();
		obj.isTest = true;
		obj.IsTestCase = true;
        System.assert(obj.getPager() == null);

        String testStr = 'test';
        obj.PageNumberText = testStr;
        System.assertEquals(obj.PageNumberText, testStr);

        //-- base pager is abstract - this does not work
        //s_BasePager bp = new s_BasePager();
        //obj.setPager(bp);
        //obj.setPager(null);

        obj.setReRender(testStr);
        System.assertEquals(obj.getReRender(), testStr);
    }
}