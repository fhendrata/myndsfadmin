public with sharing class TestController2 {

	public r_patient__c[] patientList { get; set; }
	
    public TestController2()
    { 
    }
	
	public PageReference loadData()
	{
		//patientList = [Select a.Name, a.first_name__c, a.dob__c From r_patient__c a where a.physician__c = '003R000000LLxUy'];
		
		patientList = new r_patient__c[10];
		
		return null;
	}

  	private static testmethod void testController()
    {
        TestController2 t = new TestController2();
      	t.loadData();
    }

}