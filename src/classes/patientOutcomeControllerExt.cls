public class patientOutcomeControllerExt extends CrudController
{
    public List<PatientOutcomeMedDisp> oMeds  { get; set; }
    public List<PatientOutcomeMedDisp> newMeds  { get; set; }
    public List<r_outcome_med__c> holdingList { get; set; } 
    public List<r_outcome_med__c> previousMeds { get; set; }
    public String billingCode { get; set; }
    public integer rowCount {get; set;}
	public integer newRowCount {get; set;}
    public Boolean NoStDtMessage { get; set; }
    public boolean medsErrorMessage { get; set; }
    public List<String> medsToUpdate { get; set; }
  
    public boolean addNewMed {get; set;}
    
    private String reegId { get; set; }
    private String patId { get; set; }
    private String qsId { get; set; }
    private String tid { get; set; }
    public string outcomeId { get; set; }
    public boolean isWiz { get; set; }
    public boolean isNew {get; set;}
    public boolean isNewPat {get; set;}
    public boolean isWR {get; set;}
    
    private patientOutcomeDao dao {get; set;}
    private patientOutcomeMedDao ocMedDao { get; set; }
    private patientIntervalsDao intDao { get; set; }
    private rEEGDao reegDao { get; set; }

	//---Return a casted version of the object
	public r_patient_outc__c getObj()
	{
		return (r_patient_outc__c) CrudObj;
	}

	//---Base Constructor (go to test mode)
	public patientOutcomeControllerExt()
	{
		IsTestCase = true;
		init(); 
	}
	
    // ---Constructor from the standard controller
    public patientOutcomeControllerExt( ApexPages.StandardController stdController)
    {
    	init();    	
        init(stdCont); 
    }
    
    //---Local init
    public void init()
    {
    	addNewMed = false;
    	IsNew = false;
    	BasePageName = 'PatientOutcome';
    	IsNewPageNameType = false;
        dao = new patientOutcomeDao();
    	CrudDao = dao;
    }

    //--- Number of outcome meds in list
    private Integer oMedCount;
    public Integer getOMedCount()
    {
        return oMedCount;
    }
    
    //---Local setup
    public override PageReference setup() 
    {		
    	NoStDtMessage = false;
    	medsErrorMessage = false;
    	
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.qsId = ApexPages.currentPage().getParameters().get('qsid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('id');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        String strIsNew = ApexPages.currentPage().getParameters().get('isNew');
        this.IsNew = (!e_StringUtil.isNullOrEmpty(strIsNew) && strIsNew == 'true');
        string newPatStr = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        
        ocMedDao = new patientOutcomeMedDao();
        intDao = new patientIntervalsDao();
        reegDao = new rEEGDao();
        
    	// assign most recent type I rEEG, set billing code, set outcome date to rEEG date
		List<r_reeg__c> reegs;
    	if (!e_StringUtil.isNullOrEmpty(this.patId))
   			reegs = reegDao.getByPatientId(this.patId);

    	if (IsNew && e_StringUtil.isNullOrEmpty(outcomeId))
    	{
    		CrudObj = new r_patient_outc__c();
    		Date oDate = date.today();
    		CrudObj.put('outcome_date__c', oDate); 
    		CrudObj.put('cgi__c', '4 - Baseline'); 
    		
    		/* 7.27.11 jdepetro - this is wrong. The reeg id should be the current reeg when stepping through the wizard
    		if (reegs != null && reegs.size() > 0) 
    		{ 
    			CrudObj.put('rEEG__c', reegs[0].Id);
    			billingCode = reegs[0].billing_code__c;
    		}
    		*/
    		if (!e_StringUtil.isNullOrEmpty(this.reegId))
    		{
    			r_reeg__c reeg = reegDao.getById(this.reegId);
    			if (reeg != null)
    			{
    				CrudObj.put('rEEG__c', reeg.Id);
    				billingCode = reeg.billing_code__c;
    			}
    		}
    		
    		CrudObj.put('patient__c', this.patId);
    		
			oMeds = getExistingOutcomeMeds(this.patId);
			
			if (oMeds == null) 
	    		oMeds = new List<PatientOutcomeMedDisp>();  
			
			CrudDao.saveSObject(CrudObj);
    	}
    	else
    	{
    		CrudObj = dao.getById(ObjId); 
    		if (CrudObj != null)
    		{
	    		string reegId = (String) CrudObj.get('rEEG__c');
	    		if (!e_StringUtil.isNullOrEmpty(reegId))
	    			billingCode = dao.getbillingCode(reegId);
	
	       		oMeds = getOutcomeMeds();
    		}
    	}    		
    	return null;
    } 
    
    public PageReference previousActionOverride()
    {
    	PageReference returnVal = new PageReference('/apex/reegWizard3Page');
        returnVal.getParameters().put('isNew', string.valueOf(isNew));
        
        if (!e_StringUtil.isNullOrEmpty(reegId))
    		returnVal.getParameters().put('id', reegId);
        if (!e_StringUtil.isNullOrEmpty(patId))
	    	returnVal.getParameters().put('pid', patId);
    	else if (patient != null && !e_StringUtil.isNullOrEmpty(patient.Id))
    		returnVal.getParameters().put('pid', patient.Id);
    	if (!e_StringUtil.isNullOrEmpty(CrudObj.Id))
    		returnVal.getParameters().put('oid', CrudObj.Id);
        if (!e_StringUtil.isNullOrEmpty(tid))
	    	returnVal.getParameters().put('tid', tid);
    	if (!e_StringUtil.isNullOrEmpty(qsid))
    		returnVal.getParameters().put('qsid', qsid);
    	if (isNewPat != null && isNewPat)
    		returnVal.getParameters().put('np', 'true');
    	if (isWiz != null && isWiz)
        	returnVal.getParameters().put('wiz', 'true');
    	if (isNew != null && isNew)
        	returnVal.getParameters().put('isNew', 'true');
        if (isWR != null && isWR)
    		returnVal.getParameters().put('wr', 'true');
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference nextActionOverride()
    {
    	saveActionOverride();
    	
    	if (false && medsErrorMessage)
    	{
    		return null;
    	}
    	else
    	{
	       	PageReference returnVal = new PageReference('/apex/reegWizard4Page');
	        returnVal.getParameters().put('isNew', string.valueOf(isNew));
	        
	 		if (!e_StringUtil.isNullOrEmpty(reegId))
	    		returnVal.getParameters().put('rid', reegId);
	        if (!e_StringUtil.isNullOrEmpty(patId))
		    	returnVal.getParameters().put('pid', patId);
	    	else if (patient != null && !e_StringUtil.isNullOrEmpty(patient.Id))
	    		returnVal.getParameters().put('pid', patient.Id);
	    	if (!e_StringUtil.isNullOrEmpty(CrudObj.Id))
	    		returnVal.getParameters().put('oid', CrudObj.Id);
	        if (!e_StringUtil.isNullOrEmpty(tid))
		    	returnVal.getParameters().put('tid', tid);
	    	if (!e_StringUtil.isNullOrEmpty(qsid))
	    		returnVal.getParameters().put('qsid', qsid);
	    	if (isNewPat != null && isNewPat)
	    		returnVal.getParameters().put('np', 'true');
	    	if (isWiz != null && isWiz)
	        	returnVal.getParameters().put('wiz', 'true');
	    	if (isNew != null && isNew)
	        	returnVal.getParameters().put('isNew', 'true');
	        if (isWR != null && isWR)
    			returnVal.getParameters().put('wr', 'true');
	        returnVal.setRedirect(true);
	        return returnVal;
    	}
    } 

    public PageReference editActionOverride()
    {
    	return new PageReference('/apex/patientOutcomeEditPage?pid=' + this.patId 
    		+ '&retUrl=/apex/PatientViewPage?id=' + this.patId);
    }

    public PageReference cancelOutcomeAction()
    {
    	if (isWiz && !e_StringUtil.isNullOrEmpty(reegId))
    	{
    		rEEGDao rDao = new rEEGDao();
    		rDao.deleteSObjectById(reegId);
    	}
    	
    	return new PageReference('/apex/PatientViewPage?id=' + this.patId);
    }
    
    //---Build the existing outcome meds list (meds from previous outcome)
    public List<PatientOutcomeMedDisp> getExistingOutcomeMeds(string pid)
    {	
         if (oMeds == null)
         { 	
             oMeds = new List<PatientOutcomeMedDisp>();  
             // List<string> previousIds = new List<string>();
             PatientOutcomeMedDisp oMed;          
             Integer ctr = -1;
             
             if (medsToUpdate == null)
				medsToUpdate = new List<String>();
             
             //---Get the current meds
             previousMeds = dao.getExistingOutcomeMeds(this.patId);
             if (previousMeds != null) 
             {
	            for(r_outcome_med__c oMedRow : previousMeds) {
		                ctr++;  
		                oMed = new PatientOutcomeMedDisp();
		                oMed.setDisplayAction(true);
		                oMed.setRowNum(ctr);
		                
		                r_outcome_med__c hldgMed = new r_outcome_med__c();
		                hldgMed.med_name__c = oMedRow.med_name__c;
		                hldgMed.dosage__c = oMedRow.dosage__c;
		                hldgMed.unit__c = oMedRow.unit__c;
		                hldgMed.frequency__c = oMedRow.frequency__c;
		                hldgMed.start_date__c = oMedRow.start_date__c;
		                hldgMed.end_date__c = oMedRow.end_date__c;
		                hldgMed.previous_med__c = oMedRow.Id;
		                hldgMed.start_date_is_estimated__c = oMedRow.start_date_is_estimated__c;
						oMed.Setup(hldgMed);
		                // oMed.Setup(oMedRow);     
		                // previousIds.Add(oMedRow.Id);                     
		                oMeds.Add(oMed);		      
		                
		                // running list of previous meds to update on save
						medsToUpdate.add(oMedRow.Id);          
	            }
	            oMedCount = ctr + 1;	//---ctr is zero based	 
	            rowCount = ctr + 1;	//---ctr is zero based	                         	
             }
             else {
             	oMeds = null;
             }
        }    
        return oMeds;
    }
    
    //---Build the current outcome's meds list
    public List<PatientOutcomeMedDisp> getOutcomeMeds()
    {
         if (oMeds == null)
         {
             oMeds = new List<PatientOutcomeMedDisp>();  
             PatientOutcomeMedDisp oMed;
             
             Integer ctr = -1;
             
             if (CrudObj != null)
             {
	             //---Add the current meds
	             for(r_outcome_med__c oMedRow : 
	                 [select med_name__c, dosage__c, unit__c, frequency__c, start_date__c, end_date__c, name, start_date_is_estimated__c
	                     from r_outcome_med__c
	                     where r_outcome_id__c = :(String) CrudObj.get('id')])
	             {
	                ctr++;
	                
	                oMed = new PatientOutcomeMedDisp();
	                oMed.setRowNum(ctr);
	                oMed.Setup(oMedRow );                                 
	                oMeds.Add(oMed);
	             }
	             oMedCount = ctr + 1;  //---ctr is zero based
             
		         if (ctr < 0) 
		         {
			       //---Create blank row
			     	r_outcome_med__c objMed = new r_outcome_med__c();
			        objMed.r_outcome_id__c = CrudObj.Id;
			        objMed.unit__c = 'mg';
			            
			        oMed = new PatientOutcomeMedDisp();
			        oMed.setRowNum(0);    
			        rowCount = 1; 
			        oMed.Setup(objMed);                           
			        oMeds.Add(oMed);  	
			   	 } 
             }       
	         rowCount = ctr + 1;    //---ctr is zero based
        }    
        return oMeds;
    }
    
	public PageReference saveActionOverride()
    {
        PageReference pr = null;
        
        if (oMeds != null) 
        {
        	NoStDtMessage = false;
            //---Loop through each row
            for(PatientOutcomeMedDisp oMedRow : oMeds) 
            {
                if (oMedRow != null) 
                {
                	r_outcome_med__c outcMed = oMedRow.getObj();
                    if (outcMed != null) 
                    {
                    	if (IsNew)
                   			outcMed.r_outcome_id__c = CrudObj.Id;
                   		oMedRow.Setup(outcMed);
                  		SaveOutcomeMed(outcMed);
                    }
                }
            }
        }
            
        debug('### newMeds:' + newMeds);
        if (newMeds != null && newMeds.size() > 0)
        {
         	for(PatientOutcomeMedDisp nMedRow : newMeds) 
            {
            	debug('### nMedRow:' + nMedRow);
	            if (nMedRow != null) 
	            {
                   	r_outcome_med__c nOutcMed = nMedRow.getObj();
                    if (nOutcMed != null) 
					{
                   		nOutcMed.r_outcome_id__c = CrudObj.Id;
                   		nMedRow.Setup(nOutcMed);
                   		debug('### nOutcMed:' + nOutcMed);
                  		SaveOutcomeMed(nOutcMed);   
                  		if (nOutcMed != null && nOutcMed.med_name__c != null && nOutcMed.med_name__c != '')
                  			oMeds.add(nMedRow);
	                }
	            }
	        }
        }
        
        if (isWiz && !e_StringUtil.isNullOrEmpty(reegId))
        {
        	r_reeg__c reeg = reegDao.getById(reegId);
        	if (reeg != null && !reeg.med_no_med__c && !e_StringUtil.isNullOrEmpty(reeg.reeg_type__c) && reeg.reeg_type__c != 'Type II')
			{
				boolean isActiveMed = false;
				if (oMeds != null && oMeds.size() > 0)
				{
					for (PatientOutcomeMedDisp row : oMeds)
					{
						r_outcome_med__c medObj = row.getObj();
						isActiveMed = (medObj != null && medObj.end_date__c != null);
						if (isActiveMed) break;
					}	
				}
				
				debug('### isActiveMed:' + isActiveMed);
				
				if (!isActiveMed)
				{
					medsErrorMessage = true;
				}
				else
				{
					reeg.reeg_type_mod__c = rEEGUtil.getMedicatedTestMod(oMeds);
					reegDao.saveSObject(reeg);
				}
			}
        }

        pr = saveAction();

        String patientId = (String) CrudObj.get('patient__c');
        
        GlobalPatientDao patDao = new GlobalPatientDao();
        r_patient__c pat = patDao.getById(patientId);
        
        if (pat != null)
        {
	        patientIntervalBuilder intBuilder = new patientIntervalBuilder();
	  		intBuilder.buildAllIntervalsForPat(pat.Id, pat.OwnerId);
        }
        
    	return pr;
    }
    
	// ---Save an outcome med row
    private void SaveOutcomeMed(r_outcome_med__c medObj)
    {
        if (medObj != null && medObj.med_name__c != null && medObj.med_name__c != '')
        {
            ocMedDao.saveSObject(medObj);
        }
        else 
        {
            //---If the id is not blank and the name is, then delete the old record
            if (medObj.id != null) ocMedDao.deleteSObject(medObj);
        }    
    }

	public PageReference addMedRow()
    { 	
    	Integer rowNum = Integer.valueOf(System.currentPageReference().getParameters().get('rowNumber'));
		// rowNum is 0 based, rowCount starts at 1
		if ((rowNum + 1) == rowCount) 
		{
			//---Create blank row
			r_outcome_med__c objMed = new r_outcome_med__c();
			objMed.r_outcome_id__c = ObjId;
	            
			PatientOutcomeMedDisp oMed = new PatientOutcomeMedDisp();
			oMed.setRowNum(rowCount); 
			rowCount++; 
			oMed.Setup(objMed);                           
			oMeds.Add(oMed);  	
		}        
    	return null; 	  
    }
    
    public PageReference addBlankMedRows()
    {
    	addNewMed = true;
    	if (newMeds == null) newMeds = new List<PatientOutcomeMedDisp>();    
    	    	
       	for (integer i = 0; i < 5; i++)
    	{
    		PatientOutcomeMedDisp medDisp = new PatientOutcomeMedDisp();
    		r_outcome_med__c newMed = new r_outcome_med__c();
    		newMed.unit__c = 'mg';	
    		medDisp.setObj(newMed);		
    		medDisp.setRowNum(newMeds.size());
    		newMeds.add(medDisp);    	
    	}
    	newRowCount = newMeds.size();
    	
    	return null;
    }  
    
    // if outcome is deleted, need to delete associate medication
    public PageReference deleteActionOverride()
    {   
        PageReference pr = null;     
    	retUrl = '/apex/PatientViewPage?id=' + this.patId;
        try 
        {					
	        if (oMeds != null) 
            {
	            //---Loop through each row
	            for(PatientOutcomeMedDisp oMedRow : oMeds) {
	                if (oMedRow != null) {
	                    if (oMedRow.getObj() == null) {
	                       // addMessage('oMedRow.getObj() is null');         
	                    }
	                    else {
							// Should not allow deleting of data, do a soft delete
            				oMedRow.getObj().put('is_deleted__c',true);
            				upsert oMedRow.getObj();                    
	                    }
	                }
	            }
	        }

            pr = deleteAction();

			if (pr != null)
			{
            	String retUrl = pr.getUrl();
            	pr = Page.AutoIntervalBuilder;
            	pr.getParameters().put('patId',this.patId); 
            	pr.getParameters().put('returl',retUrl); 
			}
        }
        catch (Exception ex) {
            handleError( 'deleteSObject', ex);
        }
        return pr;
    }
    
     // ---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
    	patientOutcomeControllerExt cont = new patientOutcomeControllerExt();
    	cont.oMedCount = 1;
    	cont.rowCount = 1;
    	cont.billingCode = '12345';
    	cont.NoStDtMessage = false;
    	cont.IsNew = true;
    	PageReference pr = cont.setup();
		pr = cont.setupAction();
		
		ApexPages.currentPage().getParameters().put('isNew', 'true');
		pr = cont.setup();
		pr = cont.setupAction();
    	
    	integer medCnt = cont.getOMedCount();
    	integer rowCnt = cont.rowCount;
    	rowCnt = cont.newRowCount;
    	
    	r_patient_outc__c CrudObj = new r_patient_outc__c();
    	r_patient_outc__c newCrudObj = cont.getObj();
    	
    	cont.init();
    	
    	r_patient__c patient = patientDao.getTestPat();
    	
    	r_reeg__c reeg = rEEGDao.getTestReeg();
    	
    	patientOutcomeMedDao outMedDao = new patientOutcomeMedDao();
    	r_outcome_med__c testMed = outMedDao.getTestOutcomeMed();
    	patientOutcomeDao outDao = new patientOutcomeDao();
    	r_patient_outc__c testOutcome = outDao.getById(testMed.r_outcome_id__c);
    	
    	ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
		
		pr = cont.setup();
		pr = cont.editActionOverride();
		pr = cont.cancelOutcomeAction();
		List<PatientOutcomeMedDisp> outMedList = cont.getExistingOutcomeMeds(reeg.r_patient_id__c);
		outMedList = cont.getOutcomeMeds();
		cont.CrudObj = new r_patient_outc__c();
 		pr = cont.saveActionOverride();
 		
 		pr = cont.setup();
 		
    	r_patient_outc__c patOutc = outDao.getTestOutcome();
    	r_patient_outc__c patOutc2 = outDao.getTestOutcome(patOutc.patient__c);
    	
    	patientDao patDao = new patientDao();
    	r_patient__c pat = patDao.getById(patOutc.patient__c);
    	
    	patOutc2 = outDao.getTestOutcome(patOutc.patient__c);
    	r_patient_outc__c obj = new r_patient_outc__c();
    	obj.patient__c = patOutc.patient__c;
    	obj.cgi__c = 'test222';
    	obj.outcome_date__c = Date.today().addDays(10);
    	
    	outDao.saveSObject(obj);
		
		patientOutcomeMedDao ocmedDao = new patientOutcomeMedDao();
		r_outcome_med__c ocMed = ocmedDao.getTestOutcomeMed(patOutc.Id);
 		outMedList = cont.getExistingOutcomeMeds(patOutc.patient__c);
 		outMedList = cont.getOutcomeMeds();
 		
		pr = cont.setup();
		pr = cont.editActionOverride();
		pr = cont.previousActionOverride();
		pr = cont.nextActionOverride();
		pr = cont.cancelOutcomeAction();
		outMedList = cont.getExistingOutcomeMeds(reeg.r_patient_id__c);
		outMedList = cont.getOutcomeMeds();
		cont.CrudObj = new r_patient_outc__c();
 		pr = cont.saveActionOverride();
 		pr = cont.deleteActionOverride();
 		pr = cont.deleteAction();

    }
 }