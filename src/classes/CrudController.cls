//--------------------------------------------------------------------------------
// COMPONENT: Base
// CLASS: CrudController
// PURPOSE: Core Data Access Controller clas
// CREATED: 03/26/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing abstract class CrudController extends BaseWizardController
{
	public ApexPages.StandardController StdCont { get; set;}
	public Boolean IsNew { get; set;}
	public String BasePageName { get; set;}
	public String RetUrl { get; set;}
	public String ViewOnSave { get; set;}
	public String ParentId { get; set;}
	public String ObjId { get; set;}
	public SObject CrudObj { get; set;}
	public CrudDao CrudDao { get; set;}
	public String Profile { get; set;}
	public String defaultReturnUrl  { get; set;} 
	
	//-- some page names end w/ 'Page' some do not. 
	//-- This flag determines the addition of 'Page' at the end of the page name
	public Boolean IsNewPageNameType { get; set;}   
	
	public void init(ApexPages.StandardController cont)
	{
		IsNew = false;
		StdCont = cont;		
	}

	//---To be called in the setupAction of the controller
	public virtual PageReference setupAction()
	{	
		ParentId = ApexPages.currentPage().getParameters().get('pid');
		RetUrl = restoreReturnUrlParams(ApexPages.currentPage().getParameters().get('retUrl'));
		ViewOnSave = ApexPages.currentPage().getParameters().get('viewOnSave');
		
		//if isNew is passed on request, override the value to force new object creation
		String strIsNew = ApexPages.currentPage().getParameters().get('isNew');
			
		if (!(strIsNew == null || strIsNew == '') && strIsNew == 'true')		
		{
			IsNew = true;
		}
		
		if (e_StringUtil.isNullOrEmpty(ObjId))
			ObjId = ApexPages.currentPage().getParameters().get('id');
		if (e_StringUtil.isNullOrEmpty(ObjId))
			IsNew = true;
				
		PageReference pRef = setup();
		
		setProfile();
				
		return pRef;
	}

	private void setProfile()
	{
		// ref_UserDao userDao = new ref_UserDao();
		// User user = userDao.getById(UserInfo.getUserId()); 
		// if (user != null) Profile = user.Profile.Name; 
	}   

	public String restoreReturnUrlParams(String retUrl)
	{
		if (retUrl != null) return retUrl.replace('|', '&');
		return null;
	}
	
	//---This must be overridden by the subclass
	public abstract PageReference setup();

	public virtual void postSave()
	{
		// this updates the contact info after the save
	}
	
	public virtual void preSave()
	{
		// this updates the contact info before the save
	}
	
	public virtual boolean validate()
	{
		// this performs any field validation and should be overridden
		// if validation is required
		return true;
	}
	
	public virtual String getDefaultReturnUrl()
	{
		// this returns a default return URL
		return defaultReturnUrl;
	}

	//-------------------------- Navigation methods -----------------------
	//-- Some page names (older, original) end w/ 'Page' Newer pages do not. 
	protected virtual PageReference getEditPRef()
	{
		String editUrl = '/apex/' + BasePageName;    
		editUrl += (IsNewPageNameType != null && IsNewPageNameType) ? 'Edit' : 'EditPage'; 
    	return new PageReference( editUrl);
	}
	
	protected virtual PageReference getViewPRef()
	{
		String viewUrl = '/apex/' + BasePageName;
		viewUrl += (IsNewPageNameType != null && IsNewPageNameType) ? 'View' : 'ViewPage';     	
    	return new PageReference(viewUrl);
	}
	
	//-------------------------- VIEW Actions-----------------------
	public virtual PageReference newAction()
    {
    	IsNew = true;
    	return getEditPRef();
    }
    
    public virtual PageReference editAction()
    {    	
    	PageReference pRef = getEditPRef();
    	pRef.getParameters().put('id', ObjId);
    	pRef.getParameters().put('retUrl', '/' + ObjId);
    	pRef.getParameters().put('pid', ParentId);
    	
    	return pRef;
    }
    
    //-------------------------- EDIT Actions-----------------------

	//---Save
    public virtual PageReference saveAction()
    {    	
    	PageReference pRef;
    	
    	if (validate())
    	{
	    	preSave();
	    	Exception ex = CrudDao.saveSObjectEx(CrudObj);
	    	
	    	if(ex != null)
	    	{
	    		ApexPages.addMessages(ex);
	    		return null;
	    	}
	    	
	    	ObjId = (String)CrudObj.get('id');
	    	postSave();
	    	IsNew = false;
	    	
	    	if (e_StringUtil.isNullOrEmpty(RetUrl) || !e_StringUtil.isNullOrEmpty(ViewOnSave))
	    	{
	    		pRef = getViewPRef();
	    		pRef.getParameters().put('id', ObjId);
	    		
	    		debug('### pRef:' + pRef);
	    	}
	    	else
	    	{
	    		pRef = new PageReference(RetUrl);
	    	}
	    	
	    	pRef.setRedirect(true);
    	}   	
    	
    	return pRef;    	
    }
    
    //---Delete  
    public virtual PageReference deleteAction()
    {
    	PageReference pRef;
    	
    	if (validate())
    	{
	    	CrudDao.deleteSObject(CrudObj);
	    	CrudObj = null;
    	}  		
    	
    	return getReturnUrl();    	
    }
    
    //---Save and New
    public virtual PageReference saveNewAction()
    {
    	PageReference pRef;
    	
    	if (validate())
    	{
	    	preSave();
	    	Exception ex = CrudDao.saveSObjectEx(CrudObj);	    	
	    	if(ex != null)
	    	{
	    		ApexPages.addMessages(ex);
	    		return null;
	    	}
	    	
	    	ObjId = (String)CrudObj.get('id');
	    	postSave();

	    	IsNew = true;
	    	pRef = getEditPRef();
	    	pRef.getParameters().put('parentId', ParentId);
	    	pRef.getParameters().put('isNew', 'true');	    	
	    	if (!e_StringUtil.isNullOrEmpty(RetUrl)) pRef.getParameters().put('retUrl', RetUrl);
	    	
    	}	    	
    	
    	return pRef;
    }
   /* 
     //---Cancel
    public override PageReference cancelWizAction()
    {
    	return cancelAction();   
    }
   */ 
    //---Cancel
    public virtual PageReference cancelAction()
    {
    	return getReturnUrl();   
    }
    
    public PageReference getReturnUrl()
    {    	
    	PageReference pRef = null;
    	
    	if (!e_StringUtil.isNullOrEmpty(RetUrl))
    	{
    		pRef = new PageReference(RetUrl);
    	} 
    	else if (CrudObj != null && !e_StringUtil.isNullOrEmpty(CrudObj.Id))
    	{
    		pRef = getViewPRef();
   			pRef.getParameters().put('id', CrudObj.Id); 
    	} 
    	else if (getDefaultReturnUrl() != null)
    	{
    		pRef = new PageReference(getDefaultReturnUrl());
    	}
    	
    	if (pRef != null) pRef.setRedirect(true);
    	
    	return pRef;    	 
    }
    
}