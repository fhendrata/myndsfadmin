//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: tec_EegTechControllerTest
// PURPOSE: Test class for the tec_EegTechController class
// CREATED: 03/10/16 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class tec_EegTechControllerTest 
{
    public static testMethod void testParams()
    {
        tec_EegTechController obj = new tec_EegTechController();
        obj.IsTestCase = true;

        obj.oMeds = new List<PatientOutcomeMedDisp>();
        obj.newMeds = new List<PatientOutcomeMedDisp>();
        obj.previousMeds = new List<r_outcome_med__c>();
        obj.pId = 'test';
        obj.rowCount = 0;
        obj.isEEGActive = true;
        obj.showReschedule = true;
        obj.showCancel = true;
        obj.isWashedOut = true;
        obj.setIsPrevious = true;
        obj.updatePrevious = true;
        obj.NoStDtMessage = true;
        obj.isMedEdit = true;
        obj.addNewMed = true;
        obj.medsVerified = true;
        obj.medConflict = true;
        obj.reeg = new r_reeg__c();
        obj.eegTech = new Contact();
        obj.bpTech = 'test';
        obj.weightTech = 'test';

        obj.patOcDao = new patientOutcomeDao();
        obj.ocMedDao = new patientOutcomeMedDao();
        obj.intDao = new patientIntervalsDao();
        obj.patDao = new GlobalPatientDao();
    }

    public static testMethod void testPageLoad()
    {
        r_reeg__c testReeg = rEEGDao.getTestReeg();
        ApexPages.currentPage().getParameters().put('rid', testReeg.Id);
        ApexPages.currentPage().getParameters().put('pid', testReeg.r_patient_id__c);

        tec_EegTechController obj = new tec_EegTechController();
        obj.IsTestCase = true;

        obj.loadAction();
    }

    public static testMethod void testHelperMethods()
    {
        r_reeg__c testReeg = rEEGDao.getTestReeg();
        ApexPages.currentPage().getParameters().put('rid', testReeg.Id);
        ApexPages.currentPage().getParameters().put('pid', testReeg.r_patient_id__c);

        tec_EegTechController obj = new tec_EegTechController();
        obj.IsTestCase = true;

        obj.loadAction();

        String testStr = obj.getEnv();
        PageReference pr = obj.reSched();
        pr = obj.reSched();
        pr = obj.cancelAppt();
        pr = obj.CancelReegPatReq();


        pr = obj.saveSchedChange();
        pr = obj.saveSchedCancel();
        pr = obj.cancelSchedChange();
        pr = obj.cancelSchedCancel();
        
    }

    public static testMethod void testEEGTech()
    {
        tec_EegTechController obj = new tec_EegTechController();
        obj.IsTestCase = true;
        
        obj.ocMedDao = new patientOutcomeMedDao();
        obj.intDao = new patientIntervalsDao();
        obj.patDao = new GlobalPatientDao();
        
        obj.ocMedDao.IsTestCase = true;
        obj.intDao.IsTestCase = true;
        obj.patDao.IsTestCase = true;
        
        r_reeg__c reeg = rEEGDao.getTestReeg();
        obj.reeg = reeg;
        r_patient__c pat = obj.patDao.getById(reeg.r_patient_id__c);
        obj.patOcDao = new patientOutcomeDao();
        obj.patOcDao.IsTestCase = true;
        r_patient_outc__c outc = obj.patOcDao.getTestOutcome(pat.Id);
        
        patientOutcomeMedDao medDao = new patientOutcomeMedDao();
        medDao.IsTestCase = true;
        
        r_outcome_med__c outcomeMed = medDao.getTestOutcomeMed(outc.Id);
        r_outcome_med__c outcomeMed2 = medDao.getTestOutcomeMed(outc.Id);
        
        obj.oMeds = new List<PatientOutcomeMedDisp>();
        obj.newMeds = new List<PatientOutcomeMedDisp>();
        obj.previousMeds = new List<r_outcome_med__c>();
    
        obj.outcomeId = outc.Id;
        obj.pId = pat.Id;
        obj.rowCount = obj.oMeds.size();
        obj.newRowCount = obj.newMeds.size();
        obj.isEEGActive = true;
        obj.showReschedule = true;
        obj.showCancel = true;
        obj.isWashedOut = true;
        
        obj.setIsPrevious = true;
        obj.medsToUpdate = new List<String>();
        obj.updatePrevious = true;
        obj.NoStDtMessage = true;
        obj.isMedEdit = true;
        obj.addNewMed = true;
        obj.medsVerified = true;
        obj.bpTech = '120/60';
        obj.weightTech = '160';
        obj.eegTech = contactDao.getTestPhysician();    
        
        ApexPages.currentPage().getParameters().put('rid', reeg.Id);
        ApexPages.currentPage().getParameters().put('pid', pat.Id);
        obj.ocMedDao.IsTestCase = true;
        obj.intDao.IsTestCase = true;
        obj.patDao.IsTestCase = true;
        
        outcomeMed = medDao.getTestOutcomeMed(outc.Id);
        outcomeMed2 = medDao.getTestOutcomeMed(outc.Id);
        obj.loadAction();
        
        System.currentPageReference().getParameters().put('rowNumber', '1');
        
        obj.submit();
        obj.getEnv();
        obj.reSched();
        obj.cancelAppt();
        obj.saveSchedChange();
        obj.saveSchedCancel();
        obj.cancelSchedChange();
        obj.cancelSchedCancel();
        obj.saveMeds();
        obj.editMeds();
        obj.addMedRow();
        obj.addBlankMedRows();
        obj.cancelMedEdit();
        obj.refreshAutoAction();
        
        obj.oMeds = null;
        obj.getExistingOutcomeMeds(pat.Id);
    }
}