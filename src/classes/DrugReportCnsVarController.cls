//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: DrugReportCnsVarController
// PURPOSE: Base controller class for the Drug Report page
// CREATED: 04/29/11 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class DrugReportCnsVarController extends BaseDrugReportController
{
	public DrugReportCnsVarController()
    {
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
    	this.pageId = ApexPages.currentPage().getParameters().get('pgid');
    	this.cnsTestId = ApexPages.currentPage().getParameters().get('ctid');
    	this.cnsTestId2 = ApexPages.currentPage().getParameters().get('ctid2');
    	this.cnsTestId3 = ApexPages.currentPage().getParameters().get('ctid3');
    	this.cnsTestId4 = ApexPages.currentPage().getParameters().get('ctid4');
    }
    
    public PageReference loadAction()
    {
		return null;	
    }
    
    public PageReference summaryAction()
    {
    	PageReference pr = Page.ReportSummary;
    	AddParameters(pr);
    	
    	return pr;
    }	
    
    public PageReference returnAction()
    {
    	PageReference pr = new PageReference('/a0h/o');
    	AddParameters(pr);
    	
    	return pr;
    }

    private static testmethod void testController()
    {
        DrugReportCnsVarController dc = new DrugReportCnsVarController();
        dc.returnAction(); 
        dc.summaryAction(); 
    
    }
	
}