//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: rptTypeSelectController
//   PURPOSE: Report type selection page controller
//     OWNER: CNSR
//   CREATED: 11/30/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class rptTypeSelectController extends BaseWizardController
{
	public boolean isSubmitVerified { get; set; }
	public boolean includeAllMeds { get; set; }
	public boolean includeAConv { get; set; }
	public boolean includeADep { get; set; }
	public boolean includeStim { get; set; }
	public boolean includeLorazepam { get; set; }
	public boolean includeClonazepam { get; set; }
	public boolean includeAlprazolam { get; set; }
	public boolean includeCarbamazepine { get; set; }
	public boolean includeLithium { get; set; }
	public boolean includeDivalproex { get; set; }
	public boolean includeGabapentin { get; set; }
	public boolean includeLamotrigine { get; set; }
	public boolean includeFluoxetine { get; set; }
	public boolean includeSertraline { get; set; }
	public boolean includeParoxetine { get; set; }
	public boolean includeDesipramine { get; set; }
	public boolean includeImipramine { get; set; }
	public boolean includeNortriptyline { get; set; }
	public boolean includeBupropion { get; set; }
	public boolean includeVenlafaxine { get; set; }
	public boolean includeTranylcypromine { get; set; }
	public boolean includeSelegiline { get; set; }
	public boolean includeMethylphenidate { get; set; }
	public boolean includeDextroamphetamine { get; set; }
	public boolean includeDlamphetamine { get; set; }
	
	
	public PageReference loadAction()
	{
		this.patId = ApexPages.currentPage().getParameters().get('pid');
		this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.qsId = ApexPages.currentPage().getParameters().get('qsid');
        string newPatStr = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        
        isSubmitVerified = false;
        initializeMeds();
        
        rDao = new rEEGDao();
        pDao = new patientDao();
        
		return null;
	}
	
	public PageReference previousAction()
    {
        PageReference returnVal = new PageReference('/apex/rEEGWizard4Page');
        returnVal.getParameters().put('isNew', string.valueOf(isNew));
        
        if (patient != null) returnVal.getParameters().put('pid', patient.id);
        if (!e_StringUtil.isNullOrEmpty(patId)) returnVal.getParameters().put('pid', patId);
        if (reeg != null) returnVal.getParameters().put('rid', reeg.id);
        if (!e_StringUtil.isNullOrEmpty(reegId)) returnVal.getParameters().put('rid', reegId);
        
        returnVal.getParameters().put('wiz', string.valueOf(isWiz));
        returnVal.getParameters().put('np', string.valueOf(isWiz));
        returnVal.getParameters().put('oid', outcomeId);
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference wizardSubmit()
    {
    	PageReference returnVal = null;
    	
        if (!e_StringUtil.isNullOrEmpty(reegId) && isSubmitVerified)
        {
        	submitQSAction();

			if (reeg == null)
    			reeg = rDao.getById(reegId); 

	        reeg.req_stat__c = 'In Progress';
	        reeg.eeg_rec_stat__c = 'In Progress';
	        rDao.Save(reeg, 'UPDATE_DB_STATUS');
	
	    	updateVars();
	    	
	    	if (e_StringUtil.isNullOrEmpty(patId))
	    	{
	    		if (patient != null) patId = patient.Id;
	    		else patId = reeg.r_patient_id__c;
	    	}
	    	
	    	returnVal = new PageReference('/' + reegId);
        }

        return returnVal;
    }
	
	public PageReference cancelAction()
	{
		return cancelWizAction();
	}
	
	private void submitQSAction()
    {
    	if (!e_StringUtil.isNullOrEmpty(qsid))
        {
	    	for(r_qs_file__c qsRow : [select id, name, ftp_dir__c, status__c, eeg_upload_date__c, eeg_processed_date__c, eeg_tech_id__c from r_qs_file__c where id = :qsid])
	       	{
	        	qsRow.r_reeg_id__c = reegId;
	            update qsRow;
	        }   
	
			ActionDao aDao = new ActionDao();
	        aDao.InsertQSAction(reegId);
        }
    }
    
    private void updateVars()
    {
		List<r_reeg__c> reegList = [select id, r_patient_id__c, stat_is_corr__c, reeg_type__c from r_reeg__c where id = :reegId];
	      	
        if (reegList != null && reegList.size() > 0)
	    {
		    if (reegList[0].reeg_type__c == 'Type II')
		    {
		        LoadPastTestVars();
		    }
	    }   
    }
 
    private void LoadPastTestVars()
    {
        if (!e_StringUtil.isNullOrEmpty(patId))
        {
        	ActionDao aDao = new ActionDao();
        	
        	List<r_reeg__c> reegList = rDao.getCorrCompleteByPatientId(patId);
        	
        	if (reegList != null && reegList.size() > 0)
        	{
        		 for (r_reeg__c reegRow : reegList)
        		 {
        		 	aDao.InsertQSAction(reegRow.id);
        		 }
        	}
        }
    }
    
    private void initializeMeds()
    {
	    includeAllMeds = true;
		includeAConv = false;
		includeADep = false;
		includeStim = false;
		includeLorazepam = false;
		includeClonazepam = false;
		includeAlprazolam = false;
		includeCarbamazepine = false;
		includeLithium = false;
		includeDivalproex = false;
		includeGabapentin = false;
		includeLamotrigine = false;
		includeFluoxetine = false;
		includeSertraline = false;
		includeParoxetine = false;
		includeDesipramine = false;
		includeImipramine = false;
		includeNortriptyline = false;
		includeBupropion = false;
		includeVenlafaxine = false;
		includeTranylcypromine = false;
		includeSelegiline = false;
		includeMethylphenidate = false;
		includeDextroamphetamine = false;
		includeDlamphetamine = false;	
    }
    
    //--------------- TEST METHOD ---------------------------------
    
    public static testmethod void testController()
    {
    	rptTypeSelectController rtc = new rptTypeSelectController();
    	
   
    	rtc.initializeMeds();
    }
	
}