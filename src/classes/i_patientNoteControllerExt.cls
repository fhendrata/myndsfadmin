public class i_patientNoteControllerExt extends CrudController
{
    public List<r_patient_note__c> noteList { get; set; } 
    public String DebugMsg { get; set; }
    public string strIsNew { get; set; }
    patientNoteDao dao = new patientNoteDao();

	//---Return a casted version of the object
	public r_patient_note__c getObj() {
		return (r_patient_note__c) CrudObj;
	}
	
	public void setObj(r_patient_note__c value) {
		CrudObj = value;
	}

	//---Base Constructor (go to test mode)
	public i_patientNoteControllerExt() 
	{
		init(); 
	}
	
    // ---Constructor from the standard controller
    public i_patientNoteControllerExt( ApexPages.StandardController stdController) 
    {
    	ParentId = ApexPages.currentPage().getParameters().get('pid');
		RetUrl = restoreReturnUrlParams(ApexPages.currentPage().getParameters().get('retUrl'));
		ViewOnSave = ApexPages.currentPage().getParameters().get('viewOnSave');
		ObjId = ApexPages.currentPage().getParameters().get('id');
		// if isNew is passed on request, override the value to force new object creation
		strIsNew = ApexPages.currentPage().getParameters().get('isNew');			
		
    	init();    	
        init(stdController); 
    }
    
    //---Local init
    public void init() 
    {
    	BasePageName = 'iwPatientNotes';
    	IsNewPageNameType = true;
    	CrudDao = dao;
    }
    
    public override PageReference setup() 
    {
    	if (!e_StringUtil.isNullOrEmpty(strIsNew))		
			IsNew = strIsNew.equals('isNew');
		if (e_StringUtil.isNullOrEmpty(ObjId))	
			IsNew = true;		
    	
    	setupPage();    	
    	return null;
    } 
    
    public void setupPage() 
    {
    	if (IsNew) 
    	{    		
    		CrudObj = new r_patient_note__c(); 		
    		String patId = ParentId;
    		
    		if (patId != null && patId != '') 
    		{
    			CrudObj.put('r_patient__c', patId);
    			DebugMsg = 'putting pid on patient:' + patId;
    		}
    	}
    	else 
    	{
    		CrudObj = dao.getPatientNote(ObjId); 
    	}         		
    } 
    
    public PageReference cancelNoteAction()
    {
    	PageReference pr = cancelAction();
    	pr.getParameters().put('pid', ParentId);
    	pr.setRedirect(true);        
        return  pr;	
    }
    
    public PageReference editActionOverride()
    {
    	PageReference pr = editAction(); 
    	pr.getParameters().put('retUrl', '/apex/iwPatientNotesView?id=' + ObjId);
    	pr.getParameters().put('pid', ParentId);
    	pr.setRedirect(true);        
        return  pr;	
    }
    
    public PageReference saveActionOverride() 
    {
    	PageReference pr = saveAction();
    	pr.getParameters().put('pid', ParentId);
    	pr.setRedirect(true);        
        return  pr;	
    }

    // if outcome is deleted, need to delete associate medication
    public PageReference deleteActionOverride()
    {        
        try {			
			CrudObj.put('is_deleted__c', true);
			upsert CrudObj;
			retUrl = '/apex/iwPatientDetail?show=all&id=' + ParentId;
        }
        catch (Exception ex) {
            handleError( 'deleteSObject', ex);
        }
        return deleteAction();
    }
    
    //-- jdepetro - Is this even used?
    public PageReference ieditAction()  
    {   
    	PageReference pRef = new PageReference( '/apex/iwPatientNoteEdit?id=' + ObjId + '/apex/iwPatientDetail?show=all&id=' + ParentId);
    	return pRef;
    }
    
    public String getPatientFirstName() 
    {
    	r_patient_note__c patNote = (r_patient_note__c) CrudObj;  		
        String returnVal = '';
        if (patNote != null) {
        	if (patNote.r_patient__c != null) {        	
            	r_patient__c patient = [Select Id, first_name__c From r_patient__c where Id =: patNote.r_patient__c];
            	if (patient != null && patient.first_name__c != null && patient.first_name__c != 'null' && patient.first_name__c != '') {
                	returnVal = patient.first_name__c;
            	}
        	}
        }      
        return returnVal;
    }    
    
    public String getPatientLastName() 
    {
    	r_patient_note__c patNote = (r_patient_note__c) CrudObj;  	
        String returnVal = '';
        if (patNote != null) {
         	if (patNote.r_patient__c != null) {
           		r_patient__c patient = [Select Id, name From r_patient__c where Id =: patNote.r_patient__c];        	
           		if (patient != null && patient.name != null && patient.name != 'null' && patient.name != '') {
               		returnVal = patient.name;
           		}
        	}
        }       
        return returnVal;
    }
    
    // ---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
        i_patientNoteControllerExt cont = new i_patientNoteControllerExt();
		cont.IsTestCase = true;
		cont.noteList = new List<r_patient_note__c>();  
		cont.DebugMsg = 'test';
		
		cont.strIsNew = 'IsNew';
		
		PageReference pr = cont.setup();
		System.assertNotEquals(cont.getObj(), null);
	
		r_patient_note__c obj = patientNoteDao.getTestPatNote();
		cont.setObj(obj);
		obj = cont.getObj();

		cont.ParentId = obj.r_patient__c;
		cont.ObjId = obj.Id;
		
		cont.RetUrl = '/apex/iwPatientNoteEdit?id=' + obj.Id;
		cont.ViewOnSave = 'view';
		
		cont.strIsNew = null;
		pr = cont.setup();
		System.assertNotEquals(cont.getObj(), null);
		pr = cont.saveActionOverride();
		string name = cont.getPatientFirstName();
        name = cont.getPatientLastName();  
		
        pr = cont.cancelNoteAction();
        pr = cont.editActionOverride();
        pr = cont.deleteActionOverride();
        pr = cont.ieditAction();
    }
}