//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: LPR_PatReferralLeadTest
// PURPOSE: Test class for LPR_PatReferralLeadTest class
// CREATED: 06/06/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class LPR_PatReferralLeadTest 
{	
	@isTest static void testPatRefLeadUpdate()
	{
		Lead testLead = LeadDao.getInstance().getTestLead();
		Patient_Referral__c patRef = patientReferralDao.getInstance().getTestPatientReferral();
		patRef.Lead__c = testLead.Id;
		upsert patRef;

		LPR_PatReferralLead.UpdateLead((SObject) patRef);

		Lead testLead2 = LeadDao.getInstance().getById(testLead.Id);
		System.assertEquals(testLead2.LastName, patRef.Patient_Last_Name__c);
	}

	
}