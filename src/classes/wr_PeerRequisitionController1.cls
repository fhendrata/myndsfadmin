public with sharing class wr_PeerRequisitionController1 extends wr_BaseWizardController{
	
    private patientDao pDao;
    private rEegDao rDao;
    private patientOutcomeMedDao ocMedDao;
    private patientIntervalsDao intDao;
	
	public List<PatientOutcomeMedDisp> oMeds  { get; set; }
    public List<PatientOutcomeMedDisp> newMeds  { get; set; }
    public List<r_outcome_med__c> holdingList { get; set; } 
    public List<r_outcome_med__c> previousMeds {get; set; }
    public integer rowCount {get; set;}
    public integer newRowCount {get; set;}
    public Boolean NoStDtMessage { get; set; }
    public boolean medsErrorMessage { get; set; }
    public List<String> medsToUpdate { get; set; }
    public boolean addNewMed {get; set;}
    public String isWashedOut {get; set;}
    public Boolean washoutErr {get; set;}
    
    public DateTime earliestDate { get; set; }
    public DateTime desiredDate { get; set; }

    public Boolean showOutcomeDataEntry {get; set;}

    public Boolean IsWR {
    	
    	get {
    		return false;
    	}
    	
    }
    
    public Boolean SkipValidation
    {
    	get {
    		try {
    			
    			User u = [select Id, Can_Skip_Med_Validation__c from User where Id =: UserInfo.getUserId()];
    			return u.Can_Skip_Med_Validation__c;
    			
    			return false;
    			
    		}catch(Exception e){}
    		return false;
    	}
    }

	public wr_PeerRequisitionController1()
    {
    	oMeds = new List<PatientOutcomeMedDisp>();
    	ocMedDao = new patientOutcomeMedDao();
    	rDao = new rEegDao();
        pDao = new patientDao();
        
        patId = ApexPages.currentPage().getParameters().get('id');
        reegId = ApexPages.currentPage().getParameters().get('rid');
        
        patient = pDao.getById(patId);
        reeg = rDao.getById(reegId); 
        if(outcome == null) outcome = new r_patient_outc__c();

        reeg.req_date__c = DateTime.Now();
        populateDefaultTech(); 

    	if(rDao.hasPrevTest(patient.Id)) showOutcomeDataEntry = true;
        else showOutcomeDataEntry = false;

        loadPreviousMeds(patient.Id);
        addBlankMedRows();

    }
    
    public PageReference loadAction()
    {
    	return null;
        if(reeg.eeg_rec_stat__c == 'In Progress') return gotoPatientWithMesg('You have already submitted a requisition, do not hit the back button');
        else return null;
    }
    
    public void loadPreviousMeds(string pid)
    {
    	System.debug('Trying to load previous meds for patient ' + pid);
    	
    	Integer oMedCount = 0;
        if (newMeds == null) newMeds = new List<PatientOutcomeMedDisp>();  
        
        if (oMeds == null) oMeds = new List<PatientOutcomeMedDisp>();  
        
        if (medsToUpdate == null) medsToUpdate = new List<String>();
        PatientOutcomeMedDisp oMed;          
        Integer ctr = -1;
        previousMeds = ocMedDao.getExistingOutcomeMeds(this.patId);
             if (previousMeds != null) 
             {
             	System.debug('found previous meds, trying to setup for display');
             	System.debug('size: ' + previousMeds.size());
             	
                for(r_outcome_med__c oMedRow : previousMeds) {
                        ctr++;  
                        oMed = new PatientOutcomeMedDisp();
                        oMed.setDisplayAction(true);
                        oMed.setRowNum(ctr);
                        
                        r_outcome_med__c hldgMed = new r_outcome_med__c();
                        hldgMed.med_name__c = oMedRow.med_name__c;
                        
                        System.debug('Found new med: ' + hldgMed.med_name__c);
                        hldgMed.dosage__c = oMedRow.dosage__c;
                        hldgMed.unit__c = oMedRow.unit__c;
                        hldgMed.frequency__c = oMedRow.frequency__c;
                        hldgMed.start_date__c = oMedRow.start_date__c;
                        hldgMed.end_date__c = oMedRow.end_date__c;
                        hldgMed.previous_med__c = oMedRow.Id;
                        hldgMed.start_date_is_estimated__c = oMedRow.start_date_is_estimated__c;
                        oMed.Setup(hldgMed);                     
                        oMeds.Add(oMed); 
                        newMeds.Add(oMed);             
                        
                        // running list of previous meds to update on save
                        medsToUpdate.add(oMedRow.Id);          
                }
                oMedCount = ctr + 1;    //---ctr is zero based   
                rowCount = ctr + 1; //---ctr is zero based                              
             }
             else
             {
             	System.debug('no previous meds found');
             }
    }

    public void populateDefaultTech()
    {
    	try 
        {
	        if(reeg.r_eeg_tech_contact__c == null || reeg.r_eeg_tech_contact__c == '')
	        {
	            ContactDao cDao = new ContactDao();            

	                String cId = [select ContactId from User where Id =: UserInfo.getUserId()].ContactId;
	                Contact phy = cDao.getById(cId);
	                reeg.r_eeg_tech_contact__c = phy.default_eeg_tech__c;
	        }
        }catch(Exception e){}
    }
    
    public PageReference addMedRow()
    {   
        Integer rowNum = Integer.valueOf(System.currentPageReference().getParameters().get('rowNumber'));
        // rowNum is 0 based, rowCount starts at 1
        if ((rowNum + 1) == rowCount) 
        {
            //---Create blank row
            r_outcome_med__c objMed = new r_outcome_med__c();
            
            //TODO- attach these to the outcome that we're creating. 
            //objMed.r_outcome_id__c = ObjId;
                
            PatientOutcomeMedDisp oMed = new PatientOutcomeMedDisp();
            oMed.setRowNum(rowCount); 
            rowCount++; 
            oMed.Setup(objMed);                           
            oMeds.Add(oMed);    
        }        
        return null;      
    }
    
    public PageReference addBlankMedRows()
    {
        addNewMed = true;
        if (newMeds == null) newMeds = new List<PatientOutcomeMedDisp>();    
                
        for (integer i = 0; i < 5; i++)
        {
            PatientOutcomeMedDisp medDisp = new PatientOutcomeMedDisp();
            r_outcome_med__c newMed = new r_outcome_med__c();
            newMed.unit__c = 'mg';  
            medDisp.setObj(newMed);     
            medDisp.setRowNum(newMeds.size());
            newMeds.add(medDisp);       
        }
        newRowCount = newMeds.size();
        
        return null;
    }
    
    private void SaveOutcomeMed(r_outcome_med__c medObj)
    {
        if (medObj != null && medObj.med_name__c != null && medObj.med_name__c != '')
        {
            ocMedDao.saveSObject(medObj);
        }
        else 
        {
            //---If the id is not blank and the name is, then delete the old record
            if (medObj.id != null) ocMedDao.deleteSObject(medObj);
        }    
    }
    
    public PageReference displayWashoutError()
    {
    	ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, 'Will the patient be washed out of all medications?');
        ApexPages.addMessage( msg);
        washoutErr = true;
        return null;
    }
    
    private boolean isTypeOne()
    {
        return 	reeg.reeg_type__c == 'Type I';
    }
    
    public PageReference SubmitRequest()
    {
    	if(e_StringUtil.isNullOrEmpty(isWashedOut) && isTypeOne()) return displayWashoutError();
    	saveReegData();
    	
    	//---Use either the forward or backward calculation value, blank out the backward afterwards so it won't show up on the next request
    	if (patient.anticipated_eeg_test__c == null)
		{
			if (patient.desired_eeg_test__c != null)
			{
				patient.anticipated_eeg_test__c = patient.desired_eeg_test__c;
				patient.desired_eeg_test__c = null;
			}
			else
			{
				patient.anticipated_eeg_test__c = datetime.now();
			}			
		} 
    	
    	update patient;
    	
    	System.debug('Patient anticipated test date is: ' + patient.anticipated_eeg_test__c);
    	
    	return gotoPatient();
    }
    
    public PageReference UploadEEGPage()
    {
    	if(e_StringUtil.isNullOrEmpty(isWashedOut) && isTypeOne()) return displayWashoutError();
    	saveReegData();
    	return gotoEEGUploadPage();
    }
    
    private void saveReegData()
    {
        reeg.req_stat__c = 'New';
        reeg.eeg_rec_stat__c = 'In Progress';
        reeg.use_ng_process__c = true;
        reeg.neuroguide_status__c = 'NEW';
        reeg.rpt_stat__c = 'NG Only';
        reeg.neuro_stat__c = 'NEW';
        reeg.Process_Used_For_Test__c = 'Military Study';
       // reeg.art_status__c = 'N/A';
        reeg.med_no_med__c = (isTypeOne() && !e_StringUtil.isNullOrEmpty(isWashedOut) && (isWashedOut.toUpperCase() == 'YES'));
        reeg.is_ng_only__c = true;

	    rDao.Save(reeg, 'NEW_REEG');
        
        if(outcome.Id == null) createOutcome();
        saveOutcomeMeds();
    }
     
    public void createOutcome()
    {
    	outcome.outcome_date__c = Date.today();
        outcome.patient__c = patient.Id;
        
        //patient hasn't had a previous completed test, therefore 
        //this outcome is the initial baseline outcome
        if(!showOutcomeDataEntry) outcome.cgi__c = '4 - Baseline';
        
        insert outcome;
    }
    
    public List<SelectOption> getYesNo() 
    {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('YES','Yes')); 
        options.add(new SelectOption('NO','No')); 
        return options; 
    } 
    
    public void saveOutcomeMeds()
    {
        if (newMeds != null && newMeds.size() > 0)
        {
            for(PatientOutcomeMedDisp nMedRow : newMeds) 
            {
                if (nMedRow != null) 
                {
                    r_outcome_med__c nOutcMed = nMedRow.getObj();
                    if (nOutcMed != null) 
                    {
                        nOutcMed.r_outcome_id__c = outcome.Id;
                        nMedRow.Setup(nOutcMed);
                        SaveOutcomeMed(nOutcMed);   
                        if (nOutcMed != null && nOutcMed.med_name__c != null && nOutcMed.med_name__c != '')
                            oMeds.add(nMedRow);
                    }
                }
            }
        }

        buildIntervals();  
        update outcome;   
    }
    
    public void buildIntervals()
    {
        patientIntervalBuilder intBuilder = new patientIntervalBuilder();
        intBuilder.buildAllIntervalsForPat(patient.Id, patient.OwnerId);
    }

    //---------------------- TEST METHODS -------------------------
    
    private static testmethod void testController()
    {
    	   patientOutcomeDao outDao = new patientOutcomeDao();
           patientOutcomeMedDao outMedDao = new patientOutcomeMedDao();
           r_patient__c testPatient = patientDao.getTestPat();
           r_reeg__c testReeg = rEEGDao.getTestReeg();
           r_outcome_med__c testMed = outMedDao.getTestOutcomeMed();
           r_patient_outc__c testOutcome = outDao.getById(testMed.r_outcome_id__c);
          
           
           ApexPages.currentPage().getParameters().put('rid',testReeg.Id);
           ApexPages.currentPage().getParameters().put('id',testPatient.Id);
           
           wr_PeerRequisitionController1 prc = new wr_PeerRequisitionController1();
           prc.getYesNo();
           prc.addBlankMedRows();
       
           prc.buildIntervals();
           prc.displayWashoutError();
           prc.SubmitRequest();
    	
    
    }

}