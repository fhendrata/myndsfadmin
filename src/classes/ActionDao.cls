//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: ActionDao
// PURPOSE: Data access class for Action fields
// CREATED: 10/25/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public without sharing class ActionDao extends BaseDao
{
	private static String NAME = 'r_action__c';    
	private static SysLogDao logDao {get; set;}  
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_action__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public r_action__c getById(String idInp)
    {
		return (r_action__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 

	public void AddReloadVarsAction(string reegId)
    {
    	insertReegAction(reegId, 'CNS_VAR_RELOAD');
    }
    
    public void InsertQSAction(String reegId)
    {
    	insertReegAction(reegId, 'QUICKSTART_PROCESS');
    }
    
    public static Boolean isLegacyUser()
    {
    	try
    	{
	    	String userId = UserInfo.getUserId();
	    	User u = [select Id,Enable_Neuroguide__c from User where Id =: userId];
	    	if(u.Enable_Neuroguide__c) return false;
    	
    	}catch(Exception e){}
    	
    	return true;
    }
    
    public static void insertReegAction(string reegId, string actionType)
    {
    	insertReegAction(reegId,actionType,'');
    }
    
    public static void insertReegActionPEER(string reegId, string actionType)
    {
    	insertReegActionPEER(reegId,actionType,'');
    }
    
    public static void insertNewReegAction(string reegId)
    {   
        insertReegAction(reegId,'NEW_REEG','');
    }
    
    public static void deleteReegAction(string reegId)
    {
        insertReegActionNF(reegId,'DELETE_REEG','');
    }

    public static void insertReegActionNF(string reegId, string actionType)
    {
        insertReegActionNF(reegId,actionType,'');
    }
    
    public static void insertNewReegActionNF(string reegId)
    {   
        insertReegActionNF(reegId,'NEW_REEG','');
    }
  
    public static void deleteReegActionNF(string reegId)
    {
        insertReegActionNF(reegId,'DELETE_REEG','');
    } 
    
   @future (callout=true)
    public static void insertReegAction(string reegId, string actionType, string userId)
    {	
    	try 
        {   
            string response = '';
            
            //-- DEPREC 7.8.14.jdepetro - The Pike service has not been active for some time. During the server changeover this is deprecated
            //PikeReegWs.rEEGWsSoap pikeWs = new PikeReegWs.rEEGWsSoap();
            //pikeWs.timeout_x = 60000;
            //response = pikeWs.statusAction('1769e702-d04e-421a-a86c-fba0e5abc148', reegId, userId, actionType); 
            
            //-- NOTE - this is disabled for dev
            EverestReegWs.rEEGWsSoap eWs = new EverestReegWs.rEEGWsSoap();
            eWs.timeout_x = 60000;  
            response = eWs.statusAction('1769e702-d04e-421a-a86c-fba0e5abc148', reegId, userId, actionType); 
        }   
        catch (Exception ex)
        {
            system.debug('### insertReegAction EX:' + ex.getMessage());
        }
    }
    
    //-- 7.8.14.jdepetro - this method was used to only send to the server for Everest. Since we have deprecated the Pike service this is just a pass-thru
    @future (callout=true)
    public static void insertReegActionPEER(string reegId, string actionType, string userId)
    {
        try 
        {   
            string response = '';
            //-- NOTE - this is disabled for dev
            EverestReegWs.rEEGWsSoap eWs = new EverestReegWs.rEEGWsSoap();
            eWs.timeout_x = 60000;  
            response = eWs.statusAction('1769e702-d04e-421a-a86c-fba0e5abc148', reegId, userId, actionType); 
        }   
        catch (Exception ex)
        {
            system.debug('### insertReegAction EX:' + ex.getMessage());
        }
    }
    
    public static void insertReegActionNF(string reegId, string actionType, string userId)
    {	
        try 
        {   
            string wsKey = '';
            string wsNgKey = '';
            app_keys__c appKey = app_keys__c.getInstance();
            string response = '';

            if (appKey != null)
            {
            	wsKey = appKey.web_service_key__c;
            	wsNgKey = appKey.ng_web_service_key__c;
            }
            else
            {
            	wsKey = '1769e702-d04e-421a-a86c-fba0e5abc148';
            	wsNgKey = '1769e702-d04e-421a-a86c-fba0e5abc148';
            }
       
            EverestReegWs.rEEGWsSoap eWs = new EverestReegWs.rEEGWsSoap();
            eWs.timeout_x = 60000;  
            
            //-- NOTE - this is disabled for dev
            response = eWs.statusAction(wsNgKey, reegId, userId, actionType); 
            
        }   
        catch (Exception ex)
        {
        	system.debug('### insertReegAction EX:' + ex.getMessage());
        }
    }
     
    public static r_action__c getTestQsScanAction()
    {
    	r_reeg__c reeg = rEEGDao.getTestReeg();
    	r_action__c action = new r_action__c();
        action.type__c = 'QUICKSTART_SCAN';
        action.status__c = 'NEW';
        action.reeg_id__c = reeg.Id;
        insert action;
        
        return action;
    }

    public static testMethod void testActionDao()
    {
        ActionDao dao = new ActionDao();
        ActionDao.getFieldStr();
        r_action__c actObj = ActionDao.getTestAction();
        
        r_reeg__c reeg = rEEGDao.getTestReeg();
        
        dao.InsertQSAction(reeg.Id);
        
        ActionDao.insertNewReegAction(reeg.Id);
        ActionDao.insertReegAction(reeg.Id, 'DELETE_REEG');
        
        ActionDao.insertReegActionPEER(reeg.Id, 'NEW_REEG'); 
        
        ActionDao.insertNewReegActionNF(reeg.Id);
        ActionDao.insertReegActionNF(reeg.Id, 'DELETE_REEG', UserInfo.getUserId());
        
        ActionDao.deleteReegAction(reeg.Id);
        ActionDao.deleteReegActionNF(reeg.Id);
        
        actObj = dao.getById(actObj.Id);
    }
        
	public static r_action__c getTestAction()
    {
    	r_action__c act = new r_action__c();
        act.type__c = 'QUICKSTART_PROCESS';
        act.status__c = 'NEW';
        act.reeg_id__c = 'test';
        insert act;
    	
    	return act;
    }
  
}