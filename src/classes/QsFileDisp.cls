public class QsFileDisp
{
    private String name;
    public String getName()
    {
        return name;
    }
    public void setName(String val)
    {
        name = val;
    }

    private r_qs_file__c obj;
    public r_qs_file__c getObj()
    {
        return obj;
    }
    public void setObj(r_qs_file__c s)
    {
        obj = s;
    }

    public void Setup(r_qs_file__c o)
    {
        obj = o;
    }

    public Boolean getShowFTPFail()
    {
        return obj.status__c == 'Upload Fail';
    }

    public Boolean getNotShowFTPFail()
    {
        return !getShowFTPFail();
    }

    //------------TEST-----------------------------
    public static testMethod void testDisp()
    {
        r_qs_file__c obj = new r_qs_file__c();

        QsFileDisp disp = new QsFileDisp();
        disp.setObj(obj);
        disp.Setup( obj);

        r_qs_file__c obj2 = disp.getObj();

        System.assertEquals( obj, obj2);

        disp.setName('x');
        System.assertEquals( disp.getName(), 'x');
    }
}