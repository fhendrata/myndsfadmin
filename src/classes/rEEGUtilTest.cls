//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: rEEGUtilTest
// PURPOSE: Test class for the rEEGUtil class methods
// CREATED: 10/13/16 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class rEEGUtilTest 
{
	/**
    * Test until methods
    */
    private static testmethod void testrEEGMethods() 
    {
    	String strVal = rEEGUtil.getEnvString();
		strVal = rEEGUtil.getMCnsVarsEnvString();
		strVal = rEEGUtil.getOnlineReportingUrl();
		strVal = rEEGUtil.getOldReportUrl();
		strVal = rEEGUtil.getWebUrl();
		strVal = rEEGUtil.getDownloadUrl();
		strVal = rEEGUtil.getSitesLoginPage();
		strVal = rEEGUtil.getEnvString();

		r_reeg__c testReeg = rEEGDao.getTestReeg();
        testReeg.req_stat__c = 'Complete';
        testReeg.neuroguide_status__c = 'Report Complete';
        testReeg.Process_Used_For_Test__c = 'Classic';

        System.assert(rEEGUtil.isClassicReport(testReeg));
        System.assert(!rEEGUtil.isPO1Report(testReeg));
        System.assert(!rEEGUtil.isWRMilitaryStudyReport(testReeg));
        System.assert(!rEEGUtil.isPO2Report(testReeg));
        System.assert(!rEEGUtil.isStudyReport(testReeg));

        testReeg.Process_Used_For_Test__c = 'NeuroGuide 1';

        System.assert(!rEEGUtil.isClassicReport(testReeg));
        System.assert(rEEGUtil.isPO1Report(testReeg));
        System.assert(!rEEGUtil.isWRMilitaryStudyReport(testReeg));
        System.assert(!rEEGUtil.isPO2Report(testReeg));
        System.assert(!rEEGUtil.isStudyReport(testReeg));

        testReeg.Process_Used_For_Test__c = 'NeuroGuide 2';

        System.assert(!rEEGUtil.isClassicReport(testReeg));
        System.assert(!rEEGUtil.isPO1Report(testReeg));
        System.assert(!rEEGUtil.isWRMilitaryStudyReport(testReeg));
        System.assert(rEEGUtil.isPO2Report(testReeg));
        System.assert(!rEEGUtil.isStudyReport(testReeg));

        testReeg.Process_Used_For_Test__c = 'Military Study';

        System.assert(!rEEGUtil.isClassicReport(testReeg));
        System.assert(!rEEGUtil.isPO1Report(testReeg));
        System.assert(rEEGUtil.isWRMilitaryStudyReport(testReeg));
        System.assert(!rEEGUtil.isPO2Report(testReeg));
        System.assert(!rEEGUtil.isStudyReport(testReeg));

        testReeg.Process_Used_For_Test__c = 'Canadian Study';

        System.assert(!rEEGUtil.isClassicReport(testReeg));
        System.assert(!rEEGUtil.isPO1Report(testReeg));
        System.assert(!rEEGUtil.isWRMilitaryStudyReport(testReeg));
        System.assert(!rEEGUtil.isPO2Report(testReeg));
        System.assert(rEEGUtil.isStudyReport(testReeg));
   
    }

    private static testmethod void testPEERReqMethods() 
    {
    	PEER_Requisition__c req = PEERRequisitionDao.getTestObj();
        req.reeg_reeg_type__c = 'Type 1';
        req.Process_Used_For_Test__c = 'Classic';

        System.assert(rEEGUtil.isClassicReport(req));
        System.assert(!rEEGUtil.isPO1Report(req));
        System.assert(!rEEGUtil.isWRMilitaryStudyReport(req));
        System.assert(!rEEGUtil.isPO2Report(req));
        System.assert(!rEEGUtil.isStudyReport(req));

        req.Process_Used_For_Test__c = 'NeuroGuide 1';

        System.assert(!rEEGUtil.isClassicReport(req));
        System.assert(rEEGUtil.isPO1Report(req));
        System.assert(!rEEGUtil.isWRMilitaryStudyReport(req));
        System.assert(!rEEGUtil.isPO2Report(req));
        System.assert(!rEEGUtil.isStudyReport(req));

        req.Process_Used_For_Test__c = 'NeuroGuide 2';

        System.assert(!rEEGUtil.isClassicReport(req));
        System.assert(!rEEGUtil.isPO1Report(req));
        System.assert(!rEEGUtil.isWRMilitaryStudyReport(req));
        System.assert(rEEGUtil.isPO2Report(req));
        System.assert(!rEEGUtil.isStudyReport(req));

        req.Process_Used_For_Test__c = 'Military Study';

        System.assert(!rEEGUtil.isClassicReport(req));
        System.assert(!rEEGUtil.isPO1Report(req));
        System.assert(rEEGUtil.isWRMilitaryStudyReport(req));
        System.assert(!rEEGUtil.isPO2Report(req));
        System.assert(!rEEGUtil.isStudyReport(req));

        req.Process_Used_For_Test__c = 'Canadian Study';

        System.assert(!rEEGUtil.isClassicReport(req));
        System.assert(!rEEGUtil.isPO1Report(req));
        System.assert(!rEEGUtil.isWRMilitaryStudyReport(req));
        System.assert(!rEEGUtil.isPO2Report(req));
        System.assert(rEEGUtil.isStudyReport(req));
   
    }

    private static testmethod void testrEEGUtilMethods() 
    {
    	r_reeg__c testReeg = rEEGDao.getTestReeg();

    	String strVal = rEEGUtil.getViewUrl(testReeg.Id, '5');
		strVal = rEEGUtil.getDownloadUrl(testReeg.Id, '5');
		strVal = rEEGUtil.getBrainMapUrl();

		strVal = rEEGUtil.getrEEGPrefix();
		strVal = rEEGUtil.getPatientPrefix();
		Boolean boolVal = rEEGUtil.showOnlineReportNoClassic(testReeg);

	}
}