global class e_BatchUtil implements Database.Batchable<sObject>
{
   	global final String Query;
   	global final String Entity;
   	global final String Field;
   	global final String Value;
   	global final integer BatchVersion;

   	global e_BatchUtil(String q, String e, String f, String v, integer bv)
   	{
   		Query=q; Entity=e; Field=f;	Value=v; BatchVersion=bv;
   	}
	
   	global Database.QueryLocator start(Database.BatchableContext BC)
	{
    	return Database.getQueryLocator(Query);
   	}

 	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
   		if (!e_StringUtil.IsNullOrEmpty(Field))
   		{
	      	for(Sobject s : scope)
	      	{
		      	s.put(Field,Value); 
	      	}
    	} 
    	else if (BatchVersion > 0)
   		{
   			patientIntervalBuilder intBuilder = new patientIntervalBuilder();
	      	for(Sobject s : scope)
	      	{
	      		r_patient__c pat = (r_patient__c)s;
	      		string patOwnerId = '';
	      		if (pat.Owner.IsActive)
	      		{
	      			patOwnerId = pat.OwnerId;
	      		}
	      		intBuilder.buildAllIntervalsForPat(s.Id, patOwnerId);
	      		s.put('interval_batch__c', BatchVersion);
	      	}
    	} 
      
      	update scope;
   	}

   	global void finish(Database.BatchableContext BC)
   	{
   	}
}