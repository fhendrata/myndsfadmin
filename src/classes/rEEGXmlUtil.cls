//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: rEEGXmlUtil
// PURPOSE: Utility class for parsing xml strings.
// CREATED: 06/10/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class rEEGXmlUtil 
{
	public class ActionObj 
	{
    	public string name {get; set;}
     	public string value {get; set;}
     	public string logic {get; set;}
     	public string seq {get; set;}
     	public RuleObj[] ruleArray {get; set;}
     	public ErrorObj[] errorArray {get; set;}
     	public boolean hasErrors {get; set;}
   	}
	
	public class RuleObj 
	{
		public string name {get; set;}
     	public string ruleNum {get; set;}
     	public string value {get; set;}
     	public string fieldVal {get; set;}
   	}
   	
   	public class ErrorObj 
	{
		public string category {get; set;}
     	public string description {get; set;}
     	public string value {get; set;}
   	}
   	
	public static ActionObj[] parseActions(string xmlStr) 
	{
		XmlStreamReader reader = new XmlStreamReader(xmlStr);
		
     	ActionObj[] actionArray = new ActionObj[10];
     	
     	while(reader.hasNext()) 
     	{
     		if (reader.getEventType() == XmlTag.START_ELEMENT)
     		{
	   	    	if ('act' == reader.getLocalName()) 
	   	    	{
	   	    		reader.next();
	                ActionObj actObj = parseAction(reader);
	                system.debug('!!! actObj:' + actObj);
	               	actionArray.add(actObj);
	           	}
     		}           	
        	reader.next();
     	}   
     	    	  	
    	return actionArray;
   	}

   	private static ActionObj parseAction(XmlStreamReader reader) 
   	{
     	ActionObj act = new ActionObj();
	     
     	while(reader.hasNext()) 
     	{
        	if (reader.getEventType() == XmlTag.END_ELEMENT)
        	{
        		break;
         	}
        	else if ('n' == reader.getLocalName()) 
    		{
    			reader.next();
    	    	if (reader.getEventType() == XmlTag.CHARACTERS) 
    	    	{
    	    		act.name = reader.getText();
    	    	}
   	    		reader.next();
    		} 
        	else if ('v' == reader.getLocalName())
        	{
        		reader.next();
        		if (reader.getEventType() == XmlTag.CHARACTERS) 
        		{
    	  	  		act.value = reader.getText();
        		}
    	    	reader.next();	
        	}
        	else if ('f' == reader.getLocalName())
        	{
        		reader.next();
        		if (reader.getEventType() == XmlTag.CHARACTERS) 
        		{
    	  	  		act.logic = reader.getText();
        		}
    	    	reader.next();	
        	}
        	else if ('s' == reader.getLocalName())
        	{
        		reader.next();
        		if (reader.getEventType() == XmlTag.CHARACTERS) 
        		{
    	  	  		act.seq = reader.getText();
        		}
    	    	reader.next();	
        	}
        	else if ('rules' == reader.getLocalName())
        	{
       			reader.next();
        		act.ruleArray = parseRules(reader);
        	}
        	else if ('errors' == reader.getLocalName())
        	{
       			reader.next();
        		act.errorArray = parseErrors(reader);
        	}

       		reader.next();
     	}
     	
     	act.hasErrors = (act.errorArray != null && act.errorArray.size() > 0);
     	
     	return act;
   	}
   	
   	private static RuleObj[] parseRules(XmlStreamReader reader) 
	{
     	RuleObj[] ruleArray = new RuleObj[0];
     	
     	while(reader.hasNext()) 
     	{
     		if (reader.isEndElement() && 'rules' == reader.getLocalName())
     		{
     			break;
     		}
     		else if (reader.getEventType() == XmlTag.START_ELEMENT)
     		{
	  	    	if ('rule' == reader.getLocalName()) 
	   	    	{
	                RuleObj rObj = parseRule(reader);
	               	ruleArray.add(rObj);
	           	}
     		}

       		reader.next();
     	}
     	
    	return ruleArray;
   }
   
   private static RuleObj parseRule(XmlStreamReader reader) 
   {
	     RuleObj r = new RuleObj();
	     while(reader.hasNext()) 
	     {
	        if ('n' == reader.getLocalName()) 
	    	{
	    		reader.next();
	    	    if (reader.getEventType() == XmlTag.CHARACTERS) 
	    	    {
	    	    	r.name = reader.getText();
	    	    }
	    	    reader.next();	
	    	} 
	        else if ('v' == reader.getLocalName())
	        {
	        	reader.next();
	        	if (reader.getEventType() == XmlTag.CHARACTERS) 
				{
	    	    	r.value = reader.getText();
				}
				reader.next();
	        }
	        else if ('u' == reader.getLocalName())
	        {
	        	reader.next();
	        	if (reader.getEventType() == XmlTag.CHARACTERS) 
				{
	    	    	r.ruleNum = reader.getText();
				}
				reader.next();
	        }
	        else if ('fv' == reader.getLocalName())
	        {
	        	reader.next();
	        	if (reader.getEventType() == XmlTag.CHARACTERS) 
				{
	    	    	r.fieldVal = reader.getText();
				}
				reader.next();
	        }
	        
			if (reader.isEndElement() && 'rule' == reader.getLocalName()) 
	        {
	        	break; 
	        }

       		reader.next();
	     }
	     return r;
   	}
   	
   	public static ErrorObj[] parseErrors(XmlStreamReader reader) 
	{
     	ErrorObj[] errorArray = new ErrorObj[0];
     	
     	while(reader.hasNext()) 
     	{
     		if (reader.isEndElement() && 'errors' == reader.getLocalName())
     		{
     			break;
     		}
     		else if (reader.getEventType() == XmlTag.START_ELEMENT)
     		{
     			if ('err' == reader.getLocalName()) 
	   	    	{
	                ErrorObj eObj = parseError(reader);
	               	errorArray.add(eObj);
	           	}
     		}
        	reader.next();
     	}
     	
    	return errorArray;
   	}
   	
   private static ErrorObj parseError(XmlStreamReader reader) 
   {
	     ErrorObj r = new ErrorObj();
	     while(reader.hasNext()) 
	     {
	        if ('cat' == reader.getLocalName()) 
	    	{
	    		reader.next();
	    	    if (reader.getEventType() == XmlTag.CHARACTERS) 
	    	    {
	    	    	r.category = reader.getText();
	    	    }
	    	    reader.next();	
	    	} 
	        else if ('desc' == reader.getLocalName())
	        {
	        	reader.next();
	        	if (reader.getEventType() == XmlTag.CHARACTERS) 
				{
	    	    	r.description = reader.getText();
				}
				reader.next();
	        }
	        else if ('val' == reader.getLocalName())
	        {
	        	reader.next();
	        	if (reader.getEventType() == XmlTag.CHARACTERS) 
				{
	    	    	r.value = reader.getText();
				}
				reader.next();
	        }

			if (reader.isEndElement() && 'err' == reader.getLocalName()) 
	        {
	        	break; 
	        }

        	reader.next();
	     }
	     
	     return r;
   	}
   
	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testrEEGXmlUtil()
    {
    	rEEGXmlUtil xmlUtil = new rEEGXmlUtil();
    	string xmlStr = '<dec><result>True</result><act><n>Bridged leads</n><v></v><rules><rule><n>2</n><v>True</v></rule></rules></act><act><n>test 1</n><v>True</v><rules><rule><n>18</n><v>True</v></rule><rule><n>20</n><v>False</v></rule><rule><n>19</n><v>False</v></rule></rules></act></dec>';
    	
    	
    	rEEGXmlUtil.ActionObj actObj = new rEEGXmlUtil.ActionObj();
    	rEEGXmlUtil.RuleObj ruleObj = new rEEGXmlUtil.RuleObj();
    	rEEGXmlUtil.ErrorObj errObj = new rEEGXmlUtil.ErrorObj();
    	
    	ruleObj.name = 'test';
    	ruleObj.ruleNum = '1';
    	ruleObj.value = 'test';
    	ruleObj.fieldVal = 'test';
    	
    	errObj.category = '1';
    	errObj.value = 'test';
    	errObj.description = 'test';
    	
    	actObj.name = 'test';
    	actObj.value = 'test';
    	actObj.logic = 'test';
    	actObj.ruleArray = new rEEGXmlUtil.RuleObj[] {} ;
    	actObj.errorArray = new rEEGXmlUtil.ErrorObj[] {};
    	actObj.hasErrors = true;
    	
    	rEEGXmlUtil.parseActions(xmlStr);
    	
    	xmlStr = '<dec><act><errors><err><cat>1</cat><desc>{1}</desc><val>error test</val></err></errors></act></dec>';
    	rEEGXmlUtil.parseActions(xmlStr);
    	
  //  	rEEGXmlUtil.ActionObj[] actionArr = rEEGXmlUtil.
    	
    }

}