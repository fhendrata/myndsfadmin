public class QsController 
{  
    private String selectedId;
    public void setSelectedId(String id)
    {
        selectedId = id;
    }

    public PageReference ftpScan()
    {
    	ActionDao.insertReegAction('none', 'QUICKSTART_SCAN');
    	/*
        r_action__c action = new r_action__c();
        action.type__c = 'QUICKSTART_SCAN';
        action.status__c = 'NEW';
        insert action;
        */

        qss = null;

        PageReference returnVal = new PageReference( '/apex/QuickstartPage');
        returnVal.setRedirect(true);
        return returnVal;
    }

    public PageReference refresh()
    {
        qss = null;

        PageReference returnVal = new PageReference( '/apex/QuickstartPage');
        returnVal.setRedirect(true);
        return returnVal;
    }

    public Boolean getIsFTPFail()
    {
        return true;
    }

    Public PageReference ResendFile()
    {
        String qsFTPId = System.currentPageReference().getParameters().get('qsFTPId');
        String reegId;

        r_qs_file__c qsFile = new r_qs_file__c();  

        for(r_qs_file__c qsFileRow : 
            [select id, name, ftp_dir__c, status__c, eeg_upload_date__c, eeg_processed_date__c, r_eeg_tech__c, eeg_tech_id__c, r_reeg_id__c   
                from r_qs_file__c
                where id = :qsFTPId])
        {
            qsFile = qsFileRow;
        }
        if (qsFile != null)
        {
            reegId = qsFile.r_reeg_id__c;
            qsFile.status__c = 'Resend';
            update qsFile;
        }

        if (reegId != 'null')
        {
            ActionDao.insertReegAction(reegId, 'FTP_RESEND');
    		/*
            r_action__c act = new r_action__c();
            act.type__c = 'FTP_RESEND';
            act.status__c = 'NEW';
            act.reeg_id__c = reegId;
            insert act;
            */
        }

        return refresh();
    }

    private PageReference getThisPage()
    {
        return new PageReference('/apex/QuickStartPage');
    }

    public PageReference processQsFile()
    {
        String id = System.currentPageReference().getParameters().get('qsId');
        PageReference qsFilePage = new PageReference('/apex/patientSearchPage?qsid=' + selectedId);

        return qsFilePage;
    }

    public PageReference RemoveFileFromList()
    {
        String qsId = System.currentPageReference().getParameters().get('qsId');
        r_qs_file__c qsFile = new r_qs_file__c();  

        for(r_qs_file__c qsFileRow : 
            [select id, name, ftp_dir__c, status__c, eeg_upload_date__c, eeg_processed_date__c, eeg_tech_id__c, r_eeg_tech__c
                from r_qs_file__c
                where id = :qsId])
        {
            qsFile = qsFileRow;
        }
        if (qsFile != null)
        {
            qsFile.status__c = 'Ignored';
            update qsFile;
        }

        return refresh();
    }

	private List<QsFileDisp> qss;
    public List<QsFileDisp> getQss()
    {
         if (qss == null)
         {
             qss = new List<QsFileDisp>();  
             QsFileDisp qs;

             for(r_qs_file__c qsFileRow : 
                 [select id, name, ftp_dir__c, status__c, eeg_upload_date__c, eeg_processed_date__c, r_eeg_tech__c, eeg_tech_id__c, r_reeg_id__c  
                    from r_qs_file__c
                    where (status__c = 'New' OR status__c = 'Upload Fail') and (QS_Run_Type__c = null OR QS_Run_Type__c != 'TMS')
                    order by eeg_upload_date__c desc])
             {
                qs = new QsFileDisp();
                qs.Setup(qsFileRow);                                 
                qss.Add(qs);
             }
         }
        return qss;
    }
    
       //---Build the eeg list
    private List<QsFileDisp> tmsList;
    public List<QsFileDisp> getTmsList()
    {
         if (tmsList == null)
         {
             tmsList = new List<QsFileDisp>();  
             QsFileDisp qs;

             for(r_qs_file__c qsFileRow : 
                 [select id, name, ftp_dir__c, status__c, eeg_upload_date__c, eeg_processed_date__c, r_eeg_tech__c, eeg_tech_id__c, r_reeg_id__c  
                    from r_qs_file__c
                    where (status__c = 'New' OR status__c = 'Upload Fail') and QS_Run_Type__c = 'TMS'
                    order by eeg_upload_date__c desc])
             {
                qs = new QsFileDisp();
                qs.Setup(qsFileRow);                                 
                tmsList.Add(qs);
             }
         }
        return tmsList;
    }

    private List<r_action__c> actions;
    public List<r_action__c> getActions()
    {
         if (actions == null)
         {
             actions = new List<r_action__c>();  
             r_action__c action;

             for(r_action__c actionFileRow : 
                 [select id, name, status__c, type__c, CreatedDate
                    from r_action__c
                    where type__c = 'QUICKSTART_SCAN' OR type__c = 'QUICKSTART_PROCESS'
                    order by CreatedDate desc
                    Limit 10])
             {
                actions.Add(actionFileRow);
             }
         }
        return actions;
    }
    
    public r_qs_file__c getTestQS()
    {
    	r_reeg__c reeg = rEEGDao.getTestReeg();
    	r_qs_file__c qsFile = new r_qs_file__c();
    	qsFile.name = 'test';
    	qsFile.status__c = 'NEW';
    	qsFile.eeg_upload_date__c = Date.today();
    	qsFile.r_reeg_id__c = reeg.Id;
    	
    	insert qsFile;
    	
    	return qsFile;
    }

          //---TEST METHODS ------------------------------
    public static testMethod void testpatWizController()
    {
        QsController cont = new QsController();
        cont.setSelectedId('test');
        cont.refresh();
        boolean testBool = cont.getIsFTPFail();
        
        r_qs_file__c qs = cont.getTestQS();
        System.currentPageReference().getParameters().put('qsFTPId', qs.Id);
        
        System.currentPageReference().getParameters().put('qsId', qs.Id);
        cont.RemoveFileFromList();
        
        r_action__c action = ActionDao.getTestQsScanAction();
        cont.getActions();
        
        PageReference pRef1 = cont.ftpScan();
        PageReference pRef2 = cont.processQsFile();
    
        List<QsFileDisp> qsFileList = cont.getQss();

        System.assertEquals( 1, 1);
    }

}