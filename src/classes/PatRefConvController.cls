public with sharing class PatRefConvController 
{
    public Patient_Referral__c patRef {get; set;}
	public r_patient__c pat {get; set;}
    
    public PatRefConvController()
    {
    	
    }

    //---Constructor from the standard controller
    public PatRefConvController( ApexPages.StandardController stdController)
    {    
        patRef = (Patient_Referral__c)stdController.getRecord();
    }
    
    private void loadPatRefObj()
    {
    	patRef = [select Id, Name, Patient_Last_Name__c, Patient_First_Name__c, Referred_To__c from Patient_Referral__c where Id = :patRef.Id];
    }
    
    private void loadPatient()
    {
    	pat = new r_patient__c();
    	pat.Name = patRef.Patient_Last_Name__c;
    	pat.first_name__c = patRef.Patient_First_Name__c;
    	pat.Marketing_Source__c = '';
    	pat.Patient_Referral__c = patRef.Id;
    	pat.physician__c = patRef.Referred_To__c;
    	string referralId = getReferralId(patRef.Referred_To__c);
    	if (!e_StringUtil.isNullOrEmpty(referralId)) pat.OwnerId = referralId;
    }
    
    private string getReferralId(string contactId)
    {
    	string returnVal;
    	
    	for (Contact c : [select Id, Name, physician_portal_user__c from Contact where Id = :contactId Limit 1]) {
      		returnVal = c.physician_portal_user__c; 
    	}
		
    	return returnVal;
    }
    
    public PageReference loadAction()
    {
    	loadPatRefObj();
    	
    	if (patRef != null) loadPatient();
    	
        return null;
    }
    
    public PageReference ConvertPatRef()
    {
    	PageReference pr = null;
    	
    	if (pat != null)
    	{
    		try
    		{
	    		if (e_StringUtil.isNullOrEmpty(pat.Id))
	    			insert pat;
	    		else
	    			update pat;
	    		
	    		pr = new PageReference('/' + pat.Id);
    		}
    		catch (Exception ex)
    		{
    		}	
    	}
    
        return pr;
    }
    
    public PageReference cancelEdit()
    {
        return new PageReference('/' + patRef.Id);
    }

	//-----------------TEST-----------------------------------
    public static testMethod void testController()
    {
    	Patient_Referral__c patRef = patientReferralDao.getInstance().getTestPatientReferral();
    	
        PatRefConvController cont = new PatRefConvController();
        cont.pat = new r_patient__c();
        cont.patRef = patRef;
		cont.loadAction();
		cont.ConvertPatRef();
		cont.cancelEdit();

        System.assertEquals( '1', '1');
    }

}