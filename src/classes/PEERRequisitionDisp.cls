public class PEERRequisitionDisp
{
	public PEER_Requisition__c Obj { get; set; }
	public PEERRequisitionDisp() {}

	public PEERRequisitionDisp(PEER_Requisition__c o) 
	{
		Obj = o;
	}
}