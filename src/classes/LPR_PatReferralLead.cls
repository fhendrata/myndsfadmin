//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: LPR_PatReferralLead
// PURPOSE: Functionality for updating a Lead record if any of the Lead records 
//			patient referrals records are updated.
// CREATED: 06/06/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
public class LPR_PatReferralLead 
{
	public LPR_PatReferralLead() { }

	public static void UpdateLead(Sobject obj)
	{
		Patient_Referral__c patRef = (Patient_Referral__c) obj;

		if (patRef != null && !string.isBlank(patRef.Lead__c))
		{
			LeadDao lDao = new LeadDao();
			Lead lead = LeadDao.getInstance().getById(patRef.Lead__c);
			lead.LastName = patRef.Patient_Last_Name__c;
			lead.FirstName = patRef.Patient_First_Name__c;
			lead.Street = patRef.Address_1__c;
			lead.City = patRef.City__c;
			lead.State = patRef.State__c;
			lead.PostalCode = patRef.Postal_Code__c;
			lead.Phone = patRef.Best_Phone_Number__c;
			lead.Email = patRef.Email_Address__c;

			lDao.saveSObject(lead);
		}

		patRef.Is_Updated__c = false;

		try
		{
			update patRef;
		}
		catch (Exception ex) { system.debug('Clear patient Referral update flag error on save: ' + ex.getMessage()); }
	}
}