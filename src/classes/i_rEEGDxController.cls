//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: i_rEEGDxController
// PURPOSE: Controller class for the dx selection page for iPad
// CREATED: 11/19/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class i_rEEGDxController extends BaseWizardController
{
	public rEEGDxDao dxDao { get; set; }
	public boolean isSubmitVerified { get; set; }
	public string test { get; set; }
    
    private List<rEEGDxDisp> dxs;
    private List<rEEGDxDisp> dxObjs;
    
    private List<rEEGDxGroupDisp> dxGroups;
    private List<rEEGDxGroupDisp> dxGroupsL;
    private List<rEEGDxGroupDisp> dxGroupsR;

	public PageReference setup()
	{
		this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.qsId = ApexPages.currentPage().getParameters().get('qsid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        string newStr = ApexPages.currentPage().getParameters().get('isNew');
        this.isNew = (!e_StringUtil.isNullOrEmpty(newStr) && newStr == 'true');
        string newPatStr = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        this.isiPad = true;
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        
        rDao = new rEEGDao();
        pDao = new patientDao();
        dxDao = new rEEGDxDao();
        outDao = new patientOutcomeDao();
        
        isSubmitVerified = false;
		
		return null;
	}

    public PageReference save()
    {
    	if (rDao == null) rDao = new rEEGDao();
        reeg = rDao.getById(reegId); 
        if (reeg != null)
        {
	        reeg.stat_dx_gen__c = saveDx();
	        rDao.saveSObject(reeg);
        }
       	return getDestPage();
    }
    
    public PageReference cancel()
    {
        return getDestPage();
    } 

    private PageReference getDestPage()
    {
        PageReference returnVal = new PageReference( '/' + reegId);
        returnVal.setRedirect(true);
        return returnVal;
    }

    //------- wizard action buttons -------------------

	public PageReference previousAction()
    {
        PageReference returnVal = new PageReference('/apex/iwPatientOutcomeEdit');
        returnVal.getParameters().put('isNew', string.valueOf(isNew));
        
        if (patient != null) returnVal.getParameters().put('pid', patient.id);
        if (!e_StringUtil.isNullOrEmpty(patId)) returnVal.getParameters().put('pid', patId);
        if (reeg != null) returnVal.getParameters().put('rid', reeg.id);
        if (!e_StringUtil.isNullOrEmpty(reegId)) returnVal.getParameters().put('rid', reegId);
        
        returnVal.getParameters().put('wiz', string.valueOf(isWiz));
        returnVal.getParameters().put('np', string.valueOf(isNewPat));
        returnVal.getParameters().put('oid', outcomeId);
        if (isWR != null && isWR)
        		returnVal.getParameters().put('wr', 'true');
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference nextAction()
    {
    	PageReference returnVal = null;
    	if (validateFields())
    	{
    		if (reeg == null && !e_StringUtil.isNullOrEmpty(reegId))
    			reeg = rDao.getById(reegId); 
    		
    		if (reeg != null)
    		{
    			reeg.req_stat__c = 'In Progress';
	        	reeg.eeg_rec_stat__c = 'In Progress';
	        	reeg.stat_dx_gen__c = saveDx();
	        	rDao.saveSObject(reeg);
	        	
	        	updateVars();
    		}
        
	        returnVal = new PageReference('/apex/iwReportTypeSelect');
	        returnVal.getParameters().put('isNew', string.valueOf(isNew));
	        
	        if (patient != null) returnVal.getParameters().put('pid', patient.id);
	        if (!e_StringUtil.isNullOrEmpty(patId)) returnVal.getParameters().put('pid', patId);
	        if (reeg != null) returnVal.getParameters().put('rid', reeg.id);
	        if (!e_StringUtil.isNullOrEmpty(reegId)) returnVal.getParameters().put('rid', reegId);
	        
	        returnVal.getParameters().put('wiz', string.valueOf(isWiz));
	        returnVal.getParameters().put('np', string.valueOf(isNewPat));
	        returnVal.getParameters().put('id', outcomeId);
	        if (isWR != null && isWR)
        		returnVal.getParameters().put('wr', 'true');
	        returnVal.setRedirect(true);
    	}
        
        return returnVal;
    }

   public PageReference wizardSubmit()
    {
    	PageReference returnVal = null;
    	
        if (!e_StringUtil.isNullOrEmpty(reegId) && isSubmitVerified)
        {
        	string contactId = 'cID';
        	
        	if (reeg == null)
    			reeg = rDao.getById(reegId); 
        	
	        reeg.req_stat__c = 'In Progress';
	        reeg.eeg_rec_stat__c = 'In Progress';
	        reeg.stat_dx_gen__c = saveDx();
	        
	        if (e_StringUtil.isNullOrEmpty(patId))
	    	{
	    		if (patient != null)
	    			patId = patient.Id;
	    		else 
	    			patId = reeg.r_patient_id__c;
	    	}
	    	
	    	reeg.submit_key__c = reeg.r_patient_id__r.OwnerId + '_' + patId + '_' + reeg.Id + '_' + DateTime.now();
	        
        	if (!e_StringUtil.isNullOrEmpty(qsid) && !qsid.equalsIgnoreCase('null'))
        	{
        		submitQSAction();
        		rDao.Save(reeg, '');
        	}
        	else
        	{
        		rDao.Save(reeg, 'UPDATE_DB_STATUS');
        	}
        	
	    	updateVars();
	    	
	    	returnVal = new PageReference('/apex/iwPatientDetail?id=' + patId + '&show=all');
        }

        return returnVal;
    }
    
    public PageReference cancelAction()
    {
    	return cancelWizAction();
    }
    
    private boolean validateFields()
    {
    	boolean returnVal = false;
    	
    	return true;
    }
    
    private void submitQSAction()
    {
		QsDao qDao = new QsDao();
	    qDao.setReegId(qsid, reegId);   

	    ActionDao.insertReegAction(reegId, 'QUICKSTART_PROCESS');
    }
    
    private void updateVars()
    {
		List<r_reeg__c> reegList = [select id, r_patient_id__c, stat_is_corr__c, reeg_type__c from r_reeg__c where id = :reegId];
	      	
        if (reegList != null && reegList.size() > 0)
	    {
		    if (reegList[0].reeg_type__c == 'Type II')
		    {
		        LoadPastTestVars();
		    }
	    }   
    }
 
    private void LoadPastTestVars()
    {
        if (!e_StringUtil.isNullOrEmpty(patId))
        {
        	ActionDao aDao = new ActionDao();
        	
        	List<r_reeg__c> reegList = rDao.getCorrCompleteByPatientId(patId);
        	
        	if (reegList != null && reegList.size() > 0)
        	{
        		 for (r_reeg__c reegRow : reegList)
        		 {
        		 	aDao.InsertQSAction(reegRow.id);
        		 }
        	}
        }
    }

    //-----------------------------------------------
    
    public List<rEEGDxDisp> getDxs()
    {
        if (dxs== null)
        {
            loadFullList();
            loadSavedList();                
        }    
        return dxs;
    }
    
    public List<rEEGDxGroupDisp> getDxGroupsL()
    {
        getDxGroups();
        return dxGroupsL;
    }
    public List<rEEGDxGroupDisp> getDxGroupsR()
    {
        getDxGroups();
        return dxGroupsR;
    }
    
    public List<rEEGDxGroupDisp> getDxGroups()
    {
        if (dxGroups== null)
        {
            getDxs();
            loadGroupList();
        }    
        return dxGroups;
    }
    
    private void loadFullList()
    {  
        dxs = new List<rEEGDxDisp>();
        
        //-- jdepetro - I could not see creating a dao class, we don't get this data that often and there are limits on code size
        List<ref_dx__c> refDxList = [select description__c, is_group__c, primary__c, rule_out__c, secondary__c, sequence__c, id, name 
                     		from ref_dx__c
                     		order by sequence__c];
    
    	if (refDxList != null && refDxList.size() > 0)
    	{
    		for(ref_dx__c refRow : refDxList)
    		{
    			  rEEGDxDisp dx = new rEEGDxDisp();
            dx.SetupRef( refRow );     
            dx.Setup(new r_reeg_dx__c());                                        
            dxs.Add( dx);
    		}
    	}
    }
    
    private void loadSavedList()
    {
    	List<r_reeg_dx__c> reegDxList = dxDao.getByrEEGId(reegId);
    	
    	if (reegDxList != null && reegDxList.size() > 0)
    	{
    		for(r_reeg_dx__c dxRow : reegDxList)
    		{
    			 AddSavedToList(dxRow);
    		}
    	}
    }
    
    private void AddSavedToList(r_reeg_dx__c o)
    {
        for(rEEGDxDisp dxRow : dxs)
        {
            if (dxRow.getRefObj() != null && dxRow.getRefObj().id == o.r_ref_dx_id__c)
            {
                dxRow.Setup(o);
            }
        }
    }
    
    private void loadGroupList()
    {
        dxGroups = new List<rEEGDxGroupDisp>();
        
        rEEGDxGroupDisp currGroup = null;
        Integer lastGroupId = 0;
        Integer totalItemCount = 0;
                
        for(rEEGDxDisp dxRow : dxs)
        {
            if (dxRow.getRefObj().is_group__c)
            {
                currGroup = new rEEGDxGroupDisp();
                currGroup.Setup( dxRow.getObj());
                currGroup.SetupRef( dxRow.getRefObj());
                
                dxGroups.Add(currGroup);
            }
            else
            {
                if (currGroup != null) 
                {
                    totalItemCount++;
                    currGroup.addItem( dxRow);
                }
            }
        }
               
        dxGroupsL = new List<rEEGDxGroupDisp>();
        dxGroupsR = new List<rEEGDxGroupDisp>();
 
        Integer addedItemCount = 0;
        
        for(rEEGDxGroupDisp groupRow : dxGroups)
        {
            if (addedItemCount > (totalItemCount/2))
            {
                dxGroupsR.Add(groupRow);    
            }
            else
            {
                dxGroupsL.Add(groupRow);
                 if (groupRow != null && groupRow.getItemCount() != null)
                	addedItemCount += groupRow.getItemCount();
                else
                	addedItemCount = (addedItemCount == null) ? 0 : addedItemCount;
            }
        }       
    }  
    
    public Boolean saveDx()
    {          
        saveDxGroup( dxGroupsL );
        saveDxGroup( dxGroupsR );

        return true;
    } 

    private void saveDxGroup(List<rEEGDxGroupDisp> groupList)
    {
        if (groupList!= null)
        {
            for(rEEGDxGroupDisp grpRow : groupList)
            {
                for(rEEGDxDisp dxRow : grpRow.getDxs())
                {   
                    if (dxRow == null)
                    {
                        addMessage( 'DxRow is null');         
                    }
                    else if (dxRow.getObj() == null)
                    {
                        addMessage( 'DxRow.getObj() is null');         
                    }
                    else
                    {
                        SaveDx(dxRow.getObj(), dxRow.getRefObj());
                    }
                }
            }
        }
    }
    
    public void SaveDx( r_reeg_dx__c obj, ref_dx__c refObj)
    {
        Boolean hasValue = (obj.primary__c || obj.secondary__c || obj.rule_out__c);

        if (hasValue)
        {
        	obj.r_reeg_id__c = reegId;
            obj.r_ref_dx_id__c = refObj.id; 
            obj.dx_name__c = refObj.description__c; 
            obj.name = refObj.name; 
        	dxDao.saveSObject(obj);
        }
        else
        {
            if (obj.id != null) dxDao.deleteSObject(obj);
        }    
    }
    
    private void addMessage(String value)
    {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.INFO, value);
        ApexPages.addMessage( msg);
    }

          //---TEST METHODS ------------------------------
    public static testMethod void testDxController()
    {
    	r_reeg__c reeg = rEEGDao.getTestReeg();
        patientDao patDao = new patientDao();
        patDao.IsTestCase = true;
        
        r_patient__c patient = patDao.getById(reeg.r_patient_id__c);
        
        patientOutcomeDao patOutcomeDao = new patientOutcomeDao();
        r_patient_outc__c outcome = patOutcomeDao.getTestOutcome(reeg.r_patient_id__c);
        patientOutcomeMedDao medDao = new patientOutcomeMedDao();
        medDao.IsTestCase = true;
        r_outcome_med__c outMed =  medDao.getTestOutcomeMed(outcome.Id);    
        
        //ApexPages.currentPage().getParameters().put('qsid');
 		ApexPages.currentPage().getParameters().put('oid', outcome.Id);
 		ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
 		ApexPages.currentPage().getParameters().put('wiz', 'true');
 		ApexPages.currentPage().getParameters().put('isNew', 'true');
		ApexPages.currentPage().getParameters().put('np', 'true');
		ApexPages.currentPage().getParameters().put('isNew', 'true');
		ApexPages.currentPage().getParameters().put('ln', 'last');
		ApexPages.currentPage().getParameters().put('fn', 'first');
		ApexPages.currentPage().getParameters().put('dob', '01/01/1955');
    	
    	PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('id',reeg.id);
    	
    	rEEGDxDao dxDao = new rEEGDxDao();
    	r_reeg_dx__c dx = dxDao.getTestDx();
    	dx = dxDao.getTestDx();
    	
        i_rEEGDxController cont = new i_rEEGDxController();
		cont.reegId = reeg.Id;
		PageReference pRef0 = cont.setup();
        PageReference pRef1 = cont.save();
        PageReference pRef2 = cont.cancel();
        PageReference pRef3 = cont.getDestPage();
   
        List<rEEGDxDisp> dxList = cont.getDxs();
        List<rEEGDxGroupDisp> DxGroupDisp = cont.getDxGroupsL();
        DxGroupDisp = cont.getDxGroupsR();
        DxGroupDisp = cont.getDxGroups();
        cont.saveDx();
        
        cont.previousAction();
        cont.nextAction();
        cont.wizardSubmit();
        cont.submitQSAction();
        cont.cancelWizAction();
        
        
        System.assertEquals( 1, 1);
    }

}