/* --------------------------------------------------------------------------------
* COMPONENT: PEER Online 2.0
* CLASS: GlobalPEERRequisitionDao
* PURPOSE: Data access object for the PEERRequisition object that does not have the sharing restrictions
* CREATED: 03/10/16 Ethos Solutions - www.ethos.com
* Author: Joe DePetro
* --------------------------------------------------------------------------------
*/
public without sharing class GlobalPEERRequisitionDao extends BaseDao 
{
	private static final GlobalPEERRequisitionDao reqDao = new GlobalPEERRequisitionDao();
    private static String NAME = 'PEER_Requisition__c'; 

    public static GlobalPEERRequisitionDao getInstance() 
    {
        return reqDao; 
    }

    /**
    * @Description This method will get all the fields needed for the CustomerStory and related objects. 
    * @returns a string with comma separated field names. These field names will be all the user accessable fields for the CustomerStory object and related objects.
    */
    public String getAllFieldStr()
    {
        String fldList = getFieldSql(Schema.SObjectType.PEER_Requisition__c.fields.getMap(), '');
        fldList += ', RecordType.Name, Physician__r.Account.Allow_Rapid_Turnaround__c, Physician__r.physician_portal_user__c, Physician__r.Account.PEER_Interactive__c, Physician__r.Account.Report_Build_Type__c ';
		return fldList;
    }

    /**
    * @Description This method return a PEERRequisition record by id
    * @param recId - PEERRequisition record Id
    * @return PEERRequisition record
    */
    public PEER_Requisition__c getById(String recId)
    {
        List<PEER_Requisition__c> reqList = Database.query('select ' + getAllFieldStr() + ' from PEER_Requisition__c where Id =: recId');
        if (reqList != null && !reqList.isEmpty())
            return reqList[0];
        return null;      
    }

    public List<PEERRequisitionDisp> getListForTech(PEERRequisitionPager pager)
    {
        String whereStr = (String.isNotBlank(pager.techId)) ? ' reeg_r_eeg_tech_contact__c = \'' + pager.techId + '\'' : '';
        if (String.isNotBlank(pager.ListCategory))
        {
            if (pager.ListCategory != 'All')
            {
                if (String.isNotBlank(whereStr))
                    whereStr += ' AND';
                            
                whereStr += ' reeg_eeg_rec_stat__c = \'' + pager.ListCategory + '\'';
            }
        }

        System.debug('## getListForTech:whereStr: ' + whereStr);

        List<PEER_Requisition__c> lst = (List<PEER_Requisition__c>)getSObjectListByWhere(getAllFieldStr(), NAME, whereStr, pager.CurrSort, '500');
        List<PEERRequisitionDisp> returnList = new List<PEERRequisitionDisp>();
        if (lst != null && !lst.isEmpty())
        {
            System.debug('## getListForTech:lst.size: ' + lst.size());

            pager.setRecordCount(lst.size());
            for (PEER_Requisition__c row : lst)
            {
                if (pager.shouldAddRow() || true) returnList.Add(new PEERRequisitionDisp(row)); 
            }
        }
        return returnList;
    } 

    public List<PEERRequisitionDisp> getListForClinic(PEERRequisitionPager pager)
    {
        String fieldStr = getAllFieldStr() + ',Physician__r.Account.Name ';
        String whereStr = 'Physician__r.Account.Name = \'' + pager.accountName + '\'';
        if (String.isNotBlank(pager.ListCategory))
        {
            if (pager.ListCategory != 'All')
            {
                if (String.isNotBlank(whereStr))
                    whereStr += ' AND';
                            
                whereStr += ' reeg_eeg_rec_stat__c = \'' + pager.ListCategory + '\'';
            }
        }

        System.debug('## getListForClinic:whereStr: ' + whereStr);
        
        List<PEER_Requisition__c> lst = (List<PEER_Requisition__c>)getSObjectListByWhere(fieldStr, NAME, whereStr, pager.CurrSort, '500');
        List<PEERRequisitionDisp> returnList = new List<PEERRequisitionDisp>();
        if (lst != null && !lst.isEmpty())
        {
            pager.setRecordCount(lst.size());
            for (PEER_Requisition__c row : lst)
            {
                if (pager.shouldAddRow()) returnList.Add(new PEERRequisitionDisp(row)); 
            }
        }
        return returnList;
    } 

    public Boolean Save(PEER_Requisition__c reqObj)
    {
        List<Contact> contactList;
        if (ReqObj.Physician__c != null)
            contactList = [SELECT Id, AccountId, Account.Owner.Email FROM Contact WHERE Id = :ReqObj.Physician__c];

        //-- if the PEERReq could be commercial, Custom Study, or a generalstudy. An Account can do both Commercial & Study types of tests so we need to see what the user chose (Commercial or a specific study).
        if (String.isNotBlank(ReqObj.Study_Name__c))
        {
            if (ReqObj.Study_Name__c.equalsIgnoreCase('standard'))
            {
                ReqObj.RecordTypeId = PEERRequisitionDao.getInstance().getRecordTypeIdByName('Commercial');
            }
            else
            {
                ReqObj.RecordTypeId = PEERRequisitionDao.getInstance().getRecordTypeIdByName('General Study');

                if (ReqObj.Study_Name__c.equalsIgnoreCase('Walter Reed'))
                {
                    ReqObj.Process_Used_For_Test__c = 'Military Study';
                }

                Id accountId = null;
                if (contactList != null && !contactList.isEmpty())
                    accountId = contactList[0].AccountId;
                if (accountId != null)
                {
                    UserDao uDao = new UserDao();
                    List<User> userList = uDao.getPortalManagerUsersByAccountId(accountId);
                    if (userList != null && !userList.isEmpty())
                    {
                        for (User u : userList)
                        {
                            if (ReqObj.Study_Coordinator_Email__c == null)
                                ReqObj.Study_Coordinator_Email__c = u.Email;
                            else if (ReqObj.Study_Coordinator_Email_2__c == null && ReqObj.Study_Coordinator_Email__c != u.Email)
                                ReqObj.Study_Coordinator_Email_2__c = u.Email;
                            else if (ReqObj.Study_Coordinator_Email_3__c == null && ReqObj.Study_Coordinator_Email__c != u.Email && ReqObj.Study_Coordinator_Email_2__c != u.Email)
                                ReqObj.Study_Coordinator_Email_3__c = u.Email;
                            else
                                break;
                        }
                    }
                }
            }
        }

        if (contactList != null && !contactList.isEmpty() && contactList[0].Account.Owner.Email != null)
            ReqObj.Account_Manager_Email__c = contactList[0].Account.Owner.Email;

        if (String.isBlank(ReqObj.RecordTypeId))
            ReqObj.RecordTypeId = PEERRequisitionDao.getInstance().getRecordTypeIdByName('Commercial');

        //-- If the EEG was uploaded since the last PEER Req form save there may be values on the PEER Requesition record that have been updated by the server website. 
        //-- those values need to kept in the record and not overwritted here. 
        if (reqObj != null && reqObj.Id != null && (String.isBlank(reqObj.Status__c) || !reqObj.Status__c.equalsIgnoreCase('Submitted')))
        {
            PEER_Requisition__c currentReq = getById(reqObj.Id);

            if (currentReq != null)
            {
                if (String.isNotBlank(currentReq.EEG_Status__c))
                    reqObj.EEG_Status__c = currentReq.EEG_Status__c;
                
                if (String.isNotBlank(currentReq.reeg_eeg_rec_stat__c))
                    reqObj.reeg_eeg_rec_stat__c = currentReq.reeg_eeg_rec_stat__c;
                
                if (String.isNotBlank(currentReq.EEG_Filename__c))
                    reqObj.EEG_Filename__c = currentReq.EEG_Filename__c;
                
                if (String.isNotBlank(currentReq.EEG_Make__c))
                    reqObj.EEG_Make__c = currentReq.EEG_Make__c;
                
                if (currentReq.EEG_Upload_Date__c != null)
                    reqObj.EEG_Upload_Date__c = currentReq.EEG_Upload_Date__c;
            }
        }

        return saveSObject(reqObj);
    }

    public void submit(PEER_Requisition__c reqObj, List<Req_Outcome_Medication__c> currentMedList)
    {
        Savepoint sp = Database.setSavepoint();
        Boolean saveOk = savePatient(reqObj);
        Id reegId = saveReeg(reqObj);

        List<r_reeg_dx__c> dxUpdateList = new List<r_reeg_dx__c>();

        for (r_reeg_dx__c dx : [select Id, r_reeg_id__c from r_reeg_dx__c where PEER_Requisition__c =: reqObj.Id limit 100])
        {
            dx.r_reeg_id__c = reegId;
            dxUpdateList.add(dx);
        }

        try 
        {
            upsert dxUpdateList;
        }
        catch (Exception ex)
        {
            saveOk = false;
            System.debug('Global: error updating DX for new rEEG record. reegId: ' + reegId);
        }

        String ocId = saveOutcome(reqObj);
        saveOutcomeMed(currentMedList, ocId);
        buildIntervals(reqObj);

        if (!saveOk || reegId == null)
        {
            Database.rollback( sp );
        }
        else
        {
            reqObj.Status__c = 'Submitted';
            save(reqObj);
        }
    }

    public Boolean savePatient(PEER_Requisition__c reqObj)
    {
        r_patient__c pat = new r_patient__c();

        if (String.isNotBlank(reqObj.Patient_Id__c))
        {
            pat = GlobalPatientDao.getInstance().getById(reqObj.Patient_Id__c);
        }

        pat.first_name__c = reqObj.patient_first_name__c;
        pat.name = reqObj.patient_name__c;
        if (String.isNotBlank(reqObj.patient_middle_initial__c))
            pat.middle_initial__c = reqObj.patient_middle_initial__c.left(1);
        pat.dob__c = reqObj.patient_dob__c;
        pat.Gender__c = reqObj.patient_gender__c;
        pat.EEG_Cap_Size__c = reqObj.EEG_Cap_Size__c;

        UserDao useDao = new UserDao();
        User currentOwner = (String.isNotBlank(pat.OwnerId)) ? useDao.getById(pat.OwnerId) : useDao.getCurrentUser();

        System.debug('Global: currentOwner.Contact.PEER_Ownership__c: ' + currentOwner.Contact.PEER_Ownership__c);

        if ( !(currentOwner != null && !String.isBlank(currentOwner.Contact.PEER_Ownership__c) && currentOwner.Contact.PEER_Ownership__c.equalsIgnoreCase('Self')))
        {
            if (reqObj.patient_physician_user__c != null)
                pat.OwnerId = reqObj.patient_physician_user__c;
        }
        
        pat.physician__c = reqObj.Physician__c;
        pat.test_data_flag__c = reqObj.test_data_flag__c;
        pat.anticipated_eeg_test__c = reqObj.patient_anticipated_eeg_test__c;
        pat.prov_pat_id__c = reqObj.patient_prov_pat_id__c;
        pat.desired_eeg_test__c = reqObj.patient_desired_eeg_test__c;
        pat.PEER_Requisition__c = reqObj.Id;

        try
        {
            upsert pat;
        }
        catch (Exception ex)
        {
           // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Patient save error.'));
            return false;
        }
        
        reqObj.Patient_Id__c = pat.Id;
        reqObj.Patient__c = pat.Id;
        
        return true;
    }

    public String saveReeg(PEER_Requisition__c reqObj)
    {
        r_reeg__c reeg = null;
        if (String.isBlank(ReqObj.PEER__c))
        {
            reeg = new r_reeg__c();
            reeg.r_patient_id__c = reqObj.Patient_Id__c;
        }
        else
        {
            rEEGDao rDao = new rEEGDao();
            reeg = rDao.getById(ReqObj.PEER__c);
        }

        reeg.phy_billing_code__c = reqObj.reeg_phy_billing_code__c;
        reeg.eeg_notes__c = reqObj.reeg_eeg_notes__c;
        reeg.eeg_rec_stat__c = (reqObj.EEG_Status__c == 'Uploaded' || reqObj.EEG_Status__c == 'Complete') ? 'Received' : '';
        if (reqObj.EEG_Upload_Date__c != null)
            reeg.eeg_rec_date__c = reqObj.EEG_Upload_Date__c;
        reeg.r_eeg_tech_contact__c = reqObj.reeg_r_eeg_tech_contact__c;
        reeg.eeg_upload__c = (reqObj.EEG_Status__c == 'Uploaded' || reqObj.EEG_Status__c == 'Complete') ? 'Yes' : '';
        reeg.reeg_type__c = reqObj.reeg_reeg_type__c;
        if (!reqObj.Patient_will_be_washed_out__c && reqObj.reeg_reeg_type__c != 'Type II')
            reeg.reeg_type_mod__c = '(m)';
        reeg.Use_Rapid_Turnaround__c = reqObj.reeg_Rapid_Turnaround__c;
        reeg.test_data_flag__c = reqObj.test_data_flag__c;
        reeg.do_not_send_neuro__c = !reqObj.Neuro_Review__c;
        reeg.PEER_Requisition__c = reqObj.Id;
        reeg.Study_Name__c = reqObj.Study_Name__c;
        reeg.Bill_To__c = reqObj.Bill_To__c;
        reeg.req_date__c = Datetime.now();
        reeg.req_stat__c = 'In Progress';
        reeg.rpt_stat__c = 'In Progress';
        reeg.eeg_make__c = reqObj.EEG_Make__c;
        reeg.neuroguide_status__c = 'Requisition Submitted';
        reeg.Process_Used_For_Test__c = (String.isNotBlank(reqObj.Process_Used_For_Test__c)) ? reqObj.Process_Used_For_Test__c : 'NeuroGuide 2';

        if (String.isNotBlank(ReqObj.Account_Manager_Email__c))
            reeg.Account_Manager_Email__c = ReqObj.Account_Manager_Email__c;

        if (String.isNotBlank(ReqObj.Study_Coordinator_Email__c))
            reeg.Study_Coordinator_Email__c = ReqObj.Study_Coordinator_Email__c;
        if (String.isNotBlank(ReqObj.Study_Coordinator_Email_2__c))
            reeg.Study_Coordinator_Email_2__c = ReqObj.Study_Coordinator_Email_2__c;
        if (String.isNotBlank(ReqObj.Study_Coordinator_Email_3__c))
            reeg.Study_Coordinator_Email_3__c = ReqObj.Study_Coordinator_Email_3__c;

        if (String.isNotBlank(ReqObj.patient_dob__c))
        {
            Date dobDt = e_StringUtil.setDateFromString(ReqObj.patient_dob__c);
            integer numberDaysOld = dobDt.daysBetween(Date.today());             
            if (numberDaysOld > 0)
                reeg.patient_age__c = (numberDaysOld / 365.242199);
        }
        

        if (String.isNotBlank(ReqObj.Study_Name__c) && ReqObj.Study_Name__c.equalsIgnoreCase('Walter Reed'))
        {
            reeg.RecordTypeId = rEEGDao.getInstance().getRecordTypeIdByName('WR Record Type');
        }
        else if (reqObj.RecordTypeId != null && String.isNotBlank(reqObj.RecordType.Name))
        {         
            reeg.RecordTypeId = rEEGDao.getInstance().getRecordTypeIdByName(reqObj.RecordType.Name);
        }      

        try
        {
            upsert reeg;
            ActionDao.insertReegActionPEER(reeg.Id, 'UPDATE_DB_STATUS', '');
        }
        catch (Exception ex)
        {
            System.debug('## GlobalPEERRequisitionDao:saveReeg:ex ' + ex);
          //  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'PEER save error.'));
            return null;
        }

        reqObj.PEER__c = reeg.Id;

        return reeg.Id;
    }

    public String saveOutcome(PEER_Requisition__c reqObj)
    {
        r_patient_outc__c oc = new r_patient_outc__c();
        oc.patient__c = reqObj.Patient_Id__c;
        oc.PEER_Requisition__c = reqObj.Id;
        oc.rEEG__c = reqObj.PEER__c;
        oc.cgi__c = reqObj.outcome_cgi__c;
        oc.cgs__c = reqObj.outcome_cgs__c;
        oc.insert_method__c = 'Portal 2.5';
        oc.outcome_date__c = Date.today();

        try
        {
            upsert oc;
        }
        catch (Exception ex)
        {
       //     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Outcome save error - ex: ' + ex));
            return null;
        }

        return oc.Id;
    }

    public void saveOutcomeMed(List<Req_Outcome_Medication__c> currentMedList, String outcomeId)
    {
        List<r_outcome_med__c> newMeds = new List<r_outcome_med__c>();
        for(Req_Outcome_Medication__c med : currentMedList)
        {
            newMeds.add(new r_outcome_med__c(r_outcome_id__c = outcomeId, previous_med__c = med.previous_med__c, calc_washout_date__c = med.calc_washout_date__c, dosage__c = med.dosage__c, end_date__c = med.end_date__c, frequency__c = med.frequency__c, med_name__c = med.med_name__c, start_date__c = med.start_date__c, start_date_is_estimated__c = med.start_date_is_estimated__c, unit__c = med.unit__c, washout_days__c = med.washout_days__c, Entry_Type__c = med.Entry_Type__c, Patient_Will_Wash_Out__c = med.Patient_Will_Wash_Out__c, Washout_Requirement__c = med.Washout_Requirement__c));
        }

        insert newMeds;
    }

    public void buildIntervals(PEER_Requisition__c reqObj)
    {
        patientIntervalBuilder intBuilder = new patientIntervalBuilder();
        intBuilder.buildAllIntervalsForPat(reqObj.Patient_Id__c, reqObj.patient_physician_user__c);
    }
}