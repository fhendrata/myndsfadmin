Public Class SysLogDisp
{
    private sys_log__c obj;
    public sys_log__c getObj()
    {
        return obj;
    }
    public void setObj(sys_log__c s)
    {
        obj = s;
    }

    //---Setup method
    public void Setup(sys_log__c o)
    {
        obj = o;
    }

    //------------TEST-----------------------------
    public static testMethod void testDisp()
    {
        sys_log__c obj = new sys_log__c();

        SysLogDisp disp = new SysLogDisp();
        disp.setObj(obj);
        disp.Setup( obj);

        sys_log__c obj2 = disp.getObj();

        System.assertEquals( obj, obj2);
    }
}