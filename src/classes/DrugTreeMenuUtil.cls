public with sharing class DrugTreeMenuUtil 
{
	public Map<string, MenuItem> MenuList;
	public Map<string, MenuItem> PEERMenuList;
	public Map<string, MenuItem> wrMenuList;

	public DrugTreeMenuUtil()
	{
		MenuList = loadMenuItems();
		wrMenuList = loadWrMenuItems();
		PEERMenuList = loadPEERMenuItems();
	}
	
	private static Map<string, MenuItem> loadPEERMenuItems()
	{
		Map<string, MenuItem> mList = new Map<string, MenuItem>();
		
		mlist.put('3', new MenuItem('SSRI','3','0'));
		mlist.put('4', new MenuItem('Citalopram','4','3'));
		mlist.put('5', new MenuItem('Escitalopram','5','3'));
		mlist.put('6', new MenuItem('Fluoxetine','6','3'));
		mlist.put('7', new MenuItem('Fluvoxamine','7','3'));
		mlist.put('8', new MenuItem('Paroxetine','8','3'));
		mlist.put('9', new MenuItem('Sertraline','9','3'));
	
		mlist.put('10', new MenuItem('tca','10','0'));
		mlist.put('11', new MenuItem('Desipramine','11','10'));
		mlist.put('12', new MenuItem('Imipramine','12','10'));

		mlist.put('13', new MenuItem('snri','13','0'));
		mlist.put('14', new MenuItem('Duloxetine','14','13'));
		mlist.put('15', new MenuItem('Venlafaxine','15','13'));
		
		mlist.put('16', new MenuItem('amk','16','0'));
		mlist.put('17', new MenuItem('Bupropion','17','16'));
	
		mlist.put('18', new MenuItem('maoi','18','0'));
		mlist.put('19', new MenuItem('Selegiline','19','18'));
		mlist.put('20', new MenuItem('Tranylcypromine','20','18'));
		mlist.put('21', new MenuItem('Modafanil','21','18'));
		
		mlist.put('22', new MenuItem('Stimulants','22','0'));
		mlist.put('23', new MenuItem('Atomoxetine','23','22'));
		mlist.put('24', new MenuItem('Damphetamine','24','22'));
		mlist.put('25', new MenuItem('Dlamphetamine','25','22'));
		mlist.put('26', new MenuItem('Methylphenidate','26','22'));
		
		mlist.put('27', new MenuItem('aepms','27','0'));
		mlist.put('28', new MenuItem('Carbamazepine','28','27'));
		mlist.put('29', new MenuItem('Divalproex','29','27'));
		mlist.put('30', new MenuItem('Gabapentin','30','27'));
		mlist.put('31', new MenuItem('Lamotrigine','31','27'));
		mlist.put('32', new MenuItem('Oxcarbazepine','32','27'));
	
		mlist.put('33', new MenuItem('ms','33','0'));
		mlist.put('34', new MenuItem('Lithium','34','33'));
		mlist.put('35', new MenuItem('efa','35','33'));
		
		mlist.put('36', new MenuItem('bzo','36','0'));
		mlist.put('37', new MenuItem('Alprazolam','37','36'));
		mlist.put('38', new MenuItem('Clonazepam','38','36'));
		mlist.put('39', new MenuItem('Lorazepam','39','36'));
		
		mlist.put('40', new MenuItem('aty','40','0'));
		mlist.put('41', new MenuItem('Risperidone','41','40'));
		mlist.put('42', new MenuItem('Olanzapine','42','40'));
		
		/*
		//-- jdepetro.6.19.13 PEER Online changed to match PEER Interactive w/ addtition of atyp anit.
		mlist.put('4', new MenuItem('Anticonvulsants','4','0'));
		mlist.put('5', new MenuItem('Alprazolam','5','5'));
		mlist.put('6', new MenuItem('Lorazepam','6','5'));
		mlist.put('7', new MenuItem('Clonazepam','7','5'));
		mlist.put('8', new MenuItem('Carbamazepine','8','5'));
		mlist.put('9', new MenuItem('Lamotrigine','9','5'));
		mlist.put('10', new MenuItem('Omega369','10','5'));
		mlist.put('11', new MenuItem('Oxcarbazepine','11','5'));
		mlist.put('12', new MenuItem('Divalproex','12','5'));
		mlist.put('13', new MenuItem('Gabapentin','13','5'));
		mlist.put('14', new MenuItem('Lithium','14','5'));
		mlist.put('15', new MenuItem('Antidepressants','15','0'));
		mlist.put('16', new MenuItem('Fluoxetine','16','15'));
		mlist.put('17', new MenuItem('Sertraline','17','15'));
		mlist.put('18', new MenuItem('Paroxetine','18','15'));
		mlist.put('19', new MenuItem('Fluvoxamine','19','15'));
		mlist.put('20', new MenuItem('Citalopram','20','15'));
		mlist.put('21', new MenuItem('Desipramine','21','15'));
		mlist.put('22', new MenuItem('Imipramine','22','15'));
		mlist.put('23', new MenuItem('Bupropion','23','15'));
		mlist.put('24', new MenuItem('Venlafaxine','24','15'));
		mlist.put('25', new MenuItem('Escitalopram','25','15'));
		mlist.put('26', new MenuItem('Stimulants','26','0'));
		mlist.put('27', new MenuItem('Methylphenidate','27','26'));
		mlist.put('28', new MenuItem('Damphetamine','28','26'));
		mlist.put('29', new MenuItem('Dlamphetamine','29','26'));
		mlist.put('30', new MenuItem('Aminoacids','30','26'));
		mlist.put('31', new MenuItem('Atomoxetine','31','26'));
		mlist.put('32', new MenuItem('Modafinil','32','26'));
		
		mlist.put('33', new MenuItem('MAOI','33','0'));
		mlist.put('34', new MenuItem('Selegiline','34','33'));
		mlist.put('35', new MenuItem('Tranylcypromine','35','33'));
		
		
		mlist.put('36', new MenuItem('Atypicals','36','0'));
		mlist.put('37', new MenuItem('Olanzapine','37','36'));
		mlist.put('38', new MenuItem('Risperidone','38','36'));
		
		mlist.put('39', new MenuItem('Subgroups','39','0'));
		mlist.put('40', new MenuItem('maoi','40','39'));
		mlist.put('41', new MenuItem('ssri','41','39'));
		mlist.put('42', new MenuItem('tca','42','39'));
		mlist.put('43', new MenuItem('benzodiazepines','43','39'));
		mlist.put('44', new MenuItem('Atypical Antipsychotics','44','39'));
		*/
		return mList;
	}
		
	
	private static Map<string, MenuItem> loadMenuItems()
	{
		Map<string, MenuItem> mList = new Map<string, MenuItem>();
		
		mlist.put('5', new MenuItem('SSRI','5','0'));
		mlist.put('6', new MenuItem('Citalopram','6','5'));
		mlist.put('7', new MenuItem('Escitalopram','7','5'));
		mlist.put('8', new MenuItem('Fluoxetine','8','5'));
		mlist.put('9', new MenuItem('Fluvoxamine','9','5'));
		mlist.put('10', new MenuItem('Paroxetine','10','5'));
		mlist.put('11', new MenuItem('Sertraline','11','5'));
	
		mlist.put('12', new MenuItem('tca','12','0'));
		mlist.put('13', new MenuItem('Desipramine','13','12'));
		mlist.put('14', new MenuItem('Imipramine','14','12'));

		mlist.put('15', new MenuItem('snri','15','0'));
		mlist.put('16', new MenuItem('Duloxetine','16','15'));
		mlist.put('17', new MenuItem('Venlafaxine','17','15'));
		
		mlist.put('18', new MenuItem('amk','18','0'));
		mlist.put('19', new MenuItem('Bupropion','19','18'));
	
		mlist.put('20', new MenuItem('maoi','20','0'));
		mlist.put('21', new MenuItem('Selegiline','21','20'));
		mlist.put('22', new MenuItem('Tranylcypromine','22','20'));
		
		mlist.put('23', new MenuItem('Stimulants','23','0'));
		mlist.put('24', new MenuItem('Damphetamine','24','23'));
		mlist.put('25', new MenuItem('Dlamphetamine','25','23'));
		mlist.put('26', new MenuItem('Methylphenidate','26','23'));
		
		mlist.put('27', new MenuItem('aepms','27','0'));
		mlist.put('28', new MenuItem('Carbamazepine','28','27'));
		mlist.put('29', new MenuItem('Divalproex','29','27'));
		mlist.put('30', new MenuItem('Gabapentin','30','27'));
		mlist.put('31', new MenuItem('Lamotrigine','31','27'));
		mlist.put('32', new MenuItem('Oxcarbazepine','32','27'));
	
		mlist.put('33', new MenuItem('ms','33','0'));
		mlist.put('34', new MenuItem('Lithium','34','33'));
		
		mlist.put('35', new MenuItem('bzo','35','0'));
		mlist.put('36', new MenuItem('Alprazolam','36','35'));
		mlist.put('37', new MenuItem('Clonazepam','37','35'));
		mlist.put('38', new MenuItem('Lorazepam','38','35'));
		
		mlist.put('39', new MenuItem('aty','39','0'));
		mlist.put('40', new MenuItem('Risperidone','40','39'));
		mlist.put('41', new MenuItem('Olanzapine','41','39'));
		

		return mList;
	}
	
	private static Map<string, MenuItem> loadWrMenuItems()
	{
		Map<string, MenuItem> mList = new Map<string, MenuItem>();
		
		mlist.put('5', new MenuItem('SSRI','5','0'));
		mlist.put('6', new MenuItem('Citalopram','6','5'));
		mlist.put('7', new MenuItem('Escitalopram','7','5'));
		mlist.put('8', new MenuItem('Fluoxetine','8','5'));
		mlist.put('9', new MenuItem('Fluvoxamine','9','5'));
		mlist.put('10', new MenuItem('Paroxetine','10','5'));
		mlist.put('11', new MenuItem('Sertraline','11','5'));
	
		mlist.put('12', new MenuItem('tca','12','0'));
		mlist.put('13', new MenuItem('Desipramine','13','12'));
		mlist.put('14', new MenuItem('Imipramine','14','12'));

		mlist.put('15', new MenuItem('snri','15','0'));
		mlist.put('16', new MenuItem('Duloxetine','16','15'));
		mlist.put('17', new MenuItem('Venlafaxine','17','15'));
		
		mlist.put('18', new MenuItem('amk','18','0'));
		mlist.put('19', new MenuItem('Bupropion','19','18'));
	
		mlist.put('20', new MenuItem('maoi','20','0'));
		mlist.put('21', new MenuItem('Selegiline','21','20'));
		mlist.put('22', new MenuItem('Tranylcypromine','22','20'));
		
		mlist.put('23', new MenuItem('Stimulants','23','0'));
		mlist.put('24', new MenuItem('Damphetamine','24','23'));
		mlist.put('25', new MenuItem('Dlamphetamine','25','23'));
		mlist.put('26', new MenuItem('Methylphenidate','26','23'));
		
		mlist.put('27', new MenuItem('aepms','27','0'));
		mlist.put('28', new MenuItem('Carbamazepine','28','27'));
		mlist.put('29', new MenuItem('Divalproex','29','27'));
		mlist.put('30', new MenuItem('Gabapentin','30','27'));
		mlist.put('31', new MenuItem('Lamotrigine','31','27'));
		mlist.put('32', new MenuItem('Oxcarbazepine','32','27'));
	
		mlist.put('33', new MenuItem('ms','33','0'));
		mlist.put('34', new MenuItem('Lithium','34','33'));
		
		mlist.put('35', new MenuItem('bzo','35','0'));
		mlist.put('36', new MenuItem('Alprazolam','36','35'));
		mlist.put('37', new MenuItem('Clonazepam','37','35'));
		mlist.put('38', new MenuItem('Lorazepam','38','35'));
		
		//	mlist.put('13', new MenuItem('Amitriptyline','13','12'));
		//	mlist.put('16', new MenuItem('Nortriptyline','16','12'));
		//	mlist.put('26', new MenuItem('Atomoxetine','26','25'));

		return mList;
	}
	
	public static Map<string, MenuItem> getMenuMap()
	{
		Map<string, MenuItem> returnVal = loadMenuItems();
		return returnVal;
	}
	
	public static Map<string, MenuItem> getWrMenuMap()
	{
		Map<string, MenuItem> returnVal = loadWrMenuItems();
		return returnVal;
	}
	
	public static Map<string, MenuItem> getPEERMenuMap()
	{
		Map<string, MenuItem> returnVal = loadPEERMenuItems();
		return returnVal;
	}
	
	public class MenuItem
	{
		public MenuItem (string name, string n, string pn) { drugName = name; num = n; parentNumber = pn; }
		public string drugName {get;set;}
		public string num {get;set;}
		public string parentNumber {get;set;}
	}
	
	public static testmethod void testMenu()
	{
	   DrugTreeMenuUtil.loadMenuItems();
	   DrugTreeMenuUtil.loadWrMenuItems();	
	   DrugTreeMenuUtil.loadPEERMenuItems();	
		
	}
	

}