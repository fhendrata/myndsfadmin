//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: GlobalPatientDaoTest
// PURPOSE: Test class for GlobalPatientDao class methods
// CREATED: 03/22/17 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class GlobalPatientDaoTest {
	
	public static testMethod void testPatDao()
    {
        GlobalPatientDao dao = new GlobalPatientDao();
        dao.IsTestCase = true;
        
        System.assert(!e_StringUtil.isNullOrEmpty(GlobalPatientDao.getFieldStr()));
        
        r_patient__c testPat = GlobalPatientDao.getTestPat();
        
        r_patient__c pat = dao.getById(testPat.Id);
        pat = dao.getById(testPat.Id, true);
        
        r_reeg__c reeg = rEEGDao.getTestReeg();
        pat = dao.getByReegId(reeg.Id);
        
		List<r_patient__c> patList = dao.getByPhyId(testPat.physician__c, DateTime.now());  
		
		DateTime dt = DateTime.now();
		patList = dao.getByMonthYear(dt.month(), dt.year());
		
		patList = dao.getAllByFnOrLn('Smith');
		patList = dao.getAllByFnAndLn('Smith', 'John');
		patList = dao.getAllByFnAndLnAndDOB('Smith', 'John', '01/01/1960');
		patList = dao.getAllByFnAndDOBOrLnAndDOB('Smith', 'John', '01/01/1960');
		patList = dao.getAllSearchResults('Smith', 'John', '01/01/1960');
		patList = dao.getAllByFnOrLnOrCnsId('Smith');

		Contact c = GlobalPatientDao.getInstance().getContactByPortalUsername('drgordon@noemail.com');
    }    
	
}