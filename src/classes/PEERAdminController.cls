//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: PEERController
// PURPOSE: Base controller class for the PEER Report view page
// CREATED: 05/1/14 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class PEERAdminController extends BaseController
{

	public String PEERType { get; set; }
    public String PatientName { get; set; }
    public String PhysicianName { get; set; }
    public String EEGTechName { get; set; }
    public Boolean NeuroOrdered { get; set; }
    public r_reeg__c reeg { get; set; }

    public PEERAdminController(ApexPages.StandardController stdController)
    {
        this.reeg = (r_reeg__c)stdController.getRecord();
    }

    public void loadAction()
    {
        NeuroOrdered = false;
        rEEGDao reegDao = new rEEGDao();
        reeg = reegDao.getByIdWithPatPhyTechFields(reeg.Id);

        PEERType = clearNull(reeg.reeg_type__c) + clearNull(reeg.reeg_type_mod__c);
        PatientName = clearNull(reeg.r_patient_id__r.first_name__c) + ' ' + clearNull(reeg.r_patient_id__r.name);
        PhysicianName = clearNull(reeg.r_physician_id__r.FirstName) + ' ' + clearNull(reeg.r_physician_id__r.name);
        EEGTechName = clearNull(reeg.r_eeg_tech_contact__r.FirstName) + ' ' + clearNull(reeg.r_eeg_tech_contact__r.name);
        NeuroOrdered = !reeg.do_not_send_neuro__c;
    }

    public PageReference saveAdminAction()
    {
        reeg.do_not_send_neuro__c = !NeuroOrdered;

        if (String.isNotBlank(reeg.r_patient_id__r.dob__c))
        {
            Date dobDt = e_StringUtil.setDateFromString(reeg.r_patient_id__r.dob__c);
            integer numberDaysOld = dobDt.daysBetween(Date.today());             
            if (numberDaysOld > 0)
                reeg.patient_age__c = (numberDaysOld / 365.242199);
        }

        SaveReeg(reeg, 'UPDATE_DB_STATUS');

        PageReference pr = Page.PEERView;
        pr.getParameters().put('id', reeg.Id);
        return pr;
    }

    public PageReference cancelAction()
    {
        PageReference pr = Page.PEERView;
        pr.getParameters().put('id', reeg.Id);
        return pr;
    } 

    private String clearNull(String inputVal)
    {
        return (String.isNotBlank(inputVal)) ? inputVal : '';
    }

    public void SaveReeg(r_reeg__c reeg)
    {
        SaveReeg(reeg, '');
    }

    public void SaveReeg(r_reeg__c reeg, string statusType)
    {
        System.debug('Save in rEEGDAo');
        rEEGDao rDao = new rEEGDao();
        rDao.Save(reeg, statusType);
    }
}