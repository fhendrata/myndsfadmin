//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: peer_NeurologicController
// PURPOSE: Controller class for the Neurologic Report PDF page
// CREATED: 05/18/18 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
public with sharing class peer_NeurologicController
{
    public r_reeg__c reeg { get; set; }

	public peer_NeurologicController(ApexPages.StandardController stdController)
	{
		rEEGDao reegDao = new rEEGDao();
        reeg = reegDao.getById(stdController.getId());
	}
}