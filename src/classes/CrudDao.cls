//--------------------------------------------------------------------------------
// COMPONENT: Base
// CLASS: CrudDao
// PURPOSE: Core Data Access class 
// CREATED: 03/26/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public abstract class CrudDao extends BaseDao 
{
	public String setupPagerQueryWhere( String objName, String fieldList, String sqlWhere, BasePager pager)
	{		
		String returnVal = '';
		
		if (HideDeleted != null && HideDeleted == true)
		{						
			if (!(sqlWhere == null || sqlWhere == '')) sqlWhere += ' AND ';		
			sqlWhere += ' is_deleted__c = false';
		}
		
        try
        {
        	String sortOrder = '';
        	
        	if (!(pager.CurrSort == null || pager.CurrSort == ''))        	 
        	{
        		sortOrder = pager.CurrSort;
        	} else
        	{
        		sortOrder = 'Name';
        	}	
        	
        	String countQuery;
        	String query = getSql( fieldList, objName, sqlWhere, sortOrder);			
						
			if (!(sqlWhere == null || sqlWhere == ''))			
			{
				countQuery = 'select count() from ' + objName + ' where ' + sqlWhere;
			}
			else
			{
				countQuery = 'select count() from ' + objName;
			}      
			
			System.debug('SQL Count Query: ' + sqlWhere);
											
            Integer recCount;
			if (IsTestCase != null && IsTestCase == true)
			{
				recCount = Database.countQuery(countQuery + ' limit 2');
			}
			else
			{
				recCount = Database.countQuery(countQuery + ' limit 200');
			}
		
            pager.setRecordCount( recCount);                         
            query += ' limit ' + pager.getLimit();
        	
        	returnVal = query;
        }
        catch (Exception ex)
        {
            handleError( 'setupPagerQuery', ex);
        }
		
        return returnVal;
    }
}