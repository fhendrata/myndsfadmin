//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: EegTechController
//   PURPOSE: EEG Tech rEEG detail page controller
// 
//     OWNER: CNSR
//   CREATED: 10/27/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class tec_EegTechController extends BaseController
{
    public List<PatientOutcomeMedDisp> oMeds  { get; set; }
    public List<PatientOutcomeMedDisp> newMeds  { get; set; }
    public List<r_outcome_med__c> previousMeds { get; set; }
    public string outcomeId { get; set; }
    public string pId { get; set; }
    public integer rowCount {get; set;}
    public integer newRowCount {get; set;}
    public boolean isEEGActive {get; set;}
    public boolean showReschedule {get; set;}
    public boolean showCancel {get; set;}
    public boolean isWashedOut {get; set;}
    public r_reeg__c reeg {get; set;}
    public boolean setIsPrevious {get; set;}
    public List<String> medsToUpdate {get; set;}
    public boolean updatePrevious { get; set; }
    public boolean NoStDtMessage { get; set; }
    public boolean isMedEdit {get; set;}
    public boolean addNewMed {get; set;}
    public boolean medsVerified {get; set;}
    public boolean medConflict {get; set;}
    public string bpTech {get; set;}
    public string weightTech {get; set;}
    public Contact eegTech {get; set;}
    
    public patientOutcomeDao patOcDao { get; set; }
    public patientOutcomeMedDao ocMedDao { get; set; }
    public patientIntervalsDao intDao { get; set; }
    public GlobalPatientDao patDao { get; set; }
    
    
    public tec_EegTechController()
    {
        isEEGActive = true;
        isMedEdit = false;
        setIsPrevious = false;
        addNewMed = false;
    }
    
    public PageReference loadAction()
    {
        showReschedule = false;
        showCancel = false;
        isWashedOut = false;
        updatePrevious = false;
        NoStDtMessage = false;
        
        patOcDao = new patientOutcomeDao();
        ocMedDao = new patientOutcomeMedDao();
        intDao = new patientIntervalsDao();
        patDao = new GlobalPatientDao();
        
        string reegId = ApexPages.currentPage().getParameters().get('rid');

        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            rEEGDao rDao = new rEEGDao();
            reeg = rDao.getById(reegId, true);
            
            if (reeg != null)
            {
                setTechObj();
                    
                if (eegTech != null)
                {
                    reeg.eeg_make__c = eegTech.eeg_tech_default_test_make__c;
                }
                
                //weightTech = string.valueOf(reeg.vital_weight_eeg__c);
                bpTech = reeg.vital_bp_eeg__c;
            
                isEEGActive = (!e_StringUtil.isNullOrEmpty(reeg.eeg_rec_stat__c) && reeg.eeg_rec_stat__c != 'Received');
            }
        }
        
        pId = ApexPages.currentPage().getParameters().get('pid');
        
        if (!e_StringUtil.isNullOrEmpty(pId))
        {
            oMeds = getExistingOutcomeMeds(pId);
            
            if (oMeds != null && oMeds.size() > 0)
            {
                r_outcome_med__c ocMed = oMeds[0].getObj();
                if (ocMed != null)
                {
                    outcomeId = ocMed.r_outcome_id__c;                  
                }
            }
            else
            {
                List<r_patient_outc__c> outcomeList = patOcDao.getExistingOutcomes(pId);
                if (outcomeList != null && outcomeList.size() > 0)
                    outcomeId = outcomeList[0].Id;
            }
        }

        if (oMeds == null) oMeds = new List<PatientOutcomeMedDisp>();
        rowCount = 1;
        
        System.debug('%%% loadAction complete');
        
        return null;
    }
    
    public string getEnv()
    {
        return rEEGUtil.getEnvString();
    }

    public PageReference reSched()
    {
        showReschedule = true;
        return null;
    }
    
    public PageReference cancelAppt()
    {
        showCancel = true;
        return null;
    }
    
    public PageReference CancelReegPatReq()
    {
        if (reeg != null) 
        {
             System.debug('##!! CancelReegPatReq fired');
            reeg.req_stat__c = 'Not Valid - Patient stopped process';
            rEEGDao rDao = new rEEGDao();
            rDao.Save(reeg, 'UPDATE_DB_STATUS');
        }
        
        return null;
    }
    
    public PageReference submit()
    {
        boolean isEegUploaded = false;
        string medicatedTestTypeMod = '';
        string errorMsg = '';
        rEEGDao rDao = new rEEGDao();
        Boolean isPI = false;
        Boolean isPO2 = false;

        if (!e_StringUtil.isNullOrEmpty(weightTech))
            reeg.vital_weight_eeg__c = Double.valueOf(weightTech);
        reeg.vital_bp_eeg__c = bpTech;
        rDao.Save(reeg, '');

        if (eegTech == null) setTechObj();
        
        if (eegTech != null)
        {
            eegTech.eeg_tech_default_test_make__c = reeg.eeg_make__c;
            ContactDao cDao = new ContactDao();
            cDao.saveSObject(eegTech);  
        }
            
        reeg = rDao.getById(reeg.Id, true);     
        if (!e_StringUtil.isNullOrEmpty(reeg.eeg_upload__c) && reeg.eeg_upload__c == 'Yes')
        {
            if ((!e_StringUtil.isNullOrEmpty(reeg.r_physician_id__c)) && reeg.r_physician_id__r.Account != null)
            {
                isPI = (reeg.r_physician_id__r.Account.PEER_Interactive__c != null &&  reeg.r_physician_id__r.Account.PEER_Interactive__c);
                isPO2 = (!e_StringUtil.isNullOrEmpty(reeg.r_physician_id__r.Account.Report_Build_Type__c) &&  reeg.r_physician_id__r.Account.Report_Build_Type__c == 'PEER Online 2.0');
            }

            isEegUploaded = true;
        }
        else
        {
            errorMsg = 'The EEG file needs to be uploaded before submitting';
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, errorMsg);
            ApexPages.addMessage(msg);
        }
        
        if (!e_StringUtil.isNullOrEmpty(reeg.reeg_type__c) && reeg.reeg_type__c != 'Type II')
                medicatedTestTypeMod = rEEGUtil.getMedicatedTestMod(oMeds);
        
        //-- no meds is not true (Phy has selected that meds on test is OK)
        if (!reeg.med_no_med__c)
        {
        }
        else //-- no meds allowed on test
        {
            if (oMeds != null || oMeds.size() > 0)
            {
            }
        }
        
        if (isEegUploaded && (medsVerified == null || medsVerified))
        {
            reeg.reeg_type_mod__c = medicatedTestTypeMod;
            reeg.eeg_rec_stat__c = 'Received';
            reeg.eeg_rec_date__c = DateTime.now();
            reeg.Translate_Status__c = (reeg.is_ng_only__c || isPI || isPO2) ? 'N/A' : 'Pending';
            reeg.corr_stat__c = 'New';
            reeg.req_stat__c = 'In Progress';
            reeg.rpt_stat__c = 'In Progress';  
            if (medConflict)
                reeg.req_stat__c = 'On Hold - EEG Tech conflict';
            rDao.Save(reeg, 'UPDATE_DB_STATUS');
            
            return Page.tec_EegTechViewPage;
        }
        else
        {
            errorMsg = 'A confirmation is needed when a record has active medications';
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, errorMsg);
        //  ApexPages.addMessage(msg);
            return null;
        }
        
    }
    
    public PageReference saveSchedChange()
    {
        if (!e_StringUtil.isNullOrEmpty(reeg.eeg_tech_schedule_notes__c))
        {
            showReschedule = false;
            
            r_patient__c pat = patDao.getById(reeg.r_patient_id__c);
            if (pat != null)
            {       
                pat.anticipated_eeg_test__c = reeg.r_patient_id__r.anticipated_eeg_test__c;
                patDao.saveSObject(pat);
            }
            
            rEEGDao rDao = new rEEGDao();
            reeg.eeg_rec_stat__c = 'Rescheduled';
            rDao.Save(reeg, '');
            return Page.tec_EegTechHome;
        }
        else
        {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, 'A Schedule Change Note must be entered to reschedule a test');
            ApexPages.addMessage( msg);
            return null;
        }
    }

    public PageReference saveSchedCancel()
    {
        if (!e_StringUtil.isNullOrEmpty(reeg.eeg_tech_schedule_notes__c))
        {
            showCancel = false;
            rEEGDao rDao = new rEEGDao();
            reeg.eeg_rec_stat__c = 'Canceled';
            rDao.Save(reeg, '');
            return Page.tec_EegTechHome;
        }
        else
        {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, 'A Schedule Change Note must be entered to cancel a test');
            ApexPages.addMessage( msg);
            return null;
        }
    }
    
    public PageReference cancelSchedChange()
    {
        showReschedule = false;
        return null;
    }

    public PageReference cancelSchedCancel()
    {
        showCancel = false;
        return null;
    }
    
    private void setTechObj()
    {
        UserDao uDao = new UserDao();
        User usr = uDao.getById(UserInfo.getUserId());
        
        
        
        if (usr != null)
        {
          //  ContactDao cDao = new ContactDao();
          //  eegTech = cDao.getById(usr.ContactId);
        }   
    }
    
    //---------- Outcome med methods ----------------------------------------
    
       //---Build the existing outcome meds list (meds from previous outcome)
    public List<PatientOutcomeMedDisp> getExistingOutcomeMeds(string pid)
    {   
         if (oMeds == null)
         {  
             oMeds = new List<PatientOutcomeMedDisp>();  
             // List<string> previousIds = new List<string>();
             PatientOutcomeMedDisp oMed;          
             Integer ctr = -1;
             
             if (medsToUpdate == null) {
                medsToUpdate = new List<String>();
            }
             
             //---Get the current meds only
             previousMeds = ocMedDao.getExistingOutcomeMeds(pid);
             system.debug('### previousMeds:' + previousMeds);
             
             if (previousMeds != null) {
                for(r_outcome_med__c oMedRow : previousMeds) {
                        ctr++;  
                        oMed = new PatientOutcomeMedDisp();
                        oMed.setRowNum(ctr);
                        //oMed.mObj = oMedRow;
                        oMed.Setup(oMedRow);     
                        oMeds.Add(oMed);              
                }
                rowCount = ctr + 1; //---ctr is zero based                              
             }
             else {
                oMeds = null;
             }
        }    
        return oMeds;
    }
    
    public PageReference saveMeds()
    {
        system.debug('### saveMeds');
        if (oMeds != null) 
        {
            system.debug('### oMeds:' + oMeds);
            NoStDtMessage = false;
            //---Loop through each row
            for(PatientOutcomeMedDisp oMedRow : oMeds) {
                if (oMedRow != null) {
                    if (oMedRow.getObj() == null) {
                     //  addMessage('oMedRow.getObj() is null');         
                    } else {
                        if (oMedRow.getObj().start_date__c == null && !e_StringUtil.isNullOrEmpty(oMedRow.getObj().med_name__c)) {
                            NoStDtMessage = true;
                            return null;
                        } else {
                            NoStDtMessage = false;
                            r_outcome_med__c outcMed = oMedRow.getObj();
                        //  outcMed.r_outcome_id__c = outcomeId;
                            oMedRow.Setup(outcMed);
                            SaveOutcomeMed(outcMed);   
                        }                   
                    }
                }
            }
        }
            
        if (newMeds != null && newMeds.size() > 0)
        {
            system.debug('### newMeds:' + newMeds);
            for(PatientOutcomeMedDisp nMedRow : newMeds) 
            {
                if (nMedRow != null) 
                {
                    if (nMedRow.getObj() == null) 
                    {
                     //  addMessage('oMedRow.getObj() is null');         
                    } 
                    else 
                    {
                        if (nMedRow.getObj().start_date__c == null && !e_StringUtil.isNullOrEmpty(nMedRow.getObj().med_name__c)) {
                            NoStDtMessage = true;
                            return null;
                        } 
                        else 
                        {
                            NoStDtMessage = false;
                            r_outcome_med__c outcMed = nMedRow.getObj();
                            outcMed.r_outcome_id__c = outcomeId;
                            nMedRow.Setup(outcMed);
                            SaveOutcomeMed(outcMed);   
                            if (outcMed != null && outcMed.med_name__c != null && outcMed.med_name__c != '')
                                oMeds.add(nMedRow);
                        }                   
                    }
                }
            }
        }
   
        r_patient__c pat = patDao.getById(pId);
        
        if (pat != null)
        {
            patientIntervalBuilder intBuilder = new patientIntervalBuilder();
            intBuilder.buildAllIntervalsForPat(pat.Id, pat.OwnerId);
        }
        
        //loadAction();
        isMedEdit = false;
        addNewMed = false;
  
        system.debug('### isMedEdit' + isMedEdit);
  
        return null;
    }
    
    // ---Save an outcome med row
    private void SaveOutcomeMed(r_outcome_med__c medObj)
    {
        if (medObj != null && medObj.med_name__c != null && medObj.med_name__c != '')
        {
            ocMedDao.saveSObject(medObj);
        }
        else {
            //---If the id is not blank and the name is, then delete the old record
            if (medObj.id != null) ocMedDao.deleteSObject(medObj);
        }    
    }

    public PageReference addMedRow()
    {   
        Integer rowNum = Integer.valueOf(System.currentPageReference().getParameters().get('rowNumber'));
        // rowNum is 0 based, rowCount starts at 1
        if (rowNum != null && (rowNum + 1) == rowCount) 
        {
            //---Create blank row
            r_outcome_med__c objMed = new r_outcome_med__c();
            objMed.r_outcome_id__c = outcomeId;
                
            PatientOutcomeMedDisp oMed = new PatientOutcomeMedDisp();
            oMed.setRowNum(rowCount); 
            rowCount++; 
            oMed.Setup(objMed);                           
            oMeds.Add(oMed);    
        }        
        return null;      
    }
    
    public PageReference addBlankMedRows()
    {
        addNewMed = true;
        if (newMeds == null) newMeds = new List<PatientOutcomeMedDisp>();    
                
        for (integer i = 0; i < 5; i++)
        {
            PatientOutcomeMedDisp medDisp = new PatientOutcomeMedDisp();
            r_outcome_med__c newMed = new r_outcome_med__c();
            newMed.unit__c = 'mg';  
            medDisp.setObj(newMed);     
            medDisp.setRowNum(newMeds.size());
            newMeds.add(medDisp);       
        }
        newRowCount = newMeds.size();
        
        return null;
    }  
    
    public PageReference cancelMedEdit()
    {
        addNewMed = false;
        isMedEdit = false;
        return null;
    } 
    
    public PageReference editMeds()
    {
        isMedEdit = true;
        return null;
    }
    
    public PageReference refreshAutoAction()
    {
        return null;
    }  
    
  
}