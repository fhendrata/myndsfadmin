//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: CPL_PatientLeadTest
// PURPOSE: Test class for CPL_PatientLead class
// CREATED: 05/06/16 Ethos Solutions - www.ethos.com
// Author: Mitchell Corbin & Joe DePetro
//--------------------------------------------------------------------------------
@isTest
public with sharing class CPL_PatientLeadTest
{
	private static testmethod void testPatientLeadProcess()
	{
		CPL_PatientLead cpl = new CPL_PatientLead();

		Customer_Portal_Lead__c testCPLead = CustomerPortalLeadDao.getInstance().getTestCPLead();
		PEER_Requisition__c testPEER = PEERRequisitionDao.getTestObj();

		testPEER.Patient_Customer_Portal_Lead__c = testCPLead.Id;
		upsert testPEER;

		CPL_PatientLead.ProcessLead(testCPLead);
		System.assertEquals('Complete', testCPLead.Processing_Status__c);

		testPEER = PEERRequisitionDao.getInstance().getById(CustomerPortalLeadDao.getInstance().getRelatedPEERId(testCPLead));
		Lead testLead = LeadDao.getInstance().getById(testPEER.Patient_Lead__c);
		System.assertEquals(testCPLead.Last_Name__c, testLead.LastName);

		testCPLead.Processing_Status__c = 'Needs Update';
		CPL_PatientLead.ProcessLead(testCPLead);
		System.assertEquals('Complete', testCPLead.Processing_Status__c);
	}
}