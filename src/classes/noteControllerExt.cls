Public Class noteControllerExt extends BaseController
{
    private Note note;
    private String rid;

    Public noteControllerExt()
    {
        //---this is only used for the apex test case
        note = new Note();
        rid = 'xxxxxx';
        profile = new Profile();
    }

    //---Constructor from the standard controller
    public noteControllerExt(ApexPages.StandardController stdController)
    {    
        this.note = (Note)stdController.getRecord();
        this.rid = ApexPages.currentPage().getParameters().get('rid');
    }

    public Note getNote()
    {
        return note;
    }

    public PageReference SaveNote()
    {
        rid = ApexPages.currentPage().getParameters().get('rid');

        if (note != null)
        {
            note.ParentId = rid;
            note.Title = 'test note';
            if (note.Id != null) update note;
            else insert note;
        }

        PageReference returnVal = new PageReference('/apex/rEEGViewPage?id=' + rid);
        returnVal.setRedirect(true);
        return returnVal;
    }

    //---------------TEST METHODS ---------------------------------
    //
    //-------------------------------------------------------------
    public static testMethod void testController()
    {
    	rEEGControllerExt rCont = new rEEGControllerExt();
    	r_reeg__c testReeg = rCont.getTestReeg();
    	
    	PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('rid',testReeg.id);
    	
        noteControllerExt noteCont = new noteControllerExt();

        Note note = noteCont.getNote();
        pRef = noteCont.SaveNote();
        System.assertEquals( 1, 1);
    }
}