//--------------------------------------------------------------------------------
// COMPONENT: List Paging
// CLASS: BasePager
// PURPOSE: Base Pager logic, specific pagers should subclass and override
// CREATED: 03/26/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
Public abstract Class BasePager 
{
    //------------- Attributes ---------------------------
 	public Boolean IsTestCase { get; set;}
    
    private Integer currCount;
    public Integer getCurrCount()
    {
        return currCount;
    }

    private Integer pageStartPos;
    private Integer pageEndPos;

    //---Used for tracking the total record count
    private Integer recordCount;
    public Integer getRecordCount()
    {
        return recordCount;
    }
    public void setRecordCount(Integer value)
    {
        recordCount = value;
        calcPages();
    }

    //---The number of display rows, should be fixed
    private Integer dispRows = 15;
    public Integer getDispRows()
    {
        return dispRows;
    }
    public void setDispRows(Integer value)
    {
        dispRows = value;
    }

    //---The first row in the list to be displayed, this may become fixed at 0
    private Integer firstRow = 0;
    public Integer getFirstRow()
    {
        return firstRow;
    }
    public void setFirstRow(Integer value)
    {
        firstRow = value;
    }

    //---The current page viewed
    private Integer currentPage = 1;
    public Integer getCurrentPage()
    {
        return currentPage;
    }
    public void setCurrentPage(Integer value)
    {
        currentPage = value;
    }

    //---The max pages in the data set
    private Integer maxPages = 0;
    public Integer getMaxPages()
    {
        return maxPages;
    }
    public void setMaxPages(Integer value)
    {
        maxPages = value;
    }

    //---The page limit, max pages of data
    private Integer pageLimit = 190;

    //------------- Dynamic Attribute Methods ---------------------------
    public Boolean getRenderPager()
    {
        return currentPage > 1 || currentPage < maxPages; 
    }
    
    public Boolean getRenderPrevious()
    {
        return currentPage > 1;
    }

    public Boolean getRenderNext()
    {
        return currentPage < maxPages;
    }

    public Integer getLimit()
    {
        return pageEndPos;
    }

    //------------- Public Methods ---------------------------

    //---Should a row be added to the display list
    public Boolean shouldAddRow()
    {
        //---increment the current coutner
        currCount++;

        //---Determine if in the Current page of data to display
        return currCount >= pageStartPos && currCount <= pageEndPos;
    }

    //---Next
    public void nextAction()
    {
        if (currentPage < maxPages) currentPage++;

        loadData();
    }

    //---Previous
    public void previousAction()
    {
        if (currentPage > 1)
        {
             currentPage--;
        }
        else
        {
            currentPage = 1;
        }

        loadData();
    }

    public abstract void loadData();

    public String CurrSort { get; set; }

    public void sortList(String sortField) 
    {
        String prevSort = CurrSort;

        if (!e_StringUtil.isNullOrEmpty( prevSort) && prevSort.startsWith(sortField))
        {
            if (prevSort.toUpperCase().endsWith('DESC'))
            {
                sortField = sortField + ' ASC';
            }
            else
            {
                sortField = sortField + ' DESC';
            }
        }

        CurrSort = sortField;
        currentPage = 1;
        loadData();
    }

    //------------- Private Methods ---------------------------

    //---Calculate the number of pages and set the limit
    private void calcPages()
    {
        //---Reset the record counter
        currCount = 0;

        //---Set the max pages
        Integer modVal = Math.mod( recordCount, dispRows);

        maxPages = recordCount / dispRows;

        //---Have an overall ceiling of pages
        if (maxPages > pageLimit) maxPages = pageLimit;

        //---If there are any records left, add the extra page
        if (modVal > 0) maxPages++;

        //---Always have at least 1 page
        if (maxPages == 0) maxPages++;

        //---If over the bounds, then set to the end
        if (currentPage > maxPages) currentPage = maxPages;

        //---Set the Page start and end positions
        pageStartPos = 1;
        if (currentPage > 1) pageStartPos = (currentPage - 1) * dispRows + 1;
        pageEndPos = pageStartPos + dispRows - 1;
    }

	//----------------------------------------- ATTRIBUTES ---------------------------
	public String pagerType {get; set; }
    
    private String className;
    public String getClassName()
    {
        return className;
    }   
    public void setClassName(String value)
    {
        className = value;
    }

    //----------------------------------------- LOG METHODS ---------------------------
    public void handleError(String method, Exception ex)
    {
        // e_LogUtil.handleError( getClassName(), method, ex);        
    }

    //----------------------------------------- LOG METHODS ---------------------------
    public void debug(String message)
    {
       System.debug( className + ':' + pagerType + ':' + message);
    }

    public void logMethod(String methodName)
    {
        System.debug( className + ':' + pagerType + ':' + methodName);
    }
    
   
}