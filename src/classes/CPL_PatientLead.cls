//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: CPL_PatientLead
// PURPOSE: Functionality for updating a Lead record if any of the Lead record's 
//			Customer Portal Lead records are updated.
// CREATED: 05/05/16 Ethos Solutions - www.ethos.com
// Author: Mitchell Corbin & Joe DePetro
//--------------------------------------------------------------------------------
public class CPL_PatientLead
{
	public CPL_PatientLead() { }

	public static void ProcessLead(Sobject obj)
	{
		Customer_Portal_Lead__c cpLead = (Customer_Portal_Lead__c) obj;

		if (cpLead != null)
		{
			PEER_Requisition__c relatedPEER = PEERRequisitionDao.getInstance().getById(CustomerPortalLeadDao.getInstance().getRelatedPEERId(cpLead));

			if (relatedPEER != null)
			{
				if (cpLead.Processing_Status__c == 'New')
				{
					r_patient__c pat = [select Id, Name, Patient_Referral__c, Patient_Referral__r.Lead__c from r_patient__c where Id =: cpLead.Patient__c];

					if (pat != null)
					{
						//-- there is a Lead linked to the patient
						if (String.isNotBlank(pat.Patient_Referral__c) && String.isNotBlank(pat.Patient_Referral__r.Lead__c))
						{
							relatedPEER.Patient_Lead__c = pat.Patient_Referral__r.Lead__c;
						}
						else //-- there is no Lead linked to the patient
						{
							Lead currentLead = new Lead(FirstName = cpLead.First_Name__c, Patient_First_Name__c = cpLead.First_Name__c, LastName = cpLead.Last_Name__c, Patient_Last_Name__c = cpLead.Last_Name__c, Patient__c = cpLead.Patient__c,
												Email = cpLead.Email__c, Phone = cpLead.Phone__c, RecordTypeId = LeadDao.getInstance().getRecordTypeIdByName('Patient Lead'), Company = cpLead.Company__c, PEER_Completed_Date__c = cpLead.PEER_Completed_Date__c, 
												Status = cpLead.Lead_Status__c, LeadSource = cpLead.Lead_Source__c, Referral_Physician__c = cpLead.Referral_Physician__c);

							LeadDao.getInstance().saveSObject(currentLead);

							relatedPEER.Patient_Lead__c = currentLead.Id;
						}

						cpLead.Processing_Status__c = 'Complete';
					}
				}
				else if (cpLead.Processing_Status__c == 'Needs Update')
				{
					if (relatedPEER.Patient_Lead__c != null)
					{
						Lead currentLead = LeadDao.getInstance().getById(relatedPEER.Patient_Lead__c);

						if (String.isBlank(currentLead.Patient__c) && String.isNotBlank(cpLead.Patient__c))
							currentLead.Patient__c = cpLead.Patient__c;
						
						if (String.isBlank(currentLead.Referral_Physician__c) && String.isNotBlank(cpLead.Referral_Physician__c))
							currentLead.Referral_Physician__c = cpLead.Referral_Physician__c;
						
						currentLead.Status = cpLead.Lead_Status__c;
						currentLead.PEER_Completed_Date__c = cpLead.PEER_Completed_Date__c;

						if (String.isBlank(currentLead.RecordTypeId))
							currentLead.RecordTypeId = LeadDao.getInstance().getRecordTypeIdByName('Patient Lead');

						LeadDao.getInstance().saveSObject(currentLead);

						cpLead.Processing_Status__c = 'Complete';
					}
					else // If the Lead was deleted, set the Processing Status to 'New' so a new Lead is created and linked to the related PEER Req
					{
						cpLead.Processing_Status__c = 'New';
						ProcessLead(cpLead);
					}
				}

				PEERRequisitionDao.getInstance().saveSObject(relatedPEER);
			}
		}

		try
		{
			update cpLead;
		}
		catch (Exception ex) { System.debug('Clear Customer Portal Lead Processing Status flag error on save: ' + ex.getMessage()); }
	}
}