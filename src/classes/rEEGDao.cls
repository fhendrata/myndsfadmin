//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: rEEGDao
//   PURPOSE: rEEG data access class
// 
//     OWNER: CNSR
//   CREATED: 03/25/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class rEEGDao extends BaseDao
{
	private static String NAME = 'r_reeg__c'; 
    private static final rEEGDao reegDao = new rEEGDao();

    public static rEEGDao getInstance() 
    {
        reegDao.ObjectName = NAME;
        return reegDao; 
    }
	
	public rEEGDao()
	{
        ObjectName = NAME;
	}
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_reeg__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public r_reeg__c getById(String idInp)
    {
    	string fullFieldStr = getFieldStr();
    	fullFieldStr += ' , RecordType.Id, RecordType.Name, r_physician_id__r.Account.PEER_Interactive__c, r_physician_id__r.Account.Report_Build_Type__c ';
		return (r_reeg__c)getSObjectById(fullFieldStr, NAME, idInp);
    } 
    
    public r_reeg__c getById(String idInp, boolean includePatFields)
    {
    	if (!includePatFields)
			return (r_reeg__c)getSObjectById(getFieldStr(), NAME, idInp);
		else
		{
			string fullFieldStr = getFieldStr();
			fullFieldStr += ' , RecordType.Id, RecordType.Name, r_physician_id__r.Account.PEER_Interactive__c, r_physician_id__r.Account.Report_Build_Type__c, r_patient_id__r.prov_pat_id__c, r_patient_id__r.OwnerId, r_patient_id__r.name, r_patient_id__r.CA_Control_Group_Type__c, r_patient_id__r.first_name__c, r_patient_id__r.dob__c, r_patient_id__r.anticipated_eeg_test__c, r_patient_id__r.Gender__c';
			debug('### fullFieldStr:' + fullFieldStr);
			return (r_reeg__c)getSObjectById(fullFieldStr, NAME, idInp);
		}
    } 

    public r_reeg__c getByIdWithPatPhyTechFields(String idInp)
    {
        string fullFieldStr = getFieldStr();
        fullFieldStr += ' , RecordType.Id, RecordType.Name, r_patient_id__r.name, r_patient_id__r.dob__c, r_patient_id__r.first_name__c, r_physician_id__r.Account.PEER_Interactive__c, r_physician_id__r.Account.Report_Build_Type__c, r_physician_id__r.name, r_physician_id__r.FirstName, r_eeg_tech_contact__r.name, r_eeg_tech_contact__r.FirstName';
        debug('### fullFieldStr:' + fullFieldStr);
        return (r_reeg__c)getSObjectById(fullFieldStr, NAME, idInp);
    } 

    public r_reeg__c getFieldsById(String fields, String idInp)
    {
        return (r_reeg__c)getSObjectById(fields, NAME, idInp);
    } 
    
    public List<r_reeg__c> getByName(String recName)
    {
    	string fullFieldStr = getFieldStr();
    	fullFieldStr += ', RecordType.Id, RecordType.Name, r_patient_id__r.first_name__c, r_patient_id__r.name, r_patient_id__r.CNS_ID__c, r_patient_id__r.dob__c, r_patient_id__r.Gender__c, r_patient_id__r.physician__c';
    	string whereStr = ' name LIKE \'' + recName + '\'';
    	
		return (List<r_reeg__c>)getSObjectListByWhere(fullFieldStr, NAME, whereStr, '', '500');
    }  

    public List<r_reeg__c> getByInputWildcard(String inputVal, String rLimit)
    {
        if (StringUtil.isNullOrEmpty(rLimit)) rLimit = ' LIMIT 10000';
        inputVal = escapeStr(inputVal);
        List<String> tokens = inputVal.split(' ',2);

        string baseQuery = 'SELECT ' + getFieldStr();
        baseQuery += ', RecordType.Id, RecordType.Name, r_patient_id__r.first_name__c, r_patient_id__r.middle_initial__c, r_patient_id__r.Name, r_patient_id__r.CNS_ID__c, r_patient_id__r.dob__c, r_patient_id__r.Gender__c, r_patient_id__r.physician__c, r_patient_id__r.prov_pat_id__c, r_physician_id__r.Name, r_physician_id__r.Account.Report_Build_Type__c FROM r_reeg__c ';

        string whereStr = ' WHERE Name LIKE ' + addLike(inputVal) + ' OR r_patient_id__r.prov_pat_id__c LIKE ' + addLike(inputVal) + ' OR r_patient_id__r.CNS_ID__c LIKE ' + addLike(inputVal);

        if(tokens.size() == 1)
        {
            whereStr += ' OR r_patient_id__r.first_name__c LIKE ' + addLike(inputVal) + ' OR r_patient_id__r.Name LIKE ' + addLike(inputVal);
        }
        else
        {
            whereStr += ' OR (r_patient_id__r.first_name__c LIKE ' + addLike(tokens[0]) + ' AND r_patient_id__r.Name LIKE ' + addLike(tokens[1]) + ') ';
            whereStr += ' OR (r_patient_id__r.first_name__c LIKE ' + addLike(tokens[1]) + ' AND r_patient_id__r.Name LIKE ' + addLike(tokens[0]) + ') ';
        }

        String orderBy = ' ORDER BY r_patient_id__r.Name, Name DESC ';

        return Database.query(baseQuery + whereStr + orderBy + rLimit);
    }
    
    public List<r_reeg__c> getByPatientId(String idInp)
    {
    	string whereStr = ' r_patient_id__c = \'' + idInp + '\'';
		return (List<r_reeg__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, '', '500');
    } 
    
    public List<r_reeg__c> getCorrCompleteByPatientId(String idInp)
    {
    	string whereStr = ' r_patient_id__c = \'' + idInp + '\' AND stat_is_corr__c = true';
		return (List<r_reeg__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, '', '500');
    }
    
    public List<r_reeg__c> getRelatedTests(String patientId)  
    {
    	string orderString = 'rpt_date__c';
        string whereStr = ' r_patient_id__c = \'' + patientId + '\' AND stat_is_corr__c = true';	
        return (List<r_reeg__c>) getSObjectListByWhere(getFieldStr(),NAME,whereStr,orderString,'500');
    }
     
    
    public List<reegDisp> getListForTech(rEEGPager pager)
    {
    	string fields = getFieldStr() + ', RecordType.Id, RecordType.Name, r_patient_id__r.anticipated_eeg_test__c,r_patient_id__r.Name';
    	string whereStr = (!e_StringUtil.isNullOrEmpty(pager.techId)) ? ' 	r_eeg_tech_contact__c = \'' + pager.techId + '\'' : '';
    	if (!e_StringUtil.isNullOrEmpty(pager.listCategory))
    	{
    		if (pager.listCategory != 'All')
    		{
    			if (!e_StringUtil.isNullOrEmpty(whereStr))
    				whereStr += ' AND';
    			    		
    			whereStr += ' eeg_rec_stat__c = \'' + pager.listCategory + '\'';
    		}
    	}
    	
    	List<r_reeg__c> lst = (List<r_reeg__c>)getSObjectListByWhere(fields, NAME, whereStr, pager.CurrSort, '500');
    	reegDisp dispObj;

    	List<reegDisp> returnList = new List<reegDisp>();
    	if (lst != null && lst.size() > 0)
    	{
    		pager.setRecordCount(lst.size());
	    	for (r_reeg__c row : lst)
	    	{
	    		if (pager.shouldAddRow()) returnList.Add( new reegDisp(row)); 
	    	}
    	}

		return returnList;
    } 
    
    public List<reegDisp> getListForClinic(rEEGPager pager)
    {
    	string fields = getFieldStr() + ', RecordType.Id, RecordType.Name, r_patient_id__r.anticipated_eeg_test__c,r_patient_id__r.Name';
    	string whereStr = 'r_patient_id__r.physician__r.Account.Name = \'' + pager.accountName + '\'';
    	if (!e_StringUtil.isNullOrEmpty(pager.listCategory))
    	{
    		if (pager.listCategory != 'All')
    		{
    			if (!e_StringUtil.isNullOrEmpty(whereStr))
    				whereStr += ' AND';
    			    		
    			whereStr += ' eeg_rec_stat__c = \'' + pager.listCategory + '\'';
    		}
    	}
    	
    	List<r_reeg__c> lst = (List<r_reeg__c>)getSObjectListByWhere(fields, NAME, whereStr, pager.CurrSort, '500');
    	reegDisp dispObj;
    	List<reegDisp> returnList = new List<reegDisp>();
    	if (lst != null && lst.size() > 0)
    	{
    		pager.setRecordCount(lst.size());
	    	for (r_reeg__c row : lst)
	    	{
	    		if (pager.shouldAddRow()) returnList.Add( new reegDisp(row)); 
	    	}
    	}
		return returnList;
    } 
    
    public List<r_reeg__c> getByPhyId(String phyId, DateTime dt)
    {
    	List<r_reeg__c> reegList = null;
    	
    	String whereStr = ' r_physician_id__c = \'' + phyId + '\'';
    	
    	whereStr += ' and ' + setReegDateField(getMonthSpanSql(dt));
    	
    	reegList = (List<r_reeg__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, '', '500');

		return reegList;	
    }
    
    public List<r_reeg__c> getByMonthYear(integer month, integer year)
    {
    	DateTime dt = DateTime.newInstance(year, month, 1);
    	
    	string whereStr = setReegDateField(getMonthSpanSql(dt));
    	
    	return (List<r_reeg__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'name', '500');
    } 
    
    public List<r_reeg__c> getMostRecent(string numRec)
    {
    	string fullFieldStr = getFieldStr();
    	fullFieldStr += ', RecordType.Id, RecordType.Name, r_patient_id__r.first_name__c, r_patient_id__r.name, r_patient_id__r.CNS_ID__c, r_patient_id__r.dob__c, r_patient_id__r.Gender__c, r_patient_id__r.physician__c';
    	
		return getSObjectListByWhere(fullFieldStr, NAME, '', 'LastModifiedDate DESC', numRec);
    }
    
    public Boolean hasPrevTest(String patientId)
    {  
    	List<r_reeg__c> tests = new List<r_reeg__c>();
    	try
    	{ 
    	    tests = [select Id from r_reeg__c where r_patient_id__c =: patientId AND req_stat__c =: 'Complete' limit 1];
    	}
    	catch(Exception e) 
    	{
    		system.debug('rEEGDao: hasPrevTest: ' + e);
    	}
    	return tests.size() > 0;
    }
    
    private string setReegDateField(string sql)
    {
    	return sql.replaceAll('CreatedDate', 'eeg_rec_date__c')	;
    }
    
    public boolean Save(r_reeg__c reeg, string dbUpdateType)
    {
    	return Save(reeg, dbUpdateType, '');
    }
    
    public boolean SavePEEROnly(r_reeg__c reeg, string dbUpdateType)
    {
    	return SavePEEROnly(reeg, dbUpdateType, '');
    }

    public boolean Save(r_reeg__c reeg, string dbUpdateType, string userId)
    {
    	boolean returnVal = false;
    	returnVal = saveSObject(reeg);
    	
    	debug('### reegSaveSObject:' + reeg);
    	debug('### reegSaveSObject:dbUpdateType:' + dbUpdateType);
    	
    	if (!e_StringUtil.isNullOrEmpty(dbUpdateType))
    	{	
    		Boolean isPI = false;
    		Boolean isPO2 = false;
    		if ((!e_StringUtil.isNullOrEmpty(reeg.r_physician_id__c)) && reeg.r_physician_id__r.Account != null)
    		{
    			isPI = (reeg.r_physician_id__r.Account.PEER_Interactive__c != null &&  reeg.r_physician_id__r.Account.PEER_Interactive__c);
    			isPO2 = (!e_StringUtil.isNullOrEmpty(reeg.r_physician_id__r.Account.Report_Build_Type__c) &&  reeg.r_physician_id__r.Account.Report_Build_Type__c == 'PEER Online 2.0');
    		}
    			
    		if (!e_StringUtil.isNullOrEmpty(userId))
    		{
    			if (isPI || isPO2 || reeg.is_ng_only__c)
    				ActionDao.insertReegActionPEER(reeg.Id, dbUpdateType, userId);
    			else
    				ActionDao.insertReegAction(reeg.Id, dbUpdateType, userId);
    		}
    		else
    		{
    			if (isPI || isPO2 || reeg.is_ng_only__c)
    				ActionDao.insertReegActionPEER(reeg.Id, dbUpdateType);
    			else
    				ActionDao.insertReegAction(reeg.Id, dbUpdateType);
    		}
    	}
    	
    	return returnVal;
    }
    
    public boolean SavePEEROnly(r_reeg__c reeg, string dbUpdateType, string userId)
    {
    	boolean returnVal = false;

        debug('### SavePEEROnly:eeg_rec_date__c: ' + reeg.eeg_rec_date__c);

    	returnVal = saveSObject(reeg);

        debug('### SavePEEROnly:returnVal: ' + returnVal);
    	
    	if (!e_StringUtil.isNullOrEmpty(dbUpdateType))
    	{
    		if (!e_StringUtil.isNullOrEmpty(userId))
    			ActionDao.insertReegActionPEER(reeg.Id, dbUpdateType, userId);
    		else
    			ActionDao.insertReegActionPEER(reeg.Id, dbUpdateType);
    	}
    	
    	return returnVal;
    }
    
    public String getNameSearchQuery(String firstLastName)
    {
        firstLastName = escapeStr(firstLastName);
        List<String> tokens = firstLastName.split(' ',2);
         
        String whereStr = 'where ';
        String baseQuery = 'SELECT Id,Name, is_ng_only__c, RecordType.Id, RecordType.Name, neuroguide_status__c, Process_Used_For_Test__c, rpt_stat__c, req_stat__c, req_date__c, rpt_date__c, r_physician_id__r.Name, r_physician_id__r.Account.Report_Build_Type__c, r_patient_id__c, r_patient_id__r.Name, r_patient_id__r.middle_initial__c, r_patient_id__r.first_name__c, r_patient_id__r.dob__c, r_patient_id__r.prov_pat_id__c FROM r_reeg__c ';
        
        if(!e_StringUtil.isNullOrEmpty(filter)) whereStr += filter + ' AND ';
        
        if(tokens.size() == 1)
        {
            whereStr += '( r_patient_id__r.prov_pat_id__c = ' + quote(firstLastName) + ' OR r_patient_id__r.Name like '+ addLike(firstLastName) + ' OR r_patient_id__r.first_name__c like ' + addLike(firstLastName) + ') order by r_patient_id__r.Name';
        }
        else
        {
            String whereStr1 = '( (r_patient_id__r.Name like '+ addLike(tokens[0]) + ' AND r_patient_id__r.first_name__c like ' + addLike(tokens[1]) + ') ';
            String whereStr2 = 'OR (r_patient_id__r.first_name__c like '+ addLike(tokens[0]) + ' AND r_patient_id__r.Name like ' + addLike(tokens[1]) + ')) order by r_patient_id__r.Name';
            
            
            whereStr += whereStr1 + whereStr2;
        }
        
        system.debug('RDAO NAME QUERY: ' + baseQuery + whereStr + ' limit 10000 ');
        
        return baseQuery + whereStr + ' limit 10000 ';
    }

     
    public String getMyPeerListQuery(String providerId,String limitStr, String ordStr)
    {
        return getMyPeerListQuery(providerId, limitStr, ordStr, false);
    }

    public String getMyPeerListQuery(String providerId,String limitStr, String ordStr, Boolean isAdminOrPortalUserMgr)
    {
        String query = '';
        String baseQuery = 'SELECT Id, Name, is_ng_only__c, RecordType.Id, RecordType.Name, neuroguide_status__c, Process_Used_For_Test__c, rpt_stat__c, req_stat__c, req_date__c, rpt_date__c, r_physician_id__r.Name, r_patient_id__r.physician__r.AccountId, r_physician_id__r.Account.Report_Build_Type__c, r_patient_id__c, r_patient_id__r.Name, r_patient_id__r.middle_initial__c, r_patient_id__r.first_name__c, r_patient_id__r.dob__c, r_patient_id__r.prov_pat_id__c FROM r_reeg__c ';
        
        String whereStr = '';
        system.debug('FILTER: ' + filter);
        if(!e_StringUtil.isNullOrEmpty(filter))
        {
            whereStr += ' WHERE ' + filter;
        }

        whereStr += ' AND r_patient_id__r.CA_Control_Group_Type__c != \'Control Group\' AND reeg_type__c != \'pre-Washout\'';
        
        query = baseQuery + whereStr;

        if(!e_StringUtil.isNullOrEmpty(ordStr)) query += ' ' + ordStr + ' ';
        else query += ' order by req_date__c DESC ';
        //-- jdepetro - Mike requested that the sorting be done by req date and not lastmodifieddate
        //else query += ' order by LastModifiedDate DESC';
        
        if(!e_StringUtil.isNullOrEmpty(limitStr)) query += ' ' + limitStr + ' ';
        
        //TODO - respect filter
        
        system.debug('RDAO QUERY: ' + query);
        return query;
    }

    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testrEEGDao()
    {
        rEEGDao dao = new rEEGDao();
        dao.IsTestCase = true;
      //  System.assert(!e_StringUtil.isNullOrEmpty(rEEGDao.getInstance().getFieldStr()));
        
        rEEGControllerExt rCont = new rEEGControllerExt();
        
        //r_reeg__c testrEEG = rEEGDao.getTestReeg();
        //testrEEG = dao.getById(testrEEG.Id);
        //List<r_reeg__c> reegList = dao.getByPatientId(testrEEG.r_patient_id__c);
        //reegList = dao.getCorrCompleteByPatientId(testrEEG.r_patient_id__c);
        
        DateTime dt = DateTime.now();
	//	reegList = dao.getByMonthYear(dt.month(), dt.year());

    //    testrEEG = dao.getFieldsById('Id, name', testrEEG.Id);
    }  
    
    public static r_reeg__c getTestReeg()
    {
    	r_patient__c testPat = patientDao.getTestPat();
    	
    	r_reeg__c testReeg = new r_reeg__c();
    	testReeg.r_patient_id__c = testPat.Id;
    	testReeg.reeg_type__c = 'Type I';
    	
    	insert testReeg;
    	return testReeg;
    }

    public static r_reeg__c getTestReegPO2()
    {
        r_patient__c testPatPO2 = patientDao.getTestPatPO2();   
        r_reeg__c testReegPO2 = new r_reeg__c();
        testReegPO2.r_patient_id__c = testPatPO2.Id;
        testReegPO2.reeg_type__c = 'Type I';
        
        insert testReegPO2;
        return testReegPO2;
    }
    
  
    
}