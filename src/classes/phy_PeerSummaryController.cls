public with sharing class phy_PeerSummaryController extends BaseDrugReportController
{
    public string drugName {get; set;}
    public string BrainMapUrl {get; set;}
    public boolean IsNeuroRptAvail {get; set;}
    public boolean IsClassicReportOnly {get; set;}
    public String printUrl {get; set;}

    public phy_PeerSummaryController()
    {
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        if (e_StringUtil.isNullOrEmpty(reegId))
            this.reegId = ApexPages.currentPage().getParameters().get('id');
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        rDao = new rEegDao();
       
    }
    
    public phy_PeerSummaryController(ApexPages.StandardController stdController)
    {
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        if (e_StringUtil.isNullOrEmpty(reegId))
            this.reegId = ApexPages.currentPage().getParameters().get('id');
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        rDao = new rEegDao();
    }
    
    public PageReference loadAction()
    {
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            this.reeg = rDao.getById(reegId, true);
            
            if (reeg != null)
            {
                if(this.reeg.req_stat__c != 'Complete')
                {
                	PageReference pg = new PageReference('/apex/phy_PeerViewS2?id=' + this.reeg.Id);
                	pg.setRedirect(true);
                	return pg;
                }
            
                baseUrl = '/apex/phy_PeerReportS2?rid=' + reeg.Id;
                cnsVarsUrl = '/apex/phy_CNSVarsS2?rid=' + reeg.Id + '&ctid=' + reeg.corr_cns_id__c;
                baseReportTabbedUrl = '/apex/phy_PeerReportS2?rid=' + reeg.Id;
                BrainMapUrl = '/apex/PEERBrainMapView?rid=' + reeg.id;
                IsClassicReportOnly = ((e_StringUtil.isNullOrEmpty(reeg.neuroguide_status__c) || (reeg.neuroguide_status__c == 'NEW')) && !e_StringUtil.isNullOrEmpty(reeg.rpt_stat__c) && (reeg.rpt_stat__c == 'Complete' || reeg.rpt_stat__c == 'Delivered'));
            }

            if (!e_StringUtil.isNullOrEmpty(pageId))
            {
                DrugTreeMenuUtil.MenuItem menuItem = menuMap.get(pageId);
                if (menuItem != null)
                    drugName = menuItem.drugName;
            }
        }
        else
        {
            this.reeg = new r_reeg__c();
        }  

        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        printUrl = getPrintViewPage();
        return null;    
    }
    
    public PageReference returnAction()
    {
        PageReference pr = new PageReference('/apex/phy_PeerListS2');
   
        
        return pr;
    }
    
    //---temporary
    public String getPrintViewPage()
    {
    
        String key = '';
        string versionTxt = '';
        if (!e_StringUtil.isNullOrEmpty(this.reegId))
        {
            String part1 = this.reegId.substring(5, 10);
            String part2 = this.reegId.substring(0, 5);
            String part3 = this.reegId.substring(10, reegId.length());
            key = '6H3a2' + part1 + 's' + part2 + part3;
        }
        
        return Domain + '/Public/Download.aspx?key=' + key + '&v=1';
  
    }
    
    
    //--------- TEST METHODS ----------------------
    
    public static testmethod void testController()
    {
    	phy_PeerSummaryController psc = new phy_PeerSummaryController();
    	psc.returnAction();
    	psc.loadAction();
    	
    	
    }

}