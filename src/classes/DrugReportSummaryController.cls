//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: DrugReportSummaryController
// PURPOSE: Controller class for the Drug Report Summary page
// CREATED: 06/10/11 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class DrugReportSummaryController extends BaseDrugReportController
{
	public string drugName {get; set;}

	public DrugReportSummaryController()
    {
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
    	if (e_StringUtil.isNullOrEmpty(reegId))
    		this.reegId = ApexPages.currentPage().getParameters().get('id');
    	this.pageId = ApexPages.currentPage().getParameters().get('pgid');
    	rDao = new rEegDao();
    }
    
    public DrugReportSummaryController(ApexPages.StandardController stdController)
    {
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
    	if (e_StringUtil.isNullOrEmpty(reegId))
    		this.reegId = ApexPages.currentPage().getParameters().get('id');
    	this.pageId = ApexPages.currentPage().getParameters().get('pgid');
    	rDao = new rEegDao();
    }
    
    public PageReference loadAction()
    {
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
   			this.reeg = rDao.getById(reegId);
   			baseUrl = '/apex/DrugReport?rid=' + reeg.Id;
   			cnsVarsUrl = '/apex/DrugReportCnsVar?rid=' + reeg.Id + '&ctid=' + reeg.corr_cns_id__c;
   			
   			if (!e_StringUtil.isNullOrEmpty(pageId))
   			{
   				DrugTreeMenuUtil.MenuItem menuItem = menuMap.get(pageId);
   				if (menuItem != null)
   					drugName = menuItem.drugName;
   			}
    	}
		else
		{
			this.reeg = new r_reeg__c();
		}    
		
		return null;	
    }
    
    public PageReference returnAction()
    {
    	PageReference pr = new PageReference('/a0h/o');
   // 	AddParameters(pr);
    	
    	return pr;
    }
    
	private static testmethod void testController()
	{
	    DrugReportSummaryController dc = new DrugReportSummaryController();
	    dc.returnAction();	
	
	}

}