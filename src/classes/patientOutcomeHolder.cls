//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: patientOutcomeHolder
//   PURPOSE: Holder class for the outcome and the associated outcome meds.
//     OWNER: CNSR
//   CREATED: 07/5/10 jdepetro Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class patientOutcomeHolder 
{
	public r_patient_outc__c Outcome {get; set;}
	public List<r_outcome_med__c> OutcomeMeds {get; set;}
	public Date MedChangeDate {get; set;}
	public integer prevMedsNotFound {get; set;}
	public integer newMedsFound {get; set;}
	
	public Date intervalStartDate()
	{
		return Outcome.r_interval__r.start_date__c;
	}
	
	public Date intervalStopDate()
	{
		return Outcome.r_interval__r.stop_date__c;
	}
	
	public Date outcomeDate()
	{
		return Outcome.outcome_date__c;
	}
	
 	//---------------TEST METHODS ---------------------------------
    //
    //-------------------------------------------------------------
    public static testMethod void testController()
    {
    	patientOutcomeHolder patOutHolder = new patientOutcomeHolder();
    	
    	patientOutcomeDao outcDao = new patientOutcomeDao();
    	patOutHolder.Outcome = outcDao.getTestOutcome();
    	patientOutcomeMedDao patOutMedDao = new patientOutcomeMedDao();
    	
    	r_outcome_med__c outMed = patOutMedDao.getTestOutcomeMed();
        if (outMed != null)
        	patOutHolder.OutcomeMeds = patOutMedDao.getByOutcome(outMed.r_outcome_id__c);
        patOutHolder.MedChangeDate = Date.today();	
        
        Date testDt = patOutHolder.intervalStartDate();
        testDt  = patOutHolder.intervalStopDate();
        testDt  = patOutHolder.outcomeDate();
    	
        System.assertEquals( 1, 1);
    }
}