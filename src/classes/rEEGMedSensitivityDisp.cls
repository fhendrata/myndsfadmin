public class rEEGMedSensitivityDisp
{
    private String tradeName;
    public String getTradeName()
    {
        return tradeName;
    }
    public void setTradeName(String s)
    {
        tradeName = s;
    }
    
    public String getTradeStyle()
    {
        String style = 'padding-left:3px;padding-right:3px';
        if (subGroupName != null)
        {   
            if (subGroupName == 'GROUP')
            {
                style = 'padding-left:3px;padding-right:3px;font-weight: bold';
            }
            else
            {
                style = 'padding-left:40px;padding-right:3px';
            }
        }
               
        return style;
    }
        
    private String genericName;
    public String getGenericName()
    {
        if (subGroupName == 'GROUP')
        {
            return '';
        }
        
        return genericName;
    }
    public void setGenericName(String s)
    {
        genericName= s;
    }
    
    public String getGenericStyle()
    {
        return '';
    }
    
    private String sensitivity;
    public String getSensitivity()
    {
        return sensitivity;
    }
    public void setSensitivity(String s)
    {
        sensitivity= s;
    }
    
    public String getSensitivityStyle()
    {
        String style = '';
        if (subGroupName != null && subGroupName != 'GROUP')
        {   
            style = 'padding-left:30px;padding-right:3px;';
            return style + rEEGStyleUtil.getSensitivityStyle(subGroupSensitivity);
        }
        else
        {
            style = 'padding-left:3px;padding-right:3px;';
            return style + rEEGStyleUtil.getSensitivityStyle(sensitivity);
        }
    }
    
    public String getGrpStyle()
    {
        return 'style="padding-left:10px';
    }
    
    
    private String subGroupName;
    public String getSubGroupName()
    {
        return subGroupName;
    }
    public void setSubGroupName(String s)
    {
        subGroupName = s;
    }
    
    private String subGroupSensitivity;
    public String getSubGroupSensitivity()
    {
        return subGroupSensitivity;
    }
    public void setSubGroupSensitivity(String s)
    {
        subGroupSensitivity = s;
    }
            
    public void Setup(r_reeg_med__c med)
    {
        tradeName = med.trade__c;
        genericName = med.generic__c;
        sensitivity = med.sensitivity__c;
        subGroupName = med.sub_group_name__c;
        subGroupSensitivity = med.sub_group_sensitivity__c;
    }

    //------------TEST-----------------------------
    public static testMethod void testDisp()
    {
        r_reeg_med__c obj = new r_reeg_med__c();
        obj.trade__c = 'z';
        obj.generic__c = 'z';
        obj.sensitivity__c = 'z';
        obj.sub_group_name__c = 'z';
        obj.sub_group_sensitivity__c = 'z';

        rEEGMedSensitivityDisp disp = new rEEGMedSensitivityDisp();
        disp.Setup(obj);

 //       System.assertEquals(disp.getTradeName(), 'z');
        System.assertEquals(disp.getGenericName(), 'z');
        System.assertEquals(disp.getSensitivity(), 'z');
        System.assertEquals(disp.getSubGroupName(), 'z');
        System.assertEquals(disp.getSubGroupSensitivity(), 'z');

        disp.setTradeName('x');
        disp.setGenericName('x');
        disp.setSensitivity('x');
        disp.setSubGroupName('x');
        disp.setSubGroupSensitivity('x');

        System.assertEquals(disp.getTradeName(), 'x');
        System.assertEquals(disp.getTradeStyle(), 'padding-left:40px;padding-right:3px');
        System.assertEquals(disp.getGenericName(), 'x');
        System.assertEquals(disp.getGenericStyle(), '');
        System.assertEquals(disp.getSensitivity(), 'x');
        System.assertEquals(disp.getSensitivityStyle(), 'padding-left:30px;padding-right:3px;color: black');
        System.assertEquals(disp.getGrpStyle(), 'style="padding-left:10px');
        System.assertEquals(disp.getSubGroupName(), 'x');
        System.assertEquals(disp.getSubGroupSensitivity(), 'x');

        System.assertEquals( 1, 1);
    }
}