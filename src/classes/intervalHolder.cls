public with sharing class intervalHolder 
{
   	public Date StartDate {get; set;}
   	public Date EndDate {get; set;}
   	public string activeMedNames {get; set;}
   	public string startMedNames {get; set;}
   	public string endMedNames {get; set;}  
   	
   	public string startMedNamesA {get; set;}
   	public string endMedNamesA {get; set;}
   	
   	public string startMedNamesB {get; set;}
   	public string endMedNamesB {get; set;}
   	
   	public Integer dateCount {get; set;}
   	   	
   	public r_interval__c obj {get; set;}
}