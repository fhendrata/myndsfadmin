public with sharing class phy_PeerListController extends BaseController
{
    private string searchCriteria {get; set;}
    private integer resultTableSize {get; set;}
    public string newFilterId {get; set;}
    public ApexPages.StandardSetController con {get; set;}

    public Boolean hasNext { get { return con.getHasNext(); } set; }
    public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }
    public Boolean hasResults { get { return (resultNumber > 0); } set; }
    public Integer pageNumber  { get { return con.getPageNumber(); } set; }
    public Integer resultNumber  { get { return con.getResultSize(); } set; }
    public void previous() { con.previous(); }
    public void next() { con.next(); }
    public String patSearchString {get; set;}

    public Id userID { get; set; }
    public User currentUser { get; set; }
    public User_Agreement__c currentUserAgreement { get; set; }

    private rEEGDao rDao;

    public phy_PeerListController()
    {
        rDao = new rEEGDao();
        resultTableSize = RESULT_TABLE_SIZE;
        setupFilter();
        loadPeers();
    }

    public PageReference loadAction()
    {
        PageReference pr = checkProfileForRedirect();
        if (pr != null)
            return pr;

        pr = checkTOU();
        if (pr != null)
            return pr;

        return null;
    }

    private void setupFilter()
    {
        rEEGDao.getInstance().filter = '';

        //-- jdepetro.3.22.17 Patient sharing is now done by sharing rules that are set by portal roles. These rules can go across the account hierarchy is set for that purpose. 
        //if(getIsClinicAdmin()) rEEGDao.getInstance().filter = 'r_patient_id__r.physician__r.AccountId = ' + quote(getCurrentAccount());

        if (String.isNotBlank(rEEGDao.getInstance().filter)) rEEGDao.getInstance().filter += ' AND ';
        rEEGDao.getInstance().filter += ' (NOT req_stat__c LIKE \'not valid%\')';
        //-- jdepetro.8.26.14 - this was eliminating the patient sharing between dr's. The clinic admin is still not allowed patient sharing.
       // else rDao.filter = 'r_patient_id__r.physician__c = ' + quote(getCurrentPhysician());
    }


    private PageReference loadPeers()
    {
    	String query = rEEGDao.getInstance().getMyPeerListQuery(getCurrentPhysician(), ' limit 10000 ', '', getIsAdminOrPortalUserMgr());
        System.debug('### loadPeers:query: ' + query);
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
        return null;
    }

    public List<reegDisp> getPeers()
    {
    	List<reegDisp> reegDispList = null;
        if(con != null)
        {
        	List<r_reeg__c> reegList = (List<r_reeg__c>)con.getRecords();

        	if (reegList != null && reegList.size() > 0)
        	{
        		reegDispList = new List<reegDisp>();
        		reegDisp newObj;
        		for (r_reeg__c row : reegList)
        		{
        			newObj = new reegDisp(row);
        			reegDispList.add(newObj);
        		}
        	}
        }

        return reegDispList;
    }

    public void searchPatients()
    {
        con = new ApexPages.StandardSetController(rDao.getByInputWildcard(patSearchString, ' LIMIT 10000'));
        con.setPageSize(resultTableSize);
    }

    public PageReference checkTOU()
    {
        PageReference pr = null;
        userID = UserInfo.getUserId();
        
        if (String.isNotBlank(userID))
        {
            UserDao dao = new UserDao();
            currentUser = dao.getById(userID);

            UserAgreementDao uDao = new UserAgreementDao();
            currentUserAgreement = uDao.getByUser(currentUser);

            if (!currentUserAgreement.Has_Accepted_TOU__c)
            {
                pr = new PageReference('/apex/PEERTermsOfUse?retURL=/apex/phy_PEERListS2');
                pr.setRedirect(true);
            }
        }
        return pr;
    }


    //-------- TEST METHODS ----------------


    private static testmethod void testController()
    {
    	phy_PeerListController plc = new phy_PeerListController();
    	plc.patSearchString = 'a';
    	//plc.searchPatients();
    	//plc.getPeers();
    }

}