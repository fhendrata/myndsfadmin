//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: PEERController
// PURPOSE: Base controller class for the PEER Report view page
// CREATED: 05/1/14 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class PEERController extends BaseController
{
    public String PEERType { get; set; }
    public String PatientName { get; set; }
    public String PhysicianName { get; set; }
    public String EEGTechName { get; set; }
    public Boolean NeuroOrdered { get; set; }
    private r_reeg__c reeg { get; set; }

    public PEERController(ApexPages.StandardController stdController)
    {
        this.reeg = (r_reeg__c)stdController.getRecord();
    }

    public PageReference loadAction()
    {
        NeuroOrdered = false;
        rEEGDao reegDao = new rEEGDao();
        reeg = reegDao.getByIdWithPatPhyTechFields(reeg.Id);

        if ( (!rEEGUtil.isStudyReport(reeg) && !rEEGUtil.isPO2Report(reeg)) || (!reeg.is_ng_only__c && !reeg.use_ng_process__c && String.isBlank(reeg.neuroguide_status__c)) )
        {
            PageReference pr = Page.rEEGViewPage;
            pr.getParameters().put('id',reeg.id);
            return pr;
        }


        PEERType = clearNull(reeg.reeg_type__c) + clearNull(reeg.reeg_type_mod__c);
        PatientName = clearNull(reeg.r_patient_id__r.first_name__c) + ' ' + clearNull(reeg.r_patient_id__r.name);
        PhysicianName = clearNull(reeg.r_physician_id__r.name);
        EEGTechName = clearNull(reeg.r_eeg_tech_contact__r.name);
        //NeuroOrdered = (reeg.do_not_send_neuro__c != null && reeg.do_not_send_neuro__c) ? false : true;
        NeuroOrdered = !reeg.do_not_send_neuro__c;

        return null;
    }

    public PageReference adminPage()
    {
        PageReference pr = Page.PEERAdmin;
        pr.getParameters().put('id', reeg.Id);
        return pr;
        
        return null;
    }

    public PageReference editPEERPage()
    {
        PageReference pr = Page.PEEREdit;
        pr.getParameters().put('id', reeg.Id);
        return pr;
        
        return null;
    }

    public Boolean getShowPEEREdit()
    {
        Profile profile = getProfile();
        return (profile != null && (profile.Name == 'Accounting' || profile.Name == 'CNSR Sales Director' || profile.Name == 'CNSR System Administrator' || profile.Name == 'Non Lightning System Administrator'));
    }

    public String getEegtechHref()
    {
        return URL.getSalesforceBaseUrl().toExternalForm() + '/' + reeg.r_eeg_tech_contact__c;
    }

    public String getPhysicianHref()
    {
        return URL.getSalesforceBaseUrl().toExternalForm() + '/' + reeg.r_physician_id__c;
    }

    public String getPatientHref()
    {
        return URL.getSalesforceBaseUrl().toExternalForm() + '/' + reeg.r_patient_id__c;
    }

    public PageReference CancelReegPatReq()
    {
        reeg.req_stat__c = 'Not Valid - Patient stopped process';
        SaveReeg(reeg, 'UPDATE_DB_STATUS');
        
 /*
        PageReference pr = Page.PEERView;
        pr.getParameters().put('id', reeg.Id);
        return pr;
*/

        return null;
    }

    //------------------------------------Test list -------------------------------------------------

     //---Get all the tests for this patient
    private List<rEEGTestDisp> tests;   
    public List<rEEGTestDisp> getTests()
    {
         if (tests == null)
         {
             tests = new List<rEEGTestDisp>();  
             
             if (reeg != null)
             {
                for(r_reeg__c reegRow : 
                     [select id, name, reeg_type__c, req_date__c, r_patient_id__c
                     from r_reeg__c
                     where r_patient_id__c = :reeg.r_patient_id__c AND id != :reeg.id 
                            AND req_date__c < :reeg.req_date__c
                            AND req_stat__c = 'Complete'
                     order by req_date__c])
                {
                    rEEGTestDisp reegTestDisp = new rEEGTestDisp();
                    reegTestDisp.Setup(reegRow);                                 
                    tests.Add(reegTestDisp);
                } 
            }     
        }    
        return tests;
    }

    //-------------------------------------DX items -------------------------------------------------
    
    public PageReference dxListEditPage()
    {
        PageReference pr = new PageReference('/apex/rEEGDxListEditPage');
        pr.getParameters().put('rid',reeg.id);
        return pr;
    }
    
    private List<r_reeg_dx__c> rEEGDxs;  
    public List<r_reeg_dx__c> getrEEGDxs()
    {
        rEEGDxs = new List<r_reeg_dx__c>();  
    
        for(r_reeg_dx__c dxRow : 
             [select dx_name__c, primary__c, r_ref_dx_id__c, rule_out__c, secondary__c, id, name 
                 from r_reeg_dx__c
                 where r_reeg_id__c = :reeg.id])
        {
            rEEGDxs.Add(dxRow);
        } 
        
        return rEEGDxs;
    }

    // -------- Status table methods -------------------

    public String getShowNeuroLink()
    {
        return (reeg.neuro_stat__c == 'Complete') ? 'visible' : 'hidden';
    }

    public String getShowEEGLink()
    {
        return ((reeg.eeg_rec_stat__c == 'Received' || reeg.eeg_rec_stat__c == 'Processed')) ? 'visible' : 'hidden';
    }

    public String getShowArtLink()
    {
        return (reeg.art_status__c == 'Complete') ? 'visible' : 'hidden';
    }

    public String getReportUrl()
    {
        String returnVal = 'phy_PeerSummaryS2';
        if (!e_StringUtil.IsNullOrEmpty(reeg.Process_Used_For_Test__c))
        {
            if (rEEGUtil.isWRMilitaryStudyReport(reeg))
                returnVal = 'wr_PeerSummaryS2';
            else if (reeg != null && (rEEGUtil.isPO2Report(reeg) || rEEGUtil.isStudyReport(reeg)))
                returnVal = 'peer_Summary2';
        }

        return returnVal;
    }

    public String getStatusLightEEGSrc()
    {
        String returnVal = getRedSrc();
        if (!String.isBlank(reeg.eeg_rec_stat__c))
        {            if (reeg.eeg_rec_stat__c.toLowerCase() == 'received') returnVal = getGreenSrc();
            else if (reeg.eeg_rec_stat__c.toLowerCase() == 'in progress') returnVal = getYellowSrc();
        }

        return returnVal;
    }

    public String getStatusLightNeuroSrc()
    {
        String returnVal = getRedSrc();
        if (!String.isBlank(reeg.neuro_stat__c))
        {
            if (reeg.neuro_stat__c.toLowerCase() == 'complete') returnVal = getGreenSrc();
            else if (reeg.neuro_stat__c.toLowerCase() == 'in progress' || reeg.neuro_stat__c.toLowerCase() == 'pending') returnVal = getYellowSrc();
        }

        return returnVal;
    } 

    public String getStatusLightArtSrc()
    {
        String returnVal = getRedSrc();
        if (!String.isBlank(reeg.art_status__c))
        {
            if (reeg.art_status__c.toLowerCase() == 'complete') returnVal = getGreenSrc();
            else if (reeg.art_status__c.toLowerCase() == 'in progress') returnVal = getYellowSrc();
        }

        return returnVal;
    }  

    public String getStatusLightReportSrcNG()
    {
        String returnVal = getRedSrc();
        if (!String.isBlank(reeg.neuroguide_status__c))
        {
            if (reeg.neuroguide_status__c.toLowerCase() == 'report complete') returnVal = getGreenSrc();
            else if (reeg.neuroguide_status__c.toLowerCase() == 'new') returnVal = getYellowSrc();
        }

        return returnVal;
    }    

    public String getShowOnlineReportLink()
    {
        return (showOnlineReport()) ? 'visible' : 'hidden';
    }

    public String getDownloadEEGHref()
    {
        return getDownloadHref('1');
    }

    public String getDownloadArtHref()
    {
        return getDownloadHref( '2');
    }
    
    public String getDownloadNeuroHref()
    {
        return getDownloadHref( '3');
    }

    private String getDownloadHref(String type1)
    {
        return rEEGUtil.getDownloadUrl(reeg.Id, type1);
    }

    private boolean showOnlineReport()
    {
        return (!e_StringUtil.isNullOrEmpty(reeg.neuroguide_status__c) && reeg.neuroguide_status__c == 'Report Complete');
    }

    private String getGreenSrc()
    {
        return 'greenStatus';        
    }
    private String getYellowSrc()
    {
        return 'yellowStatus';       
    }
    private String getRedSrc()
    {
        return 'redStatus';
    }

    private String clearNull(String inputVal)
    {
        return (String.isNotBlank(inputVal)) ? inputVal : '';
    }

    public void SaveReeg(r_reeg__c reeg)
    {
        SaveReeg(reeg, '');
    }

    public void SaveReeg(r_reeg__c reeg, string statusType)
    {
        rEEGDao rDao = new rEEGDao();
        rDao.Save(reeg, statusType);
    }

}