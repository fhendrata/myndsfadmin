//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: ReferenceDrugController
//   PURPOSE: Reference Drug list page controller
//     OWNER: CNSR
//   CREATED: 8/2/11 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class ReferenceDrugController extends BaseController
{
  public boolean isLoggedIn { get; set; }
  public boolean loginError { get; set; }
  public string username {get; set;}
  public string password {get; set;}
  private static RefDrug__c REF_DRUG_CREDS = RefDrug__c.getInstance('ReferenceDrug');
  private integer resultTableSize {get; set;}
  public string newFilterId {get; set;}
  public Integer CurrentPage {get; set;}
  public Integer NextPage {get; set;}
  public Integer PreviousPage {get; set;}
  public String Letter {get; set;}
  
  public ApexPages.StandardSetController con {get; set;}
  
  public ReferenceDrugController()
  {
  }
  
  public PageReference loadAction()
  {
    resultTableSize = 8;
    isLoggedIn = false;
    
    try 
    {
      Profile p = [select Name from Profile where Id =: UserInfo.getProfileId() limit 1];
      String profileName = p.Name;
      profileName = profileName.toLowerCase();
      
      if(profileName.contains('phy') || profileName.contains('manager') ||  profileName.contains('tech') || profileName.contains('walter reed'))
      {
        isLoggedIn = true;
      }
    }
    catch(Exception e)
    {
      //silently fail for now
    }
  
    loginError = false;

    Letter = ApexPages.currentPage().getParameters().get('letter');
    String pageVal = ApexPages.currentPage().getParameters().get('pg');

    if (String.isNotBlank(pageVal))
    {
      CurrentPage = Integer.valueOf(pageVal);
    }
    else
    {
      CurrentPage = 1;
    }

    if (String.isNotBlank(Letter))
      loadByLetter(Letter + '%');
    else
      loadAll();

    NextPage = (con.getHasNext()) ? CurrentPage + 1 : currentPage;
    PreviousPage = (con.getHasPrevious()) ? CurrentPage - 1 : currentPage;
    
    return null;
  }

  public void loadByLetter(String searchCriteria)
  {
    con = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [SELECT Id, Name, generic__c, trade__c, half_life_days__c, half_life_hours__c, wo_required__c 
                    FROM ref_drug__c where name like :searchCriteria and is_deleted__c = false order by name]));
    con.setPageSize(resultTableSize);
    con.setPageNumber(CurrentPage);
  }
  
  public PageReference getByView()
  {
    //--9.15.11 jdepetro - left the filter logic in even though Brian does not want filters
    //con.setFilterId(newFilterId);
    con.setPageSize(resultTableSize);
    return null;
  }
  
  public List<ref_drug__c> getDrugs()
  {
    if(con != null)  
      return (List<ref_drug__c>)con.getRecords(); 
    else
      return null;
  }
  
  private PageReference loadAll()
  {
    con = new ApexPages.StandardSetController(Database.getQueryLocator(
                      [SELECT Id, Name, generic__c, trade__c, half_life_days__c, half_life_hours__c, wo_required__c 
                    FROM ref_drug__c where is_deleted__c = false order by name]));
        con.setPageSize(resultTableSize);
        con.setPageNumber(CurrentPage);
        
    return null;
  }
  
  public PageReference login()
  {
    isLoggedIn = (REF_DRUG_CREDS.username__c == username && REF_DRUG_CREDS.password__c == password);
    //isLoggedIn = ('cnsr' == username && 'PEER7337' == password);

    loginError = !isLoggedIn;
    
    system.debug('### isLoggedIn:' + isLoggedIn); 
    
    return null;
  }

  public Boolean hasNext { get { return con.getHasNext(); } set; }  
  public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }  
  public Boolean hasResults { get { return (resultNumber > 0); } set; }  
  public Integer pageNumber  { get { return con.getPageNumber(); } set; }
  public Integer resultNumber  { get { return con.getResultSize(); } set; }  
  
  
        //---TEST METHODS ------------------------------
  public static testMethod void testRefDrugController()
  {
      ReferenceDrugController cont = new ReferenceDrugController();
      cont.IsTestCase = true;
   
      cont.username = 'cnsr';
      cont.password = 'PEER7337';
     
      cont.login();
      boolean btest = cont.isLoggedIn;
      btest = cont.loginError;
      
      ref_drug__c testObj = ReferenceDrugDao.getTestRefDrug();
      PageReference pr = cont.loadAction();
      cont.getByView();
      
      ApexPages.currentPage().getParameters().put('letter', 'a');
      
      List<ref_drug__c> drugList =  cont.getDrugs();
      
      btest = cont.hasNext;
      btest = cont.hasPrevious;
      btest = cont.hasResults;
      Integer itest = cont.pageNumber;
      itest = cont.resultNumber;
      cont.newFilterId = 'test';
      
  }
    
}