//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: BaseWizardController
//   PURPOSE: Base controller class for the wizard pages/controllers
// 
//     OWNER: CNS Response
//   CREATED: 11/19/10 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
public abstract class BaseUploadWizController extends BaseController
{
	public r_patient__c patient { get; set; }
	public r_reeg__c reeg { get; set; }
	public r_patient_outc__c outcome { get; set; }
	public boolean isWiz {get; set;}
    public boolean isNew {get; set;}
    public boolean isNewPat {get; set;}
    public boolean isiPad {get; set;}
    public string reegId {get; set;}
    public string patId {get; set;}
    public String qsid {get; set;}
    public String tid {get; set;}
    public String outcomeId {get; set;}
    public boolean isWR {get; set;}
    
    public patientDao pDao { get; set; }
    public rEEGDao rDao { get; set; }
    public patientOutcomeDao outDao { get; set; }
    
    public virtual PageReference cancelWizAction()
    {
    	PageReference returnVal = null;
    	
    	if (isWiz)
    	{
    		if (pDao == null) pDao = new patientDao();
    		if (rDao == null) rDao = new rEEGDao();
    		
    		if (isNewPat)
    		{
    			if (patient == null && !e_StringUtil.isNullOrEmpty(patId))
    			{
    				patient = pDao.getById(patId);
	    		}
	    		
	    		if (patient != null)
	    			pDao.realDeleteSObject(patient);
    		}
    		
	        if (reeg == null && !e_StringUtil.isNullOrEmpty(reegId))
	        {
	            reeg = rDao.getById(reegId);
	        }
	        
	        if (reeg != null && !e_StringUtil.isNullOrEmpty(reegId))
	            RemoveReeg(reeg);
	        
	        if (!e_StringUtil.isNullOrEmpty(outcomeId))
	        {
	        	patientOutcomeDao outDao = new patientOutcomeDao();
	    
	        	r_patient_outc__c outc = outDao.getById(outcomeId);
	        	
	        	if (outc != null)
	        	{
					outDao.deleteSObject(outc);        		
	        	}
	        }
	        
	        if (isiPad != null && isiPad)
	      	    returnVal = new PageReference('/apex/iwPatient');
	        else
		        returnVal = new PageReference('/apex/UploadWizard1');
    	}
    	else
    	{
    		if (reeg != null)
    			returnVal = new PageReference('/' + reeg.Id);
    	}
        
        return returnVal;
    }
    
          //-- delete reeg and all related objects
    public void RemoveReeg(r_reeg__c reeg)
    {
    	if (reeg != null)
    	{
	        for(r_reeg_wmed__c wmedRow : [select id from r_reeg_wmed__c where r_reeg_id__c = :reeg.id])
	        {
	            delete wmedRow;
	        }
	
	        for(r_reeg_group__c medGrpRow : [select id from r_reeg_group__c where r_reeg_id__c = :reeg.id])
	        {
	            delete medGrpRow;
	        }
	
	        for(r_reeg_med__c medSensRow : [select id from r_reeg_med__c where r_reeg_id__c = :reeg.id])
	        {
	            delete medSensRow;
	        }
	
	        for(r_reeg_dx__c dxRow : [select id from r_reeg_dx__c where r_reeg_id__c = :reeg.id])
	        {
	            delete dxRow;
	        }
	
	        for(r_reeg_var__c cnsVarRow : [select id from r_reeg_var__c where r_reeg_id__c = :reeg.id])
	        {
	            delete cnsVarRow;
	        }
	
	        delete reeg;
    	}
    }
    
    public void AddParameters(PageReference pr)
    {
    	if (!e_StringUtil.isNullOrEmpty(reegId))
        	pr.getParameters().put('rid', reegId);
        else if (reeg != null && !e_StringUtil.isNullOrEmpty(reeg.Id))
        	pr.getParameters().put('rid', reeg.Id);
        
        if (!e_StringUtil.isNullOrEmpty(patId))
        	pr.getParameters().put('pid', patId);
        else if (patient != null && !e_StringUtil.isNullOrEmpty(patient.Id))
        	pr.getParameters().put('pid', patient.Id);
        	
        if (!e_StringUtil.isNullOrEmpty(outcomeId))
        	pr.getParameters().put('oid', outcomeId);
        else if (outcome != null && !e_StringUtil.isNullOrEmpty(outcome.Id))
        	pr.getParameters().put('oid', outcome.Id);
        	
        if (!e_StringUtil.isNullOrEmpty(tid))
        	pr.getParameters().put('tid', tid);
        if (!e_StringUtil.isNullOrEmpty(qsid))
        	pr.getParameters().put('qsid', qsid);
        if (isNewPat != null && isNewPat)
        	pr.getParameters().put('np', 'true');
        if (isWiz != null && isWiz)
        	pr.getParameters().put('wiz', 'true');
        if (isNew != null && isNew)
        	pr.getParameters().put('isNew', 'true');
        if (isWR != null && isWR)
        	pr.getParameters().put('wr', 'true');
    }
}