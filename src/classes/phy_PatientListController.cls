public with sharing class phy_PatientListController extends BaseController
{
	private string searchCriteria {get; set;}
    private integer resultTableSize {get; set;}
    private patientDao pDao;
     
    public string newFilterId {get; set;}
	public ApexPages.StandardSetController con {get; set;}
	public String patSearchString {get; set;}
	 
    public Boolean hasNext { get { return con.getHasNext(); } set; }  
    public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }  
    public Boolean hasResults { get { return (resultNumber > 0); } set; }  
    public Integer pageNumber  { get { return con.getPageNumber(); } set; }
    public Integer resultNumber  { get { return con.getResultSize(); } set; }  
    public void previous() { con.previous(); }  
    public void next() { con.next(); } 
    public List<patientDisp> PatDispList { get; set; } 
    
	
	public phy_PatientListController ()
    {
    	pDao = new patientDao();
    	resultTableSize = RESULT_TABLE_SIZE;
    	setupFilter(); 
        loadPatients();
    }
    
    private void setupFilter()
    {
        //-- jdepetro.8.26.14 - this was eliminating the patient sharing between dr's. The clinic admin is still not allowed patient sharing.
    	//--if(getIsClinicPhy()) pDao.filter = 'physician__c = ' + quote(getCurrentPhysician());

        //-- mcorbin.2.24.17 - This was not respecting the newly setup Sharing Rules (MAD-213 & MAD-249)
        //--if(getIsClinicAdmin()) pDao.filter = 'physician__r.AccountId = ' + quote(getCurrentAccount());
    }
    
    private PageReference loadPatients()
    {  
    	system.debug('FILTER: ' + pDao.filter);
    	
    	String query = pDao.getProviderPatientsQuery('', '', getIsAdminOrPortalUserMgr());
    	system.debug('loadPatients:query: ' + query);
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
        return null;
    }
    
    public List<patientDisp> getPatients()
    {
        if(con != null)
        {
            PatDispList = new List<patientDisp>();

            for (sObject patSObject : con.getRecords())
            {
                PatDispList.add(new patientDisp((r_patient__c)patSObject, isPEERComplete((r_patient__c)patSObject)));
            }

            return PatDispList; 
        }
        else
            return null;
    } 
    
    public void searchPatients()
    {
            Set<Id> patIDSet = new Set<Id>();
            rEEGDao rDao = new rEEGDao();
            List<r_reeg__c> reegList = rDao.getByInputWildcard(patSearchString, ' LIMIT 10000');
            
            for (r_reeg__c reeg : reegList)
            {
                patIDSet.add(reeg.r_patient_id__c);
            }
            
            con = new ApexPages.StandardSetController(pDao.getByIds(patIDSet, ' LIMIT 10000'));
            con.setPageSize(resultTableSize);
    }

    /*
    * @Description This method will return the status of the newest PEER linked to the patient
    *
    * @param patient record
    *
    * @Return The status of the PEER by checking the PEER  linked object (Available, Accepted, Rejected).
    */
    public Boolean isPEERComplete(r_patient__c patObj)
    {
        Boolean returnVal = false;
        if (patObj.r_reeg__r != null && !patObj.r_reeg__r.isEmpty())
        {
            //-- the reeg that decides if this returns true is the newest test. The patientDao must return the reeg list in desceding order on the request date field.
            if (patObj.r_reeg__r[0] != null)
                returnVal = (!e_StringUtil.isNullOrEmpty(patObj.r_reeg__r[0].neuroguide_status__c) 
                            && (patObj.r_reeg__r[0].neuroguide_status__c == 'Report Complete' 
                                || patObj.r_reeg__r[0].rpt_stat__c == 'Complete' || patObj.r_reeg__r[0].rpt_stat__c == 'NG Complete' 
                                || patObj.r_reeg__r[0].rpt_stat__c == 'Prelim - No Neuro Commentary' || patObj.r_reeg__r[0].rpt_stat__c == 'Delivered' )
                            && (String.isNotBlank(patObj.r_reeg__r[0].req_stat__c) && !patObj.r_reeg__r[0].req_stat__c.startsWithIgnoreCase('not valid') )
                            );
        }

        return returnVal;
    }
    
    //------------ TEST METHODS ------------------------
    
    private static testmethod void testController()
    {
        phy_PatientListController plc = new phy_PatientListController();
        plc.patSearchString = 'a';
     //   plc.searchPatients();
        plc.getPatients();
    }
    
    
	
	

}