public with sharing class wr_PeerSummaryController extends BaseDrugReportController
{
    public string drugName {get; set;}
    public string BrainMapUrl {get; set;}
    public boolean IsNeuroRptAvail {get; set;}
    public String drugFilter {get; set;}
    private String overrideRedirect;
    public r_patient__c patient {get; set;}
    public String printUrl {get; set;}
    public boolean isOkToShowReport {get; set;}

    public wr_PeerSummaryController()
    {
    	overrideRedirect = ApexPages.currentPage().getParameters().get('or');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        if (e_StringUtil.isNullOrEmpty(reegId))
            this.reegId = ApexPages.currentPage().getParameters().get('id');
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        rDao = new rEegDao();
    }

    public wr_PeerSummaryController(ApexPages.StandardController stdController)
    {
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        if (e_StringUtil.isNullOrEmpty(reegId))
            this.reegId = ApexPages.currentPage().getParameters().get('id');
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        rDao = new rEegDao();
    }

    public void setupDrugFilter(Boolean isCompletedGroup)
    {
        if (IsDrugLimitedStudy && !isCompletedGroup)
        {
        	DrugUtil du = DrugUtil.getInstance();
            try
            {
            	User u = [select Id, View_All_Drug_Results__c from User where Id =: UserInfo.getUserId()];
            	if (!u.View_All_Drug_Results__c)
            	{
    				rEEGDxDao rDao = new rEEGDxDao();
    				List<r_reeg_dx__c> dxs = rDao.getByrEEGId(this.reeg.Id);
    				drugFilter = du.getHexFilter(dxs);
            	}
            }
            catch(Exception e)
            {
            	drugFilter = '';
            }
        }
    }

    public PageReference loadAction()
    {
        Boolean isCompletedGroup = false;
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            this.reeg = rDao.getById(reegId, true);
            
            Boolean isControlGroup = ((reeg.r_patient_id__r.CA_Control_Group_Type__c != null && reeg.r_patient_id__r.CA_Control_Group_Type__c != '') && (reeg.r_patient_id__r.CA_Control_Group_Type__c == 'Control Group' || reeg.r_patient_id__r.CA_Control_Group_Type__c == 'Not Randomized Group'));
            isCompletedGroup = ((reeg.r_patient_id__r.CA_Control_Group_Type__c != null && reeg.r_patient_id__r.CA_Control_Group_Type__c != '') && reeg.r_patient_id__r.CA_Control_Group_Type__c == 'Completed Group');

            isOkToShowReport = !isControlGroup || getIsSysAdmin();

            if(this.reeg.req_stat__c != 'Complete' && overrideRedirect != null)
            {
            	patient = [select Id from r_patient__c where Id =: reeg.r_patient_id__c];
            	PageReference pg = new PageReference('/apex/wr_PeerViewS2?id=' + this.reeg.Id);
            	pg.setRedirect(true);
            	return pg;
            }

            baseUrl = '/apex/wr_PeerReportS2?rid=' + reeg.Id;
            cnsVarsUrl = '/apex/wr_CNSVarsS2?rid=' + reeg.Id + '&ctid=' + reeg.corr_cns_id__c;
            baseReportTabbedUrl = '/apex/wr_PeerReportS2?rid=' + reeg.Id;
            baseDrugGoupUrl = '/apex/wr_PeerDrugClassS2?rid=' + reeg.Id;
            BrainMapUrl = '/apex/PEERBrainMapView?rid=' + reeg.id;
            if (!e_StringUtil.isNullOrEmpty(pageId))
            {
                DrugTreeMenuUtil.MenuItem menuItem = wrMenuMap.get(pageId);
                if (menuItem != null)
                    drugName = menuItem.drugName;
            }
        }
        else
        {
            this.reeg = new r_reeg__c();
        }

        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        setupDrugFilter(isCompletedGroup);
        printUrl = getPrintViewPage();
        return null;
    }

    //---temporary
    public String getPrintViewPage()
    {
        String key = '';
        string versionTxt = '';
        if (!e_StringUtil.isNullOrEmpty(this.reegId))
        {
            String part1 = this.reegId.substring(5, 10);
            String part2 = this.reegId.substring(0, 5);
            String part3 = this.reegId.substring(10, reegId.length());
            key = '6H3a2' + part1 + 's' + part2 + part3;
        }

        return '{!Domain}/Public/Download.aspx?key=' + key + '&v=1&wr=1&sdh=' + drugFilter;

    }
    
    public String getDownloadEEGHref()
    {
    	return rEEGUtil.getDownloadUrl(reegId, '1');
    }

    public PageReference returnAction()
    {
        PageReference pr = new PageReference('/apex/wr_PeerListS2');
        return pr;
    }

    //--------- TEST METHODS ----------------------

    public static testmethod void testController()
    {
    	wr_PeerSummaryController psc = new wr_PeerSummaryController();
    	psc.returnAction();
    	psc.loadAction();
		psc.getShowPrintLinkAdmin();
		psc.getShowWrPrintLink();

    }

}