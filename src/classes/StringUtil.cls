public with sharing class StringUtil 
{
	public static Boolean isNullOrEmpty(String value)
    {
        return value == null || value == '';
    }
    
    public static Boolean isMatch(String value1, String value2, Boolean ignoreCase)
	{
		Boolean returnVal = false;
		
		if (!isNullOrEmpty(value1) && !isNullOrEmpty( value2))
		{
			if (ignoreCase)
			{
				returnVal = value1.equalsIgnoreCase( value2);
			}
			else
			{
				returnVal = value1.equals( value2);
			}
		}
		
		return returnVal;		
	}

    
     //------------TEST-----------------------------
    public static testMethod void testStringUtil()
    {
    	System.assertEquals(StringUtil.isNullOrEmpty(''), true);
    	System.assertEquals(StringUtil.isNullOrEmpty('XX'), false);
    	
    	System.assertEquals(StringUtil.isMatch('XX', 'xx', true), true);
    	System.assertEquals(StringUtil.isMatch('XX', 'XX', false), true);
    	System.assertEquals(StringUtil.isMatch('XX', 'yy', true), false);
    	System.assertEquals(StringUtil.isMatch('XX', 'YY', false), false);
    }

}