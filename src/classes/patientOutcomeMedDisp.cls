public with sharing class patientOutcomeMedDisp 
{
    public void setDosageDisplay()
    {
        // obj.Unknown__c = (obj.dosage__c != null && obj.Unit__c != null) ? obj.dosage__c + ' ' + obj.Unit__c : 'unknown';
    }
    
    private r_outcome_med__c obj;
    public r_outcome_med__c getObj()
    {
        return obj;
    }
    
    public void setObj(r_outcome_med__c s)
    {
        obj = s;
    }
  
    public void Setup(r_outcome_med__c o)
    {
        obj = o;
        // setDosageDisplay();
    }
    
    //--- To hold previous outcome medication id (change dose/discontinue)
    private string previousId;
    public string getPreviousId()
    {
        return previousId;
    }
    public void setupPreviousId(string id) {
    	previousId = id;
    }
    
    //--- To hold previous outcome medication id (change dose/discontinue)
    private boolean displayActions;
    public boolean getIsDisplayActions()
    {
        return displayActions;
    }
    public void setDisplayAction(boolean value) {
    	displayActions = value;
    }

    // ---Row number of the display element
    private Integer rowNum;
    public Integer getRowNum()
    {
        return rowNum;
    }    
    public void setRowNum(Integer value)
    {
        rowNum = value;
    }
    
    public String StartDate  
    {     	
    	get	{return e_StringUtil.getDateString(obj.start_date__c);}    	 
    	set	{obj.start_date__c = e_StringUtil.setDateFromString(value);} 
    }
    
    public String EndDate  
    {     	
    	get	{
    		if (obj != null && obj.end_date__c != null)
    			return e_StringUtil.getDateString(obj.end_date__c);
    		else return '';
    	}    	 
    	set	{obj.end_date__c = e_StringUtil.setDateFromString(value);} 
    }

    //------------TEST-----------------------------
    public static testMethod void testDisp()
    {
        r_outcome_med__c obj = new r_outcome_med__c();

        patientOutcomeMedDisp disp = new patientOutcomeMedDisp();
        disp.setObj(obj);
        disp.Setup( obj);
        disp.setDosageDisplay();

        r_outcome_med__c obj2 = disp.getObj();

        System.assertEquals( obj, obj2);

        disp.setRowNum(1);
        System.assertEquals( disp.getRowNum(), 1);
        
        disp.setupPreviousId('test');
        string test =  disp.getPreviousId();
		disp.setDisplayAction(true);
    	boolean testB = disp.getIsDisplayActions();
    	
    	disp.StartDate = '01/01/1950';
    	string testDt = disp.StartDate;
    	disp.EndDate = '01/01/1950';
    	testDt = disp.EndDate;
        
    }
}