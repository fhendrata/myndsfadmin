//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: i_PatientListController
// PURPOSE: 
// CREATED: 07/29/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class i_PatientListController extends BaseController
{
	public String SearchKey { get; set; }
	public String resultListTitle { get; set; }
	public Boolean NoResultsMessage { get; set; }
	public Boolean ShowResults { get; set; }
	public Boolean showRecentItems { get; set; }
	public List<i_ObjectDisp> objList {get; set;}
	private rEEGDao rDao  {get; set;}
	private patientDao pDao  {get; set;}
	
	
	public i_PatientListController()
	{
		IsTestCase = false;
		SearchKey = '';
		ShowResults = false;
		IsTestCase = false;
		NoResultsMessage = false;
	}
	
	public PageReference loadAction()
	{
		resultListTitle = 'Recent items';
		showRecentItems = true;
		pDao = new patientDao();
		pDao.IsTestCase = IsTestCase;
		rDao = new rEEGDao();
		rDao.IsTestCase = IsTestCase;
		
		//jswenski+1.7/7/2011 - changing get most recent to 0 for rEEGs per SFDC-59
		objList = mergeLists(pDao.getMostRecent('10'), rDao.getMostRecent('0'), 10);
		
		if (objList == null || objList.size() == 0) {
			NoResultsMessage = true;
		}			
		
		return null;
	}
		
	public List<r_patient__c> getPatients() 
	{	
		if (SearchKey == null) SearchKey = '';
		
		String searchVal = SearchKey.trim() + '%';
		List<r_patient__c> retVal = new List<r_patient__c>(); 
		
		retVal = pDao.getAllByFnOrLnOrCnsId(searchVal);
	
		return retVal;
	}
	
	public List<r_reeg__c> getReegs() 
	{	
		if (SearchKey == null) SearchKey = '';
		
		String searchVal = SearchKey.trim() + '%';
		List<r_reeg__c> retVal = new List<r_reeg__c>(); 
		
		retVal = rDao.getByName(searchVal);
	
		return retVal;
	}
	
	private List<i_ObjectDisp> mergeLists(List<r_patient__c> patList, List<r_reeg__c> reegList, integer lim)
	{
		List<i_ObjectDisp> returnList = new List<i_ObjectDisp>();
		integer i = 0;
		integer j = 0;
		integer patSize = (patList != null) ? patList.size() : 0;
		integer reegSize = (reegList != null) ? reegList.size() : 0;
		integer resultLimit = (lim > 0) ? lim : (patSize + reegSize);
		i_ObjectDisp dispObj;
		boolean run = true;
		while (returnList.size() < resultLimit && run)
		{
			dispObj = new i_ObjectDisp();
			
			if (patList.size() <= i)
			{
				if (reegList.size() > j)
				{
					
					dispObj.reeg = reegList[j];
					j++;
				}
				else
					run = false;
			}
			else if (reegList.size() <= j)
			{
				if (patList.size() > i)
				{
					dispObj.patient = patList[i];
					i++;
				}
				else
					run = false;
			} 
			else
			{
				if (patList[i].LastModifiedDate > reegList[j].LastModifiedDate)
				{
					dispObj.patient = patList[i];
					i++;
				}
				else
				{
					dispObj.reeg = reegList[j];
					j++;
				}
			}
			
			if (run)
			{
				dispObj.setObjParams();
				if (!isDuplicate(returnList, dispObj))
					returnList.add(dispObj);
			}
		}
		
		return returnList;
	}
	
	private boolean isDuplicate(List<i_ObjectDisp> currentList, i_ObjectDisp obj)
	{
		boolean returnVal = false;
		
		if (currentList != null && currentList.size() > 0 && obj != null)
		{
			for (i_ObjectDisp row : currentList)
			{
				if (row.isReeg)
				{
					if (obj.isReeg)
						returnVal = (row.reeg.r_patient_id__r.Id == obj.reeg.r_patient_id__r.Id);
					else
						returnVal = (row.reeg.r_patient_id__r.Id == obj.patient.Id);
				}
				else if (row.isPatient)
				{
					if (obj.isPatient)
						returnVal = (row.patient.Id == obj.patient.Id);
					else
						returnVal = (row.patient.Id == obj.reeg.r_patient_id__r.Id);
				}
				
				if (returnVal) break;
			}
		}
		
		return returnVal;
	}
	
	public PageReference searchAction()
	{
		objList = mergeLists(getPatients(), getReegs(), 150);
		
		if (objList == null || objList.size() == 0) {
			NoResultsMessage = true;
		}	
		resultListTitle = 'Results';
		ShowResults = true;
		return null;
	}
	
	public PageReference reqAddAction()
	{
		PageReference returnVal = new PageReference( '/apex/iwPatientSearch');
        returnVal.setRedirect(true);
        return returnVal;
	}
	
	//---------------TEST METHODS ---------------------------------
    //
    //-------------------------------------------------------------
    public static testMethod void testController()
    {
    	i_PatientListController cont = new i_PatientListController();	
    	cont.IsTestCase = true;
		cont.NoResultsMessage = false;
		cont.ShowResults = true;
		cont.loadAction();
		List<r_patient__c> patList = cont.getPatients(); 
		
		r_patient__c pat = patientDao.getTestPat();
		//testPat.Name = 'Smith';
    	//testPat.first_name__c = 'John';
    	cont.SearchKey = 'Smith';
		patList = cont.getPatients(); 
		cont.SearchKey = null;
		patList = cont.getPatients(); 
	//	System.assertNotEquals(patList, null);
		
		cont.SearchKey = 'XXXXXXXXXXXXXDSASEEDaafadffffffffffffff';
		patList = cont.getPatients(); 
		
		PageReference pr = cont.searchAction();
	//	System.assertEquals(pr, null);
    }
}