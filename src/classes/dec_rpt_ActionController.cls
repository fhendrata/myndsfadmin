//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: dec_rpt_ActionController
// PURPOSE: Action controller for the Online Report Rules Based Decision engine
// CREATED: 05/10/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class dec_rpt_ActionController extends BaseDrugReportController
{
	public string objPicklist1 {get; set;}
	public string objPicklist2 {get; set;}
	public string objPicklist2_2 {get; set;}
	public string objPicklistLoc1 {get; set;}
	public string objPicklistLoc2 {get; set;}
	public string objPicklistLoc2_2 {get; set;}
	public string fieldPicklist1 {get; set;}
	public string fieldPicklist1_2 {get; set;}
	public string fieldPicklist2 {get; set;}
	public string fieldPicklist2_2 {get; set;}
	public string objPicklistLbl1 {get; set;}
	public string objPicklistLbl2 {get; set;}
	public string objPicklistLbl2_2 {get; set;}
	public string fieldPicklistLbl1 {get; set;}
	public string fieldPicklistLbl1_2 {get; set;}
	public string fieldPicklistLbl2 {get; set;}
	public string fieldPicklistLbl2_2 {get; set;}
	public string ruleActionSel {get; set;}
	public double highestRuleNum {get; set;}
	public boolean isEditAvail {get; set;}
	public boolean isActivateAvail {get; set;}
	public boolean isInactivateAvail {get; set;}
	
	public string fieldSetChangeStr {get; set;}
	public string ruleId {get; set;}
	public string reegId {get; set;}
	public boolean isCloneEdit {get; set;}
	public boolean isNew {get; set;}
	
	public string priObjSet {get; set;}
	public string priFieldSet {get; set;}
	public string selectedVariable {get; set;}
	public string variableDatatype {get; set;}

	public dec_rule__c inputRule {get; set;}
	List<dec_rule__c> ruleList {get; set;}
	public dec_action__c action {get; set;}
	dec_ActionDao dao = new dec_ActionDao();
	private List<SelectOption> objectNameList;
	
	//---Base Constructor (go to test mode)
	public dec_rpt_ActionController()
	{
		IsTestCase = true;
		init(); 
	}
	
	//---Constructor from the standard controller
    public dec_rpt_ActionController(ApexPages.StandardController stdController)
    {    
        init();
    }
    
      //---Local init
    public void init()
    {
    }
    
    public boolean getIsRuleEditable()
    {
    	return (action.status__c == 'Pending');
    }
    
    public PageReference setupAction() 
    {		
    	reegId = ApexPages.currentPage().getParameters().get('rid');
    	string idParam = ApexPages.currentPage().getParameters().get('id');
    	string categoryId = ApexPages.currentPage().getParameters().get('cid');
    	string drugCatName = ApexPages.currentPage().getParameters().get('d');
    	pageId = ApexPages.currentPage().getParameters().get('pgid');
    	tabId = ApexPages.currentPage().getParameters().get('tbid');
    	string cloneParam = ApexPages.currentPage().getParameters().get('cl');
    	if (!e_StringUtil.isNullOrEmpty(cloneParam))
    	{
    		isCloneEdit = boolean.valueOf(cloneParam); 
    	} 
    	
    	if (!e_StringUtil.isNullOrEmpty(idParam))
    	{
    		action = dao.getById(idParam);
    		isNew = false;
    	}
    	else
    	{
    		action = new dec_action__c();
    		isNew = true;
    		action.is_report_action__c = isNew;
    		action.calculation_object__c = 'Rules';
    	}
    	
    	inputRule = new dec_rule__c();
    	ruleList = new List<dec_rule__c>();
    	highestRuleNum = 0;
    	
    	if(action.status__c == null) action.status__c = 'Pending';
    	
    	isEditAvail = (action.status__c == 'Pending');
    	isActivateAvail = (action.status__c == 'Inactive' || action.status__c == 'Pending');
    	isInactivateAvail = (action.status__c == 'Active');
    	
    	fieldSetChangeStr = 'Variable';
    	
    	if (!e_StringUtil.isNullOrEmpty(categoryId))
    		action.report_drug_category__c = categoryId;
    	
    	if (!e_StringUtil.isNullOrEmpty(drugCatName))
    		action.report_drug__c = drugCatName;
    	
    	return null;
    } 
    
    public boolean getshowVariableEdit() 
    {
    	return (fieldSetChangeStr == 'Variable');
    }
    
    public PageReference saveAction() 
    {	
    	PageReference pr = null;
    	
    	dao.saveSObject(action);
    	
    	if (!StringUtil.isNullOrEmpty(action.Id))
    	{
    		pr = new PageReference('/apex/RptDecisionActionViewPage?id=' + action.Id);
    	}
    	
		if (!e_StringUtil.isNullOrEmpty(reegId))
			pr.getParameters().put('rid', reegId);
		
		if (!e_StringUtil.isNullOrEmpty(pageId))
			pr.getParameters().put('pgid', pageId);
		
		if (!e_StringUtil.isNullOrEmpty(tabId))
			pr.getParameters().put('tbid', tabId);
		
		pr.setRedirect(true);
		
    	return pr;		
    }  
    
    public PageReference editActionOverride() 
    {	
		PageReference pr = new PageReference('/apex/RptDecisionActionEditPage?id=' + action.Id);
		pr.getParameters().put('rid', reegId);
		if (!e_StringUtil.isNullOrEmpty(pageId))
			pr.getParameters().put('pgid', pageId);
		if (!e_StringUtil.isNullOrEmpty(tabId))
			pr.getParameters().put('tbid', tabId);
		pr.setRedirect(true);
    	return pr;
    }  
    
    public PageReference cloneActionOverride() 
    {	
		dec_action__c actionClone = dao.cloneObj(action);

		dao.saveSObject(actionClone);
		
		dec_RuleDao ruleDao = new dec_RuleDao();
		List<dec_rule__c> newRules = ruleDao.cloneRulesForAction(actionClone.Id, action.Id);
		ruleDao.saveSObjectList(newRules);
		
		PageReference pr = new PageReference('/apex/RptDecisionActionEditPage?id=' + action.Id);
		pr.getParameters().put('rid', reegId);
		pr.getParameters().put('isNew', 'true');
		if (!e_StringUtil.isNullOrEmpty(pageId))
			pr.getParameters().put('pgid', pageId);
		if (!e_StringUtil.isNullOrEmpty(tabId))
			pr.getParameters().put('tbid', tabId);
		pr.setRedirect(true);
    	return pr;
    }  
    
    
    public PageReference cancel() 
    {
    	PageReference pr = null;
    	if (!StringUtil.isNullOrEmpty(action.Id))
    		pr = new PageReference('/apex/RptDecisionActionViewPage?id=' + action.Id);
    	else
    		pr = new PageReference('/apex/RptDecisionDevPage');
		pr.getParameters().put('rid', reegId);
		
		if (!e_StringUtil.isNullOrEmpty(pageId))
			pr.getParameters().put('pgid', pageId);
		
		if (!e_StringUtil.isNullOrEmpty(tabId))
			pr.getParameters().put('tbid', tabId);
		
    	return pr;
    }  
    
    public PageReference inactivate()
    {        
        action.status__c = 'Inactive';
        action.inactivated_date__c = DateTime.now();
        
        return saveAction();
    }
    
    public PageReference activate()
    {        
        action.status__c = 'Active';
        action.activated_date__c = DateTime.now();
        
        return saveAction();
    }
    
    public PageReference deleteActionOverride()
    {        
    	if (action != null && action.activated_date__c == null)
    	{
    		try
    		{
    			delete action;
    			action = null;
    		}
    		catch(Exception ex)
    		{
    			handleError( 'deleteActionOverride', ex);
    		}
    		
    		PageReference pr = new PageReference('/apex/RptDecisionDevPage?rid=' + reegId);
    		
    		if (!e_StringUtil.isNullOrEmpty(pageId))
				pr.getParameters().put('pgid', pageId);
			
			if (!e_StringUtil.isNullOrEmpty(tabId))
			pr.getParameters().put('tbid', tabId);
			
    		return pr; 
    	}
    	else
        	return null;
    }
    
    public PageReference dbTableAction()
    {   
    	Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 

		Map<String,String> prefixKeyMap = new Map<String,String>{};
		for(String sObj : gd.keySet())
		{
		   Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
		   prefixKeyMap.put(r.getName(), r.getKeyPrefix());
		}
		
    	return new PageReference('/' + prefixKeyMap.get('database_table__c'));
    }

    public PageReference checkSyntaxAction()
    {  
    	List<double> condNumList = new List<double>();
    	
        return null;
    }
    
    public PageReference loadPrimaryObjAction()
	{
		action.pri_obj_name__c = objPicklist1;
    	action.pri_obj_label__c = objPicklistLbl1;
    	action.pri_field_name__c = fieldPicklist1;
    	action.pri_field_label__c = fieldPicklistLbl1;
		
		return null;
	}
    
	public PageReference addRuleAction()
	{
		if (inputRule != null && action != null)
		{
			inputRule.pri_object__c = objPicklist1;
			inputRule.sec_object__c = objPicklist2;
			inputRule.sec_object_2__c = objPicklist2_2;
			
			inputRule.pri_datatype__c = objPicklistLoc1;
			inputRule.sec_datatype__c = objPicklistLoc2;
			inputRule.sec_datatype_2__c = objPicklistLoc2_2;			

			inputRule.pri_field__c = fieldPicklist1;
			inputRule.pri_field_2__c = fieldPicklist1_2;
			inputRule.sec_field__c = fieldPicklist2;
			inputRule.sec_field_2__c = fieldPicklist2_2;
			inputRule.pri_object_label__c = objPicklistLbl1;
			inputRule.sec_object_label__c = objPicklistLbl2;
			inputRule.sec_object_label_2__c = objPicklistLbl2_2;
			inputRule.pri_field_label__c = fieldPicklistLbl1;
			inputRule.pri_field_label_2__c = fieldPicklistLbl1_2;
			inputRule.sec_field_label__c = fieldPicklistLbl2;
			inputRule.sec_field_label_2__c = fieldPicklistLbl2_2;
			inputRule.r_dec_action__c = action.Id;

			if (!e_StringUtil.isNullOrEmpty(ruleId))
				inputRule.Id = ruleId;
			else
				inputRule.rule_number__c = highestRuleNum + 1;

			dec_RuleDao ruleDao = new dec_RuleDao();
			if(ruleDao.saveSObject(inputRule))
				inputRule = new dec_rule__c();
		}
		
		return null;
	}
	
	public List<dec_rule__c> getRuleList()
	{
		if (action != null)
		{
			dec_RuleDao ruleDao = new dec_RuleDao();
			ruleList = ruleDao.getByActionId(action.Id);
		}
		
		if (ruleList != null && ruleList.size() > 0)
		{
			for(dec_rule__c rule : ruleList)
			{
				if (highestRuleNum < rule.rule_number__c) highestRuleNum = rule.rule_number__c;
			}
		}
		
		return ruleList;	
	}
	
	
	public List<SelectOption> getObjList()
	{
		if (objectNameList == null)
		{
			objectNameList = e_DataUtil.getAllObjsNames(); 
		}
		
		return objectNameList;	
	}
	
	//---------------TEST METHODS ---------------------------------
    //
    //-------------------------------------------------------------
    public static testMethod void testController()
    {
    	dec_rpt_ActionController cont= new dec_rpt_ActionController();
    	
    	cont.objPicklist1 = 'test';
	 	cont.objPicklist2 = 'test';
	 	cont.objPicklist2_2 = 'test';
	 	cont.objPicklistLoc1 = 'test';
		cont.objPicklistLoc2 = 'test';
	 	cont.objPicklistLoc2_2 = 'test';
	 	cont.fieldPicklist1 = 'test';
	 	cont.fieldPicklist1_2 = 'test';
	 	cont.fieldPicklist2 = 'test';
	 	cont.fieldPicklist2_2 = 'test';
	 	cont.objPicklistLbl1 = 'test';
	 	cont.objPicklistLbl2 = 'test';
	 	cont.objPicklistLbl2_2 = 'test';
	 	cont.fieldPicklistLbl1 = 'test';
	 	cont.fieldPicklistLbl1_2 = 'test';
	 	cont.fieldPicklistLbl2 = 'test';
	 	cont.fieldPicklistLbl2_2 = 'test';
	 	cont.ruleActionSel = 'test';
	 	cont.highestRuleNum = 5.3;
	 	cont.isEditAvail = true;
	 	cont.isActivateAvail = true;
	 	cont.isInactivateAvail = true;
	 	cont.inputRule = dec_RuleDao.getTestRule();
	 	cont.ruleId = cont.inputRule.Id;
	 	cont.ruleList = new List<dec_rule__c>();
		cont.action = dec_actionDao.getTestAction();

		ApexPages.currentPage().getParameters().put('id', cont.action.Id);
		PageReference pr = cont.setupAction(); 
		pr = cont.saveAction();
		pr = cont.editActionOverride();
		pr = cont.cancel();
		pr = cont.inactivate();
		pr = cont.activate();
		pr = cont.deleteActionOverride(); 
		pr = cont.dbTableAction();
		pr = cont.checkSyntaxAction(); 
		pr = cont.addRuleAction();
    
    	cont.ruleList = cont.getRuleList();
    	List<SelectOption> optionList = cont.getObjList();
    }
}