//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: DatabaseFieldDao
// PURPOSE: Data access class for database fields
// CREATED: 06/14/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class DatabaseFieldDao extends BaseDao 
{
private static String NAME = 'database_field__c';    
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.database_field__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public database_field__c getById(String idInp)
    {
		return (database_field__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public List<database_field__c> getActive()
    {
		return (List<database_field__c>)getSObjectListByWhere(getFieldStr(), NAME, 'status__c = \'active\'', '');
    } 
    
    public List<database_field__c> getByTableId(string tableId)
    {
    	List<database_field__c> fieldList = null;
    	if (!e_StringUtil.isNullOrEmpty(tableId))
    		fieldList = (List<database_field__c>)getSObjectListByWhere(getFieldStr(), NAME, 'r_db_table__c = \'' + tableId + '\'', '');
		return fieldList; 
    } 
    
    public List<database_field__c> getActiveByTableId(string tableId)
    {
    	List<database_field__c> fieldList = null;
    	if (!e_StringUtil.isNullOrEmpty(tableId))
    		fieldList = (List<database_field__c>)getSObjectListByWhere(getFieldStr(), NAME, 'r_db_table__c = \'' + tableId + '\' and status__c = \'active\'', '');
		return fieldList; 
    } 

	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testDatabaseFieldDao()
    {
        DatabaseFieldDao dao = new DatabaseFieldDao();
        database_field__c fieldObj = DatabaseFieldDao.getTestField();
        
        List<database_field__c> fieldList = dao.getByTableId(fieldObj.r_db_table__c);
        fieldObj = dao.getById(fieldObj.Id);
        fieldList = dao.getActiveByTableId(fieldObj.r_db_table__c);
        fieldList = dao.getActive();
    }
        
	public static database_field__c getTestField()
    {
    	database_table__c table = DatabaseTableDao.getTestDbTable();
    	
    	database_field__c testField = new database_field__c(); 
    	testField.Name = 'Test Field';
    	testField.db_data_type__c = 'string';
    	testField.r_db_table__c = table.Id;
    	insert testField;
    	
    	return testField;
    }

}