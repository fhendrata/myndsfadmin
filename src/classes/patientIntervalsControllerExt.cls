//--------------------------------------------------------------------------------
// COMPONENT: 
// CLASS: patientIntervalsControllerExt
// PURPOSE: Controls PatientOutcomeIntervalsPage, accesses patientIntervalsDao class
// CREATED: 06/09/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class patientIntervalsControllerExt extends CrudController 
{
	private r_patient__c patient; 
    public List<r_interval__c> intervalsList { get; set; }

    patientIntervalsDao intervalsDao = new patientIntervalsDao();    

	//---Return a casted version of the object
	public r_patient_outc__c getObj() {
		return (r_patient_outc__c) CrudObj;
	}

	//---Base Constructor (go to test mode)
	public patientIntervalsControllerExt() {
		IsTestCase = true;
		init(); 
	}
	
    // ---Constructor from the standard controller
    public patientIntervalsControllerExt( ApexPages.StandardController stdController) {
    	init();    	
        init(stdCont); 
    }
    
    //---Local init
    public void init() {
    	BasePageName = 'PatientOutcomeIntevals';
    	CrudDao = intervalsDao;
    }
    
    //---Local setup
    public override PageReference setup() 
    {	
    	String patientId = '';
    	if (ApexPages.currentPage().getParameters().get('pid') != null 
    		&& ApexPages.currentPage().getParameters().get('pid') != '') {
    			patientId = ApexPages.currentPage().getParameters().get('pid');  			
    			// System.debug('## PICE setup, patientId = ' + patientId);    			
    			getIntervals(patientId);
    	}
    	return null;
    }
 
    private String lastname; 
    public void setLastName(String value) {
        lastname = value;
    }
    public String getLastName() {
        return lastname;
    }

    private String firstname;
    public void setFirstName(String value) {
        firstname = value;
    }
    public String getFirstName() {
        return firstname;
    }
    
    private String medicationname;
    public void setMedicationName(String value) {
        medicationname = value;
    }
    public String getMedicationName() {
        return medicationname;
    }
    /*
    private String cgiscore;
    public void setCgiScore(String value) {
        cgiscore = value;
    }
    public String getCgiScore() {
        return cgiscore;
    }
    */
    public void setPatient(r_patient__c pat) {
        this.patient = pat;
    }
    
    public void searchPatients()
    {
    	intervalsList = null;
    	resultVisible = true;
        intervalVisible = false;  
    }

    public PageReference getIntervals(String patId)
    {
        intervalVisible = true;
	
		// Get Intervals by Patient Id
		intervalsList = new List<r_interval__c>();
		intervalsList = intervalsDao.getByPatId(patId);
		
        return null;
    } 
    
    private Boolean intervalVisible;
    public Boolean getIntervalsVisible()
    {
       return intervalVisible;
    }
 
    private Boolean resultVisible;
    public Boolean getResultsVisible()
    {
       return resultVisible;
    }
    
    private List<r_patient__c> patAllList;
    public List<r_patient__c> getAllPatients()
    {
        patAllList = new List<r_patient__c>();
        if (lastname != null || firstname != null) 
        {
            for(r_patient__c patRow : [select id, name, first_name__c, middle_initial__c, dob__c, Gender__c, physician__c from r_patient__c 
                                        where name = :lastname OR first_name__c = :firstname])
            {
                patAllList.Add(patRow);
            }
        }
        return patAllList;
    }

	public List<SelectOption> getCgiItems() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('','-- Select --'));	                
		options.add(new SelectOption('0 - Not assessed','0 - Not assessed'));
		options.add(new SelectOption('1 - Very much improved','1 - Very much improved'));
		options.add(new SelectOption('2 - Much improved','2 - Much improved'));
		options.add(new SelectOption('3 - Minimally improved','3 - Minimally improved'));
		//jswenski+1.7/14/2011 adding a new value in
	    options.add(new SelectOption('4 - Baseline','4 - Baseline'));
		options.add(new SelectOption('4 - No change from baseline','4 - No change from baseline'));
		options.add(new SelectOption('5 - Minimally worse','5 - Minimally worse'));
		options.add(new SelectOption('6 - Much worse','6 - Much worse'));
		options.add(new SelectOption('7 - Very much worse','7 - Very much worse'));
		
		/* Loop through the feature_category__c records creating a selectOption
         for each result with the record ID as the value and the name as the label 
         displayed in the selectList
		for (Feature_Category__c fc : [select name from Feature_Category__c order by Name]){
			optionList.add(new SelectOption(fc.id,fc.name));
		} */     
		return options;
	}

    private String cgiScores;	
	public String getCgiScores() {
		return cgiScores;
	}	
    public void setCgiScores(String cgiScores) {
        this.cgiScores = cgiScores;
    }
    
    public Integer numberMeds { get; set; }
     public void getNumberMeds(String intervalId) {
     	Integer n = intervalsDao.getNmbrMedsByInterval(intervalId);
    	numberMeds = n;
    }       
    public void setNumberMeds(Integer n) {
    	numberMeds = n;
    }
    
    public Integer interval;
    public Integer getInterval(Date startDate, Date endDate) {
    	if (startDate != null && endDate != null) {
    		interval = startDate.daysBetween(endDate);
    	}
    	return interval;
    }
    public void setInterval(Integer i) {   	
    	interval = i;
    }
    
     // ---TEST METHODS ------------------------------
    public static testMethod void testController()
    {

        patientIntervalsControllerExt cont = new patientIntervalsControllerExt();
        cont.setFirstName('FirstName');
        cont.setLastName('Smith');
        cont.setMedicationName('ASPIRIN');
        
        string newFirstName = cont.getFirstName();
        string newLastName = cont.getLastName();
        string newMedicationName = cont.getMedicationName();
        
        cont.getIntervals('a0bR0000001LQ4uIAG');
        List<r_patient__c> testList = cont.getAllPatients();
        
        r_patient_outc__c CrudObj = new r_patient_outc__c();
        r_patient_outc__c newCrudObj = cont.getObj();
        
        cont.init();
        cont.setup();
        PageReference pageReg = cont.getIntervals('a0b60000000IJT3AAO');
        
        cont.intervalVisible = false;
    	cont.resultVisible = false;
    	
    	cont.setCgiScores('3 - Minimally improved');
    	String newCgiScores = cont.getCgiScores();
    	
    	List<SelectOption> cgiSelectOptions = cont.getCgiItems();
    	
    	boolean newInternalVisible = cont.getIntervalsVisible();
    	boolean newResultVisible = cont.getResultsVisible();
    	
    	r_patient__c pat = patientDao.getTestPat();
    	cont.setPatient(pat);
		cont.searchPatients();
    
    }
}