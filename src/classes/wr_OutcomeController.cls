public with sharing class wr_OutcomeController extends BaseController
{
	
	private Integer oMedCount;
    private String reegId { get; set; }
    private String patId { get; set; }
    private String currentMeds = '';
   
    private patientOutcomeDao dao {get; set;}
    private patientOutcomeMedDao ocMedDao { get; set; }
    private patientIntervalsDao intDao { get; set; }
    private rEEGDao reegDao { get; set; }
    private patientDao patDao {get; set;}
	
    public List<PatientOutcomeMedDisp> oMeds  { get; set; }
    public List<PatientOutcomeMedDisp> newMeds  { get; set; }
    public List<r_outcome_med__c> holdingList { get; set; } 
    public List<r_outcome_med__c> previousMeds { get; set; }
    public String billingCode { get; set; }
    public integer rowCount {get; set;}
    public integer newRowCount {get; set;}
  
    public List<String> medsToUpdate { get; set; }
    public boolean addNewMed {get; set;}
    public string outcomeId { get; set; }
    public r_patient__c patient {get; set;}
    public r_patient_outc__c patOutcome {get; set;}
    
    public String patientName {get; set;}

   
 
    // ---Constructor from the standard controller
    public wr_OutcomeController( ApexPages.StandardController stdController)
    {
    	dao = new patientOutcomeDao();
        ocMedDao = new patientOutcomeMedDao();
        intDao = new patientIntervalsDao();
        patDao = new patientDao();
        reegDao = new rEEGDao();
       
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
    	this.outcomeId = ApexPages.currentPage().getParameters().get('id');
        
        
        if(outcomeId != null)
        {
            patOutcome = dao.getById(outcomeId);
            patient = patDao.getById(patOutcome.patient__c);
           
        }
        else
        {
        	loadPreviousMeds(this.patId);
        	patient = patDao.getById(this.patId);
        	patOutcome = new r_patient_outc__c();
        }
        
         patientName = patient.first_name__c + ' ' + patient.Name;
        
    	setupOutcome();
        addBlankMedRows();
         
    }
    
    //--- Number of outcome meds in list
    public Integer getOMedCount()
    {
        return oMedCount;
    }
    
    public PageReference newOutcome()
    {
        PageReference returnVal = new PageReference( '/apex/wr_OutcomeEditS2?pid=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }
    
 
    public void setupOutcome() 
    {       
    	
    	oMeds = getOutcomeMeds(); //getExistingOutcomeMeds(patient.Id);
    	
    	if (oMeds == null) 
            oMeds = new List<PatientOutcomeMedDisp>(); 
      
        /**if (IsNew && e_StringUtil.isNullOrEmpty(outcomeId))
        {
            CrudObj = new r_patient_outc__c();
            Date oDate = date.today();
            CrudObj.put('outcome_date__c', oDate); 
            CrudObj.put('cgi__c', '4 - Baseline'); 

            if (!e_StringUtil.isNullOrEmpty(this.reegId))
            {
                r_reeg__c reeg = reegDao.getById(this.reegId);
                if (reeg != null)
                {
                    CrudObj.put('rEEG__c', reeg.Id);
                    billingCode = reeg.billing_code__c;
                }
            }
            
            CrudObj.put('patient__c', this.patId);
            
            
            
             
            
            CrudDao.saveSObject(CrudObj);
        }
        else
        {
        	patOutcome = dao.getById(outcomeId);
        	
        	
            CrudObj = dao.getById(ObjId); 
            if (CrudObj != null)
            {
                string reegId = (String) CrudObj.get('rEEG__c');
                if (!e_StringUtil.isNullOrEmpty(reegId))
                    billingCode = dao.getbillingCode(reegId);
    
                oMeds = getOutcomeMeds();
            }
        } **/     
        
    } 
    
    public PageReference editActionOverride()
    {
        PageReference pg = new PageReference('/apex/wr_OutcomeEditS2?id=' +this.patOutcome.Id + '&pid=' + this.patId);
        pg.setRedirect(true);
        return pg;
    }

    public PageReference cancelOutcomeAction()
    {
    	PageReference pg = null;
        if(patOutcome == null || patOutcome.Id == null) pg = new PageReference('/apex/wr_PatientViewS2?id='+patient.Id);
        else pg = new PageReference('/apex/wr_OutcomeViewS2?id='+patOutcome.Id);
        pg.setRedirect(true);
        return pg;
    }
    
    public void loadPreviousMeds(string pid)
    {
    	if (newMeds == null) newMeds = new List<PatientOutcomeMedDisp>();  
        
    	if (oMeds == null) oMeds = new List<PatientOutcomeMedDisp>();  
        
        if (medsToUpdate == null) medsToUpdate = new List<String>();
        PatientOutcomeMedDisp oMed;          
        Integer ctr = -1;
        previousMeds = dao.getExistingOutcomeMeds(this.patId);
             if (previousMeds != null) 
             {
                for(r_outcome_med__c oMedRow : previousMeds) {
                        ctr++;  
                        oMed = new PatientOutcomeMedDisp();
                        oMed.setDisplayAction(true);
                        oMed.setRowNum(ctr);
                        
                        r_outcome_med__c hldgMed = new r_outcome_med__c();
                        hldgMed.med_name__c = oMedRow.med_name__c;
                        hldgMed.dosage__c = oMedRow.dosage__c;
                        hldgMed.unit__c = oMedRow.unit__c;
                        hldgMed.frequency__c = oMedRow.frequency__c;
                        hldgMed.start_date__c = oMedRow.start_date__c;
                        hldgMed.end_date__c = oMedRow.end_date__c;
                        hldgMed.previous_med__c = oMedRow.Id;
                        hldgMed.start_date_is_estimated__c = oMedRow.start_date_is_estimated__c;
                        oMed.Setup(hldgMed);
                        // oMed.Setup(oMedRow);     
                        // previousIds.Add(oMedRow.Id);                     
                        oMeds.Add(oMed); 
                        newMeds.Add(oMed);             
                        
                        // running list of previous meds to update on save
                        medsToUpdate.add(oMedRow.Id);          
                }
                oMedCount = ctr + 1;    //---ctr is zero based   
                rowCount = ctr + 1; //---ctr is zero based                              
             }
        
    }
    
    //---Build the existing outcome meds list (meds from previous outcome)
    public List<PatientOutcomeMedDisp> getExistingOutcomeMeds(string pid)
    {   
         if (oMeds == null)
         {  
             oMeds = new List<PatientOutcomeMedDisp>();  
             // List<string> previousIds = new List<string>();
             PatientOutcomeMedDisp oMed;          
             Integer ctr = -1;
             
             if (medsToUpdate == null)
                medsToUpdate = new List<String>();
             
             //---Get the current meds
             previousMeds = dao.getExistingOutcomeMeds(this.patId);
             if (previousMeds != null) 
             {
                for(r_outcome_med__c oMedRow : previousMeds) {
                        ctr++;  
                        oMed = new PatientOutcomeMedDisp();
                        oMed.setDisplayAction(true);
                        oMed.setRowNum(ctr);
                        
                        r_outcome_med__c hldgMed = new r_outcome_med__c();
                        hldgMed.med_name__c = oMedRow.med_name__c;
                        hldgMed.dosage__c = oMedRow.dosage__c;
                        hldgMed.unit__c = oMedRow.unit__c;
                        hldgMed.frequency__c = oMedRow.frequency__c;
                        hldgMed.start_date__c = oMedRow.start_date__c;
                        hldgMed.end_date__c = oMedRow.end_date__c;
                        hldgMed.previous_med__c = oMedRow.Id;
                        hldgMed.start_date_is_estimated__c = oMedRow.start_date_is_estimated__c;
                        oMed.Setup(hldgMed);
                        // oMed.Setup(oMedRow);     
                        // previousIds.Add(oMedRow.Id);                     
                        oMeds.Add(oMed);              
                        
                        // running list of previous meds to update on save
                        medsToUpdate.add(oMedRow.Id);          
                }
                oMedCount = ctr + 1;    //---ctr is zero based   
                rowCount = ctr + 1; //---ctr is zero based                              
             }
             else {
                oMeds = null;
             }
        }    
        return oMeds;
    }
    
    //---Build the current outcome's meds list
    public List<PatientOutcomeMedDisp> getOutcomeMeds()
    {
    	 if (newMeds == null) newMeds = new List<PatientOutcomeMedDisp>();    
         if (oMeds == null)
         {
             oMeds = new List<PatientOutcomeMedDisp>();  
             PatientOutcomeMedDisp oMed;
             
             Integer ctr = -1;
             
             if (outcomeId != null)
             {
                 //---Add the current meds
                 for(r_outcome_med__c oMedRow : 
                     [select r_outcome_id__c, med_name__c, dosage__c, unit__c, frequency__c, start_date__c, end_date__c, name, start_date_is_estimated__c
                         from r_outcome_med__c
                         where r_outcome_id__c = : outcomeId])
                 {
                    ctr++;
                    currentMeds += oMedRow.med_name__c + ',';
                    oMed = new PatientOutcomeMedDisp();
                    oMed.setRowNum(ctr);
                    oMed.Setup(oMedRow );                                 
                    oMeds.Add(oMed);
                    newMeds.Add(oMed);
                 }
                 oMedCount = ctr + 1;  //---ctr is zero based
             
                 if (ctr < 0) 
                 {
                   //---Create blank row
                    r_outcome_med__c objMed = new r_outcome_med__c();
                    objMed.r_outcome_id__c = outcomeId;
                    objMed.unit__c = 'mg';
                        
                    oMed = new PatientOutcomeMedDisp();
                    oMed.setRowNum(0);    
                    rowCount = 1; 
                    oMed.Setup(objMed);                           
                    oMeds.Add(oMed);    
                 } 
             }       
             rowCount = ctr + 1;    //---ctr is zero based
        }    
        return oMeds;
    }
    
    //TODO -- this needs to get copied over to the second req controller
    // and called when the req is submitted 
    public PageReference saveActionOverride()
    {
    	
    	if(outcomeId == null)
    	{
    		patOutcome.patient__c = patId;
    		insert patOutcome;
    		outcomeId = patOutcome.Id;
    	}
    	    
        if (newMeds != null && newMeds.size() > 0)
        {
            for(PatientOutcomeMedDisp nMedRow : newMeds) 
            {
                if (nMedRow != null) 
                {
                    r_outcome_med__c nOutcMed = nMedRow.getObj();
                    if (nOutcMed != null) 
                    {
                    	
                        if(nOutcMed.r_outcome_id__c == null) nOutcMed.r_outcome_id__c = outcomeId;
                        nMedRow.Setup(nOutcMed);
                        SaveOutcomeMed(nOutcMed);   
                        if (nOutcMed != null && nOutcMed.med_name__c != null && nOutcMed.med_name__c != '')
                            oMeds.add(nMedRow);
                    }
                }
            }
        }

        buildIntervals();  
        update patOutcome;

        return gotoViewMode();
             
    }
    
    public void buildIntervals()
    {
        r_patient__c pat = patDao.getById(patId);
        
        if (pat != null)
        {
            patientIntervalBuilder intBuilder = new patientIntervalBuilder();
            intBuilder.buildAllIntervalsForPat(pat.Id, pat.OwnerId);
        }
    }
    
    public PageReference gotoViewMode()
    {
    	PageReference pg = new PageReference('/apex/wr_OutcomeViewS2?id='+patOutcome.Id + '&pid=' + this.patId);
        pg.setRedirect(true);
        return pg;  
    }
    
    // ---Save an outcome med row
    
    private Boolean medExists(String medName)
    {
        	String medList = currentMeds.toLowerCase();
            return medList.contains(medName.toLowerCase());
    }
    
    private void SaveOutcomeMed(r_outcome_med__c medObj)
    {
        if (medObj != null && medObj.med_name__c != null && medObj.med_name__c != '')
        {
        	
            ocMedDao.saveSObject(medObj);
        }
        else 
        {
            //---If the id is not blank and the name is, then delete the old record
            if (medObj.id != null) ocMedDao.deleteSObject(medObj);
        }    
    }

    public PageReference addMedRow()
    {   
        Integer rowNum = Integer.valueOf(System.currentPageReference().getParameters().get('rowNumber'));
        // rowNum is 0 based, rowCount starts at 1
        if ((rowNum + 1) == rowCount) 
        {
            //---Create blank row
            r_outcome_med__c objMed = new r_outcome_med__c();
            objMed.r_outcome_id__c = outcomeId;
                
            PatientOutcomeMedDisp oMed = new PatientOutcomeMedDisp();
            oMed.setRowNum(rowCount); 
            rowCount++; 
            oMed.Setup(objMed);                           
            oMeds.Add(oMed);    
        }        
        return null;      
    }
    
    public PageReference addBlankMedRows()
    {
        addNewMed = true;
        if (newMeds == null) newMeds = new List<PatientOutcomeMedDisp>();    
                
        for (integer i = 0; i < 5; i++)
        {
            PatientOutcomeMedDisp medDisp = new PatientOutcomeMedDisp();
            r_outcome_med__c newMed = new r_outcome_med__c();
            newMed.unit__c = 'mg';  
            medDisp.setObj(newMed);     
            medDisp.setRowNum(newMeds.size());
            newMeds.add(medDisp);       
        }
        newRowCount = newMeds.size();
        
        return null;
    }  
    
    // if outcome is deleted, need to delete associate medication
    public PageReference deleteActionOverride()
    {        
        //retUrl = '/apex/PatientViewPage?id=' + this.patId;
        try {                   
            if (oMeds != null) {
                //---Loop through each row
                for(PatientOutcomeMedDisp oMedRow : oMeds) {
                    if (oMedRow != null) {
                        if (oMedRow.getObj() == null) {
                           // addMessage('oMedRow.getObj() is null');         
                        }
                        else {
                            // Should not allow deleting of data, do a soft delete
                            oMedRow.getObj().put('is_deleted__c',true);
                            upsert oMedRow.getObj();                    
                        }
                    }
                }
            }  
        }
        catch (Exception ex) {
            handleError( 'deleteSObject', ex);
        }
        return null;
    }
    
    
    //------- TEST METHODS ----------
    
    private static testmethod void testController()
    {
    	   patientOutcomeDao outDao = new patientOutcomeDao();
    	   patientOutcomeMedDao outMedDao = new patientOutcomeMedDao();
    	   r_patient__c testPatient = patientDao.getTestPat();
    	   r_reeg__c testReeg = rEEGDao.getTestReeg();
    	   r_outcome_med__c testMed = outMedDao.getTestOutcomeMed();
    	   r_patient_outc__c testOutcome = outDao.getById(testMed.r_outcome_id__c);
    	  
    	   
    	   ApexPages.StandardController sc = new ApexPages.Standardcontroller(testOutcome);
    	   ApexPages.currentPage().getParameters().put('id',testOutcome.Id);
    	   ApexPages.currentPage().getParameters().put('rid',testReeg.Id);
    	   ApexPages.currentPage().getParameters().put('pid',testPatient.Id);
    	   
    	   wr_OutcomeController oc = new wr_OutcomeController(sc);
    	   
    	   oc.editActionOverride();
    	   oc.getExistingOutcomeMeds(testPatient.Id);
    	   oc.saveActionOverride();
    	   oc.gotoViewMode();
    	   oc.addBlankMedRows();
    	   oc.buildIntervals();
    	   oc.deleteActionOverride();
    	   
    	   
    	   
    	   
    	   
    	   
    }
    

}