//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: CustomerPortalLeadDaoTest
// PURPOSE: Test class for CustomerPortalLeadDao class
// CREATED: 05/06/16 Ethos Solutions - www.ethos.com
// Author: Mitchell Corbin
//--------------------------------------------------------------------------------
@isTest
public with sharing class CustomerPortalLeadDaoTest
{
	private static testmethod void testDaoMethods()
	{
		CustomerPortalLeadDao cpDao = new CustomerPortalLeadDao();
		Customer_Portal_Lead__c testCPLead = CustomerPortalLeadDao.getInstance().getTestCPLead();
		System.assert(testCPLead != null);

		Customer_Portal_Lead__c testCPLead2 = CustomerPortalLeadDao.getInstance().getById(testCPLead.Id);
		System.assertEquals(testCPLead.Last_Name__c, testCPLead2.Last_Name__c);

		PEER_Requisition__c testPEER = PEERRequisitionDao.getTestObj();
		testPEER.Patient_Customer_Portal_Lead__c = testCPLead.Id;
		update testPEER;

		Id relatedPEERId = CustomerPortalLeadDao.getInstance().getRelatedPEERId(testCPLead);
		System.assertEquals(testPEER.Id, relatedPEERId);

		Customer_Portal_Lead__c testCPLead3 = CustomerPortalLeadDao.getInstance().getTestCPLead();
		Id nullPEERId = CustomerPortalLeadDao.getInstance().getRelatedPEERId(testCPLead3);
		System.assertEquals(null, nullPEERId);
	}
}