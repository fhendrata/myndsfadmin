//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: UserDao
//   PURPOSE: Data Access for: User
// 
//     OWNER: CNS Response
//   CREATED: 10/27/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class UserDao extends BaseDao
{
    public UserDao()
    {
    }

	private static String NAME = 'User'; 
	
	private static String fldList;	
	public static String getFieldStr()
	{
		
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.User.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public User getById(String idInp)
    {
		return (User)getSObjectById(getFieldStr() + ',Contact.PEER_Ownership__c', NAME, idInp);
    } 

    public User getByUsername(String username)
    {
    	User returnVal = null;
    	String whereStr = ' Username = \'' + username + '\'';
		List<User> userList = (List<User>)getSObjectListByWhere(getFieldStr(), NAME, whereStr);

		if (userList != null && userList.size() > 0)
			returnVal = userList[0];

		return returnVal;
    }

    public User getUserContactID(Id userId)
    {
    	List<User> userList = [SELECT Id, ContactId, IsActive FROM User WHERE Id = :userId];
    	if (userList != null && !userList.isEmpty())
    	{
    		return userList[0];
    	}
    	else
    	{
    		return null;
    	}
    } 

    public User getUserIDByContactID(Id contactId)
    {
    	List<User> userList = [SELECT Id, ContactId, IsActive FROM User WHERE ContactId = :contactId];
    	if (userList != null && !userList.isEmpty())
    	{
    		return userList[0];
    	}
    	else
    	{
    		return null;
    	}
    }

    public User getCurrentUser()
    {
        for (User u : [select Id, Contact.PEER_Ownership__c, IsActive from User where Id = : UserInfo.getUserId()])
            return u;
        return null;
    } 

	public List<User> getPortalManagerUsersByAccountId(Id accountId)
	{
		List<Profile> profiles = [SELECT Id FROM Profile WHERE Name = 'Physician Portal Manager'];
		if (!profiles.isEmpty())
		{
			Id profileId = profiles[0].Id;
			List<User> userList = [SELECT Id, Email FROM User WHERE ProfileId = :profileId AND Contact.AccountId = :accountId AND IsActive = true];
			if (userList != null && !userList.isEmpty())
				return userList;
			else
				return null;
		}
		else
			return null;
	}
    
    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testUserDao()
    {
        UserDao obj = new UserDao();

        string userFields = UserDao.getFieldStr();

        User current = obj.getUserContactID(UserInfo.getUserId());

        // Doesn't recognize empty string as a valid Id
        // current = obj.getUserIDByContactID('');

    }
}