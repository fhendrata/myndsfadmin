public class rEEGNewControllerExt extends BaseController
{
    private final r_reeg__c reeg;
    private String patId;
    private Boolean isTest = false;

    public rEEGNewControllerExt()
    {
        //---this is only used for the apex test case
        reeg = new r_reeg__c();
        profile = new Profile();
        isTest = true;
    }

    //---Constructor from the standard controller
    public rEEGNewControllerExt( ApexPages.StandardController stdController)
    {    
        this.reeg = (r_reeg__c)stdController.getRecord();
        Setup();
    }

    public void Setup()
    {
        patId = ApexPages.currentPage().getParameters().get('pid');
        if (patId != null)
        {
            reeg.r_patient_id__c = patId;
        }
    }

    private Boolean showModPicklist;
    public void CheckModPickList()
    {
        if (reeg != null) 
        {
            if (getIsMgr())
            {
               showModPicklist = (reeg.reeg_type__c == 'Type I' || reeg.reeg_type__c == 'Type 1');
            }
        }
    }

    public Boolean getShowModPicklist()
    {
        return true;
    }

    public PageReference cancel()
    {
        String reegPrefix = rEEGUtil.getrEEGPrefix();
        PageReference returnVal = new PageReference('/' + reegPrefix + '/o');
        if (patId != null)  returnVal = new PageReference( '/' + patId);
       
        returnVal.setRedirect(true);
        return returnVal;
    }

   

    //---TEST METHODS ------------------------------
    public static testMethod void testDxController()
    {
        rEEGNewControllerExt cont = new rEEGNewControllerExt();
        cont.Setup();
        cont.CheckModPickList();
        Boolean showPicklist = cont.getShowModPicklist();
        Boolean mgr = cont.getIsMgr();
        Boolean notmgr = cont.getIsNotMgr();
        Boolean testMgr = !notmgr;
        PageReference pRef1 = cont.cancel();
   
        System.assertEquals( mgr, testMgr);
        System.assertEquals( 1, 1);
    }

}