//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: bat_OutcomeIntervalProcessor
// PURPOSE: Batch implementation for creating Intervals from Outcomes
// CREATED: 02/06/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
global class bat_OutcomeIntervalProcessor implements Database.Batchable<sObject>
{
   	global final String Query;
   	global patientIntervalBuilder patIntBuilder;

   	global bat_OutcomeIntervalProcessor(String q)
   	{
   		Query = q;
   		patIntBuilder = new patientIntervalBuilder();
   	}
	
	/*
	   	-- Code to run in 15 seconds
	  	DateTime n = datetime.now().addSeconds(15);
		String cron = '';
		cron += n.second();
		cron += ' ' + n.minute();
		cron += ' ' + n.hour();
		cron += ' ' + n.day();
		cron += ' ' + n.month();
		cron += ' ' + '?';
		cron += ' ' + n.year();
		bat_OutcomeIntervalRunner batchRunner = new bat_OutcomeIntervalRunner();
		System.schedule('Outcome Interval Builder - Immediate',cron, batchRunner);
   */
	
   	global Database.QueryLocator start(Database.BatchableContext BC)
	{
    	return Database.getQueryLocator(Query);
   	}

 	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
   		//-- If any patients are found using the given query we will build Intervals for that patient.
   		for(Sobject s : scope)	patIntBuilder.buildAllIntervalsForPat(s);
   	}
 
   	global void finish(Database.BatchableContext BC)
   	{
   		
   	}
   	
}