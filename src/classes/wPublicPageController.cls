public class wPublicPageController 
{
    public String publicPageName {get; set;}    
    private r_public_page__c pubPage; 
      
    public r_public_page__c  getPublicPage() 
    {
        if (pubPage == null) LoadPublicPage();
        return pubPage;
    }
    
    public String getBodyL()
    {
        return decode( getPublicPage().lbody__c);
    }
    
    public String getBodyR()
    {
        return decode( getPublicPage().rbody__c);
    }
    
    public String getTitleL()
    {
        return decode( getPublicPage().ltitle__c);
    }
    
    public String getTitleR()
    {
        return decode( getPublicPage().rtitle__c);
    }
    
    private String decode(String inputVal)
    {
        String returnVal = inputVal;
        
        returnVal = returnVal.replace('&quot;', '"');
        returnVal = returnVal.replace('&lt;', '<');
        returnVal = returnVal.replace('&gt;', '>');
        returnVal = returnVal.replace('&amp;', '&');  
        returnVal = returnVal.replace('&rsquo;', '\'');
      
        return returnVal;
    }

    public void LoadPublicPage() 
    {   
        if (publicPageName != null && publicPageName != '')
        {
            pubPage= [Select Id, Name, ltitle__c, rtitle__c, lbody__c, rbody__c FROM r_public_page__c where Name =: publicPageName];
        }
        
        if (pubPage== null) pubPage= new r_public_page__c();
    }
}