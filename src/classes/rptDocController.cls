//--------------------------------------------------------------------------------
// COMPONENT: Ethos common
//     CLASS: rptDocController
//   PURPOSE: controller for report document page
// 
//     OWNER: CNSR
//   CREATED: 03/26/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class rptDocController extends BaseController
{
	public List<report_document__c> versionDocsList {get; set;}
	public List<report_document__c> finalizedDocsList {get; set;}
	public List<report_document__c> deliveredDocsList {get; set;}
	public boolean showVersions {get; set;}
	public boolean showFinalized {get; set;}
	public boolean showDelivered {get; set;}
	public string reegId {get; set;}
	public string docId {get; set;}
	public report_document__c selectedDoc {get; set;}
	public boolean ShowDeliverFinishButton {get; set;}
	public rptDocDao rptDao {get; set;}
	public string patFullName {get; set;}
	public r_reeg__c reeg {get; set;}
	public rEEGDao reegDao {get; set;}

	public rptDocController()
	{
		rptDao = new rptDocDao();
	}
	
	public PageReference loadAction()
	{
		loadData();
		return null;
	}
	
	public PageReference loadDeliver()
	{
		ShowDeliverFinishButton = false;
		docId = ApexPages.currentPage().getParameters().get('dId');
		if (!e_StringUtil.isNullOrEmpty(docId))
		{
			selectedDoc = rptDao.getById(docId);
			
			if (selectedDoc != null)
			{
				if (!e_StringUtil.isNullOrEmpty(selectedDoc.r_finalized_by__c))
				{
					ShowDeliverFinishButton = true;
					selectedDoc.delivered_date__c = DateTime.now();	
					patFullName = selectedDoc.patient_first_name__c + ' ' + selectedDoc.patient_last_name__c;
					
					reegDao = new rEEGDao();
					reeg = reegDao.getById(selectedDoc.r_reeg__c);
				}
			}
		} 
		
		return null;
	} 
	
	public PageReference deliverAction()
	{
		if (selectedDoc != null)
		{
			selectedDoc.r_delivered_by__c = UserInfo.getUserId();
			selectedDoc.is_delivered__c = true;
			rptDao.saveSObject(selectedDoc);
			
			if (reeg != null)
			{
				reeg.rpt_stat__c = 'Delivered';
				reegDao.Save(reeg, 'UPDATE_DB_STATUS');
			} 
		}
		return null;
	}
	
	private void loadData()
	{
		reegId = ApexPages.currentPage().getParameters().get('rId');
		
		if (!e_StringUtil.isNullOrEmpty(reegId))
		{
			versionDocsList = rptDao.getVersions(reegId);
			finalizedDocsList = rptDao.getFinalized(reegId);
			deliveredDocsList = rptDao.getDelivered(reegId);
		}
	
		showVersions = showList(versionDocsList);
		showFinalized = showList(finalizedDocsList);
		showDelivered = showList(deliveredDocsList);
	}
	
	private boolean showList(List<report_document__c> rptList)
	{
		return (rptList != null && rptList.size() > 0);
	}
	
 	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testrptDocController()
    {
    	r_reeg__c reeg = rEEGDao.getTestReeg();
    	PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('rId',reeg.id);
    	
        rptDocController dao = new rptDocController();
        PageReference pr = dao.loadAction();
		


        report_document__c testDoc = rptDocDao.getTestRptDoc();
        pRef.getParameters().put('dId',testDoc.id);
        pr = dao.loadDeliver();
        
        pr = dao.deliverAction();
        
        
 //       List<report_document__c> rptList = dao.getVersions(testDoc.r_reeg__c);
		
    }    
}