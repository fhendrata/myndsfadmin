//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: rEEGDxController
// PURPOSE: Controller class for the dx selection page
// CREATED: 11/19/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class phy_DxEdit extends BaseWizardController
{   
    public boolean isSubmitVerified { get; set; }
    
    private List<rEEGDxDisp> dxs;
    private List<rEEGDxDisp> dxObjs;
    
    private List<rEEGDxGroupDisp> dxGroups;
    private List<rEEGDxGroupDisp> dxGroupsL;
    private List<rEEGDxGroupDisp> dxGroupsR;

    public PageReference setup()
    {
        this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.qsId = ApexPages.currentPage().getParameters().get('qsid');
        string newPatStr = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        
        rDao = new rEEGDao();
        
        getDxs();
        
        isSubmitVerified = false;
        isiPad = false;
        
        return null;
    }

    public PageReference save()
    {
        
        //jswenski 6/9/2011 - need to get this when we're post QS
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        
        
        rEEGDao reegDao = new rEEGDao();
        r_reeg__c reeg = reegDao.getById(reegId); 
        
        
        
        if (reeg != null)
        {
            reeg.stat_dx_gen__c = saveDx();
            reegDao.Save(reeg, '');
        }
        
        PageReference pg = new PageReference('/apex/phy_PeerViewS2?id=' + this.reegId);
        pg.setRedirect(true);
        return pg;
        
        //return getDestPage();
    }
    
    public PageReference cancel()
    {
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
        
        PageReference pg = new PageReference('/apex/phy_PeerViewS2?id=' + this.reegId);
        pg.setRedirect(true);
        return pg;
    } 

  
    
    
    
   
    private boolean validateFields()
    {
        boolean returnVal = false;
        
        return true;
    }
    
 
  
    
    public List<rEEGDxDisp> getDxs()
    {
        if (dxs== null)
        {
            loadFullList();
            loadSavedList();                
        }    
        return dxs;
    }
    
    public List<rEEGDxGroupDisp> getDxGroupsL()
    {
        getDxGroups();
        return dxGroupsL;
    }
    public List<rEEGDxGroupDisp> getDxGroupsR()
    {
        getDxGroups();
        return dxGroupsR;
    }
    
    public List<rEEGDxGroupDisp> getDxGroups()
    {
        if (dxGroups== null)
        {
            getDxs();
            loadGroupList();
        }    
        return dxGroups;
    }
    
    private void loadFullList()
    {  
        dxs = new List<rEEGDxDisp>();
    
        for(ref_dx__c refRow : 
                 [select description__c, is_group__c, primary__c, rule_out__c, secondary__c, sequence__c, id, name 
                     from ref_dx__c
                     order by sequence__c])
        {
            rEEGDxDisp dx = new rEEGDxDisp();
            dx.SetupRef( refRow );     
            dx.Setup(new r_reeg_dx__c());                                        
            dxs.Add( dx);
        } 
    }
    
   private void loadSavedList()
    {
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            for(r_reeg_dx__c dxRow : 
                 [select dx_name__c, primary__c, r_ref_dx_id__c, rule_out__c, secondary__c, id, name 
                     from r_reeg_dx__c
                     where r_reeg_id__c = :reegId limit 5])
            {
                AddSavedToList(dxRow);
            }
        } 
    }
    
    private void AddSavedToList(r_reeg_dx__c o)
    {
        for(rEEGDxDisp dxRow : dxs)
        {
            if (dxRow.getRefObj() != null && dxRow.getRefObj().id == o.r_ref_dx_id__c)
            {
                dxRow.Setup(o);
            }
        }
    }
    
    private void loadGroupList()
    {
        dxGroups = new List<rEEGDxGroupDisp>();
        
        rEEGDxGroupDisp currGroup = null;
        Integer lastGroupId = 0;
        Integer totalItemCount = 0;
                
        for(rEEGDxDisp dxRow : dxs)
        {
            if (dxRow.getRefObj().is_group__c)
            {
                currGroup = new rEEGDxGroupDisp();
                currGroup.Setup( dxRow.getObj());
                currGroup.SetupRef( dxRow.getRefObj());
                
                dxGroups.Add(currGroup);
            }
            else
            {
                if (currGroup != null) 
                {
                    totalItemCount++;
                    currGroup.addItem( dxRow);
                }
            }
        }
               
        dxGroupsL = new List<rEEGDxGroupDisp>();
        dxGroupsR = new List<rEEGDxGroupDisp>();
 
        Integer addedItemCount = 0;
        
        for(rEEGDxGroupDisp groupRow : dxGroups)
        {
            if (addedItemCount > (totalItemCount/2))
            {
                dxGroupsR.Add(groupRow);    
            }
            else
            {
                dxGroupsL.Add(groupRow);
                if (groupRow != null && groupRow.getItemCount() != null)
                	addedItemCount += groupRow.getItemCount();
                else
                	addedItemCount = (addedItemCount == null) ? 0 : addedItemCount;
            }
        }       
    }  
    
    public Boolean saveDx()
    {          
        saveDxGroup( dxGroupsL );
        saveDxGroup( dxGroupsR );

        return true;
    } 

    private void SaveReeg(r_reeg__c reeg)
    {
        if (reeg.id != null) upsert reeg;
        else insert reeg;
    }
    
    private void saveDxGroup(List<rEEGDxGroupDisp> groupList)
    {
        if (groupList!= null)
        {
            for(rEEGDxGroupDisp grpRow : groupList)
            {
                for(rEEGDxDisp dxRow : grpRow.getDxs())
                {   
                    if (dxRow == null)
                    {
                        addMessage( 'DxRow is null');         
                    }
                    else if (dxRow.getObj() == null)
                    {
                        addMessage( 'DxRow.getObj() is null');         
                    }
                    else
                    {
                        SaveDx(dxRow.getObj(), dxRow.getRefObj());
                    }
                }
            }
        }
    }
    
    public void SaveDx( r_reeg_dx__c obj, ref_dx__c refObj)
    {
        Boolean hasValue = (obj.primary__c || obj.secondary__c || obj.rule_out__c);

        if (hasValue)
        {
             if (obj.id != null)
             {
                upsert obj;
             }
             else
             {
                 obj.r_reeg_id__c = reegId;
                 obj.r_ref_dx_id__c = refObj.id; 
                 obj.dx_name__c = refObj.description__c; 
                 obj.name = refObj.name; 

                 insert obj;
             }                
        }
        else
        {
            if (obj.id != null) delete obj;
        }    
    }
    
    private void addMessage(String value)
    {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.INFO, value);
        ApexPages.addMessage( msg);
    }

          //---TEST METHODS ------------------------------
    public static testMethod void testDxController()
    {
        phy_DxEdit cont = new phy_DxEdit();
        cont.IsTestCase = true;
        r_reeg__c reeg = rEEGDao.getTestReeg();
        cont.reeg = reeg;
        patientDao patDao = new patientDao();
        patDao.IsTestCase = true;
        
        cont.patient = patDao.getById(reeg.r_patient_id__c);
        
        patientOutcomeDao patOutcomeDao = new patientOutcomeDao();
        r_patient_outc__c outcome = patOutcomeDao.getTestOutcome(reeg.r_patient_id__c);
        patientOutcomeMedDao medDao = new patientOutcomeMedDao();
        medDao.IsTestCase = true;
        r_outcome_med__c outMed =  medDao.getTestOutcomeMed(outcome.Id);    
        
        cont.outcomeId = ApexPages.currentPage().getParameters().put('oid', outcome.Id);
        cont.patId = ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
        ApexPages.currentPage().getParameters().put('wiz', 'true');
        ApexPages.currentPage().getParameters().put('isNew', 'true');
        ApexPages.currentPage().getParameters().put('np', 'true');
        ApexPages.currentPage().getParameters().put('isNew', 'true');
        
        PageReference pRef = ApexPages.currentPage();
        pRef.getParameters().put('id',reeg.id);
        
        rEEGDxDao dxDao = new rEEGDxDao();
        dxDao.IsTestCase = true;
        r_reeg_dx__c dx = dxDao.getTestDx();
        dx = dxDao.getTestDx();
        cont.setup();
        
        cont.SaveReeg(reeg);
        cont.reegId = reeg.Id;
        PageReference pRef1 = cont.save();
        PageReference pRef2 = cont.cancel();

        
        cont.isSubmitVerified = true;
        cont.reegId = reeg.Id;
        reeg.reeg_type__c = 'Type II';
        rEEGDao rDao = new rEEGDao();
        rDao.saveSObject(reeg);
      
        
     
        List<rEEGDxDisp> dxList = cont.getDxs();
        List<rEEGDxGroupDisp> DxGroupDisp = cont.getDxGroupsL();
        DxGroupDisp = cont.getDxGroupsR();
        DxGroupDisp = cont.getDxGroups();
        

   

        cont.RemoveReeg(null);

        System.assertEquals( 1, 1);
    }

}