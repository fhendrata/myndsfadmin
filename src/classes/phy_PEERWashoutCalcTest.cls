//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: phy_PEERWashoutCalcTest
// PURPOSE: Controller class for PEERWashoutCalcTest page
// CREATED: 08/10/16 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class phy_PEERWashoutCalcTest 
{
	private static final Integer MAX_NEW_MED_ROWS = 5;
	public PEER_Requisition__c ReqObj { get; set; }
    public List<ReqOutcomeMedDisp> NewMeds  { get; set; }
    public List<Req_Outcome_Medication__c> CurrentMedList { get; set; } 
    public Integer NewRowCount { get; set; }
    public Integer RowCount { get; set; }
    public Boolean WashoutErr { get; set; }
    public Boolean HasWashOutConfirmError { get; set; }
    public String IsWashedOutStr { get; set; }
    public Map<String, Integer> RefDrugMap { get; set; }

    //-- since there is the possibility that the radio button has not yet been selected both of these can be false. that is why there are two of them that are not always opposite.
    public Boolean IsWashedOut {  get { return !String.isBlank(IsWashedOutStr) && IsWashedOutStr == 'yes';  }  }
    public Boolean IsNotWashedOut {  get { return !String.isBlank(IsWashedOutStr) && IsWashedOutStr == 'no';  }  }

    public phy_PEERWashoutCalcTest() 
    {
		 RefDrugMap = new Map<String, Integer>();

	}

	public phy_PEERWashoutCalcTest(ApexPages.StandardController stdController) 
    {
        this.ReqObj = (PEER_Requisition__c)stdController.getRecord();

        if (String.isNotBlank(ReqObj.Id))
            ReqObj = PEERRequisitionDao.getInstance().getById(ReqObj.Id);

        ReqObj.test_data_flag__c = (String.isNotBlank(UserInfo.getUserName()) && UserInfo.getUserName().ToLowerCase().contains('@ethos.com'));

        RefDrugMap = new Map<String, Integer>();
    }

    public void loadAction() 
    {
    	
    }

    public Boolean SkipValidation
    {
        get {
            try {
                User u = [select Id, Can_Skip_Med_Validation__c from User where Id =: UserInfo.getUserId()];
                return u.Can_Skip_Med_Validation__c;    
            }catch(Exception e){}
            return false;
        }
    }

    public void loadPreviousMeds()
    {
        Integer oMedCount = 0;
        if (NewMeds == null) NewMeds = new List<ReqOutcomeMedDisp>();         
        Integer ctr = -1;

        if (ReqObj.Id != null)
            CurrentMedList = ReqOutcomeMedDao.getInstance().getByRequisitionId(ReqObj.Id);

        if (CurrentMedList != null && !CurrentMedList.isEmpty())
        {
            for(Req_Outcome_Medication__c oMedRow : CurrentMedList) 
                addMedToNewList(oMedRow, ctr, null);                
        }
        else
        {
            List<r_outcome_med__c> prevMeds = patientOutcomeMedDao.getInstance().getExistingOutcomeMeds(ReqObj.Patient_Id__c);
            if (prevMeds != null && !prevMeds.isEmpty())
            {
                for(r_outcome_med__c oMedRow : prevMeds) 
                    addMedToNewList(new Req_Outcome_Medication__c(), ctr, oMedRow.Id);   
            }
                                                       
        }

        oMedCount = ctr + 1;    //---ctr is zero based   
        RowCount = ctr + 1; //---ctr is zero based 
    }

    private void addMedToNewList(Req_Outcome_Medication__c oMedRow, Integer ctr, String previousMedId)
    {
        ctr++;  
        ReqOutcomeMedDisp oMed = new ReqOutcomeMedDisp(oMedRow, oMedRow.med_name__c, oMedRow.dosage__c, oMedRow.unit__c, oMedRow.frequency__c, oMedRow.start_date__c, oMedRow.end_date__c, previousMedId, oMedRow.start_date_is_estimated__c);
        oMed.setDisplayAction(true);
        oMed.setRowNum(ctr);                  
        NewMeds.Add(oMed);  
    }

    public void addBlankMedRows()
    {
        if (NewMeds == null) NewMeds = new List<ReqOutcomeMedDisp>();    
                
        for (integer i = 0; i < MAX_NEW_MED_ROWS; i++)
        {
            ReqOutcomeMedDisp medDisp = new ReqOutcomeMedDisp();
            Req_Outcome_Medication__c newMed = new Req_Outcome_Medication__c();
            newMed.unit__c = 'mg';  
            medDisp.Obj = newMed;     
            medDisp.setRowNum(newMeds.size());
            NewMeds.add(medDisp);       
        }
        NewRowCount = NewMeds.size();
    }

    public void forwardCalc()
    {
        Date latestDate = Date.today();

        if (!NewMeds.isEmpty())
        {
            loadRefDrugMap();

            for (ReqOutcomeMedDisp medWrapper : NewMeds)
            {
                if (String.isNotBlank(medWrapper.Obj.med_name__c))
                {
                    medWrapper.Obj.washout_days__c = getWashoutDays(medWrapper.Obj.med_name__c, medWrapper.Obj.washout_days__c);
                    medWrapper.WashedOutDate = (medWrapper.Obj.end_date__c == null) ? Date.today().addDays(Integer.valueOf(medWrapper.Obj.washout_days__c) + 1) : medWrapper.Obj.end_date__c.addDays(Integer.valueOf(medWrapper.Obj.washout_days__c) + 1);

                    if (medWrapper.WashedOutDate.daysBetween(latestDate) < 0)
                        latestDate = medWrapper.WashedOutDate;
                }
            }

            for (ReqOutcomeMedDisp medWrapper : NewMeds)
            {
                if (String.isNotBlank(medWrapper.Obj.med_name__c))
                {
                    if (medWrapper.WashedOutDate.daysBetween(latestDate) < 0)
                        latestDate = medWrapper.WashedOutDate;

                    if (medWrapper.Obj.end_date__c == null)
                        medWrapper.Obj.end_date__c = latestDate.addDays((Integer.valueOf(medWrapper.Obj.washout_days__c) + 1) * -1);
                }
            }
        }

        ReqObj.patient_anticipated_eeg_test__c = latestDate;
    }

    public void backwardsCalc()
    {
        Date todayDate = Date.today();

        if (!NewMeds.isEmpty() && ReqObj.patient_desired_eeg_test__c != null && ReqObj.patient_desired_eeg_test__c > todayDate.addDays(-1))
        {
            loadRefDrugMap();

            for (ReqOutcomeMedDisp medWrapper : NewMeds)
            {
                if (String.isNotBlank(medWrapper.Obj.med_name__c))
                {
                    medWrapper.Obj.washout_days__c = getWashoutDays(medWrapper.Obj.med_name__c, medWrapper.Obj.washout_days__c);     
                    medWrapper.Obj.end_date__c = ReqObj.patient_desired_eeg_test__c.addDays((Integer.valueOf(medWrapper.Obj.washout_days__c)) * -1).date();

                    if (medWrapper.Obj.end_date__c < todayDate)
                        medWrapper.WashoutErr = true;
                }
            }
        }
    }

    private Decimal getWashoutDays(String medName, Decimal currentWashoutDaysValue)
    {
        Decimal returnVal = currentWashoutDaysValue;

        if (RefDrugMap.containsKey(medName))
        {
            if (returnVal == null || (RefDrugMap.get(medName) > currentWashoutDaysValue))
                returnVal = RefDrugMap.get(medName);
        }
        else
        {
            if (returnVal == null)
                returnVal = 0;
        }

        return returnVal;
    }

    public boolean getWashoutConfirmError()
    {
        HasWashOutConfirmError = false;
        boolean medsSpecified = isPatOutcMedListed();
        HasWashOutConfirmError = ((ReqObj.No_Medication_Verified__c && medsSpecified) || (!ReqObj.No_Medication_Verified__c && !medsSpecified));

        return HasWashOutConfirmError;
    }

    private boolean isPatOutcMedListed()
    {
        for(ReqOutcomeMedDisp nMedRow : NewMeds) 
        {
            if (nMedRow.isEmpty()) return true; 
        }

        return false;
    }

    //-- Checks if there are any misspelled medications and updates the is_misspelled__c checkbox on the Req_Outcome_Medication__c object to true if there are
    public void checkMedNames(List<Req_Outcome_Medication__c> medList, Set<String> medSet)
    {
        if (medList != null && medList.size() > 0)
        {
            for(Req_Outcome_Medication__c med : medList)
            {
                if (med != null && String.isNotBlank(med.Name))
                    med.is_misspelled__c = !medSet.contains(med.Name.ToLowerCase());
            }
        }
    }
    
    public void saveOutcomeMeds()
    {
        if (NewMeds != null && NewMeds.size() > 0)
        {
            List<Req_Outcome_Medication__c> outCMedsToSave = new List<Req_Outcome_Medication__c>();
            List<Req_Outcome_Medication__c> outCMedsToDelete = new List<Req_Outcome_Medication__c>();
            Set<String> dbMedSet = new Set<String>();

            for(ref_drug__c med : [SELECT Name, generic__c, trade__c FROM ref_drug__c])
            {
                if (String.isNotBlank(med.Name))
                    dbMedSet.add(med.Name.toLowerCase());
                if (String.isNotBlank(med.generic__c))
                    dbMedSet.add(med.generic__c.toLowerCase());
                if (String.isNotBlank(med.trade__c))
                    dbMedSet.add(med.trade__c.toLowerCase());
            }

            for(ReqOutcomeMedDisp nMedRow : NewMeds) 
            {
                nMedRow.Obj.PEER_Requisition__c = ReqObj.Id;
                if (nMedRow.isEmpty())
                {
                    if (nMedRow.Obj.Id != null)
                        outCMedsToDelete.add(nMedRow.Obj);
                }
                else
                {
                    if (String.isBlank(nMedRow.Obj.Name))
                        nMedRow.Obj.Name = nMedRow.Obj.med_name__c;

                    outCMedsToSave.add(nMedRow.Obj);
                }
                
            }

            if (!outCMedsToSave.isEmpty())
            {
                checkMedNames(outCMedsToSave, dbMedSet);
                upsert outCMedsToSave;
            }

            if (!outCMedsToDelete.isEmpty())
                delete outCMedsToDelete;

        }
    
    }

    private void refreshMeds()
    {
        loadPreviousMeds();
        addBlankMedRows();
    }
    
    public PageReference displayWashoutError()
    {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, 'Will the patient be washed out of all medications?');
        ApexPages.addMessage( msg);
        WashoutErr = true;
        return null;
    }

    private void loadRefDrugMap()
    {
        if (RefDrugMap.isEmpty())
        {
            for (ref_drug__c drug : [Select id, name, half_life_days__c,half_life_hours__c from ref_drug__c])
            {
                Integer addOnDay = (drug.half_life_hours__c != null && drug.half_life_hours__c > 0) ? 1 : 0;
                Integer woDays = (drug.half_life_days__c != null) ? Integer.valueOf(drug.half_life_days__c) : 0;
                RefDrugMap.put(drug.name, woDays + addOnDay);
            }
        }
    }

}