public with sharing class uploadWizardController6 extends BaseUploadWizController
{   
	public boolean isSubmitVerified { get; set; }
	public Contact eegTech {get; set;}
	public boolean isEegUploaded {get; set;}
    
    public uploadWizardController6()
    {
    	init();
    }
    
    public void init()
    {
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.qsid = ApexPages.currentPage().getParameters().get('qsid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        string strIsNewPat = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(strIsNewPat) && strIsNewPat == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        String strIsNew = ApexPages.currentPage().getParameters().get('isNew');
        this.IsNew = (!e_StringUtil.isNullOrEmpty(strIsNew) && strIsNew == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        
        rDao = new rEEGDao();
        pDao = new patientDao();
        isSubmitVerified = false;
    }
    
    public PageReference loadAction()
    {
    	if (!e_StringUtil.isNullOrEmpty(patId))
    	{
    		this.patient = pDao.getById(patId);
    	}
    	
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
   			this.reeg = rDao.getById(reegId, true);
    	}
    	
    	if (reeg != null)
		{
			if (!e_StringUtil.isNullOrEmpty(reeg.r_eeg_tech_contact__c))
			{
				ContactDao cDao = new ContactDao();
    			eegTech = cDao.getById(reeg.r_eeg_tech_contact__c);  				
   				if (eegTech != null)
	    		{
	    			reeg.eeg_make__c = eegTech.eeg_tech_default_test_make__c;
   				}
			}
			
			isEegUploaded = (!e_StringUtil.isNullOrEmpty(reeg.eeg_rec_stat__c) && reeg.eeg_rec_stat__c == 'Received');
		}
		
		return null;
    }
    
    public string getEnv()
	{
		return rEEGUtil.getEnvString();
	}
    
    public PageReference wizStep5()
    {
       	PageReference returnVal = new PageReference('/apex/uploadWizard5');
       	AddParameters(returnVal);
               	
        returnVal.setRedirect(true);
        return returnVal;
    } 
    
   	public PageReference submit()
    {
    	PageReference returnVal = null;
		if (eegTech != null)
		{
			eegTech.eeg_tech_default_test_make__c = reeg.eeg_make__c;
			ContactDao cDao = new ContactDao();
			cDao.saveSObject(eegTech);	
		}
			
		reeg = rDao.getById(reeg.Id, true);		
		if (!e_StringUtil.isNullOrEmpty(reeg.eeg_upload__c) && reeg.eeg_upload__c == 'Yes')
		{
			isEegUploaded = true;
		}
		else
		{
			string errorMsg = 'The EEG file needs to be uploaded before submitting';
			ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, errorMsg);
        	ApexPages.addMessage(msg);
		}
		
		if (isEegUploaded)
		{
			reeg.eeg_rec_stat__c = 'Received';
			reeg.eeg_rec_date__c = DateTime.now();
			reeg.Translate_Status__c = 'Pending';
			reeg.rpt_stat__c = 'In Progress';  
			reeg.req_stat__c = 'In Progress';
			rDao.Save(reeg, 'UPDATE_DB_STATUS');
			
			returnVal = new PageReference('/' + reeg.Id);
		}
	    
	    return returnVal; 
    } 
    
    public PageReference cancelWizOverride()
    {
    	PageReference pr = cancelWizAction();
    	pr = new PageReference('/apex/UploadWizard1');
    	pr.setRedirect(true);
        return pr;
    }       
	        
	    //---TEST METHODS ------------------------------
    public static testMethod void testUploadController()
    {
    	uploadWizardController6 cont = new uploadWizardController6();
		cont.IsTestCase = true;
    	r_reeg__c reeg = rEEGDao.getTestReeg();
        cont.reeg = reeg;
        patientDao patDao = new patientDao();
        patDao.IsTestCase = true;
        
        cont.patient = patDao.getById(reeg.r_patient_id__c);
        
        patientOutcomeDao patOutcomeDao = new patientOutcomeDao();
        r_patient_outc__c outcome = patOutcomeDao.getTestOutcome(reeg.r_patient_id__c);
        patientOutcomeMedDao medDao = new patientOutcomeMedDao();
        medDao.IsTestCase = true;
        r_outcome_med__c outMed =  medDao.getTestOutcomeMed(outcome.Id);    
        
 		cont.outcomeId = ApexPages.currentPage().getParameters().put('oid', outcome.Id);
 		cont.patId = ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
 		ApexPages.currentPage().getParameters().put('wiz', 'true');
 		ApexPages.currentPage().getParameters().put('isNew', 'true');
		ApexPages.currentPage().getParameters().put('np', 'true');
		ApexPages.currentPage().getParameters().put('isNew', 'true');
    	
    	PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('id',reeg.id);
    	
    	rEEGDxDao dxDao = new rEEGDxDao();
    	dxDao.IsTestCase = true;
    	r_reeg_dx__c dx = dxDao.getTestDx();
    	dx = dxDao.getTestDx();

		cont.loadAction();
		
		rEEGDao rDao = new rEEGDao();
		rDao.saveSObject(reeg);
		
		cont.reegId = reeg.Id;
        PageReference pRef4 = cont.wizStep5();
        
        cont.isSubmitVerified = true;
        cont.reegId = reeg.Id;
        reeg.reeg_type__c = 'Type II';
        rDao.saveSObject(reeg);
        
        cont.submit();
        cont.cancelWizOverride();
   
        System.assertEquals( 1, 1);
    }
}