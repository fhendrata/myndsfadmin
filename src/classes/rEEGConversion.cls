global class rEEGConversion implements Database.Batchable<sObject> 
{
    public final String query = 'select Id, cgi__c, rEEG__c from r_patient_outc__c';

    global Database.QueryLocator start(Database.BatchableContext BC) 
    {  
        return Database.getQueryLocator(query);
    }
	
	global void execute(Database.BatchableContext BC,  List<sObject> batch) 
	{  
		
	    for(sobject s : batch)
	    {
	    	Object oOutcome = s.get('cgi__c');
	    	String outcome = String.valueOf(oOutcome);
	    	
	    	Object oOutcomeId = s.get('Id');
	    	String outcomeId = String.valueOf(oOutcomeId);
	    	
	    	Object oREEG = s.get('rEEG__c');
	    	String reegId = String.valueOf(oREEG);
	    	
	    	//check if it's a baseline outcome first so we don't have to
	    	//cross object check on every outcome in the database
	    	if(outcome == '4 - No change from baseline')
	    	{
		    	if(reegId != null)
		    	{
		    		r_reeg__c reeg = [select Id,Name,req_date__c from r_reeg__c where Id =: reegId];
		    		r_patient_outc__c outc = [select Id,CreatedDate from r_patient_outc__c where Id =: outcomeId];
		    		   	   
		    	    DateTime d1 = outc.CreatedDate; 
		    	    DateTime d2 = reeg.req_date__c;
		    	    
		    	    if(d1 != null && d1 != null)
		    	    {
		    	     	//check if the reeg req date is the same as the creation date
		    	        //of the outcome
		    	    	if(d1.isSameDay(d2))
		    	    	{
                            //if it is, switch the cgi to the new value
		    	            s.put('cgi__c','4 - Baseline');
		    	            system.debug('### rEEGConversion: ' + reeg.Name + ' converted.');	
		    	    	}
		    	    } 
		    	}
		    	
	    	}
	    }
        //commit changes
	    update batch;
	    
	}
	
	global void finish(Database.BatchableContext BC) 
	{
    	//don't have to do anything here
	}


}