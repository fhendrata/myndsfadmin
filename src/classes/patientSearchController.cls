public with sharing class patientSearchController extends BaseController
{
    private String patId;
    private String qsid;
    private String wizardTab;
    private String tid;
    private String reegId;
    public string outcomeId { get; set; }
    private boolean isWR {get; set;}
    public boolean isTest {get; set;}
    public boolean isWiz {get; set;}
    public boolean isNew {get; set;}
    public boolean isNewPat {get; set;}
    
    private patientDao pDao {get; set;}
    
    public r_patient__c patient {get; set;}
    public r_reeg__c reeg {get; set;}
    public string firstname {get; set;}
    public string lastname {get; set;}
    public string dob {get; set;}
    public List<r_patient__c> patAllList {get; set;}
    public boolean showResults {get; set;}
    public boolean isFirstSearchDone {get; set;}

	//---Constructor
    Public patientSearchController()
    {    
        //init();
    }
    
    //---Constructor
    Public patientSearchController(ApexPages.StandardController stdController)
    {    
        //-- jdepetro 11.16.17
        //-- NOTE - This page is no longer being used to enter patient/rEEG records. We are now using the PEERRequisition to order PEER's. Since the standard "New PEER" button
        //-- must navigate to a controller with the r_reeg__c standard controller and is not able to go to the PEERRequisition__c controller we use this interim page for 
        //-- that redirect purpose.
       // this.reeg = (r_reeg__c)stdController.getRecord();
       // init();
    }

    public PageReference peerRequsitionRedirect()
    {
        return Page.PEERRequisition;
    }
    
    public void init()
    {
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.qsid = ApexPages.currentPage().getParameters().get('qsid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
    	string newParam = ApexPages.currentPage().getParameters().get('isNew');
    	this.isNew = (!e_StringUtil.isNullOrEmpty(newParam) && newParam == 'true');
    	string newPatStr = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        string testStr = ApexPages.currentPage().getParameters().get('test');
        this.isTest = (!e_StringUtil.isNullOrEmpty(testStr) && testStr == 'true');
        isFirstSearchDone = false;
        showResults = false;
        isWiz = true;
        pDao = new patientDao();
    }
    
    private Boolean newPatBtnVisible;
    public Boolean getIsNewPatBtnVisible()
    {
    	return newPatBtnVisible;
    }

    public String getQsId()
    {
        return qsid;
    }

    public Boolean getDOBRequired()
    {
        return (qsid == 'null');
    }

    private List<r_patient__c> getMatchingPatients()
    {
        patAllList = new List<r_patient__c>();
        boolean fnameOk = !e_StringUtil.isNullOrEmpty(firstname);
        boolean lnameOk = !e_StringUtil.isNullOrEmpty(lastname);
        if (!e_StringUtil.isNullOrEmpty(dob))
        {
        	if (dob == 'mm/dd/yyyy')
        		dob = '';
        }
        
        boolean dobOk = !e_StringUtil.isNullOrEmpty(dob);
        
        if (fnameOk || dobOk || lnameOk) 
        {
        	patAllList = pDao.getAllSearchResults(lastname, firstname, dob);
        }
        else
        {
        	addMessage('At least one field must be entered.');
        }
        
        return patAllList;
    }
    
    public void search2()
    {
    }

    public PageReference search1()
    {
		isFirstSearchDone = true;
        patAllList = getMatchingPatients();

       	showResults = (patAllList != null && patAllList.size() > 0);
        
        return null;
    }

    public PageReference newPatient()
    {
        if (lastname == null || firstname == null || lastname == '' || firstname == '') 
        {
            addMessage('First name and last name fields must be completed');
            return null;
        }
        else
        {
            AddPatient();
            isNewPat = true;

            if (patient != null && patient.id != null)
            {
                PageReference returnVal = new PageReference( '/apex/patientWizard1Page');
                if (!e_StringUtil.isNullOrEmpty(reegId))
	    	    	returnVal.getParameters().put('rid', reegId);
                else if (reeg != null && !e_StringUtil.isNullOrEmpty(reeg.id))
	        		returnVal.getParameters().put('rid', reeg.id);
		        if (!e_StringUtil.isNullOrEmpty(patId))
	    	    	returnVal.getParameters().put('id', patId);
	        	else if (patient != null && !e_StringUtil.isNullOrEmpty(patient.Id))
	        		returnVal.getParameters().put('id', patient.Id);
	        	if (!e_StringUtil.isNullOrEmpty(outcomeId))
	        		returnVal.getParameters().put('id', outcomeId);
		        if (!e_StringUtil.isNullOrEmpty(tid))
	    	    	returnVal.getParameters().put('tid', tid);
	        	if (!e_StringUtil.isNullOrEmpty(qsid))
	        		returnVal.getParameters().put('qsid', qsid);
	        	if (isNewPat != null && isNewPat)
	        		returnVal.getParameters().put('np', 'true');
	        	if (isWiz != null && isWiz)
		        	returnVal.getParameters().put('wiz', 'true');
	        	if (isNew != null && isNew)
		        	returnVal.getParameters().put('isNew', 'true');
	            if (isWR != null && isWR)
	        		returnVal.getParameters().put('wr', 'true');
                
                returnVal.setRedirect(true);
                return returnVal;
            }
            else
            {
                return Page.patientSearchPage;
            }
        }
    }

    public PageReference patStep1()
    {
        if (lastname == null || firstname == null || dob == null) 
        {
            addMessage('All fields must be completed.');
            return null;
        }
        else
        {
            if (patient == null || patient.id == null)
            {
                //-- lookup pat fname, lname and dob 
                for(r_patient__c patRow : [select id, name from r_patient__c 
                                                where name = :lastname AND first_name__c = :firstname AND dob__c = :dob])
                {
                    patient = patRow;
                }

                if (patient == null || patient.id == null && lastname != null)
                {
                    AddPatient();
                    isNewPat = true;
                }
            }

            if (patient != null && patient.id != null)
            {
                PageReference returnVal = new PageReference( '/apex/patientWizard1Page?id=' + patient.id + '&qsid=' + qsid + '&rid=' + reegId + '&tid=' + tid + '&np=' + isNewPat + '&wr=' + isWR + '&wiz=true');
                returnVal.setRedirect(true);
                return returnVal;
            }
            else
            {
                return Page.patientWizard1Page;
            }
        }
    }

    private void AddPatient()
    {
        patient  = new r_patient__c();
        patient.name = lastName;
        patient.first_name__c = firstname;
        patient.dob__c = dob;
        patient.test_data_flag__c = isTest;
        
        if (!e_StringUtil.isNullOrEmpty(dob))
        {
        	Date dobDt = e_StringUtil.setDateFromString(dob);
        	if (dobDt != null)
        	{
        		integer numberDaysOld = dobDt.daysBetween(Date.today());
        		if (numberDaysOld > 0)
        			patient.patient_age__c = (numberDaysOld / 365.242199);
        		else
        			System.debug('### AddPatient:numberDaysOld: ' + numberDaysOld);
        	}
        	else
        	{
        		System.debug('### AddPatient:dobDt is null');
        	}
        	
        }
        
/*
		if (true || getIsPortalUser3() || getIsPortalUser2() || getIsPortalUser1())
		{
			if (e_StringUtil.isNullOrEmpty(patient.physician__c))
			{
				UserDao uDao = new UserDao();
				User usr = uDao.getById(UserInfo.getUserId());
				if (usr != null)
				{
					patient.physician__c = usr.ContactId;
				}
			}
		}
  */      
        pDao.saveSObject(patient);
    }

    public PageReference cancelWizard()
    {
        if (patient != null && patient.id != null)
        {
            if (ApexPages.currentPage().getParameters().get('np') == 'true') delete patient;
        }

        PageReference returnVal = new PageReference('/apex/patientSearchPage?wtf');
        returnVal.setRedirect(true);

        return returnVal;
    }

       //---Get a handle back to the patient page
    private PageReference getDestPage()
    {
        PageReference returnVal = new PageReference( '/' + ApexPages.currentPage().getParameters().get('id'));
        returnVal.setRedirect(true);
        return returnVal;
    }

    public void SavePatient()
    {
        if (patient != null)
        { 
        	if (patient.id != null) upsert patient;
        	else insert patient;
        }
    }

          //---Add a message to the message list
    private void addMessage(String value)
    {
        String message = e_StringUtil.isNullOrEmpty(value) ? 'All fields must be completed' : value; 
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, message);
        ApexPages.addMessage(msg);
    }

        //---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
        patientSearchController cont = new patientSearchController();
		cont.IsTestCase = true;
		r_reeg__c reeg = rEEGDao.getTestReeg();
		ApexPages.currentPage().getParameters().put('rid', reeg.Id);
		ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
		cont.init();

		boolean testBool = cont.getIsNewPatBtnVisible();
		string testStr = cont.getQsId();
		testBool = cont.getDOBRequired();
		
		cont.search2();
		
		PageReference pRef1 = cont.search1();
		pRef1 = cont.newPatient();
		pRef1 = cont.patStep1();

		patientDao patDao = new patientDao();
		r_patient__c pat = patDao.getById(reeg.r_patient_id__c);
		cont.patient = pat;	
		cont.SavePatient();
		
		cont.lastname = 'test';
		cont.firstname = 'test';
		
		pRef1 = cont.newPatient();
		
        PageReference pRef4 = cont.cancelWizard();

        System.assertEquals( 1, 1);
    }
}