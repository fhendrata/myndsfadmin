public with sharing class wr_OutcomeListController extends BaseController
{
    private string searchCriteria {get; set;}
    private integer resultTableSize {get; set;}
    public string newFilterId {get; set;}
    public ApexPages.StandardSetController con {get; set;}
    
    public Boolean hasNext { get { return con.getHasNext(); } set; }  
    public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }  
    public Boolean hasResults { get { return (resultNumber > 0); } set; }  
    public Integer pageNumber  { get { return con.getPageNumber(); } set; }
    public Integer resultNumber  { get { return con.getResultSize(); } set; }  
    public void previous() { con.previous(); }  
    public void next() { con.next(); }
    
    public String patSearchString {get; set;}  
    public patientOutcomeDao oDao;
    
    private String sortDirection { get; set; }
    private String sortExp { get; set; }
    public String sortExpression 
    { 
    	get { return sortExp; }
	    set {
	       if (value == sortExp)
	         sortDirection = (sortDirection == 'ASC') ? 'DESC' : 'ASC';
	       else
	         sortDirection = 'ASC';
	       sortExp = value;
	    }
   	}
    
    public wr_OutcomeListController()
    {
    	oDao = new patientOutcomeDao();
        resultTableSize = RESULT_TABLE_SIZE;
        
        setupFilter();
        loadOutcomes(); 
    }
    
    private void setupFilter()
    {
    	sortExp = 'LastModifiedDate';
    	sortDirection = 'DESC';
    	
        //-- jdepetro.8.26.14 - this was eliminating the patient sharing between dr's. The clinic admin is still not allowed patient sharing.
        //if(getIsClinicPhy()) oDao.filter = 'patient__r.physician__c = ' + quote(getCurrentPhysician());
        if(getIsClinicAdmin()) oDao.filter = 'patient__r.physician__r.AccountId = ' + quote(getCurrentAccount());
        //else oDao.filter = 'patient__r.physician__c = ' + quote(getCurrentPhysician());
    }
    
    
    private PageReference loadOutcomes()
    {
    	String query = oDao.getMyOutcomesQuery(getCurrentPhysician(),' limit 10000 ',getOrderText());

        System.debug('### wr_OutcomeListController:loadOutcomes:query: ' + query);

        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
        return null;
    }
    
    public List<r_patient_outc__c> getOutcomes()
    {
        if(con != null)  
            return (List<r_patient_outc__c>)con.getRecords(); 
        else
            return null;
    }
     
    public void searchPatients()
    { 
        String query = oDao.getNameSearchQuery(patSearchString);

        System.debug('### wr_OutcomeListController:searchPatients:query: ' + query);

        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
    }
    
    public String getSortDirection()
	{
	    if (sortExpression == null || sortExpression == '')
	      return 'ASC';
	    else
	     return sortDirection;
	}
	
	public String getOrderText()
	{
	    return ' order by ' + sortExpression  + ' ' + sortDirection + '  NULLS LAST';
	}
    
    public void sortPatientName()
    {
    	sortExpression = 'patient__r.Name';
    	loadOutcomes();
    }
    
    public void sortPeerDate()
    {
    	sortExpression = 'rEEG__r.rpt_date__c';
    	loadOutcomes();
    }
    
    public void sortCgiPatient()
    {
    	sortExpression = 'cgi_patient__c';
    	loadOutcomes();
    }
    
    public void sortCgiPhy()
    {
    	sortExpression = 'cgi__c';
    	loadOutcomes();
    }
    
    public void sortCgs()
    {
    	sortExpression = 'cgs__c';
    	loadOutcomes();
    }
    
    
      //------------ TEST METHODS ------------------------
    
    private static testmethod void testController()
    {
        wr_OutcomeListController plc = new wr_OutcomeListController();
        plc.patSearchString = 'a';
     //   plc.searchPatients();
      //  plc.getOutcomes();
        
    
    }
    
    
    
    
    
}