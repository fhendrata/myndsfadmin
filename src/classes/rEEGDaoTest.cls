//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: rEEGDaoTest
// PURPOSE: Test class for rEEGDao class
// CREATED: 12/01/16 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class rEEGDaoTest 
{
	
	@isTest static void test_method_one() 
	{
		rEEGDao dao2 = new rEEGDao();
		String fieldStr = rEEGDao.getFieldStr();

		r_reeg__c reeg = rEEGDao.getTestReegPO2();

		r_reeg__c reeg2 = rEEGDao.getInstance().getById(reeg.Id);

		reeg2 = rEEGDao.getInstance().getById(reeg.Id, true);
		r_reeg__c reeg3 = rEEGDao.getInstance().getByIdWithPatPhyTechFields(reeg.Id);
		r_reeg__c reeg4 = rEEGDao.getInstance().getFieldsById('Id, Name', reeg.Id);	

		List<r_reeg__c> reegList =  rEEGDao.getInstance().getByName(reeg.Name);
		reegList =  rEEGDao.getInstance().getByPatientId(reeg.r_patient_id__c);
		reegList =  rEEGDao.getInstance().getCorrCompleteByPatientId(reeg.r_patient_id__c);
		reegList =  rEEGDao.getInstance().getRelatedTests(reeg.r_patient_id__c);

	}
	
	@isTest static void test_method_two() 
	{
		rEEGDao dao2 = new rEEGDao();
		r_reeg__c reeg = rEEGDao.getTestReegPO2();

		Contact c = ContactDao.getTestPhysicianPO2();
		rEEGPager pager = new rEEGPager();
		pager.techId = c.Id;

		List<reegDisp> reegDispList = rEEGDao.getInstance().getListForTech(pager);
		reegDispList = rEEGDao.getInstance().getListForClinic(pager);

		List<r_reeg__c> reegList =  rEEGDao.getInstance().getByPhyId(reeg.r_patient_id__r.physician__c, Datetime.now());
		reegList =  rEEGDao.getInstance().getByMonthYear(11, 2016);
		reegList =  rEEGDao.getInstance().getMostRecent('10');

		Boolean boolVal = rEEGDao.getInstance().hasPrevTest(reeg.r_patient_id__c);

		boolVal = rEEGDao.getInstance().Save(reeg, 'UPDATE_DB_STATUS');
		boolVal = rEEGDao.getInstance().SavePEEROnly(reeg, 'UPDATE_DB_STATUS');
		boolVal = rEEGDao.getInstance().Save(reeg, 'UPDATE_DB_STATUS', UserInfo.getUserId());
		boolVal = rEEGDao.getInstance().SavePEEROnly(reeg, 'UPDATE_DB_STATUS', UserInfo.getUserId());

		String queryStr = rEEGDao.getInstance().getNameSearchQuery('John');

		queryStr = rEEGDao.getInstance().getMyPeerListQuery('', ' limit 10', ' order by CReatedDate desc ');
	}
	
}