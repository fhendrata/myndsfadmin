public with sharing class uploadWizardController3 extends BaseUploadWizController
{
	public uploadWizardController3()
    {
    	init();
    }

    public void init()
    {
        this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.qsid = ApexPages.currentPage().getParameters().get('qsid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        string strIsNewPat = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(strIsNewPat) && strIsNewPat == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        String strIsNew = ApexPages.currentPage().getParameters().get('isNew');
        this.IsNew = (!e_StringUtil.isNullOrEmpty(strIsNew) && strIsNew == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        
        rDao = new rEegDao();
        pDao = new patientDao();
    }
    
    public PageReference loadAction()
    {
        if (!e_StringUtil.isNullOrEmpty(patId) && patient == null)
    	{
    		this.patient = pDao.getById(patId, true);
    	}
    	
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
   			this.reeg = rDao.getById(reegId);
    	}
    	else
    	{
    		this.reeg = new r_reeg__c();
    		reeg.med_no_med__c = true;
    	}
    	
    	if (!e_StringUtil.isNullOrEmpty(patId))
        {
            reeg.req_date__c = DateTime.Now();
        }
        
        if (patient != null && patient.physician__r != null 
    			&& patient.physician__r.Account != null && !e_StringUtil.isNullOrEmpty(patient.physician__r.Account.name))
    	{
    		if (patient.physician__r.Account.name == 'Walter Reed Army Medical Center')
    		{
    			isWR = true;
    			if (reeg != null)
    			{
    				reeg.eeg_make__c = 'EasyWindows (*.eas)';
    				reeg.eeg_loc__c = 'Walter Reed';
    			}
    		}	
    	}
    	
    	if(reeg != null)
    		reeg.eeg_type__c = 'Routine (95816 - 95819)';
    	
    	return null;
    }
    
    //------------ wizard button actions ----------------------------
    public PageReference wizStep2()
    {
       	PageReference returnVal = new PageReference('/apex/uploadWizard2');
       	AddParameters(returnVal);
               	
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference wizStep4()
    {
    	reeg.insert_method__c = 'Upload Wizard';
    	reeg.eeg_rec_stat__c = 'In Progress';
	        
	    if (!e_StringUtil.IsNullOrEmpty(reeg.phy_billing_code__c))
	    {
	     	string phyBillingCode = reeg.phy_billing_code__c;
	       	if (phyBillingCode.contains('Standard')) 
	       		reeg.billing_code__c = 'Standard - Charge';
	       	if (phyBillingCode.contains('Outcome Test')) 
	       		reeg.billing_code__c = 'Outcome Test - Charge';
	       	if (phyBillingCode.contains('Other')) 
	       		reeg.billing_code__c = 'Other – Charge';
	    }  
	         
	    
	    if (e_StringUtil.isNullOrEmpty(reeg.r_patient_id__c) && !e_StringUtil.isNullOrEmpty(patId))
        {
            reeg.r_patient_id__c = patId;
        }
        
        rDao.Save(reeg, 'NEW_REEG');
	        
	    reegId = reeg.Id;
	        
    	PageReference returnVal = new PageReference('/apex/uploadWizard4');
   	   	AddParameters(returnVal);
               	
       	returnVal.setRedirect(true);
       	return returnVal;
    }
    
    public PageReference cancelWizOverride()
    {
    	PageReference pr = cancelWizAction();
    	pr = new PageReference('/apex/UploadWizard1');
    	pr.setRedirect(true);
        return pr;
    }
    
          //---TEST METHODS ---------------------------------
    public static testMethod void testController()
    {
		r_reeg__c reeg = rEEGDao.getTestReeg();
    	patientDao patDao = new patientDao();
    	r_patient__c pat = patDao.getById(reeg.r_patient_id__c);
    	
    	uploadWizardController3 cont = new uploadWizardController3();
    	cont.loadAction();
    	PageReference pRef1 = cont.wizStep2();
        PageReference pRef2 = cont.wizStep4();
    	
    	ApexPages.currentPage().getParameters().put('id', pat.Id);
    	ApexPages.currentPage().getParameters().put('rid', reeg.Id);
		cont.loadAction();
        pRef1 = cont.wizStep2();
        pRef2 = cont.wizStep4();
        
        pRef2 = cont.cancelWizOverride();
    }
}