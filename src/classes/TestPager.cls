public with sharing class TestPager extends BasePager {


    public override void loadData()
    {
        Integer i = 0;
        
        List<String> strList = new List<String>();
        
        strList.add('Item1');
        strList.add('Item2');
        strList.add('Item3');
        strList.add('Item4');
        strList.add('Item5');
        strList.add('Item6');
        strList.add('Item7');
        strList.add('Item8');
        strList.add('Item9');
        strList.add('Item10');
        
        strList.add('Item1');
        strList.add('Item2');
        strList.add('Item3');
        strList.add('Item4');
        strList.add('Item5');
        strList.add('Item6');
        strList.add('Item7');
        strList.add('Item8');
        strList.add('Item9');
        strList.add('Item10');
        
        strList.add('Item1');
        strList.add('Item2');
        strList.add('Item3');
        strList.add('Item4');
        strList.add('Item5');
        strList.add('Item6');
        strList.add('Item7');
        strList.add('Item8');
        strList.add('Item9');
        strList.add('Item10');
        
        strList.add('Item1');
        strList.add('Item2');
        strList.add('Item3');
        strList.add('Item4');
        strList.add('Item5');
        strList.add('Item6');
        strList.add('Item7');
        strList.add('Item8');
        strList.add('Item9');
        strList.add('Item10');
        
        strList.add('Item1');
        strList.add('Item2');
        strList.add('Item3');
        strList.add('Item4');
        strList.add('Item5');
        strList.add('Item6');
        strList.add('Item7');
        strList.add('Item8');
        strList.add('Item9');
        strList.add('Item10');
        
        strList.add('Item1');
        strList.add('Item2');
        strList.add('Item3');
        strList.add('Item4');
        strList.add('Item5');
        strList.add('Item6');
        strList.add('Item7');
        strList.add('Item8');
        strList.add('Item9');
        strList.add('Item10');
        
        for(i = 0; i < 100; i++)
        {
        	system.debug('simulate load data');
        }
    
    }
    
    public static testmethod void testBasePager()
    {
    	
    	
        TestPager bp = new TestPager();
        bp.loadData();
        
        bp.setRecordCount(199);
        
        bp.getCurrCount();
        bp.setDispRows(2);
        bp.getDispRows();
        
        bp.setFirstRow(1);
        bp.getFirstRow();
        
        bp.setCurrentPage(4);
        bp.getCurrentPage();
        
        bp.setMaxPages(100);
        bp.getMaxPages();
        
        bp.getRenderPager();
        bp.getRenderNext();
        bp.getLimit();
        
        bp.shouldAddRow();
        bp.nextAction();
        bp.previousAction();
        
        bp.sortList('some field');
        
        
        
        bp.logMethod('testing');
        bp.debug('testing');
        
        
        bp.setClassName('className');       
        bp.getClassName();
    }

}