public abstract class BaserEEGDxDisp
{
    //---Reference object
    private r_reeg_dx__c obj;
    public r_reeg_dx__c getObj()
    {
        return obj;
    }
    public void setObj(r_reeg_dx__c s)
    {
        obj = s;
    }

    //---Reference object
    private ref_dx__c refObj;
    public ref_dx__c getRefObj()
    {
        return refObj;
    }
    public void setRefObj(ref_dx__c s)
    {
        refObj = s;
    }

    //---Setup Reference objects
    public void SetupRef(ref_dx__c o)
    {
        refObj = o;
    }    
    public void Setup(r_reeg_dx__c o)
    {
        obj = o;
    } 

    public void SetReegId(Id rId)
    {
        obj.r_reeg_id__c = rId;
    }

    public class TestrEEGDxDisp extends BaserEEGDxDisp {        
    }

    //---TEST METHODS ------------------------------
    public static testMethod void testCall()
    {
        r_reeg_dx__c obj = new r_reeg_dx__c();

        ref_dx__c ref = new ref_dx__c();

        TestrEEGDxDisp disp = new TestrEEGDxDisp();
        disp.setObj(obj);
        disp.Setup(obj);

        System.assertEquals( obj, disp.getObj());

        disp.setRefObj(ref);
        disp.SetupRef(ref);
        System.assertEquals( ref, disp.getRefObj());

    }
}