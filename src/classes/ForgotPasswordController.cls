/**
 * An apex page controller that exposes the site forgot password functionality
 */
public class ForgotPasswordController 
{
    public String username {get; set;}   
       
    public ForgotPasswordController() {}
	
  	public PageReference forgotPassword() 
    {
      PageReference pr = null;
      if (String.isNotBlank(username))
      {
        boolean success = Site.forgotPassword(username);

        if (success)
        {
          pr = Page.ForgotPasswordConfirm;
          pr.setRedirect(true);
        }
        else
        {
          ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, 'Your username was not valid. Please use a valid username.');
          ApexPages.addMessage(msg);
        }
      }
      else
      {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, 'The username cannot be empty. Please use a valid username.');
        ApexPages.addMessage(msg);
      }

      return pr;
    }
  	
  	public static testMethod void testForgotPasswordController() 
    {
    	// Instantiate a new controller with all parameters in the page
    	ForgotPasswordController controller = new ForgotPasswordController();
    	controller.username = 'test@salesforce.com';     	
    
    	System.assertEquals(controller.forgotPassword(),null); 
    }
}