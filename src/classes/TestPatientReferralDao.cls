@isTest
private class TestPatientReferralDao {
	
	@isTest static void testGetById() {
		Patient_Referral__c testPR = patientReferralDao.getInstance().getTestPatientReferral();
		Test.startTest();
		Patient_Referral__c retrievedPR = patientReferralDao.getInstance().getById(testPR.Id);
		Test.stopTest();

		System.assertEquals(testPR.Id, retrievedPR.Id);
	}

	@isTest static void testGetNameSearchQuery() {
		Patient_Referral__c testPR = patientReferralDao.getInstance().getTestPatientReferral();

		Test.startTest();
		String query = patientReferralDao.getInstance().getNameSearchQuery('Test');
		Test.stopTest();

		System.assert(query.contains(String.valueOf(testPR.Patient_First_Name__c)));
	}

	@isTest static void testGetMyPatientsReferralQuery() {
		Patient_Referral__c testPR = patientReferralDao.getInstance().getTestPatientReferral();
		Test.startTest();
		String query = patientReferralDao.getInstance().getMyPatientsReferralQuery();
		Test.stopTest();

		System.assertNotEquals(null,query);
	}
	
}