public with sharing class wr_NewsController {

    public List<News__c> newsItems {get; set;}
    private Integer displaySize = 5;
    
    public List<NewsWrapper> newsWrapper {get; set;}
    
    public class NewsWrapper {
    	
    	public News__c news {get; set;}
    	public String style {get; set;}
    	
    }
    

    public wr_NewsController()
    {
    
    	newsWrapper = new List<NewsWrapper>();
    	newsItems = new List<News__c>();
    	loadSyndications();
   
    }
    
    
    public void loadSyndications()
    {
        try 
        {
             newsItems
            = [select Id,News_Date__c,Body__c,Published__c from News__c where Published__c =: true  order by News_Date__c DESC limit 5];

            Integer i = 1;
            for(News__c n : newsItems)
            {
                NewsWrapper nw = new NewsWrapper();
                nw.news = n;
                if(i == displaySize)
                {
                	nw.style = 'border-bottom: none;';
                }
                newsWrapper.add(nw);
                i++;
            
            }

        }
        catch(Exception e) { system.debug('wr_News: ' + e); }
    }
    
    public static testMethod void testSyndications()
    {
        Test.startTest();
        wr_NewsController wr_Synd = new  wr_NewsController();
        wr_Synd.loadSyndications();
        Test.stopTest();
    }
}