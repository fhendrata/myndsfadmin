//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: PhyPortalLoginControllerTest
// PURPOSE: Test class for the physician portal login
// CREATED: 02/12/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class PhyPortalLoginControllerTest 
{
	/**
    * Test constructor & loadAction page load method
    */
    private static testmethod void testLoadAction() 
    {
    	PhyPortalLoginController cont = new PhyPortalLoginController();
    	PageReference pr = cont.loadAction();
    }
	
}