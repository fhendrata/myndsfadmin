public class phy_PatientReferralListController extends BaseController {

	private string searchCriteria {get; set;}
 	private integer resultTableSize {get; set;}
	private patientReferralDao prDao = patientReferralDao.getInstance();
	public string newFilterId {get; set;}
	public ApexPages.StandardSetController con {get; set;}
	public String patSearchString {get; set;}
	 
    public Boolean hasNext { get { return con.getHasNext(); } set; }  
    public Boolean hasPrevious { get { return con.getHasPrevious(); } set; }  
    public Boolean hasResults { get { return (resultNumber > 0); } set; }  
    public Integer pageNumber  { get { return con.getPageNumber(); } set; }
    public Integer resultNumber  { get { return con.getResultSize(); } set; }  
    public void previous() { con.previous(); }  
    public void next() { con.next(); } 
 //   public List<patientDisp> PatDispList { get; set; } 
    
	public phy_PatientReferralListController() {
		resultTableSize = 10;
		setupFilter();
		loadPatientReferrals();
	}

	public void searchPatients() { 
        String query = prDao.getNameSearchQuery(patSearchString);
        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);  
    }

    private void setupFilter() {
    	if(getIsClinicAdmin()) prDao.filter = 'Referred_To__r.AccountId = ' + quote(getCurrentAccount());
    	else prDao.filter = 'Referred_To__c = ' + quote(getCurrentPhysician());
    }

    private PageReference loadPatientReferrals() {
    	String query = prDao.getMyPatientsReferralQuery();
    	con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(resultTableSize);
    	return null;
    }

    public List<Patient_Referral__c> getPatientReferrals() {
    	if(con != null) {
    		return (List<Patient_Referral__c>)con.getRecords();
    	} else {
    		return null;
    	}
    }
}