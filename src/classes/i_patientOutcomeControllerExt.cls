//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: i_patientOutcomeControllerExt
//   PURPOSE: Controller class for the iPad patient outcome page
// 
//     OWNER: CNS Response
//   CREATED: 11/19/10 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------

public class i_patientOutcomeControllerExt extends CrudController
{
   	public List<PatientOutcomeMedDisp> oMeds  { get; set; }
    public List<PatientOutcomeMedDisp> newMeds  { get; set; }
    public List<r_outcome_med__c> holdingList { get; set; } 
    public List<r_outcome_med__c> previousMeds { get; set; }
    public String billingCode { get; set; }
    public integer rowCount {get; set;}
	public integer newRowCount {get; set;}
    public boolean updatePrevious { get; set; }
    public String DebugMsg { get; set; }
    public List<String> medsToUpdate { get; set; }
    public boolean addNewMed {get; set;}
    public boolean isCurrentMeds {get; set;}
    
    private patientOutcomeMedDao ocMedDao { get; set; }
    private patientIntervalsDao intDao { get; set; }
    
	//---Return a casted version of the object
	public r_patient_outc__c getObj()
	{
		return (r_patient_outc__c) CrudObj;
	}
	
	//---Base Constructor (go to test mode)
	public i_patientOutcomeControllerExt()
	{
		init(); 
	}
	
    // ---Constructor from the standard controller
    public i_patientOutcomeControllerExt( ApexPages.StandardController stdController)
    {
    	init();    	
        init(stdController); 
    }
    
    //---Local init
    public void init()
    {
    	BasePageName = 'iwPatientOutcome';
    	IsNewPageNameType = true;
    	addNewMed = false;
    	outDao = new patientOutcomeDao();
    	CrudDao = outDao;
    	ParentId = ApexPages.currentPage().getParameters().get('pid');
		RetUrl = restoreReturnUrlParams(ApexPages.currentPage().getParameters().get('retUrl'));
		ViewOnSave = ApexPages.currentPage().getParameters().get('viewOnSave');
		ObjId = ApexPages.currentPage().getParameters().get('id');
    }
    
     //--- Number of outcome meds in list
    private Integer oMedCount;
    public Integer getOMedCount()
    {
        return oMedCount;
    }
    
    private boolean previous = false;
    public boolean getIsPrevious() {
    	return previous;
    }
    public void setIsPrevious(boolean prevVal) {
    	previous = prevVal;
    }
     
    public override PageReference setup() 
    {
    	setupPage();    	
    	return null;
    } 
    
    public virtual PageReference setupActionEdit()
	{
		PageReference pRef = setup();	
		
    	System.debug('## iPOCE editActionOverride, CrudObj = ' + CrudObj);
    	if (CrudObj != null) {
			string submittedBy = (String) CrudObj.get('submitted_type__c');
			if (submittedBy == null || submittedBy.equalsIgnoreCase('')) {
				CrudObj.put('submitted_type__c', 'iPad');
			}   
    	}			
			
		if (pRef == null) addBlankMedRows();
		return pRef;
	}	
	
    public void setupPage() 
    {
    	updatePrevious = false;
    	
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.qsId = ApexPages.currentPage().getParameters().get('qsid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        string newPatStr = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        this.isiPad = true;
        
		String strIsNew = ApexPages.currentPage().getParameters().get('isNew');
		if (!e_StringUtil.isNullOrEmpty(strIsNew))		
			IsNew = strIsNew.equals('isNew');
		if (e_StringUtil.isNullOrEmpty(ObjId))		
			IsNew = true;	
        
        ocMedDao = new patientOutcomeMedDao();
        intDao = new patientIntervalsDao();
        rDao = new rEEGDao();
        outDao = new patientOutcomeDao();
        
    	// assign most recent type I rEEG, set billing code, set outcome date to rEEG date
		List<r_reeg__c> reegs;
    	if (!e_StringUtil.isNullOrEmpty(ParentId)) 
    		reegs = rDao.getByPatientId(ParentId);
    		
    	if (IsNew)
    	{    		
    		CrudObj = new r_patient_outc__c();
    		Date oDate = date.today();
    		CrudObj.put('outcome_date__c', oDate); 
    		CrudObj.put('submitted_type__c', 'iPad');
    		CrudObj.put('cgi__c', '4 - Baseline');
    			
    		if (reegs != null && reegs.size() > 0) 
    		{ 
    			CrudObj.put('rEEG__c', reegs[0].Id);
    			billingCode = reegs[0].billing_code__c;
    		}
    		
    		if (e_StringUtil.isNullOrEmpty(patId))
    			patId = ParentId;   		
    		if (!e_StringUtil.isNullOrEmpty(patId)) {
    			CrudObj.put('patient__c', patId);
    			DebugMsg = 'putting pid on patient:' + patId;
    		} 
    		
    		// check if there is an existing outcome, if true, prepopulate with those meds
			oMeds = getCurrentOutcomeMeds(ParentId);	
    	}
    	else
    	{
    		debug('### load crud obj:' + ObjId);
    		CrudObj = outDao.getById(ObjId);
    		debug('### crud obj:' + CrudObj); 
    		string reegId = (String) CrudObj.get('rEEG__c');
    		
    		if (!e_StringUtil.isNullOrEmpty(reegId)) {
    			billingCode = outDao.getbillingCode(reegId);
    		}
    		
    		// check if there is an existing outcome, if true, prepopulate with those meds
			oMeds = getAllOutcomeMeds(ParentId);
    	}
    	
    	addBlankMedRows();
    	       	
    	if (oMeds == null) oMeds = new List<PatientOutcomeMedDisp>();     
    	else
    	{
    		isCurrentMeds = (oMeds.size() > 0);
    	} 		
    }
    
    public PageReference previousWizAction()
    {
        PageReference returnVal = new PageReference('/apex/iwrEEGEdit');
        returnVal.getParameters().put('isNew', string.valueOf(isNew));
        
 		if (patient != null) returnVal.getParameters().put('pid', patient.id);
        if (!e_StringUtil.isNullOrEmpty(patId)) returnVal.getParameters().put('pid', patId);
        if (reeg != null) returnVal.getParameters().put('id', reeg.id);
        if (!e_StringUtil.isNullOrEmpty(reegId)) returnVal.getParameters().put('id', reegId);
        
        returnVal.getParameters().put('wiz', string.valueOf(isWiz));
        returnVal.getParameters().put('np', string.valueOf(isNewPat));
        returnVal.getParameters().put('oid', CrudObj.id);
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference nextWizAction()
    {
        saveActionOverride();
        
        PageReference returnVal = new PageReference('/apex/iwrEEGDX');
        returnVal.getParameters().put('isNew', string.valueOf(isNew));
       
		if (patient != null) returnVal.getParameters().put('pid', patient.id);
        if (!e_StringUtil.isNullOrEmpty(patId)) returnVal.getParameters().put('pid', patId);
        if (reeg != null) returnVal.getParameters().put('rid', reeg.id);
        if (!e_StringUtil.isNullOrEmpty(reegId)) returnVal.getParameters().put('rid', reegId);
        
        returnVal.getParameters().put('wiz', string.valueOf(isWiz));
        returnVal.getParameters().put('np', string.valueOf(isNewPat));
        returnVal.getParameters().put('oid', CrudObj.id);
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference cancelWizardAction()
    {
    	PageReference returnVal = cancelWizAction();
        if (returnVal != null) returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference cancelOutcomeAction()
    {
    	patId = ApexPages.currentPage().getParameters().get('pid');
 
    	if (!e_StringUtil.isNullOrEmpty(patId)) 
    		patId = getObj().patient__c;
    	
    	if (!e_StringUtil.isNullOrEmpty(patId))
    	{
    		return new PageReference('/apex/iwPatient');
    	}
    	
    	if (isWiz && !e_StringUtil.isNullOrEmpty(reegId))
    	{
    		rDao.deleteSObjectById(reegId);
    	}
    	
        return new PageReference('/apex/iwPatientDetail?show=all&id=' + patId);   
    }
    
    public PageReference previousActionOverride()
    {
    	PageReference returnVal = new PageReference('/apex/rEEGWizard3Page?rid=' + this.reegId + '&pid=' + this.patId  
                                                    + '&qsid=' + this.qsId + '&np=' +  this.isNewPat + '&wiz=' + this.isWiz);
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference nextActionOverride()
    {
    	saveActionOverride();
	   	PageReference returnVal = new PageReference('/apex/rEEGWizard4Page?rid=' + this.reegId + '&pid=' + this.patId + '&oid=' + ObjId
                                                    + '&qsid=' + this.qsId + '&np=' +  this.isNewPat + '&wiz=' + this.isWiz);
        returnVal.setRedirect(true);
        return returnVal;
    } 
    
    public PageReference editActionOverride()
    {
    	System.debug('## iPOCE editActionOverride, CrudObj = ' + CrudObj);
    	if (CrudObj != null) {
			string submittedBy = (String) CrudObj.get('submitted_type__c');
			if (submittedBy == null || submittedBy.equalsIgnoreCase('')) {
				CrudObj.put('submitted_type__c', 'iPad');
			}   
    	}	
    	PageReference pr = editAction(); 
    	
    	pr.getParameters().put('retUrl', '/apex/iwPatientOutcomeView?id=' + ObjId);
    	
    	pr.setRedirect(true);
        return  pr;	
    }
    
    //---Build the existing outcome meds list (meds from previous outcome)
    public List<PatientOutcomeMedDisp> getCurrentOutcomeMeds(string pid)
    {	
	    if (oMeds == null)
        { 	
             //---Get the current meds
             previousMeds = outDao.getExistingOutcomeMeds(pid);
             oMeds = getOutcomeMedDispList(previousMeds);
        }
            
        return oMeds;
    }
    
    //---Build the existing outcome meds list (meds from previous outcome)
    public List<PatientOutcomeMedDisp> getAllOutcomeMeds(string pid)
    {
    	if (oMeds == null)
        { 	
             //---Get the current meds
             previousMeds = ocMedDao.getAllOutcomeMedsForOutcome(pid);
             oMeds = getOutcomeMedDispList(previousMeds);
        }
            
        return oMeds;
    }
    
    private List<PatientOutcomeMedDisp> getOutcomeMedDispList(List<r_outcome_med__c> medList)
    {
    	List<PatientOutcomeMedDisp> medDispList = new List<PatientOutcomeMedDisp>();  
        PatientOutcomeMedDisp oMed;          
        Integer ctr = -1;
            
        if (medList != null) 
        {
        	for(r_outcome_med__c oMedRow : medList) 
        	{
	        	ctr++;  
	         	oMed = new PatientOutcomeMedDisp();
	         	oMed.setDisplayAction(true);
	         	oMed.setRowNum(ctr);
		    /*            
	         	r_outcome_med__c hldgMed = new r_outcome_med__c();
	         	hldgMed.med_name__c = oMedRow.med_name__c;
	         	hldgMed.dosage__c = oMedRow.dosage__c;
	         	hldgMed.unit__c = oMedRow.unit__c;
	         	hldgMed.frequency__c = oMedRow.frequency__c;
	         	hldgMed.start_date__c = oMedRow.start_date__c;
	         	hldgMed.end_date__c = oMedRow.end_date__c;
	         	hldgMed.previous_med__c = oMedRow.Id;
		     	oMed.Setup(hldgMed);
		     */
		     	oMed.Setup(oMedRow);
		     	medDispList.Add(oMed);		                
	     	}
	     	setIsPrevious(true);
	     	oMedCount = ctr + 1;	//---ctr is zero based	 
	     	rowCount = ctr + 1;	//---ctr is zero based	 
        }	   
        return medDispList;
    }
    
    public PageReference refreshAutoAction()
    {
    	return null;
    }    
    
    //---Build the current outcome's meds list
    public List<PatientOutcomeMedDisp> getOutcomeMeds()
    {
         if (oMeds == null)
         {
             oMeds = new List<PatientOutcomeMedDisp>();  
             PatientOutcomeMedDisp oMed;
             
             Integer ctr = -1;
             
             //---Add the current meds
             for(r_outcome_med__c oMedRow : 
                 [select med_name__c, dosage__c, unit__c, frequency__c, start_date__c, end_date__c, name, start_date_is_estimated__c
                     from r_outcome_med__c
                     where r_outcome_id__c = :(String) CrudObj.get('id')])
            {
                ctr++;
                
                oMed = new PatientOutcomeMedDisp();
                oMed.setRowNum(ctr);
                oMed.Setup(oMedRow );                                 
                oMeds.Add(oMed);
            }           
        	oMedCount = ctr + 1;  //---ctr is zero based
 
            if (ctr < 0) 
            {
	            //---Create blank row
	            r_outcome_med__c objMed = new r_outcome_med__c();
	            objMed.r_outcome_id__c = CrudObj.Id;
	            objMed.unit__c = 'mg';
	            
	            oMed = new PatientOutcomeMedDisp();
	            oMed.setRowNum(0);    
	            rowCount = 1; 
	            oMed.Setup(objMed);                           
	            oMeds.Add(oMed);  	
	        }        
            rowCount = ctr + 1;    //---ctr is zero based
        }    
        return oMeds;
    }
       
	public PageReference saveActionOverride()
    {
        CrudDao.saveSObject(CrudObj);

        if (oMeds != null && oMeds.size() > 0) 
        {
            //---Loop through each row
            for(PatientOutcomeMedDisp oMedRow : oMeds) 
            {
                if (oMedRow != null && oMedRow.getObj() != null) 
                {
                	SaveOutcomeMed(oMedRow.getObj());
                }
            }
        }
        
        if (newMeds != null && newMeds.size() > 0)
        {
         	for(PatientOutcomeMedDisp nMedRow : newMeds) 
            {
	            if (nMedRow != null) 
	            {
                    if (nMedRow.getObj() == null) 
                    {
                       debug('-- oMedRow.getObj() is null');         
                    } 
                    else 
                    {
                    	if (nMedRow.getObj().start_date__c == null && !e_StringUtil.isNullOrEmpty(nMedRow.getObj().med_name__c)) {
                    		return null;
                    	} 
                    	else 
                    	{
                    		r_outcome_med__c outcMed = nMedRow.getObj();
                    		outcMed.r_outcome_id__c = CrudObj.Id;
                    		nMedRow.Setup(outcMed);
	                  		SaveOutcomeMed(outcMed);   
	                  		if (outcMed != null && outcMed.med_name__c != null && outcMed.med_name__c != '')
	                  			oMeds.add(nMedRow);
	                 	}                   
	                }
	            }
	        }
        }
        
        if (isWiz && !e_StringUtil.isNullOrEmpty(reegId))
        {
        	reeg = rDao.getById(reegId);
        	if (reeg != null && !reeg.med_no_med__c && !e_StringUtil.isNullOrEmpty(reeg.reeg_type__c) && reeg.reeg_type__c != 'Type II')
			{
				reeg.reeg_type_mod__c = rEEGUtil.getMedicatedTestMod(oMeds);
				rDao.Save(reeg, '');
			}
        }
        
        if (patient == null)
        {
	        pDao = new patientDao();
	        patient = pDao.getById(ParentId);
        }
        
        if (patient != null)
        {
	        patientIntervalBuilder intBuilder = new patientIntervalBuilder();
	  		intBuilder.buildAllIntervalsForPat(patient.Id, patient.OwnerId);
        }
        PageReference pr = saveAction(); 
        if (pr != null)
        {
	    	pr.getParameters().put('pid', ParentId);
	    	pr.setRedirect(true);
	        return  pr;
        }
        else return null;	
    }
    
	// ---Save an outcome med row
    private void SaveOutcomeMed(r_outcome_med__c medObj)
    {
	    ObjId = (String) CrudObj.get('id');
	    
        // ---med name can't be blank
        if (medObj != null && medObj.med_name__c != null && medObj.med_name__c.trim() != '')
        { 
        	if (medObj.id == null)
        		medObj.r_outcome_id__c = ObjId;  	
            ocMedDao.saveSObject(medObj);
        }
        else 
        {
            //---If the id is not blank and the name is, then delete the old record
            if (medObj.id != null) ocMedDao.deleteSObject(medObj);
        }   
    }

    public PageReference addMedRow()
    { 	
    	string rowNumStr = System.currentPageReference().getParameters().get('rowNumber');
    	if (!e_StringUtil.isNullOrEmpty(rowNumStr))
    	{
	    	Integer rowNum = Integer.valueOf(rowNumStr);
			// rowNum is 0 based, rowCount starts at 1
			if (rowNum != null && (rowNum + 1) == rowCount) 
			{
				//---Create blank row
				r_outcome_med__c objMed = new r_outcome_med__c();
				objMed.r_outcome_id__c = ObjId;
		            
				PatientOutcomeMedDisp oMed = new PatientOutcomeMedDisp();
				oMed.setRowNum(rowCount); 
				rowCount++; 
				oMed.Setup(objMed);                           
				oMeds.Add(oMed);  	
			}
    	}        
    	return null; 	  
    }
    
    public PageReference addBlankMedRows()
    {
    	addNewMed = true;
    	if (newMeds == null) newMeds = new List<PatientOutcomeMedDisp>();    
    	    	
       	for (integer i = 0; i < 3; i++)
    	{
    		PatientOutcomeMedDisp medDisp = new PatientOutcomeMedDisp();
    		r_outcome_med__c newMed = new r_outcome_med__c();
    		newMed.unit__c = 'mg';	
    		medDisp.setObj(newMed);		
    		medDisp.setRowNum(newMeds.size());
    		newMeds.add(medDisp);    	
    	}
    	newRowCount = newMeds.size();
    	
    	return null;
    }
    
    public string getPatientFirstName()
    {
    	string returnVal = '';
    	if (patient == null)
    	{
    		r_patient_outc__c outC = (r_patient_outc__c)CrudObj;  	
        
	        if (outC != null)
	        {
	        	if (!e_StringUtil.isNullOrEmpty(outC.patient__c))
	        	{        
	        		pDao = new patientDao();
	        			
	            	patient = pDao.getById(outC.patient__c);
	            	if (patient != null && !e_StringUtil.isNullOrEmpty(patient.first_name__c))
	            	{
	                	returnVal = patient.first_name__c;
	            	}
	        	}
	        }
    	}
    	else
    	{
    		if (patient != null && !e_StringUtil.isNullOrEmpty(patient.first_name__c))
	        {
	           	returnVal = patient.first_name__c;
	        }
    	}
        
        return returnVal;
    }    
    
    public string getPatientLastName()
    {
        string returnVal = '';
    	if (patient == null)
    	{
    		r_patient_outc__c outC = (r_patient_outc__c)CrudObj;  	
        
	        if (outC != null)
	        {
	        	if (!e_StringUtil.isNullOrEmpty(outC.patient__c))
	        	{        
	        		pDao = new patientDao();
	        			
	            	patient = pDao.getById(outC.patient__c);
	            	if (patient != null && !e_StringUtil.isNullOrEmpty(patient.name))
	            	{
	                	returnVal = patient.name;
	            	}
	        	}
	        }
    	}
    	else
    	{
    		if (patient != null && !e_StringUtil.isNullOrEmpty(patient.name))
	        {
	           	returnVal = patient.name;
	        }
    	}
        
        return returnVal;
    }
    
    public string getREEGName()
    {
    	string returnVal = '';
        
        if (reeg == null)
    	{
    		r_patient_outc__c outC = (r_patient_outc__c)CrudObj;  	
        
	        if (outC != null)
	        {
	        	if (!e_StringUtil.isNullOrEmpty(outC.rEEG__c))
	        	{        
	            	reeg = rDao.getById(outC.rEEG__c);
	            	if (patient != null && !e_StringUtil.isNullOrEmpty(reeg.name))
	            	{
	                	returnVal = reeg.name;
	            	}
	        	}
	        }
    	}
    	else
    	{
    		if (reeg != null && !e_StringUtil.isNullOrEmpty(reeg.name))
	        {
	           	returnVal = reeg.name;
	        }
    	}
        
        
        return returnVal;
    }
    
    // if outcome is deleted, need to delete associate medication
    public PageReference deleteActionOverride()
    {        
        try {
			retUrl = '/apex/iwPatientDetail?show=all&id=' + ParentId;
	        if (oMeds != null) {
	            //---Loop through each row
	            for(PatientOutcomeMedDisp oMedRow : oMeds) {
	                if (oMedRow != null) 
	                {
	                    if (oMedRow.getObj() == null) 
	                    {
	                       // addMessage('oMedRow.obj is null');         
	                    }
	                    else 
	                    {
							// Should not allow deleting of data, do a soft delete
            				oMedRow.getObj().put('is_deleted__c',true);
            				upsert oMedRow.getObj();                    
	                    }
	                }
	            }	
	        }  
        }
        catch (Exception ex) {
            handleError( 'deleteSObject', ex);
        }
        return deleteAction();
    }
    
    public PageReference ieditAction() 
    {   
    	PageReference pRef = new PageReference( '/apex/iwPatientOutcomeEdit?id=' + ObjId + '/apex/iwPatientDetail?show=all&id=' + ParentId);
    	return pRef;
    }
    
    public String OutDate  
    {     	
    	get { return e_StringUtil.getDateString(getObj().outcome_date__c); }    	 
    	set { getObj().outcome_date__c = e_StringUtil.setDateFromString(value); } 
    }    

    public String NxtAppt  
    {     	
    	get { return e_StringUtil.getDateString(getObj().next_appointment__c); }    	 
    	set { getObj().next_appointment__c = e_StringUtil.setDateFromString(value); } 
    }      

      // ---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
    	i_patientOutcomeControllerExt cont = new i_patientOutcomeControllerExt();
    	cont.IsTestCase = true;
		cont.oMeds = new List<PatientOutcomeMedDisp>();
		cont.holdingList = new List<r_outcome_med__c>();
		cont.previousMeds = new List<r_outcome_med__c>();
		cont.billingCode = 'testbillingcode';
		cont.updatePrevious = false;
		cont.medsToUpdate = new List<String>();
		cont.DebugMsg = 'msg';
		cont.ViewOnSave = 'true';
		cont.IsNew = true;
		cont.getOMedCount();
		System.currentPageReference().getParameters().put('rowNumber', '1');
		
		r_reeg__c reeg = rEEGDao.getTestReeg();
		patientDao pDao = new patientDao();
		r_patient__c pat = pDao.getById(reeg.r_patient_id__c);
		cont.ParentId = pat.Id;
		
		patientOutcomeDao patOutDao = new patientOutcomeDao();
		r_patient_outc__c obj = patOutDao.getTestOutcome(pat.Id);
		
		cont.ObjId = obj.Id;
		cont.RetUrl = '/apex/iwPatientOutcomeView?id=' + cont.ObjId;
		
		cont.CrudObj = obj;
		cont.CrudDao = patOutDao;
    	obj = cont.getObj();
		
		Integer rowCount = cont.RowCount;
    	System.assertEquals(cont.getIsPrevious(), false);
    	cont.setIsPrevious(true);
    	System.assertEquals(cont.getIsPrevious(), true);
    	
    	PageReference pr = cont.setup(); 
    	System.assertEquals(pr, null);
    
    	pr = cont.setupActionEdit();
    	pr = cont.refreshAutoAction();
    	cont.init();

    	patientOutcomeMedDao outMedDao = new patientOutcomeMedDao();
    	r_outcome_med__c testMed = outMedDao.getTestOutcomeMed();
    	patientOutcomeDao outDao = new patientOutcomeDao();
    	r_patient_outc__c testOutcome = outDao.getById(testMed.r_outcome_id__c);
    	testOutcome = outDao.getTestOutcome(reeg.r_patient_id__c);
    	testMed = outMedDao.getTestOutcomeMed(testOutcome.Id);
    	
    	
    	ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
		cont.IsNew = true;
		pr = cont.setup();
		pr = cont.editActionOverride();
		pr = cont.cancelOutcomeAction();
		List<PatientOutcomeMedDisp> outMedList = cont.getCurrentOutcomeMeds(reeg.r_patient_id__c);
		outMedList = cont.getOutcomeMeds();
 		pr = cont.saveActionOverride();
 		
 		cont.IsNew = false;
		pr = cont.setup();
		pr = cont.addMedRow();
		string testStr = cont.getPatientFirstName();
		testStr = cont.getPatientLastName();
		testStr = cont.getREEGName();
		testStr = cont.OutDate;
		testStr = cont.NxtAppt;
		pr = cont.ieditAction();
		pr = cont.editActionOverride();
		pr = cont.cancelOutcomeAction();
		outMedList = cont.getCurrentOutcomeMeds(reeg.r_patient_id__c);
		outMedList = cont.getOutcomeMeds();
 		pr = cont.saveActionOverride();
 		pr = cont.previousActionOverride();
 		pr = cont.nextActionOverride();
 		
 		pr = cont.previousWizAction();
 		pr = cont.nextWizAction();
 		pr = cont.cancelWizAction();
 		
 		pr = cont.cancelWizardAction();
 		
 		cont.oMeds = null;
 		cont.getOutcomeMeds();
 
 		pr = cont.deleteActionOverride();
 		
 //		pr = cont.discontinueDose();

    }
    
    
    
 }