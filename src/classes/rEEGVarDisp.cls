Public Class rEEGVarDisp
{
    private r_reeg_var__c obj;
    public r_reeg_var__c getObj()
    {
        return obj;
    }
    public void setObj(r_reeg_var__c s)
    {
        obj = s;
    }

    //---Other test value, #1
    private String other1;
    public String getOther1()
    {
        return other1;
    }
    public void setOther1(Double s)
    {
        other1 = String.valueOf(Decimal.valueOf(s).setScale(2));
    }

    //---Other Z test value, #1
    private String other1Z;
    public String getOther1Z()
    {
        return other1Z;
    }
    public void setOther1Z(Double s)
    {
        setzvalue1Style(SetZValueStyle(Decimal.valueOf(s).setScale(2)));
        other1Z = String.valueOf(Decimal.valueOf(s).setScale(2));
    }

    //---Other test value, #2
    private String other2;
    public String getOther2()
    {
        return other2;
    }
    public void setOther2(Double s)
    {
        other2 = String.valueOf(Decimal.valueOf(s).setScale(2));
    }

     //---Other Z test value, #2
    private String other2Z;
    public String getOther2Z()
    {
        return other2Z;
    }
    public void setOther2Z(Double s)
    {
        setzvalue2Style(SetZValueStyle(Decimal.valueOf(s).setScale(2)));
        other2Z = String.valueOf(Decimal.valueOf(s).setScale(2));
    }

    //---Other test value, #3
    private String other3;
    public String getOther3()
    {
        return other3;
    }
    public void setOther3(Double s)
    {
        other3 = String.valueOf(Decimal.valueOf(s).setScale(2));
    }

    //---Other Z test value, #3
    private String other3Z;
    public String getOther3Z()
    {
        return other3Z;
    }
    public void setOther3Z(Double s)
    {
        setzvalue3Style(SetZValueStyle(Decimal.valueOf(s).setScale(2)));
        other3Z = String.valueOf(Decimal.valueOf(s).setScale(2));
    }

    private String zvalueStyle1Value;
    public String getzvalue1Style()
    {
        return zvalueStyle1Value;
    }

    public void setzvalue1Style(String s)
    {
        zvalueStyle1Value = s;
    }

    private String zvalueStyle2Value;
    public String getzvalue2Style()
    {
        return zvalueStyle2Value;
    }

    public void setzvalue2Style(String s)
    {
        zvalueStyle2Value = s;
    }

    private String zvalueStyle3Value;
    public String getzvalue3Style()
    {
        return zvalueStyle3Value;
    }

    public void setzvalue3Style(String s)
    {
        zvalueStyle3Value = s;
    }

    private String zvalueStyleValue;
    public String getzvalueStyle()
    {
        return zvalueStyleValue;
    }

    private String nameStyleValue;
    public String getnameStyle()
    {
        return nameStyleValue;
    }

    private String SetZValueStyle(Decimal val)
    {
        String returnVal = 'color:Black';
        if (val >= .75)
        {
            returnVal = 'color:red';
        }
        if (val <= -.75)
        {
            returnVal = 'color:Blue';
        }

        return returnVal;
    }

    //---Setup method
    public void Setup(r_reeg_var__c o)
    {
        obj = o;
        obj.pat_value__c = Decimal.valueOf(o.pat_value__c).setScale(2);
        obj.pat_zvalue__c = Decimal.valueOf(o.pat_zvalue__c).setScale(2);
        zvalueStyleValue = 'color:Black';
        nameStyleValue = 'color:Black';
        if (o.pat_zvalue__c >= .75)
        {
            zvalueStyleValue = 'color:red';
        }
        if (o.pat_zvalue__c <= -.75)
        {
            zvalueStyleValue = 'color:Blue';
        }
    }

    //------------TEST-----------------------------
    public static testMethod void testDxDisp()
    {
        r_reeg_var__c obj = new r_reeg_var__c();
        obj.name = 'other';

        rEEGVarDisp disp = new rEEGVarDisp();
        obj.pat_value__c = 1.0;
        obj.pat_zvalue__c = 1.0;
        disp.setObj(obj);
        disp.Setup( obj);

        disp.setOther1Z(1.1);
        disp.setOther2Z(1.1);
        disp.setOther3Z(1.1);

        System.assertEquals(disp.getOther1Z(), '1.10');
        System.assertEquals(disp.getOther2Z(), '1.10');
        System.assertEquals(disp.getOther3Z(), '1.10');

        r_reeg_var__c obj2 = disp.getObj();
        System.assertEquals( obj, obj2);
    }
}