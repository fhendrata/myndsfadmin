//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: SysLogDao
// PURPOSE: Data access class for Sytem Log fields
// CREATED: 10/25/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class SysLogDao extends BaseDao
{
	private static String NAME = 'sys_log__c';    
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.sys_log__c.fields.getMap());
				//dao.saveFields( NAME, fldList);					
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public sys_log__c getById(String idInp)
    {
		return (sys_log__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public List<sys_log__c> getRecentLogs()
    {
		return (List<sys_log__c>)getSObjectListByWhere(getFieldStr(), NAME, '', 'name desc', '400');
    }
    
    public void insertWsLog(string reegId, string actionType, string response)
    {
		insertWsLog(reegId, actionType, response, '');
    }
    
    public void insertWsLog(string reegId, string actionType, string response, string userId)
    {
		sys_log__c logObj = new sys_log__c();
		logObj.category__c = actionType;
		logObj.group__c = 'WebSrv';
		logObj.related_id__c = reegId;
		logObj.level__c = 'DEBUG';
		string message = (!e_StringUtil.isNullOrEmpty(UserId)) ? 'userId: ' + userId : '';
		logObj.message__c = message + ' response:' + response;

		saveSObject(logObj);
    }
    
    public static sys_log__c getTestSysLog()
    {
    	sys_log__c log = new sys_log__c();
    	log.category__c = 'test';
    	log.group__c = 'System';
    	log.message__c = 'test log message';
    	log.related_id__c = 'test Id';
        insert log;
        
        return log;
    }
    
    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testSysLogDao()
    {
    	SysLogDao dao = new SysLogDao();
    	dao.IsTestCase = true;
    	
    	sys_log__c testLog = SysLogDao.getTestSysLog();
    	
    	testLog = dao.getById(testLog.Id);
    	List<sys_log__c> logList = dao.getRecentLogs();
    	
    	dao.insertWsLog('test', 'db_update', 'ok');
    	dao.insertWsLog('test', 'db_update', 'ok', 'testId');
    	
    }

}