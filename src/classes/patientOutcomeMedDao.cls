//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: patientOutcomeMedDao
//   PURPOSE: data access class for the outcome med obj.
//     OWNER: CNSR
//   CREATED: 07/5/10 jdepetro Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class patientOutcomeMedDao extends BaseDao 
{
	private static String NAME = 'r_outcome_med__c';
	private static final patientOutcomeMedDao poMedDao = new patientOutcomeMedDao();

    public static patientOutcomeMedDao getInstance() 
    {
        return poMedDao; 
    }
	
	  //---Constructor
	public patientOutcomeMedDao()
	{
		ObjectName = NAME;
		ClassName = 'patientOutcomeMedDao';
		HideDeleted = true; 
	}
	
	public static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_outcome_med__c.fields.getMap());					
		}  	
		return fldList;
	}
	
	//---Get by Id
	public r_outcome_med__c getById(String idInp)  
    {
		r_outcome_med__c obj = (r_outcome_med__c) getSObjectById(getFieldStr(), NAME, idInp);
		return obj;
    }  
    
 	public List<r_outcome_med__c> getByOutcome(string oid)
	{
		String whereClause =  ' r_outcome_id__c =  \'' + oid + '\'';
		return (List<r_outcome_med__c>) getSObjectListByWhere(getFieldStr(), NAME, whereClause, 'Name');
	}
	
	public List<r_outcome_med__c> getByMedicineName(string medName)
	{
		String whereClause =  ' med_name__c =  \'' + medName + '\'';
		return (List<r_outcome_med__c>) getSObjectListByWhere(getFieldStr(), NAME, whereClause, 'Name');
	}
	
	public List<r_outcome_med__c> getMedByIdList(List<String> updateIds)
	{
		String whereClause = getWhereInIdsFromIdList('Id', updateIds);
		return (List<r_outcome_med__c>) getSObjectListByWhere(getFieldStr(), NAME, whereClause, 'Name');
	}
	
	public List<r_outcome_med__c> getAllByPatientId(string pid)
	{
		String whereClause =  ' r_outcome_id__r.patient__c =  \'' + pid + '\'';
		return (List<r_outcome_med__c>) getSObjectListByWhere(getFieldStr(), NAME, whereClause, 'start_date__c, end_date__c');
	}
	
 	public List<r_outcome_med__c> getExistingOutcomeMeds(String id)
	{
		debug('### id:' + id);
		patientOutcomeDao outDao = new patientOutcomeDao();
		List<r_patient_outc__c> outcomeList = outDao.getExistingOutcomes(id);

		debug('### outcomeList:' + outcomeList);
    	// use meds from most recent outcome reegs[0].Id
    	List<r_outcome_med__c> objList = null; 
    	if (outcomeList != null && outcomeList.size() > 0) 
    	{
    		debug('### outcomeList[0].Id:' + outcomeList[0].Id);
 
    		//--5.30.12.jdepetro - this was originally just getting open end date meds. Now current meds can have no end date.
    		//String medWhereClause =  ' r_outcome_id__c =  \'' + outcomeList[0].Id + '\'' + ' AND end_date__c = null ';	
    		String medWhereClause =  ' r_outcome_id__c =  \'' + outcomeList[0].Id + '\'';	
			objList = (List<r_outcome_med__c>) getSObjectListByWhere(getFieldStr(), NAME, medWhereClause, 'Name');
    	}
    	
        return objList;    	
	}
	
	public List<r_outcome_med__c> getAllOutcomeMedsForOutcome(String oId)
	{
		patientOutcomeDao outDao = new patientOutcomeDao();
		r_patient_outc__c outcome = outDao.getById(oId);

    	List<r_outcome_med__c> objList = null; 
    	if (outcome != null) 
    	{
    		String medWhereClause =  ' r_outcome_id__c =  \'' + outcome.Id + '\'';
			objList = (List<r_outcome_med__c>) getSObjectListByWhere(getFieldStr(), NAME, medWhereClause, 'Name');
    	}
    	
        return objList;    	
	}
	
	public r_outcome_med__c getTestOutcomeMed()
	{
		patientOutcomeDao patOutDao = new patientOutcomeDao();
		r_patient_outc__c testoutcome =  patOutDao.getTestOutcome();
    	r_outcome_med__c obj = new r_outcome_med__c();
    	obj.Name = 'test Outcome';
    	obj.med_name__c = 'test';
    	if (testoutcome != null)
    	{
	    	obj.r_outcome_id__c = testoutcome.Id;
	    	saveSObject(obj);
    	}
    	return obj;
	}
	
	public r_outcome_med__c getTestOutcomeMed(string outcomeId)
	{
		patientOutcomeDao patOutDao = new patientOutcomeDao();
    	r_outcome_med__c obj = new r_outcome_med__c();
    	obj.Name = 'test Outcome';
    	obj.med_name__c = 'test';
    	obj.r_outcome_id__c = outcomeId;
    	saveSObject(obj);

    	return obj;
	}


	//-----------------------------------------------------------------------
    //--                          TEST METHODS               ---
    //-----------------------------------------------------------------------
    public static testMethod void testPatDao()
    {
        patientOutcomeMedDao dao = new patientOutcomeMedDao();
        
        string fieldStr = patientOutcomeMedDao.getFieldStr(); 
        
        r_outcome_med__c outMed = dao.getTestOutcomeMed();
        
        if (outMed != null)
        {
        	r_outcome_med__c objMed = dao.getById(outMed.Id);
        	List<r_outcome_med__c> objMedList = dao.getByOutcome(outMed.r_outcome_id__c);
        	
        	objMedList = dao.getByMedicineName('test');
        	
        	if (objMedList != null)
        	{
        		List<string> medIdList = new List<string>();
        		for(r_outcome_med__c row : objMedList)
        		{
        			medIdList.Add(row.Id);
        		}
				objMedList = dao.getMedByIdList(medIdList);
        	}
        }
    }    
}