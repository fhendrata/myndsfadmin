//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: tec_PEEREegTechController
//   PURPOSE: PEER Requisition EEG Tech rEEG detail page controller
//     OWNER: CNSR
// DEVELOPER: Joe DePetro
//   CREATED: 03/11/16 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class tec_PEEREegTechController extends BaseController
{
    public List<ReqOutcomeMedDisp> oMeds  { get; set; }
    public List<ReqOutcomeMedDisp> newMeds  { get; set; }
    public List<Req_Outcome_Medication__c> previousMeds { get; set; }
    public String outcomeId { get; set; }
    public String pId { get; set; }
    public Integer rowCount {get; set;}
    public Integer newRowCount {get; set;}
    public Boolean isEEGActive {get; set;}
    public Boolean showReschedule {get; set;}
    public Boolean showCancel {get; set;}
    public Boolean isWashedOut {get; set;}
    public PEER_Requisition__c ReqObj {get; set;}
    public Boolean setIsPrevious {get; set;}
    public List<String> medsToUpdate {get; set;}
    public Boolean updatePrevious { get; set; }
    public Boolean NoStDtMessage { get; set; }
    public Boolean isMedEdit {get; set;}
    public Boolean addNewMed {get; set;}
    public Boolean medsVerified {get; set;}
    public Boolean medConflict {get; set;}
    public String bpTech {get; set;}
    public String weightTech {get; set;}
    public Contact eegTech {get; set;}
    
    public patientOutcomeDao patOcDao { get; set; }
    public patientOutcomeMedDao ocMedDao { get; set; }
    public patientIntervalsDao intDao { get; set; }
    public GlobalPatientDao patDao { get; set; }

    public Boolean IsCGSEmpty { get { return (String.isBlank(ReqObj.outcome_cgs__c)); } }
    public Boolean IsBillingCodeEmpty {  get { return (String.isBlank(ReqObj.reeg_phy_billing_code__c)); } }
    public Boolean IsCGIEmpty {   get { return (String.isBlank(ReqObj.outcome_cgi__c)); }  }
    public Boolean IsEegRecStatusEmpty {  get { return (String.isBlank(ReqObj.reeg_eeg_rec_stat__c)); } }
    public Boolean IsEegStatusEmpty {  get { return (String.isBlank(ReqObj.EEG_Status__c) || ReqObj.EEG_Status__c == 'Not Uploaded' || ReqObj.EEG_Status__c == 'Bad' || ReqObj.EEG_Status__c == 'Not Supported' || ReqObj.EEG_Status__c == 'EEG Make does not match file type'); } }

    public tec_PEEREegTechController()
    {
        isEEGActive = true;
        isMedEdit = false;
        setIsPrevious = false;
        addNewMed = false;
    }
    
    public PageReference loadAction()
    {
        showReschedule = false;
        showCancel = false;
        isWashedOut = false;
        updatePrevious = false;
        NoStDtMessage = false;
        
        patOcDao = new patientOutcomeDao();
        ocMedDao = new patientOutcomeMedDao();
        intDao = new patientIntervalsDao();
        patDao = new GlobalPatientDao();
        
        String reqId = ApexPages.currentPage().getParameters().get('rid');

        if (String.isNotBlank(reqId))
        {
            ReqObj = GlobalPEERRequisitionDao.getInstance().getById(reqId);
            
            if (ReqObj != null)
            {
                setTechObj();
                    
                if (eegTech != null)
                {
                    ReqObj.EEG_Make__c = eegTech.eeg_tech_default_test_make__c;
                }
                
                //weightTech = string.valueOf(reeg.vital_weight_eeg__c);
                bpTech = ReqObj.vital_bp_eeg__c;
            
                isEEGActive = (String.isNotBlank(ReqObj.Status__c) && ReqObj.Status__c != 'Submitted');
            }

            oMeds = getExistingOutcomeMeds();
            
            //if (oMeds != null && oMeds.size() > 0)
            //{
            //    Req_Outcome_Medication__c ocMed = oMeds[0].Obj;
            //    if (ocMed != null)
            //    {
            //        outcomeId = ocMed.r_outcome_id__c;                  
            //    }
            //}
            //else
            //{
            //    List<r_patient_outc__c> outcomeList = patOcDao.getExistingOutcomes();
            //    if (outcomeList != null && outcomeList.size() > 0)
            //        outcomeId = outcomeList[0].Id;
            //}
        }

        if (oMeds == null) oMeds = new List<ReqOutcomeMedDisp>();
        rowCount = 1;
        
        System.debug('%%% loadAction complete');
        
        return null;
    }
    
    public string getEnv()
    {
        return rEEGUtil.getEnvString();
    }

    public PageReference reSched()
    {
        showReschedule = true;
        return null;
    }
    
    public PageReference cancelAppt()
    {
        showCancel = true;
        return null;
    }
    
    public PageReference CancelReegPatReq()
    {
        if (ReqObj != null) 
        {
        	System.debug('##!! CancelReegPatReq fired');
            ReqObj.EEG_Status__c = 'Not Valid - Patient stopped process';
            GlobalPEERRequisitionDao.getInstance().saveSObject(ReqObj);
        }
        
        return null;
    }
    
    public PageReference submit()
    {
        boolean isEegUploaded = false;
        string medicatedTestTypeMod = '';
        string errorMsg = '';
        Boolean isPI = false;
        Boolean isPO2 = false;

        //if (!e_StringUtil.isNullOrEmpty(weightTech))
        //    reeg.vital_weight_eeg__c = Double.valueOf(weightTech);

        ReqObj = GlobalPEERRequisitionDao.getInstance().getById(ReqObj.Id);
        ReqObj.vital_bp_eeg__c = bpTech;

        if (eegTech == null) setTechObj();
        
        if (eegTech != null)
        {
            eegTech.eeg_tech_default_test_make__c = ReqObj.EEG_Make__c;
            ContactDao cDao = new ContactDao();
            cDao.saveSObject(eegTech);  
        }
             
        if (String.isNotBlank(ReqObj.reeg_eeg_rec_stat__c) && ReqObj.reeg_eeg_rec_stat__c == 'Received')
        {
            if ((String.isNotBlank(ReqObj.Physician__c)) && ReqObj.Physician__r.Account != null)
            {
                isPI = (ReqObj.Physician__r.Account.PEER_Interactive__c != null &&  ReqObj.Physician__r.Account.PEER_Interactive__c);
                isPO2 = (String.isNotBlank(ReqObj.Physician__r.Account.Report_Build_Type__c) &&  ReqObj.Physician__r.Account.Report_Build_Type__c == 'PEER Online 2.0');
            }

            isEegUploaded = true;
        }
        else
        {
            errorMsg = 'The EEG file needs to be uploaded before submitting';
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, errorMsg);
            ApexPages.addMessage(msg);
        }

        System.debug('isPI: ' + isPI);
        System.debug('isPO2: ' + isPO2);
        System.debug('isEegUploaded: ' + isEegUploaded);
        
        //if (String.isNotBlank(ReqObj.reeg_reeg_type__c) && ReqObj.reeg_reeg_type__c != 'Type II')
        //        medicatedTestTypeMod = rEEGUtil.getMedicatedTestMod(oMeds);
        
        //-- no meds is not true (Phy has selected that meds on test is OK)
        if (!ReqObj.No_Medication_Verified__c)
        {
        }
        else //-- no meds allowed on test
        {
            if (oMeds != null || oMeds.size() > 0)
            {
            }
        }

        GlobalPEERRequisitionDao.getInstance().saveSObject(ReqObj);

        System.debug('medsVerified: ' + medsVerified);
        
        if (isEegUploaded && (medsVerified == null || medsVerified))
        {
           // ReqObj.reeg_type_mod__c = medicatedTestTypeMod;

            if (medConflict)
            {
                ReqObj.Status__c = 'On Hold - EEG Tech conflict';
            }

            Boolean IsEegStatusOK = String.isNotBlank(ReqObj.EEG_Status__c) && 
                                    ReqObj.EEG_Status__c != 'Not Uploaded' && 
                                    ReqObj.EEG_Status__c != 'Bad' && 
                                    ReqObj.EEG_Status__c != 'Not Supported' &&
                                    ReqObj.EEG_Status__c != 'EEG Make does not match file type';

            Boolean CanSubmit = String.isNotBlank(ReqObj.patient_first_name__c) && 
                                String.isNotBlank(ReqObj.patient_name__c) && 
                                String.isNotBlank(ReqObj.patient_dob__c) &&
                                String.isNotBlank(ReqObj.reeg_reeg_type__c) && 
                                String.isNotBlank(ReqObj.reeg_r_eeg_tech_contact__c) &&
                                IsEegStatusOK && 
                                String.isNotBlank(ReqObj.Physician__c) &&
                                ReqObj.Status__c != 'Submitted' && 
                                ReqObj.Status__c != 'Complete' && 
                                ReqObj.Patient_Consent__c;

            if (CanSubmit)
            {
                List<Req_Outcome_Medication__c> ocMedList = new List<Req_Outcome_Medication__c>();
                for(ReqOutcomeMedDisp oMedRow : oMeds) 
                {
                    ocMedList.add(oMedRow.Obj);
                }

                GlobalPEERRequisitionDao.getInstance().submit(ReqObj, ocMedList);

                System.debug('ReqObj.Status__c1: ' + ReqObj.Status__c);
            }
            else
            {
                //-- this status value will trigger a workflow that sends the missing info email alert. 
                //-- after the email alert is sent the workflow will set this status field to Pending.
                ReqObj.Status__c = 'EEG Submitted';
                GlobalPEERRequisitionDao.getInstance().saveSObject(ReqObj);
            }

            System.debug('ReqObj.Status__c2: ' + ReqObj.Status__c);

            PageReference pr = Page.tec_EegTechHome;
            pr.getParameters().put('rid', ReqObj.Id);

            return pr;
        }
        else
        {
            errorMsg = 'A confirmation is needed when a record has active medications';
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, errorMsg);
        //  ApexPages.addMessage(msg);
            return null;
        }
        
    }
    
    public PageReference saveSchedChange()
    {
        if (String.isNotBlank(ReqObj.EEG_Tech_Schedule_Notes__c))
        {
            showReschedule = false;
            ReqObj.reeg_eeg_rec_stat__c = 'Rescheduled';
            GlobalPEERRequisitionDao.getInstance().saveSObject(ReqObj);

            return Page.tec_EegTechHome;
        }
        else
        {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, 'A Schedule Change Note must be entered to reschedule a test');
            ApexPages.addMessage( msg);
            return null;
        }
    }

    public PageReference saveSchedCancel()
    {
        if (String.isNotBlank(ReqObj.EEG_Tech_Schedule_Notes__c))
        {
            showCancel = false;
            ReqObj.reeg_eeg_rec_stat__c = 'Cancelled';
            GlobalPEERRequisitionDao.getInstance().saveSObject(ReqObj);
            return Page.tec_EegTechHome;
        }
        else
        {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, 'A Schedule Change Note must be entered to cancel a test');
            ApexPages.addMessage( msg);
            return null;
        }
    }
    
    public PageReference cancelSchedChange()
    {
        showReschedule = false;
        return null;
    }

    public PageReference cancelSchedCancel()
    {
        showCancel = false;
        return null;
    }
    
    private void setTechObj()
    {
        UserDao uDao = new UserDao();
        User usr = uDao.getById(UserInfo.getUserId());
    
        if (usr != null)
        {
          //  ContactDao cDao = new ContactDao();
          //  eegTech = cDao.getById(usr.ContactId);
        }   
    }
    
    //---------- Outcome med methods ----------------------------------------
    
       //---Build the existing outcome meds list (meds from previous outcome)
    public List<ReqOutcomeMedDisp> getExistingOutcomeMeds()
    {   
         if (oMeds == null)
         {  
            oMeds = new List<ReqOutcomeMedDisp>();  
             // List<string> previousIds = new List<string>();
            ReqOutcomeMedDisp oMed;          
            Integer ctr = -1;
             
            if (medsToUpdate == null) 
            {
                medsToUpdate = new List<String>();
            }
             
             //---Get the current meds only
            if (ReqObj.Id != null)
                previousMeds = ReqOutcomeMedDao.getInstance().getByRequisitionId(ReqObj.Id);
             
            system.debug('### previousMeds:' + previousMeds);
             
             if (previousMeds != null) 
             {
                for(Req_Outcome_Medication__c oMedRow : previousMeds) 
                {
                    ctr++;  
                    oMed = new ReqOutcomeMedDisp(oMedRow);
                    oMed.setDisplayAction(true);
                    oMed.setRowNum(ctr);                  
                    oMeds.Add(oMed);           
                }
                rowCount = ctr + 1; //---ctr is zero based                              
             }
             else {
                oMeds = null;
             }
        }    
        return oMeds;
    }
    
    public PageReference saveMeds()
    {
        system.debug('### saveMeds');
        //if (oMeds != null) 
        //{
        //    system.debug('### oMeds:' + oMeds);
        //    NoStDtMessage = false;
        //    //---Loop through each row
        //    for(ReqOutcomeMedDisp oMedRow : oMeds) {
        //        if (oMedRow != null) {
        //            if (oMedRow.Obj == null) {
        //             //  addMessage('oMedRow.getObj() is null');         
        //            } else {
        //                if (oMedRow.Obj.start_date__c == null && String.isNotBlank(oMedRow.Obj.med_name__c)) {
        //                    NoStDtMessage = true;
        //                    return null;
        //                } else {
        //                    NoStDtMessage = false;
        //                    //Req_Outcome_Medication__c outcMed = oMedRow.Obj;
        //                //  outcMed.r_outcome_id__c = outcomeId;
        //                    //oMedRow.Setup(outcMed);
        //                    SaveOutcomeMed(outcMed);   
        //                }                   
        //            }
        //        }
        //    }
        //}
            
        //if (newMeds != null && newMeds.size() > 0)
        //{
        //    system.debug('### newMeds:' + newMeds);
        //    for(ReqOutcomeMedDisp nMedRow : newMeds) 
        //    {
        //        if (nMedRow != null) 
        //        {
        //            if (nMedRow.Obj == null) 
        //            {
        //             //  addMessage('oMedRow.getObj() is null');         
        //            } 
        //            else 
        //            {
        //                if (nMedRow.Obj.start_date__c == null && String.isNotBlank(nMedRow.Obj.med_name__c)) {
        //                    NoStDtMessage = true;
        //                    return null;
        //                } 
        //                else 
        //                {
        //                    NoStDtMessage = false;
        //                    Req_Outcome_Medication__c outcMed = nMedRow.Obj;
        //                    SaveOutcomeMed(outcMed);   
        //                    if (outcMed != null && outcMed.med_name__c != null && outcMed.med_name__c != '')
        //                        oMeds.add(nMedRow);
        //                }                   
        //            }
        //        }
        //    }
        //}
   
        ////r_patient__c pat = patDao.getById(pId);
        
        ////if (pat != null)
        ////{
        ////    patientIntervalBuilder intBuilder = new patientIntervalBuilder();
        ////    intBuilder.buildAllIntervalsForPat(pat.Id, pat.OwnerId);
        ////}
        
        ////loadAction();
        //isMedEdit = false;
        //addNewMed = false;
  
        //system.debug('### isMedEdit' + isMedEdit);
  
        return null;
    }
    
    // ---Save an outcome med row
    private void SaveOutcomeMed(Req_Outcome_Medication__c medObj)
    {
        //if (medObj != null && medObj.med_name__c != null && medObj.med_name__c != '')
        //{
        //    ocMedDao.saveSObject(medObj);
        //}
        //else 
        //{
        //    //---If the id is not blank and the name is, then delete the old record
        //    if (medObj.id != null) ocMedDao.deleteSObject(medObj);
        //}    
    }

    public PageReference addMedRow()
    {   
        Integer rowNum = Integer.valueOf(System.currentPageReference().getParameters().get('rowNumber'));
        // rowNum is 0 based, rowCount starts at 1
        if (rowNum != null && (rowNum + 1) == rowCount) 
        {
            //---Create blank row
            Req_Outcome_Medication__c objMed = new Req_Outcome_Medication__c();
            ReqOutcomeMedDisp oMed = new ReqOutcomeMedDisp(objMed);
            oMed.setRowNum(rowCount); 
            rowCount++;                         
            oMeds.Add(oMed);    
        }        
        return null;      
    }
    
    public PageReference addBlankMedRows()
    {
        addNewMed = true;
        if (newMeds == null) newMeds = new List<ReqOutcomeMedDisp>();    
                
        for (integer i = 0; i < 5; i++)
        {
            Req_Outcome_Medication__c newMed = new Req_Outcome_Medication__c();
            newMed.unit__c = 'mg';  
            ReqOutcomeMedDisp medDisp = new ReqOutcomeMedDisp(newMed);   
            medDisp.setRowNum(newMeds.size());
            newMeds.add(medDisp);       
        }
        newRowCount = newMeds.size();
        
        return null;
    }  
    
    public PageReference cancelMedEdit()
    {
        addNewMed = false;
        isMedEdit = false;
        return null;
    } 
    
    public PageReference editMeds()
    {
        isMedEdit = true;
        return null;
    }
    
    public PageReference refreshAutoAction()
    {
        return null;
    }  
}