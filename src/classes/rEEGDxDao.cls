//--------------------------------------------------------------------------------
// COMPONENT: Ethos common
//     CLASS: rEEGDxDao
//   PURPOSE: data access object for DX records
//
//     OWNER: CNSR
//   CREATED: 04/6/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class rEEGDxDao extends BaseDao
{
	private static String NAME = 'r_reeg_dx__c';

	private static String fldList;
	public static String getFieldStr()
	{

		if (e_StringUtil.isNullOrEmpty(fldList))
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);

			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.r_reeg_dx__c.fields.getMap());
				//dao.saveFields( NAME, fldList);
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}

		return fldList;
	}

	public r_reeg_dx__c getById(String idInp)
    {
		return (r_reeg_dx__c)getSObjectById(getFieldStr() + ',Reference_Dx__r.PEER_Drug_mapping__c', NAME, idInp);
    }

    public List<r_reeg_dx__c> getByrEEGId(String idInp)
    {
    	string whereStr = 'r_reeg_id__c = \'' + idInp + '\'';
		return (List<r_reeg_dx__c>)getSObjectListByWhere(getFieldStr() + ',Reference_Dx__r.PEER_Drug_mapping__c', NAME, whereStr);
    }

    public r_reeg_dx__c getTestDx()
    {
    	rEEGControllerExt r = new rEEGControllerExt();
    	r_reeg__c reeg = r.getTestReeg();

    	r_reeg_dx__c dx = new r_reeg_dx__c();
		dx.primary__c = true;
		dx.r_reeg_id__c = reeg.Id;
		dx.dx_name__c = 'test';
    	insert dx;

    	return dx;
    }

	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testrEEGDxDao()
    {
        rEEGDxDao dao = new rEEGDxDao();

        System.assert(!e_StringUtil.isNullOrEmpty(rEEGDxDao.getFieldStr()));

        r_reeg_dx__c testDx = dao.getTestDx();

        testDx = dao.getById(testDx.Id);

        r_reeg__c reeg = rEEGDao.getTestReeg();
        List<r_reeg_dx__c> dxList = dao.getByrEEGId(reeg.Id);
    }
}