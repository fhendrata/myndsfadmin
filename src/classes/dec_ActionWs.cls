//Generated by wsdl2apex

public class dec_ActionWs {
    public class testActionResponse_element {
        public String testActionResult;
        private String[] testActionResult_type_info = new String[]{'testActionResult','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://216.26.169.243/','true','false'};
        private String[] field_order_type_info = new String[]{'testActionResult'};
    }
    public class RulesWsSoap {
        public String endpoint_x = 'http://216.26.169.243/pikewsqa/rulesws.asmx';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://216.26.169.243/', 'dec_ActionWs'};
        public String testAction(String sessionId,String reegId) {
            dec_ActionWs.testAction_element request_x = new dec_ActionWs.testAction_element();
            dec_ActionWs.testActionResponse_element response_x;
            request_x.sessionId = sessionId;
            request_x.reegId = reegId;
            Map<String, dec_ActionWs.testActionResponse_element> response_map_x = new Map<String, dec_ActionWs.testActionResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://216.26.169.243/testAction',
              'http://216.26.169.243/',
              'testAction',
              'http://216.26.169.243/',
              'testActionResponse',
              'dec_ActionWs.testActionResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.testActionResult;
        }
    }
    public class testAction_element {
        public String sessionId;
        public String reegId;
        private String[] sessionId_type_info = new String[]{'sessionId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] reegId_type_info = new String[]{'reegId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://216.26.169.243/','true','false'};
        private String[] field_order_type_info = new String[]{'sessionId','reegId'};
    }
}