//--------------------------------------------------------------------------------
// COMPONENT: PikeService
//     CLASS: finalizeController
//   PURPOSE: Controller for the Finalize Popup page
// 
//     OWNER: CNSR
//   CREATED: 03/8/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class finalizeController extends BaseController
{
	public report_document__c reportDoc {get; set;}
	public report_document__c newReportDoc {get; set;}
	public User user {get; set;}
	public string reegId {get; set;}
	public string password {get; set;} 
	public string ResultMessage { get; set; }
	public string javascriptCloseWin { get; set; }
	public rptDocDao rptDao { get; set; }
	
	 //------------- CONSTRUCTOR ---------------------------
    public finalizeController()
    {
    	ResultMessage = ''; 
    }
    
       //------------- Actions ---------------------------
    public void loadAction()
    { 
    	string closeParam = ApexPages.currentPage().getParameters().get('xcl');
    	if (!e_StringUtil.isNullOrEmpty(closeParam)) javascriptCloseWin = '<script> closeWin(); </script>';
    	reegId = ApexPages.currentPage().getParameters().get('rid');
    	
    	
    	user = [select Id, FirstName, LastName, rpt_finalize_pass__c From User where Id=:UserInfo.getUserId()];
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
    		rptDao = new rptDocDao();
    		List<report_document__c> results = rptDao.getActive(reegId);
    		if (results != null && results.size() > 0) reportDoc = results[0];					
    	}
    }
    
    public boolean getShowSave()
    {
    	return (reportDoc != null);
    }
    
    public string getFullUserName()
    {
    	if (user != null)
    		return user.FirstName + ' ' + user.LastName;
    	else
    		return '';
    }
    
    public string getRptName()
    {
    	if (reportDoc != null)
    		return reportDoc.Name;
    	else
    		return '';
    }
    
    public string getRptFileName()
    {
    	if (reportDoc != null)
    		return reportDoc.rpt_file_name__c;
    	else
    		return '';
    }
    
    public string getRptNotes()
    {
    	if (reportDoc != null)
    		return reportDoc.version_notes__c;
    	else
    		return '';
    }
    
    public void setRptNotes(string value)
    {
    	if (reportDoc != null)
    		reportDoc.version_notes__c = value;
    }
    
    public void setPasswordVal(string value)
    {
    	password = value;
    }
    
    public string getPasswordVal()
    {
    	return password;
    }
    
    public PageReference setPass() 
    {
        return null;
    }
    
 	public PageReference saveFinalAction()
    {
    	debug('finalizeController:saveAction:');
		PageReference pr = null;
		
    	if (user != null)
    	{
    		if (!e_StringUtil.isNullOrEmpty(password))
    		{
    			if (!e_StringUtil.isNullOrEmpty(user.rpt_finalize_pass__c))
    			{
	    			if (e_StringUtil.isMatch(user.rpt_finalize_pass__c, password, false))
    				{
    					debug('finalizeController:saveAction:passwordsMatch');
    					
    					if (createAction())
    					{
    						ResultMessage = 'Report has been finalized. This report is being built with a new validation code and will be available shortly.';
    						pr = new PageReference('/apex/finalizePage?xcl=n');	
    					}
    					else
    					{
    						ResultMessage = 'Report finalization has not been save to SalesForce.';
    						debug('ResultMessage: ' + ResultMessage);	
    					}
    				}
    			}
    			else
    				ResultMessage = 'You must enter a password.'; 
    		}
    		else
    			ResultMessage = 'Unable to find you user record.';    		
    	}
    	else
    		ResultMessage = 'You user record password cannot be empty.'; 
    	
    	return pr;
    }
    
    private boolean createAction()
    {
    	boolean returnVal = false;
    	
    	rEEGDao reegDao = new rEEGDao();
    	r_reeg__c reeg = reegDao.getById(reegId);
    	reeg.rpt_stat__c = 'Finalization In Progress';
    	reegDao.Save(reeg, 'REPORT_FINALIZE', user.Id);
    	
    	debug('finalizeController:createAction:reegSaved:reeg.rpt_stat__c: ' + reeg.rpt_stat__c);
    	returnVal = true;
    	
		return returnVal;
    }
    
    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testFinalizeController()
    {
        Account a = new Account(name = 'test account');
        insert a;
        Contact c = new Contact(FirstName = 'first', LastName = 'last', AccountId = a.Id);
        insert c;
        
    	report_document__c testDoc = rptDocDao.getTestRptDoc(); 
    	string reegId = testDoc.r_reeg__c;
    	
		rEEGDao reegDao = new rEEGDao();
		r_reeg__c testReeg = reegDao.getById(reegId);    	

		PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('rid',testReeg.id);

        finalizeController obj = new finalizeController();
        obj.loadAction();
        
        boolean boolVal = obj.getShowSave();
   		string testStr = obj.getFullUserName();
    	testStr = obj.getRptName();
		testStr = obj.getRptFileName();
		obj.setRptNotes('test');
		testStr = obj.getRptNotes();
		obj.setPasswordVal('test');
		testStr = obj.getPasswordVal();
    
    	PageReference pr = obj.setPass(); 
		obj.user = null;
		pr = obj.saveFinalAction();
		obj.loadAction();
		pr = obj.saveFinalAction();
		obj.password = null;
		pr = obj.saveFinalAction();
        obj.password = 'test';
        pr = obj.saveFinalAction();
        
        User u = obj.user;
        u.rpt_finalize_pass__c = '';
        update u;
        obj.user = u;
        pr = obj.saveFinalAction();
        u.rpt_finalize_pass__c = 'test';
        update u;
        obj.user = u;
        pr = obj.saveFinalAction();
        
        reegDao.deleteSObject(testReeg);
        pr = obj.saveFinalAction();
    }
	
}