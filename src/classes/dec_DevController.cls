//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: dec_DevController
// PURPOSE: Development controller for the Rules Based Decision engine
// CREATED: 06/10/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class dec_DevController extends BaseController
{
	public List<dec_action__c> ActiveActionList {get; set;}
	public List<dec_action__c> PendingActionList {get; set;}
	public List<dec_action__c> InactiveActionList {get; set;}
	private dec_ActionDao actDao {get; set;}
	private dec_action__c action {get; set;}
	public string reegId {get; set;}
	public boolean isRunTestBtnVisible {get; set;}
	public boolean isActionSelectEdit {get; set;}
	public boolean isSeqSelectEdit {get; set;}
	public rEEGXmlUtil.ActionObj[] ActArray {get; set;}
	public string result {get; set;}
	
	public dec_DevController()
	{
		actDao = new dec_ActionDao();
		IsTestCase = false;
	}
	
	public dec_DevController(ApexPages.StandardController std)
	{
		actDao = new dec_ActionDao();
		IsTestCase = false;
	}
	
	public PageReference loadAction()
	{
		reegId = ApexPages.currentPage().getParameters().get('rid');
		string edit = ApexPages.currentPage().getParameters().get('edit');
		string editSeq = ApexPages.currentPage().getParameters().get('editSq');
		
		if (ActiveActionList == null)
			ActiveActionList = actDao.getAllActive();
		if (PendingActionList == null)
			PendingActionList = actDao.getAllPending();
		if (InactiveActionList == null)
			InactiveActionList = actDao.getAllInactive();
			
		isRunTestBtnVisible = isRunOk();
		isActionSelectEdit = (!e_StringUtil.isNullOrEmpty(edit) && edit.equals('1'));
		isSeqSelectEdit = (!e_StringUtil.isNullOrEmpty(editSeq) && editSeq.equals('1'));
		
		return null;
	}
	
	private boolean isRunOk()
	{
		boolean activeOK = (ActiveActionList != null) && (ActiveActionList.size() > 0);
		boolean pendingOK = (PendingActionList != null) && (PendingActionList.size() > 0);
		
		return !e_StringUtil.isNullOrEmpty(reegId) && (activeOK || pendingOK);
	}
	
	public PageReference newDecAction()
	{
		return new PageReference('/apex/DecisionActionEditPage?rid=' + reegId);
	}
	
	public PageReference runTestAction()
	{
		PageReference returnVal = null;
		
		updateActionLists();
		
		if (!e_StringUtil.isNullOrEmpty(reegId))
		{
			ActionDao.insertReegAction(reegId, 'REPORT_RULES_TEST');
			/*
			r_action__c procAction = new r_action__c();
	        procAction.type__c = 'REPORT_RULES_TEST';
   	     	procAction.status__c = 'NEW';
        	procAction.reeg_id__c = reegId;
        	insert procAction;
        	*/
        	
        	returnVal = new PageReference( '/' + reegId);
        	returnVal.setRedirect(true);
		}
		
		return returnVal;
	}
	
	
	public PageReference runXmlTestAction()
	{
		PageReference returnVal = null;
		
		ActArray = null;
		String xmlString = '';
		
		if (!IsTestCase)	
		{	
			dec_ActionWs.RulesWsSoap ws = new dec_ActionWs.RulesWsSoap();
			ws.timeout_x = 60000;
			xmlString = ws.testAction('', reegId);
		}
		else
		{
			xmlString = '<dec><result>True</result><act><n>Bridged leads</n><v></v><rules><rule><n>2</n><v>True</v></rule></rules></act><act><n>test 1</n><v>True</v><rules><rule><n>18</n><v>True</v></rule><rule><n>20</n><v>False</v></rule><rule><n>19</n><v>False</v></rule></rules></act></dec>';
		}
		
		if (!e_StringUtil.isNullOrEmpty(xmlString))
		{
//			result = xmlString;	// testing
			ActArray = rEEGXmlUtil.parseActions(xmlString);
		}
		else
		{
			result = 'Error: No xml return from web service. Possible loss of rEEG Id parameter, please go back to rEEG record and start again.';
		}

		return returnVal;
	}
	
	public PageReference editDecActions()
	{
		return new PageReference('/apex/DecisionDevelopmentPage?rid=' + reegId + '&edit=1');
	}
	
	public PageReference saveDecActions()
	{
		updateActionLists();
		return new PageReference('/apex/DecisionDevelopmentPage?rid=' + reegId + '&edit=0');
	}
	
	private void updateActionLists()
	{
		ActiveActionList.addAll(PendingActionList);
		ActiveActionList.addAll(InactiveActionList);
		actDao.saveSObjectList(ActiveActionList);
		ActiveActionList = null;
	}
	
	public PageReference editDecSeq()
	{
		return new PageReference('/apex/DecisionDevelopmentPage?rid=' + reegId + '&editSq=1');
	}
	
	public PageReference saveDecSeq()
	{
		updateActionLists();
		return new PageReference('/apex/DecisionDevelopmentPage?rid=' + reegId + '&editSq=0');
	}

	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testActDevCont()
    {
        dec_DevController cont = new dec_DevController();
        cont.IsTestCase = true;
        
		r_reeg__c reeg = rEEGDao.getTestReeg();
		ApexPages.currentPage().getParameters().put('rid', reeg.Id);
        
        dec_action__c actObj = dec_ActionDao.getTestAction();
     	List<dec_action__c> actList = new List<dec_action__c>();
     	actList.add(actObj);
     	
     	cont.ActiveActionList = actList;
     	cont.PendingActionList = actList;
     	cont.InactiveActionList = actList;
		cont.actDao = new dec_ActionDao();
		cont.action = actObj;
		cont.reegId = reeg.Id;			
		cont.isRunTestBtnVisible = true;
		cont.isSeqSelectEdit = true;
		cont.isActionSelectEdit = true;
		
		cont.runXmlTestAction();
		
		PageReference pr = cont.newDecAction();
		pr = cont.loadAction();
		pr = cont.runTestAction();
    }   
}