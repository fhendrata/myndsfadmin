public abstract class PEERBaseController 
{
    private List<Contact> clinicContactList { get; set;}
    public String Domain { get  {  return rEEGUtil.getWebUrl();  }  }
    public Map<String, Profile_PEER_Permissions__c> ProfilePermissionMap { get; set; }

	public PEERBaseController() {	}

    public void loadPermissions()
    {
        ProfilePermissionMap = new Map<String, Profile_PEER_Permissions__c>();
        for (Profile_PEER_Permissions__c ppp : Profile_PEER_Permissions__c.getAll().values())
            ProfilePermissionMap.put(ppp.Profile_Full_Name__c, ppp);
    }

    public Boolean getPageParamBool(String paramName)
    {
        String val = ApexPages.currentPage().getParameters().get(paramName);
        return (String.isNotBlank(val) && val.equalsIgnoreCase('true'));
    }

    public String getPageParamStr(String paramName)
    {
        return ApexPages.currentPage().getParameters().get(paramName);
    }

    public String quote(String s)
    {
        return '\'' + s + '\'';
    }

	public String getUserName()
	{
		String name = '';
		String firstName = UserInfo.getFirstName();
		String lastName = UserInfo.getLastName();
		
		Integer firstNameLength = e_StringUtil.isNullOrEmpty(firstName) ? 0 : firstName.length();
		Integer lastNameLength = e_StringUtil.isNullOrEmpty(lastName) ? 0 : lastName.length();
		
		//-- believe it or not some records do not have a first name
		if (firstNameLength > 0 && (firstNameLength + lastNameLength) > 12)
			firstName = firstName.substring(0, 1);
		
		if (!e_StringUtil.isNullOrEmpty(firstName))
		{
			name += firstName;
			name += ' ';
		}
		name += lastName;
		return name;
	}

	protected Profile profile;
    public Profile getProfile()
    {
        if (profile == null)
            profile = [Select p.Name From Profile p where p.Id=:UserInfo.getProfileId()];

        return profile;
    }

    public boolean getIsClinicAdminOrAdmin()
    {
        return getIsMgrLevel3() || getIsPortalUserMgr();
    }

    //---Add a message to the message list
     public void addMessage(ApexPages.Severity s, String value)
     {   
        String message = e_StringUtil.isNullOrEmpty(value) ? '' : value; 
        ApexPages.Message msg = new ApexPages.Message(s, message);
        ApexPages.addMessage(msg);
     }

    public Boolean getCanManagePEERs()
    {
        return getHasPermission('Manage_PEER__c');
    }

    public Boolean getCanViewPEERs()
    {
        return getHasPermission('View_PEER__c');
    }

    private Boolean getHasPermission(String permType)
    {
        if (ProfilePermissionMap == null) 
            loadPermissions();

        if (ProfilePermissionMap.isEmpty()) 
            return false;

        List<String> permSetIdList = new List<String>();
        for (String key : ProfilePermissionMap.keySet())
        {
            Profile_PEER_Permissions__c permSetting = ProfilePermissionMap.get(key);

            if (permSetting != null && (Boolean)permSetting.get(permType) != null)
            {
                Boolean isChecked = (Boolean)permSetting.get(permType);
                if (isChecked)
                    permSetIdList.add(ProfilePermissionMap.get(key).PermissionSet_Id__c);
            }
        }

        List<PermissionSetAssignment> permList = null;
        if (!permSetIdList.isEmpty())
        {
            String query = 'Select Id, AssigneeId From PermissionSetAssignment Where AssigneeId = \'' + UserInfo.getUserId() + '\' AND PermissionSetId IN (\'' + String.join(permSetIdList,'\',\'') + '\')';
            permList = (List<PermissionSetAssignment>)Database.query(query);
        }

        return (permList != null && !permList.isEmpty());
    }

	public Boolean getIsSysAdmin()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'System Administrator' || profile.Name == 'CNSR System Administrator' || profile.Name == 'Non Lightning System Administrator'));
    }

    public Boolean getIsSeniorProdMgr()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'CNSR Senior Production Manager');
    }

    public Boolean getIsProdMgr()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'CNSR Production Manager');
    }

    public Boolean getIsMedDir()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'CNSR Medical Director');
    }

    public Boolean getIsSalesDir()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'CNSR Sales Director');
    }

    public Boolean getIsWalterReed()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'WR Physician Portal' || profile.Name == 'Walter Reed Admin Portal'));
    }

    public Boolean getIsClinicAdmin()
    {
    	if (profile == null) getProfile();
        return (profile != null && profile.Name == 'Walter Reed Admin Portal' || profile.Name == 'Physician Portal Manager');
    }

    public Boolean getIsClinicPhy()
    {
    	if (profile == null) getProfile();
        return (profile != null && profile.Name == 'WR Physician Portal');
    }

    public Boolean getIsPhyTech()
    {
    	if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'Level 2 Physician/Tech Portal Profile' || profile.Name == 'Tech Portal Manager' || getIsPortalUserMgr() || getIsMgrLevel4()));
    }

    public Boolean getIsProduction()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'CNSR Production Platform Seat');
    }

    public Boolean getIsPortalUserMgr()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'Physician Portal Manager');
    }

    public Boolean getIsPortalUser3()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'Level 3 Physician Portal Profile'|| profile.Name == 'WR Physician Portal'));
    }

    public Boolean getIsPortalUser2()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'Level 2 Physician Portal Profile'|| profile.Name == 'WR Physician Portal'));
    }

    public Boolean IsMilitaryStudyPortalUser()
    {
        if (profile == null) getProfile();
        system.debug('### profile.Name: ' + profile.Name);
        return (profile != null && (profile.Name == 'WR Physician Portal' || profile.Name == 'Walter Reed Admin Portal'));
    }

    public Boolean getIsPhyAndTechLevel2()
    {
        if (profile == null) getProfile();
        return (profile != null && (profile.Name == 'Level 2 Physician/Tech Portal Profile'));
    }

    public Boolean getIsPortalUser1_1()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'Level 1.1 Physician Portal Profile');
    }

    public Boolean getIsPortalUser1()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'Level 1 Physician Portal Profile');
    }

    public Boolean getIsQA()
    {
        if (profile == null) getProfile();
        return (profile != null && profile.Name == 'rEEG QA');
    }

    public Boolean getIsMgr()
    {
        return getIsMgrLevel3() || getIsMedDir() || getIsQA();
    }

    public Boolean getIsMgrLevel4()
    {
        return getIsSysAdmin() || getIsSeniorProdMgr();
    }

    public Boolean getIsMgrLevel3()
    {
        return getIsMgrLevel4() || getIsProdMgr() || getCanManagePEERs() || getCanViewPEERs();
    }

    public Boolean getIsMgrLevel2()
    {
        return getIsPortalUserMgr() || getIsMgrLevel3() || getIsPortalUser2();
    }

    public Boolean getIsMgrLevel1()
    {
        return getIsMgrLevel2() || getIsPortalUser1() || getIsSalesDir() || getIsProduction();
    }

    public Boolean getIsNotQA()
    {
        return !getIsQA();
    }

    public Boolean getIsNotMgr()
    {
        return !getIsMgr();
    }

    public Boolean getIsAdminOrPortalUserMgr()
    {
        return getIsPortalUserMgr() || getIsSysAdmin();
    }

    public List<Contact> getContactListForAccount()
    {
        if (clinicContactList == null)
        {
            String acctId = getCurrentAccount();

            if (!e_StringUtil.isNullOrEmpty(acctId))
            {
                clinicContactList = [select Id, Name, Title from Contact where Title != 'Administration' AND Contact.AccountId =: acctId];
            }
        }

        return clinicContactList;
    }

    public boolean getIsClinicMoreThanOnePhy()
    {
        clinicContactList = getContactListForAccount();
        return (clinicContactList != null && clinicContactList.size() > 1);
    }

    public String getCurrentAccount()
    {
        List<User> uList = [select Id,Contact.AccountId from User where Id =: UserInfo.getUserId()];
        if(uList != null && !uList.IsEmpty()) return uList.get(0).Contact.AccountId;
        return '';
    }

    public String getCurrentPhysician()
    {

        //TODO-probably need to check if this is actually a physician
        List<User> uList = [select Id,ContactId from User where Id =: UserInfo.getUserId()];
        if(uList != null && !uList.IsEmpty() && uList.get(0).ContactId != null) return uList.get(0).ContactId;
        return '';

    }
}