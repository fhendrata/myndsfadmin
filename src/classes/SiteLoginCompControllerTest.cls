//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: SiteLoginCompControllerTest
// PURPOSE: Test class for Login component controller
// CREATED: 02/12/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class SiteLoginCompControllerTest 
{
	/**
    * Test constructor & loadAction page load method
    */
    private static testmethod void testLoadAction() 
    {
    	PhyPortalLoginController cont = new PhyPortalLoginController();
    	PageReference pr = cont.loadAction();
    }
	
}