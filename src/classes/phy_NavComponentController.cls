public with sharing class phy_NavComponentController extends BaseController
{
	public Integer countOfReferrals;
    public Boolean IsPatientTab { get { return (PageName == 'phy_PatientListS2'); } }
    public Boolean IsOutcomeTab { get { return (PageName == 'phy_OutcomeListS2'); } }
    public Boolean IsPEERTab { get { return (PageName == 'phy_PeerListS2'); } }
    public Boolean IsReferralTab { get { return (PageName == 'phy_PatientReferralListS2'); } }
    public Boolean IsReqListTab { get { return (PageName == 'PEERRequisitionList'); } }
    public String PageName { get; set; }

    public Boolean IsTestUser { get {  return ((UserInfo.getLastName() == 'Doctor') && (UserInfo.getFirstName() == 'POTest'));  }  }

    public phy_NavComponentController() 
    {
        getCountOfReferrals();
    }

    public Integer getCountOfReferrals() 
    {
        User user = [Select Id,ContactId from User where Id =: UserInfo.getUserId()];
        List<Patient_Referral__c> count = [Select Id from Patient_Referral__c 
                                        where Referred_To__c =: user.ContactId
                                        AND Physician_Referral_Status__c !=: 'Accepted'
                                        AND Physician_Referral_Status__c !=: 'Missed'
                                        AND Physician_Referral_Status__c !=: 'Passed'];
        countOfReferrals = count.size();
        return countOfReferrals;
    }


}