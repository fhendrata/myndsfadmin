//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: CustomerPortalLeadDao
// PURPOSE: Data Access for Customer_Portal_Lead__c
// CREATED: 05/05/16 Ethos Solutions - www.ethos.com
// Author: Mitchell Corbin
//--------------------------------------------------------------------------------
public with sharing class CustomerPortalLeadDao extends BaseDao
{
	private static final String NAME = 'Customer_Portal_Lead__c';
	private static final CustomerPortalLeadDao CPLeadDao = new CustomerPortalLeadDao();

	public List<Customer_Portal_Lead__c> CPLeadList { get; set; }

	public CustomerPortalLeadDao()
	{
		super(NAME);
	}

	public static CustomerPortalLeadDao getInstance()
	{
		return CPLeadDao;
	}

	public Customer_Portal_Lead__c getById(String idInp)
	{
		return (Customer_Portal_Lead__c)getSObjectById(getFieldStrNew(), idInp);
	}

	public Customer_Portal_Lead__c getTestCPLead() 
	{
		r_patient__c testPat = patientDao.getTestPat();

		Customer_Portal_Lead__c newCPLead = new Customer_Portal_Lead__c(First_Name__c = 'First', Last_Name__c = 'Last', Email__c = 'test123@excample.com', Phone__c = '(123) 456-7890', 
																		RecordTypeId = CPLeadDao.getRecordTypeIdByName('Patient Lead'), Company__c = 'Physician Referral',
																		Lead_Status__c = 'EEG/PEER Requested', Lead_Source__c = 'Physician Referral', Processing_Status__c = 'New',
																		Referral_Physician__c = ContactDao.getTestPhysician().Id);
		newCPLead.Patient__c = testPat.Id;
		insert newCPLead;
		return newCPLead;
		
	}

	public Id getRelatedPEERId(Customer_Portal_Lead__c cpLead)
	{
		CPLeadList = new List<Customer_Portal_Lead__c>([SELECT Id, (SELECT Id FROM 	PEER_Requisitions__r) FROM Customer_Portal_Lead__c WHERE Id = :cpLead.Id]);

		if (!CPLeadList.isEmpty() && !CPLeadList.get(0).PEER_Requisitions__r.isEmpty())
		{
			return CPLeadList.get(0).PEER_Requisitions__r.get(0).Id;
		}
		else
			return null;
	}
}