//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: rpt_PhyDao
// PURPOSE: Data access class for the Physician report object
// CREATED: 09/17/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class rpt_PhyDao extends BaseDao
{
	private static String NAME = 'rpt_physician__c';    
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			e_SysTableDao dao = new e_SysTableDao();
			e_SysTable__c obj = dao.getByName(NAME);
			
			if (obj == null)
			{
				fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.rpt_physician__c.fields.getMap());
			}
			else
			{
				fldList = obj.field_list__c;
			}
		}
		
		return fldList;
	}
	
	public rpt_physician__c getById(String idInp)
    {
		return (rpt_physician__c)getSObjectById(getFieldStr(), NAME, idInp);
    } 
    
    public List<rpt_physician__c> getByMonthYear(integer month, integer year)
    {
    	rpt_physician__c returnVal;
    	string whereStr = ' month__c = ' + month + ' and year__c = ' + year;
    	return getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'name', '9500');
    } 
    
    public rpt_physician__c getByIdMonthYear(string phyId, integer month, integer year)
    {
    	rpt_physician__c returnVal;
    	string whereStr = ' physician__c = \'' + phyId + '\' and month__c = ' + month + ' and year__c = ' + year;
    	
    	List<rpt_physician__c> phyList = (List<rpt_physician__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr);
		return (phyList != null && phyList.size() > 0) ? phyList[0] : null;
    } 
    
    public List<rpt_physician__c> getAll()
    {
		return (List<rpt_physician__c>)getSObjectListByWhere(getFieldStr(), NAME, '');
    }
    
    public List<rpt_physician__c> getfirstK()
    {
    	return (List<rpt_physician__c>)getSObjectListByWhere(getFieldStr(), NAME, '', 'name', '999');
    }  
    
    public List<rpt_physician__c> getByName(string lname)
    {
    	string whereStr = ' name like \'' + lname + '%\'';
    	return (List<rpt_physician__c>)getSObjectListByWhere(getFieldStr(), NAME, whereStr, 'name', '999');
    }  
    
    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testrpt_PhyDao()
    {
        rpt_PhyDao dao = new rpt_PhyDao();
        dao.IsTestCase = true;
        rpt_physician__c rptObj = rpt_PhyDao.getTestRptPhy();
        
        rptObj = dao.getById(rptObj.Id);
        
        List<rpt_physician__c> phyList = dao.getAll();
        phyList = dao.getfirstK();
        phyList = dao.getByName('test');
    }
        
	public static rpt_physician__c getTestRptPhy()
    {
    	Contact con = ContactDao.getTestPhysician();
    	
    	rpt_physician__c rptPhy = new rpt_physician__c();
    	rptPhy.name = 'test';
    	rptPhy.physician__c = con.Id;
    	
    	insert rptPhy;
    	
    	return rptPhy;
    }

}