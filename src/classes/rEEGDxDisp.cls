public class rEEGDxDisp extends BaserEEGDxDisp
{
    public rEEGDxDisp () {}
    public rEEGDxDisp (r_reeg_dx__c dx)
    {
        Setup(dx);
    }

    public Boolean getShowLabel()
    {
        return !getShowOther();
    }
    
    public Boolean getShowOther()
    {
        if (getRefObj() != null && getRefObj().name != null && getRefObj().name.startsWith('other')) return true;

        return false;
    }

    public Boolean IsSelected()
    {
        return getObj().primary__c || getObj().secondary__c || getObj().rule_out__c;
    }   

     //------------TEST-----------------------------
    public static testMethod void testDxDisp()
    {
        r_reeg_dx__c refObj = new r_reeg_dx__c();

        ref_dx__c obj = new ref_dx__c();
        obj.name = 'other';

        rEEGDxDisp disp = new rEEGDxDisp();
        disp.setRefObj(obj);
        disp.SetupRef(obj);
        disp.Setup( refObj);

        ref_dx__c obj2 = disp.getRefObj();

        System.assertEquals( obj, obj2);
        System.assertEquals( disp.getShowOther(), true);
        System.assertEquals( disp.getShowLabel(), false);


        r_reeg__c reeg = rEEGDao.getTestReeg();
        disp.SetReegId(reeg.Id);
    }
}