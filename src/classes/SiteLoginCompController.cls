global with sharing class SiteLoginCompController  extends BaseController
{
    public String username {get; set;}
    public String password {get; set;}
    
   	public SiteLoginCompController () {}

    global PageReference loginAction() 
    {   
        PageReference pr = null;
        String startUrl = 'apex/phy_PeerListS2';
        
        system.debug('### username: ' + username + ' pass: ' + password + ' startUrl: ' + startUrl);
        
        try
        {
            system.debug('### login attempt');
            pr = Site.login(username, password, startUrl);
        }
        catch(Exception ex)
        {
            //- we never get here
            system.debug('### login ex: ' + ex);
        }
        
        return pr;
    }
    
 
}