//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: i_eegTechListController
// PURPOSE: 
// CREATED: 11/22/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class i_eegTechListController extends BaseController
{
	public List<Contact> techList {get; set;}
	
	public void loadAction()
    {
    	loadTechList();
    	
    }
    
    public void loadTechList()
    {
    	ContactDao cDao = new ContactDao();
    	techList = cDao.getAllEegTechs();
    	if(techList.size() > 0 && techList != null) {
        	for (Integer i = 0; i < 20; i++)
        	{
        		techList.add(techList[0]);
        	}
        }
    }

	//-----------------------------------------------------------------------
    //--                          TEST METHODS               ---
    //-----------------------------------------------------------------------
    public static testMethod void testEEGTechListController()
    {
        RecordTypeDao rtDao = new RecordTypeDao();
        ContactDao ctDao = new ContactDao();
        RecordType rt = rtDao.getByRecordTypeName('EEG Tech Contact');
        system.debug(rt.Id);
    	i_eegTechListController cont = new i_eegTechListController();
        Account a = new Account(name = 'test account');
        insert a;
        Contact c = new Contact(FirstName = 'Joe', LastName = 'Test', AccountId = a.Id,RecordTypeId=rt.Id);
        insert c;
    	cont.loadAction();
    }

}