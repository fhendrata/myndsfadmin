global class bat_PhyReport implements Database.Batchable<sObject>
{
   	global final String Query;
   	global final date todaysDate;
   	global final date nextMonthsDate;

   	global bat_PhyReport(String q)
   	{
   		Query = q;
   		todaysDate = date.today();
   		nextMonthsDate = todaysDate.addMonths(1);
   	}
	
   	global Database.QueryLocator start(Database.BatchableContext BC)
	{
    	return Database.getQueryLocator(Query);
   	}

 	global void execute(Database.BatchableContext BC, List<sObject> scope)
   	{
   		if (nextMonthsDate != null && nextMonthsDate.day() == 1)
   		{
			DateTime selDt = DateTime.newInstance(nextMonthsDate.year(), nextMonthsDate.month(), 1);
			rpt_PhyProcessController cont = new rpt_PhyProcessController();
			for(Sobject s : scope)
      		{
      			cont.buildRpt(s, selDt);
      		}
   		}
   	}
 
   	global void finish(Database.BatchableContext BC)
   	{
   		date tDate = date.today();
		DateTime selDt = DateTime.newInstance(tDate.year(), tDate.month(), 1);
		rpt_PhyProcessController cont = new rpt_PhyProcessController();
		cont.processRpts(todaysDate);
   	}
   	
   	// ---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
    	r_patient__c pat = patientDao.getTestPat();
    	string query = 'select id from r_patient__c where id=\'' + pat.id + '\'';
    	bat_PhyReport obj = new bat_PhyReport(query);
    }
}