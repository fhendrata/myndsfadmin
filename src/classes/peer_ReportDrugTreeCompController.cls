//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: peer_ReportDrugTreeCompController
// PURPOSE: Controller class for the ReportDrugTree Component
// CREATED: 09/017/13 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
public with sharing class peer_ReportDrugTreeCompController  extends BaseDrugReportController
{
    public string BrainMapUrl {get; set;}
    //public r_reeg__c reeg { get; set; }
   
    public String getSummaryUrl()
    {
    	return '/apex/peer_Summary2?pgid=' + this.pageId + '&rid=' + this.reegId;
    }
    
    public PageReference orderNeuroReport()
    {
    	reeg.do_not_send_neuro__c = false;
    	reeg.Neuro_Order_Date__c = DateTime.now();
        reeg.neuro_stat__c  = 'Pending';
    	rEegDao rDao = new rEegDao();
    	rDao.Save(reeg, 'ORDER_NEURO_REPORT');
    	
    	return null;
    }
    
    public PageReference returnAction()
    {
        PageReference pr = new PageReference('/apex/phy_PeerListS2');
  
        pr.setRedirect(true);
        return pr;
    }
}