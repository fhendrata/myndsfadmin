//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: wr_PeerRequisitionController2
// PURPOSE: Controller class for EEG upload page
// CREATED: 04/18/12 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class wr_PeerRequisitionController2 extends wr_BaseWizardController
{
	public boolean isSubmitVerified { get; set; }
	public Contact eegTech {get; set;}
	public boolean isEegUploaded {get; set;}
	public boolean isEEGActive {get; set;}
    
    public wr_PeerRequisitionController2()
    {
    	init();
    }
    
    public void init()
    {
    	this.patId = ApexPages.currentPage().getParameters().get('id');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        
        rDao = new rEEGDao();
        pDao = new patientDao();
        isSubmitVerified = false;
    }
    
    public PageReference loadAction()
    {
    	if (!e_StringUtil.isNullOrEmpty(patId))
    		this.patient = pDao.getById(patId);
    	
    	if (!e_StringUtil.isNullOrEmpty(reegId))
   			this.reeg = rDao.getById(reegId, true);
    	
    	if (reeg != null)
		{
			if (!e_StringUtil.isNullOrEmpty(reeg.r_eeg_tech_contact__c))
			{
				ContactDao cDao = new ContactDao();
    			eegTech = cDao.getById(reeg.r_eeg_tech_contact__c);  				
   				if (eegTech != null)
	    			reeg.eeg_make__c = eegTech.eeg_tech_default_test_make__c;
			}
			
			isEegUploaded = (!e_StringUtil.isNullOrEmpty(reeg.eeg_rec_stat__c) && reeg.eeg_rec_stat__c == 'Received');
		}
		
		return null;
    }
    
    public string getEnv()
	{
		return rEEGUtil.getEnvString();
	}
    
   	public PageReference submit()
    {
    	PageReference returnVal = null;
		if (eegTech != null)
		{
			eegTech.eeg_tech_default_test_make__c = reeg.eeg_make__c;
			ContactDao cDao = new ContactDao();
			cDao.saveSObject(eegTech);	
		}
			
		r_reeg__c reegCheck = rDao.getById(reeg.Id, true);		
		if (!e_StringUtil.isNullOrEmpty(reegCheck.eeg_upload__c) && reegCheck.eeg_upload__c == 'Yes')
		{
			isEegUploaded = true;
		}
		else
		{
			string errorMsg = 'The EEG file needs to be uploaded before submitting';
			ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, errorMsg);
        	ApexPages.addMessage(msg);
		}
		
		if (isEegUploaded)
		{
			reeg.eeg_rec_date__c = DateTime.now();
			reeg.eeg_rec_stat__c = 'Received';
			reeg.is_ng_only__c = true;
			reeg.Process_Used_For_Test__c = 'Military Study';

	        rDao.Save(reeg, 'NEW_REEG');
			
			returnVal = gotoReegView();
		}
	    
	    return returnVal; 
    } 
     
	    //---TEST METHODS ------------------------------
    public static testMethod void testUploadController()
    {
    	wr_PeerRequisitionController2 cont = new wr_PeerRequisitionController2();
		cont.IsTestCase = true;
    	r_reeg__c reeg = rEEGDao.getTestReeg();
        cont.reeg = reeg;
        patientDao patDao = new patientDao();
        patDao.IsTestCase = true;
        
        cont.patient = patDao.getById(reeg.r_patient_id__c);
 		cont.patId = ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
    	
    	PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('id',reeg.id);

		cont.loadAction();
		
		rEEGDao rDao = new rEEGDao();
		rDao.saveSObject(reeg);
		
		cont.reegId = reeg.Id;
        
        cont.isSubmitVerified = true;
        cont.reegId = reeg.Id;
        reeg.reeg_type__c = 'Type II';
        rDao.saveSObject(reeg);
        
        cont.submit();
   
        System.assertEquals( 1, 1);
    }
}