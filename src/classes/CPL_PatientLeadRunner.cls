//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: CPL_PatientLeadRunner
// PURPOSE: Runner for the Lead update scheduled process
// CREATED: 05/05/16 Ethos Solutions - www.ethos.com
// Author: Mitchell Corbin & Joe DePetro
//--------------------------------------------------------------------------------
global class CPL_PatientLeadRunner implements Schedulable
{
	global void execute(SchedulableContext sc) 
	{	
		string query = 'SELECT Id, Name, First_Name__c, Last_Name__c, Email__c, Phone__c, Company__c, Lead_Status__c, PEER_Completed_Date__c, Lead_Source__c, Referral_Physician__c, Processing_Status__c, Patient__c FROM Customer_Portal_Lead__c WHERE (Processing_Status__c = \'New\' OR Processing_Status__c = \'Needs Update\')';	
		system.debug('CPL_PatientLeadRunner:query: ' + query);

		database.executeBatch(new CPL_PatientLeadProcessor(query), 10);
	}
}