global class sch_PhyRpt implements Schedulable
{
	global void execute(SchedulableContext SC) 
   	{
   		// we don't allow blank, CNSR EEG Tech or CNSR Contact record types
    	string whereClause = ' RecordTypeId != \'\' and RecordTypeId != \'0126000000056yj\' and RecordTypeId != \'01260000000563M\'';
		string orderClause = '';
		
		ContactDao conDao = new ContactDao();
		string query = conDao.getSql('Id, FirstName, LastName, reeg_forecast__c', 'Contact', whereClause, orderClause);

		database.executeBatch(new bat_PhyReport(query), 10);
   	}
}