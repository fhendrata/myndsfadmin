public class patientIntervalsDisp 
{
    public r_patient_outc__c obj { get; set; }
    public r_outcome_med__c objMed { get; set; }
    
    public void Setup(r_patient_outc__c o) {
        obj = o;
    }

    public void SetupMed(r_outcome_med__c oMed) {
        objMed = oMed;
    }
    
    public Integer duration { get; set; }
    public void setDuration(Integer d) {
    	duration = d;
    }
    
    public Integer numberMeds { get; set; }
    public void setNumberMeds (Integer n) {
    	numberMeds = n;
    }
    
    public Integer medIndex { get; set; }
    public void setMedIndex (Integer mi) {
    	medIndex = mi;
    }
    
    public Date startDate { get; set; }
    public void setStartDate (Date sd) {
    	startDate = sd;
    }
    
    public Date endDate { get; set; }
    public void setEndDate (Date ed) {
    	endDate = ed;
    }
    
    public Integer interval;
    public Integer getInterval() {
    	if (startDate != null && endDate != null) {
    		interval = startDate.daysBetween(endDate);
    	}
    	return interval;
    }
    public void setInterval(Integer i) {   	
    	interval = i;
    }
    
    public String cgiScore;
    public void setCgiScore(String c) {
    	cgiScore = c;
    }
    
    //------------TEST-----------------------------
    public static testMethod void testpatientIntervalsDisp()
    {
        patientIntervalsDisp disp = new patientIntervalsDisp();
        disp.setDuration(1);
        disp.setInterval(2);
		disp.setNumberMeds(3);
		disp.setMedIndex(4);
		disp.setCgiScore('4 - Baseline');
		disp.setStartDate(date.today());
		disp.setEndDate(date.today());
		
		integer intervalVal = disp.getInterval();
		
		r_patient_outc__c obj = new r_patient_outc__c();
		disp.Setup(obj);
		
		r_outcome_med__c objMed = new r_outcome_med__c();
		disp.SetupMed(objMed);
    }
}