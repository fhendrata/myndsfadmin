//--------------------------------------------------------------------------------
// COMPONENT: SSearch
//     CLASS: e_PEERRequisitionPagerController
//   PURPOSE: Controller for the PEER Requisition Pager Component
// 
//     OWNER: CNS Response
//   CREATED: 03/10/16 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class e_PEERRequisitionPagerController extends BaseController
{
    public e_PEERRequisitionPagerController()
    {
        isTest = false;
        PageNumberText = '';
    }

	public boolean isTest { get; set; }
    public String PageNumberText { get; set; }

    private PEERRequisitionPager pager;
    public PEERRequisitionPager getPager()
    {
        return pager;
    }
    public void setPager(PEERRequisitionPager value)
    {
    	if (isTest)
    	{
    		if (value != null) value.setRecordCount(10);
    	}
    	
        if (value != null && value.getRecordCount() != null)
        {
            PageNumberText = '';
            Decimal numPages = Double.valueOf(String.valueOf(value.getRecordCount())) / value.getDispRows();

            if (numPages > 1)
                PageNumberText = '' + value.getCurrentPage() + ' of ' + numPages.round(system.roundingMode.CEILING);
        
            pager = value;
        }
    }
    
    private String reRender;
    public String getReRender()
    {
        return reRender;
    }
    public void setReRender(String value)
    {
        reRender = value;
    }
    
    
}