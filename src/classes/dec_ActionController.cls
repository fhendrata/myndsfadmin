//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: dec_ActionController
// PURPOSE: Action controller for the Rules Based Decision engine
// CREATED: 05/10/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class dec_ActionController extends CrudController
{
	public string objPicklist1 {get; set;}
	public string objPicklist2 {get; set;}
	public string objPicklist2_2 {get; set;}
	public string objPicklistLoc1 {get; set;}
	public string objPicklistLoc2 {get; set;}
	public string objPicklistLoc2_2 {get; set;}
	public string fieldPicklist1 {get; set;}
	public string fieldPicklist1_2 {get; set;}
	public string fieldPicklist2 {get; set;}
	public string fieldPicklist2_2 {get; set;}
	public string objPicklistLbl1 {get; set;}
	public string objPicklistLbl2 {get; set;}
	public string objPicklistLbl2_2 {get; set;}
	public string fieldPicklistLbl1 {get; set;}
	public string fieldPicklistLbl1_2 {get; set;}
	public string fieldPicklistLbl2 {get; set;}
	public string fieldPicklistLbl2_2 {get; set;}
	public string ruleActionSel {get; set;}
	public double highestRuleNum {get; set;}
	public boolean isEditAvail {get; set;}
	public boolean isActivateAvail {get; set;}
	public boolean isInactivateAvail {get; set;}
	public string ruleId {get; set;}
	public string reegId {get; set;}
	public boolean isCloneEdit {get; set;}
	
	public string priObjSet {get; set;}
	public string priFieldSet {get; set;}

	public dec_rule__c inputRule {get; set;}
	List<dec_rule__c> ruleList {get; set;}
	public dec_action__c action {get; set;}
	dec_ActionDao dao = new dec_ActionDao();
	private List<SelectOption> objectNameList;
	
	//---Base Constructor (go to test mode)
	public dec_ActionController()
	{
		IsTestCase = true;
		init(); 
	}
	
	//---Constructor from the standard controller
    public dec_ActionController(ApexPages.StandardController stdController)
    {    
        init(stdController); 
        init();
    }
    
      //---Local init
    public void init()
    {
    	BasePageName = 'DecisionAction';
    	IsNewPageNameType = false;
    	CrudDao = dao;
    }
    
    public boolean getIsRuleEditable()
    {
    	return (action.status__c == 'Pending');
    }
    
     //---Local setup
    public override PageReference setup() 
    {		
    	reegId = ApexPages.currentPage().getParameters().get('rid');
    	string idParam = ApexPages.currentPage().getParameters().get('id');
    	string cloneParam = ApexPages.currentPage().getParameters().get('cl');
    	if (!e_StringUtil.isNullOrEmpty(cloneParam))
    	{
    		isCloneEdit = boolean.valueOf(cloneParam); 
    	} 
    	
    	if (!e_StringUtil.isNullOrEmpty(idParam))
    		action = dao.getById(idParam);
    	else
    		action = new dec_action__c();
    	
    	inputRule = new dec_rule__c();
    	ruleList = new List<dec_rule__c>();
    	highestRuleNum = 0;
    	
    	if(action.status__c == null) action.status__c = 'Pending';
    	
    	isEditAvail = (action.status__c == 'Pending');
    	isActivateAvail = (action.status__c == 'Inactive' || action.status__c == 'Pending');
    	isInactivateAvail = (action.status__c == 'Active');
    	
    	return null;
    }  
    
    public PageReference save() 
    {	
		CrudObj = action;
		ViewOnSave = 'true';
		
		PageReference pr = saveAction();
		
		if (!e_StringUtil.isNullOrEmpty(reegId)) {
			pr.getParameters().put('rid', reegId);
		}
		
    	return pr;		
    	// return saveAction();
    }  
    
    public PageReference editActionOverride() 
    {	
		CrudObj = action;
		ViewOnSave = 'true';
		PageReference pr = editAction();
		pr.getParameters().put('rid', reegId);
    	return pr;
    }  
    
    public PageReference cloneActionOverride() 
    {	
		CrudObj = dao.cloneObj(action);

		dao.saveSObject(CrudObj);
		
		dec_RuleDao ruleDao = new dec_RuleDao();
		List<dec_rule__c> newRules = ruleDao.cloneRulesForAction(action.Id, CrudObj.Id);
		ruleDao.saveSObjectList(newRules);
		
		ViewOnSave = 'false';
		ObjId = CrudObj.Id;
		PageReference pr = editAction();
		pr.getParameters().put('rid', reegId);
		pr.getParameters().put('isNew', 'true');
		pr.setRedirect(true);
    	return pr;
    }  
    
    
    public PageReference cancel() 
    {
    	CrudObj = action;	
    	
    	PageReference pr = cancelAction();
		pr.getParameters().put('rid', reegId);
    	return pr;
    }  
    
    public PageReference inactivate()
    {        
        action.status__c = 'Inactive';
        action.inactivated_date__c = DateTime.now();
        
        return save();
    }
    
    public PageReference activate()
    {        
        action.status__c = 'Active';
        action.activated_date__c = DateTime.now();
        
        return save();
    }
    
    public PageReference deleteActionOverride()
    {        
    	if (action != null && action.activated_date__c == null)
    	{
    		try
    		{
    			delete action;
    			CrudObj = null;
    		}
    		catch(Exception ex)
    		{
    			handleError( 'deleteActionOverride', ex);
    		}
    		
    		return new PageReference('/apex/DecisionDevelopmentPage?rid=' + reegId);
    	}
    	else
        	return null;
    }
    
    public PageReference dbTableAction()
    {   
    	Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 

		Map<String,String> prefixKeyMap = new Map<String,String>{};
		for(String sObj : gd.keySet())
		{
		   Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
		   prefixKeyMap.put(r.getName(), r.getKeyPrefix());
		}
		
    	return new PageReference('/' + prefixKeyMap.get('database_table__c'));
    }

    public PageReference checkSyntaxAction()
    {  
    	List<double> condNumList = new List<double>();
    	
        return null;
    }
    
    public PageReference loadPrimaryObjAction()
	{
		action.pri_obj_name__c = objPicklist1;
    	action.pri_obj_label__c = objPicklistLbl1;
    	action.pri_field_name__c = fieldPicklist1;
    	action.pri_field_label__c = fieldPicklistLbl1;
		
		return null;
	}
    
	public PageReference addRuleAction()
	{
		if (inputRule != null && action != null)
		{
			inputRule.pri_object__c = objPicklist1;
			inputRule.sec_object__c = objPicklist2;
			inputRule.sec_object_2__c = objPicklist2_2;
			
			inputRule.pri_datatype__c = objPicklistLoc1;
			inputRule.sec_datatype__c = objPicklistLoc2;
			inputRule.sec_datatype_2__c = objPicklistLoc2_2;			

			inputRule.pri_field__c = fieldPicklist1;
			inputRule.pri_field_2__c = fieldPicklist1_2;
			inputRule.sec_field__c = fieldPicklist2;
			inputRule.sec_field_2__c = fieldPicklist2_2;
			inputRule.pri_object_label__c = objPicklistLbl1;
			inputRule.sec_object_label__c = objPicklistLbl2;
			inputRule.sec_object_label_2__c = objPicklistLbl2_2;
			inputRule.pri_field_label__c = fieldPicklistLbl1;
			inputRule.pri_field_label_2__c = fieldPicklistLbl1_2;
			inputRule.sec_field_label__c = fieldPicklistLbl2;
			inputRule.sec_field_label_2__c = fieldPicklistLbl2_2;
			inputRule.r_dec_action__c = action.Id;

			if (!e_StringUtil.isNullOrEmpty(ruleId))
				inputRule.Id = ruleId;
			else
				inputRule.rule_number__c = highestRuleNum + 1;

			dec_RuleDao ruleDao = new dec_RuleDao();
			if(ruleDao.saveSObject(inputRule))
				inputRule = new dec_rule__c();
		}
		
		return null;
	}
	
	public List<dec_rule__c> getRuleList()
	{
		if (action != null)
		{
			dec_RuleDao ruleDao = new dec_RuleDao();
			ruleList = ruleDao.getByActionId(action.Id);
		}
		
		if (ruleList != null && ruleList.size() > 0)
		{
			for(dec_rule__c rule : ruleList)
			{
				if (highestRuleNum < rule.rule_number__c) highestRuleNum = rule.rule_number__c;
			}
		}
		
		return ruleList;	
	}
	
	
	public List<SelectOption> getObjList()
	{
		if (objectNameList == null)
		{
			objectNameList = e_DataUtil.getAllObjsNames(); 
		}
		
		return objectNameList;	
	}
	
	//---------------TEST METHODS ---------------------------------
    //
    //-------------------------------------------------------------
    public static testMethod void testController()
    {
    	dec_ActionController cont= new dec_ActionController();
    	
    	cont.objPicklist1 = 'test';
	 	cont.objPicklist2 = 'test';
	 	cont.objPicklist2_2 = 'test';
	 	cont.objPicklistLoc1 = 'test';
		cont.objPicklistLoc2 = 'test';
	 	cont.objPicklistLoc2_2 = 'test';
	 	cont.fieldPicklist1 = 'test';
	 	cont.fieldPicklist1_2 = 'test';
	 	cont.fieldPicklist2 = 'test';
	 	cont.fieldPicklist2_2 = 'test';
	 	cont.objPicklistLbl1 = 'test';
	 	cont.objPicklistLbl2 = 'test';
	 	cont.objPicklistLbl2_2 = 'test';
	 	cont.fieldPicklistLbl1 = 'test';
	 	cont.fieldPicklistLbl1_2 = 'test';
	 	cont.fieldPicklistLbl2 = 'test';
	 	cont.fieldPicklistLbl2_2 = 'test';
	 	cont.ruleActionSel = 'test';
	 	cont.highestRuleNum = 5.3;
	 	cont.isEditAvail = true;
	 	cont.isActivateAvail = true;
	 	cont.isInactivateAvail = true;
	 	cont.inputRule = dec_RuleDao.getTestRule();
	 	cont.ruleId = cont.inputRule.Id;
	 	cont.ruleList = new List<dec_rule__c>();
		cont.action = dec_actionDao.getTestAction();

		ApexPages.currentPage().getParameters().put('id', cont.action.Id);
		PageReference pr = cont.setup(); 
		pr = cont.save();
		pr = cont.editActionOverride();
		pr = cont.cancel();
		pr = cont.inactivate();
		pr = cont.activate();
		pr = cont.deleteActionOverride(); 
		pr = cont.dbTableAction();
		pr = cont.checkSyntaxAction(); 
		pr = cont.addRuleAction();
    
    	cont.ruleList = cont.getRuleList();
    	List<SelectOption> optionList = cont.getObjList();
    }
}