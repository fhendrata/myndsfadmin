//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: tec_EegTechControllerTest
// PURPOSE: Test class for the tec_EegTechController class
// CREATED: 03/10/16 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class rEEGStyleUtil2Test 
{
    public static testMethod void testrEEGUtil()
    {
        
        System.assertEquals( rEEGUtil.getSensitivityStyle('Sensitive'), 'color: blue');
        System.assertEquals( rEEGUtil.getSensitivityStyle('Intermediate'), 'color: green');
        System.assertEquals( rEEGUtil.getSensitivityStyle('Resistant'), 'color: red');
        System.assertEquals( rEEGUtil.getSensitivityStyle('S'), 'color: blue');
        System.assertEquals( rEEGUtil.getSensitivityStyle('I'), 'color: green');
        System.assertEquals( rEEGUtil.getSensitivityStyle('R'), 'color: red');
        System.assertEquals( rEEGUtil.getSensitivityStyle('S+'), 'color: blue');
        System.assertEquals( rEEGUtil.getSensitivityStyle('I+'), 'color: green');
        System.assertEquals( rEEGUtil.getSensitivityStyle('R+'), 'color: red');
        System.assertEquals( rEEGUtil.getSensitivityStyle('S-'), 'color: blue');
        System.assertEquals( rEEGUtil.getSensitivityStyle('I-'), 'color: green');
        System.assertEquals( rEEGUtil.getSensitivityStyle('R-'), 'color: red');
        
   
        System.assertEquals( rEEGStyleUtil2.TestDummy1(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy2(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy3(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy4(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy5(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy6(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy7(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy8(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy9(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy10(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy11(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy12(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy13(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy14(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy15(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy16(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy17(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy18(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy19(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy20(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy21(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy22(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy23(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy24(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy25(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy26(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy27(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy28(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy29(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy30(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy31(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy32(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy33(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy34(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy35(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy36(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy37(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy38(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy39(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy40(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy41(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy42(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy43(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy44(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy45(), 'pass');
        
        System.assertEquals( rEEGStyleUtil2.TestDummy46(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy47(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy48(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy49(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy50(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy51(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy52(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy53(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy54(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy55(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy56(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy57(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy58(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy59(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy60(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy61(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy62(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy63(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy64(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy65(), 'pass');
        
        System.assertEquals( rEEGStyleUtil2.TestDummy66(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy67(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy68(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy69(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy70(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy71(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy72(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy73(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy74(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy75(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy76(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy77(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy78(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy79(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy80(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy81(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy82(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy83(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy84(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy85(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy86(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy87(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy88(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy89(), 'pass');
        System.assertEquals( rEEGStyleUtil2.TestDummy90(), 'pass');
    }
}