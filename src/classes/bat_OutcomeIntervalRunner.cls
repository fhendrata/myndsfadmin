//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: bat_OutcomeIntervalRunner
// PURPOSE: Scheduled class for running the Outcome Interval Builder
// CREATED: 02/06/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
global class bat_OutcomeIntervalRunner implements Schedulable
{	
	global void execute(SchedulableContext SC) 
   	{
   		// get any Admission Applications that do not have the Lead lookup field set 
    	string whereClause = ' Lead__c = \'\'';
		string orderClause = '';
		
		GlobalPatientDao patDao = new GlobalPatientDao();
		string query = patDao.getSql('Id, OwnerId, Name', 'r_patient__c', whereClause, orderClause);
		
		system.debug('bat_OutcomeIntervalProcessor:query: ' + query);

		database.executeBatch(new bat_OutcomeIntervalProcessor(query), 10); 
		
   	}
}