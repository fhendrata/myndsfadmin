//--------------------------------------------------------------------------------
// COMPONENT: CNSR rEEG
//     CLASS: e_SysTableDao
//   PURPOSE: record for saving the fields of objects
// 
//     OWNER: CNSR
//   CREATED: 03/25/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class e_SysTableDao extends BaseDao
{
	private static final String NAME = 'e_SysTable__c';
	
	private static String fldList;	
	public static String getFieldStr()
	{
		if (e_StringUtil.isNullOrEmpty(fldList)) 
		{
			fldList = e_FieldUtil.getFieldSql(Schema.SObjectType.e_SysTable__c.fields.getMap());
		}
		
		return fldList;
	}  
	
	public void saveFields(String tableName, String fieldList)
	{
		e_SysTable__c obj = new e_SysTable__c();
		obj.name = tableName;
		obj.field_list__c = fieldList;
				
		insert obj;
	}
	
	public void clearAll()
	{
		List<e_SysTable__c> tableList = (List<e_SysTable__c>)getSObjectListByWhere(getFieldStr(), NAME, '');

		if (tableList != null)
		{
			for(e_SysTable__c row : tableList)
			{
				deleteSObject(row);
			}
		}
		
	}
	
	public e_SysTable__c getByName(String tableName)
	{
        clearAll();

		String whereCond = 'name = \'' + clean(tableName) + '\'';
		List<e_SysTable__c> tableList = (List<e_SysTable__c>)getSObjectListByWhere(getFieldStr(), NAME, whereCond);
		
		if (tableList != null && tableList.size() > 0)
		{
			return tableList[0];
		}
		
		return null;
	}
	
	public e_SysTable__c getById(String idInp)
    {
    	return (e_SysTable__c)getSObjectById(getFieldStr(), NAME, idInp);
    }

    //-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---  
    //-----------------------------------------------------------------------
    public static testMethod void test()
    {
        e_SysTableDao.getFieldStr();

        e_SysTableDao obj = new e_SysTableDao();

        obj.clearAll();
        obj.saveFields('xxx', 'rrr');
        obj.getByName('xxx');
        obj.getById('xxx');

        System.assertEquals(1, 1);
    }
}