public with sharing class phy_NewsController {

    public List<News__c> newsItems {get; set;}
    private Integer displaySize = 5;
    
    public List<NewsWrapper> newsWrapper {get; set;}
    
    public class NewsWrapper {
    	
    	public News__c news {get; set;}
    	public String style {get; set;}
    	
    }
    

    public phy_NewsController()
    {
    
    	newsWrapper = new List<NewsWrapper>();
    	newsItems = new List<News__c>();
    	loadSyndications();
   
    }
    
    
    public void loadSyndications()
    {
        try 
        {
             newsItems
            = [select Id,News_Date__c,Body__c,Published__c from News__c where Published__c =: true  order by News_Date__c DESC limit 5];

            Integer i = 1;
            for(News__c n : newsItems)
            {
                NewsWrapper nw = new NewsWrapper();
                nw.news = n;
                if(i == displaySize)
                {
                	nw.style = 'border-bottom: none;';
                }
                newsWrapper.add(nw);
                i++;
            
            }

        }
        catch(Exception e) { system.debug('phy_News: ' + e); }
    }
    
    public static testMethod void testSyndications()
    {
        Test.startTest();
        phy_NewsController phy_Synd = new  phy_NewsController();
        phy_Synd.loadSyndications();
        Test.stopTest();
    }
}