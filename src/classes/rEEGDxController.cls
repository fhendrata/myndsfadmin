//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: rEEGDxController
// PURPOSE: Controller class for the dx selection page
// CREATED: 11/19/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class rEEGDxController extends BaseWizardController
{   
	public boolean isSubmitVerified { get; set; }
    
    private List<rEEGDxDisp> dxs;
    private List<rEEGDxDisp> dxObjs;
    
    private List<rEEGDxGroupDisp> dxGroups;
    private List<rEEGDxGroupDisp> dxGroupsL;
    private List<rEEGDxGroupDisp> dxGroupsR;

	public PageReference setup()
	{
		this.patId = ApexPages.currentPage().getParameters().get('pid');
		this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.qsId = ApexPages.currentPage().getParameters().get('qsid');
        string newPatStr = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        
        rDao = new rEEGDao();
        
        isSubmitVerified = false;
        isiPad = false;
        
		return null;
	}

    public void loadParams()
    {
        this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
    }

    public PageReference save()
    {
    	
    	//jswenski 6/9/2011 - need to get this when we're post QS
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
    	
    	
       	rEEGDao reegDao = new rEEGDao();
        r_reeg__c reeg = reegDao.getById(reegId); 
        
        
        
        if (reeg != null)
        {
	        reeg.stat_dx_gen__c = saveDx();
	        reegDao.Save(reeg, '');
        }
       	return getDestPage();
    }
    
    public PageReference cancel()
    {
        return getDestPage();
    } 

    private PageReference getDestPage()
    {
    	//jswenski 6/10/2011 - also need it here if we cancel without saving
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
        PageReference returnVal = new PageReference( '/' + reegId);
        returnVal.setRedirect(true);
        return returnVal;
    }

    //------- wizard action buttons -------------------
    public PageReference reegStep3()
    {
        PageReference returnVal = new PageReference('/apex/patientOutcomeEditPage?rid=' + reegId
                                                        + '&pid=' + patId
                                                         + '&id=' + outcomeId
                                                        + '&qsid=' + qsId
                                                        + '&np=' + isNewPat
                                                        + '&wiz=true');
        if (isWR != null && isWR)
        		returnVal.getParameters().put('wr', 'true');
        returnVal.setRedirect(true);
        return returnVal;
    } 
    
    //jdepetro - this was used when we had an extra page after this one that allowed the selection of multiple reports.
    public PageReference nextAction()
    {
    	PageReference returnVal = null;
    	if (validateFields())
    	{
    		if (reeg == null && !e_StringUtil.isNullOrEmpty(reegId))
    			reeg = rDao.getById(reegId); 
    		
    		if (reeg != null)
    		{
    			reeg.req_stat__c = 'In Progress';
	        	reeg.eeg_rec_stat__c = 'In Progress';
	        	reeg.stat_dx_gen__c = saveDx();
	        	rDao.saveSObject(reeg);
	        	
	        	updateVars();
    		}
        
	        returnVal = new PageReference('/apex/ReportTypeSelect');
	        returnVal.getParameters().put('isNew', string.valueOf(isNew));
	        
	        if (patient != null) returnVal.getParameters().put('pid', patient.id);
	        if (!e_StringUtil.isNullOrEmpty(patId)) returnVal.getParameters().put('pid', patId);
	        if (reeg != null) returnVal.getParameters().put('rid', reeg.id);
	        if (!e_StringUtil.isNullOrEmpty(reegId)) returnVal.getParameters().put('rid', reegId);
	        
	        returnVal.getParameters().put('wiz', string.valueOf(isWiz));
	        returnVal.getParameters().put('np', string.valueOf(isNewPat));
	        returnVal.getParameters().put('oid', outcomeId);
	        returnVal.setRedirect(true);
    	}
        
        return returnVal;
    }

    public PageReference wizardSubmit()
    {
    	PageReference returnVal = null;
    	
        if (!e_StringUtil.isNullOrEmpty(reegId) && isSubmitVerified)
        {
        	if (reeg == null)
    			reeg = rDao.getById(reegId); 
    		
    		Boolean isPI = false;
    		Boolean isPO2 = false;
    		if ((!e_StringUtil.isNullOrEmpty(reeg.r_physician_id__c)) && reeg.r_physician_id__r.Account != null)
    		{
    			isPI = (reeg.r_physician_id__r.Account.PEER_Interactive__c != null &&  reeg.r_physician_id__r.Account.PEER_Interactive__c);
    			isPO2 = (!e_StringUtil.isNullOrEmpty(reeg.r_physician_id__r.Account.Report_Build_Type__c) &&  reeg.r_physician_id__r.Account.Report_Build_Type__c == 'PEER Online 2.0');
    		}
        	
	        reeg.req_stat__c = 'In Progress';
	        reeg.eeg_rec_stat__c = 'In Progress';
	        reeg.is_ng_only__c = (isPI || isPO2);
	        reeg.stat_dx_gen__c = saveDx();
	        
        	if (!e_StringUtil.isNullOrEmpty(qsid) && !qsid.equalsIgnoreCase('null'))
        	{
        		submitQSAction();
        		rDao.Save(reeg, '');
        	}
        	else
        	{
        		rDao.Save(reeg, 'UPDATE_DB_STATUS');
        	}
	
	       	updateVars();  
	        
	        returnVal = new PageReference('/apex/rEEGViewPage?id=' + reeg.Id); 
        }

        return returnVal;
    }
    
    public PageReference cancelAction()
    {
    	return cancelWizAction();
    }
    
    private boolean validateFields()
    {
    	boolean returnVal = false;
    	
    	return true;
    }
    
    private void submitQSAction()
    {
		QsDao qDao = new QsDao();
	    qDao.setReegId(qsid, reegId);   

	    ActionDao.insertReegAction(reegId, 'QUICKSTART_PROCESS');
    }
    
    private void updateVars()
    {
		List<r_reeg__c> reegList = [select id, r_patient_id__c, stat_is_corr__c, reeg_type__c from r_reeg__c where id = :reegId];
	      	
        if (reegList != null && reegList.size() > 0)
	    {
		    if (reegList[0].reeg_type__c == 'Type II')
		    {
		        //LoadPastTestVars();
		    }
	    }   
    }
    
    private void LoadPastTestVars()
    {
        if (!e_StringUtil.isNullOrEmpty(patId))
        {
        	ActionDao aDao = new ActionDao();
        	
            for(r_reeg__c reegRow : [select id, stat_is_corr__c from r_reeg__c where r_patient_id__c=:patId])
            {
               if (reegRow.id != reegId && reegRow.stat_is_corr__c)
               {
               		//-- jdepetro 3-23.11 - this is causing re-parcing, and rebuilding of the Type 1 reports
               		//-- associated with this Type 2 rEEG
               		//aDao.InsertQSAction(reegRow.id);
               }
            }      
        }
    }

    //-----------------------------------------------
    
    public List<rEEGDxDisp> getDxs()
    {
        if (dxs== null)
        {
            loadFullList();
            loadSavedList();                
        }    
        return dxs;
    }
    
    public List<rEEGDxGroupDisp> getDxGroupsL()
    {
        getDxGroups();
        return dxGroupsL;
    }
    public List<rEEGDxGroupDisp> getDxGroupsR()
    {
        getDxGroups();
        return dxGroupsR;
    }
    
    public List<rEEGDxGroupDisp> getDxGroups()
    {
        if (dxGroups== null)
        {
            getDxs();
            loadGroupList();
        }    
        return dxGroups;
    }
    
    private void loadFullList()
    {  
        dxs = new List<rEEGDxDisp>();
    
        for(ref_dx__c refRow : 
                 [select description__c, is_group__c, primary__c, rule_out__c, secondary__c, sequence__c, id, name 
                     from ref_dx__c
                     order by sequence__c])
        {
            rEEGDxDisp dx = new rEEGDxDisp();
            dx.SetupRef( refRow );     
            dx.Setup(new r_reeg_dx__c());                                        
            dxs.Add( dx);
        } 
    }
    
   private void loadSavedList()
    {
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
	        for(r_reeg_dx__c dxRow : 
	             [select dx_name__c, primary__c, r_ref_dx_id__c, rule_out__c, secondary__c, id, name , Reference_Dx__r.PEER_Drug_mapping__c
	                 from r_reeg_dx__c
	                 where r_reeg_id__c = :reegId limit 5])
	        {
	            AddSavedToList(dxRow);
	        }
    	} 
    }
    
    private void AddSavedToList(r_reeg_dx__c o)
    {
        for(rEEGDxDisp dxRow : dxs)
        {
            if (dxRow.getRefObj() != null && dxRow.getRefObj().id == o.r_ref_dx_id__c)
            {
                dxRow.Setup(o);
            }
        }
    }
    
    private void loadGroupList()
    {
        dxGroups = new List<rEEGDxGroupDisp>();
        
        rEEGDxGroupDisp currGroup = null;
        Integer lastGroupId = 0;
        Integer totalItemCount = 0;
                
        for(rEEGDxDisp dxRow : dxs)
        {
            if (dxRow.getRefObj().is_group__c)
            {
                currGroup = new rEEGDxGroupDisp();
                currGroup.Setup( dxRow.getObj());
                currGroup.SetupRef( dxRow.getRefObj());
                
                dxGroups.Add(currGroup);
            }
            else
            {
                if (currGroup != null) 
                {
                    totalItemCount++;
                    currGroup.addItem( dxRow);
                }
            }
        }
               
        dxGroupsL = new List<rEEGDxGroupDisp>();
        dxGroupsR = new List<rEEGDxGroupDisp>();
 
        Integer addedItemCount = 0;
        
        for(rEEGDxGroupDisp groupRow : dxGroups)
        {
            if (addedItemCount > (totalItemCount/2))
            {
                dxGroupsR.Add(groupRow);    
            }
            else
            {
                dxGroupsL.Add(groupRow);
                if (groupRow != null && groupRow.getItemCount() != null)
                	addedItemCount += groupRow.getItemCount();
                else
                	addedItemCount = (addedItemCount == null) ? 0 : addedItemCount;
            }
        }       
    }  
    
    public Boolean saveDx()
    {          
        saveDxGroup( dxGroupsL );
        saveDxGroup( dxGroupsR );

        return true;
    } 

    private void SaveReeg(r_reeg__c reeg)
    {
        if (reeg.id != null) upsert reeg;
        else insert reeg;
    }
    
    private void saveDxGroup(List<rEEGDxGroupDisp> groupList)
    {
        if (groupList!= null)
        {
            for(rEEGDxGroupDisp grpRow : groupList)
            {
                for(rEEGDxDisp dxRow : grpRow.getDxs())
                {   
                    if (dxRow == null)
                    {
                        addMessage( 'DxRow is null');         
                    }
                    else if (dxRow.getObj() == null)
                    {
                        addMessage( 'DxRow.getObj() is null');         
                    }
                    else
                    {
                        SaveDx(dxRow.getObj(), dxRow.getRefObj());
                    }
                }
            }
        }
    }
    
    public void SaveDx( r_reeg_dx__c obj, ref_dx__c refObj)
    {
        Boolean hasValue = (obj.primary__c || obj.secondary__c || obj.rule_out__c);

        if (hasValue)
        {
        	obj.OwnerId = UserInfo.getUserId();
             if (obj.id != null)
             {
                upsert obj;
             }
             else
             {
                 obj.r_reeg_id__c = reegId;
                 obj.r_ref_dx_id__c = refObj.id; 
                 obj.dx_name__c = refObj.description__c; 
                 obj.name = refObj.name; 

                 insert obj;
             }                
        }
        else
        {
            if (obj.id != null) delete obj;
        }    
    }
    
    private void addMessage(String value)
    {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.INFO, value);
        ApexPages.addMessage( msg);
    }

          //---TEST METHODS ------------------------------
    public static testMethod void testDxController()
    {
    	rEEGDxController cont = new rEEGDxController();
		cont.IsTestCase = true;
    	r_reeg__c reeg = rEEGDao.getTestReeg();
        cont.reeg = reeg;
        patientDao patDao = new patientDao();
        patDao.IsTestCase = true;
        
        cont.patient = patDao.getById(reeg.r_patient_id__c);
        
        patientOutcomeDao patOutcomeDao = new patientOutcomeDao();
        r_patient_outc__c outcome = patOutcomeDao.getTestOutcome(reeg.r_patient_id__c);
        patientOutcomeMedDao medDao = new patientOutcomeMedDao();
        medDao.IsTestCase = true;
        r_outcome_med__c outMed =  medDao.getTestOutcomeMed(outcome.Id);    
        
 		cont.outcomeId = ApexPages.currentPage().getParameters().put('oid', outcome.Id);
 		cont.patId = ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
 		ApexPages.currentPage().getParameters().put('wiz', 'true');
 		ApexPages.currentPage().getParameters().put('isNew', 'true');
		ApexPages.currentPage().getParameters().put('np', 'true');
		ApexPages.currentPage().getParameters().put('isNew', 'true');
    	
    	PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('id',reeg.id);
    	
    	rEEGDxDao dxDao = new rEEGDxDao();
    	dxDao.IsTestCase = true;
    	r_reeg_dx__c dx = dxDao.getTestDx();
    	dx = dxDao.getTestDx();
		cont.setup();
		
		cont.SaveReeg(reeg);
		cont.reegId = reeg.Id;
        PageReference pRef1 = cont.save();
        PageReference pRef2 = cont.cancel();
        PageReference pRef3 = cont.getDestPage();
        PageReference pRef4 = cont.reegStep3();
        
        cont.nextAction();
        
        cont.isSubmitVerified = true;
        cont.reegId = reeg.Id;
        reeg.reeg_type__c = 'Type II';
        rEEGDao rDao = new rEEGDao();
        rDao.saveSObject(reeg);
        
        cont.wizardSubmit();
        
        cont.submitQSAction();
        
        cont.cancelAction();
     //   PageReference pRef6 = cont.cancelWizard();
   
        List<rEEGDxDisp> dxList = cont.getDxs();
        List<rEEGDxGroupDisp> DxGroupDisp = cont.getDxGroupsL();
        DxGroupDisp = cont.getDxGroupsR();
        DxGroupDisp = cont.getDxGroups();
        

   //     cont.saveDx();

     	cont.RemoveReeg(null);

        System.assertEquals( 1, 1);
    }

}