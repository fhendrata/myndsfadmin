public with sharing class ReqOutcomeMedDisp 
{
    public Req_Outcome_Medication__c Obj { get; set; }
    public Date WashedOutDate { get; set; }
    public Boolean WashoutErr { get; set; }
    public Boolean IsFoundInDatabase { get; set; }

	public ReqOutcomeMedDisp() { }

    public ReqOutcomeMedDisp(Req_Outcome_Medication__c o)
    {
        Obj = o;
        WashoutErr = false;
        IsFoundInDatabase = true;
    }

    public ReqOutcomeMedDisp(Req_Outcome_Medication__c o, Id prevMedId)
    {
        Obj = o;
        Obj.previous_med__c = prevMedId;
        WashoutErr = false;
        IsFoundInDatabase = true;
    }

    public ReqOutcomeMedDisp(Req_Outcome_Medication__c o, String medName, Decimal dosage, String unit, String freq, Date startDt, Date endDt, Id prevMedId, Boolean startDtIsEst)
    {
        Obj = o;
        Obj.med_name__c = medName;
        Obj.dosage__c = dosage;
        Obj.unit__c = unit;
        Obj.frequency__c = freq;
        Obj.start_date__c = startDt;
        Obj.end_date__c = endDt;
        Obj.previous_med__c = prevMedId;
        Obj.start_date_is_estimated__c = startDtIsEst;   
        WashoutErr = false;
        IsFoundInDatabase = true;
    }

    public void setDosageDisplay()
    {
        // obj.Unknown__c = (obj.dosage__c != null && obj.Unit__c != null) ? obj.dosage__c + ' ' + obj.Unit__c : 'unknown';
    }
    
    public void Setup(Req_Outcome_Medication__c o)
    {
        Obj = o;
    }
    
    //--- To hold previous outcome medication id (change dose/discontinue)
    private string previousId;
    public string getPreviousId()
    {
        return previousId;
    }
    public void setupPreviousId(string id) {
    	previousId = id;
    }
    
    //--- To hold previous outcome medication id (change dose/discontinue)
    private boolean displayActions;
    public boolean getIsDisplayActions()
    {
        return displayActions;
    }
    public void setDisplayAction(boolean value) {
    	displayActions = value;
    }

    // ---Row number of the display element
    private Integer rowNum;
    public Integer getRowNum()
    {
        return rowNum;
    }    
    public void setRowNum(Integer value)
    {
        rowNum = value;
    }
    
    public String StartDate  
    {     	
    	get	{return e_StringUtil.getDateString(Obj.start_date__c);}    	 
    	set	{Obj.start_date__c = e_StringUtil.setDateFromString(value);} 
    }
    
    public String TempEndDate { get; set { this.TempEndDate = value; } }
    public String EndDate  
    {     	
    	get	{
    		if (Obj != null && Obj.end_date__c != null && (String.isBlank(TempEndDate) || TempEndDate.contains(':')))
                return getDateTimeString(Obj.end_date__c, 'MM/dd/yyyy hh:mm');
            else if (Obj != null && Obj.end_date__c != null && String.isNotBlank(TempEndDate) && !TempEndDate.contains(':'))
                return TempEndDate;
    		else
                return '';
    	}    	 
    	set	{Obj.end_date__c = e_StringUtil.setDateFromString(value);} 
    }

    public boolean isEmpty()
    {
        return (Obj != null && String.isBlank(Obj.med_name__c));
    }

    // An example of the format is --> yyyy-MM-dd hh:mm:ss
    public static String getDateTimeString(DateTime inputVal, String format)
    {
        if (inputVal == null) return '';
        if (String.isBlank(format)) return inputVal.format();
        else
            return inputVal.addDays(1).addSeconds(-1).format(format);
    }
    
}