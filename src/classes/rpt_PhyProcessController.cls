//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: rpt_PhyProcessController
// PURPOSE: Used for building records for the Phy statistics report.
// CREATED: 09/17/10 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class rpt_PhyProcessController extends BaseController
{
	List<rpt_physician__c> phyRptInsertList {get; set;}
	
	public rpt_PhyProcessController()
	{
		IsTestCase = false;
	}
	
	public PageReference delAllRpt(DateTime dt)
    {
     	rpt_PhyDao phyDao = new rpt_PhyDao();
     	phyDao.IsTestCase = IsTestCase;
     	
		List<rpt_physician__c> phyList = phyDao.getByMonthYear(dt.month(), dt.year());
     	
     	phyDao.realDeleteSObjectList(phyList);
        
        return null;
    }
	
	public void buildRpt(Sobject conObj, DateTime inputDt)
	{
		if (inputDt != null && conObj != null)
		{
			rpt_PhyDao phyDao = new rpt_PhyDao();
			Contact con = (Contact)conObj;
			phyDao.saveSObject(getBlankPhyRpt(con, inputDt));
		}
	}

	public void processRpts(DateTime inputDt)
	{
		rpt_PhyDao phyDao = new rpt_PhyDao();
		phyDao.IsTestCase = IsTestCase;
		
		phyRptInsertList = new List<rpt_physician__c>();
		phyRptInsertList = processReegList(inputDt);
		if (phyRptInsertList.size() > 0)
			phyDao.saveSObjectList(phyRptInsertList);

		phyRptInsertList = new List<rpt_physician__c>();
		phyRptInsertList = processPatList(inputDt);
		if (phyRptInsertList.size() > 0)
			phyDao.saveSObjectList(phyRptInsertList);
	}

	private List<rpt_physician__c> processPatList(DateTime inputDt)
	{
		List<rpt_physician__c> phyRptList = new List<rpt_physician__c>();	
		
		if (inputDt != null)
		{
			rpt_physician__c phyRpt;
			patientDao patDao = new patientDao();
			patDao.IsTestCase = IsTestCase;
			List<r_patient__c> patList = patDao.getByMonthYear(inputDt.month(), inputDt.year());

			if (patList != null && patList.size() > 0)
			{		
				for (r_patient__c row : patList)
				{
					integer index = -1;
					boolean found = false;
					if (phyRptList != null && phyRptList.size() > 0)
					{
						index = 0;
						for(rpt_physician__c pr : phyRptList)
						{
							if (!e_StringUtil.isNullOrEmpty(pr.physician__c) && !e_StringUtil.isNullOrEmpty(row.physician__c))
							{
								if (pr.physician__c == row.physician__c)
								{
									phyRpt = pr;
									found = true;
									break;
								}
							}
							index++;
						}
					}
					
					if (!found)
					{
						rpt_PhyDao phyDao = new rpt_PhyDao();
						phyDao.IsTestCase = IsTestCase;
						phyRpt = phyDao.getByIdMonthYear(row.physician__c, inputDt.month(), inputDt.year());
						zeroPatFieldVals(phyRpt);
					}
					else
					{
						phyRptList.remove(index);
					}
					
					if (phyRpt != null)
					{
						if (!e_StringUtil.isNullOrEmpty(row.Marketing_Source__c))
						{
							if (row.Marketing_Source__c == 'DTC - Broadcast Television') 
								phyRpt.pat_ms_dtc_broadcast_tv__c = getVal(phyRpt.pat_ms_dtc_broadcast_tv__c);
							if (row.Marketing_Source__c == 'DTC - Cable Television') 
								phyRpt.pat_ms_dtc_cable_tv__c = getVal(phyRpt.pat_ms_dtc_cable_tv__c);
							if (row.Marketing_Source__c == 'DTC - Company Website') 
								phyRpt.pat_ms_dtc_company_website__c = getVal(phyRpt.pat_ms_dtc_company_website__c);
							if (row.Marketing_Source__c == 'DTC - Flyer') 
								phyRpt.pat_ms_dtc_flyer__c = getVal(phyRpt.pat_ms_dtc_flyer__c);
							if (row.Marketing_Source__c == 'DTC - Yoddle') 
								phyRpt.pat_ms_dtc_yoddle__c = getVal(phyRpt.pat_ms_dtc_yoddle__c);
							if (row.Marketing_Source__c == 'DTC - Reach Local') 
								phyRpt.pat_ms_dtc_reach_local__c = getVal(phyRpt.pat_ms_dtc_reach_local__c);
							if (row.Marketing_Source__c == 'DTC - Radio') 
								phyRpt.pat_ms_dtc_radio__c = getVal(phyRpt.pat_ms_dtc_radio__c);
							if (row.Marketing_Source__c == 'DTC - Newspaper') 
								phyRpt.pat_ms_dtc_newspaper__c = getVal(phyRpt.pat_ms_dtc_newspaper__c);
						}
						else
						{
							phyRpt.pat_ms_none__c = getVal(phyRpt.pat_ms_none__c);
						}
						
						if (!e_StringUtil.isNullOrEmpty(row.Reason_for_Not_Scheduling_other__c))
						{
							if (row.Reason_for_Not_Scheduling_other__c == 'Can\'t afford fees') 
								phyRpt.pat_nsa_cant_afford_fees__c = getVal(phyRpt.pat_nsa_cant_afford_fees__c);
							if (row.Reason_for_Not_Scheduling_other__c == 'Inconvenient Location') 
								phyRpt.pat_nsa_inconv_location__c = getVal(phyRpt.pat_nsa_inconv_location__c);
							if (row.Reason_for_Not_Scheduling_other__c == 'Inconvenient Time') 
								phyRpt.pat_nsa_inconv_time__c = getVal(phyRpt.pat_nsa_inconv_time__c);
							if (row.Reason_for_Not_Scheduling_other__c == 'Does not accept insurance') 
								phyRpt.pat_nsa_not_accept_ins__c = getVal(phyRpt.pat_nsa_not_accept_ins__c);
							if (row.Reason_for_Not_Scheduling_other__c == 'Service not offered') 
								phyRpt.pat_nsa_service_not_offered__c = getVal(phyRpt.pat_nsa_service_not_offered__c);
						}
						else
						{
							phyRpt.pat_nsa_none__c = getVal(phyRpt.pat_nsa_none__c);
						}
						
						if (!e_StringUtil.isNullOrEmpty(row.rEEG_Guided_Rx__c))
						{
							if (row.rEEG_Guided_Rx__c == 'No') 
								phyRpt.pat_rgx_no__c = getVal(phyRpt.pat_rgx_no__c);
							if (row.rEEG_Guided_Rx__c == 'Yes') 
								phyRpt.pat_rgx_yes__c = getVal(phyRpt.pat_rgx_yes__c);
						}
						else
						{
							phyRpt.pat_rgx_none__c = getVal(phyRpt.pat_rgx_none__c);
						}
						
						if (!e_StringUtil.isNullOrEmpty(row.rEEG_Guided_Rx_Reason__c))
						{
							if (row.rEEG_Guided_Rx_Reason__c == 'Reason 1') 
								phyRpt.pat_rgxr_reason_1__c = getVal(phyRpt.pat_rgxr_reason_1__c);
							if (row.rEEG_Guided_Rx_Reason__c == 'Reason 2') 
								phyRpt.pat_rgxr_reason_2__c = getVal(phyRpt.pat_rgxr_reason_2__c);
							if (row.rEEG_Guided_Rx_Reason__c == 'Reason 3') 
								phyRpt.pat_rgxr_reason_3__c = getVal(phyRpt.pat_rgxr_reason_3__c);
							if (row.rEEG_Guided_Rx_Reason__c == 'Reason 4') 
								phyRpt.pat_rgxr_reason_4__c = getVal(phyRpt.pat_rgxr_reason_4__c);
						}
						else
						{
							phyRpt.pat_rgxr_none__c = getVal(phyRpt.pat_rgxr_none__c);
						}
						
						if (!e_StringUtil.isNullOrEmpty(row.rEEG_Helpfulness_AM__c))
						{
							if (row.rEEG_Helpfulness_AM__c.startsWith('1')) 
								phyRpt.pat_rhelp_am_1_essential__c = getVal(phyRpt.pat_rhelp_am_1_essential__c);
							if (row.rEEG_Helpfulness_AM__c.startsWith('2'))
								phyRpt.pat_rhelp_am_2_moderately_helpfu__c = getVal(phyRpt.pat_rhelp_am_2_moderately_helpfu__c);
							if (row.rEEG_Helpfulness_AM__c.startsWith('3'))
								phyRpt.pat_rhelp_am_3_minimally_helpful__c = getVal(phyRpt.pat_rhelp_am_3_minimally_helpful__c);
							if (row.rEEG_Helpfulness_AM__c.startsWith('4')) 
								phyRpt.pat_rhelp_am_4_not_helpful__c = getVal(phyRpt.pat_rhelp_am_4_not_helpful__c);
							if (row.rEEG_Helpfulness_AM__c.startsWith('5')) 
								phyRpt.pat_rhelp_am_5_minimally_harmful__c = getVal(phyRpt.pat_rhelp_am_5_minimally_harmful__c);
							if (row.rEEG_Helpfulness_AM__c.startsWith('6')) 
								phyRpt.pat_rhelp_am_6_somewhat_harmful__c = getVal(phyRpt.pat_rhelp_am_6_somewhat_harmful__c);
							if (row.rEEG_Helpfulness_AM__c.startsWith('7')) 
								phyRpt.pat_rhelp_am_7_very_harmful__c = getVal(phyRpt.pat_rhelp_am_7_very_harmful__c);
						}
						else
						{
							phyRpt.pat_rhelp_am_none__c = getVal(phyRpt.pat_rhelp_am_none__c);
						}
						
						phyRptList.add(phyRpt);	
						phyRpt = null;
					}
				}
			}
		}
		
		return phyRptList;
	}
	
	private List<rpt_physician__c> processReegList(DateTime inputDt)
	{
		List<rpt_physician__c> phyRptList = new List<rpt_physician__c>();	
		
		if (inputDt != null)
		{
			rpt_physician__c phyRpt;
			rEEGDao rDao = new rEEGDao();
			rDao.IsTestCase = IsTestCase;
			List<r_reeg__c> reegList = rDao.getByMonthYear(inputDt.month(), inputDt.year());

			if (reegList != null && reegList.size() > 0)
			{
				for (r_reeg__c row : reegList)
				{
					integer index = -1;
					boolean found = false;
					if (phyRptList != null && phyRptList.size() > 0)
					{
						index = 0;
						for(rpt_physician__c pr : phyRptList)
						{
							if (pr.physician__c == row.r_physician_id__c)
							{
								phyRpt = pr;
								found = true;
								break;
							}
							
							index++;
						}
					}
					
					if (!found)
					{
						rpt_PhyDao phyDao = new rpt_PhyDao();
						phyDao.IsTestCase = IsTestCase;
						phyRpt = phyDao.getByIdMonthYear(row.r_physician_id__c, inputDt.month(), inputDt.year());
						zeroReegFieldVals(phyRpt);
					}
					else
					{
						phyRptList.remove(index);
					}
					
					if (phyRpt != null)
					{
						if (!e_StringUtil.isNullOrEmpty(row.billing_code__c))
						{
							if (row.billing_code__c == 'Compassionate Use - No Charge') 
								phyRpt.bc_compassionate_use_nc__c = getVal(phyRpt.bc_compassionate_use_nc__c);
							if (row.billing_code__c == 'Database Test – Charge')
								phyRpt.bc_database_test_c__c = getVal(phyRpt.bc_database_test_c__c);
							if (row.billing_code__c == 'Database Test - No Charge')
								phyRpt.bc_database_test_nc__c = getVal(phyRpt.bc_database_test_nc__c);
							if (row.billing_code__c == 'Exception - No Charge') 
								phyRpt.bc_exception_nc__c = getVal(phyRpt.bc_exception_nc__c);
							if (row.billing_code__c == 'Other – Charge') 
								phyRpt.bc_other_charge__c = getVal(phyRpt.bc_other_charge__c);
							if (row.billing_code__c == 'Other – No Charge') 
								phyRpt.bc_other_nc__c = getVal(phyRpt.bc_other_nc__c);
							if (row.billing_code__c == 'Outcome Test - Charge') 
								phyRpt.bc_outcome_test_c__c = getVal(phyRpt.bc_outcome_test_c__c);
							if (row.billing_code__c == 'Sample Test – No Charge') 
								phyRpt.bc_sample_test_nc__c = getVal(phyRpt.bc_sample_test_nc__c);
							if (row.billing_code__c == 'Standard - Charge')
								phyRpt.bc_standard_c__c = getVal(phyRpt.bc_standard_c__c);
							if (row.billing_code__c == 'Training - No Charge')
								phyRpt.bc_training_nc__c = getVal(phyRpt.bc_training_nc__c);
							if (row.billing_code__c == 'TRD Study – No Charge') 
								phyRpt.bc_trd_study_nc__c = getVal(phyRpt.bc_trd_study_nc__c);
						}
						else
						{
							phyRpt.bc_none__c = getVal(phyRpt.bc_none__c);
						}	
						
						if (!e_StringUtil.isNullOrEmpty(row.billing_other__c))
						{
							phyRpt.b_other__c = getVal(phyRpt.b_other__c);
						}
						else
						{
							phyRpt.b_none__c = getVal(phyRpt.b_none__c);
						}
						
						//-- reeg billing status
						if (!e_StringUtil.isNullOrEmpty(row.billed_status__c))
						{
							if (row.billed_status__c == 'Billed') 
								phyRpt.bs_billed__c = getVal(phyRpt.bs_billed__c);
							if (row.billed_status__c == 'Paid')
								phyRpt.bs_paid__c = getVal(phyRpt.bs_paid__c);
							if (row.billed_status__c == 'Rebilled')
								phyRpt.bs_rebilled__c = getVal(phyRpt.bs_rebilled__c);
						}
						else
						{
							phyRpt.bs_none__c = getVal(phyRpt.bs_none__c);
						}	
						
						//-- reeg source
						if (!e_StringUtil.isNullOrEmpty(row.marketing_source__c))
						{
							if (row.marketing_source__c == 'DTC Advertising') 
								phyRpt.src_dtc_advertising__c = getVal(phyRpt.src_dtc_advertising__c);
							if (row.marketing_source__c == 'Office Generated')
								phyRpt.src_office_generated__c = getVal(phyRpt.src_office_generated__c);
						}
						else
						{
							phyRpt.src_none__c = getVal(phyRpt.src_none__c);
						}	
						
						//-- reeg type
						if (!e_StringUtil.isNullOrEmpty(row.reeg_type__c))
						{
							if (row.reeg_type__c == 'Type I')
							{
								if (!e_StringUtil.isNullOrEmpty(row.reeg_type_mod__c))
								{
									if (row.reeg_type_mod__c == '(m)') 
										phyRpt.type_I_m_rEEG_s__c = getVal(phyRpt.type_I_m_rEEG_s__c);
									if (row.reeg_type_mod__c == '(u)')
										phyRpt.type_I_u_rEEG_s__c = getVal(phyRpt.type_I_u_rEEG_s__c);
								}
								else
								{
									phyRpt.type_I_rEEG_s__c = getVal(phyRpt.type_I_rEEG_s__c);
								}
							}
							else
							{
								phyRpt.type_II__c = getVal(phyRpt.type_II__c);
							}
						}
						
						//-- reeg status
						if (!e_StringUtil.isNullOrEmpty(row.req_stat__c))
						{
							if (row.req_stat__c == 'Complete') 
								phyRpt.s_complete__c = getVal(phyRpt.s_complete__c);
							if (row.req_stat__c == 'In Progress')
								phyRpt.s_in_progress__c = getVal(phyRpt.s_in_progress__c);
							if (row.req_stat__c == 'Missing Value')
								phyRpt.s_missing_value__c = getVal(phyRpt.s_missing_value__c);
							if (row.req_stat__c == 'New') 
								phyRpt.s_new__c = getVal(phyRpt.s_new__c);
							if (row.req_stat__c == 'Not Valid - CNSR stopped process') 
								phyRpt.s_not_valid_cnsr_stopped_process__c = getVal(phyRpt.s_not_valid_cnsr_stopped_process__c);
							if (row.req_stat__c == 'Not Valid - Corr Calc errors') 
								phyRpt.s_not_valid_corr_calc_errors__c = getVal(phyRpt.s_not_valid_corr_calc_errors__c);
							if (row.req_stat__c == 'Not Valid - EEG errors') 
								phyRpt.s_not_valid_eeg_errors__c = getVal(phyRpt.s_not_valid_eeg_errors__c);
							if (row.req_stat__c == 'Not Valid - Patient stopped process') 
								phyRpt.s_not_valid_pat_stopped_process__c = getVal(phyRpt.s_not_valid_pat_stopped_process__c);
							if (row.req_stat__c == 'Not Valid - Physician stopped process')
								phyRpt.s_not_valid_phy_stopped_process__c = getVal(phyRpt.s_not_valid_phy_stopped_process__c);
							if (row.req_stat__c == 'On Hold - CNSR')
								phyRpt.s_on_hold_cnsr__c = getVal(phyRpt.s_on_hold_cnsr__c);
							if (row.req_stat__c == 'On Hold - CNSR review') 
								phyRpt.s_on_hold_cnsr_review__c = getVal(phyRpt.s_on_hold_cnsr_review__c);
							if (row.req_stat__c == 'On Hold - EEG scheduled') 
								phyRpt.s_on_hold_eeg_scheduled__c = getVal(phyRpt.s_on_hold_eeg_scheduled__c);
							if (row.req_stat__c == 'On Hold - Washout in progress')
								phyRpt.s_on_hold_washout_in_progress__c = getVal(phyRpt.s_on_hold_washout_in_progress__c);
							if (row.req_stat__c == 'Pending')
								phyRpt.s_pending__c = getVal(phyRpt.s_pending__c);
						}
						else
						{
							phyRpt.s_none__c = getVal(phyRpt.s_none__c);
						}
						
						phyRptList.add(phyRpt);	
						phyRpt = null;
					}
				}
			}
		}
		
		return phyRptList;
	}
	
	private void zeroPatFieldVals(rpt_physician__c phyRpt)
	{
		if (phyRpt != null){
		if (phyRpt.pat_ms_dtc_broadcast_tv__c != null && phyRpt.pat_ms_dtc_broadcast_tv__c > 0)
			phyRpt.pat_ms_dtc_broadcast_tv__c = 0;
		if (phyRpt.pat_ms_dtc_cable_tv__c != null && phyRpt.pat_ms_dtc_cable_tv__c > 0)
			phyRpt.pat_ms_dtc_cable_tv__c = 0;
		if (phyRpt.pat_ms_dtc_company_website__c != null && phyRpt.pat_ms_dtc_company_website__c > 0)
			phyRpt.pat_ms_dtc_company_website__c = 0;
		if (phyRpt.pat_ms_dtc_flyer__c != null && phyRpt.pat_ms_dtc_flyer__c > 0)
			phyRpt.pat_ms_dtc_flyer__c = 0;
		if (phyRpt.pat_ms_dtc_yoddle__c != null && phyRpt.pat_ms_dtc_yoddle__c > 0)
			phyRpt.pat_ms_dtc_yoddle__c = 0;
		if (phyRpt.pat_ms_dtc_reach_local__c != null && phyRpt.pat_ms_dtc_reach_local__c > 0)
			phyRpt.pat_ms_dtc_reach_local__c = 0;
		if (phyRpt.pat_ms_dtc_radio__c != null && phyRpt.pat_ms_dtc_radio__c > 0)
			phyRpt.pat_ms_dtc_radio__c = 0;
		if (phyRpt.pat_ms_dtc_newspaper__c != null && phyRpt.pat_ms_dtc_newspaper__c > 0)
			phyRpt.pat_ms_dtc_newspaper__c = 0;
		if (phyRpt.pat_ms_none__c != null && phyRpt.pat_ms_none__c > 0)
			phyRpt.pat_ms_none__c = 0;
		
		if (phyRpt.pat_nsa_cant_afford_fees__c != null && phyRpt.pat_nsa_cant_afford_fees__c > 0)
			phyRpt.pat_nsa_cant_afford_fees__c = 0;
		if (phyRpt.pat_nsa_inconv_location__c != null && phyRpt.pat_nsa_inconv_location__c > 0)
			phyRpt.pat_nsa_inconv_location__c = 0;
		if (phyRpt.pat_nsa_inconv_time__c != null && phyRpt.pat_nsa_inconv_time__c > 0)
			phyRpt.pat_nsa_inconv_time__c = 0;
		if (phyRpt.pat_nsa_not_accept_ins__c != null && phyRpt.pat_nsa_not_accept_ins__c > 0)
			phyRpt.pat_nsa_not_accept_ins__c = 0;
		if (phyRpt.pat_nsa_service_not_offered__c != null && phyRpt.pat_nsa_service_not_offered__c > 0)
			phyRpt.pat_nsa_service_not_offered__c = 0;
		if (phyRpt.pat_nsa_none__c != null && phyRpt.pat_nsa_none__c > 0)
			phyRpt.pat_nsa_none__c = 0;
			
		if (phyRpt.pat_rgx_no__c != null && phyRpt.pat_rgx_no__c > 0)
			phyRpt.pat_rgx_no__c = 0;
		if (phyRpt.pat_rgx_yes__c != null && phyRpt.pat_rgx_yes__c > 0)
			phyRpt.pat_rgx_yes__c = 0;
		if (phyRpt.pat_rgx_none__c != null && phyRpt.pat_rgx_none__c > 0)
			phyRpt.pat_rgx_none__c = 0;
		
		if (phyRpt.pat_rgxr_reason_1__c != null && phyRpt.pat_rgxr_reason_1__c > 0)
			phyRpt.pat_rgxr_reason_1__c = 0;
		if (phyRpt.pat_rgxr_reason_2__c != null && phyRpt.pat_rgxr_reason_2__c > 0)
			phyRpt.pat_rgxr_reason_2__c = 0;
		if (phyRpt.pat_rgxr_reason_3__c != null && phyRpt.pat_rgxr_reason_3__c > 0)
			phyRpt.pat_rgxr_reason_3__c = 0;
		if (phyRpt.pat_rgxr_reason_4__c != null && phyRpt.pat_rgxr_reason_4__c > 0)
			phyRpt.pat_rgxr_reason_4__c = 0;	
		if (phyRpt.pat_rgxr_none__c != null && phyRpt.pat_rgxr_none__c > 0)
			phyRpt.pat_rgxr_none__c = 0;	
			
		if (phyRpt.pat_rhelp_am_1_essential__c != null && phyRpt.pat_rhelp_am_1_essential__c > 0)
			phyRpt.pat_rhelp_am_1_essential__c = 0;
		if (phyRpt.pat_rhelp_am_2_moderately_helpfu__c != null && phyRpt.pat_rhelp_am_2_moderately_helpfu__c > 0)
			phyRpt.pat_rhelp_am_2_moderately_helpfu__c = 0;
		if (phyRpt.pat_rhelp_am_3_minimally_helpful__c != null && phyRpt.pat_rhelp_am_3_minimally_helpful__c > 0)
			phyRpt.pat_rhelp_am_3_minimally_helpful__c = 0;
		if (phyRpt.pat_rhelp_am_4_not_helpful__c != null && phyRpt.pat_rhelp_am_4_not_helpful__c > 0)
			phyRpt.pat_rhelp_am_4_not_helpful__c = 0;	
		if (phyRpt.pat_rhelp_am_5_minimally_harmful__c != null && phyRpt.pat_rhelp_am_5_minimally_harmful__c > 0)
			phyRpt.pat_rhelp_am_5_minimally_harmful__c = 0;			
		if (phyRpt.pat_rhelp_am_6_somewhat_harmful__c != null && phyRpt.pat_rhelp_am_6_somewhat_harmful__c > 0)
			phyRpt.pat_rhelp_am_6_somewhat_harmful__c = 0;
		if (phyRpt.pat_rhelp_am_7_very_harmful__c != null && phyRpt.pat_rhelp_am_7_very_harmful__c > 0)
			phyRpt.pat_rhelp_am_7_very_harmful__c = 0;	
		if (phyRpt.pat_rhelp_am_none__c != null && phyRpt.pat_rhelp_am_none__c > 0)
			phyRpt.pat_rhelp_am_none__c = 0;	
		}
	}
	
	private void zeroReegFieldVals(rpt_physician__c phyRpt)
	{
		if (phyRpt != null){
		if (phyRpt.bc_compassionate_use_nc__c != null && phyRpt.bc_compassionate_use_nc__c > 0)
			phyRpt.bc_compassionate_use_nc__c = 0;
		if (phyRpt.bc_database_test_c__c != null && phyRpt.bc_database_test_c__c > 0)
			phyRpt.bc_database_test_c__c = 0;
		if (phyRpt.bc_database_test_nc__c != null && phyRpt.bc_database_test_nc__c > 0)
			phyRpt.bc_database_test_nc__c = 0;
		if (phyRpt.bc_exception_nc__c != null && phyRpt.bc_exception_nc__c > 0)
			phyRpt.bc_exception_nc__c = 0;
		if (phyRpt.bc_other_charge__c != null && phyRpt.bc_other_charge__c > 0)
			phyRpt.bc_other_charge__c = 0;
		if (phyRpt.bc_other_nc__c != null && phyRpt.bc_other_nc__c > 0)
			phyRpt.bc_other_nc__c = 0;
		if (phyRpt.bc_outcome_test_c__c != null && phyRpt.bc_outcome_test_c__c > 0)
			phyRpt.bc_outcome_test_c__c = 0;
		if (phyRpt.bc_sample_test_nc__c != null && phyRpt.bc_sample_test_nc__c > 0)
			phyRpt.bc_sample_test_nc__c = 0;
		if (phyRpt.bc_standard_c__c != null && phyRpt.bc_standard_c__c > 0)
			phyRpt.bc_standard_c__c = 0;
		if (phyRpt.bc_training_nc__c != null && phyRpt.bc_training_nc__c > 0)
			phyRpt.bc_training_nc__c = 0;
		if (phyRpt.bc_trd_study_nc__c != null && phyRpt.bc_trd_study_nc__c > 0)
			phyRpt.bc_trd_study_nc__c = 0;
		if (phyRpt.bc_none__c != null && phyRpt.bc_none__c > 0)
			phyRpt.bc_none__c = 0;
			
		if (phyRpt.b_other__c != null && phyRpt.b_other__c > 0)
			phyRpt.b_other__c = 0;
		if (phyRpt.b_none__c != null && phyRpt.b_none__c > 0)
			phyRpt.b_none__c = 0;
		
		if (phyRpt.bs_billed__c != null && phyRpt.bs_billed__c > 0)
			phyRpt.bs_billed__c = 0;
		if (phyRpt.bs_paid__c != null && phyRpt.bs_paid__c > 0)
			phyRpt.bs_paid__c = 0;
		if (phyRpt.bs_rebilled__c != null && phyRpt.bs_rebilled__c > 0)
			phyRpt.bs_rebilled__c = 0;
		if (phyRpt.bs_none__c != null && phyRpt.bs_none__c > 0)
			phyRpt.bs_none__c = 0;
			
		if (phyRpt.src_dtc_advertising__c != null && phyRpt.src_dtc_advertising__c > 0)
			phyRpt.src_dtc_advertising__c = 0;
		if (phyRpt.src_office_generated__c != null && phyRpt.src_office_generated__c > 0)
			phyRpt.src_office_generated__c = 0;
		if (phyRpt.src_none__c != null && phyRpt.src_none__c > 0)
			phyRpt.src_none__c = 0;
			
		if (phyRpt.type_I_m_rEEG_s__c != null && phyRpt.type_I_m_rEEG_s__c > 0)
			phyRpt.type_I_m_rEEG_s__c = 0;
		if (phyRpt.type_I_u_rEEG_s__c != null && phyRpt.type_I_u_rEEG_s__c > 0)
			phyRpt.type_I_u_rEEG_s__c = 0;
		if (phyRpt.type_I_rEEG_s__c != null && phyRpt.type_I_rEEG_s__c > 0)
			phyRpt.type_I_rEEG_s__c = 0;
		if (phyRpt.type_II__c != null && phyRpt.type_II__c > 0)
			phyRpt.type_II__c = 0;
			
		if (phyRpt.s_complete__c != null && phyRpt.s_complete__c > 0)
			phyRpt.s_complete__c = 0;
		if (phyRpt.s_in_progress__c != null && phyRpt.s_in_progress__c > 0)
			phyRpt.s_in_progress__c = 0;
		if (phyRpt.s_missing_value__c != null && phyRpt.s_missing_value__c > 0)
			phyRpt.s_missing_value__c = 0;
		if (phyRpt.s_new__c != null && phyRpt.s_new__c > 0)
			phyRpt.s_new__c = 0;
		if (phyRpt.s_not_valid_cnsr_stopped_process__c != null && phyRpt.s_not_valid_cnsr_stopped_process__c > 0)
			phyRpt.s_not_valid_cnsr_stopped_process__c = 0;
		if (phyRpt.s_not_valid_corr_calc_errors__c != null && phyRpt.s_not_valid_corr_calc_errors__c > 0)
			phyRpt.s_not_valid_corr_calc_errors__c = 0;
		if (phyRpt.s_not_valid_eeg_errors__c != null && phyRpt.s_not_valid_eeg_errors__c > 0)
			phyRpt.s_not_valid_eeg_errors__c = 0;
		if (phyRpt.s_not_valid_pat_stopped_process__c != null && phyRpt.s_not_valid_pat_stopped_process__c > 0)
			phyRpt.s_not_valid_pat_stopped_process__c = 0;
		if (phyRpt.s_not_valid_phy_stopped_process__c != null && phyRpt.s_not_valid_phy_stopped_process__c > 0)
			phyRpt.s_not_valid_phy_stopped_process__c = 0;
		if (phyRpt.s_on_hold_cnsr__c != null && phyRpt.s_on_hold_cnsr__c > 0)
			phyRpt.s_on_hold_cnsr__c = 0;
		if (phyRpt.s_on_hold_cnsr_review__c != null && phyRpt.s_on_hold_cnsr_review__c > 0)
			phyRpt.s_on_hold_cnsr_review__c = 0;
		if (phyRpt.s_on_hold_eeg_scheduled__c != null && phyRpt.s_on_hold_eeg_scheduled__c > 0)
			phyRpt.s_on_hold_eeg_scheduled__c = 0;			
		
		if (phyRpt.s_on_hold_washout_in_progress__c != null && phyRpt.s_on_hold_washout_in_progress__c > 0)
			phyRpt.s_on_hold_washout_in_progress__c = 0;
		if (phyRpt.s_pending__c != null && phyRpt.s_pending__c > 0)
			phyRpt.s_pending__c = 0;
		if (phyRpt.s_none__c != null && phyRpt.s_none__c > 0)
			phyRpt.s_none__c = 0;	
		}			
	}
	
	private rpt_physician__c getBlankPhyRpt(Contact con, DateTime dt)
	{
		rpt_physician__c pRpt = new rpt_physician__c();
		pRpt.Name = con.LastName + ',' + con.FirstName + ' ' + dt.month() + '/' + dt.year();
		pRpt.month__c = dt.month();
		pRpt.year__c = dt.year();
		pRpt.physician__c = con.Id;
		pRpt.phy_name__c = con.LastName + ',' + con.FirstName;
		pRpt.reeg_forecast__c = con.reeg_forecast__c;
		return pRpt;
	}
	
	private Double getVal(Double field)
	{
		return (field == null) ? 1 : field + 1;
	}
	
	public void testZeroSetMethods()
	{
		rpt_physician__c pRpt = new rpt_physician__c();
		zeroReegFieldVals(pRpt);
		zeroPatFieldVals(pRpt);
	}
	
	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---
    //-----------------------------------------------------------------------
    public static testMethod void testrpt_PhyProcessController()
    {
    	rEEGDao rDao = new rEEGDao();
    	rDao.IsTestCase = true;
    	r_reeg__c reeg = rEEGDao.getTestReeg();
    	
    	patientDao pDao = new patientDao();
    	pDao.IsTestCase = true;
    	r_patient__c pat = pDao.getById(reeg.r_patient_id__c);
    	
    	Contact c = ContactDao.getTestPhysician();
    	
    	DateTime selDt = DateTime.now();

		rpt_PhyProcessController cont = new rpt_PhyProcessController();
		cont.testZeroSetMethods();
		cont.IsTestCase = true;
		cont.buildRpt(c, selDt);
		cont.processRpts(selDt);
		
    }   
	
}