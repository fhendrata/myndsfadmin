public with sharing class phy_PeerReportController extends BaseDrugReportController
{
    public string drugName {get; set;}
    public string BrainMapUrl {get; set;}
    public boolean IsNeuroRptAvail {get; set;}
    public boolean IsClassicReportOnly {get; set;}
    
    public phy_PeerReportController()
    {
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        rDao = new rEegDao();
    }
    
    public PageReference loadAction()
    {
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            this.reeg = rDao.getById(reegId, true);
            baseUrl = '/apex/phy_PeerReportS2?rid=' + reeg.Id;
            baseReport2Url = '/apex/phy_PeerReportS2?rid=' + reeg.Id;
            baseReportTabbedUrl = '/apex/phy_PeerReportS2?rid=' + reeg.Id;
            BrainMapUrl = '/apex/PEERBrainMapView?rid=' + reeg.id;
            cnsVarsUrl = '/apex/phy_CNSVarsS2?rid=' + reeg.Id + '&ctid=' + reeg.corr_cns_id__c;
            
            if (!e_StringUtil.isNullOrEmpty(pageId))
            {
                DrugTreeMenuUtil.MenuItem menuItem = menuMap.get(pageId);
                if (menuItem != null)
                    drugName = menuItem.drugName;
            }
        }
        else
        {
            this.reeg = new r_reeg__c();
        }    

        System.debug('### neurostat:' + reeg.neuro_stat__c);
        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        IsClassicReportOnly = false;
       // IsClassicReportOnly = (e_StringUtil.isNullOrEmpty(reeg.neuroguide_status__c) && !e_StringUtil.isNullOrEmpty(reeg.rpt_stat__c) && (reeg.rpt_stat__c == 'Complete' || reeg.rpt_stat__c == 'Delivered'));
        return null;    
    }
    
    public String getSummaryUrl()
    {
    	return '/apex/phy_PeerSummaryS2?pgid=' + this.pageId + '&rid=' + this.reegId;
    }
    
    public PageReference summaryAction()
    {
        PageReference pr = new PageReference('/apex/phy_PeerSummaryS2?pgid=' + this.pageId + '&rid=' + this.reegId);
        pr.setRedirect(true);
        
        return pr;
    }   
    
    public PageReference returnAction()
    {
        PageReference pr = new PageReference('/apex/phy_PatientListS2');
  
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference printAction()
    {
        PageReference pr = new PageReference('/apex/phy_PeerReportPrintPage?pgid=' + this.pageId + '&rid=' + this.reegId);
  
        pr.setRedirect(true);
        return pr;
    }

    // --------- TEST METHODS ----------
    
    private static testmethod void testController()
    {
    	phy_PeerReportController prc = new phy_PeerReportController();
    	prc.returnAction();
    	prc.summaryAction();
    	prc.loadAction();
    	prc.getSummaryUrl();
    }
}