//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: PhyPortalLoginController
// PURPOSE: Controller class for the physician portal login page
// CREATED: 02/12/14 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
public with sharing class PhyPortalLoginController extends BaseController
{
	
	public PhyPortalLoginController() 
	{
		System.debug('### PhyPortalLoginController:constructor ');
	}

	public PageReference loadAction()
	{
		PageReference pr = null;

		String redirected = ApexPages.currentPage().getParameters().get('redirect');	

		if (URL.getSalesforceBaseUrl().getProtocol() == 'http' && e_StringUtil.isNullOrEmpty(redirected))
		{
			pr = new PageReference('https://myreeg.secure.force.com');
			pr.setRedirect(true);
		}

		return pr;
	}

    

}