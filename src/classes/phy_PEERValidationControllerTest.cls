//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: phy_PEERValidationControllerTest
// PURPOSE: Test class for the phy_PEERValidationController class methods
// CREATED: 01/09/15 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class phy_PEERValidationControllerTest
{
	
	private static testmethod void testMethods() 
	{
		PEER_Requisition__c req = PEERRequisitionDao.getTestObj();
		
		Test.startTest();

		phy_PEERValidationController reqController = new phy_PEERValidationController(new ApexPages.standardController(req));       
    	ApexPages.currentPage().getParameters().put('id', req.id);
		reqController.loadAction();

    	Boolean testBool = reqController.IsTypeOne;
		testBool = reqController.IsDxListEmpty;
		//testBool = reqController.IsMedListVerified;
		testBool = reqController.IsFirstNameEmpty;
		testBool = reqController.IsLastNameEmpty;
		testBool = reqController.IsDobEmpty;
		testBool = reqController.IsPeerTypeEmpty;
		testBool = reqController.IsEegTechEmpty;
		testBool = reqController.IsEegRecStatusEmpty;
		testBool = reqController.IsEegStatusEmpty;
		testBool = reqController.CanSubmit;
		
		reqController.ReqObj.patient_physician_user__c = UserInfo.getUserId();

		reqController.submit();

		r_reeg__c reeg = rEEGDao.getTestReegPO2();
		req.Patient__c = reeg.r_patient_id__c;
		PEERRequisitionDao.getInstance().Save(req);

		ApexPages.currentPage().getParameters().put('retURL', reeg.r_patient_id__c);
		reqController.returnToPatient();
		reqController.returnToDx();
		reqController.returnToWo();
		reqController.returnToEeg();
		reqController.returnToSubmit();
		reqController.returnToRequisition();

		Test.stopTest();
	}
	
}