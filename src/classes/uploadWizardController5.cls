public with sharing class uploadWizardController5 extends BaseUploadWizController
{   
	public boolean isSubmitVerified { get; set; }
    private List<rEEGDxDisp> dxs;
    private List<rEEGDxDisp> dxObjs;
    
    private List<rEEGDxGroupDisp> dxGroups;
    private List<rEEGDxGroupDisp> dxGroupsL;
    private List<rEEGDxGroupDisp> dxGroupsR;
    
    public uploadWizardController5()
    {
    	init();
    }
    
    public void init()
    {
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.qsid = ApexPages.currentPage().getParameters().get('qsid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        string strIsNewPat = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(strIsNewPat) && strIsNewPat == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        String strIsNew = ApexPages.currentPage().getParameters().get('isNew');
        this.IsNew = (!e_StringUtil.isNullOrEmpty(strIsNew) && strIsNew == 'true');
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        
        rDao = new rEEGDao();
    }
    
    public PageReference loadAction()
    {
    	if (!e_StringUtil.isNullOrEmpty(reegId))
			reeg = rDao.getById(reegId); 
			
		return null;   	
    }
    
    public PageReference wizStep4()
    {
       	PageReference returnVal = new PageReference('/apex/uploadWizard4');
       	AddParameters(returnVal);
               	
        returnVal.setRedirect(true);
        return returnVal;
    } 

    public PageReference wizStep6()
    {
    	PageReference returnVal = null;
    	
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
        	if (reeg == null)
    			reeg = rDao.getById(reegId); 
	        
	        if (reeg != null)
	        {
		        reeg.stat_dx_gen__c = saveDx();
	        	rDao.saveSObject(reeg);
		
		        returnVal = new PageReference('/apex/uploadWizard6');
		        AddParameters(returnVal);
		        returnVal.setRedirect(true);
	        }
        }

        return returnVal;
    }
    
    public PageReference submit()
    {
    	PageReference returnVal = null;
		
		if (isSubmitVerified)
		{
			reeg.req_stat__c = 'In Progress';
	        reeg.eeg_rec_stat__c = 'In Progress';
			
			reeg.stat_dx_gen__c = saveDx();
			rDao.Save(reeg, 'UPDATE_DB_STATUS');
			
			returnVal = new PageReference('/' + reeg.Id);
		}
	    
	    return returnVal; 
    } 
    
    private boolean validateFields()
    {
    	boolean returnVal = false;
    	
    	return true;
    }
    
    //-------------------- DX ---------------------------
    public List<rEEGDxDisp> getDxs()
    {
        if (dxs== null)
        {
            loadFullList();
            loadSavedList();                
        }    
        return dxs;
    }
    
    public List<rEEGDxGroupDisp> getDxGroupsL()
    {
        getDxGroups();
        return dxGroupsL;
    }
    public List<rEEGDxGroupDisp> getDxGroupsR()
    {
        getDxGroups();
        return dxGroupsR;
    }
    
    public List<rEEGDxGroupDisp> getDxGroups()
    {
        if (dxGroups== null)
        {
            getDxs();
            loadGroupList();
        }    
        return dxGroups;
    }
    
    private void loadFullList()
    {  
        dxs = new List<rEEGDxDisp>();
    
        for(ref_dx__c refRow : 
                 [select description__c, is_group__c, primary__c, rule_out__c, secondary__c, sequence__c, id, name 
                     from ref_dx__c
                     order by sequence__c])
        {
            rEEGDxDisp dx = new rEEGDxDisp();
            dx.SetupRef( refRow );     
            dx.Setup(new r_reeg_dx__c());                                        
            dxs.Add( dx);
        } 
    }
    
   private void loadSavedList()
    {
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
	        for(r_reeg_dx__c dxRow : 
	             [select dx_name__c, primary__c, r_ref_dx_id__c, rule_out__c, secondary__c, id, name 
	                 from r_reeg_dx__c
	                 where r_reeg_id__c = :reegId limit 5])
	        {
	            AddSavedToList(dxRow);
	        }
    	} 
    }
    
    private void AddSavedToList(r_reeg_dx__c o)
    {
        for(rEEGDxDisp dxRow : dxs)
        {
            if (dxRow.getRefObj() != null && dxRow.getRefObj().id == o.r_ref_dx_id__c)
            {
                dxRow.Setup(o);
            }
        }
    }
    
    private void loadGroupList()
    {
        dxGroups = new List<rEEGDxGroupDisp>();
        
        rEEGDxGroupDisp currGroup = null;
        Integer lastGroupId = 0;
        Integer totalItemCount = 0;
                
        for(rEEGDxDisp dxRow : dxs)
        {
            if (dxRow.getRefObj().is_group__c)
            {
                currGroup = new rEEGDxGroupDisp();
                currGroup.Setup( dxRow.getObj());
                currGroup.SetupRef( dxRow.getRefObj());
                
                dxGroups.Add(currGroup);
            }
            else
            {
                if (currGroup != null) 
                {
                    totalItemCount++;
                    currGroup.addItem( dxRow);
                }
            }
        }
               
        dxGroupsL = new List<rEEGDxGroupDisp>();
        dxGroupsR = new List<rEEGDxGroupDisp>();
 
        Integer addedItemCount = 0;
        
        for(rEEGDxGroupDisp groupRow : dxGroups)
        {
            if (addedItemCount > (totalItemCount/2))
            {
                dxGroupsR.Add(groupRow);    
            }
            else
            {
                dxGroupsL.Add(groupRow);
                 if (groupRow != null && groupRow.getItemCount() != null)
                	addedItemCount += groupRow.getItemCount();
                else
                	addedItemCount = (addedItemCount == null) ? 0 : addedItemCount;
            }
        }       
    }  
    
    public Boolean saveDx()
    {          
        saveDxGroup( dxGroupsL );
        saveDxGroup( dxGroupsR );

        return true;
    } 
    
    private void saveDxGroup(List<rEEGDxGroupDisp> groupList)
    {
        if (groupList!= null)
        {
            for(rEEGDxGroupDisp grpRow : groupList)
            {
                for(rEEGDxDisp dxRow : grpRow.getDxs())
                {   
                    if (dxRow == null)
                    {
                        addMessage( 'DxRow is null');         
                    }
                    else if (dxRow.getObj() == null)
                    {
                        addMessage( 'DxRow.getObj() is null');         
                    }
                    else
                    {
                        SaveDx(dxRow.getObj(), dxRow.getRefObj());
                    }
                }
            }
        }
    }
    
    public void SaveDx( r_reeg_dx__c obj, ref_dx__c refObj)
    {
        Boolean hasValue = (obj.primary__c || obj.secondary__c || obj.rule_out__c);

        if (hasValue)
        {
             if (obj.id != null)
             {
                upsert obj;
             }
             else
             {
                 obj.r_reeg_id__c = reegId;
                 obj.r_ref_dx_id__c = refObj.id; 
                 obj.dx_name__c = refObj.description__c; 
                 obj.name = refObj.name; 

                 insert obj;
             }                
        }
        else
        {
            if (obj.id != null) delete obj;
        }    
    }
    
    private void addMessage(String value)
    {
        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.INFO, value);
        ApexPages.addMessage( msg);
    }
    
    public PageReference cancelWizOverride()
    {
    	PageReference pr = cancelWizAction();
    	pr = new PageReference('/apex/UploadWizard1');
    	pr.setRedirect(true);
        return pr;
    }

          //---TEST METHODS ------------------------------
    public static testMethod void testDxController()
    {
    	uploadWizardController5 cont = new uploadWizardController5();
		cont.IsTestCase = true;
    	r_reeg__c reeg = rEEGDao.getTestReeg();
        cont.reeg = reeg;
        patientDao patDao = new patientDao();
        patDao.IsTestCase = true;
        
        cont.patient = patDao.getById(reeg.r_patient_id__c);
        
        patientOutcomeDao patOutcomeDao = new patientOutcomeDao();
        r_patient_outc__c outcome = patOutcomeDao.getTestOutcome(reeg.r_patient_id__c);
        patientOutcomeMedDao medDao = new patientOutcomeMedDao();
        medDao.IsTestCase = true;
        r_outcome_med__c outMed =  medDao.getTestOutcomeMed(outcome.Id);    
        
 		cont.outcomeId = ApexPages.currentPage().getParameters().put('oid', outcome.Id);
 		cont.patId = ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
 		ApexPages.currentPage().getParameters().put('wiz', 'true');
 		ApexPages.currentPage().getParameters().put('isNew', 'true');
		ApexPages.currentPage().getParameters().put('np', 'true');
		ApexPages.currentPage().getParameters().put('isNew', 'true');
    	
    	PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('id',reeg.id);
    	
    	rEEGDxDao dxDao = new rEEGDxDao();
    	dxDao.IsTestCase = true;
    	r_reeg_dx__c dx = dxDao.getTestDx();
    	dx = dxDao.getTestDx();
		cont.loadAction();
		
		rEEGDao rDao = new rEEGDao();
		rDao.saveSObject(reeg);
		
		cont.reegId = reeg.Id;
        PageReference pRef4 = cont.wizStep4();
        
        cont.reegId = reeg.Id;
        reeg.reeg_type__c = 'Type II';
        rDao.saveSObject(reeg);
        
        cont.wizStep6();
        cont.cancelWizOverride();
   
        List<rEEGDxDisp> dxList = cont.getDxs();
        List<rEEGDxGroupDisp> DxGroupDisp = cont.getDxGroupsL();
        DxGroupDisp = cont.getDxGroupsR();
        DxGroupDisp = cont.getDxGroups();
        

   //     cont.saveDx();

     	cont.RemoveReeg(null);

        System.assertEquals( 1, 1);
    }

}