/* --------------------------------------------------------------------------------
* COMPONENT: PEER Online 2.0
* CLASS: PEERExternalWebService
* PURPOSE: Webservice methods that are available to the exteranl C# applications
* CREATED: 08/21/17 Ethos Solutions - www.ethos.com
* Author: Joe DePetro
* --------------------------------------------------------------------------------
*/
global class PEERExternalWebService 
{
	webservice static void submitPeerRequsition(String peerReqId) 
	{
		if (String.isNotBlank(peerReqId))
		{
			PEER_Requisition__c pr = GlobalPEERRequisitionDao.getInstance().getById(peerReqId);
			if (pr != null)
			{
				List<Req_Outcome_Medication__c> objMedList = ReqOutcomeMedDao.getInstance().getByRequisitionId(pr.Id);
				GlobalPEERRequisitionDao.getInstance().submit(pr, objMedList);
			}
		}
	}
}