public with sharing class phy_PatientController extends BaseController
{
    public r_patient__c patient {get; set;}
    public List<r_patient_outc__c> outcomeList {get; set;}
    public List<r_outcome_med__c> previousMeds { get; set; }

    public List<PEERRequisitionDisp> PeerRequisitionList { get; set; }
    public Boolean ShowPeerRequisitionList { get; set; }
    public Boolean isValidDOB {get; set;}
   
    private Contact contact;

    //used for the multi peer report page.
    public PageReference loadActionMulti()
    {
        String isNewPEEROrder = ApexPages.currentPage().getParameters().get('order');
        String patId = ApexPages.currentPage().getParameters().get('id');
        if (String.isNotBlank(isNewPEEROrder) && isNewPEEROrder.equalsIgnoreCase('1') && String.isNotBlank(patId))
        {
            PageReference pr = new PageReference( '/apex/PEERRequisition?pid=' + patId  + '&ps=true');
            pr.setRedirect(true);
            return pr;
        }

        String isPEERReq = ApexPages.currentPage().getParameters().get('pr');

        if (String.isNotBlank(isPEERReq) && isPEERReq.equalsIgnoreCase('1'))
        {
            ShowPeerRequisitionList = true;
            loadPEERs();
            if(PeerRequisitionList == null || PeerRequisitionList.size() == 0)
            {
                //add message saying nothing found
                ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.INFO, 'This patient has no PEER Requistions');
                ApexPages.addMessage( msg);
                
                return null;
            }
            
            if(PeerRequisitionList.size() == 1)
            {
                PageReference returnVal = null;
                returnVal = new PageReference( '/apex/PEERRequisition?id=' + PeerRequisitionList[0].Obj.Id  + '&ps=true');
                returnVal.setRedirect(true);
                return returnVal;
            }
            
            //add message
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.WARNING, 'This patient has more than one PEER Requisition. Please select one below.');
            ApexPages.addMessage( msg);
            return null;
        }
        else
        {
            ShowPeerRequisitionList = false;
        	getReegs();
        	if(reegs == null || reegs.size() == 0)
        	{
        		//add message saying nothing found
        		
        		ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.INFO, 'This patient has no PEER Reports');
                ApexPages.addMessage( msg);
        		
        		return null;
        	}
        	
        	if(reegs.size() == 1)
        	{
                PageReference returnVal = null;
                String pageStr = '/apex/phy_PeerSummaryS2?pgid=';

                if (reegs[0].isPEER2)
                {
                    if (reegs[0].isWR)
                        pageStr = '/apex/wr_PeerSummaryS2?pgid=';
                    else
                        pageStr = '/apex/peer_Summary2?pgid=';
                }

        		returnVal = new PageReference( pageStr + patient.Id + '&rid=' + reegs[0].getObj().Id );
                returnVal.setRedirect(true);
                return returnVal;
        	}
        	
        	//add message
    	   ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.WARNING, 'This patient has more than one PEER report. Please select one below.');
           ApexPages.addMessage( msg);
           return null;
       }
    	
    }


    //---this is only used for the apex test case
    public phy_PatientController()
    {
        patient = new r_patient__c();
        patient.dob__c = '12/12/1970';
        patient.first_name__c = 'test case patient2';
        
        Profile p = new Profile();
        p = [select Id,Name from Profile where Name =: 'Physician Portal Manager'];
        setProfile(p);

        contact = new Contact();
        contact.gender_cd__c = 'Male';

        isValidDOB = true;
    }

    //---Constructor from the standard controller
    public phy_PatientController( ApexPages.StandardController stdController)
    {    
        this.patient = (r_patient__c)stdController.getRecord();
        
        String mesg = ApexPages.currentPage().getParameters().get('mesg');
        if(mesg != null && mesg != '')
        {
	        ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, mesg);
	        ApexPages.addMessage( msg);
        }   
       
        init();

        isValidDOB = true;
    }
    
    public void init()
    {
        this.outcomes = getOutcomes();
    }
    
    public PageReference buildIntervals()
    {
        patientIntervalBuilder intBuilder = new patientIntervalBuilder();
        intBuilder.buildAllIntervalsForPat(patient.Id, patient.OwnerId);
        
        return null;
    }
    
    public PageReference Cancel()
    {
    	PageReference returnVal = new PageReference( '/apex/phy_PatientViewS2?id=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference Edit()
    {
    	PageReference returnVal = new PageReference( '/apex/phy_PatientEditS2?id=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }
    

    public PageReference Save()
    {
        setPatientAge();

        if (isValidDOB) {        
            if (patient.id != null) upsert patient;
            else insert patient;

            PageReference returnVal = new PageReference( '/apex/phy_PatientViewS2?id=' + patient.id);
            returnVal.setRedirect(true);
            return returnVal;
        }
        else {
            return null;
        }
    }

      //----------- These should be depreciated 
    public PageReference saveAndNew()
    {
        setPatientAge();

        if (patient.id != null) upsert patient;
        else insert patient;

        PageReference returnVal = new PageReference( '/apex/patientNewPage');
        returnVal.setRedirect(true);
        return returnVal;
    }


    public PageReference medListEditPage()
    {
        return new PageReference('/apex/patientMedListEditPage?id=' + patient.id);
    }
    
    public PageReference medListPastEditPage()
    {
        return new PageReference('/apex/patientMedListPastEditPage?id=' + patient.id);
    }
     
    public PageReference patOutcomeEditPage()
    {
        return new PageReference('/r_patient__c.object?id=' + patient.id);
    }
    
    public PageReference patSharingPage()
    {
        return new PageReference('/p/share/CustomObjectSharingDetail?parentId=' + patient.id);
    }
    
    private void setPatientAge()
    {
        if ((patient.patient_age__c == null || patient.patient_age__c < 1) && patient.dob__c != null)
        {
            Date dobDt = e_StringUtil.setDateFromString(patient.dob__c);
            if (dobDt != null) {
                isValidDOB = true;
                Integer numberDaysOld = dobDt.daysBetween(Date.today());             
                if (numberDaysOld > 0)
                    patient.patient_age__c = (numberDaysOld / 365.242199);
            }
            else {
                isValidDOB = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a valid Date of Birth'));
            }
        }
    }
    
    //-------------
    
    List<PatientOutcomeMedDisp> omeds;
    public List<PatientOutcomeMedDisp> getoMeds()
    {
        if (omeds == null)
        {
            omeds = new List<PatientOutcomeMedDisp>();  
            PatientOutcomeMedDisp oMed;          
            Integer ctr = -1;
             
            patientOutcomeMedDao medDao = new patientOutcomeMedDao();
            previousMeds = medDao.getExistingOutcomeMeds(patient.id);
            if (previousMeds != null) 
            {
                for(r_outcome_med__c oMedRow : previousMeds) 
                {
                    ctr++;  
                    oMed = new PatientOutcomeMedDisp();
                    oMed.setDisplayAction(true);
                    oMed.setRowNum(ctr);
                        
                    r_outcome_med__c hldgMed = new r_outcome_med__c();
                    hldgMed.med_name__c = oMedRow.med_name__c;
                    hldgMed.dosage__c = oMedRow.dosage__c;
                    hldgMed.unit__c = oMedRow.unit__c;
                    hldgMed.frequency__c = oMedRow.frequency__c;
                    hldgMed.start_date__c = oMedRow.start_date__c;
                    hldgMed.end_date__c = oMedRow.end_date__c;
                    hldgMed.previous_med__c = oMedRow.Id;
                    hldgMed.start_date_is_estimated__c = oMedRow.start_date_is_estimated__c;
                    oMed.Setup(hldgMed);                    
                    omeds.Add(oMed);                        
                }
            }             
        }    
        
        return omeds;
    }

    private List<PatientMedDisp> meds;   
    public List<PatientMedDisp> getMeds()
    {
         if (meds == null)
         {
             meds = new List<PatientMedDisp>();  

             if (patient.id != null)
             {
                for(r_patient_med__c medRow : 
                     [select dosage__c, drugreview__c, end_m_pl__c, end_y_pl__c, generic__c, is_wo_extended__c, med_name__c, no_dosage__c, reason_stop_text__c, reason_stop_pl__c, effect_pl__c, side_effects__c, start_m_pl__c, start_y_pl__c, status__c, symtoms__c, Unit__c, id, name, washout_required__c 
                     from r_patient_med__c
                     where r_patient_id__c = :patient.id
                     order by CreatedDate desc])
                {
                    PatientMedDisp med = new PatientMedDisp();
                    med.Setup( medRow );                                 
                    meds.Add( med);
                } 
            }     
        }    

        return meds;
    }

    private List<ReegDisp> reegs;   
    public List<ReegDisp> getReegs()
    {
         if (reegs == null)
         {
             reegs = new List<ReegDisp>();  

             if (patient.id != null)
             {
                for(r_reeg__c reegRow : 
                     [select id,name,Process_Used_For_Test__c,neuroguide_status__c,req_stat__c,rpt_stat__c,rpt_date__c ,req_date__c,reeg_type__c,reeg_type_mod__c,is_ng_only__c,r_physician_id__c, r_physician_id__r.Name, r_physician_id__r.Account.Report_Build_Type__c
                     from r_reeg__c
                     where r_patient_id__c = :patient.id AND (NOT req_stat__c LIKE 'not valid%') AND r_patient_id__r.CA_Control_Group_Type__c != 'Control Group' AND reeg_type__c != 'pre-Washout' order by req_date__c desc])
                {
                    ReegDisp reeg = new ReegDisp();
                    reeg.Setup( reegRow );                                 
                    reegs.Add( reeg);
                }

            }
        }    
        return reegs;
    }  


    public void loadPEERs()
    {
        if (PeerRequisitionList == null)
            PeerRequisitionList = new List<PEERRequisitionDisp>();  

        if (patient.id != null)
        {
            for(PEER_Requisition__c reqRow : PEERRequisitionDao.getInstance().getByPatientId(patient.id))
            {                       
                PeerRequisitionList.Add( new PEERRequisitionDisp(reqRow));
            }
        }
    }  

    public PageReference peerNewPage()
    {
        PageReference returnVal = new PageReference('/apex/PEERRequisition');
        returnVal.setRedirect(true);
        return returnVal;
    }

    public PageReference rEEGNewPage()
    {
        PageReference returnVal = new PageReference( '/apex/phy_PeerRequisition1S2?id=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }


    private List<patientOutcomeDisp> outcomes;   
    public List<patientOutcomeDisp> getOutcomes()
    {
        string outcomesMeds = '';
        string patId = patient.id;
        if (outcomes == null)
        {
            patientOutcomeDao outDao = new patientOutcomeDao();
            outcomes = outDao.getPatOutcomeDispListNonWR(patient.id);  
        }    
        
        return outcomes;
    }  

    public PageReference newOutcome()
    {
        PageReference returnVal = new PageReference( '/apex/phy_OutcomeEditS2?pid=' + patient.id);
        returnVal.setRedirect(true);
        return returnVal;
    }

    private List<patientIntervalListDisp> intervalDispList;   
    public List<patientIntervalListDisp> getIntervalDispList()
    {
        patientIntervalsDao intDao = new patientIntervalsDao();
        List<r_interval__c> intList = intDao.getByPatId(patient.id);
            
        patientIntervalListDisp intDisp;
        intervalDispList = new List<patientIntervalListDisp>();
        
        if (intList != null && intList.size() > 0)
        {
            for (r_interval__c row : intList)
            {
                intDisp = new patientIntervalListDisp();
                intDisp.Interval = row;
                if (row.start_date__c != null && row.stop_date__c != null)
                    intDisp.Duration = row.start_date__c.daysBetween(row.stop_date__c);
                
                intervalDispList.add(intDisp);
            }
        }
        
        return intervalDispList;

    }  

    public PageReference deletePatient()
    {
        if (getIsMgr()) delete patient;

        String patientPrefix = rEEGUtil.getPatientPrefix();
        PageReference returnVal = new PageReference('/' + patientPrefix + '/o');
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public List<r_patient__History> getPatHistList()
    {
       List<r_patient__History> patHistList = new List<r_patient__History>(); 

       if (patient.id != null)
       {
            patHistList = [Select ParentId, OldValue, NewValue, Id, Field, CreatedDate, CreatedById 
                            From r_patient__History
                            where ParentId = :patient.id
                            order by CreatedDate Desc limit 25];
       }     
       
       return patHistList;
    } 

    //-----------------TEST-----------------------------------
    public static testMethod void testController()
    {
        phy_PatientController cont = new phy_PatientController();
        cont.IsTestCase = true;
        cont.init();
        r_interval__c intv = patientIntervalsDao.getTestInterval();
        patientDao patDao = new patientDao();
        r_patient__c pat = patDao.getById(intv.r_patient__c);
        cont.patient = pat;
        List<patientIntervalListDisp> iDispList = cont.getIntervalDispList();  
        
        patientOutcomeMedDao ocmedDao = new patientOutcomeMedDao();
        r_outcome_med__c oMed = ocmedDao.getTestOutcomeMed();
        
        if (oMed != null)
        {
            patientOutcomeDao patOutDao = new patientOutcomeDao();
            r_patient_outc__c testoutcome =  patOutDao.getById(oMed.r_outcome_id__c);
            
            if (testoutcome != null)
            {
                pat = patDao.getById(testoutcome.patient__c);
                cont.patient = pat;
            }
        }
        
        List<PatientOutcomeMedDisp> omedList = cont.getoMeds();

        cont.buildIntervals();
        PageReference pRef1 = cont.Save();
        PageReference pRef2 = cont.saveAndNew();
        PageReference pRef3 = cont.medListEditPage();
        PageReference pRef4 = cont.rEEGNewPage();
        PageReference pRef5 = cont.patOutcomeEditPage();
        //PageReference pRef6 = cont.rEEGAnalysisPage();
        PageReference pRef7 = cont.newOutcome();
        pRef7 = cont.medListPastEditPage();

        Boolean val1 = cont.getIsMgr();
        Boolean notval = cont.getIsNotMgr();
        Boolean val = !notval;
        System.assertEquals( val1, val);
        List<PatientMedDisp> medList = cont.getMeds();
        
        List<ReegDisp> reegList = cont.getReegs();
        List<patientOutcomeDisp> outList = cont.getOutcomes();
        
        
        List<r_patient__History> patHistList = cont.getPatHistList();
        
        pRef7 = cont.patSharingPage();

        pRef7 = cont.deletePatient();
        System.assertEquals( '1', '1');
    }
}