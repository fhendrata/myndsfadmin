public with sharing class phy_HomeController 
{
	private GlobalPatientDao pDao;
	private integer resultTableSize;
    public ApexPages.StandardSetController OutcomeCon {get; set;}
    public ApexPages.StandardSetController PeerCon {get; set;}
    public ApexPages.StandardSetController PatientCon {get; set;}
  
	public String patSearchString {get; set;}
	
	
    public phy_HomeController()
    {
    	 pDao = new GlobalPatientDao();
    	 resultTableSize = 5;
    	 loadPatients();
    	 loadOutcomes();
    	 loadPeers();
    	
    }
    
    public void searchPatients()
    { 
    	
    	
        String query = pDao.getNameSearchQuery(patSearchString,'limit 5');
        PatientCon = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        PatientCon.setPageSize(resultTableSize);
        
    }
    
    private PageReference loadOutcomes()
    {
        OutcomeCon = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id,Name,patient__c,rEEG__c,cgi__c,cgi_patient__c,cgs__c,patient__r.Name,patient__r.first_name__c,patient__r.middle_initial__c,rEEG__r.rpt_date__c FROM r_patient_outc__c order by Name limit 5]));
        OutcomeCon.setPageSize(resultTableSize);
        return null;
    }
    
    private PageReference loadPeers()
    {
        PeerCon = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id,Name, rpt_stat__c, req_date__c, rpt_date__c, r_patient_id__c, r_patient_id__r.Name, r_patient_id__r.middle_initial__c, r_patient_id__r.first_name__c, r_patient_id__r.dob__c, r_patient_id__r.prov_pat_id__c FROM r_reeg__c order by Name limit 5]));
        PeerCon.setPageSize(resultTableSize);
        return null;
    }
    
    private PageReference loadPatients()
    {
        PatientCon = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id, Name, first_name__c, Outcomes__c, Days_Since_Last_PEER__c,Last_PEER_Date__c, middle_initial__c, prov_pat_id__c FROM r_patient__c order by Name limit 10]));
        PatientCon.setPageSize(10);
        return null;
    }
    
    
    public List<r_patient_outc__c> getOutcomes()
    {
        if(OutcomeCon != null)  
            return (List<r_patient_outc__c>)OutcomeCon.getRecords(); 
        else
            return null;
    }
    
   
    
    public List<r_patient__c> getPatients()
    {
    	
    	
        if(PatientCon != null)  
            return (List<r_patient__c>)PatientCon.getRecords(); 
        else
            return null;
    }
    
    public List<r_reeg__c> getPeers()
    {
        if(PeerCon != null)  
            return (List<r_reeg__c>)PeerCon.getRecords(); 
        else
            return null;
    }
    
    
    //---------------------- TEST METHODS ------------------------
    
    private static testmethod void testController()
    {
        phy_HomeController hc = new phy_HomeController();
        hc.patSearchString = 'a';
        hc.searchPatients();
        hc.getOutcomes();
        hc.getPatients();
        hc.getPeers();
    }
    
    
    
}