//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: patientControllerExtTest
// PURPOSE: Test class for the patientControllerExt class and also the BaseController base class methods
// CREATED: 03/22/18 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class patientControllerExtTest 
{


    //---BASECONTROLLER TEST METHODS ------------------------------
    public static testMethod void testCall()
    {
        rEEGAnalysisController cont = new rEEGAnalysisController();
		Profile p = cont.getProfile();
	    p = null;

        Boolean val = cont.getIsSysAdmin();
        cont.setProfile(p);
        val = cont.getIsSeniorProdMgr();
        cont.setProfile(p);
        val = cont.getIsProduction();
        cont.setProfile(p);
        val = cont.getIsSalesDir();
        cont.setProfile(p);
        val = cont.getIsSalesProvider();
        cont.setProfile(p);
        val = cont.getIsNotQA();
        cont.setProfile(p);

        val = cont.getIsProdMgr();
        cont.setProfile(p);
        val = cont.getIsMedDir();
        cont.setProfile(p);
        val = cont.getIsPortalUser3();
        cont.setProfile(p);
        val = cont.getIsPortalUser2();
        cont.setProfile(p);
        val = cont.getIsPortalUser1();
        cont.setProfile(p);
        val = cont.getIsQA();
        cont.setProfile(p);
         val = cont.getIsMgrLevel4();
         cont.setProfile(p);
        val = cont.getIsMgrLevel3();
        cont.setProfile(p);
        val = cont.getIsMgrLevel2();
        cont.setProfile(p);
        val = cont.getIsMgrLevel1();
        cont.setProfile(p);
        val = cont.getIsPhyAndTechLevel2();

        val = cont.isSsnValid('123-45-6789');
        val = cont.isZipValid('85020');
        val = cont.isStateValid('AZ');
        val = cont.isEmailValid('joe@ethos.com');
        val = cont.isPhoneValid('999-999-9999');
        val = cont.isLoggedIn();
  //      Date dt = new Date('1950', '1', '1');
  //      val = cont.isDobValid(dt);
      //  val = cont.isDateRangeValid();
        String test = cont.getClassName2();
    }

	 //-----------------TEST-----------------------------------
    public static testMethod void testController()
    {
        patientControllerExt cont = new patientControllerExt();
        cont.IsTestCase = true;
        cont.init();
        r_interval__c intv = patientIntervalsDao.getTestInterval();
        patientDao patDao = new patientDao();
        r_patient__c pat = patDao.getById(intv.r_patient__c);
        cont.patient = pat;
        List<patientIntervalListDisp> iDispList = cont.getIntervalDispList();  
        
        patientOutcomeMedDao ocmedDao = new patientOutcomeMedDao();
        r_outcome_med__c oMed = ocmedDao.getTestOutcomeMed();
        
        if (oMed != null)
        {
	        patientOutcomeDao patOutDao = new patientOutcomeDao();
	        r_patient_outc__c testoutcome =  patOutDao.getById(oMed.r_outcome_id__c);
	        
	        if (testoutcome != null)
	        {
        		pat = patDao.getById(testoutcome.patient__c);
        		cont.patient = pat;
	        }
        }
        
        List<PatientOutcomeMedDisp> omedList = cont.getoMeds();

        cont.buildIntervals();
        PageReference pRef1 = cont.Save();
        PageReference pRef2 = cont.saveAndNew();
        PageReference pRef3 = cont.medListEditPage();
        PageReference pRef4 = cont.rEEGNewPage();
        PageReference pRef5 = cont.patOutcomeEditPage();
        PageReference pRef6 = cont.rEEGAnalysisPage();
        PageReference pRef7 = cont.newOutcome();
        pRef7 = cont.medListPastEditPage();

        Boolean val1 = cont.getIsMgr();
        Boolean notval = cont.getIsNotMgr();
        Boolean val = !notval;
        System.assertEquals( val1, val);
        List<PatientMedDisp> medList = cont.getMeds();
        
        List<ReegDisp> reegList = cont.getReegs();
        List<patientOutcomeDisp> outList = cont.getOutcomes();
        
        List<r_patient__History> patHistList = cont.getPatHistList();

        val = cont.getCanViewStudy();
        val = cont.getCanEditStudy();
        val = cont.getCanViewIntervals();
        val = cont.getCanViewTestFlag();
        
        pRef7 = cont.patSharingPage();

        pRef7 = cont.deletePatient();
        System.assertEquals( '1', '1');
    }
}