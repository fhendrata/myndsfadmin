//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: PEERRequisitionPager
//   PURPOSE: Pager logic for the PEERRequisition result items
//     OWNER: CNS Response
//   CREATED: 03/10/16 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public class PEERRequisitionPager extends BasePager 
{
    public String TechId {get; set;}
    public String AccountName {get; set;}
    public String ListCategory {get; set;}
    public List<PEERRequisitionDisp> DispList {get; set;}

    public PEERRequisitionPager()
    {
        setDispRows(500);
        loadData();
    }

    public override void loadData()
    {
     	if (String.isNotBlank(TechId) && TechId != 'clinicAdmin')
			DispList = GlobalPEERRequisitionDao.getInstance().getListForTech(this);
		else
			DispList = GlobalPEERRequisitionDao.getInstance().getListForClinic(this);
    }
}