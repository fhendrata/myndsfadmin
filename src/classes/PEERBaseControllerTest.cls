//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: PEERBaseControllerTest
// PURPOSE: Test class for the PEERBaseController base class methods
// CREATED: 01/09/15 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
private class PEERBaseControllerTest {
	
	private static testmethod void testMethods() 
	{
		PEER_Requisition__c req = PEERRequisitionDao.getTestObj();
		phy_PeerUnifiedRequisitionController reqController = new phy_PeerUnifiedRequisitionController(new ApexPages.standardController(req));       
    	ApexPages.currentPage().getParameters().put('id', req.id);
    	reqController.loadAction();
		
		Test.startTest();

    	reqController.getUserName();
		reqController.getIsClinicAdminOrAdmin();
		reqController.getIsSysAdmin();
		reqController.getIsSeniorProdMgr();
		reqController.getIsProdMgr();
		reqController.getIsMedDir();
		reqController.getIsSalesDir();

		reqController.getIsWalterReed();
		reqController.getIsClinicAdmin();
		reqController.getIsClinicPhy();
		reqController.getIsPhyTech();
		reqController.getIsProduction();

		reqController.getIsPortalUserMgr();
		reqController.getIsPortalUser3();
		reqController.getIsPortalUser2();

		reqController.IsMilitaryStudyPortalUser();
		reqController.getIsPhyAndTechLevel2();
		reqController.getIsPortalUser1_1();
		reqController.getIsPortalUser1();

		reqController.getIsMgrLevel4();
		reqController.getIsMgrLevel3();
		reqController.getIsMgrLevel2();
		reqController.getIsMgrLevel1();
		reqController.getIsNotQA();
		reqController.getIsNotMgr();
		reqController.getCurrentPhysician();
		reqController.getCurrentAccount();
		reqController.getIsClinicMoreThanOnePhy();
		List<Contact> conList = reqController.getContactListForAccount();

		reqController.addMessage(ApexPages.Severity.ERROR, 'test error');

		Test.stopTest();
	}
	
}