//--------------------------------------------------------------------------------
// COMPONENT: rEEG
// CLASS: phy_PeerRequisitionController2
// PURPOSE: Controller class for EEG upload page
// CREATED: 04/18/12 Ethos Solutions - www.ethos.com
//--------------------------------------------------------------------------------
public with sharing class phy_PeerRequisitionController2 extends BaseWizardController
{
	public boolean isSubmitVerified { get; set; }
	public Contact eegTech {get; set;}
	public boolean isEegUploaded {get; set;}
	public boolean isEEGActive {get; set;}
    
    public phy_PeerRequisitionController2()
    {
    	init();
    }
    
    public void init()
    {
    	this.patId = ApexPages.currentPage().getParameters().get('id');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        
        rDao = new rEEGDao();
        pDao = new patientDao();
        isSubmitVerified = false;
    }
    
    public PageReference loadAction()
    {
    	if (!e_StringUtil.isNullOrEmpty(patId))
    		this.patient = pDao.getById(patId);
    	
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
    		String selectedFieldsStr = 'Id, Name, r_patient_id__r.name, eeg_upload__c, r_patient_id__r.first_name__c, r_physician_id__c, r_patient_id__r.dob__c, eeg_rec_stat__c, r_patient_id__r.Gender__c, eeg_notes__c';
   			this.reeg = rDao.getFieldsById(selectedFieldsStr, reegId);
    	}
    	
    	if (reeg != null)
		{	
			isEegUploaded = (!e_StringUtil.isNullOrEmpty(reeg.eeg_rec_stat__c) && reeg.eeg_rec_stat__c == 'Received');
		}
		
		return null;
    }
    
    public string getEnv()
	{
		return rEEGUtil.getEnvString();
	}
    
   	public PageReference submit()
    {
    	PageReference returnVal = null;
    	Boolean isPI = false;
    	Boolean isPO2 = false;
    		
    	if (!e_StringUtil.isNullOrEmpty(reegId))
    	{
    		String eegNotes = reeg.eeg_notes__c;
    		String selectedFieldsStr = 'Id, Name, r_patient_id__r.name, eeg_upload__c, is_ng_only__c, Translate_Status__c, corr_stat__c, eeg_rec_stat__c, eeg_rec_date__c, rpt_stat__c, req_stat__c, r_patient_id__r.first_name__c, r_physician_id__c, r_patient_id__r.dob__c, r_patient_id__r.Gender__c, eeg_notes__c, r_physician_id__r.Account.PEER_Interactive__c, r_physician_id__r.Account.Report_Build_Type__c';
   			this.reeg = rDao.getFieldsById(selectedFieldsStr, reegId);

   			if (reeg != null)
   			{
   				reeg.eeg_notes__c = eegNotes;
		    	if (!e_StringUtil.isNullOrEmpty(reeg.eeg_upload__c) && reeg.eeg_upload__c == 'Yes')
		    	{
					if ((!e_StringUtil.isNullOrEmpty(reeg.r_physician_id__c)) && reeg.r_physician_id__r.Account != null)
		    		{
		    			isPI = (reeg.r_physician_id__r.Account.PEER_Interactive__c != null &&  reeg.r_physician_id__r.Account.PEER_Interactive__c);
		    			isPO2 = (!e_StringUtil.isNullOrEmpty(reeg.r_physician_id__r.Account.Report_Build_Type__c) && reeg.r_physician_id__r.Account.Report_Build_Type__c == 'PEER Online 2.0');
		    		}
		    		
					isEegUploaded = true;
				}
				else
				{
					string errorMsg = 'The EEG file needs to be uploaded before submitting';
					ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.CONFIRM, errorMsg);
		        	ApexPages.addMessage(msg);
				}
			}
		}
		
		if (isEegUploaded && reeg != null)
		{		
			reeg.Translate_Status__c = (reeg.is_ng_only__c || isPI || isPO2) ? 'N/A' : 'Pending';
			reeg.corr_stat__c = 'New';
			reeg.eeg_rec_stat__c = 'Received';
			reeg.eeg_rec_date__c = DateTime.now();
			
			reeg.rpt_stat__c = 'In Progress';  
			reeg.req_stat__c = 'In Progress';
			
			if (isPO2)
				rDao.SavePEEROnly(reeg, 'UPDATE_DB_STATUS');
			else
				rDao.Save(reeg, 'UPDATE_DB_STATUS');
			
			returnVal = gotoReegView();
		}
	    
	    return returnVal; 
    } 
     
	    //---TEST METHODS ------------------------------
    public static testMethod void testUploadController()
    {
    	phy_PeerRequisitionController2 cont = new phy_PeerRequisitionController2();
		cont.IsTestCase = true;
    	r_reeg__c reeg = rEEGDao.getTestReeg();
        cont.reeg = reeg;
        patientDao patDao = new patientDao();
        patDao.IsTestCase = true;
        
        cont.patient = patDao.getById(reeg.r_patient_id__c);
 		cont.patId = ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
    	
    	PageReference pRef = ApexPages.currentPage();
    	pRef.getParameters().put('id',reeg.id);

		cont.loadAction();
		
		rEEGDao rDao = new rEEGDao();
		rDao.saveSObject(reeg);
        
        cont.isSubmitVerified = true;
        cont.reegId = reeg.Id;
        reeg.reeg_type__c = 'Type II';
        rDao.saveSObject(reeg);
        
        cont.submit();
   
        System.assertEquals( 1, 1);
    }
}