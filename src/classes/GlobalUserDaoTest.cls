//--------------------------------------------------------------------------------
// COMPONENT: CNS Response
// CLASS: GlobalUserDaoTest
// PURPOSE: Test class for GlobalUserDao class methods
// CREATED: 01/02/18 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
@isTest
public with sharing class GlobalUserDaoTest 
{
	@testSetup static void testSetup()
	{
		Account a = new Account(name = 'test account');
        insert a;
        Contact c = new Contact(FirstName = 'Joe', LastName = 'Test1234', AccountId = a.Id);
        insert c;
		User testUser = getTestUser('Physician Portal Manager', 'Joe', 'Banner1234', c.Id);

	}

	public static testMethod void testUserDao()
    {
    	List<User> userList = [select Id from User where firstname = 'Joe' and lastname = 'Banner1234'];
    	System.assertNotEquals(userList.size(), 0);

    	List<Contact> contactList = [select Id from Contact where firstname = 'Joe' and lastname = 'Test1234'];
    	System.assertNotEquals(contactList.size(), 0);

    	Test.startTest();

        System.runAs(userList[0]) 
        {
	        User currentUser = GlobalUserDao.getInstance().getById(userList[0].Id);
	        System.assertNotEquals(currentUser, null);
	        System.assertEquals(currentUser.Id, userList[0].Id);
	        
	        GlobalUserDao.getInstance().getUserIDByContactID(contactList[0].Id);
	    }

	    Test.stopTest();
    }    

    /**
	* @Description This method will get a new User record for testing
	* @param profileName - Profile of the User
	* @param firstName - First name of the User
	* @param lastName - Last name of the User
	* @return A new User record
	*/
	public static User getTestUser(String profileName, String firstName, String lastName, String conId)
    {
        List<Profile> profile = [Select Id, Name from Profile where Name = :profileName];
        User newUser = new User();
        newUser.FirstName = firstName;
        newUser.LastName = lastName;
        newUser.Email = 'test@example.com';
        newUser.Phone = '1231231234';
        newUser.LocaleSidKey = 'en_US';
        newUser.Alias = '' + firstName.left(1).toLowerCase() + firstName.left(4).toLowerCase();
        newUser.TimeZoneSidKey = 'America/Los_Angeles';
        newUser.EmailEncodingKey = 'ISO-8859-1';
        newUser.UserName = firstName + '.' + lastName + '@apexTest' + UserInfo.getUsername().substringAfter('@');
        newUser.LanguageLocaleKey = 'en_US';
        newUser.ProfileId = [select Id from Profile where Name = :profileName].get(0).Id;

        if (String.isNotBlank(conId))
            newUser.ContactId = conId;
        else
        {
            List<UserRole> urList = new List<UserRole>([SELECT Id FROM UserRole WHERE Name = 'Production Manager']);
            if(urList != null && !urList.isEmpty() && urList.get(0) != null)
                newUser.UserRoleId = urList.get(0).Id;
        }

        insert newUser;

        return newUser;
    }


}