@isTest
private class LeadConvertMappingTest {
	
	@isTest static void testTrigger() {
		Account a = new Account(name = 'test account', Report_Build_Type__c = 'PEER Online 2.0');
        insert a;
        Contact c = new Contact(FirstName = 'Joe', LastName = 'Test', AccountId = a.Id);
        insert c;
		Lead testLead = new Lead();
		testLead.LastName = 'Test';
		testLead.Status = 'Open';
		testLead.Title = 'Mr';
		testLead.Company = 'LeadTest';
		testLead.LeadSource = 'Print';
		testLead.Lead_Source_Detail__c = '2008-Oprah Mag-If Youre Barely Hanging On';
		testLead.Lead_Source_Detail_2__c = 'test';
		testLead.Practice_Type__c = 'Private';
		testLead.Number_of_Physicians_in_Practice__c = 5;
		testLead.Ave_New_Patients_Month__c = 5;
		testLead.Primary_Specialty__c = 'Eating Disorders';
		testLead.Other_Specialty_s__c = 'Psychiatry; GP; Pediatrics';
		testLead.Insurance_Carrier_s_Accepted__c = 'Anthem, Inc.; Assurant Health; Blue Cross; BlueCross BlueShield; Blue Cross of OR; CIGNA Corp.';
		testLead.Other_Insurance_Carrier__c = 'meh';
		testLead.Treats_Military_Veterans__c = 'Yes';
		testLead.Patient_Type_s_Treated__c = 'Adolescents; Adults; Geriatrics';
		testLead.Other_Treatment_Type_s_Provided__c = 'Amen Spect Scan; Biofeedback; Neurofeedback';
		testLead.EEG_Availability__c = 'In-House';
		testLead.EEG_Tech__c = c.Id;

		insert testLead;

		Test.startTest();
		Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(testLead.Id);

		LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted = true LIMIT 1];
		lc.setConvertedStatus(convertStatus.MasterLabel);

		//Database.LeadConvertResult lcr = Database.convertLead(lc);
		//System.assert(lcr.isSuccess());
		Test.stopTest();

		//Lead testLead2 = [Select Id, ConvertedAccountId, ConvertedContactId from Lead where Id =: testLead.Id];

		//Account account = [Select Id, Practice_Type__c,Number_of_Physicians_in_Practice__c,EEG_Tech__c,
		//							Treats_Military_Veterans__c,Patient_Type_s_Treated__c,EEG_Availability__c,Insurance_Carrier_s_Accepted__c,
		//							Other_Insurance_Carrier__c from Account where Id =: testLead2.ConvertedAccountId];
		//Contact contact = [Select Id, Practice_Type__c, Primary_Specialty__c, Other_Specialty_s__c, Ave_New_Patients_Month__c,
		//							Treats_Military_Veterans__c,Patient_Type_s_Treated__c,Other_Treatment_Type_s_Provided__c,Insurance_Carrier_s_Accepted__c,
		//							Other_Insurance_Carrier__c from Contact where Id =: testLead2.ConvertedContactId];

		//System.assertEquals(contact.Practice_Type__c ,testLead.Practice_Type__c);
		//System.assertEquals(contact.Primary_Specialty__c, testLead.Primary_Specialty__c);
		//System.assert(contact.Other_Specialty_s__c.contains('Psychiatry'));
		//System.assertEquals(contact.Ave_New_Patients_Month__c, testLead.Ave_New_Patients_Month__c);
		//System.assertEquals(contact.Treats_Military_Veterans__c, testLead.Treats_Military_Veterans__c);
		//System.assertEquals(contact.Patient_Type_s_Treated__c.contains('Adolescents'),true);
		//System.assertEquals(contact.Other_Treatment_Type_s_Provided__c.contains('Neurofeedback'),true);
		//System.assert(contact.Insurance_Carrier_s_Accepted__c.contains('Assurant Health'));
		//System.assertEquals(contact.Other_Insurance_Carrier__c, testLead.Other_Insurance_Carrier__c);
		//System.assertEquals(account.Practice_Type__c,testLead.Practice_Type__c);
		//System.assertEquals(account.Number_of_Physicians_in_Practice__c,testLead.Number_of_Physicians_in_Practice__c);
		//System.assertEquals(account.EEG_Tech__c,testLead.EEG_Tech__c);
		//System.assertEquals(account.Treats_Military_Veterans__c,testLead.Treats_Military_Veterans__c);
		//System.assertEquals(account.Patient_Type_s_Treated__c.contains('Geriatrics'),true);
		//System.assertEquals(account.EEG_Availability__c,testLead.EEG_Availability__c);
		//System.assertEquals(account.Insurance_Carrier_s_Accepted__c.contains('Anthem, Inc.'),true);
		//System.assertEquals(account.Other_Insurance_Carrier__c,testLead.Other_Insurance_Carrier__c);
	}
	
}