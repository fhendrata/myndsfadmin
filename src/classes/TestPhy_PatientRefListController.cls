@isTest
private class TestPhy_PatientRefListController {
	
	@isTest static void testController() {
		Patient_Referral__c testPR = patientReferralDao.getInstance().getTestPatientReferral();
		phy_PatientReferralListController cont = new phy_PatientReferralListController();
		cont.patSearchString = 'Test';
		cont.searchPatients();
		cont.getPatientReferrals();
	}
	
}