public class rEEGTestDisp
{
    private String name;
    public String getName()
    {
        return name;
    }
    public void setName(String val)
    {
        name = val;
    }

    private r_reeg__c obj;
    public r_reeg__c getObj()
    {
        return obj;
    }
    public void setObj(r_reeg__c s)
    {
        obj = s;
    }

    public void Setup(r_reeg__c o)
    {
        obj = o;
    }

    //------------TEST-----------------------------
    public static testMethod void testrEEGTestDisp()
    {
        r_reeg__c obj = new r_reeg__c();

        rEEGTestDisp disp = new rEEGTestDisp();
        disp.setObj(obj);
        disp.Setup( obj);

        r_reeg__c obj2 = disp.getObj();

        System.assertEquals( obj, obj2);

        disp.setName('1');
        System.assertEquals( disp.getName(), '1');
    }
}