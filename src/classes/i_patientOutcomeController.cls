//--------------------------------------------------------------------------------
// COMPONENT: rEEG
//     CLASS: i_patientOutcomeControllerExt
//   PURPOSE: Controller class for the iPad patient outcome page
// 
//     OWNER: CNS Response
//   CREATED: 11/19/10 Ethos Solutions - www.ethos.com 
//--------------------------------------------------------------------------------
public with sharing class i_patientOutcomeController extends BaseWizardController
{
   	public List<PatientOutcomeMedDisp> oMeds  { get; set; }
    public List<PatientOutcomeMedDisp> newMeds  { get; set; }
    
    private patientOutcomeMedDao ocMedDao { get; set; }
    private patientIntervalsDao intDao { get; set; }
    public boolean isCurrentMeds { get; set; }
    public boolean isNewOutcome { get; set; }
    
	//---Base Constructor (go to test mode)
	public i_patientOutcomeController()
	{
	}
	
    // ---Constructor from the standard controller
    public i_patientOutcomeController( ApexPages.StandardController stdController)
    {
    	outcome = (r_patient_outc__c) stdController.getRecord();
    }
    
    public void loadAction() 
    {
    	this.patId = ApexPages.currentPage().getParameters().get('pid');
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.qsId = ApexPages.currentPage().getParameters().get('qsid');
        this.outcomeId = ApexPages.currentPage().getParameters().get('oid');
        this.tid = ApexPages.currentPage().getParameters().get('tid');
        string newPatStr = ApexPages.currentPage().getParameters().get('np');
        this.isNewPat = (!e_StringUtil.isNullOrEmpty(newPatStr) && newPatStr == 'true');
        string wizStr = ApexPages.currentPage().getParameters().get('wiz');
        this.isWiz = (!e_StringUtil.isNullOrEmpty(wizStr) && wizStr == 'true');
        String strIsNew = ApexPages.currentPage().getParameters().get('isNew');
        this.IsNew = (!e_StringUtil.isNullOrEmpty(strIsNew) && strIsNew == 'true');
        this.isiPad = true;
        isNewOutcome = (IsNew && (e_StringUtil.isNullOrEmpty(outcomeId) || outcomeId.equalsIgnoreCase('null')));
        string wrStr = ApexPages.currentPage().getParameters().get('wr');
        this.isWR = (!e_StringUtil.isNullOrEmpty(wrStr) && wrStr == 'true');
        
        ocMedDao = new patientOutcomeMedDao();
        intDao = new patientIntervalsDao();
        rDao = new rEEGDao();
        pDao = new patientDao();
        outDao = new patientOutcomeDao();
        
        isCurrentMeds = false;
        
        if (!isNewOutcome)
    	{
    		outcome = outDao.getById(outcomeId);
    	}
    	else if (outcome != null)
    	{
    		outcome = outDao.getById(outcome.Id);
    	}
    	
    	if (outcome == null)
    	{
    		outcome = new r_patient_outc__c();
    	}
    	
    	if (isNewOutcome)
    	{
    		if (!e_StringUtil.isNullOrEmpty(this.reegId))
    		{
    			r_reeg__c reeg = rDao.getById(this.reegId);
    			if (reeg != null)
    				outcome.rEEG__c = reeg.Id;
    		}
    		    		
    		if (!e_StringUtil.isNullOrEmpty(patId))
    		{ 
    			patient = pDao.getById(patId);
    			loadCurrentOutcomeMeds(patId);	
    		}
    		
    		Date oDate = date.today();
    		outcome.outcome_date__c = oDate;
    		outcome.submitted_type__c = 'iPad';
    		outcome.cgi__c = '4 - Baseline';
    		
    		if (e_StringUtil.isNullOrEmpty(outcome.patient__c))
	    		outcome.patient__c = patId;
    	}
    	else
    	{
   			if (!e_StringUtil.isNullOrEmpty(outcome.rEEG__c))
   				reeg = rDao.getById(outcome.rEEG__c, true);
   			
   			patient = pDao.getById(outcome.patient__c);
   			outcome.outcome_date__c = Date.today();
   			patient = pDao.getById(outcome.patient__c);
			
			oMeds = getAllOutcomeMeds(outcome.Id);
    	}
    	
    	addBlankMedRows();
    	       	
    	if (oMeds == null) oMeds = new List<PatientOutcomeMedDisp>();    
    	
    	isCurrentMeds = (oMeds != null && oMeds.size() > 0); 
    }
    
    public PageReference previousWizAction()
    {
        PageReference returnVal = new PageReference('/apex/iwrEEGEdit');
        returnVal.getParameters().put('isNew', string.valueOf(isNew));
        
 		if (patient != null) returnVal.getParameters().put('pid', patient.id);
        if (!e_StringUtil.isNullOrEmpty(patId)) returnVal.getParameters().put('pid', patId);
        if (reeg != null) returnVal.getParameters().put('id', reeg.id);
        if (!e_StringUtil.isNullOrEmpty(reegId)) returnVal.getParameters().put('id', reegId);
        
        returnVal.getParameters().put('wiz', string.valueOf(isWiz));
        returnVal.getParameters().put('np', string.valueOf(isNewPat));
        returnVal.getParameters().put('oid', outcome.id);
        if (isWR != null && isWR)
        		returnVal.getParameters().put('wr', 'true');
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference nextWizAction()
    {
        saveActionOverride();
        
        PageReference returnVal = new PageReference('/apex/iwrEEGDX');
        returnVal.getParameters().put('isNew', string.valueOf(isNew));
       
		if (patient != null) returnVal.getParameters().put('pid', patient.id);
        if (!e_StringUtil.isNullOrEmpty(patId)) returnVal.getParameters().put('pid', patId);
        if (reeg != null) returnVal.getParameters().put('rid', reeg.id);
        if (!e_StringUtil.isNullOrEmpty(reegId)) returnVal.getParameters().put('rid', reegId);
        
        returnVal.getParameters().put('wiz', string.valueOf(isWiz));
        returnVal.getParameters().put('np', string.valueOf(isNewPat));
        returnVal.getParameters().put('oid', outcome.id);
        if (isWR != null && isWR)
        		returnVal.getParameters().put('wr', 'true');
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference cancelWizardAction()
    {
    	PageReference returnVal = cancelWizAction();
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference cancelOutcomeAction()
    {
    	if (isWiz && !e_StringUtil.isNullOrEmpty(reegId))
    	{
    		rDao.deleteSObjectById(reegId);
    		
    		if (!e_StringUtil.isNullOrEmpty(patId))
    			return new PageReference('/apex/iwPatientDetail?show=all&id=' + patId); 
    		else
    			return new PageReference('/apex/iwPatient');
    	}
    	
        return new PageReference('/apex/iwPatientDetail?show=all&id=' + patient.Id);   
    }
    
    public PageReference previousActionOverride()
    {
    	PageReference returnVal = new PageReference('/apex/rEEGWizard3Page?rid=' + this.reegId + '&pid=' + this.patId + '&oid=' + outcome.Id 
                                                    + '&qsid=' + this.qsId + '&np=' +  this.isNewPat + '&wiz=' + this.isWiz);
        returnVal.setRedirect(true);
        return returnVal;
    }
    
    public PageReference nextActionOverride()
    {
    	saveActionOverride();
	   	PageReference returnVal = new PageReference('/apex/rEEGWizard4Page?rid=' + this.reegId + '&pid=' + this.patId + '&oid=' + outcome.Id
                                                    + '&qsid=' + this.qsId + '&np=' +  this.isNewPat + '&wiz=' + this.isWiz);
        returnVal.setRedirect(true);
        return returnVal;
    } 
    
    public PageReference editActionOverride()
    {
    	PageReference pr = new PageReference('/apex/iwPatientOutcomeEdit');
    	pr.getParameters().put('id', outcome.Id);
    	
    	pr.getParameters().put('retUrl', '/apex/iwPatientOutcomeView?id=' + outcome.Id + '&oid=' + outcome.Id);
    	
    	pr.setRedirect(true);
        return  pr;	
    }
    
    //---Build the existing outcome meds list (meds from previous outcome)
    public void loadCurrentOutcomeMeds(string pid)
    {
	    if (oMeds == null)
        { 
        	oMeds = new List<PatientOutcomeMedDisp>();  
            // List<string> previousIds = new List<string>();
            PatientOutcomeMedDisp oMed;          
            Integer ctr = -1;	
             
            //---Get the current meds
            List<r_outcome_med__c> medList = outDao.getExistingOutcomeMeds(pid);
            if (medList != null) 
            {
            	for(r_outcome_med__c oMedRow : medList) 
	           	{
		       		ctr++;  
               		oMed = new PatientOutcomeMedDisp();
               		oMed.setDisplayAction(true);
               		oMed.setRowNum(ctr);
	               
	               	r_outcome_med__c hldgMed;
	               	if (isNewOutcome)
	               	{
	               		hldgMed = new r_outcome_med__c();
						hldgMed.med_name__c = oMedRow.med_name__c;
	               		hldgMed.dosage__c = oMedRow.dosage__c;
	               		hldgMed.unit__c = oMedRow.unit__c;
	               		hldgMed.frequency__c = oMedRow.frequency__c;
	               		hldgMed.start_date__c = oMedRow.start_date__c;
	               		hldgMed.end_date__c = oMedRow.end_date__c;
	               		hldgMed.previous_med__c = oMedRow.Id;
	               		hldgMed.start_date_is_estimated__c = oMedRow.start_date_is_estimated__c;
	               	}
	               	else
	               	{
	               		oMedRow.r_outcome_id__c = null;
	               		hldgMed = oMedRow;
	               	}
					oMed.Setup(hldgMed);
               		// oMed.Setup(oMedRow);     
                	// previousIds.Add(oMedRow.Id);                     
                	oMeds.Add(oMed);		      
                
                	// running list of previous meds to update on save
				//	medsToUpdate.add(oMedRow.Id);          
	            }
             }
             else 
             {
             	oMeds = null;
             }
        }    
    }
    
    //---Build the existing outcome meds list (meds from previous outcome)
    public List<PatientOutcomeMedDisp> getAllOutcomeMeds(string oid)
    {
        List<r_outcome_med__c> medList = ocMedDao.getAllOutcomeMedsForOutcome(oid);
        oMeds = getOutcomeMedDispList(medList);
            
        return oMeds;
    }
    
    private List<PatientOutcomeMedDisp> getOutcomeMedDispList(List<r_outcome_med__c> medList)
    {
    	List<PatientOutcomeMedDisp> medDispList = new List<PatientOutcomeMedDisp>();  
        PatientOutcomeMedDisp oMed;          
        Integer ctr = -1;
            
        if (medList != null) 
        {
        	for(r_outcome_med__c oMedRow : medList) 
        	{
	        	ctr++;  
	         	oMed = new PatientOutcomeMedDisp();
	         	oMed.setDisplayAction(true);
	         	oMed.setRowNum(ctr);
		     	oMed.Setup(oMedRow);
		     	medDispList.Add(oMed);		                
	     	}
        }	   
        return medDispList;
    }
    
	public PageReference saveActionOverride()
    {
    	outDao.saveSObject(outcome);

        if (oMeds != null && oMeds.size() > 0) 
        {
            //---Loop through each row
            for(PatientOutcomeMedDisp oMedRow : oMeds) 
            {
                if (oMedRow != null)
                {
	                r_outcome_med__c outcMed = oMedRow.getObj();
	                if (outcMed != null) 
	                {
	                	if (e_StringUtil.isNullOrEmpty(outcMed.r_outcome_id__c))
	                		outcMed.r_outcome_id__c = outcome.Id;
	                	SaveOutcomeMed(oMedRow.getObj());
	                }
                }
            }
        }
        
        if (newMeds != null && newMeds.size() > 0)
        {
         	for(PatientOutcomeMedDisp nMedRow : newMeds) 
            {
	            if (nMedRow != null) 
	            {
                    if (nMedRow.getObj() == null) 
                    {
                       debug('-- oMedRow.getObj() is null');         
                    } 
                    else 
                    {
                    	if (nMedRow.getObj().start_date__c == null && !e_StringUtil.isNullOrEmpty(nMedRow.getObj().med_name__c)) {
                    		return null;
                    	} 
                    	else 
                    	{
                    		r_outcome_med__c outcMed = nMedRow.getObj();
                    		outcMed.r_outcome_id__c = outcome.Id;
                    		nMedRow.Setup(outcMed);
	                  		SaveOutcomeMed(outcMed);   
	                  		if (outcMed != null && outcMed.med_name__c != null && outcMed.med_name__c != '')
	                  			oMeds.add(nMedRow);
	                 	}                   
	                }
	            }
	        }
        }
        
        if (isWiz && !e_StringUtil.isNullOrEmpty(reegId))
        {
        	reeg = rDao.getById(reegId);
        	if (reeg != null && !reeg.med_no_med__c && !e_StringUtil.isNullOrEmpty(reeg.reeg_type__c) && reeg.reeg_type__c != 'Type II')
			{
				reeg.reeg_type_mod__c = rEEGUtil.getMedicatedTestMod(oMeds);
				rDao.Save(reeg, '');
			}
        }
        
        if (patient == null)
        {
	        pDao = new patientDao();
	        patient = pDao.getById(patId);
        }
        
        if (patient != null)
        {
	        patientIntervalBuilder intBuilder = new patientIntervalBuilder();
	  		intBuilder.buildAllIntervalsForPat(patient.Id, patient.OwnerId);
        }
        PageReference pr = new PageReference('/apex/iwPatientOutcomeView'); 
    	pr.getParameters().put('pid', patId);
    	pr.getParameters().put('id', outcome.Id);
    	pr.getParameters().put('oid', outcome.Id);
    	pr.setRedirect(true);

        return  pr;
    }
    
	// ---Save an outcome med row
    private void SaveOutcomeMed(r_outcome_med__c medObj)
    {
        if (medObj != null && medObj.med_name__c != null && medObj.med_name__c.trim() != '')
            ocMedDao.saveSObject(medObj);
    }

    public PageReference addBlankMedRows()
    {
    	if (newMeds == null) newMeds = new List<PatientOutcomeMedDisp>();    
    	    	
       	for (integer i = 0; i < 3; i++)
    	{
    		PatientOutcomeMedDisp medDisp = new PatientOutcomeMedDisp();
    		r_outcome_med__c newMed = new r_outcome_med__c();
    		newMed.unit__c = 'mg';	
    		medDisp.setObj(newMed);		
    		medDisp.setRowNum(newMeds.size());
    		newMeds.add(medDisp);    	
    	}

    	return null;
    }
    
    // if outcome is deleted, need to delete associate medication
    public PageReference deleteActionOverride()
    { 
        if (oMeds != null && oMeds.size() > 0) 
        {
        	List<r_outcome_med__c> medDelList = new List<r_outcome_med__c>();
            for(PatientOutcomeMedDisp oMedRow : oMeds) 
            {
                if (oMedRow != null) 
                {
                    if (oMedRow.getObj() == null) 
                    {
                       // addMessage('oMedRow.obj is null');         
                    }
                    else 
                    {
						// Should not allow deleting of data, do a soft delete
           				oMedRow.getObj().put('is_deleted__c',true);
           				medDelList.add(oMedRow.getObj());                    
                    }
                }
            }
            
            ocMedDao.deleteSObjectList(medDelList);
        } 
        
        outDao.deleteSObject(outcome); 
        
        PageReference pr = new PageReference('/apex/iwPatientDetail'); 
        pr.getParameters().put('show', 'all');
    	pr.getParameters().put('id', patId);
    	pr.setRedirect(true);
    	
    	return pr;
    }
    
    public PageReference refreshAutoAction()
    {
    	return null;
    }
    
    public String OutDate  
    {     	
    	get { return e_StringUtil.getDateString(outcome.outcome_date__c); }    	 
    	set { outcome.outcome_date__c = e_StringUtil.setDateFromString(value); } 
    }    

    public String NxtAppt  
    {     	
    	get { return e_StringUtil.getDateString(outcome.next_appointment__c); }    	 
    	set { outcome.next_appointment__c = e_StringUtil.setDateFromString(value); } 
    }      
    
      // ---TEST METHODS ------------------------------
    public static testMethod void testController()
    {
    	i_patientOutcomeController cont = new i_patientOutcomeController();
    	cont.IsTestCase = true;
    	cont.oMeds = new List<PatientOutcomeMedDisp>();
    	cont.newMeds = new List<PatientOutcomeMedDisp>();
		cont.isCurrentMeds = true;
		cont.isNewOutcome = true;
		
		
		r_reeg__c reeg = rEEGDao.getTestReeg();
        cont.reeg = reeg;
        patientDao patDao = new patientDao();
        patDao.IsTestCase = true;
        
        cont.patient = patDao.getById(reeg.r_patient_id__c);
        
        patientOutcomeDao patOutcomeDao = new patientOutcomeDao();
        r_patient_outc__c outcome = patOutcomeDao.getTestOutcome(reeg.r_patient_id__c);
        patientOutcomeMedDao medDao = new patientOutcomeMedDao();
        medDao.IsTestCase = true;
        r_outcome_med__c outMed =  medDao.getTestOutcomeMed(outcome.Id);    
        
        //ApexPages.currentPage().getParameters().put('qsid');
        ApexPages.currentPage().getParameters().put('rid', reeg.Id);
 		ApexPages.currentPage().getParameters().put('oid', outcome.Id);
 		cont.patId = ApexPages.currentPage().getParameters().put('pid', reeg.r_patient_id__c);
 		ApexPages.currentPage().getParameters().put('wiz', 'true');
 		
		ApexPages.currentPage().getParameters().put('np', 'true');
		ApexPages.currentPage().getParameters().put('isNew', 'true');
		ApexPages.currentPage().getParameters().put('ln', 'last');
		ApexPages.currentPage().getParameters().put('fn', 'first');
		ApexPages.currentPage().getParameters().put('dob', '01/01/1955');
    
	    cont.loadAction();
	    cont.loadCurrentOutcomeMeds(reeg.r_patient_id__c);
	    string testStr = cont.OutDate;
	    testStr = cont.NxtAppt;
	    cont.refreshAutoAction();
	    cont.previousWizAction();
	    cont.nextWizAction();
	    
	    ApexPages.currentPage().getParameters().put('isNew', 'true');
	    ApexPages.currentPage().getParameters().put('oid', '');
	    cont.outcomeId = null;
	    cont.isNew = true;
	    cont.loadAction();
	    
	    cont.nextActionOverride();
	    cont.previousActionOverride();
	    cont.editActionOverride();
	    
	    cont.deleteActionOverride();
	    cont.cancelWizardAction();
	    cont.cancelOutcomeAction();
	    

    }
    
 }