public with sharing class wr_CNSVarsController extends BaseDrugReportController
{
    public wr_CNSVarsController()
    {
        this.reegId = ApexPages.currentPage().getParameters().get('rid');
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        this.cnsTestId = ApexPages.currentPage().getParameters().get('ctid');
        this.cnsTestId2 = ApexPages.currentPage().getParameters().get('ctid2');
        this.cnsTestId3 = ApexPages.currentPage().getParameters().get('ctid3');
        this.cnsTestId4 = ApexPages.currentPage().getParameters().get('ctid4');
    }
    
    public PageReference loadAction()
    {
    	rDao = new rEEGDao();
    	if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            this.reeg = rDao.getById(reegId);
            baseUrl = '/apex/wr_PeerReportS2?rid=' + reeg.Id;
            cnsVarsUrl = '/apex/wr_CNSVarsS2?rid=' + reeg.Id + '&ctid=' + reeg.corr_cns_id__c;
            
        }
        else
        {
            this.reeg = new r_reeg__c();
        }    
    	
        return null;    
    }
    
    public PageReference summaryAction()
    {
        PageReference pr = new PageReference('/apex/wr_PeerSummaryS2?pgid=' + this.pageId + '&rid=' + this.reegId);
        pr.setRedirect(true);
        
        return pr;
    }   
    
    public PageReference returnAction()
    {
        PageReference pr = new PageReference('/apex/wr_PatientListS2');
  
        pr.setRedirect(true);
        return pr;
    }
    
    //------------------ TEST METHODS -------------------
    
    private static testmethod void testController()
    {
         wr_CNSVarsController cv = new wr_CNSVarsController();
         cv.returnAction();   	
    	 cv.summaryAction();
    	 cv.loadAction();
    	
    }
}