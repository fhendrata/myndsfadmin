public class patientMedDisp
{
    public void setDosageDisplay()
    {
        obj.Unknown__c = (obj.dosage__c != null && obj.Unit__c != null) ? obj.dosage__c + ' ' + obj.Unit__c : 'unknown';
    }
    
    private r_patient_med__c obj;
    public r_patient_med__c getObj()
    {
        return obj;
    }
    public void setObj(r_patient_med__c s)
    {
        obj = s;
    }
                    
    public void Setup(r_patient_med__c o)
    {
        obj = o;
        setDosageDisplay();
    }

    //---Row number of the display element
    private Integer rowNum;
    public Integer getRowNum()
    {
        return rowNum;
    }    
    public void setRowNum(Integer value)
    {
        rowNum = value;
    }

    //------------TEST-----------------------------
    public static testMethod void testDisp()
    {
        r_patient_med__c obj = new r_patient_med__c();

        patientMedDisp disp = new patientMedDisp();
        disp.setObj(obj);
        disp.Setup( obj);

        r_patient_med__c obj2 = disp.getObj();

        System.assertEquals( obj, obj2);

        disp.setRowNum(1);
        System.assertEquals( disp.getRowNum(), 1);
    }
}