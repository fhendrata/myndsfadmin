//--------------------------------------------------------------------------------
// COMPONENT: PEER Online 2.0
// CLASS: peer_SummaryController
// PURPOSE: Controller class for the Summary page
// CREATED: 09/03/13 Ethos Solutions - www.ethos.com
// Author: Joe DePetro
//--------------------------------------------------------------------------------
public with sharing class peer_SummaryController extends BaseDrugReportController
{
    public string drugName {get; set;}
    public string BrainMapUrl {get; set;}
    public boolean IsNeuroRptAvail {get; set;}
    public boolean isNeuroRptOrdered { get; set; }
    public String drugFilter {get; set;}
    public String printUrl {get; set;}
    public boolean isOkToShowReport {get; set;}
    public Boolean isPOTestDr {get; set;}

    public peer_SummaryController()
    {
        getParams();
    }
    
    public peer_SummaryController(ApexPages.StandardController stdController)
    {
        getParams();
    }
    
    private void getParams()
    {
    	this.reegId = ApexPages.currentPage().getParameters().get('rid');
        if (e_StringUtil.isNullOrEmpty(reegId))
            this.reegId = ApexPages.currentPage().getParameters().get('id');
        this.pageId = ApexPages.currentPage().getParameters().get('pgid');
        rDao = new rEegDao();
    }
    
    public PageReference loadAction()
    {
        isPOTestDr = UserInfo.getUserName().equalsIgnoreCase('md5@ethos.com');
        Boolean isCompletedGroup = false;
        if (!e_StringUtil.isNullOrEmpty(reegId))
        {
            this.reeg = rDao.getById(reegId, true);
            
            if(this.reeg.req_stat__c != 'Complete')
            {
            	PageReference pg = new PageReference('/apex/phy_PeerViewS2?id=' + this.reeg.Id);
            	pg.setRedirect(true);
            	return pg;
            }

            Boolean isControlGroup = ((reeg.r_patient_id__r.CA_Control_Group_Type__c != null && reeg.r_patient_id__r.CA_Control_Group_Type__c != '') && (reeg.r_patient_id__r.CA_Control_Group_Type__c == 'Control Group' || reeg.r_patient_id__r.CA_Control_Group_Type__c == 'Not Randomized Group'));
            isCompletedGroup = ((reeg.r_patient_id__r.CA_Control_Group_Type__c != null && reeg.r_patient_id__r.CA_Control_Group_Type__c != '') && reeg.r_patient_id__r.CA_Control_Group_Type__c == 'Completed Group');
            
            isOkToShowReport = !isControlGroup || getIsSysAdmin();
         
            baseUrl = '/apex/peer_Report?rid=' + reeg.Id;
            baseReport2Url = '/apex/peer_Report?rid=' + reeg.Id;
            baseReportTabbedUrl = '/apex/peer_Report?rid=' + reeg.Id;
            baseDrugGoupUrl = '/apex/peer_DrugClass?rid=' + reeg.Id;
            BrainMapUrl = '/apex/PEERBrainMapView?rid=' + reeg.id;
            //baseDrugGoupUrl = '/apex/wr_PeerDrugClassS2?rid=' + reeg.Id;
            
            if (!e_StringUtil.isNullOrEmpty(pageId))
            {
                DrugTreeMenuUtil.MenuItem menuItem = menuPEERMap.get(pageId);
                if (menuItem != null)
                    drugName = menuItem.drugName;
            }
        }
        else
        {
            this.reeg = new r_reeg__c();
        }  
         
        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        isNeuroRptOrdered = (reeg != null && reeg.neuro_stat__c == 'In Progress');
        IsNeuroRptAvail = (reeg != null && reeg.neuro_stat__c == 'Complete');
        
        setupDrugFilter(isCompletedGroup);
        
        printUrl = getPrintViewPage();

        return null;    
    }
    
    public PageReference returnAction()
    {
    	return null;
       // return Page.peer_ReportList;
    }

    public void setupDrugFilter(Boolean isCompletedGroup)
    {
        drugFilter = '';
        
        if (IsDrugLimitedStudy && !isCompletedGroup)
        {
            DrugUtil du = DrugUtil.getInstance();
            try
            {
               rEEGDxDao rDao = new rEEGDxDao();
               List<r_reeg_dx__c> dxs = rDao.getByrEEGId(this.reeg.Id);
               drugFilter = du.getHexFilter(dxs);
            }
            catch(Exception e)
            {
                drugFilter = '';
            }
        }
    }
    
    //---temporary
    public String getPrintViewPage()
    {
        String key = '';
        string versionTxt = '';
        if (!e_StringUtil.isNullOrEmpty(this.reegId))
        {
            String part1 = this.reegId.substring(5, 10);
            String part2 = this.reegId.substring(0, 5);
            String part3 = this.reegId.substring(10, reegId.length());
            key = '6H3a2' + part1 + 's' + part2 + part3;
        }
        
        return 'https://mypeerinteractive.com/Public/Download.aspx?key=' + key + '&v=1&p2=1&nr=1&enc=' + reeg.r_patient_id__c + '&sdh=' + drugFilter;
  
    }
    
    public void orderNeuroReport()
    {
    	//reeg.do_not_send_neuro__c = false;
    	//isNeuroRptOrdered = true;
    	//rDao.Save(reeg, 'ORDER_NEURO_REPORT');
    }
    
    
    //--------- TEST METHODS ----------------------
    @isTest(SeeAllData=true)
    public static void testController()
    {
    	peer_SummaryController psc = new peer_SummaryController();

		r_reeg__c testrEEG = rEEGDao.getTestReeg();
			
    	ApexPages.StandardController sc = new ApexPages.standardController(testrEEG);
    	peer_SummaryController objController = new peer_SummaryController(sc);
    	
    	objController.loadAction();
    	objController.reeg = testrEEG;
    	r_reeg__c reeg = objController.reeg;
    	reeg.req_stat__c = 'Complete';
    	
    	upsert reeg;
    	
    	objController.loadAction();
    	objController.returnAction();

    }

}