public class i_patientIntervalsControllerExt extends BaseController
{
    public r_patient__c patient { get; set; }
    private r_interval__c interval; 
    patientDao dao = new patientDao();
    patientIntervalsDao intDao = new patientIntervalsDao();
	public String RetUrl { get; set;}
	public String ParentId { get; set;}
	public String ObjId { get; set;}

    //--- this is only used for the apex test case
    public i_patientIntervalsControllerExt()
    {
    }

    //---Constructor from the standard controller
    public i_patientIntervalsControllerExt(ApexPages.StandardController stdController)
    {    
        ParentId = ApexPages.currentPage().getParameters().get('pid');
		ObjId = ApexPages.currentPage().getParameters().get('oid');
		this.interval = intDao.getByIntId(ObjId);
    }

    public r_interval__c getInterval()
    {
        return interval;
    } 
    
    public void setInterval(r_interval__c obj)
    {
        interval = obj;
    }
    
    public Integer getDuration()
    {
    	Integer duration;
    	if (interval.start_date__c != null && interval.stop_date__c != null)
	       			duration = interval.start_date__c.daysBetween(interval.stop_date__c);
	   	return duration;
    }
	
	public String getPatientFirstName()
    {
        String returnVal = '';
        if (interval != null && (interval.r_patient__c != null || interval.r_patient__c != 'null' || interval.r_patient__c != ''))
        {
            r_patient__c patient = [Select Id, first_name__c From r_patient__c where Id=:interval.r_patient__c];
            if (patient != null && patient.first_name__c != null && patient.first_name__c != 'null' && patient.first_name__c != '')
            {
                returnVal = patient.first_name__c;
            }
        }
        return returnVal; 
    }
    
    public String getPatientLastName()
    {
        String returnVal = '';
        if (interval != null && (interval.r_patient__c != null || interval.r_patient__c != 'null' || interval.r_patient__c != ''))
        {
            r_patient__c patient = [Select Id, name From r_patient__c where Id=:interval.r_patient__c];
            if (patient != null && patient.name != null && patient.name != 'null' && patient.name != '')
            {
                returnVal = patient.name;
            }
        }
        return returnVal;
    }
	
	public void loadAction()
    {
    }
    
   	//-----------------------------------------------------------------------
    //--                          TEST METHODS                            ---  
    //-----------------------------------------------------------------------
    public static testMethod void testInterval()
    {
    	i_patientIntervalsControllerExt cont = new i_patientIntervalsControllerExt();
    	cont.IsTestCase = true;
    	
    	cont.dao = new patientDao();
    	cont.patient = patientDao.getTestPat();
		cont.intDao = new patientIntervalsDao();
		cont.ParentId = cont.patient.Id;
		cont.RetUrl = '/apex/patientSearchPage';
		patientOutcomeDao patOutDao = new patientOutcomeDao();
		r_patient_outc__c outcome = patOutDao.getTestOutcome(cont.ParentId);		
		cont.ObjId = outcome.Id;
		cont.loadAction();
		cont.setInterval(patientIntervalsDao.getTestInterval());
		r_interval__c intervalObj = cont.getInterval();
		Integer testInt = cont.getDuration();
		string testStr = cont.getPatientFirstName();
		testStr = cont.getPatientLastName(); 
    }
}